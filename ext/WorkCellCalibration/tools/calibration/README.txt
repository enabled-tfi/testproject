CALIBRATION TOOL QUICK GUIDE:

BRIEF EXPLAINATION
The CalibrationTool is a console application providing functionality for calibrating a work cell containing one or more robots and one or more sensors. 
The system assumes that measurements are stored as relative transformations between static frames in the work cell and moving frames on the robots. For the case where the sensor is mounted stationary in the cell it will be the sensor the corresponds to the static frame and the marker on the robot as the moving. 

Internally the tool uses two methods. The first is a global method for calibrating extrinsic parameters (transform between the static frame and robot base and robot tcp and the moving frame) and does not require an initial guess of these transformations. This method does not use the measurement noise specified, but includes an reweighting of measurements to suppress potential outliers.

The second method is an iterative method able to calibrate both the extrinsic and the intrinsic parameters. The method requires a feasible starting guess of both the extrinsic and intrinsic parameters. Using command lines arguments the user can specify which of the intrinsic parameters to calibrate.

To get the most robust calibration it is generally recommended to use both the global and the iterative method. When both are enabled the program starts running the global method to obtain a good starting guess for the extrinsic parameters and subsequently calls the iterative method.


DATA:
To use the global extrinsic calibration one should for each static-moving frame pair use at least 12 measurement, but it is recommended to include more (e.g. 20 or so) to make the calibration more robust.

If calibrating all (both extrinsic and intrinsic) parameters with the global method one should use (at least) in the order to 100 measurements. 

Remember that the quality of the calibration very much on the quality of the data provided. If you provide the method with low quality measurements, you will also get a low quality calibration.

The measurement file is using a XML format with the following format:

<CalibrationMeasurements>
 <CalibrationMeasurement>
  <Q>q1 q2  qn</Q>
  <Transform3D>t11 t12  t34</Transform3D>
  <DeviceName>NAME_OF_DEVICE</DeviceName>
  <MovingFrame>NAME_OF_MOVING_FRAME</MovingFrame>
  <StaticFrame>NAME_OF_STATIC_FRAME</StaticFrame>
  <CovarianceMatrix>rows cols c11 c12 c66</CovarianceMatrix>
 </CalibrationMeasurement>

</CalibrationMeasurements>

Writing and reading the format can be done using XMLCalibrationMeasurementFile 


USAGE:
To see the options call
rw_calibration-tool.exe help

Typical use:
rw_calibration-tool.exe -workCellFile Marvin.model.calibration.wc.xml --measurementFile CalibMeasurements.xml --calibrationFile Calib.xml


