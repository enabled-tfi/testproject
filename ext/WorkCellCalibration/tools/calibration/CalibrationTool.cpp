﻿#include "MultivariateNormalDistribution.hpp"
#include <WorkCellCalibration.hpp>
#include <CalibrationMeasurement.hpp>
#include <CalibrationUtils.hpp>
#include <WorkCellExtrinsicCalibrator.hpp>
#include <WorkCellCalibrator.hpp>
#include <WorkCellJacobian.hpp>
#include <xml/XMLCalibrationMeasurementFile.hpp>
#include <xml/XmlCalibrationSaver.hpp>
#include <xml/XmlCalibrationLoader.hpp>

#include <rw/common.hpp>
#include <rw/loaders.hpp>
#include <rw/math/Statistics.hpp>
//#include <rwlibs/calibration.hpp>
#include <iomanip>

#include <boost/program_options.hpp>
#include <iostream>
#include <fstream>


using namespace rw::math;
using namespace rw::kinematics;
using namespace rw::models;
using namespace sdurobotics::calibration;

class CalibrationOptionParser { 
public:
	CalibrationOptionParser() : _optionsDescription("Options") {
		_optionsDescription.add_options()
			("help", boost::program_options::value<bool>(&_isHelpPrintingEnabled)->default_value(false)->zero_tokens(), "Print help messages")
			("details", boost::program_options::value<bool>(&_isDetailPrintingEnabled)->default_value(false)->zero_tokens(), "Print details")
			("workCellFile", boost::program_options::value<std::string>(&_workCellFilePath)->required(), "Set the work cell file path")
			("measurementFile", boost::program_options::value<std::string>(&_measurementFilePath)->required(), "Set the measurement file path")
			("calibrationFile", boost::program_options::value<std::string>(&_calibrationFilePath), "Set the calibration file path")
			("weighting", boost::program_options::value<bool>(&_isWeightingMeasurements)->default_value(true), "Enable/disable weighting of measurements")
			("baseCalibration", boost::program_options::value<bool>(&_isBaseCalibrationEnabled)->default_value(true), "Enable/disable calibration of base transformation")
			("endCalibration", boost::program_options::value<bool>(&_isEndCalibrationEnabled)->default_value(true), "Disable calibration of end transformation")
			("linkCalibration", boost::program_options::value<bool>(&_isLinkCalibrationEnabled)->default_value(true), "Enable/disable calibration of link transformations")
			("jointCalibration", boost::program_options::value<bool>(&_isJointCalibrationEnabled)->default_value(true), "Enable/disable calibration of joint transformations")
			("useGlobalExtrinsicCalibration", boost::program_options::value<bool>(&_isGlobalExtrinsicsEnabled)->default_value(true), "Enable/disable whether to use the global extrinsic parameter calibration")
            ("useIterativeCalibration", boost::program_options::value<bool>(&_isIterativeCalibrationEnabled)->default_value(false), "Select whether to use the iterative calibration (both extrinsic and intrinsic parameters)")
			("validationMeasurementPercentage", boost::program_options::value<double>(&_validationMeasurementPercentage)->default_value(0.2), "Percentage of measurements to reserve for validation");
	}
	
	void parseArguments(int argumentCount, char** argumentArray) {
		boost::program_options::positional_options_description positionalOptionsDescription;
		positionalOptionsDescription.add("workCellFile", 1).add("measurementFile", 1).add("calibrationFile", 1);

		try {
			boost::program_options::variables_map variablesMap;
			boost::program_options::command_line_parser commandLineParser(argumentCount, argumentArray);
			boost::program_options::basic_parsed_options<char> parsedOptions = commandLineParser.options(_optionsDescription).positional(positionalOptionsDescription).run();
			boost::program_options::store(parsedOptions, variablesMap);
			boost::program_options::notify(variablesMap);
		} catch (boost::program_options::error& error) {
			RW_THROW(error.what());
		}
	}

	bool isHelpPrintingEnabled() const {
		return _isHelpPrintingEnabled;
	}

	bool isDetailPrintingEnabled() const {
		return _isDetailPrintingEnabled;
	}

	std::string getWorkCellFilePath() const {
		return _workCellFilePath;
	}

	std::string getMeasurementFilePath() const {
		return _measurementFilePath;
	}

	std::string getCalibrationFilePath() const {
		return _calibrationFilePath;
	}

	std::string getMathematicaOutputFileName() const {
		return _mathematicaOutputFileName;
	}

	bool isWeightingMeasurements() const {
		return _isWeightingMeasurements;
	}

	bool isBaseCalibrationEnabled() const {
		return _isBaseCalibrationEnabled;
	}

	bool isEndCalibrationEnabled() const {
		return _isEndCalibrationEnabled;
	}

	bool isLinkCalibrationEnabled() const {
		return _isLinkCalibrationEnabled;
	}

	bool isJointCalibrationEnabled() const {
		return _isJointCalibrationEnabled;
	}

	bool isGlobalExtrinsicsEnabled() const {
		return _isGlobalExtrinsicsEnabled;	
	}

    bool isIterativeCalibrationEnabled() const {
        return _isIterativeCalibrationEnabled;
    }

	double getValidationMeasurementPercentage() const {
		return _validationMeasurementPercentage;
	}


	friend std::ostream& operator<<(std::ostream& out, CalibrationOptionParser& parser) {
		out << "Usage:" << std::endl;
		out << "  rw_calibration-tool [work cell file] [measurement file] [calibration file]" << std::endl;
		out << std::endl;
		out << parser._optionsDescription;
		return out;
	}

private:
	boost::program_options::options_description _optionsDescription;
	bool _isHelpPrintingEnabled;
	bool _isDetailPrintingEnabled;
	std::string _workCellFilePath;
	std::string _measurementFilePath;
	std::string _calibrationFilePath;
	bool _isWeightingMeasurements;
	bool _isBaseCalibrationEnabled;
	bool _isEndCalibrationEnabled;
	bool _isLinkCalibrationEnabled;
	bool _isJointCalibrationEnabled;
	bool _isGlobalExtrinsicsEnabled;
    bool _isIterativeCalibrationEnabled;
	double _validationMeasurementPercentage;	
	std::string _mathematicaOutputFileName;
};

std::ostream& operator<<(std::ostream& out, const NLLSSolver::Ptr solver);
std::ostream& operator<<(std::ostream& out, const WorkCellCalibration::Ptr calibration);


int main(int argumentCount, char** argumentArray) {

	std::cout << "Parsing arguments.. "<<std::endl;
	CalibrationOptionParser optionParser;
	try {
		optionParser.parseArguments(argumentCount, argumentArray);
		std::cout << "Parsed." << std::endl;
	} catch(rw::common::Exception& exception) {
		std::cout << "FAILED: " << exception.getMessage() << std::endl;
		std::cout << optionParser << std::endl;
		return -1;
	}

	if (optionParser.isHelpPrintingEnabled()) {
		std::cout << optionParser << std::endl;
		return 0;
	}


	// Load workcell.
	std::string workCellFilePath = optionParser.getWorkCellFilePath();
	std::cout << "Loading work cell [ " << workCellFilePath << " ].. "<<std::endl;
	rw::models::WorkCell::Ptr workCell = rw::loaders::XMLRWLoader::load(workCellFilePath);
	const rw::kinematics::State workCellState = workCell->getDefaultState();
	if (workCell.isNull()) {
		std::cout << "FAILED." << std::endl;
		return -1;
	}
	std::cout << "Loaded [ " << workCell->getName() << " ]." << std::endl;


	// Load robot pose measurements from file.
	std::string measurementFilePath = optionParser.getMeasurementFilePath();
	std::cout << "Loading measurements [ " << measurementFilePath << " ].. "<<std::endl;	
	std::vector<CalibrationMeasurement::Ptr> measurements = XMLCalibrationMeasurementFile<XMLDetectionInfoBaseSerializer>::load(measurementFilePath);
	std::cout<<"Loaded [ "<<measurementFilePath<<" ]."<<std::endl;


	for (CalibrationMeasurement::Ptr measurement : measurements)
	{
		Transform3D<> transform = inverse(measurement->staticTmoving());
		std::string movingFrame = measurement->getMovingFrameName();
		std::string staticFrame = measurement->getStaticFrameName();
		measurement->setMovingFrameName(staticFrame);
		measurement->setStaticFrameName(movingFrame);
		measurement->setStaticTmoving(transform);
	}


    //Run through the measurements to find the device and marker pairs and the static frame names
	typedef std::pair<std::string, std::string> StringPair;
	std::set<StringPair> deviceAndFrameNames;
	std::set<std::string> staticFrameNames;
	int cnt = 0;
	std::map<std::string, Statistics<double> > uncertainties;
    std::map<std::string, Statistics<double> > staticToStats;
	for (std::vector<CalibrationMeasurement::Ptr>::iterator it = measurements.begin(); it != measurements.end(); ++it) {
		CalibrationMeasurement::Ptr measurement = (*it);
        staticToStats[measurement->getStaticFrameName()].add(measurement->staticTmoving().P().norm2());
		if (workCell->findDevice(measurement->getDeviceName()) == NULL) {
			std::cout<<"Unable to find device named: "<<measurement->getDeviceName()<<std::endl;
			return -1;
		}
		if (workCell->findFrame(measurement->getMovingFrameName()) == NULL) {
			std::cout<<"Unable to find frame named: "<<measurement->getMovingFrameName()<<std::endl;
			return -1;
		}
		if (workCell->findFrame(measurement->getStaticFrameName()) == NULL) {
			std::cout<<"Unable to find frame named: "<<measurement->getStaticFrameName()<<std::endl;
			return -1;
		}

		StringPair devAndFrame(measurement->getDeviceName(), measurement->getMovingFrameName());		
		deviceAndFrameNames.insert(devAndFrame);
		staticFrameNames.insert(measurement->getStaticFrameName());


		if (measurement->hasCovarianceMatrix() == false) {
            Eigen::MatrixXd cov = Eigen::MatrixXd::Zero(6,6);	
		    cov(0,0) = 1e-10;
		    cov(1,1) = 1e-10;
		    cov(2,2) = 1e-10;
		    cov(3,3) = 1e-8;
		    cov(4,4) = 1e-8;
		    cov(5,5) = 1e-8;
			measurement->setCovarianceMatrix(cov);
        }
		else {
			const Eigen::Matrix<double, 6, 6> weightMatrix = Eigen::SelfAdjointEigenSolver<Eigen::Matrix<double, 6, 6> >(measurement->getCovarianceMatrix()).operatorInverseSqrt();
			if (boost::math::isnan(weightMatrix(0,0))) {
				std::cout<<"Problem with weight matrix ["<<cnt<<"] = "<<weightMatrix<<std::endl;
				std::cout<<"Do you wish to remove the measurement? (Y/N): ";
				char ch[3]; std::cin.getline(ch, 3);
				if (ch[0] == 'y' || ch[0] == 'Y') {
					std::cout<<"Removing measurement..."<<std::endl;
					it = measurements.erase(it);
				}
			}
            std::string id = measurement->getStaticFrameName();
			uncertainties[id].add(measurement->getCovarianceMatrix().eigenvalues()(0).real());
		}
		cnt++;
	}

	typedef std::pair<SerialDevice::Ptr, Frame*> SerialDeviceFramePair;
	std::vector<SerialDeviceFramePair> devicesAndFrames;
	for (std::set<StringPair>::iterator it = deviceAndFrameNames.begin(); it != deviceAndFrameNames.end(); ++it) {
		std::cout<<"Device "<<(*it).first<<" has moving frame "<<(*it).second<<std::endl;		
		SerialDevice::Ptr dev = workCell->findDevice<SerialDevice>((*it).first);
		Frame* frame = workCell->findFrame((*it).second);
		devicesAndFrames.push_back(SerialDeviceFramePair(dev, frame));
	}

	std::vector<rw::kinematics::Frame*> staticFrames;
	for (std::set<std::string>::iterator it = staticFrameNames.begin(); it != staticFrameNames.end(); ++it) {
		staticFrames.push_back(workCell->findFrame(*it));
	}

	//Find the measurements to be used for calibration and validation 
	const size_t measurementCount = measurements.size();
	size_t calibrationMeasurementCount = (int)std::floor((double) measurementCount * (1.0 - optionParser.getValidationMeasurementPercentage()));		
	size_t validationMeasurementCount = measurementCount - calibrationMeasurementCount;
	std::vector<CalibrationMeasurement::Ptr> calibrationMeasurements;
	std::vector<CalibrationMeasurement::Ptr> validationMeasurements;

	std::set<int> indices;
	Math::seed(10);
	while (indices.size() < validationMeasurementCount) {
		indices.insert(Math::ranI(0, measurementCount));	
	}

	for (size_t i = 0; i<measurementCount; i++) {
		if (indices.find(i) == indices.end()) {
			calibrationMeasurements.push_back(measurements[i]);
		} else {
			validationMeasurements.push_back(measurements[i]);
		}
	}
	
	std::cout<<"Total Measurements = "<<measurements.size()<<std::endl;
	std::cout<<"Calibration Measurements = "<<calibrationMeasurements.size()<<std::endl;
	std::cout<<"Validation Measurements = "<<validationMeasurements.size()<<std::endl;



	std::cout<<"Summary without extrinsic calibration"<<std::endl;
	std::cout<<"Calibration Data"<<std::endl;	
	CalibrationUtils::printMeasurementSummary(calibrationMeasurements, workCell, workCellState, std::cout);
	std::cout<<"Validation Data"<<std::endl;
	CalibrationUtils::printMeasurementSummary(validationMeasurements, workCell, workCellState, std::cout);	
	//std::cout<<"All data"<<std::endl;
	//CalibrationUtils::printMeasurementSummary(measurements, workCell, workCellState, std::cout);

	std::cout << "Press enter to continue..." << std::endl;
	char buffer[2];
	std::cin.getline(buffer, 1);

	//Run the extrinsic calibration
	WorkCellCalibration::Ptr exCalibration = rw::common::ownedPtr(new WorkCellCalibration(devicesAndFrames, staticFrames));
    if (optionParser.isGlobalExtrinsicsEnabled()) {
		std::cout<<"Precalibrating Extrinsics...";
		std::wcout.flush();
		WorkCellExtrinsicCalibrator extrinsicCalibrator(workCell, &rw::common::Log::debugLog());
		extrinsicCalibrator.setMeasurements(calibrationMeasurements);
        extrinsicCalibrator.setUseRotation(true);
		extrinsicCalibrator.setUsePosition(true);
		extrinsicCalibrator.calibrate(exCalibration, workCell->getDefaultState());
		std::cout<<"Extrinsics precalibrated"<<std::endl;
		exCalibration->apply();

    	std::cout<<"Summary after the global extrinsic calibration"<<std::endl;
	    std::cout << "Residual summary (calibration):" << std::endl;
	    CalibrationUtils::printMeasurementSummary(calibrationMeasurements, workCell, workCellState, std::cout);
	    std::cout << "Residual summary (validation):" << std::endl;
	    CalibrationUtils::printMeasurementSummary(validationMeasurements, workCell, workCellState, std::cout);
    }
	
    WorkCellCalibration::Ptr workcellCalibration = rw::common::ownedPtr(new WorkCellCalibration(devicesAndFrames, staticFrames));

    if (true || optionParser.isIterativeCalibrationEnabled() == true) {
	    // Initialize calibration, jacobian and calibrator.
	    std::cout << "Preparing iterative calibration..."<<std::endl;	
    	WorkCellJacobian::Ptr workcellJacobian= rw::common::ownedPtr(new WorkCellJacobian(workcellCalibration));
    	bool isWeightingMeasurements = optionParser.isWeightingMeasurements();
    	WorkCellCalibrator::Ptr workcellCalibrator = rw::common::ownedPtr(new WorkCellCalibrator(workCell, workcellCalibration, workcellJacobian));
	    workcellCalibrator->setMeasurements(calibrationMeasurements);
	    workcellCalibrator->setUseWeightedMeasurements(true || isWeightingMeasurements);
	    std::cout<<"Iterative calibration method prepared."<<std::endl;
	    try {
		    // Run calibrator.
		    bool isBaseCalibrationEnabled = optionParser.isBaseCalibrationEnabled();
		    bool isEndCalibrationEnabled = optionParser.isEndCalibrationEnabled();
		    bool isLinkCalibrationEnabled = optionParser.isLinkCalibrationEnabled();
		    bool isJointCalibrationEnabled = optionParser.isJointCalibrationEnabled();
		    std::cout << "Calibrating [ Base: " << (isBaseCalibrationEnabled ? "Enabled" : "Disabled") << " - End: " << (isEndCalibrationEnabled ? "Enabled" : "Disabled") << " - Link: " << (isLinkCalibrationEnabled ? "Enabled" : "Disabled") << " - Joint: " << (isJointCalibrationEnabled ? "Enabled" : "Disabled") << " ].. "<<std::endl;

            workcellCalibration->getFixedFrameCalibrations()->getCalibration(0)->setEnabled(isBaseCalibrationEnabled);
		    workcellCalibration->getFixedFrameCalibrations()->getCalibration(1)->setEnabled(isEndCalibrationEnabled);
		    workcellCalibration->getCompositeLinkCalibration()->setEnabled(isLinkCalibrationEnabled);
		    workcellCalibration->getCompositeJointEncoderCalibration()->setEnabled(isJointCalibrationEnabled);
		
		    workcellCalibrator->calibrate(workCellState);
		    const int iterationCount = workcellCalibrator->getSolver()->getIterationCount();
		    std::cout << "Calibrated [ Iteration count: " << iterationCount << " ]." << std::endl;
		    if (optionParser.isDetailPrintingEnabled()) {
			    std::cout << "Solver summary:" << std::endl;
			    std::cout << workcellCalibrator->getSolver() << std::endl;
		    }

		    //In case the extrinsics are precalibrated they are reverted here
		    exCalibration->revert();

		    //In case the extrinsics are precalibrated they are prepended to the workcell calibration here.
		    workcellCalibration->prependCalibration(exCalibration);

	    } catch (rw::common::Exception& exception) {
		    std::cout << "FAILED: " << exception.getMessage() << std::endl;

		    std::cout << "Solver log:" << std::endl;
		    std::cout << workcellCalibrator->getSolver() << std::endl;
	    }

	    workcellCalibration->apply();
	    std::cout<<"Summary after the iterative calibration"<<std::endl;
	    std::cout << "Residual summary (calibration):" << std::endl;
	    CalibrationUtils::printMeasurementSummary(calibrationMeasurements, workCell, workCellState, std::cout);
	    std::cout << "Residual summary (validation):" << std::endl;
	    CalibrationUtils::printMeasurementSummary(validationMeasurements, workCell, workCellState, std::cout);
    } else {
        workcellCalibration = exCalibration;
    }

   	// Save calibration.
	std::string calibrationFilePath = optionParser.getCalibrationFilePath();
	if (!calibrationFilePath.empty()) {
		std::cout << "Saving calibration [" << calibrationFilePath << "].. ";
		std::cout.flush();			
		XmlCalibrationSaver::save(workcellCalibration, calibrationFilePath);
		std::cout << "Saved." << std::endl;
	}
    return 0;
}

std::ostream& operator<<(std::ostream& out, const NLLSIterationLog& calibration);
std::ostream& operator<<(std::ostream& out, const NLLSSolver::Ptr solver) {
	std::vector<NLLSIterationLog> iterationLogs = solver->getIterationLogs();
	for (std::vector<NLLSIterationLog>::const_iterator it = iterationLogs.begin(); it != iterationLogs.end(); it++) {
		NLLSIterationLog iterationLog = (*it);
		std::cout << iterationLog;
		if (it + 1 != iterationLogs.end())
			std::cout << std::endl;
	}
	return out;
}

std::ostream& operator<<(std::ostream& out, const NLLSIterationLog& iterationLog) {
	out << "\tIteration " << iterationLog.getIterationNumber() << ": Jacobian [ Singular: " << (iterationLog.isSingular() ? "Yes" : "No")
			<< ", Condition: " << iterationLog.getConditionNumber() << " ] ||Residuals||: " << iterationLog.getResidualNorm() << " ||Step||: "
			<< iterationLog.getStepNorm();
	return out;
}

std::ostream& operator<<(std::ostream& out, const FixedFrameCalibration::Ptr calibration);
//std::ostream& operator<<(std::ostream& out, const DHLinkCalibration::Ptr calibration);
std::ostream& operator<<(std::ostream& out, const JointEncoderCalibration::Ptr calibration);
std::ostream& operator<<(std::ostream& out, const WorkCellCalibration::Ptr calibration) {
	bool hasPrevious = false;

	CompositeCalibration<FixedFrameCalibration>::Ptr ffCalibrations = calibration->getFixedFrameCalibrations();
	for (int i = 0; i<ffCalibrations->getCalibrationCount(); i++) {
		if (ffCalibrations->getCalibration(i)->isEnabled()) {
			if (hasPrevious)
				out<<std::endl;
			out<<"\tFixed frame calibration: "<<ffCalibrations->getCalibration(i);
			hasPrevious = true;
		}
	}


	CompositeCalibration<ParallelAxisDHCalibration>::Ptr compositeLinkCalibration = calibration->getCompositeLinkCalibration();
	if (compositeLinkCalibration->isEnabled()) {
		for (int calibrationIndex = 0; calibrationIndex < compositeLinkCalibration->getCalibrationCount(); calibrationIndex++) {
			ParallelAxisDHCalibration::Ptr linkCalibration = compositeLinkCalibration->getCalibration(calibrationIndex);
			if (linkCalibration->isEnabled()) {
				if (hasPrevious)
					out << std::endl;
				out << "\tLink calibration: " << linkCalibration;
				hasPrevious = true;
			}
		}
	}

	CompositeCalibration<JointEncoderCalibration>::Ptr compositeJointCalibration = calibration->getCompositeJointEncoderCalibration();
	if (compositeJointCalibration->isEnabled()) {
		for (int calibrationIndex = 0; calibrationIndex < compositeJointCalibration->getCalibrationCount(); calibrationIndex++) {
			JointEncoderCalibration::Ptr jointCalibration = compositeJointCalibration->getCalibration(calibrationIndex);
			if (jointCalibration->isEnabled()) {
				if (hasPrevious)
					out << std::endl;
				out << "\tJoint calibration: " << jointCalibration;
				hasPrevious = true;
			}
		}
	}

	return out;
}

std::ostream& operator<<(std::ostream& out, const FixedFrameCalibration::Ptr calibration) {
	out << "Frame [ " << calibration->getFrame()->getName() << " ]";
	
	const rw::math::Transform3D<> correctionTransform = calibration->getCorrectionTransform();
	out << " Summary [";
	out << " Translation: " << correctionTransform.P().norm2() * 1000.0 << " mm";
	out << " Rotation: " << rw::math::EAA<>(correctionTransform.R()).angle() * rw::math::Rad2Deg << " \u00B0";
	out << " ]";
	
	out << " Type [ RPY ]";

	const CalibrationParameterSet parameterSet = calibration->getParameterSet();
	out << " Parameters [";
	if (parameterSet(FixedFrameCalibration::PARAMETER_X).isEnabled()) {
		out << " x: " << parameterSet(FixedFrameCalibration::PARAMETER_X) * 1000.0;
		if (parameterSet(FixedFrameCalibration::PARAMETER_X).hasVariance())
			out << " (sd: " << parameterSet(FixedFrameCalibration::PARAMETER_X).getStandardDeviation() * 1000.0 << ")";
		out << " mm";
	}
	if (parameterSet(FixedFrameCalibration::PARAMETER_Y).isEnabled()) {
		out << " y: " << parameterSet(FixedFrameCalibration::PARAMETER_Y) * 1000.0;
		if (parameterSet(FixedFrameCalibration::PARAMETER_Y).hasVariance())
			out << " (sd: " << parameterSet(FixedFrameCalibration::PARAMETER_Y).getStandardDeviation() * 1000.0 << ")";
		out << " mm";
	}
	if (parameterSet(FixedFrameCalibration::PARAMETER_Z).isEnabled()) {
		out << " z: " << parameterSet(FixedFrameCalibration::PARAMETER_Z) * 1000.0;
		if (parameterSet(FixedFrameCalibration::PARAMETER_Z).hasVariance())
			out << " (sd: " << parameterSet(FixedFrameCalibration::PARAMETER_Z).getStandardDeviation() * 1000.0 << ")";
		out << " mm";
	}
	if (parameterSet(FixedFrameCalibration::PARAMETER_ROLL).isEnabled()) {
		out << " roll: " << parameterSet(FixedFrameCalibration::PARAMETER_ROLL) * rw::math::Rad2Deg;
		if (parameterSet(FixedFrameCalibration::PARAMETER_ROLL).hasVariance())
			out << " (sd: " << parameterSet(FixedFrameCalibration::PARAMETER_ROLL).getStandardDeviation() * rw::math::Rad2Deg << ")";
		out << " \u00B0";
	}
	if (parameterSet(FixedFrameCalibration::PARAMETER_PITCH).isEnabled()) {
		out << " pitch: " << parameterSet(FixedFrameCalibration::PARAMETER_PITCH) * rw::math::Rad2Deg;
		if (parameterSet(FixedFrameCalibration::PARAMETER_PITCH).hasVariance())
			out << " (sd: " << parameterSet(FixedFrameCalibration::PARAMETER_PITCH).getStandardDeviation() * rw::math::Rad2Deg << ")";
		out << " \u00B0";
	}
	if (parameterSet(FixedFrameCalibration::PARAMETER_YAW).isEnabled()) {
		out << " yaw: " << parameterSet(FixedFrameCalibration::PARAMETER_YAW) * rw::math::Rad2Deg;
		if (parameterSet(FixedFrameCalibration::PARAMETER_YAW).hasVariance())
			out << " (sd: " << parameterSet(FixedFrameCalibration::PARAMETER_YAW).getStandardDeviation() * rw::math::Rad2Deg << ")";
		out << " \u00B0";
	}
	out << " ]";

	return out;
}
/*
std::ostream& operator<<(std::ostream& out, const DHLinkCalibration::Ptr calibration) {
	out << "Joint [ " << calibration->getJoint()->getName() << " ]";
	
	out << " Type [ DH ]";

	out << " Parameters [";
	CalibrationParameterSet parameterSet = calibration->getParameterSet();
	if (parameterSet(DHLinkCalibration::PARAMETER_A).isEnabled()) {
		out << " a: " << parameterSet(DHLinkCalibration::PARAMETER_A) * 1000.0;
		if (parameterSet(DHLinkCalibration::PARAMETER_A).hasVariance())
			out << " (sd: " << parameterSet(DHLinkCalibration::PARAMETER_A).getStandardDeviation() * 1000.0 << ")";
		out << " mm";
	}
	if (parameterSet(DHLinkCalibration::PARAMETER_B).isEnabled()) {
		out << " b: " << parameterSet(DHLinkCalibration::PARAMETER_B) * 1000.0;
		if (parameterSet(DHLinkCalibration::PARAMETER_B).hasVariance())
			out << " (sd: " << parameterSet(DHLinkCalibration::PARAMETER_B).getStandardDeviation() * 1000.0 << ")";
		out << " mm";
	}
	if (parameterSet(DHLinkCalibration::PARAMETER_D).isEnabled()) {
		out << " d: " << parameterSet(DHLinkCalibration::PARAMETER_D) * 1000.0;
		if (parameterSet(DHLinkCalibration::PARAMETER_D).hasVariance())
			out << " (sd: " << parameterSet(DHLinkCalibration::PARAMETER_D).getStandardDeviation() * 1000.0 << ")";
		out << " mm";
	}
	if (parameterSet(DHLinkCalibration::PARAMETER_ALPHA).isEnabled()) {
		out << " alpha: "	<< parameterSet(DHLinkCalibration::PARAMETER_ALPHA) * rw::math::Rad2Deg;
		if (parameterSet(DHLinkCalibration::PARAMETER_ALPHA).hasVariance())
			out << " (sd: " << parameterSet(DHLinkCalibration::PARAMETER_ALPHA).getStandardDeviation() * rw::math::Rad2Deg << ")";
		out << " \u00B0";
	}
	if (parameterSet(DHLinkCalibration::PARAMETER_BETA).isEnabled()) {
		out << " beta: " << parameterSet(DHLinkCalibration::PARAMETER_BETA) * rw::math::Rad2Deg;
		if (parameterSet(DHLinkCalibration::PARAMETER_BETA).hasVariance())
			out << " (sd: " << parameterSet(DHLinkCalibration::PARAMETER_BETA).getStandardDeviation() * rw::math::Rad2Deg << ")";
		out << " \u00B0";
	}
	if (parameterSet(DHLinkCalibration::PARAMETER_THETA).isEnabled()) {
		out << " theta: " << parameterSet(DHLinkCalibration::PARAMETER_THETA) * rw::math::Rad2Deg;
		if (parameterSet(DHLinkCalibration::PARAMETER_THETA).hasVariance())
			out << " (sd: " << parameterSet(DHLinkCalibration::PARAMETER_THETA).getStandardDeviation() * rw::math::Rad2Deg << ")";
		out << " \u00B0";
	}
	out << " ]";

	return out;
}
*/
std::ostream& operator<<(std::ostream& out, const JointEncoderCalibration::Ptr calibration) {
	out << "Joint [ " << calibration->getJoint()->getName() << " ]";
	
	out << " Type [ Encoder ]";

	out << " Parameters [";
	CalibrationParameterSet parameterSet = calibration->getParameterSet();
	for (int parameterIndex = 0; parameterIndex < parameterSet.getCount(); parameterIndex++) {
		if (parameterSet(parameterIndex).isEnabled()) {
			out << " " << parameterSet(parameterIndex) * 1000.0;
			if (parameterSet(parameterIndex).hasVariance())
				out << " (sd: " << parameterSet(parameterIndex).getStandardDeviation() << ")";
		}
	}
	out << " ]";

	return out;
}


