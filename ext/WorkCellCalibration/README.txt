WORKCELL CALIBRATION 

The workcell calibration package contains functionality for extrinsic hand-eye calibration and calibration of the robot intrinsic parameters.

CONTENT:
src/ compiles into a library containing the core functionality.

tools/ compiles into a console application for running the calibration. See README file in the tools folder for a details explaination about the use.

test/ contains a test which using simulated data tests the iterative calibration method for finding both extrinsic and intrinsic parameters.



