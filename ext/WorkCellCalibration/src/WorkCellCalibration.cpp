/*
 * WorkCellCalibration.cpp
 *
 *  Created on: Jul 4, 2012
 *      Author: bing
 */

#include "WorkCellCalibration.hpp"

#include <rw/models.hpp>

#include <boost/foreach.hpp>



using namespace rw::math;
using namespace rw::common;
using namespace rw::models;
using namespace rw::kinematics;
using namespace sdurobotics::calibration;


WorkCellCalibration::WorkCellCalibration(std::vector<std::pair<SerialDevice::Ptr, Frame*> > deviceFramePairs,
										 const std::vector<Frame*>& staticFrames) 
										 :	_primaryDevice(NULL), _deviceFramePairs(deviceFramePairs)
{

	//The calibration of the moving frame. Typically the camera frame, but could also be other frames.
	_fixedFrameCalibrations = ownedPtr(new CompositeCalibration<FixedFrameCalibration>());
	BOOST_FOREACH(Frame* staticFrame, staticFrames) {
		if (dynamic_cast<FixedFrame*>(staticFrame) == NULL) {
			RW_THROW("The frame '"<< staticFrame->getName()<<"' is not a FixedFrame as required by the calibration.");
		}

		FixedFrameCalibration::Ptr staticFrameCalibration = rw::common::ownedPtr(new FixedFrameCalibration(rw::kinematics::Frame::Ptr(staticFrame).cast<rw::kinematics::FixedFrame>()));
		_fixedFrameCalibrations->addCalibration(staticFrameCalibration);
		_staticFrameCalibrations[staticFrame->getName()] = staticFrameCalibration;
	}
	

	_compositeLinkCalibration = rw::common::ownedPtr(new CompositeCalibration<ParallelAxisDHCalibration>());
	_compositeJointEncoderCalibration = rw::common::ownedPtr(new CompositeCalibration<JointEncoderCalibration>());


	BOOST_FOREACH(DeviceFramePair deviceFramePair, deviceFramePairs) {
		SerialDevice::Ptr device = deviceFramePair.first;
		if (_primaryDevice == NULL) {
			_primaryDevice = device;
		}
		if (_primaryDevice != device) {
			FixedFrameCalibration::Ptr secondaryBaseCalibration = rw::common::ownedPtr(new FixedFrameCalibration(rw::kinematics::Frame::Ptr(device->getBase()).cast<rw::kinematics::FixedFrame>()));
			_baseCalibrations[device->getName()] = secondaryBaseCalibration;
			_fixedFrameCalibrations->addCalibration(secondaryBaseCalibration);
		}
		FixedFrameCalibration::Ptr endCalibration = rw::common::ownedPtr(new FixedFrameCalibration(rw::kinematics::Frame::Ptr(deviceFramePair.second).cast<rw::kinematics::FixedFrame>()));
		_movingFrameCalibrations[deviceFramePair.first->getName()] = endCalibration;
		_fixedFrameCalibrations->addCalibration(endCalibration);
		
	//_endCalibration = rw::common::ownedPtr(new FixedFrameCalibration(device->getEnd(), false));
	

		std::vector<rw::models::Joint*> joints = deviceFramePair.first->getJoints();
		for (std::vector<rw::models::Joint*>::iterator jointIterator = joints.begin(); jointIterator != joints.end(); jointIterator++) {
			rw::models::Joint::Ptr joint = (*jointIterator);

			// Add link calibrations for intermediate links.
			if (jointIterator != joints.begin() ) 
			{
				ParallelAxisDHCalibration::Ptr linkCalibration = rw::common::ownedPtr(new ParallelAxisDHCalibration(joint));
				_compositeLinkCalibration->addCalibration(linkCalibration);

				// Disable d and theta for first link.
				if (jointIterator == ++(joints.begin())) {
					CalibrationParameterSet parameterSet = linkCalibration->getParameterSet();
					parameterSet(ParallelAxisDHCalibration::PARAMETER_D).setEnabled(false);
					parameterSet(ParallelAxisDHCalibration::PARAMETER_THETA).setEnabled(false);
					parameterSet(ParallelAxisDHCalibration::PARAMETER_A).setEnabled(true);
					parameterSet(ParallelAxisDHCalibration::PARAMETER_ALPHA).setEnabled(true);
					parameterSet(ParallelAxisDHCalibration::PARAMETER_B).setEnabled(false);
					parameterSet(ParallelAxisDHCalibration::PARAMETER_BETA).setEnabled(false);
				
					linkCalibration->setParameterSet(parameterSet);
					//break;
				}
			}

			// Add joint calibrations.
			JointEncoderCalibration::Ptr jointCalibration = rw::common::ownedPtr(new JointEncoderCalibration(deviceFramePair.first.cast<rw::models::JointDevice>(), joint));
			_compositeJointEncoderCalibration->addCalibration(jointCalibration);
		}
	}

	((CompositeCalibration<Calibration>*) this)->addCalibration(_fixedFrameCalibrations.cast<Calibration>());	
	((CompositeCalibration<Calibration>*) this)->addCalibration(_compositeLinkCalibration.cast<Calibration>());
	((CompositeCalibration<Calibration>*) this)->addCalibration(_compositeJointEncoderCalibration.cast<Calibration>());
}

		
WorkCellCalibration::WorkCellCalibration(CompositeCalibration<FixedFrameCalibration>::Ptr fixedFrameCalibrations,
		CompositeCalibration<ParallelAxisDHCalibration>::Ptr compositeLinkCalibration,
		CompositeCalibration<JointEncoderCalibration>::Ptr compositeJointCalibration) :
			_fixedFrameCalibrations(fixedFrameCalibrations),
			_compositeLinkCalibration(compositeLinkCalibration),
			_compositeJointEncoderCalibration(compositeJointCalibration)
{
	((CompositeCalibration<Calibration>*) this)->addCalibration(_fixedFrameCalibrations.cast<Calibration>());
	((CompositeCalibration<Calibration>*) this)->addCalibration(_compositeLinkCalibration.cast<Calibration>());
	((CompositeCalibration<Calibration>*) this)->addCalibration(_compositeJointEncoderCalibration.cast<Calibration>());
}

WorkCellCalibration::~WorkCellCalibration() {

}

FixedFrameCalibration::Ptr WorkCellCalibration::getFixedFrameCalibrationForStaticFrame(const std::string& name) {
	if (_staticFrameCalibrations.find(name) != _staticFrameCalibrations.end()) {
		return _staticFrameCalibrations[name];
	}
	RW_THROW("Unable to find a calibration for the static frame with name: '"<<name<<"'");
}

FixedFrameCalibration::Ptr WorkCellCalibration::getFixedFrameCalibrationForMovingFrame(const std::string& name) {
	if (_movingFrameCalibrations.find(name) != _movingFrameCalibrations.end()) {
		return _movingFrameCalibrations[name];
	}
	RW_THROW("Unable to find a calibration for the frame with name: '"<<name<<"'");

}

FixedFrameCalibration::Ptr WorkCellCalibration::getFixedFrameCalibrationForDevice(const std::string& deviceName) 
{
	if (_baseCalibrations.find(deviceName) != _baseCalibrations.end()) {
		return _baseCalibrations[deviceName];
	}
	if (deviceName == _primaryDevice->getName()) {
		RW_THROW("The device requested is the primary device in the work cell which do not have a base calibration");
	}
	RW_THROW("Unable to find a calibration for the device with name: '"<<deviceName<<"'");
}


CompositeCalibration<FixedFrameCalibration>::Ptr WorkCellCalibration::getFixedFrameCalibrations() const {
	return _fixedFrameCalibrations;
}

CompositeCalibration<ParallelAxisDHCalibration>::Ptr WorkCellCalibration::getCompositeLinkCalibration() const {
	return _compositeLinkCalibration;
}
 
CompositeCalibration<JointEncoderCalibration>::Ptr WorkCellCalibration::getCompositeJointEncoderCalibration() const {
	return _compositeJointEncoderCalibration;
}

void WorkCellCalibration::prependCalibration(WorkCellCalibration::Ptr calibration) 
{
	typedef std::map<std::string, FixedFrameCalibration::Ptr> StringCalibMap;
	for (StringCalibMap::iterator it = _movingFrameCalibrations.begin(); it != _movingFrameCalibrations.end(); ++it) {
		FixedFrameCalibration::Ptr ffc = calibration->getFixedFrameCalibrationForMovingFrame((*it).first);
		Transform3D<> oldTransform = (*it).second->getCorrectionTransform();
		Transform3D<> preTransform = ffc->getCorrectionTransform();
		Transform3D<> newTransform = preTransform*oldTransform;
		(*it).second->setCorrectionTransform(newTransform);
	}
	
	for (StringCalibMap::iterator it = _baseCalibrations.begin(); it != _baseCalibrations.end(); ++it) {
		FixedFrameCalibration::Ptr ffc = calibration->getFixedFrameCalibrationForDevice((*it).first);
		Transform3D<> oldTransform = (*it).second->getCorrectionTransform();
		Transform3D<> preTransform = ffc->getCorrectionTransform();
		Transform3D<> newTransform = preTransform*oldTransform;
		(*it).second->setCorrectionTransform(newTransform);
	}


	for (StringCalibMap::iterator it = _staticFrameCalibrations.begin(); it != _staticFrameCalibrations.end(); ++it) {
		FixedFrameCalibration::Ptr ffc = calibration->getFixedFrameCalibrationForStaticFrame((*it).first);
		Transform3D<> oldTransform = (*it).second->getCorrectionTransform();
		Transform3D<> preTransform = ffc->getCorrectionTransform();
		Transform3D<> newTransform = preTransform*oldTransform;
		(*it).second->setCorrectionTransform(newTransform);
	}

}

