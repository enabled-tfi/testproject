/********************************************************************************
 * Copyright 2009 The Robotics Group, The Maersk Mc-Kinney Moller Institute, 
 * Faculty of Engineering, University of Southern Denmark 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ********************************************************************************/

#ifndef SDUR_CALIBRATION_CalibrationMeasurement_HPP
#define SDUR_CALIBRATION_CalibrationMeasurement_HPP

#include <rw/math.hpp>
#include <rw/models.hpp>

#include <Eigen/Core>
#include <Eigen/StdVector>

namespace sdurobotics {
namespace calibration {

/**
 * @brief Base class for the detection information
 */
class DetectionInfoBase {
public:
	typedef rw::common::Ptr<DetectionInfoBase> Ptr;
};

/**
 * @brief Represents a calibration measurement containing of robot configuration and a SE(3) transformation. 
 * 
 * The transformation is expected to be between a static frame and a frame being moved by the robot.
 *
 * Associated to a measurement should be the device name, the name of the static frame frame and the name of the moving frame.
 * Can optionally also store covariance matrix and detection information.
 */
	class CalibrationMeasurement {
public:
	/**
	 * @brief Smart pointer to a CalibrationMeasurement
	 */
	typedef rw::common::Ptr<CalibrationMeasurement> Ptr;	
	
	/**
	 * @brief Creates a calibration measurement without a covariance
	 *
	 * @param q [in] Robot configuration
	 * @param staticTmoving [in] Transform of moving frame relative to the static frame
	 * @param deviceName [in] Name of the device/robot
	 * @param staticFrameName [in] Name of the static frame
	 * @param movingFrameName [in] Name of the frame of the moving frame
         * @param id [in] unique ID for the given measurement
	 * @param rmsError [in] The RMS error computed from the residuals
	 */
	CalibrationMeasurement(const rw::math::Q& q, const rw::math::Transform3D<>& staticTmoving, const std::string& deviceName = "", const std::string& staticFrameName = "", const std::string& movingFrameName = "", const std::string& id ="", double rmsError = 0.0);


	CalibrationMeasurement(const rw::math::Transform3D<>& baseTmoving, const rw::math::Transform3D<>& staticTmoving, const std::string& deviceName = "", const std::string& staticFrameName = "", const std::string& movingFrameName = "", const std::string& id = "", double rmsError = 0.0);

	/**
	 * @brief Creates a calibration measurement with a covariance
	 *
	 * @param q [in] Robot configuration
	 * @param staticTmoving [in] Transform from static frame to moving frame
	 * @param covarianceMatrix [in] The covariance of the moving position seen in the static frame
	 * @param deviceName [in] Name of the device/robot
	 * @param staticFrameName [in] Name of the static frame
	 * @param movingFrameName [in] Name of the moving frame
         * @param id [in] unique ID for the given measurement
	 * @param rmsError [in] The RMS error computed from the residuals
	 */	
	CalibrationMeasurement(const rw::math::Q& q, const rw::math::Transform3D<>& staticTmoving, const Eigen::MatrixXd& covarianceMatrix, const std::string& deviceName = "", const std::string& staticFrameName = "", const std::string& movingFrameName = "", const std::string& id ="", double rmsError = 0.0);


	CalibrationMeasurement(const rw::math::Transform3D<>& baseTmoving, const rw::math::Transform3D<>& staticTmoving, const Eigen::MatrixXd& covarianceMatrix, const std::string& deviceName = "", const std::string& staticFrameName = "", const std::string& movingFrameName = "", const std::string& id = "", double rmsError = 0.0);

	/**
	 * @brief Destructor
	 */
	virtual ~CalibrationMeasurement();

	/**
	 * @brief Returns robot configuration of measurement
	 */
	const rw::math::Q& getQ() const;

	void setQ(const rw::math::Q& q);

	const rw::math::Transform3D<> baseTmoving() const;

	void setBaseTmoving(const rw::math::Transform3D<>& baseTmoving);

	/**
	 * @brief Returns transform if moving frame relative to the reference frame
	 */
	const rw::math::Transform3D<>& staticTmoving() const;

	/**
	 * @brief Sets the referenceTmoving transform
	 */
	void setStaticTmoving(const rw::math::Transform3D<>& staticTmoving);

	/**
	 * @brief Returns the covariance matrix associated to the measurement
	 */
	const Eigen::MatrixXd& getCovarianceMatrix() const;

	/**
	 * @brief Sets the covariance matrix for the measurement
	 * @param covar [in] The covariance to use for the measurement.
	 */
	void setCovarianceMatrix(const Eigen::MatrixXd& covar); 

	/**
	 * @brief Returns true is the covariance has been setted.
	 */
	bool hasCovarianceMatrix() const;

	/**
	 * @brief Returns the device name
	 */
	const std::string& getDeviceName() const;

	/**
	 * @brief Returns the name of the static frame
	 */
	const std::string& getStaticFrameName() const;

	/**
	 * @brief Returns the name of the moving frame
	 */
	const std::string& getMovingFrameName() const;

	/**
	 * @brief Returns ID for measurement
	 */
	const std::string& getID() const;

	/**
	 * @brief Returns RMS error for measurement
	 */
	const double getRMSError() const;

	/**
	 * @brief Sets the device name
	 */
	void setDeviceName(const std::string& deviceName);

	/**
	 * @brief Sets the name of the static frame
	 */
	void setStaticFrameName(const std::string& staticFrameName);

	/**
	 * @brief Sets the name of the moving frame
	 */
	void setMovingFrameName(const std::string& movingFrameName);

	/**
	 * @brief Sets the id for the measurement
	 */
	void setID(const std::string& id);

	/**
	 * @brief Sets the RMS error for the measurement
	 */
	void setRMSError(const double rmsError);

	/**
	 * @brief Sets the calibration error for the measurement
	 */
	void setCalibrationError(const double calibraitonError);

	/**
	 * @brief Returns the calibration error for the measurement
	 */
	double getCalibrationError();

	/**
	 * @brief Returns the detection info.
	 *
	 * If no detection info has been set, then the methods returns NULL
	 */
	DetectionInfoBase::Ptr getDetectionInfo() const;

	/**
	 * @brief Sets the detection info.
	 *
	 * Overwrites any previous set detection info.
	 */
	void setDetectionInfo(DetectionInfoBase::Ptr detectionInfo);


	bool hasQ() const;  

	bool hasBaseTmoving() const;


private:
	rw::math::Transform3D<> _staticTmoving;
	
	rw::math::Q _q;
	bool _hasQ;

	rw::math::Transform3D<> _baseTmoving;
	bool _hasBaseTmoving;

	Eigen::MatrixXd _covarianceMatrix;
	bool _hasCovarianceMatrix;
	std::string _deviceName;
	std::string _staticFrameName;
	std::string _movingFrameName;
        std::string _id;
	double _rmsError;
	double _calibraitonError;

	DetectionInfoBase::Ptr _detectionInfo;
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


}
}


#endif /* SDUR_CALIBRATION_CalibrationMeasurement_HPP */
