#include "WorkCellExtrinsicCalibrator.hpp"

#include "nlls/NLLSNewtonSolver.hpp"

#include <rw/math/EAA.hpp>

#include <Eigen/Eigenvalues>
#include <Eigen/Cholesky>

#include <boost/foreach.hpp>

#include <iomanip>


using namespace rw::math;
using namespace rw::models;
using namespace rw::kinematics;

using namespace sdurobotics::calibration;

WorkCellExtrinsicCalibrator::WorkCellExtrinsicCalibrator(rw::models::WorkCell::Ptr workcell, rw::common::LogWriter::Ptr writer) :
	_workcell(workcell),
	_writer(writer)
{
	_useRotation = true;
	_usePosition = true;
}

WorkCellExtrinsicCalibrator::~WorkCellExtrinsicCalibrator() {

}

rw::models::WorkCell::Ptr WorkCellExtrinsicCalibrator::getWorkCell() const {
	return _workcell;
}

void WorkCellExtrinsicCalibrator::setMeasurements(const std::vector<CalibrationMeasurement::Ptr>& measurements) {
	_measurements = measurements;



}

void WorkCellExtrinsicCalibrator::calibrate(WorkCellCalibration::Ptr workcellCalibration) {
	calibrate(workcellCalibration, _workcell->getDefaultState());
}

void WorkCellExtrinsicCalibrator::calibrate(WorkCellCalibration::Ptr workcellCalibration, const State& state) {
	_state = state;
	//Initially we need to sort the data according to device and frame
	typedef std::map<std::string, std::vector<CalibrationMeasurement::Ptr> > StringMeasurementMap;
	StringMeasurementMap sortedMeasurements;

	BOOST_FOREACH(CalibrationMeasurement::Ptr measurement, _measurements) {
		sortedMeasurements[measurement->getDeviceName()].push_back(measurement);
	}

	std::vector<Device2SensorResult> results;	
	for (StringMeasurementMap::iterator it = sortedMeasurements.begin(); it != sortedMeasurements.end(); ++it) {
		//(*_writer)<<"Calibrate "<<(*it).first<<" which has "<<(*it).second.size()<<" measurements "<<std::endl;
		calibrateForSingleDevice((*it).first, (*it).second, results);
	}

	//Extract the needed information from the results

	//Find the tool offset for each device
	std::map<std::string, Vector3D<> > positions;
	std::map<std::string, Vector3D<> > eaas;
	std::map<std::string, int> cnts;
	BOOST_FOREACH(Device2SensorResult res, results) {
		EAA<> eaa(res.tool2marker.R());
		Vector3D<> eaaVector(eaa(0), eaa(1), eaa(2));
		if (positions.find(res.device) != positions.end()) {
			positions[res.device] += res.tool2marker.P() * res.cnt;
			eaas[res.device] += eaaVector * res.cnt;
			cnts[res.device] += res.cnt;
		}
		else {
			positions[res.device] = res.tool2marker.P() * res.cnt;
			eaas[res.device] = eaaVector * res.cnt;
			cnts[res.device] = res.cnt;
		}
	}

	for (std::map<std::string, Vector3D<> >::iterator it = positions.begin(); it != positions.end(); ++it) {
		const std::string name = (*it).first;
		Vector3D<> pos = positions[name] /= (double)cnts[name];
		Vector3D<> eaavec = eaas[name] /= (double)cnts[name];
		(*_writer)<<"Marker "<<(*it).first<<" Correction Pos = "<<pos<<"  EAA="<<eaavec<<std::endl;
        workcellCalibration->getFixedFrameCalibrationForMovingFrame((*it).first)->setCorrectionTransform(Transform3D<>(pos, EAA<>(eaavec(0), eaavec(1), eaavec(2))));

        /*Transform3D<> originalTransform = workcellCalibration->getFixedFrameCalibrationForMovingFrame((*it).first)->getFrame()->getFixedTransform();
        Transform3D<> newTransform(pos, EAA<>(eaavec(0), eaavec(1), eaavec(2)));
        Transform3D<> correction = inverse(originalTransform)*newTransform;

        (*_writer) << "Marker " << (*it).first << " Original Transform = " << originalTransform << std::endl;
        (*_writer) << "Updated transform " << correction << std::endl;
            
		workcellCalibration->getFixedFrameCalibrationForMovingFrame((*it).first)->setCorrectionTransform(correction); 		*/
	}


	//Find the offset of the sensors
	std::string primaryDevice = workcellCalibration->getDeviceFramePairs().front().first->getName();
	//(*_writer)<<"Primary Device = "<<primaryDevice<<std::endl;
	std::map<std::string, Transform3D<> > primaryDevice2Sensor;
	std::map<std::pair<std::string, std::string>, Transform3D<> > secondaryDeviceTransforms;
	BOOST_FOREACH(Device2SensorResult res, results) {		
		if (res.device == primaryDevice) {
			Frame* baseFrame = _workcell->findDevice(res.device)->getBase();
			Frame* sensorFrame = _workcell->findFrame(res.sensor);
			(*_writer) << "Frames for frameTframe " << sensorFrame->getName() << "  " << baseFrame->getName() << std::endl;
			Transform3D<> Tsensor2base = Kinematics::frameTframe(sensorFrame, baseFrame, _state);
			Transform3D<> Tcorrection = Tsensor2base * inverse(res.sensor2base);
			workcellCalibration->getFixedFrameCalibrationForStaticFrame(res.sensor)->setCorrectionTransform(Tcorrection);
	        (*_writer)<<"Sensor "<<res.sensor<<" correction = "<<Tcorrection<<std::endl;
			Transform3D<> Tbase2sensor= Kinematics::frameTframe(baseFrame, sensorFrame, _state);
			primaryDevice2Sensor[res.sensor] = Tcorrection;
		} else {
			if (secondaryDeviceTransforms.find(std::make_pair(res.device, res.sensor)) != secondaryDeviceTransforms.end()) {
				RW_THROW("For some reason the device sensor pair "<<res.device<<" and "<<res.sensor<<" is represented several times in the result!");
			}
			secondaryDeviceTransforms[std::make_pair(res.device, res.sensor)] = res.sensor2base;
		}
	}

	//Runs through the device, sensor pairs and compute the corresponding corrections.
	std::map<std::string, std::vector<Transform3D<> > > deviceBaseCorrections;
	for (std::map<std::pair<std::string, std::string>, Transform3D<> >::iterator it = secondaryDeviceTransforms.begin(); it != secondaryDeviceTransforms.end(); ++it) {
		const std::string& deviceName = (*it).first.first;
		const std::string& sensorName = (*it).first.second;
		const Transform3D<>& sensor2base = (*it).second;
		
		Frame* baseFrame = _workcell->findDevice(deviceName)->getBase();
		Frame* sensorFrame = _workcell->findFrame(sensorName);
		Transform3D<> Tsensor2baseModel = Kinematics::frameTframe(sensorFrame, baseFrame, _state);
		Transform3D<> Tcorrection = inverse(Tsensor2baseModel)*primaryDevice2Sensor[sensorName] * sensor2base;
		//(*_writer)<<"Sensor Correction for Secondary Device = "<<Tcorrection<<std::endl;
		deviceBaseCorrections[deviceName].push_back(Tcorrection);
	}

	//Average the corrections
	for (std::map<std::string, std::vector<Transform3D<> > >::iterator it = deviceBaseCorrections.begin(); it != deviceBaseCorrections.end(); ++it) {
		const std::vector<Transform3D<> >& corrections = (*it).second;
		Vector3D<> eaas(0,0,0);
		Vector3D<> pos(0,0,0);
		BOOST_FOREACH(const Transform3D<>& correction, corrections) {
			EAA<> eaa(correction.R());
			eaas += eaa.axis() * eaa.angle();
			pos += correction.P();
		}
		eaas /= (*it).second.size();
		pos /= (*it).second.size();
		Transform3D<> Tcorrection(pos, EAA<>(eaas(0), eaas(1), eaas(2)));
	//	(*_writer)<<"Final Correction for Secondary Device = "<<Tcorrection<<std::endl;
		workcellCalibration->getFixedFrameCalibrationForDevice((*it).first)->setCorrectionTransform(Tcorrection);
	}

}

void WorkCellExtrinsicCalibrator::calibrateForSingleDevice(const std::string& deviceName, const std::vector<CalibrationMeasurement::Ptr>& measurements, std::vector<Device2SensorResult>& results) {
	typedef std::map<std::string, std::vector<CalibrationMeasurement::Ptr> > StringMeasurementMap;
	StringMeasurementMap sortedMeasurements;

	BOOST_FOREACH(CalibrationMeasurement::Ptr measurement, measurements) {
		sortedMeasurements[measurement->getStaticFrameName()].push_back(measurement);
	}
	(*_writer)<<"Measurement Count = "<<measurements.size()<<std::endl;
	int cnt = 0;
	for (StringMeasurementMap::iterator it = sortedMeasurements.begin(); it != sortedMeasurements.end(); ++it) {
		Device2SensorResult result;
		result.device = deviceName;
		result.sensor = (*it).first; //The sensor name
		//std::pair<Transform3D<>, Transform3D<> > res = 
		(*_writer)<<"Calibrate "<<deviceName<<" and "<<(*it).first<<" with "<<(*it).second.size()<<" measurements "<<std::endl;
		calibrateSingleDeviceAndSensor((*it).second, result);
		results.push_back(result);
		(*_writer)<<"Result = "<<result.device<<std::endl;
		(*_writer)<<"\tsensor = "<<result.sensor<<std::endl;
		(*_writer)<<"\tsensor2base = "<<result.sensor2base<<std::endl;
		(*_writer)<<"\ttool2marker = "<<result.tool2marker<<std::endl;
	}
}

Transform3D<> WorkCellExtrinsicCalibrator::getFK(const std::string& device, const std::string& markerFrame, const Q& q) {
	SerialDevice::Ptr dev = _workcell->findDevice<SerialDevice>(device);
	Frame* marker = _workcell->findFrame(markerFrame);
	State state = _state;
	dev->setQ(q, state);
	//(*_writer)<<"Q = "<<q<<std::endl;
	//return dev->baseTframe(dev->getJoints().back(), state);
	return dev->baseTframe(marker, state);
	//return dev->baseTend(state);	
}

Transform3D<> WorkCellExtrinsicCalibrator::getFK(CalibrationMeasurement::Ptr measurement) {
	if (measurement->hasBaseTmoving()) {
		return measurement->baseTmoving();		
	}
	else {
		return getFK(measurement->getDeviceName(), measurement->getMovingFrameName(), measurement->getQ());
	}
}

void WorkCellExtrinsicCalibrator::calibrateSingleDeviceAndSensor(const std::vector<CalibrationMeasurement::Ptr>& measurements, Device2SensorResult& result) 
{
	//Perform the HGP pre calibration 

	//Compute the initial estimate of the base transformation
	//1: Compute p_avg = 1/K Sum(p_k)
	const unsigned int K = measurements.size();
	Eigen::VectorXd p_avg = Eigen::VectorXd::Zero(3);
	BOOST_FOREACH(CalibrationMeasurement::Ptr measurement, measurements) {
		p_avg += measurement->staticTmoving().P().e();
	}
	p_avg /= K;

	//2: Compute p_k
	std::vector<Eigen::MatrixXd> p_k;
	BOOST_FOREACH(CalibrationMeasurement::Ptr measurement, measurements) {		
		p_k.push_back( (measurement->staticTmoving().P().e() - p_avg) );
	}

	//3, 4: Compute Ai,j, Bi,j, alphai,j, betai,j
	Transform3D<>** Aij = new Transform3D<>*[K];
	Transform3D<>** Bij = new Transform3D<>*[K];
	EAA<>** alphaij = new EAA<>*[K];
	EAA<>** betaij = new EAA<>*[K];
	for (size_t i = 0; i<K; i++) {
		Aij[i] = new Transform3D<>[K];
		Bij[i] = new Transform3D<>[K];
		alphaij[i] = new EAA<>[K];
		betaij[i] = new EAA<>[K];
		for (size_t j = 0; j<K; j++) {
			//if (j < 2) {
			//	(*_writer)<<j<<": Q = "<<measurements[j]->getQ()<<std::endl<<std::endl;
			//	(*_writer)<<j<<": FK = "<<getFK(measurements[j])<<std::endl<<std::endl;
			//	(*_writer)<<j<<": Measurement = "<<measurements[j]->getTransform()<<std::endl<<std::endl;
			//}

			Aij[i][j] = measurements[i]->staticTmoving() * inverse(measurements[j]->staticTmoving());
			//(*_writer)<<"Measumement["<<i<<"] = "<<measurements[i]->getTransform()<<std::endl;
			//(*_writer)<<"Measumement["<<j<<"] = "<<measurements[j]->getTransform()<<std::endl;
			Bij[i][j] = getFK(measurements[i]) * inverse(getFK(measurements[j]));
			alphaij[i][j] = EAA<>(Aij[i][j].R());
			betaij[i][j] = EAA<>(Bij[i][j].R());
			//if (i == 0 && j == 1) 
			//{
			//	(*_writer)<<"Aij["<<i<<","<<j<<"] = "<<Aij[i][j]<<std::endl;
			//	(*_writer)<<"Bij["<<i<<","<<j<<"] = "<<Bij[i][j].P()<<std::endl;
			//	(*_writer)<<"alphaij["<<i<<","<<j<<"] = "<<alphaij[i][j]<<std::endl;
			//	(*_writer)<<"betaij["<<i<<","<<j<<"] = "<<betaij[i][j]<<std::endl;
			//	char ch[4]; std::cin.getline(ch, 1);
			//}
				
		}
	}


	//7,8,9,10: Compute alpha_k, beta_k, Gamma_k and pi_k
	std::vector<Eigen::VectorXd> alpha_k; //Average of the EAA
	std::vector<Eigen::VectorXd> beta_k; //Average of the EAA angles
	std::vector<Eigen::MatrixXd> Gamma_k; //Average of the rotation matrices
	std::vector<Eigen::VectorXd> pi_k; //Average of the translation matrices
	for (size_t k = 0; k<K; k++) {
		Eigen::VectorXd alpha = Eigen::VectorXd::Zero(3);
		Eigen::VectorXd beta = Eigen::VectorXd::Zero(3);
		Eigen::MatrixXd Gamma = Eigen::MatrixXd::Zero(3,3);
		Eigen::VectorXd p = Eigen::VectorXd::Zero(3);
		for (size_t i = 0; i<K; i++) {
			alpha(0) += alphaij[i][k](0);
			alpha(1) += alphaij[i][k](1);
			alpha(2) += alphaij[i][k](2);

			beta(0) += betaij[i][k](0);
			beta(1) += betaij[i][k](1);
			beta(2) += betaij[i][k](2);

			Gamma += Bij[i][k].R().e();
			p += Bij[i][k].P().e();
		}
		alpha /= (double)K;
		alpha_k.push_back(alpha);
		beta /= (double)K;
		beta_k.push_back(beta);
//		(*_writer)<<"Gamma Org = "<<Gamma<<std::endl;
		Gamma /= (double)K;
		Gamma_k.push_back(Gamma);	
		//(*_writer)<<"Gamma = "<<Gamma<<std::endl;
		p /= (double)K;
		pi_k.push_back(p);
		//(*_writer)<<"pi_k = "<<pi_k.back()<<std::endl;
		//if (k == 1) {
		//	(*_writer)<<"alpha["<<k<<"] = "<<alpha.transpose()<<std::endl;
		//	(*_writer)<<"beta["<<k<<"] = "<<beta.transpose()<<std::endl;
		//	(*_writer)<<"Gamma["<<k<<"] = "<<Gamma<<std::endl;
		//	(*_writer)<<"p["<<k<<"] = "<<p.transpose()<<std::endl;
		//	char ch[2]; std::cin.getline(ch, 1);
		//}
		
	}
	//std::cin.getline(ch, 1);
	//BOOST_FOREACH(const Eigen::MatrixXd& ra, RA_k) {		
	//	Rotation3D<> R(ra);
	//	EAA<> eaa(R);
	//	Eigen::MatrixXd m(3,1);
	//	m(0,0) = eaa(0);
	//	m(1,0) = eaa(1);
	//	m(2,0) = eaa(2);

	//	alpha_k.push_back(m);
	//}

	////6: Compute beta_k
	//std::vector<Eigen::MatrixXd> beta_k;
	//BOOST_FOREACH(const Eigen::MatrixXd& rb, RB_k) {
	//	Rotation3D<> R(rb);
	//	EAA<> eaa(R);
	//	Eigen::MatrixXd m(3,1);
	//	m(0,0) = eaa(0);
	//	m(1,0) = eaa(1);
	//	m(2,0) = eaa(2);
	//	beta_k.push_back(m);
	//}

	//11: Define the matrix P
	Eigen::MatrixXd P(3, K);
	for (size_t i = 0; i<K; i++) {
		const Eigen::MatrixXd& p = p_k[i];
		P(0, i) = p(0,0);
		P(1, i) = p(1,0);
		P(2, i) = p(2,0);
	}
	//(*_writer)<<"P="<<P<<std::endl;
	
	//12: Compute U = P^T (P P^T)^-1 P
	Eigen::MatrixXd U = P.transpose() * (P * P.transpose()).inverse() * P;
	//(*_writer)<<"U = "<<U<<std::endl;
	//13, 14: Compute Z(k) and z(k)
	std::vector<Eigen::MatrixXd> Zk;
	std::vector<Eigen::MatrixXd> zk;
	for (size_t k = 0; k<K; k++) {
		Eigen::MatrixXd Z(Eigen::MatrixXd::Zero(3,3));
		Eigen::MatrixXd z(Eigen::MatrixXd::Zero(3,1));
		for (size_t i = 0; i<K; i++) {
			const Eigen::MatrixXd I_Gammai_inv = (Eigen::MatrixXd::Identity(3,3) - Gamma_k[i].inverse());
			const Eigen::MatrixXd I_Gammak_inv = (Eigen::MatrixXd::Identity(3,3) - Gamma_k[k]).inverse();
			Z += U(k, i)*I_Gammai_inv * I_Gammak_inv * Gamma_k[k];
			//Z += U(k, i)*I_Gammai_inv * (Eigen::MatrixXd::Identity(3,3) - Gamma_k[k].inverse()).inverse() * Gamma_k[k];
			z += U(k, i)*( (-1*(Gamma_k[i].inverse()*pi_k[i]))-I_Gammai_inv*(Eigen::MatrixXd::Identity(3,3) - Gamma_k[k]).inverse()*pi_k[k]);
		}

		Zk.push_back(Z);
		zk.push_back(z);
		//(*_writer)<<"zk = "<<zk.back()<<std::endl;
	}
	//std::cin.getline(ch, 2);

	//15: Compute v_k
	std::vector<Eigen::MatrixXd> v_k;
	for (size_t k = 0; k<K; k++) {
		Eigen::MatrixXd v = (Eigen::MatrixXd::Identity(3,3) + Zk[k]).inverse() * zk[k];
		v_k.push_back(v);
		//(*_writer)<<"vk = "<<v_k.back()<<std::endl;
	}
	//char ch[3];
	//std::cin.getline(ch, 2);

	//std::ofstream outfile("d:\\temp\\MComponents.txt");


	//double* Rfac = new double[K];
	//double* Pfac = new double[K];
	Q Rfac(K);
	Q Pfac(K);
	Q fac(K);

	for (size_t i = 0; i<K; i++) {
		Rfac[i] = 1;
		Pfac[i] = 1;
		fac[i] = 1;
	}

	const size_t N_IRLS_MAX = 50;
	const double IRLS_STEPSIZE_THRESHOLD = 1e-2;
	size_t irlsIterationCnt = 0;
	Eigen::MatrixXd RX = Eigen::MatrixXd::Identity(3,3);
	//	for (size_t i = 0; i<N_IRLS; i++)  {
	double irlsStepSizeR = 1;
	double irlsStepSizeP = 1;
	double irlsStepSize = 1;
	do {
		//Step 16: Compute M 
		Eigen::MatrixXd M(Eigen::MatrixXd::Zero(3,3));
		
		//(*_writer)<<"Weight used: "<<fac<<std::endl;
		//char ch[3]; std::cin.getline(ch, 1);
		for (size_t k = 0; k<K; k++) {
			//outfile<<"beta["<<k<<"] = "<<beta_k[k]<<std::endl;
			//outfile<<"alpha["<<k<<"] = "<<alpha_k[k]<<std::endl;
			//outfile<<"v["<<k<<"] = "<<v_k[k]<<std::endl;
			//outfile<<"p["<<k<<"] = "<<p_k[k]<<std::endl;
			//outfile<<"z["<<k<<"] = "<<zk[k]<<std::endl;
			//outfile<<"Z["<<k<<"] = "<<Zk[k]<<std::endl;
			//outfile<<"Gamma["<<k<<" = "<<Gamma_k[k]<<std::endl<<std::endl;
			double weight = fac[k];//(std::min(Rfac[k], Pfac[k]);

			
			if (_useRotation) {
				M += Rfac[k] * beta_k[k] * alpha_k[k].transpose();
				//M += weight * beta_k[k] * alpha_k[k].transpose();
			}
			if (_usePosition) {
				M += Pfac[k] * v_k[k] * p_k[k].transpose();
				//M += weight * v_k[k] * p_k[k].transpose();
			}
			//M += (beta_k[k]*alpha_k[k].transpose() + p_k[k]*v_k[k].transpose());
			//M += (beta_k[k]*alpha_k[k].transpose() + v_k[k]*p_k[k].transpose());
		}
		//(*_writer)<<"UseRotation: "<<_useRotation<<std::endl;
		//(*_writer)<<"UsePosition: "<<_usePosition<<std::endl;
		//(*_writer)<<"M = "<<std::setprecision(16)<<M<<std::endl;

		const Eigen::JacobiSVD<Eigen::MatrixXd> svdM = M.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV);
		
		Eigen::MatrixXd UsvdM = svdM.matrixU();
		Eigen::VectorXd sigmaM = svdM.singularValues();
		Eigen::MatrixXd VsvdM = svdM.matrixV();

		//(*_writer)<<"M: SVD U = "<<UsvdM<<std::endl;
		//(*_writer)<<"M: SVD sigma = "<<sigmaM<<std::endl;
		//(*_writer)<<"M: SVD V = "<<VsvdM<<std::endl;

		//(*_writer)<<"U.sigma.V = "<<UsvdM*Eigen::DiagonalMatrix<double, 3>(sigmaM)*VsvdM.transpose()<<std::endl;


	/*	(*_writer)<<"(VsvdM*UsvdM.transpose() = "<<VsvdM*UsvdM.transpose()<<std::endl;
		(*_writer)<<"Det = "<<(VsvdM*UsvdM.transpose()).determinant()<<std::endl;
	*/	double diaVal = Math::sign((VsvdM*UsvdM.transpose()).determinant());
//		(*_writer)<<"Dia Val = "<<diaVal<<std::endl;
		Eigen::DiagonalMatrix<double, 3> dia;
	
		for (size_t i = 0; i<3; i++)
			dia.diagonal()(i) = diaVal;

		//(*_writer)<<"Dia = "<<dia.diagonal()<<std::endl;

		RX = VsvdM*dia*(UsvdM.transpose());
		//(*_writer)<<"Rotation = "<<RX<<std::endl;
		//(*_writer)<<"Det Rot = "<<RX.determinant()<<std::endl;


		const double W_EPS = 1e-5;

		//Update the weights
		Q RfacPrev = Rfac;
		Q PfacPrev = Pfac;
		Q facPrev = fac;
		double wRmean = 0;
		double wPmean = 0;
		std::vector<double> residualRotations;
		std::vector<double> residualPositions;
		Statistics<double> meanErrorsRot;
		Statistics<double> meanErrorsPos;
		for (size_t i = 0; i<K; i++) {
			//Eigen::VectorXd residualRotTerm = (RX*beta_k[i])-alpha_k[i];
			//Eigen::VectorXd residualPosTerm = (RX*v_k[i])-p_k[i];

			double residualRotTerm = ((RX*beta_k[i])-alpha_k[i]).norm();
			double residualPosTerm = ((RX*v_k[i])-p_k[i]).norm();

			residualRotations.push_back(residualRotTerm);
			residualPositions.push_back(residualPosTerm);

			meanErrorsRot.add(residualRotTerm);
			meanErrorsPos.add(residualPosTerm);
		}

		double meanRot = meanErrorsRot.mean();
		double meanPos = meanErrorsPos.mean();

		for (size_t i = 0; i<K; i++) {
			//Rfac[i] = 1/(W_EPS + residualRotations[i].norm());
			//Pfac[i] = 1/(W_EPS + residualPositions[i].norm());
			Rfac[i] = std::exp(-1*(residualRotations[i]/meanRot));
			Pfac[i] = std::exp(-1*(residualPositions[i]/meanPos));
			
			wRmean += Rfac[i];
			wPmean += Pfac[i];
			//(*_writer)<<"Residuals = "<<residualRotTerm.norm()<<"  "<<residualPosTerm.norm()<<std::endl;
			//(*_writer)<<"Weights["<<i<<"] = "<<Rfac[i]<<"  "<<Pfac[i]<<std::endl;
		}

		wRmean /= (double)K;
		wPmean /= (double)K;
		for (size_t i = 0; i<K; i++) {
			Rfac[i] /= wRmean;
			Pfac[i] /= wPmean;
		}		

		if (_usePosition == true && _useRotation == true) 
			fac = Math::min(Rfac, Pfac);
		else if (_usePosition == true && _useRotation == false)
			fac = Pfac;
		else if (_usePosition == false && _useRotation == true)
			fac = Rfac;
		//(*_writer)<<"Rfac = "<<Rfac<<std::endl;
		//(*_writer)<<"Pfac = "<<Pfac<<std::endl;
		//(*_writer)<<"Fac: "<<fac<<std::endl;
		//(*_writer)<<irlsIterationCnt<<": DeltaR, DeltaP = "<<(Rfac-RfacPrev).norm2()<<" \t"<<(Pfac-PfacPrev).norm2()<<std::endl;
		irlsStepSizeR =  (Rfac-RfacPrev).norm2();
		irlsStepSizeP =  (Pfac-PfacPrev).norm2();
		irlsStepSize = (fac-facPrev).norm2();
		//(*_writer)<<"irlsStepSize = "<<irlsStepSize<<std::endl;
		irlsIterationCnt++;
		//if (irlsIterationCnt > N_IRLS_MAX / 2) 
        {
			(*_writer) << "Using many Iterations: StepSize: " <<irlsStepSize<< std::endl;
			//(*_writer) << "\t" << fac << std::endl;
			(*_writer) << "\t Pos Error: " << meanErrorsPos;
			(*_writer) << "\t Rot Error: "<< meanErrorsRot << std::endl;


		}
		if (irlsIterationCnt > N_IRLS_MAX) {
			(*_writer)<<"Throws Exception: "<<"IRLS unable to convert in "<<N_IRLS_MAX<<" iterations with weights "<<fac<<std::endl;
			char ch[3]; std::cin.getline(ch, 1);
			//RW_THROW("IRLS unable to convert in "<<N_IRLS_MAX<<" iterations");
			break;
		}
		//std::cin.getline(ch, 2);
	} while (irlsStepSize > IRLS_STEPSIZE_THRESHOLD  && irlsIterationCnt < N_IRLS_MAX ); 
	//while (irlsStepSizeR > IRLS_STEPSIZE_THRESHOLD || irlsStepSizeR > IRLS_STEPSIZE_THRESHOLD);
	//End for (size_t i = 0; i<N_IRLS; i++) 


	
	////
	////Step 17: Compute RX=(M^T M)^(-1/2) M^T
	//Eigen::MatrixXd MTM = M.transpose()*M;
	////(*_writer)<<"MTM = "<<MTM<<std::endl;
	//const Eigen::JacobiSVD<Eigen::MatrixXd> svd = MTM.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV);
	//Eigen::MatrixXd Usvd = svd.matrixU();
	//Eigen::VectorXd sigma = svd.singularValues();
	//Eigen::MatrixXd Vsvd = svd.matrixV();
	////(*_writer)<<"SVD U = "<<Usvd<<std::endl;
	////(*_writer)<<"SVD sigma = "<<sigma<<std::endl;
	////(*_writer)<<"SVD V = "<<Vsvd<<std::endl;
	//for (int i = 0; i<sigma.size(); i++) {
	//	sigma(i) = sqrt(sigma(i));
	//}

//	
//
//	//(*_writer)<<"SVD Sqrt = "<<Usvd*Eigen::DiagonalMatrix<double, Eigen::Dynamic>(sigma)*Vsvd<<std::endl;
//	//(*_writer)<<"SVD Sqrt = "<<Usvd*Eigen::DiagonalMatrix<double, Eigen::Dynamic>(sigma)*Vsvd.transpose()<<std::endl;
//	Eigen::MatrixXd sqrtM = Usvd*Eigen::DiagonalMatrix<double, Eigen::Dynamic>(sigma)*Vsvd.transpose();
//	//Eigen::LDLT<Eigen::MatrixXd> ldl = MTM.ldlt();
//	//Eigen::Diagonal<const Eigen::MatrixXd> diagonal = ldl.vectorD();
//	//Eigen::MatrixXd sqrtDiagonal = Eigen::MatrixXd::Zero(diagonal.size(), diagonal.size());
//	//for (int i = 0; i<diagonal.size(); i++) {
//	//	sqrtDiagonal(i, i) = sqrt(diagonal(i));
//	//}
//	//Eigen::Transpositions<Eigen::Dynamic> transposition = ldl.transpositionsP();
//
//	/*(*_writer)<<"TransitionP = "<<transposition*Eigen::MatrixXd(ldl.matrixL())<<std::endl;
//	(*_writer)<<"TransitionP = "<<Eigen::MatrixXd(ldl.matrixU())*transposition<<std::endl;
//	(*_writer)<<"Inverse ? = "<<transposition.transpose()*Eigen::MatrixXd(ldl.matrixL())*sqrtDiagonal*Eigen::MatrixXd(ldl.matrixU())*transposition<<std::endl;
//	(*_writer)<<"ldl.matrixLDLT"<<ldl.matrixLDLT()<<std::endl;
//	(*_writer)<<"Lower = "<<Eigen::MatrixXd(ldl.matrixL())<<std::endl;
//	(*_writer)<<"Diagonal = "<<diagonal<<std::endl;
//	(*_writer)<<"Upper = "<<Eigen::MatrixXd(ldl.matrixU())<<std::endl;*/
////	Eigen::MatrixXd tl = ldl.transpositionsP().transpose() * ldl.matrixL();
////	Eigen::MatrixXd sqrtM =  ldl.matrixL() * sqrtDiagonal * ldl.matrixU();
//	//(*_writer)<<"sqrtM = "<<sqrtM<<std::endl;
//	Eigen::MatrixXd RX = (sqrtM).inverse()*M.transpose();
//	(*_writer)<<"RX = "<<std::setprecision(16)<<RX<<std::endl;
//	//(*_writer)<<"Det(RX) = "<<RX.determinant()<<std::endl;

	//Step 18: Compute C as 3K x 3 stacked with (I-RBk) RX^T
	Eigen::MatrixXd C(3*K, 3);
	for (size_t k = 0; k<K; k++) {
		Eigen::MatrixXd I_Gamma_RX = (Eigen::MatrixXd::Identity(3,3) - Gamma_k[k]) * RX.transpose();
		for (size_t i = 0; i<3; i++) {
			for (size_t j = 0; j<3; j++) {
				C(3*k+i, j) = I_Gamma_RX(i,j);
			}
		}		
	}

	//(*_writer)<<"C = "<<C<<std::endl;

	//(*_writer)<<"RX = "<<RX<<std::endl;

	//Step 19: Compute c as the 3K vector based on -(pbk+RBk RX^T pk)
	Eigen::MatrixXd c(3*K,1);
	for (size_t k = 0; k<K; k++) {
		Eigen::MatrixXd a = -(pi_k[k]+ Gamma_k[k]*RX.transpose()*p_k[k]);
		for (size_t i = 0; i<3; i++) {
			c(3*k + i, 0) = a(i);
		}
	}

	//(*_writer)<<"c = "<<c<<std::endl;

	//Step 20: Compute px=(C^T C)^-1 C^T c+ p_avg
	Eigen::MatrixXd px = (C.transpose()*C).inverse()*C.transpose() * c + p_avg;
	//Find the Tbase2cam
	Transform3D<> TbaseCorrection = Transform3D<>(Vector3D<>(px), Rotation3D<>(RX));
	for (size_t k = 0; k<K; k++) {
		CalibrationMeasurement::Ptr measurement = measurements[k];
		Transform3D<> fk = getFK(measurement);
		Transform3D<> Ttcp = inverse(TbaseCorrection*fk) * measurement->staticTmoving();
	}

	Transform3D<> Tbase = Transform3D<>(Vector3D<>(px), Rotation3D<>(RX));
	//(*_writer)<<"Tbase = "<<Tbase<<std::endl;
	//Find the average TnTCP
	std::vector<Transform3D<> > tntcps;
	Vector3D<> posTool(0,0,0);
	Vector3D<> eaaAxis(0,0,0);
	double eaaAngle = 0;
	for (size_t k = 0; k<K; k++) {
		Transform3D<> fk = Tbase;
		Transform3D<> fk2 = getFK(measurements[k]); 
		//(*_writer)<<"fk = "<<fk<<std::endl;
		//(*_writer)<<"fk2 = "<<fk2<<std::endl;
		//(*_writer)<<"Measurement = "<<(measurements[k]->getTransform())<<std::endl;
		//(*_writer)<<"q = "<< measurements[k]->getQ()<<std::endl;
		//(*_writer)<<"fk total = "<<fk*fk2<<std::endl;
		Transform3D<> tntcp = inverse(fk*fk2) * (measurements[k]->staticTmoving());
		//(*_writer)<<"TNTCP = "<<tntcp<<std::endl;
		posTool += tntcp.P();
		EAA<> eaa(tntcp.R());
		//(*_writer)<<"eaa["<<k<<"] = "<<eaa<<std::endl;
		eaaAxis += eaa.axis();
		eaaAngle += eaa.angle();
		//eaaTool(0) += eaa(0);
		//eaaTool(1) += eaa(1);
		//eaaTool(2) += eaa(2);
		//char ch[4]; std::cin.getline(ch, 1);
	}
	posTool /= (double)K;
	eaaAxis /= (double)K;
	eaaAxis = normalize(eaaAxis);
	eaaAngle /= (double)K;
	//(*_writer)<<"posTool = "<<posTool<<std::endl;
	//(*_writer)<<"eaaTool = "<<eaaAxis<<"  "<<eaaAngle<<std::endl;
	//(*_writer)<<"Ttool = "<<Transform3D<>(posTool, EAA<>(eaaAxis, eaaAngle))<<std::endl;
	result.sensor2base = Tbase;
	result.tool2marker = Transform3D<>(posTool, EAA<>(eaaAxis, eaaAngle));
	result.cnt = measurements.size();
	//return std::make_pair(Tbase, Transform3D<>(posTool, EAA<>(eaaTool(0), eaaTool(1), eaaTool(2))));


	for (size_t i = 0; i<K; ++i) {
		delete[] Aij[i];
		delete[] Bij[i];
		delete[] alphaij[i];
		delete[] betaij[i];
	}

	delete[] Aij;
	delete[] Bij;
	delete[] alphaij;
	delete[] betaij;

	//delete[] Rfac;
	//delete[] Pfac;



}
