#include "CalibrationUtils.hpp"

#include <rw/math.hpp>
#include <rw/common.hpp>
using namespace rw::common;
using namespace rw::math;
using namespace rw::models;
using namespace rw::kinematics;

namespace sdurobotics {
namespace calibration {

void CalibrationUtils::printMeasurementSummary(const std::vector<CalibrationMeasurement::Ptr>& measurements, rw::models::WorkCell::Ptr workcell, const rw::kinematics::State& workcellState, std::ostream& outstream, bool printDetailsForAllMeasurements, std::map<std::string, Statistics<double> >* stats) {
	LogStreamWriter writer(&outstream);
	printMeasurementSummary(measurements, workcell, workcellState, writer, printDetailsForAllMeasurements, stats);
}


void CalibrationUtils::printMeasurementSummary(const std::vector<CalibrationMeasurement::Ptr>& measurements, WorkCell::Ptr workcell, const State& workcellState, LogWriter& output, bool printDetailsForAllMeasurements, std::map<std::string, Statistics<double> >* stats) {
	const unsigned int measurementCount = measurements.size();
	rw::kinematics::State state = workcellState;
	std::map<std::string, Statistics<double> > distances;
	std::map<std::string, Statistics<double> > angles;

	Statistics<double> allDistances;
	Statistics<double> allAngles;
	//Run through measurements and compute the errors
	for (unsigned int measurementIndex = 0; measurementIndex < measurementCount; measurementIndex++) {
		CalibrationMeasurement::Ptr measurement = measurements[measurementIndex];
		SerialDevice::Ptr serialDevice = workcell->findDevice<SerialDevice>(measurement->getDeviceName());
		Frame* staticFrame = workcell->findFrame(measurement->getStaticFrameName());
    //    std::cout << "Static Frame = " << staticFrame->getName() << std::endl;
		Frame* movingFrame = workcell->findFrame(measurement->getMovingFrameName());
    //    std::cout << "Moving Frame = " << movingFrame->getName() << std::endl;
		const rw::math::Transform3D<> tfmMeasurement = measurements[measurementIndex]->staticTmoving();

		//		std::cout << "Measurement = " << tfmMeasurement.P() << " vs ";
		//const rw::math::Transform3D<> tfmModel = rw::kinematics::Kinematics::frameTframe(staticFrame, movingFrame, state);
		Transform3D<> tfmModel;
		if (measurement->hasBaseTmoving()) {
			Transform3D<> staticTbase = rw::kinematics::Kinematics::frameTframe(staticFrame, serialDevice->getBase(), state);
			Transform3D<> baseTmoving = measurement->baseTmoving();
			//Transform3D<> movingTmoving2 = rw::kinematics::Kinematics::frameTframe(movingFrame->getParent(), movingFrame, state);
            tfmModel = staticTbase * baseTmoving;// *movingTmoving2;
            //std::cout << "static to moving ["<<measurementIndex<<"] = " << tfmModel << std::endl;
		}
		else {
			serialDevice->setQ(measurement->getQ(), state);
			tfmModel = rw::kinematics::Kinematics::frameTframe(staticFrame, movingFrame, state);
		}

	//	std::cout << "Model = " << tfmModel.P() << std::endl;
		const rw::math::Transform3D<> tfmError = rw::math::Transform3D<>(tfmModel.P() - tfmMeasurement.P(), tfmModel.R() * rw::math::inverse(tfmMeasurement.R()));
		//std::cout<<"Measurement["<<measurementIndex<<"] = "<<measurement->getDeviceName()<<" "<<measurement->getStaticFrameName()<<" "<<tfmError.P().norm2()*1000.<<"mm "<<rw::math::EAA<>(tfmError.R()).angle()*Rad2Deg<<std::endl;
		distances[measurement->getStaticFrameName()].add(tfmError.P().norm2());
		angles[measurement->getStaticFrameName()].add(rw::math::EAA<>(tfmError.R()).angle());
		if (printDetailsForAllMeasurements) {
			output << "Measurement[" << measurementIndex << "]: Errors: Pos = " << tfmError.P() <<" = "<<tfmError.P().norm2()*1000.0 << "mm  Angle = " << rw::math::EAA<>(tfmError.R()).angle()*rw::math::Rad2Deg << std::endl;
		}
		allDistances.add(tfmError.P().norm2());
		allAngles.add(rw::math::EAA<>(tfmError.R()).angle());
	}

	//Print output results for each sensor
	for (std::map<std::string, Statistics<double> >::iterator it = distances.begin(); it != distances.end(); ++it) {
		double dist = (*it).second.mean();
		std::pair<double, double> distMinMax = (*it).second.minAndMaxValue();
		double angle = angles[(*it).first].mean();
		std::pair<double, double> angleMinMax = angles[(*it).first].minAndMaxValue();
		std::cout<<"Number of measurements: "<<(*it).second.data().size()<<std::endl;
		output << "\tSummary("<<(*it).first<<") - : [ Avg: " << dist * 1000.0 << " mm / " << angle * rw::math::Rad2Deg << " deg - Min: "
			<< distMinMax.first * 1000.0 << " mm / " << angleMinMax.first * rw::math::Rad2Deg << " deg - Max: " << distMinMax.second * 1000.0 << " mm / "
			<< angleMinMax.second * rw::math::Rad2Deg << " deg ]" << std::endl;
	}
	double dist = allDistances.mean();
	std::pair<double, double> distMinMax = allDistances.minAndMaxValue();
	double angle = allAngles.mean();
	std::pair<double, double> angleMinMax = allAngles.minAndMaxValue();
	std::cout<<"Number of measurements: "<<allDistances.data().size()<<std::endl;
	output << "\tSummary( All Sensors ) - : [ Avg: " << dist * 1000.0 << " mm / " << angle * rw::math::Rad2Deg << " deg - Min: "
			<< distMinMax.first * 1000.0 << " mm / " << angleMinMax.first * rw::math::Rad2Deg << " deg - Max: " << distMinMax.second * 1000.0 << " mm / "
			<< angleMinMax.second * rw::math::Rad2Deg << " deg ]" << std::endl;

    if (stats != NULL) {
        (*stats)["AvgDist"].add(dist*1000);
        (*stats)["AvgAngle"].add(angle);
        (*stats)["MinDist"].add(distMinMax.first*1000);
        (*stats)["MinAngle"].add(angleMinMax.first);
        (*stats)["MaxDist"].add(distMinMax.second*1000);
        (*stats)["MaxAngle"].add(angleMinMax.second);
    }
}


void CalibrationUtils::computeResiduals(const std::vector<CalibrationMeasurement::Ptr>& measurements, rw::models::WorkCell::Ptr workcell, const rw::kinematics::State& workcellState, std::map<std::string, rw::math::Statistics<double> >* stats, bool printSummary, bool printForAllMeasurements, LogWriter::Ptr writer)
{
	if (writer == NULL) {
		printSummary = false;
		printForAllMeasurements = false;
	}
	const unsigned int measurementCount = measurements.size();
	rw::kinematics::State state = workcellState;
	std::map<std::string, Statistics<double> > distances;
	std::map<std::string, Statistics<double> > angles;

	Statistics<double> allDistances;
	Statistics<double> allAngles;
	//Run through measurements and compute the errors
	for (unsigned int measurementIndex = 0; measurementIndex < measurementCount; measurementIndex++) {
		CalibrationMeasurement::Ptr measurement = measurements[measurementIndex];
		SerialDevice::Ptr serialDevice = workcell->findDevice<SerialDevice>(measurement->getDeviceName());
		Frame* staticFrame = workcell->findFrame(measurement->getStaticFrameName());
		Frame* movingFrame = workcell->findFrame(measurement->getMovingFrameName());

		Transform3D<> tfmModel;
		if (measurement->hasBaseTmoving()) {
			Transform3D<> staticTbase = rw::kinematics::Kinematics::frameTframe(staticFrame, serialDevice->getBase(), state);
			Transform3D<> baseTmoving = measurement->baseTmoving();
			//Transform3D<> movingTmoving2 = rw::kinematics::Kinematics::frameTframe(movingFrame->getParent(), movingFrame, state);
            tfmModel = staticTbase * baseTmoving;// *movingTmoving2;
		}
		else {
			serialDevice->setQ(measurement->getQ(), state);
			tfmModel = rw::kinematics::Kinematics::frameTframe(staticFrame, movingFrame, state);
		}
		const rw::math::Transform3D<> tfmMeasurement = measurements[measurementIndex]->staticTmoving();
		
		const rw::math::Transform3D<> tfmError = rw::math::Transform3D<>(tfmModel.P() - tfmMeasurement.P(), tfmModel.R() * rw::math::inverse(tfmMeasurement.R()));
		//std::cout<<"Measurement["<<measurementIndex<<"] = "<<measurement->getDeviceName()<<" "<<measurement->getStaticFrameName()<<" "<<tfmError.P().norm2()*1000.<<"mm "<<rw::math::EAA<>(tfmError.R()).angle()*Rad2Deg<<std::endl;
		distances[measurement->getStaticFrameName()].add(tfmError.P().norm2());
		angles[measurement->getStaticFrameName()].add(rw::math::EAA<>(tfmError.R()).angle());
		if (printForAllMeasurements) {
			(*writer) << "Measurement[" << measurementIndex << "]: Errors: Pos = " << tfmError.P() << " = " << tfmError.P().norm2()*1000.0 << "mm  Angle = " << rw::math::EAA<>(tfmError.R()).angle()*rw::math::Rad2Deg << std::endl;
		}
		measurements[measurementIndex]->setCalibrationError(tfmError.P().norm2()*1000.0);
		allDistances.add(tfmError.P().norm2());
		allAngles.add(rw::math::EAA<>(tfmError.R()).angle());
	}

	if (printSummary) {
		//Print output results for each sensor
		for (std::map<std::string, Statistics<double> >::iterator it = distances.begin(); it != distances.end(); ++it) {
			double dist = (*it).second.mean();
			std::pair<double, double> distMinMax = (*it).second.minAndMaxValue();
			double angle = angles[(*it).first].mean();
			std::pair<double, double> angleMinMax = angles[(*it).first].minAndMaxValue();
			std::cout << "Number of measurements: " << (*it).second.data().size() << std::endl;
			(*writer) << "\tSummary(" << (*it).first << ") - : [ Avg: " << dist * 1000.0 << " mm / " << angle * rw::math::Rad2Deg << " deg - Min: "
				<< distMinMax.first * 1000.0 << " mm / " << angleMinMax.first * rw::math::Rad2Deg << " deg - Max: " << distMinMax.second * 1000.0 << " mm / "
				<< angleMinMax.second * rw::math::Rad2Deg << " deg ]" << std::endl;
		}
	}
	double dist = allDistances.mean();
	std::pair<double, double> distMinMax = allDistances.minAndMaxValue();
	double angle = allAngles.mean();
	std::pair<double, double> angleMinMax = allAngles.minAndMaxValue();
	if (printSummary) {
		(*writer) << "Number of measurements: " << allDistances.data().size() << std::endl;
		(*writer) << "\tSummary( All Sensors ) - : [ Avg: " << dist * 1000.0 << " mm / " << angle * rw::math::Rad2Deg << " deg - Min: "
			<< distMinMax.first * 1000.0 << " mm / " << angleMinMax.first * rw::math::Rad2Deg << " deg - Max: " << distMinMax.second * 1000.0 << " mm / "
			<< angleMinMax.second * rw::math::Rad2Deg << " deg ]" << std::endl;
	}
	if (stats != NULL) {
		(*stats)["AvgDist"].add(dist * 1000);
		(*stats)["AvgAngle"].add(angle);
		(*stats)["MinDist"].add(distMinMax.first * 1000);
		(*stats)["MinAngle"].add(angleMinMax.first);
		(*stats)["MaxDist"].add(distMinMax.second * 1000);
		(*stats)["MaxAngle"].add(angleMinMax.second);
	}

}



 
}
}
