/*
 * CalibrationMeasurement.cpp
 *
 */

#include "CalibrationMeasurement.hpp"

#include <rw/kinematics.hpp>

using namespace sdurobotics::calibration;


CalibrationMeasurement::CalibrationMeasurement(const rw::math::Q& q, const rw::math::Transform3D<>& staticTmoving, const std::string& deviceName, const std::string& staticFrameName, const std::string& movingFrameName, const std::string& id, double rmsError) :
	_q(q), _hasQ(true), _hasBaseTmoving(false), _staticTmoving(staticTmoving), _hasCovarianceMatrix(false), _deviceName(deviceName), _staticFrameName(staticFrameName), _movingFrameName(movingFrameName), _id(id), _rmsError(rmsError) {
}

CalibrationMeasurement::CalibrationMeasurement(const rw::math::Transform3D<>& baseTmoving, const rw::math::Transform3D<>& staticTmoving, const std::string& deviceName, const std::string& staticFrameName, const std::string& movingFrameName, const std::string& id, double rmsError) :
	_baseTmoving(baseTmoving), _hasQ(false), _hasBaseTmoving(true), _staticTmoving(staticTmoving), _hasCovarianceMatrix(false), _deviceName(deviceName), _staticFrameName(staticFrameName), _movingFrameName(movingFrameName), _id(id), _rmsError(rmsError) {
}


CalibrationMeasurement::CalibrationMeasurement(const rw::math::Q& q, const rw::math::Transform3D<>& staticTmoving, const Eigen::MatrixXd& covarianceMatrix, const std::string& deviceName, const std::string& staticFrameName, const std::string& movingFrameName, const std::string& id, double rmsError) :
	_q(q), _hasQ(true), _hasBaseTmoving(false), _staticTmoving(staticTmoving), _covarianceMatrix(covarianceMatrix), _hasCovarianceMatrix(true), _deviceName(deviceName), _staticFrameName(staticFrameName), _movingFrameName(movingFrameName), _id(id), _rmsError(rmsError) {
}

CalibrationMeasurement::CalibrationMeasurement(const rw::math::Transform3D<>& baseTmoving, const rw::math::Transform3D<>& staticTmoving, const Eigen::MatrixXd& covarianceMatrix, const std::string& deviceName, const std::string& staticFrameName, const std::string& movingFrameName, const std::string& id, double rmsError) :
	_baseTmoving(baseTmoving), _hasQ(false), _hasBaseTmoving(true), _staticTmoving(staticTmoving), _covarianceMatrix(covarianceMatrix), _hasCovarianceMatrix(true), _deviceName(deviceName), _staticFrameName(staticFrameName), _movingFrameName(movingFrameName), _id(id), _rmsError(rmsError) {
}

 
CalibrationMeasurement::~CalibrationMeasurement() {

}

bool CalibrationMeasurement::hasQ() const
{
	return _hasQ;
}

bool CalibrationMeasurement::hasBaseTmoving() const
{
	return _hasBaseTmoving;
}


const rw::math::Q& CalibrationMeasurement::getQ() const {
	if (hasQ()) {
		return _q;
	}
	else {
		RW_THROW("No robot configuration specified for measurement");
	}
}

void CalibrationMeasurement::setQ(const rw::math::Q& q) {
	_q = q;
	_hasQ = true;
}

const rw::math::Transform3D<> CalibrationMeasurement::baseTmoving() const
{
	if (hasBaseTmoving()) {
		return _baseTmoving;
	}
	else {
		RW_THROW("No base to moving frame transform specified for measurement");
	}
}

void CalibrationMeasurement::setBaseTmoving(const rw::math::Transform3D<>& baseTmoving)
{
	_baseTmoving = baseTmoving;
	_hasBaseTmoving = true;
}

const rw::math::Transform3D<>& CalibrationMeasurement::staticTmoving() const {
	return _staticTmoving;
}

void CalibrationMeasurement::setStaticTmoving(const rw::math::Transform3D<>& staticTmoving) {
	_staticTmoving = staticTmoving;
}

const Eigen::MatrixXd& CalibrationMeasurement::getCovarianceMatrix() const {
	return _covarianceMatrix;
}

void CalibrationMeasurement::setCovarianceMatrix(const Eigen::MatrixXd& covar) {
	_covarianceMatrix = covar;
	_hasCovarianceMatrix = true;
}

bool CalibrationMeasurement::hasCovarianceMatrix() const {
	return _hasCovarianceMatrix;
}



const std::string& CalibrationMeasurement::getDeviceName() const
{
	return _deviceName;
}

const std::string& CalibrationMeasurement::getStaticFrameName() const
{
	return _staticFrameName;
}


const std::string& CalibrationMeasurement::getMovingFrameName() const 
{
	return _movingFrameName;
}

const std::string& CalibrationMeasurement::getID() const 
{
	return _id;
}

const double CalibrationMeasurement::getRMSError() const
{
	return _rmsError;
}

void CalibrationMeasurement::setDeviceName(const std::string& deviceName) 
{
	_deviceName = deviceName;
}

void CalibrationMeasurement::setStaticFrameName(const std::string& staticFrameName) 
{
	_staticFrameName = staticFrameName;
}

void CalibrationMeasurement::setMovingFrameName(const std::string& movingFrameName) 
{
	_movingFrameName = movingFrameName;
}

void CalibrationMeasurement::setID(const std::string& id) 
{
	_id = id;
}

void CalibrationMeasurement::setRMSError(const double rmsError) 
{
	_rmsError = rmsError;
}

void CalibrationMeasurement::setCalibrationError(double calibraitonError){
	_calibraitonError = calibraitonError;
}

double CalibrationMeasurement::getCalibrationError(){
	return _calibraitonError;
}


void CalibrationMeasurement::setDetectionInfo(DetectionInfoBase::Ptr detectionInfo) {

}

