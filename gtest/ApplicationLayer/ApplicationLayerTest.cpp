#include <iostream>

#include "gtest/gtest.h"

#include "ApplicationLayer/EntityRepositoryInitializer.hpp"

TEST(ApplicationLayer, InitializeRepository)
{
	EXPECT_EQ(1, 1);
}


TEST(ApplicationLayer, somethingelseTest)
{
    EXPECT_EQ(1, 1);
}

TEST(ApplicationLayerOther, somethingelseTest)
{
    EXPECT_EQ(1, 1);
}

