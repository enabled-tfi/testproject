#include <iostream>
#include <fstream>

#include "gtest/gtest.h"

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/predicate.hpp>

#include <gtest/TestEnvironment.hpp>
#include <src/SecurityLayer/CopyProtector.hpp>
#include <src/SecurityLayer/HWAddressProvider.hpp>

TEST(SecurityLayer, CreateAndUnlock){
	std::vector<std::string> interfaces = er_security::HWAddressProvider::getNetworkInterfaces();
	std::string ethInterface;
	for(int i = 0; i < interfaces.size(); i++){
		if (boost::starts_with(interfaces[i], "enp"))
			ethInterface = interfaces[i];
	}

	er_security::CopyProtector copyPro;
	copyPro.generateSecurityFile(TestEnvironment::getDataDir() + "license.er", ethInterface, "TestPassword");
	EXPECT_TRUE(copyPro.unlock(TestEnvironment::getDataDir() + "license.er", "TestPassword"));
	EXPECT_TRUE(copyPro.isLegal());
}

TEST(SecurityLayer, UnlockCorruptedLicense)
{
	std::vector<std::string> interfaces = er_security::HWAddressProvider::getNetworkInterfaces();

	std::string ethInterface;
	for(int i = 0; i < interfaces.size(); i++){
		if (boost::starts_with(interfaces[i], "enp"))
			ethInterface = interfaces[i];
	}

	er_security::CopyProtector copyPro;
	copyPro.generateSecurityFile(TestEnvironment::getDataDir() + "corruptedLicense.er", ethInterface, "TestPassword");

		// Add character in the end of license file
	std::ofstream ofs (TestEnvironment::getDataDir() + "corruptedLicense.er", std::ofstream::app);
	ofs << "1 2a";
	ofs.close();

		// License file should now be corrupted
	EXPECT_FALSE(copyPro.unlock(TestEnvironment::getDataDir() + "corruptedLicense.er", "TestPassword"));
	EXPECT_FALSE(copyPro.isLegal());
}

TEST(SecurityLayer, UnlockWrongPassword)
{
	std::vector<std::string> interfaces = er_security::HWAddressProvider::getNetworkInterfaces();
	std::string ethInterface;
	for(int i = 0; i < interfaces.size(); i++){
		if (boost::starts_with(interfaces[i], "enp"))
			ethInterface = interfaces[i];
	}

	er_security::CopyProtector copyPro;
	copyPro.generateSecurityFile(TestEnvironment::getDataDir() + "license.er", ethInterface, "TestPassword");

		// Unlock with wrong password should lead to locked system
	EXPECT_FALSE(copyPro.unlock(TestEnvironment::getDataDir() + "license.er", "WrongTestPassword"));
	EXPECT_FALSE(copyPro.isLegal());
}


class SecurityTestClass : public er_security::CopyProtector{
	public:
	void testEncryptionMethod(){
		std::string str = "MyDataMyDataMyData";
		std::string encryptedStr = this->encrypt(str, "MyPassWord");
		std::string decryptedStr = this->decrypt(encryptedStr, "MyPassWord");
		EXPECT_STREQ(str.c_str(), decryptedStr.c_str());

		decryptedStr = this->decrypt(encryptedStr, "MyPassWordWrong");
		EXPECT_STRNE(str.c_str(), decryptedStr.c_str());
	}
};

TEST(SecurityLayer, EncryptionDecryption){
	SecurityTestClass test;
	test.testEncryptionMethod();
}



TEST(HWAddressProvider, GetMacAddresses){
	std::vector<std::string> interfaces = er_security::HWAddressProvider::getNetworkInterfaces();
	for(int i = 0; i < interfaces.size(); i++){
		std::string macAdd = er_security::HWAddressProvider::getMacAddress(interfaces[i]);
		EXPECT_DOUBLE_EQ(macAdd.length(), 17);
	}
}


int main(int argc, char** argv) {
	::testing::InitGoogleTest(&argc, argv);
    ::testing::AddGlobalTestEnvironment(TestEnvironment::make(argc,argv));
	return RUN_ALL_TESTS();
}

