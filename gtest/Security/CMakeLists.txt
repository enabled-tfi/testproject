########################################################################
# SecurityLayer
########################################################################

SET(SECURITY_LAYER_TEST_LIBRARIES
  er_security
  er_common
  ${BOOST_LIBRARIES}
)

SET(SECURITY_LAYER_TEST_SRC
  SecurityTest.cpp
)

ADD_EXECUTABLE(security-gtest ${SECURITY_LAYER_TEST_SRC})
TARGET_LINK_LIBRARIES(security-gtest ${SECURITY_LAYER_TEST_LIBRARIES})
ADD_GTEST(security-gtest)
 