#ifndef ER_BASE_GTEST_TESTENVIRONMENT_HPP_
#define ER_BASE_GTEST_TESTENVIRONMENT_HPP_

#include <gtest/gtest.h>

/**
 * @brief Class for setting up the googletest environment.
 */
class TestEnvironment: public ::testing::Environment {
public:

	TestEnvironment();
	virtual ~TestEnvironment();


	//! @brief Sets up this Google Test environment.
	void SetUp();

	//! @brief TearDown of the test environment.
	void TearDown();

	/**
	 * @brief Initialize the environment with command-line arguments.
	 * @param argc [in] the number of arguments.
	 * @param argv [in] the arguments.
	 */
	void init(int argc, char** argv);

	/**
	 * @brief Get the test data directory.
	 * @return the path.
	 */
	static std::string getDataDir();

	/**
	 * @brief Get the executable directory
	 * @return the directory
	 */
	static std::string getExecutableDir();
	/**
	 * @brief Get the environment object.
	 * @return pointer to this environment.
	 */
	static TestEnvironment* get();

	/**
	 * @brief Create the environment object, initialized with arguments.
	 * @param argc [in] the number of arguments.
	 * @param argv [in] the arguments.
	 * @return pointer to this environment.
	 */
	static TestEnvironment* make(int argc, char** argv);

//    void TestBody(){};

private:
	int _argc;
	char** _argv;
};

#endif /* ER_BASE_GTEST_TESTENVIRONMENT_HPP_ */
