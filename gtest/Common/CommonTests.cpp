#include <iostream>
#include <fstream>

#include <rw/math/MetricFactory.hpp>

#include "gtest/gtest.h"

#include "Common/Utils.hpp"
#include "Common/Target.hpp"
#include "Common/System.hpp"

#include <Serialization/InputArchiveXML.hpp>

#include <gtest/TestEnvironment.hpp>

TEST(Target, Create){
	er_common::Target::Ptr target = new er_common::Target();
	EXPECT_FALSE(target->isInitialized());
}

TEST(UtilsTest, Pose2DtoTransform3D)
{
	rw::math::Pose2D<> testPose(1,2,0.5);
	rw::math::Transform3D<> result;
	result = er_common::Utils::pose2DtoTransform3D(testPose, result);

	EXPECT_EQ(result.P()[0], 1);
	EXPECT_EQ(result.P()[1], 2);
	EXPECT_EQ(result.P()[2], 0);

	EXPECT_TRUE(result.R().equal(rw::math::Rotation3D<>(0.877583, -0.479426, 0, 0.479426, 0.877583, 0, -0, 0, 1), 0.01));
	EXPECT_FALSE(result.R().equal(rw::math::Rotation3D<>(1,0,0,0,1,0,0,0,1), 0.01));
}

TEST(UtilsTest, Transform3DtoTransform2D)
{
	rw::math::Transform3D<> testTransform(rw::math::Vector3D<>(2,1,3), rw::math::Rotation3D<>(1,3,2,2,1,3,3,1,2));
	rw::math::Pose2D<> result;
	result = er_common::Utils::transform3DtoTransform2D(testTransform);

	EXPECT_EQ(result.x(), 2);
	EXPECT_EQ(result.y(), 1);
	ASSERT_NEAR(result.theta(), 1.10715, 0.0001 );
}

TEST(UtilsTest, SortQ)
{
	std::vector<rw::math::Q> testQs;
	testQs.push_back(rw::math::Q(4, 1,  0, 5,  0));
	testQs.push_back(rw::math::Q(4, 3, -3, 3, -4));
	testQs.push_back(rw::math::Q(4, 0, 12, 3,  4));

	rw::math::Q testQnear(4,0,0,0,0);

	std::queue<rw::math::Q> resultQs = er_common::Utils::sortQ(testQs, testQnear, rw::math::MetricFactory::makeEuclidean<rw::math::Q>());
	EXPECT_EQ(resultQs.front()[0], 1);
	resultQs.pop();
	EXPECT_EQ(resultQs.front()[0], 3);
	resultQs.pop();
	EXPECT_EQ(resultQs.front()[0], 0);
	resultQs.pop();

	resultQs = er_common::Utils::sortQ(testQs, testQnear, rw::math::MetricFactory::makeInfinity<rw::math::Q>());
	EXPECT_EQ(resultQs.front()[0], 3);
	resultQs.pop();
	EXPECT_EQ(resultQs.front()[0], 1);
	resultQs.pop();
	EXPECT_EQ(resultQs.front()[0], 0);
	resultQs.pop();

}

TEST(UtilsTest, GetTimestampAsString){
	std::string timeStamp = er_common::Utils::getTimestampAsString();
	EXPECT_NE(timeStamp, "");
}

