#include <Common/Settings.hpp>
#include <gtest/TestEnvironment.hpp>
#include "gtest/gtest.h"

TEST(SettingsTest, Create){
	er_common::Settings::Ptr testSettings = new er_common::Settings;
	EXPECT_FALSE(testSettings.isNull());
	EXPECT_TRUE(testSettings->empty());
	EXPECT_FALSE(testSettings->has("Test"));
	EXPECT_THROW(testSettings->get<std::string>("Test"), rw::common::Exception);
}

TEST(SettingsTest, AddSetting){
	er_common::Settings::Ptr testSettings = new er_common::Settings;

		// Add simple double
	testSettings->add<double>("TestSetting", "Desc", 2);

		// Settings should have and return correct setting
	EXPECT_TRUE(testSettings->has("TestSetting"));
	EXPECT_DOUBLE_EQ(testSettings->get<double>("TestSetting"), 2);

		// Throw exception if wrong type is specified
	EXPECT_THROW(testSettings->get<std::string>("TestSetting"), rw::common::Exception);
	EXPECT_THROW(testSettings->get<int>("TestSetting"), rw::common::Exception);
	EXPECT_THROW(testSettings->get<bool>("TestSetting"), rw::common::Exception);

		// Add string to settings
	testSettings->add<std::string>("TestSetting1", "Desc", "Test");

		// See that get and has methods works
	EXPECT_TRUE(testSettings->has("TestSetting1"));
	EXPECT_STREQ(testSettings->get<std::string>("TestSetting1").c_str(), "Test");
	EXPECT_THROW(testSettings->get<double>("TestSetting1"), rw::common::Exception);
	EXPECT_STREQ(testSettings->get<std::string>("TestSettingWrong", "ReturnStr").c_str(), "ReturnStr");

		// Try to add setting that already exist
	er_common::Setting<std::string>::Ptr alreadyDefinedSetting = testSettings->add<std::string>("TestSetting1", "Desc", "TestNew");
	EXPECT_STREQ("Test", alreadyDefinedSetting->getValue().c_str());

		// Try adding setting that already exists with force
	testSettings->addForce<std::string>("TestSetting1", "Desc", "TestNew");
	EXPECT_STREQ("TestNew", testSettings->get<std::string>("TestSetting1").c_str());

		// Set existing setting
	testSettings->set<std::string>("TestSetting1", "NewTest");
	EXPECT_STREQ("NewTest", testSettings->get<std::string>("TestSetting1").c_str());

		// Try to set setting that has not yet been set
	testSettings->set<std::string>("NotDefinedTestSetting", "TestTest");
	EXPECT_STREQ("TestTest", testSettings->get<std::string>("NotDefinedTestSetting").c_str());


}

TEST(SettingsTest, RemoveSetting){
	er_common::Settings::Ptr testSettings = new er_common::Settings;
	testSettings->add<double>("TestSetting", "Desc", 2);

		// Check added correctly
	EXPECT_TRUE(testSettings->has("TestSetting"));

		// Erase and check
	EXPECT_TRUE(testSettings->erase("TestSetting"));
	EXPECT_FALSE(testSettings->has("TestSetting"));

		// Erase setting that has already been erased
	EXPECT_FALSE(testSettings->erase("TestSetting"));
}


TEST(SettingsTest, CopySettings){
	er_common::Settings::Ptr testSettings = new er_common::Settings;
	testSettings->add<double>("TestSetting", "Desc", 2);

		// Copy settings to new settings and test
	er_common::Settings::Ptr testSettingsCopy = new er_common::Settings(*testSettings.get());
	EXPECT_TRUE(testSettingsCopy->has("TestSetting"));

		// Add setting to original and test the other
	testSettings->add<double>("TestSetting1", "Desc", 2);
	EXPECT_FALSE(testSettingsCopy->has("TestSetting1"));

		// Swap settings and test
	testSettings->swap(*testSettingsCopy.get());

	EXPECT_TRUE(testSettingsCopy->has("TestSetting1"));
	EXPECT_FALSE(testSettings->has("TestSetting1"));
}

	// TODO: Still need test:
//	testSettings->read()
//	testSettings->write()


