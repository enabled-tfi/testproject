#include "Common/System.hpp"
#include "Common/HWEntity.hpp"
#include <Serialization/InputArchiveXML.hpp>
#include <Serialization/OutputArchiveXML.hpp>

#include <gtest/TestEnvironment.hpp>
#include <boost/filesystem.hpp>


	// Currently needs defined names on devices, e.g. UR10, MobileDevice etc.
TEST(System, ReadInitialize){
	TestEnvironment::getDataDir();
	std::string filename = TestEnvironment::getDataDir() + "Systems/SystemNoHWEntities.xml";

		// Test if system is initialized - should be when getInstance is called
	er_common::System::Ptr system = er_common::System::getInstance();
	EXPECT_TRUE(system->isInitialized());

		// If system is not read from file or entites are initialized, should throw exception
    EXPECT_THROW(system->initialize(), rw::common::Exception);

		// Read system from file and initialize
	std::string systemFolder = rw::common::StringUtil::getDirectoryName(filename);
    system->setSystemFolder(systemFolder);

    er_serialization::InputArchiveXML iarchive(filename);
    iarchive.initialize();

    system->read(iarchive, "System");
	EXPECT_NO_THROW(system->initialize());

		// Test if all variable pointers are set
	EXPECT_FALSE(system->getWorkCell().isNull());
	EXPECT_STREQ(system->getCalibrationTCP()->getName().c_str(), "CalibrationTCP");
	EXPECT_FALSE(system->getCollisionDetector().isNull());
	EXPECT_FALSE(system->getKnowledgeBase().isNull());
	EXPECT_FALSE(system->getManipulator().isNull());
	EXPECT_FALSE(system->getManipulatorIKSolver().isNull());
	EXPECT_FALSE(system->getManipulatorJointMotionMetric().isNull());
	EXPECT_FALSE(system->getMobileBase().isNull());
	EXPECT_FALSE(system->getMobileBaseCollisionDetector().isNull());
	EXPECT_FALSE(system->getWorkCell().isNull());
	EXPECT_TRUE(system->getState().size() > 0);
	EXPECT_FALSE(system->getManipulatorConstraint(system->getState()).isNull());
	EXPECT_FALSE(system->getManipulatorQ2QPlanner(system->getState()).isNull());
	EXPECT_FALSE(system->getQHome().empty());
	EXPECT_FALSE(system->getReferenceFrames().empty());
	EXPECT_FALSE(system->getToolTCP().isNull());

}

TEST(System, SetMethodTests)
{

	er_common::System::Ptr system = er_common::System::getInstance();

		// Test setCalibrationTCP method
	std::string testFrameName = "TestFrame";
	rw::kinematics::FixedFrame::Ptr testFrame = new rw::kinematics::FixedFrame(testFrameName, rw::math::Transform3D<>());
	system->setCalibrationTCP(testFrame);
	EXPECT_STREQ(system->getCalibrationTCP()->getName().c_str(), testFrameName.c_str());

		// Test setToolTcp method
	system->setToolTCP(testFrame);
	EXPECT_STREQ(system->getToolTCP()->getName().c_str(), testFrameName.c_str());

		// Test setReferenceFrame method
	system->setReferenceFrame(testFrame);
	EXPECT_STREQ(system->getReferenceFrame()->getName().c_str(), testFrameName.c_str());

		// Test setDataFolder
	std::string testFolder =  TestEnvironment::getDataDir() + "TestFolder";
	system->setDataFolder(testFolder);
	EXPECT_STREQ(system->getDataFolder().c_str(), testFolder.c_str());

		// Test setMarkerImageFolder()
	system->setMarkerImageFolder(testFolder);
	EXPECT_STREQ(system->getMarkerImageFolder().c_str(), testFolder.c_str());

		// TODO: Set methods test needing implementation
//	system->setCollisionDetector()
//	system->setManipulatorIKSolver()
//	system->setMarkerImageFolder();
//	system->setMobileBaseCollisionDetector()
//	system->setQHome()
//	system->setState()
//	system->setSystemFolder()
//	system->setWorkCell()
//	system->addWorkCellCalibrationFile()


}

TEST(System, UpdateRules){

	class TestEntity : public hwl::HWEntity{
	public:
	    typedef rw::common::Ptr<TestEntity> Ptr;
	};

	class TestUpdateRule : public er_common::System::UpdateRule{
	public:
	    typedef rw::common::Ptr<TestUpdateRule> Ptr;

		TestUpdateRule():
			er_common::System::UpdateRule()
		{
		}

		~TestUpdateRule(){}

		void update(rw::kinematics::State& state)
		{
			updateCounter++;
		}

		er_common::Entity::Ptr getEntity(){
			return _testEntity;
		}

		int updateCounter = 0;

	private:
		TestEntity::Ptr _testEntity;
	};

	TestUpdateRule::Ptr updRule = new TestUpdateRule();

	er_common::System::Ptr system = er_common::System::getInstance();

	system->addUpdateRule(updRule);
	system->update();

	EXPECT_EQ(updRule->updateCounter, 1);

	system->removeUpdateRuleForEntity(updRule->getEntity());
	system->update();
	EXPECT_EQ(updRule->updateCounter, 1);
	EXPECT_NE(updRule->updateCounter, 2);
	EXPECT_NE(updRule->updateCounter, 0);
}

TEST(System, Write){
	std::string filename = TestEnvironment::getDataDir() + "Systems/WriteSystemNoHWEntities.xml";

    er_serialization::OutputArchiveXML oarchive(filename);

    er_common::System::Ptr system = er_common::System::getInstance();
	system->write(oarchive, "System");
	EXPECT_NO_THROW(oarchive.finalize());

	EXPECT_TRUE(boost::filesystem::exists(filename));

}


