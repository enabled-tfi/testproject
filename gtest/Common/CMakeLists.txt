########################################################################
# CommonTest
########################################################################




SET(COMMON_LAYER_TEST_LIBRARIES
	gtest
	gtest_main
	er_common
	er_applicationlayer
	MIR
	UR
	CameraUSBOpenCV
	CameraBaslerPylon 
	CameraSee3CAM
	udev
	LeuzeODSL9Analog
	er_programitems
	er_security
	er_environment
	er_serialization
	er_executionmodel 
	CameraSimulated
	Scanner2DSimulated
	DistanceSensor1DSimulated
	RealSenseD400
	${ROBWORK_LIBRARIES}
)

SET(COMMON_LAYER_TEST_SRC
  CommonTests.cpp
  SystemTests.cpp
  SettingsTest.cpp
  #SettingBaseTest.cpp
  #SerializationTest.cpp
  ../test-main.cpp
)

ADD_EXECUTABLE(common-gtest ${COMMON_LAYER_TEST_SRC})
TARGET_LINK_LIBRARIES(common-gtest ${COMMON_LAYER_TEST_LIBRARIES})
ADD_GTEST(common-gtest)
