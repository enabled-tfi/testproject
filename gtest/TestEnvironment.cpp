#include "TestEnvironment.hpp"

#include <rw/RobWork.hpp>
#include <rw/common/DOMElem.hpp>
#include <rw/common/DOMParser.hpp>

#include <boost/filesystem.hpp>


using rw::RobWork;
using rw::common::DOMElem;
using rw::common::DOMParser;

TestEnvironment::TestEnvironment():
	_argc(0), _argv(NULL)
{
}

TestEnvironment::~TestEnvironment() {
}

void TestEnvironment::SetUp() {
}

void TestEnvironment::TearDown(){
		// Remove written system file
	std::string filename = TestEnvironment::getDataDir() + "Systems/WriteSystemNoHWEntities.xml";
	remove(filename.c_str());

		// Remove license files
	filename = TestEnvironment::getDataDir() + "license.er";
	remove(filename.c_str());

	filename = TestEnvironment::getDataDir() + "corruptedLicense.er";
	remove(filename.c_str());


}

void TestEnvironment::init(int argc, char** argv) {
	_argc = argc;
	_argv = argv;
}

std::string TestEnvironment::getExecutableDir(){
    boost::filesystem::path path(get()->_argv[0]);
	boost::filesystem::path full_path = boost::filesystem::canonical(path);
    std::string ret = full_path.parent_path().string()+"/";
    return ret;
}

std::string TestEnvironment::getDataDir() {
	const DOMParser::Ptr parser = DOMParser::make();
	getExecutableDir();
	parser->load(getExecutableDir() + "/TestSuiteConfig.xml");
	const DOMElem::Ptr testElem = parser->getRootElement()->getChild("ERBaseTest",false);
	const DOMElem::Ptr dirElem = testElem->getChild("TestDataDIR",false);
	return dirElem->getValue()+"/";}

TestEnvironment* TestEnvironment::get() {
	static TestEnvironment* env = new TestEnvironment();
	return env;
}

TestEnvironment* TestEnvironment::make(int argc, char** argv) {
	TestEnvironment* const env = get();
	env->init(argc,argv);
	return env;
}
