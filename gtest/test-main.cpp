#include "TestEnvironment.hpp"

#include <gtest/gtest.h>

int main(int argc, char** argv) {
	::testing::InitGoogleTest(&argc, argv);
    ::testing::AddGlobalTestEnvironment(TestEnvironment::make(argc,argv));
	return RUN_ALL_TESTS();
}

