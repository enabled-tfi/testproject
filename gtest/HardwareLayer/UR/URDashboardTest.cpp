#include <iostream>

#include <HardwareLayer/UR/URDashboard.hpp>
#include <chrono>
#include <thread>

#include "gtest/gtest.h"
#include <gtest/TestEnvironment.hpp>

#define IP_ADDRESS "192.168.1.41"

TEST(URDashboard, NoConnection)
{
	hwl::URDashboard urdash(IP_ADDRESS);
	EXPECT_THROW(urdash.play(), std::exception);
}


TEST(URDashboard, Connect)
{
		//Only works if connection to robot controller can be established
	hwl::URDashboard urdash(IP_ADDRESS);
	EXPECT_TRUE(urdash.connect());
}

TEST(URDashboard, Running)
{
		//Only works if connection to robot controller can be established
	hwl::URDashboard urdash(IP_ADDRESS);
	EXPECT_TRUE(urdash.connect());
	EXPECT_FALSE(urdash.running());
}

TEST(URDashboard, Load)
{
		//Only works if connection to robot controller can be established
	hwl::URDashboard urdash(IP_ADDRESS);
	EXPECT_TRUE(urdash.connect());
	EXPECT_TRUE(urdash.load("test.urp"));
}

TEST(URDashboard, Play)
{
		//Only works if connection to robot controller can be established
	hwl::URDashboard urdash(IP_ADDRESS);
	EXPECT_TRUE(urdash.connect());
	EXPECT_FALSE(urdash.running());
	EXPECT_TRUE(urdash.play());

	std::this_thread::sleep_for(std::chrono::milliseconds(100));
    EXPECT_TRUE(urdash.running());
    EXPECT_TRUE(urdash.stop());
	std::this_thread::sleep_for(std::chrono::milliseconds(300));
	EXPECT_FALSE(urdash.running());

}

TEST(URDashboard, Pause)
{
		//Only works if connection to robot controller can be established
	hwl::URDashboard urdash(IP_ADDRESS);
	EXPECT_TRUE(urdash.connect());
	urdash.stop();

	std::this_thread::sleep_for(std::chrono::milliseconds(200));
	EXPECT_TRUE(urdash.play());
	std::this_thread::sleep_for(std::chrono::milliseconds(100));
    EXPECT_TRUE(urdash.running());
    EXPECT_TRUE(urdash.pause());
	std::this_thread::sleep_for(std::chrono::milliseconds(300));
	EXPECT_FALSE(urdash.running());
    EXPECT_TRUE(urdash.stop());
}

TEST(URDashboard, Quit)
{
		//Only works if connection to robot controller can be established
	hwl::URDashboard urdash(IP_ADDRESS);
	EXPECT_TRUE(urdash.connect());
	EXPECT_TRUE(urdash.quit());
}

TEST(URDashboard, Robotmode)
{
		//Only works if connection to robot controller can be established
	hwl::URDashboard urdash(IP_ADDRESS);
	EXPECT_TRUE(urdash.connect());
	EXPECT_EQ(urdash.robotmode(), hwl::robotMode::RUNNING);
}

TEST(URDashboard, GetLoadedProgram)
{
		//Only works if connection to robot controller can be established
	hwl::URDashboard urdash(IP_ADDRESS);
	EXPECT_TRUE(urdash.connect());
	EXPECT_TRUE(urdash.load("test.urp"));
	EXPECT_STREQ(urdash.getLoadedProgram().c_str(), "/home/ur/ursim-current/programs/test.urp");
}

TEST(URDashboard, PopUp)
{
		//Only works if connection to robot controller can be established
	hwl::URDashboard urdash(IP_ADDRESS);
	EXPECT_TRUE(urdash.connect());
	EXPECT_TRUE(urdash.popup("Test"));
}

TEST(URDashboard, ClosePopUp)
{
		//Only works if connection to robot controller can be established
	hwl::URDashboard urdash(IP_ADDRESS);
	EXPECT_TRUE(urdash.connect());
	EXPECT_TRUE(urdash.closePopup());
}

TEST(URDashboard, AddToLog)
{
		//Only works if connection to robot controller can be established
	hwl::URDashboard urdash(IP_ADDRESS);
	EXPECT_TRUE(urdash.connect());
	EXPECT_TRUE(urdash.addToLog("Test"));
}

TEST(URDashboard, IsProgramSaved)
{
		//Only works if connection to robot controller can be established
	hwl::URDashboard urdash(IP_ADDRESS);
	EXPECT_TRUE(urdash.connect());
	EXPECT_TRUE(urdash.isProgramSaved());
}

TEST(URDashboard, ProgramState)
{
		//Only works if connection to robot controller can be established
	hwl::URDashboard urdash(IP_ADDRESS);
	EXPECT_TRUE(urdash.connect());
	urdash.stop();
	urdash.load("Test");
	std::this_thread::sleep_for(std::chrono::milliseconds(300));
	EXPECT_EQ(urdash.getProgramState(), hwl::eprogramState::STOPPED);
	urdash.play();
	std::this_thread::sleep_for(std::chrono::milliseconds(300));
	EXPECT_EQ(urdash.getProgramState(), hwl::eprogramState::PLAYING);
	urdash.stop();
}

TEST(URDashboard, PolyscopeVersion)
{
		//Only works if connection to robot controller can be established
	hwl::URDashboard urdash(IP_ADDRESS);
	EXPECT_TRUE(urdash.connect());
	urdash.PolyscopeVersion();
}

TEST(URDashboard, SetOperationalMode)
{
		//Only works if connection to robot controller can be established
	hwl::URDashboard urdash(IP_ADDRESS);
	EXPECT_TRUE(urdash.connect());
	EXPECT_TRUE(urdash.setOperationalMode("AUTOMATIC"));
	EXPECT_TRUE(urdash.setOperationalMode("MANUAL"));
	EXPECT_TRUE(urdash.clearOperationalMode());
}

TEST(URDashboard, PowerOnOff)
{
		//Only works if connection to robot controller can be established
	hwl::URDashboard urdash(IP_ADDRESS);
	EXPECT_TRUE(urdash.connect());
	EXPECT_EQ(urdash.robotmode(), hwl::robotMode::RUNNING);
	urdash.powerOff();
	std::this_thread::sleep_for(std::chrono::milliseconds(150));
	EXPECT_EQ(urdash.robotmode(), hwl::robotMode::POWER_OFF);
	urdash.powerOn();
	std::this_thread::sleep_for(std::chrono::milliseconds(150));
	EXPECT_EQ(urdash.robotmode(), hwl::robotMode::IDLE);
	urdash.brakeRelease();
	std::this_thread::sleep_for(std::chrono::milliseconds(150));
	EXPECT_EQ(urdash.robotmode(), hwl::robotMode::RUNNING);
}

TEST(URDashboard, SafetyMode)
{
		//Only works if connection to robot controller can be established
	hwl::URDashboard urdash(IP_ADDRESS);
	EXPECT_TRUE(urdash.connect());
	EXPECT_EQ(urdash.safetymode(), hwl::safetyMode::NORMAL);
}


