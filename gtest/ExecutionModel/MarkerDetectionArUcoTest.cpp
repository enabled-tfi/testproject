#include <iostream>

#include <ExecutionModel/MarkerDetectionArUco.hpp>
#include "gtest/gtest.h"
#include <gtest/TestEnvironment.hpp>

#include <Serialization/InputArchiveXML.hpp>
#include <Serialization/OutputArchiveXML.hpp>
#include <Common/EntityRepository.hpp>
#include <Common/HWEntity.hpp>
#include <Common/EntityFactory.hpp>
#include <HardwareLayer/CameraUSBOpenCV/CameraUSBOpenCV.hpp>

using namespace hwl;
using namespace exm;
using namespace er_common;

using namespace pgm;
using namespace exm;
using namespace hwl;
using namespace rw::common;

TEST(MarkerDetectionArUco, TestDetection)
{
		// Disable, as it requires actual hardware to run
	return;

	ER_LOG_DEBUG("Create CameraSee3CAM Factory ");
	EntityRepository<HWEntity>::addEntityFactory("CameraUSBOpenCV", rw::common::ownedPtr(new EntityFactory<CameraUSBOpenCV>()));

	TestEnvironment::getDataDir();
	std::string filename = TestEnvironment::getDataDir() + "Systems/SystemCameraSee3CAM.system.xml";

		// Test if system is initialized - should be when getInstance is called
	er_common::System::Ptr system = er_common::System::getInstance();

	std::string systemFolder = rw::common::StringUtil::getDirectoryName(filename);
    system->setSystemFolder(systemFolder);

    er_serialization::InputArchiveXML iarchive(filename);
    iarchive.initialize();

    system->read(iarchive, "System");
	system->initialize();

	system->setMarkerImageFolder("/home/thomas/temp");
	system->setSaveImages(true);

	std::vector<HWEntity::Ptr> entities = EntityRegister::getAll<HWEntity>();

	for (HWEntity::Ptr entity : entities) {
		std::cout << entity->getName() << std::endl;
		try {
			entity->connect();
			std::cout << "connected" << std::endl;
			entity->addUpdateRule(System::getInstance());
			std::cout << "added" << std::endl;
		}
		catch (const std::exception& exp) {
			throw exp;
		}
	}


	exm::MarkerDetection::Ptr markerDetection;
	hwl::Camera::Ptr camera = er_common::EntityRegister::get<hwl::Camera>("Camera");

	camera->acquire();
	camera->acquire();
	camera->acquire();
	camera->acquire();
	markerDetection = new exm::MarkerDetectionArUco(camera, 0);
    markerDetection->getSettings().set("ArUco_MarkerSize", 0.1);
    markerDetection->getSettings().set("ArUco_MarkerId", 2);
    markerDetection->getSettings().set("ArUco_KnowledgebaseId", std::string("MarkerMonoPose"));

    exm::ExecutionManager execManager(system);

	try {
		execManager.executeBlocking(markerDetection);
	} catch (const std::exception& exp) {
	}

	std::cout << "isDetected: " << markerDetection->isDetected() << std::endl;
}

int main(int argc, char** argv) {
	::testing::InitGoogleTest(&argc, argv);
    ::testing::AddGlobalTestEnvironment(TestEnvironment::make(argc,argv));
	return RUN_ALL_TESTS();
}
