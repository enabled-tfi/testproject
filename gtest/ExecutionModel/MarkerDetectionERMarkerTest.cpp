#include <iostream>

#include <ExecutionModel/MarkerDetectionERMarker.hpp>
#include <ExecutionModel/ERMarkerDetector.hpp>
#include "gtest/gtest.h"
#include <gtest/TestEnvironment.hpp>

#include <Serialization/InputArchiveXML.hpp>
#include <Serialization/OutputArchiveXML.hpp>
#include <Common/EntityRepository.hpp>
#include <Common/HWEntity.hpp>
#include <Common/EntityFactory.hpp>
#include <HardwareLayer/CameraSimulated/StaticImageCamera.hpp>
#include <HardwareLayer/CameraSee3CAM/CameraSee3CAM.hpp>

#include "boost/filesystem.hpp"

using namespace hwl;
using namespace exm;
using namespace er_common;

using namespace pgm;
using namespace exm;
using namespace hwl;
using namespace rw::common;

using namespace cv;

using namespace std;

using namespace boost::filesystem;

		// Used to debug a single image
TEST(MarkerDetectionERMarker, TestSingle)
{
return;
	rw::common::Log::log().setLogIndexMask(rw::common::Log::AllMask);
	EntityRepository<HWEntity>::addEntityFactory("StaticImageCamera", rw::common::ownedPtr(new EntityFactory<StaticImageCamera>()));
	std::string filename = TestEnvironment::getDataDir() + "Systems/SystemStaticCamera.xml";
	er_common::System::Ptr system = er_common::System::getInstance();
	std::string systemFolder = rw::common::StringUtil::getDirectoryName(filename);
    system->setSystemFolder(systemFolder);
    er_serialization::InputArchiveXML iarchive(filename);
    iarchive.initialize();
    system->read(iarchive, "System");
	system->initialize();
	system->setMarkerImageFolder(TestEnvironment::getDataDir() + "output");
	std::cout << TestEnvironment::getDataDir() + "output" << std::endl;
	system->setSaveImages(true);
	std::vector<HWEntity::Ptr> entities = EntityRegister::getAll<HWEntity>();
	for (HWEntity::Ptr entity : entities) {
		ER_LOG_ERROR("Entity name connecting: " << entity->getName());
		try {
			entity->connect();
			entity->addUpdateRule(System::getInstance());
		}
		catch (const std::exception& exp) {
			throw exp;
		}
	}

	StaticImageCamera::Ptr camSim = EntityRegister::get<StaticImageCamera>("Camera");
	exm::MarkerDetectionERMarker::Ptr markerDetection;
	hwl::Camera::Ptr camera = er_common::EntityRegister::get<hwl::Camera>("Camera");
	markerDetection = new exm::MarkerDetectionERMarker(camera, 0);
    markerDetection->getSettings().set("ArUco_SquareSize", 0.005);
    markerDetection->getSettings().set("ERMarker_ID", 0);
    markerDetection->getSettings().set("Chessboard_SquareSize", 0.01);

    markerDetection->getSettings().set("Chessboard_Rows", 7);
    markerDetection->getSettings().set("Chessboard_Columns", 6);

    markerDetection->getSettings().set("ArUco_KnowledgebaseId", std::string("MarkerMonoPose"));

    exm::ExecutionManager execManager(system);

	std::string filenameImg = TestEnvironment::getDataDir() + "Images/ERMarker/Visible/New/2019-05-09-084727.jpg";

	int success = 0;
	int fail  = 0;

    std::chrono::time_point<std::chrono::system_clock> start, end;

    start = std::chrono::system_clock::now();

	// cycle through the directory
		// If it's not a directory, list it. If you want to list directories too, just remove this check.
			// assign current file name to current_file and echo it out to the console.

	camSim->setImage(cv::imread(filenameImg));

	markerDetection->getSettings().set("ERMarker_ID", 0);
	execManager.executeBlocking(markerDetection);

	EXPECT_TRUE(markerDetection->isDetected()) << "Failed with image: " << filenameImg <<  ", id: " << 0;

		//
		//    	if(markerDetection->isDetected()){
		//    		imshow("Gtest",markerDetection->getDetectionImage());
		//    		waitKey(0);
		//    	}


    end = std::chrono::system_clock::now();

    double elapsed_seconds = std::chrono::duration_cast<std::chrono::milliseconds>
                             (end-start).count()  / 1000;
    std::cout << "elapsed time: " << elapsed_seconds << "s\n";






}






//TEST(MarkerDetectionERMarker, TestDetection)
//{
//	rw::common::Log::log().setLogIndexMask(rw::common::Log::AllMask);
//
//
//	std::string filename = TestEnvironment::getDataDir() + "Images/ERMarker/Visible/New";
//
//    path p (filename);
//
//    directory_iterator end_itr;
//
//    // cycle through the directory
//    for (directory_iterator itr(p); itr != end_itr; ++itr)
//    {
//        // If it's not a directory, list it. If you want to list directories too, just remove this check.
//        if (is_regular_file(itr->path())) {
//            // assign current file name to current_file and echo it out to the console.
//
//            string current_file = itr->path().string();
//
//
//        	cv::Mat img = cv::imread(current_file);
//
////        	cv::resize(img, img, cv::Size(), 0.5, 0.5);
//
//        	Size pattern_size(7,6);
//        	std::vector<cv::Point2f> points;
//
//        	ERMarkerDetector chbDet(pattern_size);
//        	bool found = chbDet.findErMarkerCorners(img, pattern_size, points,CALIB_CB_ADAPTIVE_THRESH + CALIB_CB_NORMALIZE_IMAGE);
////			cornerSubPix(img, points, Size(11, 11), Size(-1, -1), TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
//
//        	EXPECT_TRUE(found);
//
//
//        }
//    }
//}
//
//TEST(MarkerDetectionERMarker, TestLiveImage)
//{
//	return;
//
//	rw::common::Log::log().setLogIndexMask(rw::common::Log::AllMask);
//
////	EntityRepository<HWEntity>::addEntityFactory("StaticImageCamera", rw::common::ownedPtr(new EntityFactory<StaticImageCamera>()));
//	EntityRepository<HWEntity>::addEntityFactory("CameraSee3CAM", rw::common::ownedPtr(new EntityFactory<CameraSee3CAM>()));
//
//	TestEnvironment::getDataDir();
//	std::string filename = TestEnvironment::getDataDir() + "Systems/SystemCameraSee3CAM.system.xml";
//
//	er_common::System::Ptr system = er_common::System::getInstance();
//
//	std::string systemFolder = rw::common::StringUtil::getDirectoryName(filename);
//    system->setSystemFolder(systemFolder);
//
//    er_serialization::InputArchiveXML iarchive(filename);
//    iarchive.initialize();
//
//    system->read(iarchive, "System");
//	system->initialize();
//
//	system->setMarkerImageFolder("/home/thomas/temp");
//	system->setSaveImages(true);
//
//	std::vector<HWEntity::Ptr> entities = EntityRegister::getAll<HWEntity>();
//
//	for (HWEntity::Ptr entity : entities) {
//		std::cout << entity->getName() << std::endl;
//		try {
//			entity->connect();
//			std::cout << "connected" << std::endl;
//			entity->addUpdateRule(System::getInstance());
//			std::cout << "added" << std::endl;
//
//		}
//		catch (const std::exception& exp) {
//			throw exp;
//		}
//	}
//	exm::MarkerDetection::Ptr markerDetection;
//	hwl::Camera::Ptr camera = er_common::EntityRegister::get<hwl::Camera>("Camera");
//	markerDetection = new exm::MarkerDetectionERMarker(camera, 0);
//    markerDetection->getSettings().set("ArUco_MarkerSize", 0.1);
//    markerDetection->getSettings().set("ArUco_MarkerId", 9);
//    markerDetection->getSettings().set("ArUco_KnowledgebaseId", std::string("MarkerMonoPose"));
//
//
//    exm::ExecutionManager execManager(system);
//
//	try {
//		execManager.execute(markerDetection);
//	} catch (const std::exception& exp) {
//	}
//
//
//	std::cout << "isDetected: " << markerDetection->isDetected() << std::endl;
//
//	if(markerDetection->isDetected()){
//		imshow("Gtest",markerDetection->getDetectionImage());
//		waitKey(0);
//	}
//
//}


TEST(MarkerDetectionERMarker, FullTest)
{

	rw::common::Log::log().setLogIndexMask(rw::common::Log::CriticalMask);
	EntityRepository<HWEntity>::addEntityFactory("StaticImageCamera", rw::common::ownedPtr(new EntityFactory<StaticImageCamera>()));
	std::string filename = TestEnvironment::getDataDir() + "Systems/SystemStaticCamera.xml";
	er_common::System::Ptr system = er_common::System::getInstance();
	std::string systemFolder = rw::common::StringUtil::getDirectoryName(filename);
    system->setSystemFolder(systemFolder);
    er_serialization::InputArchiveXML iarchive(filename);
    iarchive.initialize();
    system->read(iarchive, "System");
	system->initialize();
	system->setMarkerImageFolder(TestEnvironment::getDataDir() + "output");
	std::cout << TestEnvironment::getDataDir() + "output" << std::endl;
	system->setSaveImages(true);
	std::vector<HWEntity::Ptr> entities = EntityRegister::getAll<HWEntity>();
	for (HWEntity::Ptr entity : entities) {
		ER_LOG_ERROR("Entity name connecting: " << entity->getName());
		try {
			entity->connect();
			entity->addUpdateRule(System::getInstance());
		}
		catch (const std::exception& exp) {
			throw exp;
		}
	}

	StaticImageCamera::Ptr camSim = EntityRegister::get<StaticImageCamera>("Camera");

	exm::MarkerDetectionERMarker::Ptr markerDetection;
	hwl::Camera::Ptr camera = er_common::EntityRegister::get<hwl::Camera>("Camera");
	markerDetection = new exm::MarkerDetectionERMarker(camera, 0);
    markerDetection->getSettings().set("ArUco_SquareSize", 0.005);
    markerDetection->getSettings().set("ERMarker_ID", 0);
    markerDetection->getSettings().set("Chessboard_SquareSize", 0.01);

    markerDetection->getSettings().set("Chessboard_Rows", 7);
    markerDetection->getSettings().set("Chessboard_Columns", 6);

    markerDetection->getSettings().set("ArUco_KnowledgebaseId", std::string("MarkerMonoPose"));

    exm::ExecutionManager execManager(system);

	std::string filenameImg = TestEnvironment::getDataDir() + "Images/ERMarker/Visible/New";

	path p (filenameImg);

	directory_iterator end_itr;

	int success = 0;
	int fail  = 0;

    std::chrono::time_point<std::chrono::system_clock> start, end;

    start = std::chrono::system_clock::now();


	// cycle through the directory
	for (directory_iterator itr(p); itr != end_itr; ++itr)
	{
		// If it's not a directory, list it. If you want to list directories too, just remove this check.
		if (is_regular_file(itr->path())) {
			// assign current file name to current_file and echo it out to the console.

			string current_file = itr->path().string();
			camSim->setImage(cv::imread(current_file));

		    for(int i = 0; i < 6; i++){
		        markerDetection->getSettings().set("ERMarker_ID", i);
		        execManager.executeBlocking(markerDetection);

		        EXPECT_TRUE(markerDetection->isDetected()) << "Failed with image: " << current_file <<  ", id: " << i;

		        if(markerDetection->isDetected())
		        	success++;
		        else
		        	fail++;

		//
		//    	if(markerDetection->isDetected()){
		//    		imshow("Gtest",markerDetection->getDetectionImage());
		//    		waitKey(0);
		//    	}
		    }


		}
	}

	std::cout << "Successes: " << success << std::endl;
	std::cout << "Fails: " << fail << std::endl;
	std::cout << "Success percentage: " << (double) success / (success+fail);

    end = std::chrono::system_clock::now();

    double elapsed_seconds = std::chrono::duration_cast<std::chrono::milliseconds>
                             (end-start).count()  / 1000;
    std::cout << "elapsed time: " << elapsed_seconds << "s\n";

    std::cout << "Total duration: " <<  (double)elapsed_seconds << std::endl;
    std::cout << "Duration per image: " <<  (double)elapsed_seconds/(success+fail) << std::endl;


	//    markerDetection->getSettings().set("ERMarker_ID", 6);
//    execManager.execute(markerDetection);
//
//    EXPECT_FALSE(markerDetection->isDetected());
//
//    markerDetection->getSettings().set("ERMarker_ID", 7);
//    execManager.execute(markerDetection);
//
//    EXPECT_FALSE(markerDetection->isDetected());
}



