#include <iostream>

#include <ExecutionModel/MarkerDetectionChessboard.hpp>
#include "gtest/gtest.h"
#include <gtest/TestEnvironment.hpp>

#include <Serialization/InputArchiveXML.hpp>
#include <Serialization/OutputArchiveXML.hpp>
#include <Common/EntityRepository.hpp>
#include <Common/HWEntity.hpp>
#include <Common/EntityFactory.hpp>
#include <HardwareLayer/CameraSimulated/StaticImageCamera.hpp>
#include <HardwareLayer/CameraSee3CAM/CameraSee3CAM.hpp>

#include "boost/filesystem.hpp"

using namespace hwl;
using namespace exm;
using namespace er_common;

using namespace pgm;
using namespace exm;
using namespace hwl;
using namespace rw::common;

using namespace cv;

using namespace std;

using namespace boost::filesystem;


TEST(MarkerDetectionChessboard, FullTest)
{

	rw::common::Log::log().setLogIndexMask(rw::common::Log::CriticalMask);
	EntityRepository<HWEntity>::addEntityFactory("StaticImageCamera", rw::common::ownedPtr(new EntityFactory<StaticImageCamera>()));
	std::string filename = TestEnvironment::getDataDir() + "Systems/SystemStaticCamera.xml";
	er_common::System::Ptr system = er_common::System::getInstance();
	std::string systemFolder = rw::common::StringUtil::getDirectoryName(filename);
    system->setSystemFolder(systemFolder);
    er_serialization::InputArchiveXML iarchive(filename);
    iarchive.initialize();
    system->read(iarchive, "System");
	system->initialize();
	system->setMarkerImageFolder(TestEnvironment::getDataDir() + "output");
	std::cout << TestEnvironment::getDataDir() + "output" << std::endl;
	system->setSaveImages(true);
	std::vector<HWEntity::Ptr> entities = EntityRegister::getAll<HWEntity>();
	for (HWEntity::Ptr entity : entities) {
		ER_LOG_ERROR("Entity name connecting: " << entity->getName());
		try {
			entity->connect();
			entity->addUpdateRule(System::getInstance());
		}
		catch (const std::exception& exp) {
			throw exp;
		}
	}

	StaticImageCamera::Ptr camSim = EntityRegister::get<StaticImageCamera>("Camera");

	exm::MarkerDetectionChessboard::Ptr markerDetection;
	hwl::Camera::Ptr camera = er_common::EntityRegister::get<hwl::Camera>("Camera");
	markerDetection = new exm::MarkerDetectionChessboard(camera, 0);
    markerDetection->getSettings().set("Chessboard_SquareSize", 0.01);
    markerDetection->getSettings().set("Chessboard_Rows", 6);
    markerDetection->getSettings().set("Chessboard_Columns", 5);

    exm::ExecutionManager execManager(system);

	std::string filenameImg = TestEnvironment::getDataDir() + "Images/Chessboard/Visible";

	path p (filenameImg);

	directory_iterator end_itr;

	int success = 0;
	int fail  = 0;

    std::chrono::time_point<std::chrono::system_clock> start, end;

    start = std::chrono::system_clock::now();

	// cycle through the directory
	for (directory_iterator itr(p); itr != end_itr; ++itr)
	{
		// If it's not a directory, list it. If you want to list directories too, just remove this check.
		if (is_regular_file(itr->path())) {
			// assign current file name to current_file and echo it out to the console.

			string current_file = itr->path().string();
			camSim->setImage(cv::imread(current_file));

			execManager.executeBlocking(markerDetection);

			EXPECT_TRUE(markerDetection->isDetected()) << "Failed with image: " << current_file;

			if(markerDetection->isDetected())
				success++;
			else
				fail++;
		}
	}

    end = std::chrono::system_clock::now();

    double elapsed_seconds = std::chrono::duration_cast<std::chrono::milliseconds>
                             (end-start).count()  / 1000;
    std::cout << "elapsed time: " << elapsed_seconds << "s\n";


	std::cout << "Successes: " << success << std::endl;
	std::cout << "Fails: " << fail << std::endl;
	std::cout << "Success percentage: " << (double) success / (success+fail) << std::endl;;


    std::cout << "Total duration: " <<  (double)elapsed_seconds << std::endl;
    std::cout << "Duration per image: " <<  (double)elapsed_seconds/(success+fail) << std::endl;


	//    markerDetection->getSettings().set("ERMarker_ID", 6);
//    execManager.execute(markerDetection);
//
//    EXPECT_FALSE(markerDetection->isDetected());
//
//    markerDetection->getSettings().set("ERMarker_ID", 7);
//    execManager.execute(markerDetection);
//
//    EXPECT_FALSE(markerDetection->isDetected());
}



