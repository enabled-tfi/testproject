#include "WebSocketProtocol.hpp"

#include "../HardwareLayer/SerialDevice.hpp"

using namespace wss;
using namespace swm;
using namespace hwl;
using namespace syscomp;
using namespace rw::math;
using namespace rw::common;


class ManipulatorMoveQRelative: public WebSocketProtocol::Command
{
public:
    ManipulatorMoveQRelative(SerialDevice::Ptr device):
        Command("MoveDeltaQ"),
        _device(device)
    {}

    void parseAndExecute(std::istream& istream)
    {
        unsigned int index = 0;
        istream >> index;
        int direction = 0;
        istream >> direction;
        Q qtarget = _device->getQ();
        ER_LOG_DEBUG("Target Size = "<<qtarget);
        try {

            if (index >= qtarget.size())
                RW_THROW("Invalid joint index "<<index);
        }
        catch (const std::exception& exp) {
            ER_LOG_DEBUG("Got Exception: "<<exp.what());
            qtarget = Q::zero(6);
        }

        const double delta = 0.05;
        qtarget(index) += delta*direction;

        std::pair<rw::math::Q, rw::math::Q> bounds = _device->getBounds();
        qtarget = Math::clampQ(qtarget, bounds);
        ER_LOG_DEBUG("Move to Target "<<qtarget);
        _device->ptp(qtarget);
    }
private:
    SerialDevice::Ptr _device;
};

class GetPrograms: public WebSocketProtocol::Command
{
public:
    GetPrograms(std::queue<std::string>& messageQueue, ProgramManager::Ptr programManager):
        Command("GetPrograms"),
        _messageQueue(messageQueue),
        _programManager(programManager)
    {
        ER_LOG_DEBUG("GetPrograms::GetPrograms Program Manager "<<_programManager);
    }

    void parseAndExecute(std::istream& istream)
    {
        ER_LOG_DEBUG("GetPrograms::parseAndExecute Program Manager "<<_programManager);
        ER_LOG_DEBUG("Add message to queue");
        std::vector<std::string> programs = _programManager->getPrograms();
        BOOST_FOREACH(const std::string& program, programs) {
            _messageQueue.push(program);
        }
    }
private:
    std::queue<std::string>& _messageQueue;
    ProgramManager::Ptr _programManager;
};

class RunProgram: public WebSocketProtocol::Command
{
public:
    RunProgram(ProgramManager::Ptr programManager):
        Command("RunProgram"),
        _programManager(programManager)
    {
        ER_LOG_DEBUG("RunProgram::RunProgram Program Manager "<<_programManager);
    }

    void parseAndExecute(std::istream& istream)
    {
        std::string program;
        istream >> program;
        ER_LOG_DEBUG("Run Program: "<<program<<" "<<_programManager);
        try {
            _programManager->runProgram(program);
        } catch (const std::exception& exp) {
            ER_LOG_DEBUG("Exception: "<<exp.what());
        }
    }
private:
    ProgramManager::Ptr _programManager;
};


WebSocketProtocol::WebSocketProtocol(swm::EntityRegister::Ptr entityRegister, ProgramManager::Ptr programManager):
    _programManager(programManager)
{
    ER_LOG_DEBUG("WS::WS Program Manager "<<_programManager);
    try {
        SerialDevice::Ptr manipulator = EntityRegister::get<SerialDevice::Ptr>("Manipulator");
        if (manipulator != NULL) {
            Command::Ptr cmd = ownedPtr(new ManipulatorMoveQRelative(manipulator));
            ER_LOG_DEBUG("Add cmd with id = "<<cmd->cmdId);
            _commandMap[cmd->cmdId] = cmd;
        }
    } catch (const std::exception& exp) {
        RW_WARN("Exception: "<<exp.what());
    }

    Command::Ptr cmdGetPrograms = ownedPtr(new GetPrograms(_messageQueue, programManager));
    _commandMap[cmdGetPrograms->cmdId] = cmdGetPrograms;

    Command::Ptr cmdRunProgram = ownedPtr(new RunProgram(programManager));
    _commandMap[cmdRunProgram->cmdId] = cmdRunProgram;

}


void WebSocketProtocol::handle(const std::string& msg)
{
    ER_LOG_DEBUG("Got message: "<<msg);
    std::istringstream istream(msg);
    std::string command;
    istream >> command;
    ER_LOG_DEBUG("Command = "<<command);
    if (_commandMap.find(command) != _commandMap.end()) {
        ER_LOG_DEBUG("Found Command");
        _commandMap[command]->parseAndExecute(istream);
    } else {
        ER_LOG_DEBUG("Command not supported");
    }
}


bool WebSocketProtocol::hasMessages() const
{
    return _messageQueue.empty() == false;
}

std::string WebSocketProtocol::nextMessage()
{
    std::string msg = _messageQueue.front();
    _messageQueue.pop();
    return msg;
}
