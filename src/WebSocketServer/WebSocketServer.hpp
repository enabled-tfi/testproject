#ifndef WSS_WEBSOCKETSERVER_HPP
#define WSS_WEBSOCKETSERVER_HPP

#include "WebSocketProtocol.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <libwebsockets.h>

#include <iostream>
#include <string>

//#define LWS_DEBUG

void sighandler(int);

namespace wss {

class WebSocketServer {
public:
    WebSocketServer(WebSocketProtocol::Ptr protocol);

    void init_server();
    void step_server();
    void close_server();



    void run_server();

    bool isRunning() const;




    static WebSocketProtocol::Ptr Protocol();

private:
    /// Member functions
    bool _isRunning;
    static WebSocketProtocol::Ptr _protocol;



    // Call back functions
    static int callback_http(struct lws*, enum lws_callback_reasons, void*, void*, size_t);
    static int callback_manipulator_control(struct lws*, enum lws_callback_reasons, void*, void*, size_t);
    static int callback_run_program(struct lws*, enum lws_callback_reasons, void*, void*, size_t);


    /// Data
    struct lws_context *context;


    struct per_session_data__dumb_increment{
        int number;

    };
    struct per_session_data__http {
        lws_filefd_type fd;
        unsigned int client_finished:1;
    };

    enum protocols_i {
        PROTOCOL_HTTP = 0,          // Allways first
        MANIPULATOR_CONTROL_PROTOCOL,
        PROTOCOL_COUNT              // Allways last
    };

    struct lws_protocols protocols[3] {
        /* first protocol must always be HTTP handler */
        {
            "http-only",   // name
            callback_http, // callback
            sizeof(struct per_session_data__http),
            0              // per_session_data_size
        },
        {
            "manipulator_control_protocol", // protocol name - very important!
            callback_manipulator_control,   // callback
            sizeof(struct per_session_data__dumb_increment),
            256
        },
      /*  {
            "run_program_protocol", // protocol name - very important!
            callback_run_program,   // callback
            sizeof(struct per_session_data__dumb_increment),
            20
        },*/
        { NULL, NULL, 0,0 }   /* terminator */
    };
};

}//end namespace

#endif //_WEBSOCKETSERVER_H
