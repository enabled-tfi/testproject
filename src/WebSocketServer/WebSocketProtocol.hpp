#ifndef WSS_WEBSOCKETPROTOCOL_HPP
#define WSS_WEBSOCKETPROTOCOL_HPP

#include "../SoftwareModel/EntityRegister.hpp"
#include "../SystemComponents/ProgramManager.hpp"

#include <string>
#include <queue>

namespace wss
{


class WebSocketProtocol {
public:

    typedef rw::common::Ptr<WebSocketProtocol> Ptr;


    WebSocketProtocol(swm::EntityRegister::Ptr entityRegister, syscomp::ProgramManager::Ptr programManager);

    void handle(const std::string& msg);

    bool hasMessages() const;

    std::string nextMessage();


    class Command {
    public:
        typedef rw::common::Ptr<Command> Ptr;
        std::string cmdId;

        Command(const std::string id):
            cmdId(id)
        {}

        virtual void parseAndExecute(std::istream& instream) = 0;
    };


private:
    swm::EntityRegister::Ptr _entityRegister;
    syscomp::ProgramManager::Ptr _programManager;

    std::map<std::string, Command::Ptr> _commandMap;
    std::queue<std::string> _messageQueue;
};

}


#endif //WSS_WEBSOCKETPROTOCOL_HPP
