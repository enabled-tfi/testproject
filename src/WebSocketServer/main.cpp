#include "WebSocketServer.hpp"

#include "../HardwareLayer/System.hpp"
#include "../HardwareLayer/MobileDevice.hpp"
#include "../HardwareLayer/MobileDeviceSimulated.hpp"
#include "../HardwareLayer/SerialDevice.hpp"
#include "../HardwareLayer/SerialDeviceSimulated.hpp"


#include <rw/loaders/WorkCellLoader.hpp>

using namespace swm;
using namespace hwl;
using namespace syscomp;
using namespace serialization;
using namespace rw::math;
using namespace rw::loaders;
using namespace rw::models;
using namespace rw::kinematics;

int main(int argc, char** argv){
    if (argc < 2) {
        ER_LOG_DEBUG("Missing WorkCell File");
        return -1;
    }
    ER_LOG_DEBUG("Trying to load work cell file: "<<argv[1]);
    WorkCell::Ptr workcell = WorkCellLoader::Factory::load(argv[1]);


    MovableFrame* mobileDeviceFrame = workcell->findFrame<MovableFrame>("MobileDevice");
    if (mobileDeviceFrame == NULL) {
        ER_LOG_DEBUG("No MovableFrame found for the Mobile Device");
        return -1;
    }

    System::Ptr system = rw::common::ownedPtr(new System(workcell->getDefaultState()));
    hwl::MobileDevice::Ptr mobileDevice = rw::common::ownedPtr(new MobileDeviceSimulated(mobileDeviceFrame, system, "MIR"));
    EntityRegister::add(mobileDevice->getIdentifier(), mobileDevice);

    Device::Ptr ur10 = workcell->findDevice("UR10");
    if (ur10 == NULL) {
        ER_LOG_DEBUG("No Device for UR10");
        return -1;
    }
    hwl::SerialDevice::Ptr serialDevice = rw::common::ownedPtr(new SerialDeviceSimulated(ur10, system, "Manipulator"));
    EntityRegister::add(serialDevice->getIdentifier(), serialDevice);


    ProgramManager programManager;
    programManager.addFilesInFolder("/home/lpe/mms_programs/");

    wss::WebSocketProtocol protocol(NULL, &programManager);
    wss::WebSocketServer server(&protocol);
    server.run_server();
    return 0;
}
