#include "WebSocketServer.hpp"

using namespace wss;

//#define LOCAL_RESOURCE_PATH "/home/nicolai/Documents/MiniPicker/code/di-cpp-test-server/src"
//char *resource_path = LOCAL_RESOURCE_PATH;
//ER_LOG_DEBUG(std::string((const char*)resource_path,strlen((const char *)resource_path)) );

volatile int force_exit = 0;

WebSocketProtocol::Ptr WebSocketServer::_protocol = NULL;


void sighandler(int sig){
    force_exit = 1;
    //lws_cancel_service(context);
}

WebSocketServer::WebSocketServer(WebSocketProtocol::Ptr protocol) {
    _isRunning = false;
    _protocol = protocol;
    // server url will be http://localhost:7681 (9000 "default")
    struct lws_context_creation_info info;
    memset(&info, 0, sizeof info);
    info.port = 7681;
    info.iface = NULL;
    info.protocols = protocols;
    info.ssl_cert_filepath = NULL;
    info.ssl_private_key_filepath = NULL;
    info.ssl_ca_filepath = NULL;
    info.gid = -1;
    info.uid = -1;
    info.max_http_header_pool = 16;
    //info.options = 0 | LWS_SERVER_OPTION_VALIDATE_UTF8;
    info.extensions = NULL;
    info.timeout_secs = 5;
    info.ssl_cipher_list = "ECDHE-ECDSA-AES256-GCM-SHA384:"
                   "ECDHE-RSA-AES256-GCM-SHA384:"
                   "DHE-RSA-AES256-GCM-SHA384:"
                   "ECDHE-RSA-AES256-SHA384:"
                   "HIGH:!aNULL:!eNULL:!EXPORT:"
                   "!DES:!MD5:!PSK:!RC4:!HMAC_SHA1:"
                   "!SHA1:!DHE-RSA-AES128-GCM-SHA256:"
                   "!DHE-RSA-AES128-SHA256:"
                   "!AES128-GCM-SHA256:"
                   "!AES128-SHA256:"
                   "!DHE-RSA-AES256-SHA256:"
                   "!AES256-GCM-SHA384:"
                   "!AES256-SHA256";


    // create libwebsocket context representing this server
    context = lws_create_context(&info);

    if (context == NULL) {
        lwsl_err("libwebsocket init failed\n");
        exit(-1);
    }

    lwsl_notice("Starting server...\n");

}

WebSocketProtocol::Ptr WebSocketServer::Protocol() {
    return _protocol;
}

bool WebSocketServer::isRunning() const
{
    return _isRunning;
}

namespace {
const char * get_mimetype(const char *file)
{
    int n = strlen(file);

    if (n < 5)
        return NULL;

    if (!strcmp(&file[n - 4], ".ico"))
        return "image/x-icon";

    if (!strcmp(&file[n - 4], ".png"))
        return "image/png";

    if (!strcmp(&file[n - 5], ".html"))
        return "text/html";

    return NULL;
}

}

int WebSocketServer::callback_http(struct lws *wsi,
                         enum lws_callback_reasons reason, void *user,
                         void *in, size_t len)
{
    char buf[256];
    const char *mimetype;
    int n;

    switch (reason) {
        case LWS_CALLBACK_HTTP:{
            char name[100], rip[50];
            lws_get_peer_addresses(wsi, lws_get_socket_fd(wsi), name, sizeof(name), rip, sizeof(rip));
            sprintf(buf, "%s (%s)", name, rip);
            lwsl_notice("HTTP connect from %s\n", buf);

            // Serve file
            strcpy(buf, ".");
            strcat(buf, "/index.html");
            buf[sizeof(buf) - 1] = '\0';
            mimetype = get_mimetype(buf);

            n = lws_serve_http_file(wsi, buf, mimetype, NULL, 0);

            if (n < 0 || ((n > 0) && lws_http_transaction_completed(wsi))){
                return -1; // error or can't reuse connection: close the socket
            }else{
                // Success (do nothing)
            }
            break;
        }
        case LWS_CALLBACK_HTTP_BODY:
            strncpy(buf, (const char*)in, 50);
            buf[50] = '\0';
            if (len < 50){
                buf[len] = '\0';
            }

            lwsl_notice("LWS_CALLBACK_HTTP_BODY: %s... len %d\n",(const char *)buf, (int)len);
            break;
        case LWS_CALLBACK_HTTP_BODY_COMPLETION:
            lwsl_notice("LWS_CALLBACK_HTTP_BODY_COMPLETION\n");
            lws_return_http_status(wsi, HTTP_STATUS_OK, NULL);
            break;
        case LWS_CALLBACK_HTTP_FILE_COMPLETION:
            lwsl_info("LWS_CALLBACK_HTTP_FILE_COMPLETION\n");
            break;
        case LWS_CALLBACK_HTTP_WRITEABLE:
            lwsl_info("LWS_CALLBACK_HTTP_WRITEABLE\n");
            break;
        case LWS_CALLBACK_ESTABLISHED_CLIENT_HTTP:
            break;
        case LWS_CALLBACK_CLOSED_HTTP:
            break;
        case LWS_CALLBACK_RECEIVE_CLIENT_HTTP:
            break;
        case LWS_CALLBACK_COMPLETED_CLIENT_HTTP:
            break;
        default:
            break;
    }

    return 0;
}

int WebSocketServer::callback_manipulator_control(struct lws *wsi,
                                   enum lws_callback_reasons reason,
                                   void *user, void *in, size_t len)
{
    unsigned char buf[LWS_PRE + 512];
    //struct per_session_data__dumb_increment *pss = (struct per_session_data__dumb_increment *)user;
    unsigned char *p = &buf[LWS_PRE];
    int n,m;
    ER_LOG_DEBUG("WebSocketServer::callback_manipulator_control "<<reason);
    switch (reason) {
        case LWS_CALLBACK_ESTABLISHED:
            // log message that someone is connecting
            lwsl_notice(" Connection established\n");
            // Reset counter:
            //pss->number = 0;
            break;
        case LWS_CALLBACK_SERVER_WRITEABLE:
            ER_LOG_DEBUG("Server Writable");
            #ifdef LWS_DEBUG
                lwsl_notice("[Main Service]: Called -> LWS_CALLBACK_SERVER_WRITEABLE\n");
            #endif
            if (WebSocketServer::Protocol()->hasMessages()) {
                n = sprintf((char*)p,"%s\n", WebSocketServer::Protocol()->nextMessage().c_str());
                ER_LOG_DEBUG("Send Message: "<<p);
                //n = sprintf((char*)p,"%d",pss->number++);
                m = lws_write(wsi,p,n,LWS_WRITE_TEXT);
                lws_callback_on_writable(wsi);
            } /*else {
                n = sprintf((char*)p,"No Messages");
                m = lws_write(wsi,p,n,LWS_WRITE_TEXT);
            }*/

            #ifdef LWS_DEBUG
                lwsl_notice("n = %d, m = %d\n",n,m);
            #endif

            if(m < n){
                lwsl_err("Error %d writing to (DI) socket\n",n);
            }else{
                // Success
            }
            break;
        case LWS_CALLBACK_RECEIVE:{
            // Copy const char * to std::string

            std::string input((const char*)in,strlen((const char *)in));
            ER_LOG_DEBUG("[Received] " << input );
            WebSocketServer::Protocol()->handle(input);
            //if(input == "reset\n")
              //  pss->number = 0;
            lws_callback_on_writable(wsi);
            /*if (WebSocketServer::Protocol()->hasMessages()) {
                  ER_LOG_DEBUG("Send Message ");
                  n = sprintf((char*)p,"%s\n", WebSocketServer::Protocol()->nextMessage().c_str());
                  ER_LOG_DEBUG("Msg = "<<p);
                  //n = sprintf((char*)p,"%d",pss->number++);
                  m = lws_write(wsi,p,n,LWS_WRITE_TEXT);
              }
*/
            break;
        }
        default:
            break;
    }
  /* else {
        ER_LOG_DEBUG("No Messages ");
        n = sprintf((char*)p,"Still Messages");
        m = lws_write(wsi,p,n,LWS_WRITE_TEXT);
    }
*/
    return 0;
}

/*
int WebSocketServer::callback_run_program(struct lws* wsi, enum lws_callback_reasons reason, void* user, void* in, size_t len) {
    ER_LOG_DEBUG("CallBackRunProgram");
    unsigned char buf[LWS_PRE + 512];
    //struct per_session_data__dumb_increment *pss = (struct per_session_data__dumb_increment *)user;
    unsigned char *p = &buf[LWS_PRE];
    int n,m;
    ER_LOG_DEBUG("WebSocketServer::callback_run_program");
    switch (reason) {
        case LWS_CALLBACK_ESTABLISHED:
            lwsl_notice(" Connection established\n");
            break;
        case LWS_CALLBACK_SERVER_WRITEABLE:
            #ifdef LWS_DEBUG
                lwsl_notice("[Main Service]: Called -> LWS_CALLBACK_SERVER_WRITEABLE\n");
            #endif
            ER_LOG_DEBUG("Looking for messages: "<<WebSocketServer::Protocol()->hasMessages());
            if (WebSocketServer::Protocol()->hasMessages()) {
                while (WebSocketServer::Protocol()->hasMessages()) {
                    n = sprintf((char*)p,"%s", WebSocketServer::Protocol()->nextMessage().c_str());
                    ER_LOG_DEBUG("Sends data:"<<p);
                    //n = sprintf((char*)p,"%d",pss->number++);
                    m = lws_write(wsi,p,n,LWS_WRITE_TEXT);
                }
            } else {
                n = sprintf((char*)p,"No Messages");
                m = lws_write(wsi,p,n,LWS_WRITE_TEXT);
            }

            #ifdef LWS_DEBUG
                lwsl_notice("n = %d, m = %d\n",n,m);
            #endif

            if(m < n){
                lwsl_err("Error %d writing to (DI) socket\n",n);
            }else{
                // Success
            }
            break;
        case LWS_CALLBACK_RECEIVE:{
            // Copy const char * to std::string
            std::string input((const char*)in,strlen((const char *)in));
            WebSocketServer::Protocol()->handle(input);
            //if(input == "reset\n")
              //  pss->number = 0;

            ER_LOG_DEBUG("[Received] " << input );
            break;
        }
        default:
            break;
    }
    //while (WebSocketServer::Protocol()->hasMessages()) {
    //    n = sprintf((char*)p,"Got new message: %s", WebSocketServer::Protocol()->nextMessage().c_str());
    //    ER_LOG_DEBUG("Sends data:"<<p);
        //n = sprintf((char*)p,"%d",pss->number++);
    //    m = lws_write(wsi,p,n,LWS_WRITE_TEXT);
    //}

    return 0;

}
*/

void WebSocketServer::init_server()
{
    _isRunning = true;

}

void WebSocketServer::step_server()
{
    /*struct timeval tv;
    gettimeofday(&tv,NULL);
    ms = (tv.tv_sec * 1000) + (tv.tv_usec / 1000);
    if((ms - oldms) > 20){
        lws_callback_on_writable_all_protocol(context,&protocols[MANIPULATOR_CONTROL_PROTOCOL]);
        oldms = ms;
    }
*/
    int n = lws_service(context, 50);
    // libwebsocket_service will process all waiting events with their
    // callback functions and then wait 50 ms.
    // (this is a single threaded webserver and this will keep our server
    // from generating load while there are not requests to process)

    #ifdef LWS_DEBUG
        lwsl_notice("%d\n",n);
    #endif

}

void WebSocketServer::close_server() {
    lws_cancel_service(context);
    lws_context_destroy(context);
    lwsl_notice("WebSocket Server exited cleanly\n");
    _isRunning = false;
}

void WebSocketServer::run_server(){
    init_server();
    // Main service: infinite loop, to end this server send SIGTERM. (CTRL+C)
    signal(SIGINT, sighandler);
    while (force_exit == 0) {
        usleep(1000*10);
        step_server();
    }

    close_server();
}
