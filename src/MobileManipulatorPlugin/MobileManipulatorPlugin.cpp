#include "MobileManipulatorPlugin.hpp"
#include "HMIQt.hpp"

#include <ProgramModel/Program.hpp>
#include <ExecutionModel/Sequence.hpp>
#include <ExecutionModel/DriveRel.hpp>
#include <ExecutionModel/DriveTo.hpp>
#include <ExecutionModel/ScanWithMotion.hpp>
#include <ExecutionModel/InstructionRepository.hpp>

#include <HardwareLayer/ManipulatorSimulated.hpp>
#include <HardwareLayer/MobileDeviceSimulated.hpp>
#include <HardwareLayer/SMAC/SMACSimulated.hpp>
#include <HardwareLayer/MIR/MIR.hpp>
#include <HardwareLayer/LeuzeLPS36/LeuzeLPS36.hpp>
#include <HardwareLayer/Scanner2DSimulated.hpp>
#include <Common/EntityRegister.hpp>
#include <Serialization/InputArchiveXML.hpp>
#include <Serialization/OutputArchiveXML.hpp>

#include <QPushButton>
#include <QMessageBox>
#include <QFileDialog>
#include <QInputDialog>
#include <RobWorkStudio.hpp>
#include <rw/common/Ptr.hpp>

#include <rw/common/LogMultiWriter.hpp>
#include <rw/common/LogFileWriter.hpp>
#include <rw/common/LogStreamWriter.hpp>
#include <rwlibs/task/GraspTask.hpp>
#include <rw/models/SerialDevice.hpp>
#include <rw/invkin/ClosedFormIKSolverUR.hpp>

#include <rwlibs/opengl/DrawableUtil.hpp>
#include <rwlibs/opengl/Drawable.hpp>




USE_ROBWORK_NAMESPACE
using namespace robwork;
using namespace rw::common;
using namespace rw::math;
using namespace rw::trajectory;
using namespace rw::models;
using namespace rw::invkin;
using namespace rws;

using namespace hwl;
using namespace exm;
using namespace pgm;
using namespace mms_common;




MobileManipulatorPlugin::MobileManipulatorPlugin():
    RobWorkStudioPlugin("MobileManipulatorPlugin", QIcon(":/workspace_icon.png"))
{
    setupUi(this);
    connect(&_timer, SIGNAL(timeout()), this, SLOT(timeout()));
	connect(&_scanTimer, SIGNAL(timeout()), this, SLOT(scanTimeTimeOut()));
} 


MobileManipulatorPlugin::~MobileManipulatorPlugin()
{
}



void MobileManipulatorPlugin::initialize() {
    getRobWorkStudio()->stateChangedEvent().add(boost::bind(&MobileManipulatorPlugin::stateChangedListener, this, _1), this);

	rw::common::Log::setLog(&getRobWorkStudio()->log());

	getRobWorkStudio()->genericAnyEvent().add(boost::bind(&MobileManipulatorPlugin::genericAnyEventListener, this, _1, _2), this);
}

void MobileManipulatorPlugin::genericAnyEventListener(const std::string& id, boost::any data)
{
	ER_LOG_DEBUG("Generic Any Event: " << id );
	if (id == "MobileManipulatorTask") {
		if (boost::any_cast<std::string>(&data) != NULL) {
			ER_LOG_DEBUG("Message: " << (*boost::any_cast<std::string>(&data)) );
			std::string filename = (*boost::any_cast<std::string>(&data));
			loadProgram(filename);
		}
	} 
	
}



void MobileManipulatorPlugin::open(WorkCell* workcell)
{
	ER_LOG_DEBUG("MobileManipulatorPlugin::open "<<workcell);
	_workcell = workcell;
	if (workcell == NULL)
		return;
	if (workcell->getDevices().size() == 0)
		return;


	_state = workcell->getDefaultState();	

    MovableFrame* mobileDeviceFrame = workcell->findFrame<MovableFrame>("MobileDevice");
    if (mobileDeviceFrame == NULL) {
        QMessageBox::critical(this, "Error", "No MovableFrame found for the Mobile Device");
        return;
    }
/*	try {
		_mobileManipulator = ownedPtr(new MobileManipulator(_workcell, ownedPtr(new Setup()), _state));
	}
	catch (const std::exception& exp)
	{
		QMessageBox::critical(this, "MobileManipulatorPlugin", tr("Exception: %1").arg(exp.what()));
	}	*/	
	 
	//Frame* tcpFrame = workcell->findFrame("TCP");
	//_mobileManipulator->setCalibrationTCP(tcpFrame);
	//_mobileManipulator->setToolTCP(tcpFrame);
	//_mobileManipulator->setQHome(_manipulator->getQ(_state));
	//_mobileManipulator->setMobileBaseCollisionDetector(getRobWorkStudio()->getCollisionDetector());
	//_mobileManipulator->setCollisionDetector(getRobWorkStudio()->getCollisionDetector());
	//FixedFrame* referenceFrame = dynamic_cast<FixedFrame*>(mobileDeviceFrame->getParent());
	//if (referenceFrame == NULL) {
	//	QMessageBox::critical(this, "Error", "The parent from of the mobile platform does not appear to be a FixedFrame");
	//	return;
	//}
	//_mobileManipulator->setReferenceFrame(referenceFrame);

	//ClosedFormIKSolverUR::Ptr ikSolverUR = ownedPtr(new ClosedFormIKSolverUR(_manipulator, _state));
	//_mobileManipulator->setManipulatorIKSolver(ikSolverUR);

	_system = System::getInstance();
	_system->initialize(_workcell);
	HMIQt::Ptr hmi = ownedPtr(new HMIQt(this));
	connect(hmi.get(), SIGNAL(showMessageBox(const QString&, const QString&, bool*)), this, SLOT(showMessageBox(const QString&, const QString&, bool*)));
	HMI::setInstance(hmi);

}

void MobileManipulatorPlugin::close()
{
//	_timer.stop();
}


 
void MobileManipulatorPlugin::stateChangedListener(const State& state) {
	_state = state;	
	/*if (_system != NULL)
		_system->setState(_state);
		*/
    //if (_setup->collisionDetector()->inCollision(state)) {
    //    ER_LOG_DEBUG("In Collision ");
    //} else {
    //    ER_LOG_DEBUG("No collision ");
    //}
}
 
void MobileManipulatorPlugin::stateChangedCallBack(const State& state) {
    //ER_LOG_DEBUG("Q = "<<_manipulator->getQ(state));
    //Q q = _manipulator->getQ(state);
    //_outfile<<q(0)<<" "<<q(1)<<" "<<q(2)<<" "<<q(3)<<" "<<q(4)<<" "<<q(5));
    _timedStatePath.push_back(TimedState(0.1*_timedStatePath.size(), state));
	//getRobWorkStudio()->setState(state);
}

void MobileManipulatorPlugin::on_btnConnectMIR_clicked()
{
	if (_hwlMobilePlatform != NULL) {
		EntityRegister::remove("MobileDevice");
		_hwlMobilePlatform = NULL;
		btnConnectMIR->setText("Connect MIR");
		cmbMIRConnectType->setEnabled(true);

		return;
	}
    if (cmbMIRConnectType->currentText() == "MIR Simulated") {
		_hwlMobilePlatform = ownedPtr(new MobileDeviceSimulated(_system->getMobileBase(), _system, "MobileDevice"));
        EntityRegister::add("MobileDevice", _hwlMobilePlatform);
		_system->addUpdateRule(ownedPtr(new hwl::MobileDevice::MobileDeviceUpdateRule(_system->getMobileBase(), _hwlMobilePlatform)));
		btnConnectMIR->setText("Disconnect MIR");
		cmbMIRConnectType->setEnabled(false);

	} else if (cmbMIRConnectType->currentText() == "MIR Direct") {
        QStringList list;
        list<<tr("127.0.0.1")<<tr("mir.com")<<tr("192.168.1.21");
        QString host = QInputDialog::getItem(this, "MIR Host", "Host:", list);
        if (host.isEmpty())
            return;

		try {
			MIR::Ptr mir = ownedPtr(new MIR()); 
			mir->connect(host.toStdString());
			_hwlMobilePlatform = mir;
			EntityRegister::add("MobileDevice", _hwlMobilePlatform);
			_system->addUpdateRule(ownedPtr(new hwl::MobileDevice::MobileDeviceUpdateRule(_system->getMobileBase(), _hwlMobilePlatform)));
			btnConnectMIR->setText("Disconnect MIR");
			cmbMIRConnectType->setEnabled(false);
		}
		catch (const std::exception& exp) {
			QMessageBox::critical(this, "Exception:", tr("Unable to connect to MIR with message: %1").arg(exp.what()));
		}
		catch (...) {
			QMessageBox::critical(this, "Exception:", tr("Caught unknown exception trying to connect to MIR"));
		}
    } else if (cmbMIRConnectType->currentText() == "MIR ROS") {
        QMessageBox::information(this, "", "Connect MIR ROS");
    }
}


void MobileManipulatorPlugin::on_btnConnectUR_clicked()
{
	if (_hwlManipulator != NULL) {
		EntityRegister::remove("Manipulator");
		_hwlManipulator = NULL;
		btnConnectUR->setText("Connect UR");
		cmbURConnectType->setEnabled(true);		
		return;
	} 

	if (cmbURConnectType->currentText() == "UR Simulated") {
		//QMessageBox::information(this, "", "Connect UR Simulated");
		_hwlManipulator = ownedPtr(new ManipulatorSimulated(_system->getManipulator(), _system, "Manipulator"));
		EntityRegister::add("Manipulator", _hwlManipulator);
		_system->addUpdateRule(ownedPtr(new hwl::Manipulator::ManipulatorUpdateRule(_system->getManipulator(), _hwlManipulator)));

		btnConnectUR->setText("Disconnect UR");
		cmbURConnectType->setEnabled(false);
	} else if (cmbURConnectType->currentText() == "UR Direct") {
		QStringList list;
		list << tr("127.0.0.1") << tr("192.168.1.5") << tr("192.168.60.4");
		QString host = QInputDialog::getItem(this, "UR Host", "Host:", list);
		if (host.isEmpty())
			return;

		try {
			UR::Ptr ur = ownedPtr(new UR());
			ur->connect(host.toStdString());
			_hwlManipulator = ur;
			EntityRegister::add("Manipulator", _hwlManipulator);
			_system->addUpdateRule(ownedPtr(new hwl::Manipulator::ManipulatorUpdateRule(_system->getManipulator(), _hwlManipulator)));
			btnConnectUR->setText("Disconnect UR");
			cmbURConnectType->setEnabled(false);
		}
		catch (const std::exception& exp) {
			QMessageBox::critical(this, "Exception:", tr("Unable to connect to UR with message: %1").arg(exp.what()));
		}
		catch (...) {
			QMessageBox::critical(this, "Exception:", tr("Caught unknown exception trying to connect to UR"));
		}
		
	
	} else if (cmbURConnectType->currentText() == "UR ROS") {
		QMessageBox::information(this, "", "Connect UR ROS - Not supported yet!");
	}
}

void MobileManipulatorPlugin::on_btnConnectSMAC_clicked()
{
	if (_hwlSMAC != NULL) {
		EntityRegister::remove("SMAC");
		_hwlSMAC = NULL;
		btnConnectSMAC->setText("Connect SMAC");
		cmbSMACConnectType->setEnabled(true);
		return;
	}

	if (cmbSMACConnectType->currentText() == "SMAC Simulated") {
		//QMessageBox::information(this, "", "Connect UR Simulated");
		Device::Ptr smacDevice = _workcell->findDevice("SMAC");
		if (smacDevice == NULL)
		{
			QMessageBox::information(this, "MobileManipulatorPlugin", "Unable to find Device named SMAC in work cell");
			return;
		}
		_hwlSMAC = ownedPtr(new SMACSimulated(smacDevice, _system));
		EntityRegister::add("SMAC", _hwlSMAC);
		//_system->addUpdateRule(ownedPtr(new hwl::Manipulator::ManipulatorUpdateRule(_mobileManipulator->getManipulator(), _hwlManipulator)));

		btnConnectSMAC->setText("Disconnect SMAC");
		cmbSMACConnectType->setEnabled(false);
	}
	else if (cmbSMACConnectType->currentText() == "SMAC Direct") {
		QStringList list;
		list << tr("COM9") << tr("/dev/ttyUSB0");
		QString port = QInputDialog::getItem(this, "SMAC", "Port:", list);
		if (port.isEmpty())
			return;

		try {
			SMAC::Ptr smac = ownedPtr(new SMAC());
			smac->connect(port.toStdString());			

			QMessageBox::information(this, "Wait Until finished initialize", "");

			smac->execute(1);


			_hwlSMAC = smac;
			EntityRegister::add("SMAC", _hwlSMAC); 
			btnConnectSMAC->setText("Disconnect SMAC");
			cmbSMACConnectType->setEnabled(false);
		}
		catch (const std::exception& exp) {
			QMessageBox::critical(this, "Exception:", tr("Unable to connect to SMAC with message: %1").arg(exp.what()));
		}
		catch (...) {
			QMessageBox::critical(this, "Exception:", tr("Caught unknown exception trying to connect to SMAC"));
		}
	}
}

void MobileManipulatorPlugin::on_btnConnectScanner_clicked()
{
	if (_hwlScanner != NULL) {
		EntityRegister::remove("Scanner");
		_hwlScanner = NULL;
		btnConnectScanner->setText("Connect Scanner");
		cmbScannerConnectType->setEnabled(true);
		getRobWorkStudio()->getWorkCellScene()->removeDrawable("Scan", _system->getToolScannerFrame().get());
		return;
	}
	if (cmbScannerConnectType->currentText() == "Scanner Simulated") {
		_hwlScanner = ownedPtr(new Scanner2DSimulated("Scanner", _system, 45*Deg2Rad, 100, std::make_pair(0.2, 1.8), _workcell->findObject("Part")));
		EntityRegister::add("Scanner", _hwlScanner);

		btnConnectScanner->setText("Disconnect Scanner");
		cmbScannerConnectType->setEnabled(false);

	}
	else if (cmbScannerConnectType->currentText() == "Scanner Direct") {
		QStringList list;
		list << tr("192.168.60.3") ;
		QString ip = QInputDialog::getItem(this, "Scanner", "IP:", list);
		if (ip.isEmpty())
			return;

		try {
			LeuzeLPS36::Ptr lps = ownedPtr(new LeuzeLPS36("Scanner"));
			lps->connect(ip.toStdString());
			lps->scannerOff();
			_hwlScanner = lps;
			EntityRegister::add("Scanner", _hwlScanner);
			btnConnectScanner->setText("Disconnect Scanner");
			cmbScannerConnectType->setEnabled(false);
		}
		catch (const std::exception& exp) {
			QMessageBox::critical(this, "Exception:", tr("Unable to connect to Leuze LPS36 with message: %1").arg(exp.what()));
		}
		catch (...) {
			QMessageBox::critical(this, "Exception:", tr("Caught unknown exception trying to connect to Leuze LPS36"));
		}
	}

	_pointCloudRender = ownedPtr(new RenderPointCloud());
	getRobWorkStudio()->getWorkCellScene()->addDrawable(ownedPtr(new Drawable(_pointCloudRender, "Scan")), _system->getManipulator()->getBase());

}

void MobileManipulatorPlugin::on_chkScannerOn_stateChanged(int state)
{
	if (_hwlScanner == NULL)
		return;

	if (chkScannerOn->isChecked()) {
		_hwlScanner->scannerOn();
	}
	else {
		_hwlScanner->scannerOff();
	}
}


void MobileManipulatorPlugin::on_chkAutoUpdateState_stateChanged(int state)
{
	if (chkAutoUpdateState->isChecked()) {
		_timer.start();
	}
	else {
		_timer.stop();
	}
}

void MobileManipulatorPlugin::timeout()
{
	//ER_LOG_DEBUG("TimeOut" );
    if (_hwlManipulator != NULL) {
        Q q = _hwlManipulator->getQ();
		//ER_LOG_DEBUG("Manipulator Q = " << q );
		_system->getManipulator()->setQ(q, _state);
    }
    if (_hwlMobilePlatform != NULL) {
        Pose2D<> pose = _hwlMobilePlatform->getCurrentPose();
		//ER_LOG_DEBUG("Mobile Pose = " << pose.getPos() << " "<<pose.theta());
        Transform3D<> t3d = _system->getMobileBase()->getTransform(_state);
        t3d.P()(0) = pose.x();
        t3d.P()(1) = pose.y();
        t3d.R() = RPY<>(pose.theta(), 0, 0).toRotation3D();
        _system->getMobileBase()->setTransform(t3d, _state);
    }
    getRobWorkStudio()->setState(_state);
}




void MobileManipulatorPlugin::on_btnExecuteProgram_clicked()
{
	ExecutionManager::Ptr executionManager = ownedPtr(new ExecutionManager(_system));
	if (_program != NULL) {
		_executeThread = ownedPtr(new ProgramExecuteThread(_program, executionManager));
		_executeThread->start();
	}
	else {
		QMessageBox::information(this, "MobileManipulatorPlugin", "No Program to Execute");
	}
}


void MobileManipulatorPlugin::on_btnSaveProgram_clicked()
{
	if (_program == NULL) {
		QMessageBox::information(this, "MobileManipulatorPlugin", "No program to save");
		return;
	}
	
	QString filename = QFileDialog::getSaveFileName(this, "Save Program", ".", ".xml");
	if (filename.isEmpty())
		return;

	mms_serialization::OutputArchiveXML oarchive(filename.toStdString());
	_program->write(oarchive, "Program");
	oarchive.finalize();

}

void MobileManipulatorPlugin::on_btnLoadProgram_clicked()
{
	QString filename = QFileDialog::getOpenFileName(this, "Open Program", ".", "*.xml");
	if (filename.isEmpty())
		return;
	loadProgram(filename.toStdString());

}

void MobileManipulatorPlugin::loadProgram(const std::string& filename)
{
	listProgramView->clear();
	try {
		mms_serialization::InputArchiveXML iarchive(filename);
		iarchive.initialize();
		Program::Ptr program = ownedPtr(new Program());
		program->read(iarchive, "Program");
		_program = program;
		listProgramView->addItem(filename.c_str());
		std::ifstream ifile(filename);
		while (ifile.eof() == false) {
			char buffer[512];
			ifile.getline(buffer, 512);
			listProgramView->addItem(buffer);
		}
	}
	catch (const std::exception& exp) {
		//listProgramView->addItem(tr("Unable to load '%1'\nException: %2").arg(filename.c_str()).arg(exp.what()));
		QMessageBox::critical(this, "Error", tr("Unable to load '%1' with exception %2").arg(filename.c_str()).arg(exp.what()));		
		return;
	}

}

void MobileManipulatorPlugin::on_btnLoadPlugin_clicked() {
    QString qfilename = QFileDialog::getOpenFileName(this, "Plugin", "", "*.so");
    if (qfilename.isEmpty())
        return;
    InstructionRepository rep;
    std::string filename = qfilename.toStdString();
    std::string withOutEnding = filename.substr(0, filename.size()-3);
    ER_LOG_DEBUG("File without endning "<<withOutEnding);
    rep.load(withOutEnding);
    std::vector<std::string> ids = rep.getInstructionIds();
    BOOST_FOREACH(const std::string& id, ids) {
        ER_LOG_DEBUG("Plugin Instruction = "<<id);
    }
    ER_LOG_DEBUG("Create Instruction");
    Instruction::Ptr instruction = rep.make("MyPrintInstruction");
    ER_LOG_DEBUG("Create Instruction"<<instruction);
    
	/*Program program("MyProgram", instruction);
    ER_LOG_DEBUG("Exectute Instruction");

	ExecutionManager::Ptr executionManager = ownedPtr(new ExecutionManager(_system));
	program.execute(executionManager);
	*/
}


void MobileManipulatorPlugin::showMessageBox(const QString& title, const QString& message, bool* blocking)
{
	ER_LOG_DEBUG("Received show message box " );
	QMessageBox::information(this, title, message);
	(*blocking) = false;
}

void MobileManipulatorPlugin::on_btnTestScanner_clicked()
{
/*	ScanWithMotion::Ptr scanWithMotion = ownedPtr(new ScanWithMotion(_hwlScanner, _hwlManipulator, _workcell->findObject("Part"), 45 * Deg2Rad, 5 * Deg2Rad));
	ExecutionManager::Ptr executionManager = ownedPtr(new ExecutionManager(_system));
	_program = ownedPtr(new Program("TestScan", scanWithMotion));	
	_executeThread = ownedPtr(new ProgramExecuteThread(_program, executionManager));
	_executeThread->start();
	
	if (_hwlScanner != NULL)
	{
		getRobWorkStudio()->getWorkCellScene()->addGeometry("RayModel", ownedPtr(new Geometry(_hwlScanner.cast<Scanner2DSimulated>()->getRayModel())), _mobileManipulator->getToolScannerFrame().get());
		

		ER_LOG_DEBUG("Get Scan " );
		_system->setState(_state);
		std::vector<Vector3D<> > points = _hwlScanner->getScan();
		_pointCloudRender->clear();
		_pointCloudRender->addPoints(points);
		getRobWorkStudio()->updateAndRepaint();
	}
	*/
}

void MobileManipulatorPlugin::on_btnShowScanFromSystem_clicked()
{
	_pointCloudRender->clear();
	//ER_LOG_DEBUG("Number of points to visualize = " << _system->getPointCloud().size() );
	if (_system->getKnowledgeBase()->has("PointCloud")) {
		_pointCloudRender->addPoints(_system->getKnowledgeBase()->get<std::vector<Vector3D<> > >("PointCloud"));
	}
	else {
		_pointCloudRender->addPoints(std::vector<Vector3D<> >());
	}
	getRobWorkStudio()->updateAndRepaint();
}

void MobileManipulatorPlugin::on_btnExportSystemScan_clicked()
{
	QString filename = QFileDialog::getSaveFileName(this, "Export Scan", ".");
	if (filename.isEmpty())
		return;

	std::ofstream outfile(filename.toStdString());
	if (outfile.is_open() == false) {
		QMessageBox::critical(this, "Error", "Unable to open file for wrting");
		return;
	}

	std::vector<Vector3D<> > scan;
	if (_system->getKnowledgeBase()->has("PointCloud")) {
		scan = _system->getKnowledgeBase()->get<std::vector<Vector3D<> > >("PointCloud");
	}
	for (auto& p: scan) {
		outfile << p(0) << " " << p(1) << " " << p(2) );
	}
	outfile.close();


}


void MobileManipulatorPlugin::scanTimeTimeOut()
{
	ER_LOG_DEBUG("Scan " );
	Q qmanipulator = _hwlManipulator->getQ();
	FixedFrame::Ptr sensorFrame = _system->getToolScannerFrame();
	Frame* baseFrame = _system->getManipulator()->getBase();
	_system->getManipulator()->setQ(qmanipulator, _state);
	Transform3D<> baseTsensor = Kinematics::frameTframe(baseFrame, sensorFrame.get(), _state);
	Transform3D<> delta = inverse(_baseTsensorLast)*baseTsensor;
	const double thresholdP = 0.0025;
	const double thresholdR = 5 * Deg2Rad;

	if (delta.P().norm2() > thresholdP || EAA<>(delta.R()).angle() > thresholdR) {
		ER_LOG_DEBUG("Push back scan " );
		std::vector<Vector3D<> > points = _hwlScanner->getScan();
		_scans.push_back(std::make_pair(baseTsensor, points));
		_baseTsensorLast = baseTsensor;

	}
	else {
		ER_LOG_DEBUG("Did not move enought " );
	}

}

void MobileManipulatorPlugin::on_btnStartPartScan_clicked()
{
	if (_hwlScanner == NULL)
	{
		QMessageBox::critical(this, "Error", "No scanner connected ");
		return;
	}

	if (_hwlManipulator == NULL)
	{
		QMessageBox::critical(this, "Error", "No manipulator connected ");
		return;
	}
	_hwlScanner->scannerOn();
	_baseTsensorLast = Transform3D<>::identity();
	_scanTimer.setInterval(100);
	_scanTimer.setSingleShot(false);
	_scanTimer.start();

}


void MobileManipulatorPlugin::on_btnStopPartScan_clicked()
{
	_scanTimer.stop();
	_hwlScanner->scannerOff();
}



void MobileManipulatorPlugin::on_btnShowScan_clicked()
{
	_pointCloudRender->clear();
	std::vector<Vector3D<> > allpoints;
	for (auto& scan : _scans) {
		const Transform3D<>& baseTsensor = scan.first;
		const std::vector<Vector3D<> > points = scan.second;

		for (auto& p : points) 
		{
			Vector3D<> point = baseTsensor*p;
			if (point(2) > spnZMin->value() && point(2) < spnZMax->value())
			{
				allpoints.push_back(point);
			}
		}
	}
	ER_LOG_DEBUG("Number of points to visualize = " << allpoints.size() );
	_pointCloudRender->addPoints(allpoints);
	getRobWorkStudio()->updateAndRepaint();

}

void MobileManipulatorPlugin::on_btnClearScan_clicked()
{
	_scans.clear();
}


void MobileManipulatorPlugin::on_btnExportScan_clicked()
{
	QString filename = QFileDialog::getSaveFileName(this, "Export Scan", ".");
	if (filename.isEmpty())
		return;

	std::ofstream outfile(filename.toStdString());
	if (outfile.is_open() == false) {
		QMessageBox::critical(this, "Error", "Unable to open file for wrting");
		return;
	}

	for (auto& scan : _scans) {
		const Transform3D<>& baseTsensor = scan.first;
		const std::vector<Vector3D<> > points = scan.second;

		for (auto& p : points)
		{
			Vector3D<> point = baseTsensor*p;
			if (point(2) > spnZMin->value() && point(2) < spnZMax->value())
			{
				outfile << point(0) << " " << point(1) << " " << point(2) );
			}
		}
	}
	outfile.close();
}

void MobileManipulatorPlugin::on_btnImportScan_clicked()
{
	QString filename = QFileDialog::getOpenFileName(this, "Import Scan", ".");
	if (filename.isEmpty())
		return;

	std::ifstream infile(filename.toStdString());
	if (infile.is_open() == false) {
		QMessageBox::critical(this, "Error", "Unable to open file for reading");
		return;
	}

	_scans.clear();
	_scans.push_back(std::make_pair(Transform3D<>::identity(), std::vector<Vector3D<> >()));
	while (infile.eof() == false) {
		Vector3D<> p;
		infile >> p(0);
		infile >> p(1);
		infile >> p(2);
		_scans.front().second.push_back(p);
		ER_LOG_DEBUG("Points Read: " << _scans.front().second.size() );
	}

}