
#ifndef HMIQT_HPP
#define HMIQT_HPP


#include "../ExecutionModel/HMI.hpp"

#include <QWidget>

class HMIQt: public QObject, public exm::HMI {
	Q_OBJECT
public:
	typedef rw::common::Ptr<HMIQt> Ptr;

	HMIQt(QWidget* parent);


	void infoBox(const std::string& title, const std::string& message, bool* blocking);
signals:
	void showMessageBox(const QString& title, const QString& message, bool* blocking);

private:
	QWidget* _parent;
};

#endif /* HMIQT_HPP*/
