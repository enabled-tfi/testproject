/*
 * ProgramExecuteThread.cpp
 *
 *  Created on: Dec 22, 2015
 *      Author: lpe
 */

#include "ProgramExecuteThread.hpp"

using namespace pgm;
using namespace exm;

ProgramExecuteThread::ProgramExecuteThread(Program::Ptr program, ExecutionManager::Ptr executionManager) :
	_program(program),
	_executionManager(executionManager)
{
	// TODO Auto-generated constructor stub

}

ProgramExecuteThread::~ProgramExecuteThread() {
	// TODO Auto-generated destructor stub
}

void ProgramExecuteThread::run() {
	_program->execute(_executionManager);

	emit executionFinished();

}
