#ifndef MOBILEMANIPULATORPLUGIN_HPP
#define MOBILEMANIPULATORPLUGIN_HPP

#include <rws/RobWorkStudioPlugin.hpp>

#ifndef Q_MOC_RUN
#include <rw/models/WorkCell.hpp>
#include <rw/models/SerialDevice.hpp>
#include <rw/kinematics/State.hpp>
#include <rw/rw.hpp>
#include <rwlibs/opengl/RenderLines.hpp>
#include <rwlibs/opengl/RenderPointCloud.hpp>

//#include "../VibrationAnalyser/Task.hpp"
//#include "../VibrationAnalyser/Setup.hpp"
#include "../HardwareLayer/SMAC/SMAC.hpp"
#include "../HardwareLayer/UR/UR.hpp"
#include "../HardwareLayer/MobileDevice.hpp"
#include "../HardwareLayer/Scanner2D.hpp"
#include "../Common/System.hpp"
#endif 

//Include the stuff running in ROS


#include <map>

#include "ui_MobileManipulatorPlugin.h" 
#include "ProgramExecuteThread.hpp"

#include <QTimer>

class MobileManipulatorPlugin: public rws::RobWorkStudioPlugin, private Ui::MobileManipulatorPlugin {
	Q_OBJECT
	Q_INTERFACES(rws::RobWorkStudioPlugin)
	Q_PLUGIN_METADATA(IID "mms.MobileManipulatorPlugin.RobWorkStudioPlugin/0.1" FILE "mms_plugin.json")
public:
	MobileManipulatorPlugin();
	virtual ~MobileManipulatorPlugin();

	virtual void open(rw::models::WorkCell* workcell);

	virtual void close();

	virtual void initialize();

private slots: 

	void stateChangedListener(const rw::kinematics::State& state);
	void stateChangedCallBack(const rw::kinematics::State& state);
	

	void on_btnConnectMIR_clicked();
	void on_btnConnectUR_clicked();
	void on_btnConnectSMAC_clicked();
	void on_btnConnectScanner_clicked();
	
	void on_btnExecuteProgram_clicked();
	void on_btnLoadProgram_clicked();
	void on_btnSaveProgram_clicked();

	void on_btnLoadPlugin_clicked();


	void on_chkAutoUpdateState_stateChanged(int state);
	void on_chkScannerOn_stateChanged(int state);
	void timeout();

	void showMessageBox(const QString& title, const QString& message, bool* blocking);

	void on_btnTestScanner_clicked();
	
	void on_btnExportSystemScan_clicked(); 
	void on_btnShowScanFromSystem_clicked();

	void scanTimeTimeOut();
	void on_btnStartPartScan_clicked();
	void on_btnStopPartScan_clicked();
	void on_btnShowScan_clicked();
	void on_btnClearScan_clicked();
	void on_btnExportScan_clicked();
	void on_btnImportScan_clicked();

	
private:
	rw::models::WorkCell::Ptr _workcell;
	//rw::models::SerialDevice::Ptr _manipulator;
	rw::kinematics::State _state;


	//std::vector<vibration_analyzer::Task::Ptr> _tasks;

	rw::trajectory::TimedStatePath _timedStatePath;

	std::vector<rw::math::Transform3D<> > _calibrationData;

	 
//	mms_common::MobileManipulator::Ptr _mobileManipulator;
	mms_common::System::Ptr _system;
	hwl::Manipulator::Ptr _hwlManipulator;
	hwl::MobileDevice::Ptr _hwlMobilePlatform;
	hwl::ImpulseDevice::Ptr _hwlSMAC;
	hwl::Scanner2D::Ptr _hwlScanner;
	//hwl::UR::Ptr _ur;

	rwlibs::opengl::RenderPointCloud::Ptr _pointCloudRender;




	QTimer _timer;
	ProgramExecuteThread::Ptr _programExecuteThread;

	void genericAnyEventListener(const std::string& id, boost::any data);
	void loadProgram(const std::string& filename);
	pgm::Program::Ptr _program;
	ProgramExecuteThread::Ptr _executeThread;
	rw::math::Transform3D<> _baseTsensorLast;
	std::vector<std::pair<rw::math::Transform3D<>, std::vector<rw::math::Vector3D<> > > > _scans;


	QTimer _scanTimer;
};

#endif /*MOBILEMANIPULATORPLUGIN_HPP*/
