/*
 * ProgramExecuteThread.hpp
 *
 *  Created on: Dec 22, 2015
 *      Author: lpe
 */

#ifndef PROGRAMEXECUTETHREAD_HPP_
#define PROGRAMEXECUTETHREAD_HPP_


#include <ProgramModel/Program.hpp>
#include <ExecutionModel/ExecutionManager.hpp>

#include <QThread>

class ProgramExecuteThread: public QThread {
	Q_OBJECT
public:
	typedef rw::common::Ptr<ProgramExecuteThread> Ptr;

	ProgramExecuteThread(pgm::Program::Ptr program, exm::ExecutionManager::Ptr executionManager);
	virtual ~ProgramExecuteThread();

	void run();

signals:
    void executionFinished();

private:
    pgm::Program::Ptr _program;
	exm::ExecutionManager::Ptr _executionManager;
};

#endif /* PROGRAMEXECUTETHREAD_HPP_ */
