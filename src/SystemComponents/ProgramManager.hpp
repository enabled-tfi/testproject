/* */
#ifndef SYSTEMCOMPONENTS_PROGRAMMANAGER_HPP
#define SYSTEMCOMPONENTS_PROGRAMMANAGER_HPP

#include <ProgramModel/Program.hpp>
#include <Common/Serializable.hpp>

#include <rw/common/Ptr.hpp>

#include <string>
#include <map>
namespace syscomp {
	

/**
 * General interface for program
 */
class ProgramManager
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<ProgramManager> Ptr;

	ProgramManager();

	void addFilesInFolder(const std::string& folder);

	void addProgram(const std::string& id, const std::string& filename);

	void addProgram(const std::string& id, pgm::Program::Ptr program);

	std::vector<std::string> getPrograms();

	void runProgram(const std::string& id, exm::ExecutionManager::Ptr executionManager);

private:
	std::map<std::string, std::pair<pgm::Program::Ptr, std::string> > _programs;

};

} //end namespace

#endif //#ifndef SYSTEMCOMPONENTS_PROGRAMMANAGER_HPP
