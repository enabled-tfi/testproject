#include "ProgramManager.hpp"

#include <boost/foreach.hpp>
#include "../Serialization/OutputArchiveXML.hpp"
#include "../Serialization/InputArchiveXML.hpp"

#include <rw/common/IOUtil.hpp>

#include <iostream>
#include <string>


using namespace syscomp;
using namespace exm;
using namespace pgm;
using namespace er_serialization;

ProgramManager::ProgramManager()
{

}

void ProgramManager::addFilesInFolder(const std::string& folder)
{
    std::vector<std::string> files = rw::common::IOUtil::getFilesInFolder(folder, false /* recursive*/, false /* add path */, "*.xml");
    BOOST_FOREACH(std::string& file, files) {
        addProgram(file, folder+"/"+file);
    }
}

void ProgramManager::addProgram(const std::string& id, const std::string& filename)
{
    if (_programs.find(id) != _programs.end()) {
        RW_THROW("Program named "<<id<<" already exists.");
    }
    _programs[id] = std::make_pair((Program::Ptr)NULL, filename);

}


void ProgramManager::addProgram(const std::string& id, Program::Ptr program)
{
    if (_programs.find(id) != _programs.end()) {
        RW_THROW("Program named "<<id<<" already exists.");
    }
    _programs[id] = std::make_pair(program, "");
}

std::vector<std::string> ProgramManager::getPrograms()
{
    std::vector<std::string> result;
    for (std::map<std::string, std::pair<Program::Ptr, std::string> >::iterator it = _programs.begin(); it != _programs.end(); ++it) {
        result.push_back((*it).first);
    }
    return result;
}

void ProgramManager::runProgram(const std::string& id, ExecutionManager::Ptr executionManager)
{
    ER_LOG_DEBUG("ProgramManager::runProgram("<<id<<") "<<this);
    ER_LOG_DEBUG("Number of programs "<<_programs.size());
    if (_programs.find(id) == _programs.end()) {
        ER_LOG_DEBUG("No program named "<<id<<" found.");
        RW_THROW("No program named "<<id<<" found.");
    }
    ER_LOG_DEBUG("Program Exists");

    if (_programs[id].first == NULL) {
        ER_LOG_DEBUG("Needs to load program ");
        InputArchiveXML iarchive(_programs[id].second);
        ER_LOG_DEBUG("Input Archive Initialized");
        iarchive.initialize();
        ER_LOG_DEBUG("Load Initialized");
        Program::Ptr program = rw::common::ownedPtr(new Program());
        program->read(iarchive, id);
        ER_LOG_DEBUG("Program read");
        _programs[id].first = program;
    }
    ER_LOG_DEBUG("Ready to execute program");
    _programs[id].first->execute(executionManager);
    ER_LOG_DEBUG("Program Executed");
}


