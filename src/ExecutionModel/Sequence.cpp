#include "Sequence.hpp"

#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>

#include <Common/EntityRepository.hpp>
#include "ExecutionManager.hpp"

#include <boost/foreach.hpp>
#include <iostream>

using namespace exm;
using namespace er_common;

Sequence::Sequence():
	Instruction("Sequence", "")
{
    initializeSettings();
    setIsInitialized(true);
}

bool Sequence::execute(ExecutionManager::Ptr execManager) {
    ER_LOG_DEBUG("Execute Sequence ");
	BOOST_FOREACH(Instruction::Ptr instruction, _instructions) {
        ER_LOG_DEBUG("\tExecute Instruction "<<instruction);
        execManager->executeBlocking(instruction);
		//instruction->execute(execManager);
	}
	return true;
}
		

void Sequence::addInstruction(Instruction::Ptr instruction) {
	_instructions.push_back(instruction);
}


void Sequence::initializeSettings()
{

}


/* Methods from the serializable interface */
void Sequence::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    ER_LOG_DEBUG("Sequence::read");
    iarchive.readEnterScope("Sequence");
    Instruction::read(iarchive, id);

    iarchive.readEnterScope("Instructions");


	std::vector<std::pair<std::string, er_serialization::InputArchive::Ptr> > subarchs = iarchive.getSubArchives();
	for (auto sa: subarchs) {
		Instruction::Ptr instruction = EntityRepository<Instruction>::make(sa.first);
		instruction->read(*(sa.second), sa.first);
		_instructions.push_back(instruction);
	}


    iarchive.readLeaveScope("Instructions");
    iarchive.readLeaveScope("Sequence");

}

void Sequence::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("Sequence");
    Instruction::write(oarchive, id);
    int cnt = 0;
    oarchive.writeEnterScope("Instructions");
    BOOST_FOREACH(Instruction::Ptr instruction, _instructions) {
        std::stringstream sstr;
        sstr<<"Instruction"<<cnt;
        instruction->write(oarchive, sstr.str());
        cnt++;
    }
    oarchive.writeLeaveScope("Instructions");
    oarchive.writeLeaveScope("Sequence");
}
