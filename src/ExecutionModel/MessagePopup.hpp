/* */
#ifndef EXM_MESSAGEPOPUP_HPP
#define EXM_MESSAGEPOPUP_HPP

#include "Instruction.hpp"
#include "../HardwareLayer/MobileDevice.hpp"
//#include <rw/math.hpp>


namespace exm {
	
/**
 * @brief 
 */
class MessagePopup: public Instruction
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<MessagePopup> Ptr;

	MessagePopup();

	MessagePopup(const std::string& title, const std::string& message);


    bool execute(ExecutionManager::Ptr execManager);


   /* Methods from the serializable interface */
   virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

   virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


protected:
   void initializeSettings();

private:
	er_common::Setting<std::string>::Ptr _title;
	er_common::Setting<std::string>::Ptr _message;
};

} //end namespace

#endif //#ifndef EXM_MESSAGEPOPUP_HPP
