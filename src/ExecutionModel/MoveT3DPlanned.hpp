/* */
#ifndef EXM_MOVET3DPLANNED_HPP
#define EXM_MOVET3DPLANNED_HPP

#include "Instruction.hpp"
#include <HardwareLayer/Manipulator.hpp>
#include <Common/Target.hpp>
#include <Common/EntityReference.hpp>
#include <rw/math.hpp>
#include <vector>
namespace exm {

/**
 * @brief 
 */
class MoveT3DPlanned: public Instruction
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<MoveT3DPlanned> Ptr;

	MoveT3DPlanned();



	/**
	 */
	MoveT3DPlanned(const rw::math::Transform3D<>& refTtarget, rw::kinematics::Frame* reference, hwl::Manipulator::Ptr deviceInterface);

	MoveT3DPlanned(const rw::math::Transform3D<>& refTtarget, const std::string& reference, const std::string& deviceIdentifier);

	bool execute(ExecutionManager::Ptr execManager);

    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


protected:
    void initializeSettings();

private:
	er_common::Setting<er_common::Target>::Ptr _target;
	//er_common::Setting<hwl::Manipulator::Ptr>::Ptr _deviceInterface;
	er_common::Setting < er_common::EntityReference<hwl::Manipulator> >::Ptr _deviceInterface;

};

} //end namespace

#endif //#ifndef EXM_MOVET3DPLANNED_HPP
