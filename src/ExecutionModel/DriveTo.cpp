#include "DriveTo.hpp"

#include "../Common/EntityRegister.hpp"

#include <Common/InputArchive.hpp>
#include <Common/OutputArchive.hpp>

#include <rw/common/TimerUtil.hpp>

#include <boost/foreach.hpp>

using namespace exm;
using namespace er_common;
using namespace hwl;
using namespace rw::math;
using rw::common::TimerUtil;

namespace {
	std::string createDescription(const Transform2D<>& target) {
		std::stringstream sstr;
		sstr<<"DriveTo(target="<<target;
		return sstr.str();
	}
}


DriveTo::DriveTo():
	Instruction("DriveTo","")
{
    initializeSettings();
    setIsInitialized(false);
}

DriveTo::DriveTo(const Transform2D<>& target, bool isBlocking, hwl::MobileDevice::Ptr deviceInterface):
	Instruction("DriveTo", createDescription(target)),
	_isBlocking(isBlocking)
{
    initializeSettings();
    _target->setValue(target);
    _deviceInterface->getValue().set(deviceInterface);
    setIsInitialized(true);
}

DriveTo::DriveTo(const rw::math::Transform2D<>& target, bool isBlocking, const std::string& deviceIdentifier):
	Instruction("DriveTo", createDescription(target)),
    _isBlocking(isBlocking)
{
    initializeSettings();
    _target->setValue(target);
	if (EntityRegister::has(deviceIdentifier)) {
		_deviceInterface->getValue().set(EntityRegister::get<MobileDevice>(deviceIdentifier));
	}
	else {
		_deviceInterface->getValue().set(deviceIdentifier);
	}
    setIsInitialized(true);
}

void DriveTo::initializeSettings()
{
    _target = getSettings().add<Transform2D<> >("Target","Target as Transform2D", Transform2D<>());
    //_deviceInterface = getSettings().add<hwl::MobileDevice::Ptr>("MobileDevice", "Interface to the mobile device", NULL);
	_deviceInterface = getSettings().add<EntityReference<hwl::MobileDevice> >("MobileDevice", "Interface of the mobile device", EntityReference<hwl::MobileDevice>("MobileDevice"));
}



bool DriveTo::execute(ExecutionManager::Ptr execManager)
{
	ER_LOG_DEBUG("DriveTo::execute" );
	ER_LOG_DEBUG("\tTarget = " << _target->getValue() );
    //ER_LOG_DEBUG("DriveTo "<<_target<<" blocking = "<<_isBlocking);
	_deviceInterface->getValue().get()->moveTo(_target->getValue(), 0.1);
	if (_isBlocking) {
	    //ER_LOG_DEBUG("Blocking motions");
	    while (_deviceInterface->getValue().get()->isMoving()) {
	        ER_LOG_DEBUG(".");
            TimerUtil::sleepUs(10*1000);
	    }
	    //ER_LOG_DEBUG("Finished Waiting");
	}
	return true;
}
		
void DriveTo::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    iarchive.readEnterScope("DriveTo");
    Instruction::read(iarchive, id);

    Transform2D<> t2d = iarchive.read<Transform2D<> >("Target");
    _target->setValue(t2d);

    std::string deviceIdentifier = iarchive.read<std::string>("MobileDevice");
    ER_LOG_DEBUG("Device Identifier = "<<deviceIdentifier);

    _isBlocking = iarchive.read<bool>("IsBlocking");

    _deviceInterface->getValue().set(deviceIdentifier);

    iarchive.readLeaveScope("DriveTo");
}

void DriveTo::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("DriveTo");
    Instruction::write(oarchive, id);

    oarchive.write(_target->getValue(), "Target");
    oarchive.write(_deviceInterface->getValue().getName(), "MobileDevice");
    oarchive.write(_isBlocking, "IsBlocking");
    oarchive.writeLeaveScope("DriveTo");
}
