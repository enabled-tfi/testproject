#include "Print.hpp"

#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>

using namespace exm;


Print::Print():
        Instruction("Print")
{
    initializeSettings();
    setIsInitialized(false);
}

Print::Print(const std::string& msg, std::ostream& out):
        Instruction("Print")
{
    initializeSettings();
    _msg->setValue(msg);
    _outstream = &out;
    setIsInitialized(true);
}

void Print::initializeSettings()
{
    _msg = getSettings().add<std::string>("Message", "Message to print", "");
}

bool Print::execute(ExecutionManager::Ptr execManager)
{
   (*_outstream)<<_msg->getValue();
   return true;
}


/* Methods from the serializable interface */
void Print::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    ER_LOG_DEBUG("Print::read");
    iarchive.readEnterScope("Print");
    Instruction::read(iarchive, id);
    std::string msg = iarchive.read<std::string>("Message");

    size_t npos;
    do {
        npos = msg.find_first_of("\\n");
        if (npos < msg.length()) {
            msg.replace(npos, 2, "\n");
        }
    } while (npos < msg.length());

    _msg->setValue(msg);
    iarchive.readLeaveScope("Print");
}

void Print::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("Print");
    Instruction::write(oarchive, id);

    std::string msg = _msg->getValue();
    size_t npos;
    do {
        npos = msg.find_first_of('\n');
        if (npos < msg.length()) {
            msg.replace(npos, 1, "\\n");
        }
    } while (npos < msg.length());

    oarchive.write(msg, "Message");
    oarchive.writeLeaveScope("Print");
}
