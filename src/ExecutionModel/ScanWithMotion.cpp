#include "ScanWithMotion.hpp"

#include "MoveT3DPlanned.hpp"
#include "MessagePopup.hpp"
#include <Common/EntityRegister.hpp>
#include <HardwareLayer/Scanner2DSimulated/Scanner2DSimulated.hpp>

#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>


using namespace exm;
using namespace hwl;
using namespace er_common;
using namespace rw::math;
using namespace rw::kinematics;

ScanWithMotion::ScanWithMotion():
	Instruction("ScanWithMotion","")
{
    initializeSettings();
    setIsInitialized(false);
}


ScanWithMotion::ScanWithMotion(Scanner2D::Ptr scanner, Manipulator::Ptr manipulator, rw::models::Object::Ptr part, double fov, double angleStep):
	Instruction("ScanWithMotion", "")
{
	initializeSettings();
	_scanner2d->setValue(scanner);
	_manipulator->setValue(manipulator);
	_fov->setValue(fov);
	_angleStep->setValue(angleStep);
	_partFrame->getValue().set(part->getBase());
	setIsInitialized(true);
}

ScanWithMotion::ScanWithMotion(const std::string& scanner, const std::string& manipulator, rw::models::Object::Ptr part, double fov, double angleStep):
	Instruction("ScanWithMotion", "")
{
	initializeSettings();

	if (EntityRegister::has(scanner)) {
		_scanner2d->setValue(EntityRegister::get<hwl::Scanner2D>(scanner));
	}
	else {
		//_scanner2d->setValue(rw::common::ownedPtr(new hwl::Scanner2DSimulated("Scanner", NULL, 45*Deg2Rad, 100, std::make_pair(0.2, 0.8), part)));
	}

	if (EntityRegister::has(manipulator)) {
		_manipulator->getValue().set(EntityRegister::get<hwl::Manipulator>(manipulator));
	}
	else {
		_manipulator->getValue().set(manipulator);
	}

	_fov->setValue(fov);
	_angleStep->setValue(angleStep);
	_partFrame->getValue().set(part->getBase());
	setIsInitialized(true);
}

void ScanWithMotion::initializeSettings()
{	
	_fov = getSettings().add<double>("FOV", "Field of view", 45*Deg2Rad);
	_angleStep = getSettings().add<double>("AngleStep", "AngularStepSize", 5*Deg2Rad);
	_scanner2d = getSettings().add<Scanner2D::Ptr>("Scanner2D", "Scanner", NULL);
	_manipulator = getSettings().add<EntityReference<hwl::Manipulator> >("Manipulator", "Interface of the device to move", EntityReference<hwl::Manipulator>("Manipulator"));
	
	_partFrame = getSettings().add<FrameReference>("PartFrame", "Frame of the part", NULL);
}


void ScanWithMotion::scan(Scanner2D::Ptr scanner, Manipulator::Ptr manipulator, System::Ptr system, const State& state, std::vector<Vector3D<> >& points)
{
	//Set scanner to on
	scanner->scannerOn();

	Frame* scannerFrame = scanner->getSensorFrame();//system->getToolScannerFrame();
	Transform3D<> baseTscanner = system->getManipulator()->baseTframe(scannerFrame, state);
	Transform3D<> scannerTbase = inverse(baseTscanner);
	double fov = _fov->getValue();
	double dtheta = _angleStep->getValue();
	int steps = (int)std::ceil(fov / dtheta);
	dtheta = fov / (double)steps;
	for (int i = 0; i < steps; i++) {
		double theta = -fov / 2.0 + i*dtheta;
		Transform3D<> baseTscannerNew = baseTscanner*Transform3D<>(RPY<>(0, 0, theta).toRotation3D());
		manipulator->lin(baseTscannerNew);
		std::vector<Vector3D<> > scan = scanner->getScan();
		for (Vector3D<> p : scan) {
			p = baseTscannerNew*p;
			//TODO: Fix Hardcoded limits :-(
			if (p(2) > 0 && p(2) < 0.3) {
				points.push_back(p);
			}
		}
		ER_LOG_DEBUG("Accumulated Number of points " << points.size());
	}
	scanner->scannerOff();
	manipulator->lin(baseTscanner);
}

Transform3D<> ScanWithMotion::lookAtPart(const rw::math::Vector3D<>& basePsensor, System::Ptr system, const State& state) const {
	Vector3D<> basePpart = Kinematics::frameTframe(system->getManipulator()->getBase(), _partFrame->getValue().get(), state).P();

	Vector3D<> viewingDirection = normalize(basePpart - basePsensor);
	Vector3D<> horizontal(-1, 0, 0);
	Vector3D<> ydirection = normalize(cross(viewingDirection, horizontal));
	Vector3D<> xdirection = normalize(cross(ydirection, viewingDirection));

	Rotation3D<> rot(xdirection, ydirection, viewingDirection);
	Transform3D<> baseTsensor(basePsensor, rot);
	return baseTsensor;
}

std::vector<Transform3D<> > ScanWithMotion::simpleScanStrategy(System::Ptr system, const State& state) const
{
	std::vector<Transform3D<> > result;
	result.push_back(lookAtPart(Vector3D<>(0, -0.5, 0.5), system, state));
	return result;
}

std::vector<Transform3D<> > ScanWithMotion::threePointStrategy(System::Ptr system, const State& state) const
{
	std::vector<Transform3D<> > result;
	result.push_back(lookAtPart(Vector3D<>(0, -0.5, 0.4), system, state));
	result.push_back(lookAtPart(Vector3D<>(-0.5, -0.5, 0.5), system, state));
	result.push_back(lookAtPart(Vector3D<>(0.5, -0.5, 0.5), system, state));
	return result;
}

bool ScanWithMotion::execute(ExecutionManager::Ptr execManager)
{
	Scanner2D::Ptr scanner = _scanner2d->getValue();
	Manipulator::Ptr manipulator = _manipulator->getValue().get();
	System::Ptr system = execManager->getSystem();


	system->update();
	State state = system->getState();
	//if (_partFrame->getValue() == NULL) 
	//{
	//	_partFrame->setValue(execManager->getSystem()->getWorkCell()->findFrame(_partFrameName));
	//}


	//Set the TCP Frame to the right one
	Frame* scannerFrame = scanner->getSensorFrame();//system->getToolScannerFrame();
	Transform3D<> endTscanner = Kinematics::frameTframe(system->getManipulator()->getEnd(), scannerFrame, state);
	Transform3D<> endTtcpOld = manipulator->getEndToTCP();
	manipulator->setEndToTCP(endTscanner);

	//Find the right position for scanning the object

	//Simple Scan Position a bit in front of the robot
	std::vector<Transform3D<> > baseTsensorTransforms = simpleScanStrategy(system, state);
	//std::vector<Transform3D<> > baseTsensorTransforms = threePointStrategy(system, state);

	//Transform3D<> worldTbase = Kinematics::worldTframe(mm->getManipulator()->getBase(), state);

	std::vector<Vector3D<> > points;
	for (auto baseTsensor : baseTsensorTransforms) {
		MoveT3DPlanned movePlanned(baseTsensor, system->getManipulator()->getBase(), manipulator);
		movePlanned.execute(execManager);
		execManager->getSystem()->update();
		state = execManager->getSystem()->getState();

		//MessagePopup popup("Wait", "Check if configuration allows scan");
		//popup.execute(execManager);

		scan(scanner, manipulator, system, state, points);
	}



	system->getKnowledgeBase()->add("PointCloud", "Point cloud from scanner", points);
	//system->setPointCloud(points);

	//Restore old endTtcp
	manipulator->setEndToTCP(endTtcpOld);
   
   return true;
}


/* Methods from the serializable interface */
void ScanWithMotion::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    ER_LOG_DEBUG("ScanWithMotion::read");
    iarchive.readEnterScope("ScanWithMotion");

    Instruction::read(iarchive, id);
    
	double fov = iarchive.read<double>("FOV");
	_fov->setValue(fov);

	double angleStep = iarchive.read<double>("AngleStep");
	_angleStep->setValue(angleStep);

	std::string manipulator = iarchive.read<std::string>("Manipulator");
	_manipulator->getValue().set(manipulator);

	std::string scanner = iarchive.read<std::string>("Scanner");
	_scanner2d->setValue(EntityRegister::get<hwl::Scanner2D>(scanner));
	_partFrame->getValue().read(iarchive, "PartFrame");

	iarchive.readLeaveScope("ScanWithMotion");
}

void ScanWithMotion::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("ScanWithMotion");
    Instruction::write(oarchive, id);

    
	oarchive.write(_fov->getValue(), "FOV");
	oarchive.write(_angleStep->getValue(), "AngleStep");
	oarchive.write(_manipulator->getValue().get()->getName(), "Manipulator");
	oarchive.write(_scanner2d->getValue()->getName(), "Scanner");
	_partFrame->getValue().write(oarchive, "PartFrame");

	oarchive.writeLeaveScope("ScanWithMotion");
}
