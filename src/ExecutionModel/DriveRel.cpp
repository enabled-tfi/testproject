#include "DriveRel.hpp"

#include <Common/InputArchive.hpp>
#include <Common/OutputArchive.hpp>
#include <Common/EntityRegister.hpp>

#include <boost/foreach.hpp>

using namespace exm;
using namespace er_common;
using namespace hwl;
using namespace rw::math;

namespace {
	std::string createDescription(double distance, double angle) {
		std::stringstream sstr;
		sstr<<"Drive(distance="<<distance<<" angle="<<angle;;
		return sstr.str();
	}
}

DriveRel::DriveRel():
        Instruction("DriveRel", "")
{
    initializeSettings();
    setIsInitialized(false);
}

DriveRel::DriveRel(double distance, double angle, hwl::MobileDevice::Ptr deviceInterface):
	Instruction("DriveRel", createDescription(distance, angle))
{
    initializeSettings();
    _distance->setValue(distance);
    _angle->setValue(angle);
    _deviceInterface->getValue().set(deviceInterface);
    setIsInitialized(true);
}

DriveRel::DriveRel(double distance, double angle, const std::string& deviceIdentifier):
    Instruction("DriveRel", createDescription(distance, angle))
{
    initializeSettings();
    _distance->setValue(distance);
    _angle->setValue(angle);
	if (EntityRegister::has(deviceIdentifier)) {
		_deviceInterface->getValue().set(EntityRegister::get<MobileDevice>(deviceIdentifier));
	}
	else {
		_deviceInterface->getValue().set(deviceIdentifier);
	}
    setIsInitialized(true);
}

void DriveRel::initializeSettings()
{
    _distance = getSettings().add<double>("Distance", "Distance to move in meters", 0);
    _angle = getSettings().add<double>("Angle", "Angle to rotate in radians", 0);
	_deviceInterface = getSettings().add<EntityReference<hwl::MobileDevice> >("MobileDevice", "Interface of the mobile device", EntityReference<hwl::MobileDevice>("MobileDevice"));
    //_deviceInterface = getSettings().add<hwl::MobileDevice::Ptr>("MobileDevice", "Interface to the mobile device", NULL);
}

bool DriveRel::execute(ExecutionManager::Ptr execManager)
{
	//_deviceInterface->getValue()->move(_distance->getValue(), _angle->getValue());
	return true;
}
		
void DriveRel::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    ER_LOG_DEBUG("DriveRel::read");
    iarchive.readEnterScope("DriveRel");
    Instruction::read(iarchive, id);

    _distance->setValue(iarchive.read<double>("Distance"));
    _angle->setValue(iarchive.read<double>("Angle"));

    std::string deviceIdentifier = iarchive.read<std::string>("MobileDevice");
    _deviceInterface->getValue().set(deviceIdentifier);

    iarchive.readLeaveScope("DriveRel");
}

void DriveRel::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("DriveRel");
    Instruction::write(oarchive, id);

    oarchive.write(_distance->getValue(), "Distance");
    oarchive.write(_angle->getValue(), "Angle");
    oarchive.write(_deviceInterface->getValue().getName(), "MobileDevice");
    oarchive.writeLeaveScope("DriveRel");
}
