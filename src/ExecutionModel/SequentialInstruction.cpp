#include "SequentialInstruction.hpp"

using namespace swm;


SequentialInstruction::SequentialInstruction(const std::string& name, const std::string& description):
	Instruction(name, description)
{

}


void SequentialInstruction::doSetNextInstruction(Instruction::Ptr next) {
	_nextInstruction = next;
}

Instruction::Ptr SequentialInstruction::execute(ProgramState& state) {
	doExecute(state);
	return _nextInstruction;
}
