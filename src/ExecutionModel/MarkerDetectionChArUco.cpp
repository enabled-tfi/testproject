#include "MarkerDetectionChArUco.hpp"

#include <rw/math/EAA.hpp>
#include <rw/math/Transform3D.hpp>
#include <Common/EntityRegister.hpp>

#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>

#include <opencv2/aruco.hpp>
#include <opencv2/aruco/charuco.hpp>


using namespace exm;
using namespace hwl;
using namespace er_common;
using namespace rw::math;
using namespace rw::kinematics;
using namespace rw::common;
using namespace cv;

MarkerDetectionChArUco::MarkerDetectionChArUco() :
	MarkerDetection("MarkerDetectionChArUco", "")
{
	initializeSettings();
	setIsInitialized(false);
	_executionCount = 0;
}

MarkerDetectionChArUco::MarkerDetectionChArUco(Camera::Ptr camera, int execCount) :
    MarkerDetection("MarkerDetectionChArUco", "")
{
	initializeSettings();
	_camera->getValue().set(camera);
	setIsInitialized(true);
	_executionCount = execCount;
}


void MarkerDetectionChArUco::initializeSettings()
{
	_camera = getSettings().add<EntityReference<hwl::Camera> >("Camera", "Interface of the camera", EntityReference<hwl::Camera>("Camera"));
	_markerSize = getSettings().add<double>("ChArUco_SquareSize", "Size of square in meter", 0.1);
	_markerSize->setRange(0, 1.0);

//	_arucoMarkerId = getSettings().add<int>("ArUco_MarkerId", "Id of Aruco marker", 0);
//	_arucoMarkerId->setRange(0, 999999);

	_knowledgeBaseIdentifier = getSettings().add<std::string>("ChArUco_KnowledgebaseId", "Identifier for the results in the knowledge base", "ChArUcoMarkerPose");

	//_camera = getSettings().add<Camera::Ptr>("Camera", "Camera for acquiring marker", NULL);

}

const cv::Mat& MarkerDetectionChArUco::getDetectionImage() const
{
	return _detectionImage;
}


bool MarkerDetectionChArUco::execute(ExecutionManager::Ptr execManager)
{
	try {
		bool saveImages = er_common::System::getInstance()->getSaveImages();

		_detected = false;
		_executionCount++;
		ER_LOG_DEBUG("MarkerDetectionChArUco" );
		//Acquire Image
		Camera::Ptr camera = _camera->getValue().get();
		//ER_LOG_DEBUG("Camera = " << camera );
		
		for (int i = 0; i < 5; i++) {
			camera->acquire();
			TimerUtil::sleepMs(100); 
		}

		cv::Mat inputImage = camera->getLastImage();

		if(saveImages){
			std::stringstream imageOriginalfile;
			imageOriginalfile << er_common::System::getInstance()->getMarkerImageFolder() << "/ChArUcoImages/" << _executionCount << "_ChArucoInputImage_" << ".png";
			imwrite(imageOriginalfile.str(), inputImage);
		}

		//cv::cvtColor(inputImage, inputImage, cv::COLOR_BGR2GRAY);
//		imwrite("ImageForArucoMarkerDetection.png", inputImage);
		//cv::aruco::DetectorParameters parameters;
		cv::Ptr<aruco::DetectorParameters> params = cv::aruco::DetectorParameters::create();
		
		/*params->cornerRefinementMethod = cv::aruco::CORNER_REFINE_SUBPIX;
		params->cornerRefinementWinSize = 10;
		params->cornerRefinementMaxIterations = 1000;
		params->cornerRefinementMinAccuracy = 0.5;
		params->adaptiveThreshWinSizeMin = 3;
		params->adaptiveThreshWinSizeMax = 30;
		params->adaptiveThreshWinSizeStep = 2;
		*/

//		params->cornerRefinementMethod = cv::aruco::CORNER_REFINE_SUBPIX;
		params->cornerRefinementWinSize = 20;
		params->cornerRefinementMaxIterations = 200;
		params->cornerRefinementMinAccuracy = 1.0;
		params->adaptiveThreshConstant = 10;
		params->adaptiveThreshWinSizeMin = 3;
		params->adaptiveThreshWinSizeMax = 50;
		params->adaptiveThreshWinSizeStep = 1;
		params->maxErroneousBitsInBorderRate = 0.01;


		
		cv::Ptr<aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(aruco::DICT_4X4_50);
		//std::vector<int> ids;
		//std::vector<std::vector<cv::Point2f> > corners;
		//std::vector< std::vector<Point2f> > rejectedCandidates; 

		int squaresX = 6;
		int squaresY = 8;
		float squareLength = (float)_markerSize->getValue();
		float markerLength = squareLength / 2.0f;

		cv::Ptr<cv::aruco::CharucoBoard> charucoboard = cv::aruco::CharucoBoard::create(squaresX, squaresY, squareLength, markerLength, dictionary);
		cv::Ptr<cv::aruco::Board> board = charucoboard.staticCast<cv::aruco::Board>();
		cv::Mat charucoImage = inputImage.clone();

		charucoboard->draw(Size(1000,1000), charucoImage, 1, 1);

		if(saveImages){
			std::stringstream charucoBoardFile;
			charucoBoardFile << er_common::System::getInstance()->getMarkerImageFolder() << "/ChArUcoImages/" << _executionCount << "_CharucoBoardArt_" << ".png";
			imwrite(charucoBoardFile.str(), charucoImage);
		}
		ER_LOG_DEBUG("Wrote charuco board to file" );
		ER_LOG_DEBUG("Lengths: " << squaresX << " - " << squaresY << " - " );

		std::vector< int > markerIds, charucoIds;
		std::vector< std::vector< cv::Point2f > > markerCorners, rejectedMarkers;
		std::vector< cv::Point2f > charucoCorners;
		cv::Vec3d rvec, tvec;

		// detect markers
		aruco::detectMarkers(inputImage, dictionary, markerCorners, markerIds, params, rejectedMarkers);

		std::pair<Mat, Mat> cameraMatrixAndDistCoeffs = camera->getCameraCalibration();
		cv::Mat camMatrix = cameraMatrixAndDistCoeffs.first;
		cv::Mat distCoeffs = cameraMatrixAndDistCoeffs.second;

		// refind strategy to detect more markers
		aruco::refineDetectedMarkers(inputImage, board, markerCorners, markerIds, rejectedMarkers, camMatrix, distCoeffs);

		// interpolate charuco corners
		int interpolatedCorners = 0;
		if (markerIds.size() > 0)
			interpolatedCorners = aruco::interpolateCornersCharuco(markerCorners, markerIds, inputImage, charucoboard, charucoCorners, charucoIds, camMatrix, distCoeffs);

		// estimate charuco board pose
		bool validPose = false;
		if (camMatrix.total() != 0)
			validPose = aruco::estimatePoseCharucoBoard(charucoCorners, charucoIds, charucoboard, camMatrix, distCoeffs, rvec, tvec);





		//cv::aruco::detectMarkers(inputImage, dictionary, corners, ids, params, rejectedCandidates);
		ER_LOG_DEBUG("MarkerDetectionChArUco: " );
		ER_LOG_DEBUG("\tImage Id: " << _executionCount );
		ER_LOG_DEBUG("\tMarkerIds Count: " << markerIds.size());
		ER_LOG_DEBUG("\tCharuco Corners: " << charucoCorners.size() );

		//ER_LOG_DEBUG("\tRejected Candidates Count: " << rejectedCandidates.size() );

		_meanProjectionError = calcMeanErrorOfProjection(charucoboard->chessboardCorners, Mat(rvec), Mat(tvec), cameraMatrixAndDistCoeffs, charucoCorners);

		cv::Mat outputImage = inputImage.clone();
//		if (markerIds.size() > 0) {
//			aruco::drawDetectedMarkers(outputImage, markerCorners);
//		}
//
//		if (rejectedMarkers.size() > 0)
//			aruco::drawDetectedMarkers(outputImage, rejectedMarkers, noArray(), Scalar(0, 0, 255));


		if (interpolatedCorners > 0) {
			Scalar color;
			color = Scalar(255, 0, 0);

			aruco::drawDetectedCornersCharuco(outputImage, charucoCorners, charucoIds, color);
		}

		if (validPose) {
			ER_LOG_DEBUG("\tFound valid pose" );
			aruco::drawAxis(outputImage, camMatrix, distCoeffs, rvec, tvec, 0.1f);
			std::stringstream imageoutfile;
			std::stringstream projErrStr;
			projErrStr << "Mean Projection Error: " << _meanProjectionError;
			cv::putText(outputImage, projErrStr.str(), cv::Point(40,40), cv::FONT_HERSHEY_COMPLEX, 1, (0,0,255), 1, cv::LINE_AA, false);
			if(saveImages){
				imageoutfile << er_common::System::getInstance()->getMarkerImageFolder() << "/ChArUcoImages/" << _executionCount << "_ChArUcoMarkerDetection_" << ".png";
				imwrite(imageoutfile.str(), outputImage);
			}

		}
		else {
			ER_LOG_DEBUG("\tNo valid pose found" );
			if(saveImages){
				std::stringstream imageoutfile;
				imageoutfile << er_common::System::getInstance()->getMarkerImageFolder() << "/ChArUcoImages/" << _executionCount << "_CharucoMarkerDetectionNoPoseFound_" << ".png";
				imwrite(imageoutfile.str(), outputImage);
			}
			_detected = false;

		}

		Transform3D<> cam2marker;
		cam2marker.R() = EAA<>(rvec(0), rvec(1), rvec(2)).toRotation3D();
		cam2marker.P() = Vector3D<>(tvec(0), tvec(1), tvec(2));
		//cv::aruco::drawAxis(inputImage, cameraMatrixAndDistCoeffs.first, cameraMatrixAndDistCoeffs.second, rvecs.row(0), tvecs.row(0), 0.1f);


		_detectionImage = outputImage;

		std::string knowledgeBaseId = _knowledgeBaseIdentifier->getValue();
		execManager->getSystem()->getKnowledgeBase()->addForce(knowledgeBaseId, "Camera to Marker transform", cam2marker);
		ER_LOG_DEBUG("\tcam2marker: XYZ = " << cam2marker.P()<<" RPY = "<<RPY<>(cam2marker.R()) );
	}
	catch (const std::exception& exp) {
		ER_LOG_ERROR("MarkerDetectionChArUco: Exception while trying to identify marker: " <<exp.what());
		throw exp;
	}
	catch (...) {
		ER_LOG_ERROR("MarkerDetectionChArUco: Unknown Exception while trying to identify marker.");
		RW_THROW("Caught unknown exception while doing Aruco Marker Detection ");
	}

	return true;
}


double MarkerDetectionChArUco::calcMeanErrorOfProjection(std::vector<Point3f> boardPoints, Mat rvecs, Mat tvecs, std::pair<Mat, Mat> cameraMatrixAndDistCoeffs, std::vector<cv::Point2f> points){
		// Projecting all points to chessboard
	std::vector<Point2f> projectedPoints;
	projectPoints(boardPoints, rvecs, tvecs, cameraMatrixAndDistCoeffs.first, cameraMatrixAndDistCoeffs.second, projectedPoints);

	double totalError = 0;

	for(size_t i = 0; i < projectedPoints.size(); i++)
		ER_LOG_DEBUG(projectedPoints[i] );

	for(size_t i = 0; i < points.size(); i++){
		ER_LOG_DEBUG(points[i]);

		totalError += std::sqrt(pow((projectedPoints[i].x - points[i].x),2.0) + pow((projectedPoints[i].y - points[i].y),2.0) );
	}

	return totalError/projectedPoints.size();
}

double MarkerDetectionChArUco::getMeanProjectionError(){
	return _meanProjectionError;
}

bool MarkerDetectionChArUco::isDetected(){
	return _detected;
}


/* Methods from the serializable interface */
void MarkerDetectionChArUco::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    iarchive.readEnterScope("MarkerDetectionChArUco");

    MarkerDetection::read(iarchive, id);
	getSettings().read(iarchive, "Settings");
	iarchive.readLeaveScope("MarkerDetectionChArUco");
}


void MarkerDetectionChArUco::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("MarkerDetectionChArUco");
    MarkerDetection::write(oarchive, id);

	getSettings().write(oarchive, "Settings");
	

	oarchive.writeLeaveScope("MarkerDetectionChArUco");
}
