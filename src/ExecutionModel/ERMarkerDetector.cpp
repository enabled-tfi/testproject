#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <opencv2/calib3d.hpp>
#include <opencv2/core/types_c.h>
#include <opencv2/imgproc/imgproc_c.h>
#include <opencv2/imgproc/types_c.h>

#include <Common/Logging.hpp>
#include "ERMarkerDetector.hpp"

#define MAX_CONTOUR_APPROX  7
bool SHOW_CORNER_PROCESS = false;

using namespace cv;

/***************************************************************************************************/
//COMPUTE FAST HISTOGRAM GRADIENT
template<typename ArrayContainer>
static void icvGradientOfHistogram256(const ArrayContainer& piHist, ArrayContainer& piHistGrad)
{
    CV_DbgAssert(piHist.size() == 256);
    CV_DbgAssert(piHistGrad.size() == 256);
    piHistGrad[0] = 0;
    int prev_grad = 0;
    for (int i = 1; i < 255; ++i)
    {
        int grad = piHist[i-1] - piHist[i+1];
        if (std::abs(grad) < 100)
        {
            if (prev_grad == 0)
                grad = -100;
            else
                grad = prev_grad;
        }
        piHistGrad[i] = grad;
        prev_grad = grad;
    }
    piHistGrad[255] = 0;
}


template<typename ArrayContainer>
static void icvGetIntensityHistogram256(const Mat& img, ArrayContainer& piHist)
{
    for (int i = 0; i < 256; i++)
        piHist[i] = 0;
    // sum up all pixel in row direction and divide by number of columns
    for (int j = 0; j < img.rows; ++j)
    {
        const uchar* row = img.ptr<uchar>(j);
        for (int i = 0; i < img.cols; i++)
        {
            piHist[row[i]]++;
        }
    }
}

//SMOOTH HISTOGRAM USING WINDOW OF SIZE 2*iWidth+1
template<int iWidth_, typename ArrayContainer>
static void icvSmoothHistogram256(const ArrayContainer& piHist, ArrayContainer& piHistSmooth, int iWidth = 0)
{
    CV_DbgAssert(iWidth_ == 0 || (iWidth == iWidth_ || iWidth == 0));
    iWidth = (iWidth_ != 0) ? iWidth_ : iWidth;
    CV_Assert(iWidth > 0);
    CV_DbgAssert(piHist.size() == 256);
    CV_DbgAssert(piHistSmooth.size() == 256);
    for (int i = 0; i < 256; ++i)
    {
        int iIdx_min = std::max(0, i - iWidth);
        int iIdx_max = std::min(255, i + iWidth);
        int iSmooth = 0;
        for (int iIdx = iIdx_min; iIdx <= iIdx_max; ++iIdx)
        {
            CV_DbgAssert(iIdx >= 0 && iIdx < 256);
            iSmooth += piHist[iIdx];
        }
        piHistSmooth[i] = iSmooth/(2*iWidth+1);
    }
}



static void icvBinarizationHistogramBased(cv::Mat & img)
{
    CV_Assert(img.channels() == 1 && img.depth() == CV_8U);
    int iCols = img.cols;
    int iRows = img.rows;
    int iMaxPix = iCols*iRows;
    int iMaxPix1 = iMaxPix/100;
    const int iNumBins = 256;
    const int iMaxPos = 20;
    cv::AutoBuffer<int, 256> piHistIntensity(iNumBins);
    cv::AutoBuffer<int, 256> piHistSmooth(iNumBins);
    cv::AutoBuffer<int, 256> piHistGrad(iNumBins);
    cv::AutoBuffer<int> piMaxPos(iMaxPos);

    icvGetIntensityHistogram256(img, piHistIntensity);

    // first smooth the distribution
    //const int iWidth = 1;
    icvSmoothHistogram256<1>(piHistIntensity, piHistSmooth);

    // compute gradient
    icvGradientOfHistogram256(piHistSmooth, piHistGrad);

    // check for zeros
    unsigned iCntMaxima = 0;
    for (int i = iNumBins-2; (i > 2) && (iCntMaxima < iMaxPos); --i)
    {
        if ((piHistGrad[i-1] < 0) && (piHistGrad[i] > 0))
        {
            int iSumAroundMax = piHistSmooth[i-1] + piHistSmooth[i] + piHistSmooth[i+1];
            if (!(iSumAroundMax < iMaxPix1 && i < 64))
            {
                piMaxPos[iCntMaxima++] = i;
            }
        }
    }

            iCntMaxima > 0 ? piMaxPos[0] : -1;
            iCntMaxima > 1 ? piMaxPos[1] : -1;
            iCntMaxima > 2 ? piMaxPos[2] : -1;

    int iThresh = 0;

    CV_Assert((size_t)iCntMaxima <= piMaxPos.size());

                iCntMaxima > 0 ? piMaxPos[0] : -1;
                iCntMaxima > 1 ? piMaxPos[1] : -1;
                iCntMaxima > 2 ? piMaxPos[2] : -1;

    if (iCntMaxima == 0)
    {
        // no any maxima inside (only 0 and 255 which are not counted above)
        // Does image black-write already?
        const int iMaxPix2 = iMaxPix / 2;
        for (int sum = 0, i = 0; i < 256; ++i) // select mean intensity
        {
            sum += piHistIntensity[i];
            if (sum > iMaxPix2)
            {
                iThresh = i;
                break;
            }
        }
    }
    else if (iCntMaxima == 1)
    {
        iThresh = piMaxPos[0]/2;
    }
    else if (iCntMaxima == 2)
    {
        iThresh = (piMaxPos[0] + piMaxPos[1])/2;
    }
    else // iCntMaxima >= 3
    {
        // CHECKING THRESHOLD FOR WHITE
        int iIdxAccSum = 0, iAccum = 0;
        for (int i = iNumBins - 1; i > 0; --i)
        {
            iAccum += piHistIntensity[i];
            // iMaxPix/18 is about 5,5%, minimum required number of pixels required for white part of chessboard
            if ( iAccum > (iMaxPix/18) )
            {
                iIdxAccSum = i;
                break;
            }
        }

        unsigned iIdxBGMax = 0;
        int iBrightMax = piMaxPos[0];
        // printf("iBrightMax = %d\n", iBrightMax);
        for (unsigned n = 0; n < iCntMaxima - 1; ++n)
        {
            iIdxBGMax = n + 1;
            if ( piMaxPos[n] < iIdxAccSum )
            {
                break;
            }
            iBrightMax = piMaxPos[n];
        }

        // CHECKING THRESHOLD FOR BLACK
        int iMaxVal = piHistIntensity[piMaxPos[iIdxBGMax]];

        //IF TOO CLOSE TO 255, jump to next maximum
        if (piMaxPos[iIdxBGMax] >= 250 && iIdxBGMax + 1 < iCntMaxima)
        {
            iIdxBGMax++;
            iMaxVal = piHistIntensity[piMaxPos[iIdxBGMax]];
        }

        for (unsigned n = iIdxBGMax + 1; n < iCntMaxima; n++)
        {
            if (piHistIntensity[piMaxPos[n]] >= iMaxVal)
            {
                iMaxVal = piHistIntensity[piMaxPos[n]];
                iIdxBGMax = n;
            }
        }

        //SETTING THRESHOLD FOR BINARIZATION
        int iDist2 = (iBrightMax - piMaxPos[iIdxBGMax])/2;
        iThresh = iBrightMax - iDist2;
    }

    if (iThresh > 0)
    {
        img = (img >= iThresh);
    }
}

static void SHOW_QUADS(const std::string & name, const Mat & img_, ChessBoardQuad * quads, int quads_count)
{
    Mat img = img_.clone();
    if (img.channels() == 1)
        cvtColor(img, img, COLOR_GRAY2BGR);
    for (int i = 0; i < quads_count; ++i)
    {
        ChessBoardQuad & quad = quads[i];
        for (int j = 0; j < 4; ++j)
        {
            line(img, quad.corners[j]->pt, quad.corners[(j + 1) & 3]->pt, Scalar(0, 240, 0), 2, LINE_AA);
        }
    }
    imshow(name, img);
    waitKey(0);
}


void drawStuff( CvArr* img, CvPoint center, int radius,CvScalar color, int row){

	cvCircle( img, center, 4, color, -1, 18, 0 );

    CvFont font;
    cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, 0.5, 0.5);
    std::stringstream text;
    text << row;
    cvPutText(img, text.str().c_str(),  center, &font , cvScalar(255,255,255));

    if(SHOW_CORNER_PROCESS){
    	cv::Mat dst = cvarrToMat(img);
    	imshow("CornerProcess",  dst);
    	waitKey(0);
    }


}


bool ERMarkerDetector::findErMarkerCorners(InputArray image_, Size pattern_size,
                           OutputArray corners_, int flags){


    Mat img = image_.getMat();

	setOrigImage(img);

    if(img.channels() != 1)
    	cvtColor(img, img, COLOR_BGR2GRAY);

	cv::Mat thresh_img_new = img.clone();

	icvBinarizationHistogramBased(thresh_img_new); // process image in-place
//		cv::imshow("test", thresh_img_new);
//		cv::waitKey(0);

	int min_dilations = 0;
	int max_dilations = 15;
	std::vector<cv::Point2f> out_corners;
	int prev_sqr_size = 0;
	bool found = false;

		// Dilate until we find the correct pattern
	for (int dilations = min_dilations; dilations <= max_dilations; dilations++)
	{
		//USE BINARY IMAGE COMPUTED USING icvBinarizationHistogramBased METHOD
		dilate( thresh_img_new, thresh_img_new, Mat(), Point(-1, -1), 1 );

		// So we can find rectangles that go to the edge, we draw a white line around the image edge.
		// Otherwise FindContours will miss those clipped rectangle contours.
		// The border color will be the image mean, because otherwise we risk screwing up filters like cvSmooth()...
		rectangle( thresh_img_new, Point(0,0), Point(thresh_img_new.cols-1, thresh_img_new.rows-1), Scalar(255,255,255), 3, LINE_8);


		Mat binarized_img = thresh_img_new.clone(); // make clone because cvFindContours modifies the source image

			// Generate quads on the input image
		generateQuads(binarized_img, 0);

		ER_LOG_DEBUG( "Quad count: " << all_quads_count << " - " << (pattern_size.width/2+1)*(pattern_size.height/2+1));
		if(SHOW_CORNER_PROCESS)
			SHOW_QUADS("test", binarized_img, &all_quads[0], all_quads_count);

			// process quads - find connected quads, check that quad pattern matches the expected pattern of the marker
		if (processQuads(out_corners, prev_sqr_size))
		{
			found = true;
			break;
		}
	}


    // revert to old, slower, method if detection failed
    if (!found)
    {
        if (flags & CALIB_CB_NORMALIZE_IMAGE)
        {
            img = img.clone();
            equalizeHist(img, img);
        }

        Mat thresh_img;
        prev_sqr_size = 0;

        ER_LOG_DEBUG("Fallback to old algorithm");
        const bool useAdaptive = flags & CALIB_CB_ADAPTIVE_THRESH;
        if (!useAdaptive)
        {
            // empiric threshold level
            // thresholding performed here and not inside the cycle to save processing time
            double mean = cv::mean(img).val[0];
            int thresh_level = std::max(cvRound(mean - 10), 10);
            threshold(img, thresh_img, thresh_level, 255, THRESH_BINARY);
        }
        //if flag CALIB_CB_ADAPTIVE_THRESH is not set it doesn't make sense to iterate over k
        int max_k = useAdaptive ? 6 : 1;
        for (int k = 0; k < max_k && !found; k++)
        {
            for (int dilations = min_dilations; dilations <= max_dilations; dilations++)
            {
                // convert the input grayscale image to binary (black-n-white)
                if (useAdaptive)
                {
                    int block_size = cvRound(prev_sqr_size == 0
                                             ? std::min(img.cols, img.rows) * (k % 2 == 0 ? 0.2 : 0.1)
                                             : prev_sqr_size * 2);
                    block_size = block_size | 1;
                    // convert to binary
                    adaptiveThreshold( img, thresh_img, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, block_size, (k/2)*5 );
                    if (dilations > 0)
                        dilate( thresh_img, thresh_img, Mat(), Point(-1, -1), dilations-1 );

                }
                else
                {
                    dilate( thresh_img, thresh_img, Mat(), Point(-1, -1), 1 );
                }

                // So we can find rectangles that go to the edge, we draw a white line around the image edge.
                // Otherwise FindContours will miss those clipped rectangle contours.
                // The border color will be the image mean, because otherwise we risk screwing up filters like cvSmooth()...
                rectangle( thresh_img, Point(0,0), Point(thresh_img.cols-1, thresh_img.rows-1), Scalar(255,255,255), 3, LINE_8);

                reset();

#ifdef USE_CV_FINDCONTOURS
                Mat binarized_img = thresh_img;
#else
                Mat binarized_img = (useAdaptive) ? thresh_img : thresh_img.clone(); // make clone because cvFindContours modifies the source image
#endif
                generateQuads(binarized_img, flags);
                if(SHOW_CORNER_PROCESS)
                	SHOW_QUADS("Old quads", thresh_img, &all_quads[0], all_quads_count);
                if (processQuads(out_corners, prev_sqr_size))
                {
                    found = 1;
                    break;
                }
            }
        }
    }




	// check that none of the found corners is too close to the image boundary
	if (found)
	{
		ER_LOG_DEBUG("Checking borders");
		const int BORDER = 8;
		for (int k = 0; k < out_corners.size(); ++k)
		{
			if( out_corners[k].x <= BORDER || out_corners[k].x > img.cols - BORDER ||
				out_corners[k].y <= BORDER || out_corners[k].y > img.rows - BORDER )
			{
				found = false;
				break;
			}
		}
	}

	ER_LOG_DEBUG("Done checking borders, copying");

	Mat(out_corners).copyTo(corners_);

	ER_LOG_DEBUG("Done copying");

	return found;
}


bool ERMarkerDetector::processQuads(std::vector<cv::Point2f>& out_corners, int &prev_sqr_size)
{
    out_corners.resize(0);
    if (all_quads_count <= 0)
        return false;

    size_t max_quad_buf_size = all_quads.size();

    // Find quad's neighbors
    findQuadNeighbors();

    // allocate extra for adding in orderFoundQuads
    std::vector<ChessBoardQuad*> quad_group;
    std::vector<ChessBoardCorner*> corner_group; corner_group.reserve(max_quad_buf_size * 4);

    for (int group_idx = 0; ; group_idx++)
    {
        findConnectedQuads(quad_group, group_idx);
        if (quad_group.empty())
            break;

        int count = (int)quad_group.size();

        ER_LOG_DEBUG("group id: " << group_idx);
        ER_LOG_DEBUG("count: " << count);

        // order the quad corners globally
        // maybe delete or add some
        ER_LOG_DEBUG("Starting ordering of inner quads " << count);
        count = orderFoundConnectedQuads(quad_group);
        ER_LOG_DEBUG("Finished ordering of inner quads " << count);

        if (count == 0)
            continue;       // haven't found inner quads

        // If count is more than it should be, this will remove those quads
        // which cause maximum deviation from a nice square pattern.
        count = cleanFoundConnectedQuads(quad_group);
        ER_LOG_DEBUG("Connected group: " << group_idx  << ", count: " << count);

        count = checkQuadGroup(quad_group, corner_group);

        ER_LOG_DEBUG("Checked group: " << group_idx << ", count: " << count);

        int n = count;
        float sum_dist = 0;
        int total = 0;
        ER_LOG_DEBUG("count: " << count);
        for(int i = 0; i < n; i++ )
        {
            int ni = 0;
            float sum = corner_group[i]->sumDist(ni);
            sum_dist += sum;
            total += ni;
        }
        prev_sqr_size = cvRound(sum_dist/std::max(total, 1));

        if(count != 30)
        	return false;

        if (count > 0 || (-count > (int)out_corners.size()))
        {
            // copy corners to output array
            out_corners.reserve(n);
            for (int i = 0; i < n; i++){
            	ER_LOG_DEBUG(i);
                out_corners.push_back(corner_group[i]->pt);}

//            if (count == 30 && checkBoardMonotony(out_corners))
			if (count == 30)
            {
                return true;
            }
        }
    }
    return false;
}

// returns corners in clockwise order
// corners don't necessarily start at same position on quad (e.g.,
//   top left corner)
void ERMarkerDetector::generateQuads(const cv::Mat& image_, int flags)
{
    binarized_image = image_;  // save for debug purposes

    int quad_count = 0;

    all_quads.deallocate();
    all_corners.deallocate();

    // empiric bound for minimal allowed perimeter for squares
    int min_size = 25; //cvRound( image->cols * image->rows * .03 * 0.01 * 0.92 );

    bool filterQuads = (flags & CALIB_CB_FILTER_QUADS) != 0;
#ifdef USE_CV_FINDCONTOURS // use cv::findContours

    std::vector<std::vector<Point> > contours;
    std::vector<Vec4i> hierarchy;

    cv::findContours(image_, contours, hierarchy, RETR_CCOMP, CHAIN_APPROX_SIMPLE);

    if (contours.empty())
    {
        CV_LOG_DEBUG(NULL, "calib3d(chessboard): cv::findContours() returns no contours");
        return;
    }

    std::vector<int> contour_child_counter(contours.size(), 0);
    int boardIdx = -1;

    std::vector<QuadCountour> contour_quads;

    for (int idx = (int)(contours.size() - 1); idx >= 0; --idx)
    {
        int parentIdx = hierarchy[idx][3];
        if (hierarchy[idx][2] != -1 || parentIdx == -1)  // holes only (no child contours and with parent)
            continue;
        const std::vector<Point>& contour = contours[idx];

        Rect contour_rect = boundingRect(contour);
        if (contour_rect.area() < min_size)
            continue;

        std::vector<Point> approx_contour;

        const int min_approx_level = 1, max_approx_level = MAX_CONTOUR_APPROX;
        for (int approx_level = min_approx_level; approx_level <= max_approx_level; approx_level++ )
        {
            approxPolyDP(contour, approx_contour, (float)approx_level, true);
            if (approx_contour.size() == 4)
                break;

            // we call this again on its own output, because sometimes
            // approxPoly() does not simplify as much as it should.
            std::vector<Point> approx_contour_tmp;
            std::swap(approx_contour, approx_contour_tmp);
            approxPolyDP(approx_contour_tmp, approx_contour, (float)approx_level, true);
            if (approx_contour.size() == 4)
                break;
        }

        // reject non-quadrangles
        if (approx_contour.size() != 4)
            continue;
        if (!cv::isContourConvex(approx_contour))
            continue;

        cv::Point pt[4];
        for (int i = 0; i < 4; ++i)
            pt[i] = approx_contour[i];
        CV_LOG_VERBOSE(NULL, 9, "... contours(" << contour_quads.size() << " added):" << pt[0] << " " << pt[1] << " " << pt[2] << " " << pt[3]);

        if (filterQuads)
        {
            double p = cv::arcLength(approx_contour, true);
            double area = cv::contourArea(approx_contour, false);

            double d1 = sqrt(normL2Sqr<double>(pt[0] - pt[2]));
            double d2 = sqrt(normL2Sqr<double>(pt[1] - pt[3]));

            // philipg.  Only accept those quadrangles which are more square
            // than rectangular and which are big enough
            double d3 = sqrt(normL2Sqr<double>(pt[0] - pt[1]));
            double d4 = sqrt(normL2Sqr<double>(pt[1] - pt[2]));
            if (!(d3*4 > d4 && d4*4 > d3 && d3*d4 < area*1.5 && area > min_size &&
                d1 >= 0.15 * p && d2 >= 0.15 * p))
                continue;
        }

        contour_child_counter[parentIdx]++;
        if (boardIdx != parentIdx && (boardIdx < 0 || contour_child_counter[boardIdx] < contour_child_counter[parentIdx]))
            boardIdx = parentIdx;

        contour_quads.push_back(QuadCountour(pt, parentIdx));
    }

    size_t total = contour_quads.size();
    size_t max_quad_buf_size = std::max((size_t)2, total * 3);
    all_quads.allocate(max_quad_buf_size);
    all_corners.allocate(max_quad_buf_size * 4);

    // Create array of quads structures
    for (size_t idx = 0; idx < total; ++idx)
    {
        QuadCountour& qc = contour_quads[idx];
        if (filterQuads && qc.parent_contour != boardIdx)
            continue;

        int quad_idx = quad_count++;
        ChessBoardQuad& q = all_quads[quad_idx];

        // reset group ID
        q = ChessBoardQuad();
        for (int i = 0; i < 4; ++i)
        {
            Point2f pt(qc.pt[i]);
            ChessBoardCorner& corner = all_corners[quad_idx * 4 + i];

            corner = ChessBoardCorner(pt);
            q.corners[i] = &corner;
        }
        q.edge_len = FLT_MAX;
        for (int i = 0; i < 4; ++i)
        {
            float d = normL2Sqr<float>(q.corners[i]->pt - q.corners[(i+1)&3]->pt);
            q.edge_len = std::min(q.edge_len, d);
        }
    }

#else // use legacy API: cvStartFindContours / cvFindNextContour / cvEndFindContours

    CvMat image_old = cv::Mat(image_), *image = &image_old;

    CvContourEx* board = 0;

    // create temporary storage for contours and the sequence of pointers to found quadrangles
    cv::Ptr<CvMemStorage> temp_storage(cvCreateMemStorage(0));
    CvSeq *root = cvCreateSeq(0, sizeof(CvSeq), sizeof(CvSeq*), temp_storage);

    // initialize contour retrieving routine
    CvContourScanner scanner = cvStartFindContours(image, temp_storage, sizeof(CvContourEx),
                                   CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

    // get all the contours one by one
    CvSeq* src_contour = NULL;
    while ((src_contour = cvFindNextContour(scanner)) != NULL)
    {
        CvSeq *dst_contour = 0;
        CvRect rect = ((CvContour*)src_contour)->rect;

        // reject contours with too small perimeter
        if( CV_IS_SEQ_HOLE(src_contour) && rect.width*rect.height >= min_size )
        {
            const int min_approx_level = 1, max_approx_level = MAX_CONTOUR_APPROX;
            for (int approx_level = min_approx_level; approx_level <= max_approx_level; approx_level++ )
            {
                dst_contour = cvApproxPoly( src_contour, sizeof(CvContour), temp_storage,
                                            CV_POLY_APPROX_DP, (float)approx_level );
                if( dst_contour->total == 4 )
                    break;

                // we call this again on its own output, because sometimes
                // cvApproxPoly() does not simplify as much as it should.
                dst_contour = cvApproxPoly( dst_contour, sizeof(CvContour), temp_storage,
                                            CV_POLY_APPROX_DP, (float)approx_level );

                if( dst_contour->total == 4 )
                    break;
            }

            // reject non-quadrangles
            if( dst_contour->total == 4 && cvCheckContourConvexity(dst_contour) )
            {
                cv::Point2i pt[4];
                double p = cvContourPerimeter(dst_contour);
                double area = fabs(cvContourArea(dst_contour, CV_WHOLE_SEQ));

                for (int i = 0; i < 4; ++i)
                    pt[i] = *(CvPoint*)cvGetSeqElem(dst_contour, i);
                CV_LOG_VERBOSE(NULL, 9, "... contours(" << root->total << " added):" << pt[0] << " " << pt[1] << " " << pt[2] << " " << pt[3]);

                double d1 = sqrt(normL2Sqr<double>(pt[0] - pt[2]));
                double d2 = sqrt(normL2Sqr<double>(pt[1] - pt[3]));

                // philipg.  Only accept those quadrangles which are more square
                // than rectangular and which are big enough
                double d3 = sqrt(normL2Sqr<double>(pt[0] - pt[1]));
                double d4 = sqrt(normL2Sqr<double>(pt[1] - pt[2]));
                if (!filterQuads ||
                    (d3*4 > d4 && d4*4 > d3 && d3*d4 < area*1.5 && area > min_size &&
                    d1 >= 0.15 * p && d2 >= 0.15 * p))
                {
                    CvContourEx* parent = (CvContourEx*)(src_contour->v_prev);
                    parent->counter++;
                    if( !board || board->counter < parent->counter )
                        board = parent;
                    dst_contour->v_prev = (CvSeq*)parent;
                    //for( i = 0; i < 4; i++ ) cvLine( debug_img, pt[i], pt[(i+1)&3], cvScalar(200,255,255), 1, CV_AA, 0 );
                    cvSeqPush( root, &dst_contour );
                }
            }
        }
    }

    // finish contour retrieving
    cvEndFindContours( &scanner );

    // allocate quad & corner buffers
    int total = root->total;
    size_t max_quad_buf_size = std::max((size_t)2, (size_t)total * 3);
    all_quads.allocate(max_quad_buf_size);
    all_corners.allocate(max_quad_buf_size * 4);

    // Create array of quads structures
    for (int idx = 0; idx < total; ++idx)
    {
        /* CvSeq* */src_contour = *(CvSeq**)cvGetSeqElem(root, idx);
        if (filterQuads && src_contour->v_prev != (CvSeq*)board)
            continue;

        int quad_idx = quad_count++;
        ChessBoardQuad& q = all_quads[quad_idx];

        // reset group ID
        q = ChessBoardQuad();
        CV_Assert(src_contour->total == 4);
        for (int i = 0; i < 4; i++)
        {
            Point* onePoint = (Point*)cvGetSeqElem(src_contour, i);
            CV_Assert(onePoint != NULL);
            Point2f pt(*onePoint);
            ChessBoardCorner& corner = all_corners[quad_idx*4 + i];

            corner = ChessBoardCorner(pt);
            q.corners[i] = &corner;
        }
        q.edge_len = FLT_MAX;
        for (int i = 0; i < 4; ++i)
        {
            float d = normL2Sqr<float>(q.corners[i]->pt - q.corners[(i+1)&3]->pt);
            q.edge_len = std::min(q.edge_len, d);
        }
    }
#endif

    all_quads_count = quad_count;

    CV_LOG_VERBOSE(NULL, 3, "Total quad contours: " << total);
    CV_LOG_VERBOSE(NULL, 3, "max_quad_buf_size=" << max_quad_buf_size);
    CV_LOG_VERBOSE(NULL, 3, "filtered quad_count=" << quad_count);
}

void ERMarkerDetector::findQuadNeighbors()
{
    const float thresh_scale = 1.f;
    // find quad neighbors
    for (int idx = 0; idx < all_quads_count; idx++)
    {
        ChessBoardQuad& cur_quad = (ChessBoardQuad&)all_quads[idx];

        // choose the points of the current quadrangle that are close to
        // some points of the other quadrangles
        // (it can happen for split corners (due to dilation) of the
        // checker board). Search only in other quadrangles!

        // for each corner of this quadrangle
        for (int i = 0; i < 4; i++)
        {
            if (cur_quad.neighbors[i])
                continue;

            float min_dist = FLT_MAX;
            int closest_corner_idx = -1;
            ChessBoardQuad *closest_quad = 0;

            cv::Point2f pt = cur_quad.corners[i]->pt;

            // find the closest corner in all other quadrangles
            for (int k = 0; k < all_quads_count; k++)
            {
                if (k == idx)
                    continue;

                ChessBoardQuad& q_k = all_quads[k];

                for (int j = 0; j < 4; j++)
                {
                    if (q_k.neighbors[j])
                        continue;

                    float dist = normL2Sqr<float>(pt - q_k.corners[j]->pt);
                    if (dist < min_dist &&
                        dist <= cur_quad.edge_len*thresh_scale &&
                        dist <= q_k.edge_len*thresh_scale )
                    {
                        // check edge lengths, make sure they're compatible
                        // edges that are different by more than 1:4 are rejected
                        float ediff = cur_quad.edge_len - q_k.edge_len;
                        if (ediff > 32*cur_quad.edge_len ||
                            ediff > 32*q_k.edge_len)
                        {
                            ER_LOG_DEBUG("Incompatible edge lengths");
                            continue;
                        }
                        closest_corner_idx = j;
                        closest_quad = &q_k;
                        min_dist = dist;
                    }
                }
            }

            // we found a matching corner point?
            if (closest_corner_idx >= 0 && min_dist < FLT_MAX)
            {
                CV_Assert(closest_quad);

                if (cur_quad.count >= 4 || closest_quad->count >= 4)
                    continue;

                // If another point from our current quad is closer to the found corner
                // than the current one, then we don't count this one after all.
                // This is necessary to support small squares where otherwise the wrong
                // corner will get matched to closest_quad;
                ChessBoardCorner& closest_corner = *closest_quad->corners[closest_corner_idx];

                int j = 0;
                for (; j < 4; j++)
                {
                    if (cur_quad.neighbors[j] == closest_quad)
                        break;

                    if (normL2Sqr<float>(closest_corner.pt - cur_quad.corners[j]->pt) < min_dist)
                        break;
                }
                if (j < 4)
                    continue;

                // Check that each corner is a neighbor of different quads
                for(j = 0; j < closest_quad->count; j++ )
                {
                    if (closest_quad->neighbors[j] == &cur_quad)
                        break;
                }
                if (j < closest_quad->count)
                    continue;

                // check whether the closest corner to closest_corner
                // is different from cur_quad->corners[i]->pt
                for (j = 0; j < all_quads_count; j++ )
                {
                    ChessBoardQuad* q = &const_cast<ChessBoardQuad&>(all_quads[j]);
                    if (j == idx || q == closest_quad)
                        continue;

                    int k = 0;
                    for (; k < 4; k++ )
                    {
                        if (!q->neighbors[k])
                        {
                            if (normL2Sqr<float>(closest_corner.pt - q->corners[k]->pt) < min_dist)
                                break;
                        }
                    }
                    if (k < 4)
                        break;
                }
                if (j < all_quads_count)
                    continue;

                closest_corner.pt = (pt + closest_corner.pt) * 0.5f;

                // We've found one more corner - remember it
                cur_quad.count++;
                cur_quad.neighbors[i] = closest_quad;
                cur_quad.corners[i] = &closest_corner;

                closest_quad->count++;
                closest_quad->neighbors[closest_corner_idx] = &cur_quad;
            }
        }
    }
}

void ERMarkerDetector::findConnectedQuads(std::vector<ChessBoardQuad*>& out_group, int group_idx)
{
    out_group.clear();

    std::stack<ChessBoardQuad*> stack;

    int i = 0;
    for (; i < all_quads_count; i++)
    {
        ChessBoardQuad* q = (ChessBoardQuad*)&all_quads[i];

        // Scan the array for a first unlabeled quad
        if (q->count <= 0 || q->group_idx >= 0) continue;

        // Recursively find a group of connected quads starting from the seed all_quads[i]
        stack.push(q);
        out_group.push_back(q);
        q->group_idx = group_idx;
        q->ordered = false;

        while (!stack.empty())
        {
            q = stack.top(); CV_Assert(q);
            stack.pop();
            for (int k = 0; k < 4; k++ )
            {
                ChessBoardQuad *neighbor = q->neighbors[k];
                if (neighbor && neighbor->count > 0 && neighbor->group_idx < 0 )
                {
                    stack.push(neighbor);
                    out_group.push_back(neighbor);
                    neighbor->group_idx = group_idx;
                    neighbor->ordered = false;
                }
            }
        }
        break;
    }
}

int ERMarkerDetector::orderFoundConnectedQuads(std::vector<ChessBoardQuad*>& quads)
{
    const int max_quad_buf_size = (int)all_quads.size();
    int quad_count = (int)quads.size();

    std::stack<ChessBoardQuad*> stack;

    // first find an interior quad
    ChessBoardQuad *start = NULL;
    for (int i = 0; i < quad_count; i++)
    {
        if (quads[i]->count == 4)
        {
            start = quads[i];
            break;
        }
    }

    if (start == NULL)
        return 0;   // no 4-connected quad

    // start with first one, assign rows/cols
    int row_min = 0, col_min = 0, row_max=0, col_max = 0;

    std::map<int, int> col_hist;
    std::map<int, int> row_hist;

    stack.push(start);
    start->row = 0;
    start->col = 0;
    start->ordered = true;

    // Recursively order the quads so that all position numbers (e.g.,
    // 0,1,2,3) are in the at the same relative corner (e.g., lower right).

    while (!stack.empty())
    {
        ChessBoardQuad* q = stack.top(); stack.pop(); CV_Assert(q);

        int col = q->col;
        int row = q->row;
        col_hist[col]++;
        row_hist[row]++;

        // check min/max
        if (row > row_max) row_max = row;
        if (row < row_min) row_min = row;
        if (col > col_max) col_max = col;
        if (col < col_min) col_min = col;

        for (int i = 0; i < 4; i++)
        {
            ChessBoardQuad *neighbor = q->neighbors[i];
            switch(i)   // adjust col, row for this quad
            {           // start at top left, go clockwise
            case 0:
                row--; col--; break;
            case 1:
                col += 2; break;
            case 2:
                row += 2;   break;
            case 3:
                col -= 2; break;
            }

            // just do inside quads
            if (neighbor && neighbor->ordered == false ) // && neighbor->count == 4
            {
//                DPRINTF("col: %d  row: %d", col, row);
                CV_Assert(q->corners[i]);
                orderQuad(*neighbor, *(q->corners[i]), (i+2)&3); // set in order
                neighbor->ordered = true;
                neighbor->row = row;
                neighbor->col = col;
                stack.push(neighbor);
            }
        }
    }

    // analyze inner quad structure
    int w = pattern_size.width - 1;
    int h = pattern_size.height - 1;
    int drow = row_max - row_min;
    int dcol = col_max - col_min;

    // normalize pattern and found quad indices
    if ((w > h && dcol < drow) || (w < h && drow < dcol))
    {
        h = pattern_size.width - 1;
        w = pattern_size.height - 1;
    }

    ER_LOG_DEBUG("Size: "<< dcol << "x" <<  drow <<" Pattern: " << w << "x" << h);

    // check if there are enough inner quads
    if (dcol < w || drow < h)   // found enough inner quads?
    {
        ER_LOG_DEBUG("Too few inner quad rows/cols");
        return 0;   // no, return
    }
#ifdef ENABLE_TRIM_COL_ROW
    // too many columns, not very common
    if (dcol == w+1)    // too many, trim
    {
        DPRINTF("Trimming cols");
        if (col_hist[col_max] > col_hist[col_min])
        {
            DPRINTF("Trimming left col");
            trimCol(quads, col_min, -1);
        }
        else
        {
            DPRINTF("Trimming right col");
            trimCol(quads, col_max, +1);
        }
    }

    // too many rows, not very common
    if (drow == h+1)    // too many, trim
    {
        DPRINTF("Trimming rows");
        if (row_hist[row_max] > row_hist[row_min])
        {
            DPRINTF("Trimming top row");
            trimRow(quads, row_min, -1);
        }
        else
        {
            DPRINTF("Trimming bottom row");
            trimRow(quads, row_max, +1);
        }
    }

    quad_count = (int)quads.size(); // update after icvTrimCol/icvTrimRow
#endif

    // check edges of inner quads
    // if there is an outer quad missing, fill it in
    // first order all inner quads
    int found = 0;
    for (int i=0; i < quad_count; ++i)
    {
//    	std::cout << "here" << std::endl;
        ChessBoardQuad& q = *quads[i];
        if (q.count != 4)
            continue;

        {   // ok, look at neighbors
            int col = q.col;
            int row = q.row;
            for (int j = 0; j < 4; j++)
            {
                switch(j)   // adjust col, row for this quad
                {           // start at top left, go clockwise
                case 0:
                    row--; col--; break;
                case 1:
                    col += 2; break;
                case 2:
                    row += 2;   break;
                case 3:
                    col -= 2; break;
                }
                ChessBoardQuad *neighbor = q.neighbors[j];
                if (neighbor && !neighbor->ordered && // is it an inner quad?
                    col <= col_max && col >= col_min &&
                    row <= row_max && row >= row_min)
                {
                    // if so, set in order
//                    ER_LOG_DEBUG("Adding inner: col: %d  row: %d", col, row);
                    found++;
                    CV_Assert(q.corners[j]);
                    orderQuad(*neighbor, *q.corners[j], (j+2)&3);
                    neighbor->ordered = true;
                    neighbor->row = row;
                    neighbor->col = col;
                }
            }
        }
    }

    // if we have found inner quads, add corresponding outer quads,
    //   which are missing
    if (found > 0)
    {
    	ER_LOG_DEBUG("Found " << found << " inner quads not connected to outer quads, repairing");
        for (int i = 0; i < quad_count && all_quads_count < max_quad_buf_size; i++)
        {
            ChessBoardQuad& q = *quads[i];
            if (q.count < 4 && q.ordered)
            {
                int added = addOuterQuad(q, quads);
                quad_count += added;
            }
        }

        if (all_quads_count >= max_quad_buf_size)
            return 0;
    }

    ER_LOG_DEBUG("Grid:");

//    for (int i = 0; i < quad_count; i++)
//    {
//    	std::cout << quads[i]->col << " - " <<  quads[i]->row << std::endl;
//    }

    ER_LOG_DEBUG(dcol << " - " << w << " - " << drow << " - " << h);


    // final trimming of outer quads
    if (dcol == w+1 && drow == h+1) // found correct inner quads
    {
    	ER_LOG_DEBUG("Inner bounds ok, check outer quads");
        for (int i = quad_count - 1; i >= 0; i--) // eliminate any quad not connected to an ordered quad
        {
            ChessBoardQuad& q = *quads[i];
            if (q.ordered == false)
            {
                bool outer = false;
                for (int j=0; j<4; j++) // any neighbors that are ordered?
                {
                    if (q.neighbors[j] && q.neighbors[j]->ordered)
                        outer = true;
                }
                if (!outer) // not an outer quad, eliminate
                {
                	ER_LOG_DEBUG("Removing quad " << i);
                    removeQuadFromGroup(quads, q);
                }
            }

        }
        return (int)quads.size();
    }

    return 0;
}

// if we found too many connect quads, remove those which probably do not belong.
int ERMarkerDetector::cleanFoundConnectedQuads(std::vector<ChessBoardQuad*>& quad_group)
{
    // number of quads this pattern should contain
    int count = ((pattern_size.width + 1)*(pattern_size.height + 1) + 1)/2;

    // if we have more quadrangles than we should,
    // try to eliminate duplicates or ones which don't belong to the pattern rectangle...
    int quad_count = (int)quad_group.size();
    if (quad_count <= count)
        return quad_count;
    CV_DbgAssert(quad_count > 0);

    // create an array of quadrangle centers
    cv::AutoBuffer<cv::Point2f> centers(quad_count);

    cv::Point2f center;
    for (int i = 0; i < quad_count; ++i)
    {
        ChessBoardQuad* q = quad_group[i];

        const cv::Point2f ci = (
                q->corners[0]->pt +
                q->corners[1]->pt +
                q->corners[2]->pt +
                q->corners[3]->pt
            ) * 0.25f;

        centers[i] = ci;
        center += ci;
    }
    center.x *= (1.0f / quad_count);

    // If we still have more quadrangles than we should,
    // we try to eliminate bad ones based on minimizing the bounding box.
    // We iteratively remove the point which reduces the size of
    // the bounding box of the blobs the most
    // (since we want the rectangle to be as small as possible)
    // remove the quadrange that causes the biggest reduction
    // in pattern size until we have the correct number
    for (; quad_count > count; quad_count--)
    {
        double min_box_area = DBL_MAX;
        int min_box_area_index = -1;

        // For each point, calculate box area without that point
        for (int skip = 0; skip < quad_count; ++skip)
        {
            // get bounding rectangle
            cv::Point2f temp = centers[skip]; // temporarily make index 'skip' the same as
            centers[skip] = center;            // pattern center (so it is not counted for convex hull)
            std::vector<Point2f> hull;
            Mat points(1, quad_count, CV_32FC2, &centers[0]);
            cv::convexHull(points, hull, true);
            centers[skip] = temp;
            double hull_area = contourArea(hull, true);

            // remember smallest box area
            if (hull_area < min_box_area)
            {
                min_box_area = hull_area;
                min_box_area_index = skip;
            }
        }

        ChessBoardQuad *q0 = quad_group[min_box_area_index];

        // remove any references to this quad as a neighbor
        for (int i = 0; i < quad_count; ++i)
        {
            ChessBoardQuad *q = quad_group[i];
            for (int j = 0; j < 4; ++j)
            {
                if (q->neighbors[j] == q0)
                {
                    q->neighbors[j] = 0;
                    q->count--;
                    for (int k = 0; k < 4; ++k)
                    {
                        if (q0->neighbors[k] == q)
                        {
                            q0->neighbors[k] = 0;
                            q0->count--;
                            break;
                        }
                    }
                    break;
                }
            }
        }

        // remove the quad
        quad_count--;
        quad_group[min_box_area_index] = quad_group[quad_count];
        centers[min_box_area_index] = centers[quad_count];
    }

    return quad_count;
}

int ERMarkerDetector::findFirstLine(std::vector<ChessBoardCorner*>& out_corners, bool inverted, ChessBoardCorner** cur, ChessBoardCorner** right, ChessBoardCorner** below, ChessBoardCorner** first, CvMat &debug_img){

    for (int k = 0; k < 4; ++k)
    {
        ChessBoardCorner* c = (*cur)->neighbors[k];
        if (c)
        {
            if (!(*right))
            	(*right) = c;
            else if (!(*below))
            	(*below) = c;
        }
    }

    if(inverted){
    	ChessBoardCorner* cc = (*right);
    	(*right) = (*below);
    	(*below) = cc;
    }

    ER_LOG_LINE;
    ER_LOG_DEBUG(!right);
    ER_LOG_DEBUG(!(*below));
    ER_LOG_DEBUG((*right)->count);
    ER_LOG_DEBUG((*below)->count);

    	// Check that first has right and below and each have 3 neighbors
    if( !(*right) || (*right)->count != 3 ||
        !(*below) || (*below)->count != 3){
        ER_LOG_LINE;
        return -1;
    }

    ER_LOG_LINE;

    (*cur)->row = 0;

    //drawStuff(&debug_img, cvPointFrom32f((*cur)->pt), 4, cvScalar(0,255,0), (*cur)->row);

    (*first) = (*below); // remember the first corner in the next row

    int n;

    // find and store the first row (or column)
    for (int j = 1; ; ++j)
    {
        if( !(*right)){
            ER_LOG_LINE;
            break;
        }

        (*right)->row = 0;
        out_corners.push_back((*right));

        //drawStuff(&debug_img, cvPointFrom32f((*right)->pt), 4, cvScalar(255,255-j*10,0), (*right)->row);


//        if( right->count == 2 ){
//            ER_LOG_LINE;
//            break;
//        }
//        if( right->count != 3 || (int)out_corners.size() >= std::max(pattern_size.width,pattern_size.height) ){
//            ER_LOG_LINE;
//            goto finalize;
//        }

        (*cur) = (*right);
        (*right) = NULL;
        for (int k = 0; k < 4; ++k)
        {
            ChessBoardCorner* c = (*cur)->neighbors[k];
            if (c && c->row > 0)
            {
                int kk = 0;
                for (; kk < 4; ++kk)
                {
                    if (c->neighbors[kk] == (*below))
                        break;
                }
                if (kk < 4)
                	(*below) = c;
                else
                	(*right) = c;
            }
        }
    }

    return out_corners.size();

}

int ERMarkerDetector::checkQuadGroup(std::vector<ChessBoardQuad*>& quad_group, std::vector<ChessBoardCorner*>& out_cornersIn)
{
	std::vector<ChessBoardCorner*> out_corners;
    const int ROW1 = 1000000;
    const int ROW2 = 2000000;
    const int ROW_ = 3000000;
    const int ROWUNDF = 999;

    const int COLOROFFSETPERROW = 20;

    int quad_count = (int)quad_group.size();
    CvMat* debug_img;
	CvMat debug_img1;
	debug_img1 = origimg.clone();
	debug_img = &debug_img1;
//    imshow("tet", origimg);
//    waitKey(0);
    std::vector<ChessBoardCorner*> corners(quad_count*4);
    int corner_count = 0;
    int result = 0;

    int width = 0, height = 0;
    int hist[5] = {0,0,0,0,0};
    //ChessBoardCorner* first = 0, *first2 = 0, *right, *cur, *below, *c;

    // build dual graph, which vertices are internal quad corners
    // and two vertices are connected iff they lie on the same quad edge
    for (int i = 0; i < quad_count; ++i)
    {
        ChessBoardQuad* q = quad_group[i];
        CvScalar color = q->count == 0 ? cvScalar(0,255,255) :
                         q->count == 1 ? cvScalar(0,0,255) :
                         q->count == 2 ? cvScalar(0,255,0) :
                         q->count == 3 ? cvScalar(255,255,0) :
                                         cvScalar(255,0,0);

        for (int j = 0; j < 4; ++j)
        {
            cvLine(debug_img, cvPointFrom32f(q->corners[j]->pt), cvPointFrom32f(q->corners[(j+1)&3]->pt), color, 1, CV_AA, 0 );
            if (q->neighbors[j])
            {
                int next_j = (j + 1) & 3;
                ChessBoardCorner *a = q->corners[j], *b = q->corners[next_j];
                // mark internal corners that belong to:
                //   - a quad with a single neighbor - with ROW1,
                //   - a quad with two neighbors     - with ROW2
                // make the rest of internal corners with ROW_
                int row_flag = q->count == 1 ? ROW1 : q->count == 2 ? ROW2 : ROW_;

                if (a->row == 0)
                {
                    corners[corner_count++] = a;
                    a->row = row_flag;
                }
                else if (a->row > row_flag)
                {
                    a->row = row_flag;
                }

                if (q->neighbors[next_j])
                {
                    if (a->count >= 4 || b->count >= 4)
                        goto finalize;
                    for (int k = 0; k < 4; ++k)
                    {
                        if (a->neighbors[k] == b)
                            goto finalize;
                        if (b->neighbors[k] == a)
                            goto finalize;
                    }
                    a->neighbors[a->count++] = b;
                    b->neighbors[b->count++] = a;
                }
            }
        }
    }

//    if (corner_count != pattern_size.width*pattern_size.height){
//    	ER_LOG_DEBUG("Wrong corner count");
//    	goto finalize;
//    }

{
    ChessBoardCorner* first = NULL, *first2 = NULL;
    for (int i = 0; i < corner_count; ++i)
    {
        int n = corners[i]->count;
        CV_DbgAssert(0 <= n && n <= 4);
        hist[n]++;
        if (!first && n == 2)
        {
            if (corners[i]->row == ROW1)
                first = corners[i];
            else if (!first2 && corners[i]->row == ROW2)
                first2 = corners[i];
        }
    }

    // start with a corner that belongs to a quad with a single neighbor.
    // if we do not have such, start with a corner of a quad with two neighbors.
    if( !first )
        first = first2;

    ER_LOG_DEBUG("hist0: " << hist[0]);
    ER_LOG_DEBUG("hist1: " << hist[1]);
    ER_LOG_DEBUG("hist2: " << hist[2]);
    ER_LOG_DEBUG("hist3: " << hist[3]);
    ER_LOG_DEBUG("hist4: " << hist[4]);
    ER_LOG_DEBUG(( hist[3] != (pattern_size.width + pattern_size.height)*2 - 8 ));

    if( !first || hist[0] != 0 || hist[1] != 0 || hist[2] != 10 || hist[3] != 14  ){ //||hist[3] != (pattern_size.width + pattern_size.height)*2 - 8
    	ER_LOG_LINE;
    	goto finalize;
    }

    ChessBoardCorner* alltFirst = first;
    ChessBoardCorner* cur = first;
    ChessBoardCorner* next = NULL;
    ChessBoardCorner* right = NULL;
    ChessBoardCorner* below = NULL;
    out_corners.push_back(cur);
    cur->firstInRow = true;

    int n;
    double sumDistFirst = first->sumDist(n)/n;


    findFirstLine(out_corners, false, &cur, &right, &below, &first, *debug_img);

    if(out_corners.size() < std::max(pattern_size.width, pattern_size.height)){

    	ER_LOG_DEBUG("Got first line wrong, trying inverted");

    	debug_img1 = origimg.clone();
    	debug_img = &debug_img1;

    	for(int l = 0; l < out_corners.size(); l++){
            int row_flag = out_corners[l]->count == 1 ? ROW1 : out_corners[l]->count == 2 ? ROW2 : ROW_;
            out_corners[l]->row = row_flag;
    	}

        first = alltFirst;
        cur = first;
        next = NULL;
        right = NULL;
        below = NULL;
        out_corners.clear();
        out_corners.push_back(cur);
        cur->firstInRow = true;

        findFirstLine(out_corners, true, &cur, &right, &below, &first, *debug_img);
    }

    if(out_corners.size() < std::max(pattern_size.width, pattern_size.height)){
    	ER_LOG_DEBUG("First line still wrong, returning");

    	goto finalize;
    }


    width = (int)out_corners.size();

    ER_LOG_DEBUG("width: " << width);

    if (width == pattern_size.width)
        height = pattern_size.height;
    else if (width == pattern_size.height)
        height = pattern_size.width;
    else{
    	ER_LOG_DEBUG("Width doesnt fit either pattern width or height");
        goto finalize;
    }

    ER_LOG_DEBUG("height: " << height);


    // find and store the next row (the one leading up to the ArUco part)
    std::vector<ChessBoardCorner*> secondRowCorners; // = new std::vector<ChessBoardCorner*>;
    for (int i = 1; ; ++i)
    {
        if( !first ){
            ER_LOG_LINE;
        	break;
        }
        cur = first;
        cur->firstInRow = true;
        first = 0;
        int j = 0;
        for (; ; ++j)
        {
        	secondRowCorners.push_back(cur);
            cur->row = i;
            out_corners.push_back(cur);

            //drawStuff(debug_img, cvPointFrom32f(cur->pt), 4, cvScalar(100*cur->firstInRow,0,255-(cur->row*COLOROFFSETPERROW)), cur->row );

        	ER_LOG_DEBUG("count: " << cur->count << " - " << 2 + (i < height-1));
        	ER_LOG_DEBUG("j: " << j << " - i: " << i);
            right = 0;

            if (cur->count == 2 + (i < height-1) && j > 3){
                ER_LOG_LINE;
            	break;
            }


            // find a neighbor that has not been processed yet
            // and that has a neighbor from the previous row (only if previous row was evaluated as "right")
            for (int k = 0; k < 4; ++k)
            {
                ChessBoardCorner* c = cur->neighbors[k];
                if (c && c->row > i)
                {
                    int kk = 0;
                    for (; kk < 4; ++kk)
                    {
						if (c->neighbors[kk] && c->neighbors[kk]->row == i-1){
							ER_LOG_LINE;
							break;
						}
                    }
                    if(kk < 4)
                    {
                        right = c;
                        if (j > 0){
                            ER_LOG_LINE;
                        	break;
                        }
                    }
                    else if (j == 0)
                        first = c;
                }
            }
            if (!right){
				break;
            }
            else
            	cur = right;
        }
        if (!right){
			break;
        }

        	// Check should check if j = expected width of attern
//        if (j != width - 1){
//        	ER_LOG_LINE;
//            goto finalize;
//        }
    }

    ER_LOG_DEBUG("secondRowCorners: " << secondRowCorners.size());

//    if(secondRowCorners.size() != pattern_size.width - 2 && secondRowCorners.size() != pattern_size.height - 3){
//    	ER_LOG_DEBUG("Number of corners in second row doesnt match neither width or height of given pattern size");
//    	ER_LOG_DEBUG("Second row corners: " << secondRowCorners.size() << " -  pattern width and height: " <<  pattern_size.width << " - " << pattern_size.height);
//    	goto finalize;
//    }

    std::vector<ChessBoardCorner*> thirdRowCorners; // = new std::vector<ChessBoardCorner*>;
    for(int i = 0; i < secondRowCorners.size(); i++){
    	cur = secondRowCorners[i];
    	int k = 0;
        for (; k < 4; ++k)
        {
        	// See if neighbour exists and if not initialized
        	if(cur->neighbors[k] && cur->neighbors[k]->row > 1000){
                ChessBoardCorner* c = cur->neighbors[k];

                int n;
                	// If neighbour is edge of ArUco marker
                if( c->sumDist(n)/n > sumDistFirst * 1.5)
                	break;
                thirdRowCorners.push_back(c);

                if(secondRowCorners[i]->firstInRow)
                	c->firstInRow=true;

                c->row = 2;
                out_corners.push_back(c);

                //drawStuff(debug_img, cvPointFrom32f(c->pt), 4, cvScalar(100*c->firstInRow,0,255-(c->row*COLOROFFSETPERROW)), c->row );

        	}

        }

    }

    std::vector<ChessBoardCorner*> fourthRowCorners; // = new std::vector<ChessBoardCorner*>;
    for(int i = 0; i < thirdRowCorners.size(); i++){
    	cur = thirdRowCorners[i];
    	int k = 0;
    	ER_LOG_DEBUG("");
        for (; k < 4; ++k)
        {
        	// See if neighbour exists and if not initialized
        	if(cur->neighbors[k] && cur->neighbors[k]->row > 1000){
                ChessBoardCorner* c = cur->neighbors[k];

                int n;
                	// If neighbour is edge of ArUco marker
                if( c->sumDist(n)/n > sumDistFirst * 1.5)
                	break;
                fourthRowCorners.push_back(c);

                if(thirdRowCorners[i]->firstInRow)
                	c->firstInRow=true;

                c->row = 3;
                out_corners.push_back(c);

                //drawStuff(debug_img, cvPointFrom32f(c->pt), 4, cvScalar(100*c->firstInRow,0,255-(c->row*COLOROFFSETPERROW)), c->row );

        	}
        }

    }

    std::vector<ChessBoardCorner*> fifthRowCorners; // = new std::vector<ChessBoardCorner*>;
    for(int i = 0; i < fourthRowCorners.size(); i++){
    	cur = fourthRowCorners[i];
    	int k = 0;
    	ER_LOG_DEBUG("");
        for (; k < 4; ++k)
        {
        	// See if neighbour exists and if not initialized
        	if(cur->neighbors[k] && cur->neighbors[k]->row > 1000){
                ChessBoardCorner* c = cur->neighbors[k];

                int n;
                	// If neighbour is edge of ArUco marker
                if( c->sumDist(n)/n > sumDistFirst * 1.5)
                	break;
                fifthRowCorners.push_back(c);

                if(fourthRowCorners[i]->firstInRow)
                	c->firstInRow=true;

                c->row = 4;
                out_corners.push_back(c);

                //drawStuff(debug_img, cvPointFrom32f(c->pt), 4, cvScalar(100*c->firstInRow,0,255-(c->row*COLOROFFSETPERROW)), c->row );

        	}
        }

    }

    ER_LOG_LINE;
    std::vector<ChessBoardCorner*> sixthRowCorners; // = new std::vector<ChessBoardCorner*>;
    for(int i = 0; i < fifthRowCorners.size(); i++){
    	cur = fifthRowCorners[i];
    	int k = 0;
    	ER_LOG_DEBUG("");
        for (; k < 4; ++k)
        {
        	// See if neighbour exists and if not initialized
        	if(cur->neighbors[k] && cur->neighbors[k]->row > 1000){
                ChessBoardCorner* c = cur->neighbors[k];

                int n;
                	// If neighbour is edge of ArUco marker
                if( c->sumDist(n)/n > sumDistFirst * 1.5){
                	ER_LOG_DEBUG("Sum dist larger");
                	fifthRowCorners.push_back(c);
                    c->row = 4;
                }
                else{
                	ER_LOG_LINE;
                    c->row = 5;
                    sixthRowCorners.push_back(c);
                    if(fifthRowCorners[i]->firstInRow){
                    	c->firstInRow=true;
                    	first = c;
                    }
                }


                out_corners.push_back(c);

                //drawStuff(debug_img, cvPointFrom32f(c->pt), 4, cvScalar(100*c->firstInRow,0,255-(c->row*COLOROFFSETPERROW)), c->row );

        	}
        }

    }

    ER_LOG_LINE;

    //Find first in next row
    for(int i = 0; i < first->count; i++){
    	cur = first->neighbors[i];
    	if(cur->row > 1000){
    		cur->row = first->row+1;
    		cur->firstInRow = true;
    		out_corners.push_back(cur);

            //drawStuff(debug_img, cvPointFrom32f(cur->pt), 4, cvScalar(100*cur->firstInRow,0,255-(cur->row*COLOROFFSETPERROW)), cur->row );

    		first = cur;
    		break;
    	}
    	cur = 0;
    }

    	// If no below nbeighbour foiund, use next in line
    if(cur == 0){
    	ER_LOG_DEBUG("No CUR");
        for(int i = 0; i < first->count; i++){
        	cur = first->neighbors[i];
        	ER_LOG_DEBUG("first row: " << first->row);
        	ER_LOG_DEBUG("Cur row: " << cur->row);
        	if(cur->row == first->row){
        		first = cur;
        		break;
        	}
        }
    }

    if(cur == 0)
    	goto finalize;

    	// Find and store rest of the rows, starting with the latest firstInRow corner
//    // find and store all the other rows

    std::queue<ChessBoardCorner*> openList;
    openList.push(first);

    right = 0;
    int i = 5;
    while(!openList.empty()){
        cur = openList.front();
        openList.pop();


    	if(cur && cur->row > 1000){

    			// Figure which row current corner belongds to
    		std::vector<int> neighbourRows;
    		for(int k = 0; k < cur->count; k++){
    			if(cur->neighbors[k]->row < 1000){
    				neighbourRows.push_back(cur->neighbors[k]->row );
    			}
    		}

    		if(neighbourRows.size() == 1)
    			cur->row = ROWUNDF;

    		cur->row = i;
        	out_corners.push_back(cur);

            //drawStuff(debug_img, cvPointFrom32f(cur->pt), 4, cvScalar(100*cur->firstInRow,0,255-(cur->row*COLOROFFSETPERROW)), cur->row );

    	}

//            if (cur->count == 2 + (i < height-1) && j > 0)
//                break;
        for (int k = 0; k < 4; ++k)
        {
            ChessBoardCorner* c = cur->neighbors[k];
            if (c && c->row > 1000)
            {
            	openList.push(c);
//                int kk = 0;
//                for (; kk < c->count; ++kk)
//                {
//                	ER_LOG_DEBUG(c->neighbors[kk]->row);
//                    if (c->neighbors[kk] && c->neighbors[kk]->row <= i){
//                    	ER_LOG_LINE;
//                        break;
//                    }
//                }
//                if(kk < c->count)
//                {
//                    right = c;
//                    if (j > 0){
//                    	ER_LOG_LINE;
//                        break;
//                    }
//                }
//                else if (j == 0)
//                    first = c;
            }
        }

    	cur = 0;

    }

    ER_LOG_DEBUG("Corners: " << out_corners.size() << " - " << corner_count);

    if ((int)out_corners.size() != corner_count ){
    	ER_LOG_LINE;
    	goto finalize;
    }

    	// Sort corners, we know that the first 19 corners are correct
    for(int i = 0; i < 19; i++){
    	out_cornersIn.push_back(out_corners[i]);
        drawStuff(debug_img, cvPointFrom32f(out_corners[i]->pt), 4, cvScalar(100*out_corners[i]->firstInRow,0,255-(out_corners[i]->row*COLOROFFSETPERROW)), out_corners[i]->row );

    }

    ER_LOG_DEBUG("Size here: " << out_cornersIn.size());

    first = out_corners[18];
    cur = first;
    ChessBoardCorner* last = NULL;


    	// Sort corners in same row as 19th corner
    for(int i = 0; i < std::max(pattern_size.height, pattern_size.width) ; i++){
    	for(int k = 0; k < cur->count; k++){
    		if(cur->row == cur->neighbors[k]->row && cur->neighbors[k] != last){
    			out_cornersIn.push_back(cur->neighbors[k]);
    	        drawStuff(debug_img, cvPointFrom32f(cur->neighbors[k]->pt), 4, cvScalar(100*cur->neighbors[k]->firstInRow,0,255-(cur->neighbors[k]->row*COLOROFFSETPERROW)), cur->neighbors[k]->row );

    			last = cur;
    			cur = cur->neighbors[k];
    		}
    	}
    }
    ER_LOG_DEBUG("Size here: " << out_cornersIn.size());

    	// Sort corners in row below 19th corner
    for(int i = 0; i < first->count; i++){
    	if(first->neighbors[i]->row > first->row){
    		cur = first->neighbors[i];
    		break;
    	}
    }

    out_cornersIn.push_back(cur);
    drawStuff(debug_img, cvPointFrom32f(cur->pt), 4, cvScalar(100*cur->firstInRow,0,255-(cur->row*COLOROFFSETPERROW)), cur->row );

    for(int i = 0; i < std::max(pattern_size.height, pattern_size.width) ; i++){
    	ER_LOG_DEBUG("ERERER");
        ER_LOG_DEBUG("Size here: " << out_cornersIn.size());
    	for(int k = 0; k < cur->count; k++){
    		if(cur->row == cur->neighbors[k]->row && cur->neighbors[k] != last){
    			out_cornersIn.push_back(cur->neighbors[k]);
    			last = cur;
    			cur = cur->neighbors[k];
                drawStuff(debug_img, cvPointFrom32f(cur->pt), 4, cvScalar(100*cur->firstInRow,0,255-(cur->row*COLOROFFSETPERROW)), cur->row );

    		}
    	}
    }

    ER_LOG_DEBUG("Size here: " << out_cornersIn.size());

//
//    // check if we need to revert the order in each row
    reverted = false;
        cv::Point2f p0 = out_cornersIn[0]->pt,
                    p1 = out_cornersIn[pattern_size.width-1]->pt,
                    p2 = out_cornersIn[pattern_size.width]->pt;
        if( (p1.x - p0.x)*(p2.y - p1.y) - (p1.y - p0.y)*(p2.x - p1.x) < 0 )
        {
        	reverted = true;
        	ER_LOG_DEBUG( "Should be reverted" );

        }
//    }

    result = out_cornersIn.size();
}

finalize:

	if(SHOW_CORNER_PROCESS){
		cv::Mat dst = cvarrToMat(debug_img);
//		cv::resize(dst, dst, cv::Size(), 4, 4);
		imshow("tssest",  dst);
		waitKey(0);
	}


	ER_LOG_DEBUG("Result: " << result);

    if (result <= 0)
    {
        corner_count = std::min(corner_count, pattern_size.area());
        out_cornersIn.resize(corner_count);
        for (int i = 0; i < corner_count; i++)
        	out_cornersIn[i] = corners[i];

        result = -corner_count;

        if (result == -pattern_size.area())
            result = -result;
    }

    return result;
}

bool ERMarkerDetector::checkBoardMonotony(const std::vector<cv::Point2f>& corners)
{
    for (int k = 0; k < 2; ++k)
    {
        int max_i = (k == 0 ? pattern_size.height : pattern_size.width);
        int max_j = (k == 0 ? pattern_size.width: pattern_size.height) - 1;
        for (int i = 0; i < max_i; ++i)
        {
            cv::Point2f a = k == 0 ? corners[i*pattern_size.width] : corners[i];
            cv::Point2f b = k == 0 ? corners[(i+1)*pattern_size.width-1]
                                   : corners[(pattern_size.height-1)*pattern_size.width + i];
            float dx0 = b.x - a.x, dy0 = b.y - a.y;
            if (fabs(dx0) + fabs(dy0) < FLT_EPSILON)
                return false;
            float prevt = 0;
            for (int j = 1; j < max_j; ++j)
            {
                cv::Point2f c = k == 0 ? corners[i*pattern_size.width + j]
                                       : corners[j*pattern_size.width + i];
                float t = ((c.x - a.x)*dx0 + (c.y - a.y)*dy0)/(dx0*dx0 + dy0*dy0);
                if (t < prevt || t > 1)
                    return false;
                prevt = t;
            }
        }
    }
    return true;
}

void ERMarkerDetector::orderQuad(ChessBoardQuad& quad, ChessBoardCorner& corner, int common)
{
    CV_DbgAssert(common >= 0 && common <= 3);

    // find the corner
    int tc = 0;;
    for (; tc < 4; ++tc)
        if (quad.corners[tc]->pt == corner.pt)
            break;

    // set corner order
    // shift
    while (tc != common)
    {
        // shift by one
        ChessBoardCorner *tempc = quad.corners[3];
        ChessBoardQuad *tempq = quad.neighbors[3];
        for (int i = 3; i > 0; --i)
        {
            quad.corners[i] = quad.corners[i-1];
            quad.neighbors[i] = quad.neighbors[i-1];
        }
        quad.corners[0] = tempc;
        quad.neighbors[0] = tempq;
        tc = (tc + 1) & 3;
    }
}

// add an outer quad
// looks for the neighbor of <quad> that isn't present,
//   tries to add it in.
// <quad> is ordered
int ERMarkerDetector::addOuterQuad(ChessBoardQuad& quad, std::vector<ChessBoardQuad*>& quads)
{
    int added = 0;
    int max_quad_buf_size = (int)all_quads.size();

    for (int i = 0; i < 4 && all_quads_count < max_quad_buf_size; i++) // find no-neighbor corners
    {
        if (!quad.neighbors[i])    // ok, create and add neighbor
        {
            int j = (i+2)&3;
            ER_LOG_DEBUG("Adding quad as neighbor 2");
            int q_index = all_quads_count++;
            ChessBoardQuad& q = all_quads[q_index];
            q = ChessBoardQuad(0);
            added++;
            quads.push_back(&q);

            // set neighbor and group id
            quad.neighbors[i] = &q;
            quad.count += 1;
            q.neighbors[j] = &quad;
            q.group_idx = quad.group_idx;
            q.count = 1;   // number of neighbors
            q.ordered = false;
            q.edge_len = quad.edge_len;

            // make corners of new quad
            // same as neighbor quad, but offset
            const cv::Point2f pt_offset = quad.corners[i]->pt - quad.corners[j]->pt;
            for (int k = 0; k < 4; k++)
            {
                ChessBoardCorner& corner = (ChessBoardCorner&)all_corners[q_index * 4 + k];
                const cv::Point2f& pt = quad.corners[k]->pt;
                corner = ChessBoardCorner(pt);
                q.corners[k] = &corner;
                corner.pt += pt_offset;
            }
            // have to set exact corner
            q.corners[j] = quad.corners[i];

            // now find other neighbor and add it, if possible
            int next_i = (i + 1) & 3;
            int prev_i = (i + 3) & 3; // equal to (j + 1) & 3
            ChessBoardQuad* quad_prev = quad.neighbors[prev_i];
            if (quad_prev &&
                quad_prev->ordered &&
                quad_prev->neighbors[i] &&
                quad_prev->neighbors[i]->ordered )
            {
                ChessBoardQuad* qn = quad_prev->neighbors[i];
                q.count = 2;
                q.neighbors[prev_i] = qn;
                qn->neighbors[next_i] = &q;
                qn->count += 1;
                // have to set exact corner
                q.corners[prev_i] = qn->corners[next_i];
            }
        }
    }
    return added;
}

//
// remove quad from quad group
//
void ERMarkerDetector::removeQuadFromGroup(std::vector<ChessBoardQuad*>& quads, ChessBoardQuad& q0)
{
    const int count = (int)quads.size();

    int self_idx = -1;

    // remove any references to this quad as a neighbor
    for (int i = 0; i < count; ++i)
    {
        ChessBoardQuad* q = quads[i];
        if (q == &q0)
            self_idx = i;
        for (int j = 0; j < 4; j++)
        {
            if (q->neighbors[j] == &q0)
            {
                q->neighbors[j] = NULL;
                q->count--;
                for (int k = 0; k < 4; ++k)
                {
                    if (q0.neighbors[k] == q)
                    {
                        q0.neighbors[k] = 0;
                        q0.count--;
#ifndef _DEBUG
                        break;
#endif
                    }
                }
                break;
            }
        }
    }
    CV_Assert(self_idx >= 0); // item itself should be found

    // remove the quad
    if (self_idx != count-1)
        quads[self_idx] = quads[count-1];
    quads.resize(count - 1);
}

void ERMarkerDetector::setOrigImage(cv::Mat img){
	origimg = img.clone();
}

