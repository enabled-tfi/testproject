/* */
#ifndef EXM_IF_HPP
#define EXM_IF_HPP

#include "Instruction.hpp"
#include "Argument.hpp"

#include <rw/common/Ptr.hpp>
#include <rw/common/PropertyMap.hpp>

namespace exm {
	


/**
 * Implements an If with optional else instruction
 */
class If: public Instruction
{
public:
	/** @brief Smart pointer to the If object*/
	typedef rw::common::Ptr<If> Ptr;

	If();

	/**
	 * @brief Construct If instruction
	 *
	 * @param argument [in] Argument specifying whether to execute the primary body (in case of true) or the optional else. Assumes evaluate to a boolean.
	 * @param ifBody [in] The instruction to be implemented in case the argument evaluates to true.
	 * @param elseBody [in] Optional else instruction, to be executed if the argument evaluate to false. Defaults to NULL.
	 */
	If(Argument::Ptr argument, Instruction::Ptr ifBody, Instruction::Ptr elseBody = NULL);

	/**
	 * @copydoc Instruction::execute
	 */
	bool execute(ExecutionManager::Ptr execManager);


    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


protected:
	void initializeSettings();

private:

	er_common::Setting<Argument::Ptr>::Ptr _argument;
	er_common::Setting<Instruction::Ptr>::Ptr _ifBody;
	er_common::Setting<Instruction::Ptr>::Ptr _elseBody;
};

} //end namespace

#endif //#ifndef EXM_IF_HPP
