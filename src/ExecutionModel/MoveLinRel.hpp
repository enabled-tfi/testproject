/* */
#ifndef EXM_MOVELINREL_HPP
#define EXM_MOVELINREL_HPP

#include "Instruction.hpp"
#include "ExecutionManager.hpp"
#include "../HardwareLayer/Manipulator.hpp"
#include <Common/EntityReference.hpp>
#include <rw/math.hpp>

#include <vector>
namespace exm {
	
/**
 * @brief Action for moving robot relative to the current pose with a linear path in tool space
 */
class MoveLinRel: public Instruction
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<MoveLinRel> Ptr;

	MoveLinRel();


	MoveLinRel(const rw::math::Transform3D<>& currentTtarget, const std::string& deviceIdentifier);


	/**
	 * @brief Constructs MoveLin action moving to \btarget using \bdeviceInterface
	 * @param target [in] Target to which to move.
	 * @param deviceInterface [in] Interface to the device to move
	 */
	MoveLinRel(const rw::math::Transform3D<>& currentTtarget, hwl::Manipulator::Ptr deviceInterface);


    bool execute(ExecutionManager::Ptr execManager);


    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


protected:
   void initializeSettings();

private:
	er_common::Setting<rw::math::Transform3D<> >::Ptr _target;
	//er_common::Setting<hwl::Manipulator::Ptr>::Ptr _deviceInterface;
	er_common::Setting < er_common::EntityReference<hwl::Manipulator> >::Ptr _deviceInterface;
};

} //end namespace

#endif //#ifndef EXM_MOVELIN_HPP
