/* */
#ifndef EXM_MARKERDETECTIONCHARUCO_HPP
#define EXM_MARKERDETECTIONCHARUCO_HPP

#include "MarkerDetection.hpp"
#include "ExecutionManager.hpp"
#include <Common/EntityReference.hpp>
#include <HardwareLayer/Camera.hpp>

#include <ostream>
#include <string>

namespace exm {

/** 
 * @brief Performs a marker detection based on the OpenCV ChArUco marker detection
 */
class MarkerDetectionChArUco: public MarkerDetection
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<MarkerDetectionChArUco> Ptr;

	/**
	 * @brief Default Constructor
	 */
	MarkerDetectionChArUco();

	MarkerDetectionChArUco(hwl::Camera::Ptr camera, int execCount = 0);

	bool execute(ExecutionManager::Ptr execManager);

    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

	const cv::Mat& getDetectionImage() const;

	double getMeanProjectionError();

	bool isDetected();


protected:
    virtual void initializeSettings();
    double calcMeanErrorOfProjection(std::vector<cv::Point3f> boardPoints, cv::Mat rvecs, cv::Mat tvecs, std::pair<cv::Mat, cv::Mat> cameraMatrixAndDistCoeffs, std::vector<cv::Point2f> points);


private:

	er_common::Setting < er_common::EntityReference<hwl::Camera> >::Ptr _camera;
	er_common::Setting<double>::Ptr _markerSize;
	//er_common::Setting<int>::Ptr _arucoMarkerId;
	er_common::Setting<std::string>::Ptr _knowledgeBaseIdentifier;
	int _executionCount;
	cv::Mat _detectionImage;
	//er_common::Setting<hwl::Camera::Ptr>::Ptr _camera;
	bool _detected;
	double _meanProjectionError;

};

} //end namespace

#endif //#ifndef EXM_MARKERDETECTIONCHARUCO_HPP
