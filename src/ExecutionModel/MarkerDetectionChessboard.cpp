#include "MarkerDetectionChessboard.hpp"

#include <cmath>

#include <rw/math/EAA.hpp>
#include <rw/math/Transform3D.hpp>
#include <Common/EntityRegister.hpp>

#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>

#include <opencv2/aruco.hpp>

using namespace exm;
using namespace hwl;
using namespace er_common;
using namespace rw::math;
using namespace rw::kinematics;
using namespace rw::common;
using namespace cv;

MarkerDetectionChessboard::MarkerDetectionChessboard() :
    MarkerDetection("MarkerDetectionChessboard", "")
{
	initializeSettings();
	setIsInitialized(false);
	_executionCount = 0;
}

MarkerDetectionChessboard::MarkerDetectionChessboard(Camera::Ptr camera, int execCount) :
    MarkerDetection("MarkerDetectionChessboard", "")
{
	initializeSettings();
	_camera->getValue().set(camera);
	setIsInitialized(true);
	_executionCount = execCount;
}


void MarkerDetectionChessboard::initializeSettings()
{
	_camera = getSettings().add<EntityReference<hwl::Camera> >("Camera", "Interface of the camera", EntityReference<hwl::Camera>("Camera"));
	_squareSize = getSettings().add<double>("Chessboard_SquareSize", "Size of squares on chessboard in meter", 0.1);
	_squareSize->setRange(0, 1.0);

	_rows = getSettings().add<int>("Chessboard_Rows", "Number of rows on chess board marker", 3);
	_rows->setRange(0, 20);

	_cols = getSettings().add<int>("Chessboard_Columns", "Number of colums on chess board marker", 3);
	_cols->setRange(0, 20);

	_knowledgeBaseIdentifier = getSettings().add<std::string>("Chessboard_KnowledgebaseId", "Identifier for the results in the knowledge base", "ChessboardPose");

}

const cv::Mat& MarkerDetectionChessboard::getDetectionImage() const
{
	return _detectionImage;
}


bool MarkerDetectionChessboard::execute(ExecutionManager::Ptr execManager)
{
	_meanProjectionError = -1;
	_executionCount++;
	try {
		bool saveImages = er_common::System::getInstance()->getSaveImages();

		ER_LOG_DEBUG("MarkerDetectionChessboard");
		//Acquire Image
		Camera::Ptr camera = _camera->getValue().get();
		//ER_LOG_DEBUG("Camera = " << camera );

			// TODO: Is this really necessary?
		for (int i = 0; i < 5; i++) {
			camera->acquire();
			TimerUtil::sleepMs(100); 
		}

		cv::Mat inputImage = camera->getLastImage();

		if(saveImages){
			std::stringstream imageOriginalfile;
			imageOriginalfile << er_common::System::getInstance()->getMarkerImageFolder() << "/ChessboardImages/" << _executionCount  << "_ChessboardInputImage" << ".png";
			imwrite(imageOriginalfile.str(), inputImage);

		}

		cv::Mat greyImage;
		cvtColor(inputImage, greyImage, CV_BGR2GRAY);

		if(saveImages){
			std::stringstream imageGreyfile;
			imageGreyfile << er_common::System::getInstance()->getMarkerImageFolder() << "/ChessboardImages/" << _executionCount  << "_ChessboardGrayImage" << ".png";
			imwrite(imageGreyfile.str(), greyImage);
		}

		std::vector<cv::Point2f> points;
		Size patternSize(_rows->getValue(), _cols->getValue());
		ER_LOG_DEBUG("Rows and cols: " << patternSize );
		//bool found = findChessboardCorners(inputImage, patternSize, points, CV_CALIB_CB_ADAPTIVE_THRESH);

		bool patternfound = findChessboardCorners(greyImage, patternSize, points, CALIB_CB_ADAPTIVE_THRESH +CALIB_CB_NORMALIZE_IMAGE + CALIB_CB_FAST_CHECK);

		if (!patternfound) {
			if(saveImages){
				std::stringstream imageNoMarkerFile;
				imageNoMarkerFile << er_common::System::getInstance()->getMarkerImageFolder() << "/ChessboardImages/" << _executionCount << "_ChessboardNoMarkerDetected_" << ".png";
				imwrite(imageNoMarkerFile.str(), inputImage);
			}
			_detected = false;
			return false;
		}

		try {
			cornerSubPix(greyImage, points, Size(11, 11), Size(-1, -1), TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
		}
		catch (std::exception& exp) {
			ER_LOG_DEBUG("Exception: " << exp.what() );
		}
		drawChessboardCorners(inputImage, patternSize, Mat(points), patternfound);

		std::vector<Point3f> boardPoints;
		float squareSize = (float)_squareSize->getValue();
		for (int i = 0; i < patternSize.height; ++i) {
			for (int j = 0; j < patternSize.width; ++j) {
				boardPoints.push_back(Point3f(j*squareSize, i*squareSize, 0));
			}
		}

		std::pair<Mat, Mat> cameraMatrixAndDistCoeffs = camera->getCameraCalibration();
		Mat  rvecs, tvecs;
		solvePnP(Mat(boardPoints), Mat(points), cameraMatrixAndDistCoeffs.first, cameraMatrixAndDistCoeffs.second, rvecs, tvecs, false);

		_meanProjectionError = calcMeanErrorOfProjection(boardPoints, rvecs, tvecs, cameraMatrixAndDistCoeffs, points);
		ER_LOG_DEBUG("Mean error of projection: " << _meanProjectionError );

		std::vector<Point3f> coordinateSystem;
		coordinateSystem.push_back(Point3f(0, 0, 0));
		coordinateSystem.push_back(Point3f(squareSize * 2, 0, 0));
		coordinateSystem.push_back(Point3f(0, squareSize * 2, 0));
		coordinateSystem.push_back(Point3f(0, 0, squareSize * 2));

		std::vector<Point2f> coordinateImagePoints;
		projectPoints(coordinateSystem, rvecs, tvecs, cameraMatrixAndDistCoeffs.first, cameraMatrixAndDistCoeffs.second, coordinateImagePoints);
		line(inputImage, coordinateImagePoints.at(0), coordinateImagePoints.at(1), Scalar(0, 0, 255), 3);
		line(inputImage, coordinateImagePoints.at(0), coordinateImagePoints.at(2), Scalar(0, 255, 0), 3);
		line(inputImage, coordinateImagePoints.at(0), coordinateImagePoints.at(3), Scalar(255, 0, 0), 3);

		if(saveImages){
			std::stringstream imageDetectedfile;
			imageDetectedfile << er_common::System::getInstance()->getMarkerImageFolder() << "/ChessboardImages/" << _executionCount  << "_ChessboardDetected_"<< ".png";
			imwrite(imageDetectedfile.str(), inputImage);
		}

		Transform3D<> cam2marker;
		cam2marker.R() = EAA<>(rvecs.at<double>(0, 0), rvecs.at<double>(0, 1), rvecs.at<double>(0, 2)).toRotation3D();
		cam2marker.P() = Vector3D<>(tvecs.at<double>(0, 0), tvecs.at<double>(0, 1), tvecs.at<double>(0, 2));

		std::stringstream projErrStr;
		projErrStr << "Mean Projection Error: " << _meanProjectionError;
		cv::putText(inputImage, projErrStr.str(), cv::Point(40,40), cv::FONT_HERSHEY_COMPLEX, 1, (0,0,255), 1, cv::LINE_AA, false);

		if(saveImages){
			std::stringstream imageoutfile;
			imageoutfile << er_common::System::getInstance()->getMarkerImageFolder() << "/ChessboardImages/" << _executionCount << "_Chessboard_" << ".png";
			imwrite(imageoutfile.str(), inputImage);
		}

		_detectionImage = inputImage;

		std::string knowledgeBaseId = _knowledgeBaseIdentifier->getValue();
		execManager->getSystem()->getKnowledgeBase()->addForce(knowledgeBaseId, "Camera to Marker transform", cam2marker);

		ER_LOG_DEBUG("\tcam2marker: XYZ = " << cam2marker.P()<<" RPY = "<<RPY<>(cam2marker.R()) );

	}
	catch (const std::exception& exp) {
		ER_LOG_ERROR("MarkerDetectionChessboard: Exception while trying to identify marker: " <<exp.what());
		RW_THROW(exp.what());
	}
	catch (...) {
		ER_LOG_ERROR("MarkerDetectionChessboard: Unknown Exception while trying to identify marker.");
		RW_THROW("Caught unknown exception while doing Chessboard Marker Detection ");
	}
	_detected = true;

	return true;
}


double MarkerDetectionChessboard::calcMeanErrorOfProjection(std::vector<Point3f> boardPoints, Mat rvecs, Mat tvecs, std::pair<Mat, Mat> cameraMatrixAndDistCoeffs, std::vector<cv::Point2f> points){
		// Projecting all points to chessboard
	std::vector<Point2f> projectedPoints;
	projectPoints(boardPoints, rvecs, tvecs, cameraMatrixAndDistCoeffs.first, cameraMatrixAndDistCoeffs.second, projectedPoints);

	double totalError = 0;

	for(size_t i = 0; i < projectedPoints.size(); i++){
		totalError += std::sqrt(pow((projectedPoints[i].x - points[i].x),2.0) + pow((projectedPoints[i].y - points[i].y),2.0) );
	}

	return totalError/projectedPoints.size();
}

double MarkerDetectionChessboard::getMeanProjectionError(){
	return _meanProjectionError;
}

bool MarkerDetectionChessboard::isDetected(){
	return _detected;
}



/* Methods from the serializable interface */
void MarkerDetectionChessboard::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    iarchive.readEnterScope("MarkerDetectionChessboard");

    MarkerDetection::read(iarchive, id);


	getSettings().read(iarchive, "Settings");

		
	iarchive.readLeaveScope("MarkerDetectionChessboard");
}

void MarkerDetectionChessboard::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("MarkerDetectionChessboard");
    MarkerDetection::write(oarchive, id);

	getSettings().write(oarchive, "Settings");
	
	oarchive.writeLeaveScope("MarkerDetectionChessboard");
}
