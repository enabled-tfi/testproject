#include "AlignICP.hpp"


#include <Common/EntityRegister.hpp>
#include <HardwareLayer/Scanner2DSimulated.hpp>
//#include <HardwareLayer/ManipulatorDummy.hpp>

#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>

#include <Eigen/Core>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/common/time.h>
#include <pcl/console/print.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/fpfh_omp.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/pcd_io.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/sample_consensus_prerejective.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/visualization/pcl_visualizer.h>


using namespace exm;
using namespace hwl;
using namespace er_common;
using namespace rw::math;
using namespace rw::kinematics;

AlignICP::AlignICP() :
	Instruction("AlignICP", "")
{
	initializeSettings();
	setIsInitialized(false);
}


AlignICP::AlignICP(const std::string& pcdFilename, rw::kinematics::FixedFrame::Ptr partFrame) :
	Instruction("AlignICP", "")
{
	initializeSettings();
	_filenamePCD->setValue(pcdFilename);
	_partFrame = partFrame;
	_partFrameName = partFrame->getName();
	setIsInitialized(true);
}



void AlignICP::initializeSettings()
{
	_filenamePCD = getSettings().add<std::string>("PCDFile", "Name of PCD File", "");
	_partFrame = NULL;

}



bool AlignICP::execute(ExecutionManager::Ptr execManager)
{
	ER_LOG_DEBUG("AlignICP" );
	if (_partFrame == NULL)
	{
		_partFrame = execManager->getSystem()->getWorkCell()->findFrame<FixedFrame>(_partFrameName);
	}

	execManager->getSystem()->update();
	State state = execManager->getSystem()->getState();


	//std::vector<Vector3D<> > pointcloud = execManager->getSystem()->getKnowledgeBase().get < std::vector<Vector3D<> > >("PointCloud");
	//= execManager->getSystem()->getPointCloud();
	Transform3D<> partTbase = Kinematics::frameTframe(_partFrame.get(), execManager->getSystem()->getManipulator()->getBase(), state);

	pcl::PointCloud<pcl::PointXYZ>::Ptr cloudModel(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloudScan(new pcl::PointCloud<pcl::PointXYZ>);


	// Get input object and scene
	std::string objectFileName = _filenamePCD->getValue();
	//std::string sceneFileName = "D:/Code/MobileManipulatorSystems/WorkCell/Parts/Cabinet.pcd";

	// Load object and scene
	pcl::console::print_highlight("Loading point clouds...\n");
	if (pcl::io::loadPCDFile<pcl::PointXYZ>(objectFileName, *cloudModel) < 0)
	{
		pcl::console::print_error("Error loading object/scene file!\n");
		return (1);
	}


	std::vector<Vector3D<> > scan = execManager->getSystem()->getKnowledgeBase()->get < std::vector<Vector3D<> > >("PointCloud");
	//std::vector<Vector3D<> > scan = execManager->getSystem()->getPointCloud();

	for (const Vector3D<>& p : scan)
	{
		Vector3D<> p_part = partTbase*p;
		pcl::PointXYZ pclPoint(p_part(0), p_part(1), p_part(2));
		cloudScan->push_back(pclPoint);
	}

	pcl::io::savePCDFile<pcl::PointXYZ>("ScanForICP.pcd", *cloudScan);

	pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
	icp.setInputSource(cloudScan);
	icp.setInputTarget(cloudModel);
	pcl::PointCloud<pcl::PointXYZ> Final;
	icp.align(Final);
	ER_LOG_DEBUG("has converged:" << icp.hasConverged() << " score: " <<
		icp.getFitnessScore() );
	ER_LOG_DEBUG(icp.getFinalTransformation() );

	Transform3D<> partTpart;
	for (size_t i = 0; i < 3; i++) {
		for (size_t j = 0; j < 4; j++) {
			partTpart(i, j) = icp.getFinalTransformation()(i, j);
		}
	}

	ER_LOG_DEBUG("Part T part = " << partTpart );

	Transform3D<> worldTpartNew = _partFrame->getFixedTransform() * inverse(partTpart);
	_partFrame->setTransform(worldTpartNew);

	return true;
}


/* Methods from the serializable interface */
void AlignICP::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    ER_LOG_DEBUG("AlignICP::read");
    iarchive.readEnterScope("AlignICP");

    Instruction::read(iarchive, id);

	_filenamePCD->setValue(iarchive.read<std::string>("PCDFileName"));
	_partFrameName = iarchive.read<std::string>("PartFrame");
	iarchive.readLeaveScope("AlignICP");
}

void AlignICP::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("AlignICP");
    Instruction::write(oarchive, id);

	oarchive.write(_filenamePCD->getValue(), "PCDFileName");
	oarchive.write(_partFrameName, "PartFrame");


	oarchive.writeLeaveScope("AlignICP");
}
