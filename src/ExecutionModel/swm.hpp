/* */
#ifndef SWM_WAIT_HPP
#define SWM_WAIT_HPP

#include "SequentialInstruction.hpp"
#include <vector>
namespace swm {

/** 
 * @brief Waits a specified amount of time before returning from the execute method.
 */
class Wait: public SequentialInstruction
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<Wait> Ptr;

	/**
	 * @brief Construct Wait instruction for waiting \btime.
	 * @param time [in] The amount of time to wait.
	 */
	Wait(double time);

protected:
	virtual void doExecute(ProgramState& state);

private:
	double _time;

};

} //end namespace

#endif //#ifndef SWM_WAIT_HPP
