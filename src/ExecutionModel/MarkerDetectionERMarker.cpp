#include "MarkerDetectionERMarker.hpp"
#include "MarkerDetectionArUco.hpp"
#include "ERMarkerDetector.hpp"
#include <cmath>

#include <rw/math/EAA.hpp>
#include <rw/math/Transform3D.hpp>
#include <Common/EntityRegister.hpp>

#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>

#include <opencv2/aruco.hpp>

using namespace exm;
using namespace hwl;
using namespace er_common;
using namespace rw::math;
using namespace rw::kinematics;
using namespace rw::common;
using namespace cv;

MarkerDetectionERMarker::MarkerDetectionERMarker() :
    MarkerDetection("MarkerDetectionERMarker", "")
{
	initializeSettings();
	setIsInitialized(false);
	_executionCount = 0;
}

MarkerDetectionERMarker::MarkerDetectionERMarker(Camera::Ptr camera, int execCount) :
    MarkerDetection("MarkerDetectionERMarker", "")
{
	initializeSettings();
	_camera->getValue().set(camera);
	setIsInitialized(true);
	_executionCount = execCount;
}


void MarkerDetectionERMarker::initializeSettings()
{
	_camera = getSettings().add<EntityReference<hwl::Camera> >("Camera", "Interface of the camera", EntityReference<hwl::Camera>("Camera"));
	_squareSize = getSettings().add<double>("Chessboard_SquareSize", "Size of squares on chessboard in meter", 0.1);
	_squareSize->setRange(0, 1.0);
	_aRsquareSize = getSettings().add<double>("ArUco_SquareSize", "Size of squares on ArUco in meter", 0.1);
	_aRsquareSize->setRange(0, 1.0);

	_rows = getSettings().add<int>("Chessboard_Rows", "Number of rows on chess board marker", 7);
	_rows->setRange(0, 20);
	_cols = getSettings().add<int>("Chessboard_Columns", "Number of colums on chess board marker", 6);
	_cols->setRange(0, 20);

	_arUcoID = getSettings().add<int>("ERMarker_ID", "ID of the marker", 1);
	_arUcoID->setRange(0, 200);

	_knowledgeBaseIdentifier = getSettings().add<std::string>("ERMarker_KnowledgebaseId", "Identifier for the results in the knowledge base", "ERMarkerPose");

}

const cv::Mat& MarkerDetectionERMarker::getDetectionImage() const
{
	return _detectionImage;
}

bool MarkerDetectionERMarker::execute(ExecutionManager::Ptr execManager)
{
	_meanProjectionError = -1;
	_executionCount++;
	try {
		bool saveImages = er_common::System::getInstance()->getSaveImages();

		ER_LOG_DEBUG("MarkerDetectionERMarker");
		//Acquire Image
		Camera::Ptr camera = _camera->getValue().get();

			// TODO: Is this really necessary?
		for (int i = 0; i < 5; i++) {
			camera->acquire();
			TimerUtil::sleepMs(100); 
		}

		cv::Mat inputImage = camera->getLastImage();

		if(saveImages){
			std::stringstream imageOriginalfile;
			imageOriginalfile << er_common::System::getInstance()->getMarkerImageFolder() << "/ERMarkerImages/" << _executionCount  << "_ERMarkerImagesInputImage" << ".png";
			imwrite(imageOriginalfile.str(), inputImage);
		}
		MarkerDetectionArUco detectAruCo;

		try{
			detectAruCo.getSettings().set("ArUco_MarkerSize", _aRsquareSize->getValue());
			detectAruCo.getSettings().set("ArUco_MarkerId", _arUcoID->getValue());
			detectAruCo.execute(execManager);
		}
		catch(...){
			if(saveImages){
				std::stringstream imageOriginalfile;
				imageOriginalfile << er_common::System::getInstance()->getMarkerImageFolder() << "/ERMarkerImages/" << _executionCount  << "_ERMarkerIDNotFound" << ".png";
				imwrite(imageOriginalfile.str(), detectAruCo.getDetectionImage());
			}

			_detected = false;

			ER_LOG_DEBUG("No ArUco with given ID found");
			return false;

		}


		rw::math::RPY<> rpyArUco(EAA<>(detectAruCo.getRvecs().at<double>(0),detectAruCo.getRvecs().at<double>(1), detectAruCo.getRvecs().at<double>(2)).toRotation3D());

			// project points of outer border, with regards to the found ArUco marker
		float squareSize = (float)_squareSize->getValue();
		float aRsquareSize = (float)_aRsquareSize->getValue();

		std::vector<Point3f> erMarkerOuterBounds;
		// TODO: These probably need to be multiplied with number of squares on both sides
		double multiplier =  0.8;
		erMarkerOuterBounds.push_back(Point3f(-squareSize*multiplier, -squareSize*multiplier, 0));
		erMarkerOuterBounds.push_back(Point3f(-squareSize*multiplier, squareSize*multiplier, 0));
		erMarkerOuterBounds.push_back(Point3f(squareSize*multiplier, squareSize*multiplier, 0));
		erMarkerOuterBounds.push_back(Point3f(squareSize*multiplier, -squareSize*multiplier, 0));

		std::pair<Mat, Mat> cameraMatrixAndDistCoeffs = camera->getCameraCalibration();

		std::vector<Point2f> erMarkerOuterBoundsProjected;
		projectPoints(erMarkerOuterBounds, detectAruCo.getRvecs(), detectAruCo.getTvecs(), cameraMatrixAndDistCoeffs.first, cameraMatrixAndDistCoeffs.second, erMarkerOuterBoundsProjected);
		ER_LOG_DEBUG("ERMARKEROUTERBOUNDS: " << erMarkerOuterBoundsProjected.at(0));
		circle(detectAruCo.getDetectionImage(), erMarkerOuterBoundsProjected.at(0), 2,  Scalar(0, 0, 255), 2 );
		circle(detectAruCo.getDetectionImage(), erMarkerOuterBoundsProjected.at(1), 2,  Scalar(0, 0, 255), 2 );
		circle(detectAruCo.getDetectionImage(), erMarkerOuterBoundsProjected.at(2), 2,  Scalar(0, 0, 255), 2 );
		circle(detectAruCo.getDetectionImage(), erMarkerOuterBoundsProjected.at(3), 2,  Scalar(0, 0, 255), 2 );


			// Create mask that removes everything around the marker
		Mat mask(inputImage.rows, inputImage.cols, CV_8UC1);
		for(int i=0; i<mask.cols; i++)
		   for(int j=0; j<mask.rows; j++)
		       mask.at<uchar>(Point(i,j)) = 0;

		std::vector<Point> ROI_Poly;


		for(int i = 0; i < erMarkerOuterBoundsProjected.size(); i++){
			ROI_Poly.push_back(Point(erMarkerOuterBoundsProjected.at(i).x, erMarkerOuterBoundsProjected.at(i).y));
		}
		fillConvexPoly(mask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);

		// Create new image for result storage
		Mat imageCut(inputImage.rows, inputImage.cols,  inputImage.type());

		// Cut out ROI and store it in imageDest
		for(int i=0; i<mask.cols; i++)
		   for(int j=0; j<mask.rows; j++)
		       if(mask.at<uchar>(Point(i,j)) != 0){
		    	   imageCut.at<Vec3b>(Point(i,j)) =  inputImage.at<Vec3b>(Point(i,j));
		       }
		       else{
		    	   imageCut.at<Vec3b>(Point(i,j))[0] = 126;
		    	   imageCut.at<Vec3b>(Point(i,j))[1] = 126;
		    	   imageCut.at<Vec3b>(Point(i,j))[2] = 126;

		       }

		if(saveImages){
			std::stringstream imageAruCodetFile;
			imageAruCodetFile << er_common::System::getInstance()->getMarkerImageFolder() << "/ERMarkerImages/" << _executionCount  << "_ERMarkerArUcoDetection" << ".png";
			imwrite(imageAruCodetFile.str(), detectAruCo.getDetectionImage());
			imageAruCodetFile.str(std::string());
			imageAruCodetFile << er_common::System::getInstance()->getMarkerImageFolder() << "/ERMarkerImages/" << _executionCount  << "_ERMarkerArUcoDetectionCut" << ".png";
			ER_LOG_DEBUG("Writing to: " << imageAruCodetFile.str());
			imwrite(imageAruCodetFile.str(), imageCut);
		}



		std::vector<cv::Point2f> points;
		Size patternSize(_rows->getValue(), _cols->getValue());
		ER_LOG_DEBUG("Rows and cols: " << patternSize );

		ERMarkerDetector ermDet(patternSize);
		bool patternfound = ermDet.findErMarkerCorners(imageCut, patternSize, points, CALIB_CB_ADAPTIVE_THRESH);

		ER_LOG_DEBUG("Found ERMarker: " << patternfound);


		if (!patternfound) {
			if(saveImages){
				std::stringstream imageNoMarkerFile;
				imageNoMarkerFile << er_common::System::getInstance()->getMarkerImageFolder() << "/ERMarkerImages/" << _executionCount << "_ERMarkerNoMarkerDetected_" << ".png";
				imwrite(imageNoMarkerFile.str(), imageCut);
			}
			_detected = false;
			return false;
		}
		else{
			_detected = true;
		}

		try {
			cv::Mat greyImage;
			cvtColor(imageCut, greyImage, CV_BGR2GRAY);

			cornerSubPix(greyImage, points, Size(11, 11), Size(-1, -1), TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
		}
		catch (std::exception& exp) {
			ER_LOG_DEBUG("Exception: " << exp.what() );
		}
		drawChessboardCorners(inputImage, patternSize, Mat(points), false);

		if(saveImages){
			std::stringstream imageNoMarkerFile;
			imageNoMarkerFile << er_common::System::getInstance()->getMarkerImageFolder() << "/ERMarkerImages/" << _executionCount << "_ERMarkerMarkerDetected_" << ".png";
			imwrite(imageNoMarkerFile.str(), inputImage);
		}

		std::vector<Point3f> boardPoints;
		if(ermDet.reverted){
			for (int i = patternSize.height-1; i > -1; --i) {
				for (int j = 0; j < patternSize.width; ++j) {

						// Remove boardpoints where ArUco marker is located
					if((j > 1 && j < 4) && (i > 0 && i < 5))
						continue;
					if((i > 1 && i < 4) && (j > 0 && j < 5))
						continue;

					boardPoints.push_back(Point3f(j*squareSize, i*squareSize, 0));
				}
			}
		} else{
			for (int i = 0; i < patternSize.height; ++i) {
				for (int j = 0; j < patternSize.width; ++j) {

						// Remove boardpoints where ArUco marker is located
					if((j > 1 && j < 4) && (i > 0 && i < 5))
						continue;
					if((i > 1 && i < 4) && (j > 0 && j < 5))
						continue;

					boardPoints.push_back(Point3f(j*squareSize, i*squareSize, 0));
				}
			}
		}


		Mat  rvecs, tvecs;
		solvePnP(Mat(boardPoints), Mat(points), cameraMatrixAndDistCoeffs.first, cameraMatrixAndDistCoeffs.second, rvecs, tvecs, false);

		Transform3D<> cam2marker;
		cam2marker.R() = EAA<>(rvecs.at<double>(0, 0), rvecs.at<double>(0, 1), rvecs.at<double>(0, 2)).toRotation3D();
		cam2marker.P() = Vector3D<>(tvecs.at<double>(0, 0), tvecs.at<double>(0, 1), tvecs.at<double>(0, 2));

		ER_LOG_DEBUG("ArUco RPY: " << rpyArUco);
		ER_LOG_DEBUG("ERMarker RPY: " << rw::math::RPY<>(cam2marker.R()));


		_meanProjectionError = calcMeanErrorOfProjection(boardPoints, rvecs, tvecs, cameraMatrixAndDistCoeffs, points);
		ER_LOG_DEBUG("Mean error of projection: " << _meanProjectionError );

		std::vector<Point3f> coordinateSystem;
		coordinateSystem.push_back(Point3f(0, 0, 0));
		coordinateSystem.push_back(Point3f(squareSize * 2, 0, 0));
		coordinateSystem.push_back(Point3f(0, squareSize * 2, 0));
		coordinateSystem.push_back(Point3f(0, 0, squareSize * 2));

		std::vector<Point2f> coordinateImagePoints;
		projectPoints(coordinateSystem, rvecs, tvecs, cameraMatrixAndDistCoeffs.first, cameraMatrixAndDistCoeffs.second, coordinateImagePoints);
		line(inputImage, coordinateImagePoints.at(0), coordinateImagePoints.at(1), Scalar(0, 0, 255), 3);
		line(inputImage, coordinateImagePoints.at(0), coordinateImagePoints.at(2), Scalar(0, 255, 0), 3);
		line(inputImage, coordinateImagePoints.at(0), coordinateImagePoints.at(3), Scalar(255, 0, 0), 3);


		if(saveImages){
			std::stringstream imageDetectedfile;
			imageDetectedfile << er_common::System::getInstance()->getMarkerImageFolder() << "/ERMarkerImages/" << _executionCount  << "_ChessboardDetected_"<< ".png";
			imwrite(imageDetectedfile.str(), inputImage);
		}


		std::stringstream projErrStr;
		projErrStr << "Mean Projection Error: " << _meanProjectionError;
		cv::putText(inputImage, projErrStr.str(), cv::Point(40,40), cv::FONT_HERSHEY_COMPLEX, 1, (0,0,255), 1, cv::LINE_AA, false);

		if(saveImages){
			std::stringstream imageoutfile;
			imageoutfile << er_common::System::getInstance()->getMarkerImageFolder() << "/ERMarkerImages/" << _executionCount << "_Chessboard_" << ".png";
			imwrite(imageoutfile.str(), inputImage);
		}

		_detectionImage = inputImage;

		std::string knowledgeBaseId = _knowledgeBaseIdentifier->getValue();
		execManager->getSystem()->getKnowledgeBase()->addForce(knowledgeBaseId, "Camera to Marker transform", cam2marker);

		ER_LOG_DEBUG("\tcam2marker: XYZ = " << cam2marker.P()<<" RPY = "<<RPY<>(cam2marker.R()) );

	}
	catch (const std::exception& exp) {
		ER_LOG_ERROR("MarkerDetectionERMarker: Exception while trying to identify marker: " <<exp.what());
		RW_THROW(exp.what());
	}
	catch (...) {
		ER_LOG_ERROR("MarkerDetectionERMarker: Unknown Exception while trying to identify marker.");
		RW_THROW("Caught unknown exception while doing Chessboard Marker Detection ");
	}
	_detected = true;

	return true;
}


double MarkerDetectionERMarker::calcMeanErrorOfProjection(std::vector<Point3f> boardPoints, Mat rvecs, Mat tvecs, std::pair<Mat, Mat> cameraMatrixAndDistCoeffs, std::vector<cv::Point2f> points){
		// Projecting all points to chessboard
	std::vector<Point2f> projectedPoints;
	projectPoints(boardPoints, rvecs, tvecs, cameraMatrixAndDistCoeffs.first, cameraMatrixAndDistCoeffs.second, projectedPoints);

	double totalError = 0;

	for(size_t i = 0; i < projectedPoints.size(); i++){
		totalError += std::sqrt(pow((projectedPoints[i].x - points[i].x),2.0) + pow((projectedPoints[i].y - points[i].y),2.0) );
	}

	return totalError/projectedPoints.size();
}

double MarkerDetectionERMarker::getMeanProjectionError(){
	return _meanProjectionError;
}

bool MarkerDetectionERMarker::isDetected(){
	return _detected;
}



/* Methods from the serializable interface */
void MarkerDetectionERMarker::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    iarchive.readEnterScope("MarkerDetectionERMarker");

    MarkerDetection::read(iarchive, id);


	getSettings().read(iarchive, "Settings");

		
	iarchive.readLeaveScope("MarkerDetectionERMarker");
}

void MarkerDetectionERMarker::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("MarkerDetectionERMarker");
    MarkerDetection::write(oarchive, id);

	getSettings().write(oarchive, "Settings");
	
	oarchive.writeLeaveScope("MarkerDetectionERMarker");
}
