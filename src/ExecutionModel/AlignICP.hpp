/* */
#ifndef EXM_ALIGNICP_HPP
#define EXM_ALIGNICP_HPP

#include "Instruction.hpp"
#include "ExecutionManager.hpp"
#include "../HardwareLayer/Scanner2D.hpp"
#include "../HardwareLayer/Manipulator.hpp"

#include <rw/models/Object.hpp>

#include <ostream>
#include <string>

namespace exm {

/** 
 * @brief Waits a specified amount of time before returning from the execute method.
 */
class AlignICP: public Instruction
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<AlignICP> Ptr;

	/**
	 * @brief Default Constructor
	 */
	AlignICP();

	AlignICP(const std::string& pcdFilename, rw::kinematics::FixedFrame::Ptr partFrame );

	bool execute(ExecutionManager::Ptr execManager);

    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

	

protected:
    virtual void initializeSettings();

private:
	er_common::Setting<std::string>::Ptr _filenamePCD;
	rw::kinematics::FixedFrame::Ptr _partFrame;
	std::string _partFrameName;
};

} //end namespace

#endif //#ifndef EXM_ALIGNICP_HPP
