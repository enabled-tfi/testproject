/* */
#ifndef EXM_EXERCISEIMPULSE_HPP
#define EXM_EXERCISEIMPULSE_HPP

#include "Instruction.hpp"
#include "ExecutionManager.hpp"
#include <HardwareLayer/ImpulseDevice.hpp>
#include <Common/EntityReference.hpp>
#include <rw/math.hpp>

#include <vector>
namespace exm {
	
/**
 * @brief Instruction for activating equipment for applying an impulse
 */
class ExerciseImpulse: public Instruction
{
public:

	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<ExerciseImpulse> Ptr;


	ExerciseImpulse();

	/**
	 * @brief Constructs ExerciseImpulse action using the \bdeviceInterface as the device for exercising the impulse.
	 */
    ExerciseImpulse(hwl::ImpulseDevice::Ptr deviceInterface);

	ExerciseImpulse(const std::string& deviceIdentifier);

    bool execute(ExecutionManager::Ptr execManager);



    /* Methods from the serializable interface */

    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

protected:
    void initializeSettings();


private:	
	er_common::Setting < er_common::EntityReference<hwl::ImpulseDevice> >::Ptr _deviceInterface;
	
};

} //end namespace

#endif //#ifndef EXM_EXERCISEIMPULSE_HPP
