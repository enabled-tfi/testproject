#include "ExecutionManager.hpp"

#include "Instruction.hpp"

using namespace exm;
using namespace er_common;
using namespace rw::common;

ExecutionManager::ExecutionManager(System::Ptr system):
	_system(system)
{
    _errorMessage = "";
}



void ExecutionManager::executeBlocking(rw::common::Ptr<Instruction> instruction)
{
    _errorMessage = "";
	setExecutionState(Executing);
	doExecute(instruction);
	setExecutionState(Success);
}


void ExecutionManager::executeNonBlocking(rw::common::Ptr<Instruction> instruction, const std::function<void(ExecutionResult)>& callback)
{
    _executionFinishedCallback = callback;
    _errorMessage = "";
    setExecutionState(Executing);
    _instruction = instruction;
    _thread = ownedPtr(new std::thread([this] { run();}));
}


void ExecutionManager::run() {
    try {
        doExecute(_instruction);
        setExecutionState(Success);
        if (_executionFinishedCallback) {
            if (getExecutionState() == Stopped) {
                _executionFinishedCallback(ExecutionStopped);
            } else {
                _executionFinishedCallback(ExecutionSuccess);
            }
        }
    } catch (const std::exception& exp) {
        _executionFinishedCallback(ExecutionError);
        setExecutionState(Error);
        _errorMessage = exp.what();
    }
}


ExecutionManager::ExecutionState ExecutionManager::getExecutionState() const {
    std::lock_guard<std::mutex> lk(_mutex);
    return _executionState;
}



void ExecutionManager::setExecutionState(ExecutionState state) {
    std::lock_guard<std::mutex> lk(_mutex);
    _executionState = state;
}

void ExecutionManager::doExecute(rw::common::Ptr<Instruction> instruction)

{

    if (getExecutionState() == Pausing) {
        setExecutionState(Paused);
        while (getExecutionState() == Paused) {
            TimerUtil::sleepMs(500);
        }
    }

    if (getExecutionState() == Stopping) {
        setExecutionState(Stopped);
        return;
    }

    if (getExecutionState() == Error) {
        return;
    }

    try {
        instruction->execute(this);
    }
    catch (const std::exception& exp) {
        ER_LOG_ERROR("Exception while executing " << instruction->getName() << ": " << exp.what());
        std::stringstream sstr;
        sstr << "Exception while execution " << instruction->getName() << ": " << exp.what();
        _errorMessage = sstr.str();
        setExecutionState(Error);
        RW_THROW(sstr.str());
    }
}




void ExecutionManager::stop()
{
    setExecutionState(Stopping);
    //TODO Implement stop in execution manager
    RW_THROW("Sorry stop not implemented yet");
}

void ExecutionManager::pause()
{
    if (getExecutionState() == Executing) {
        setExecutionState(Pausing);
    }
}

void ExecutionManager::resume()
{
    if (getExecutionState() == Paused || getExecutionState() == Pausing) {
        setExecutionState(Executing);
    }
}

const std::string& ExecutionManager::getErrorMessage() const {
	return _errorMessage;
}


System::Ptr ExecutionManager::getSystem()
{
	return _system;
}
