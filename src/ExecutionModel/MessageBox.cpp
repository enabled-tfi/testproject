#include "MessagePopup.hpp"
#include "HMI.hpp"

#include "../Serialization/InputArchive.hpp"
#include "../Serialization/OutputArchive.hpp"

using namespace swm;
using namespace mms_common;


MessageBox::MessageBox():
        Instruction("MessageBox", "")
{
    initializeSettings();
    setIsInitialized(false);
}

MessageBox::MessageBox(const std::string& title, const std::string& message):
	Instruction("MessageBox", "")
{
	initializeSettings();
	_title->setValue(title);
	_message->setValue(message);

	setIsInitialized(true);
}

void MessageBox::initializeSettings()
{
    _title = getSettings().add<std::string>("Title", "Title of MessageBox", "");
    _message = getSettings().add<std::string>("Message", "Message in MessageBos", "");
}

bool MessageBox::execute(ExecutionManager::Ptr execManager)
{
	HMI::instance()->infoBox(_title->getValue(), _message->getValue()); 
	return true;
}
		
void MessageBox::read(class mms_serialization::InputArchive& iarchive, const std::string& id)
{
    ER_LOG_DEBUG("MessageBox::read");
    iarchive.readEnterScope("MessageBox");
    Instruction::read(iarchive, id);

    _title->setValue(iarchive.read<std::string>("Title"));
    _message->setValue(iarchive.read<std::string>("Message"));

    iarchive.readLeaveScope("MessageBox");
}

void MessageBox::write(class mms_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("MessageBox");
    Instruction::write(oarchive, id);

    oarchive.write(_title->getValue(), "Title");
    oarchive.write(_message->getValue(), "Message");
    oarchive.writeLeaveScope("MessageBox");
}
