/* */
#ifndef SWM_PROGRAMSTATE_HPP
#define SWM_PROGRAMSTATE_HPP

#include <rw/common/Ptr.hpp>
#include <boost/any.hpp>
#include <map>

namespace swm {
	
/**
 *
 */
class ProgramState
{
public:
	ProgramState();

	boost::any& get(int id);
	void set(int id, const boost::any& val);

	bool has(int id);

	void remove(int id);
private:
	std::map<int, boost::any> _map;

};

} //end namespace

#endif //#ifndef SWM_PROGRAMSTATE_HPP
