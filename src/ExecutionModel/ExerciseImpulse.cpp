#include "ExerciseImpulse.hpp"

#include "../Common/EntityRegister.hpp"

#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>
#include <boost/foreach.hpp>

using namespace exm;
using namespace hwl;
using namespace er_common;

ExerciseImpulse::ExerciseImpulse() :
	Instruction("ExerciseImpulse", "")
{
	initializeSettings();
	setIsInitialized(false);
}

ExerciseImpulse::ExerciseImpulse(hwl::ImpulseDevice::Ptr deviceInterface):
	Instruction("ExerciseImpulse", "")
{
    initializeSettings();
	_deviceInterface->setValue(deviceInterface);
    setIsInitialized(true);
}


ExerciseImpulse::ExerciseImpulse(const std::string& deviceIdentifier):
	Instruction("ExerciseImpulse", "")
{
	initializeSettings();

	ER_LOG_DEBUG("Device Identifier = " << deviceIdentifier );
	if (EntityRegister::has(deviceIdentifier)) {
		ER_LOG_DEBUG("device = "<<EntityRegister::get<ImpulseDevice>(deviceIdentifier) );
		_deviceInterface->getValue().set(EntityRegister::get<ImpulseDevice>(deviceIdentifier));
	}
	else {
		_deviceInterface->getValue().set(deviceIdentifier);
	}

	setIsInitialized(true);
}


bool ExerciseImpulse::execute(ExecutionManager::Ptr execManager)
{
	ER_LOG_DEBUG("Exercise Impulse " << _deviceInterface);
	ER_LOG_DEBUG("Device = " << _deviceInterface->getValue().get() );
	 _deviceInterface->getValue().get()->execute(0);
	 return true;
}
		

void ExerciseImpulse::initializeSettings() 
{
	//_deviceInterface = getSettings().add<hwl::ImpulseDevice::Ptr>("ImpulseDevice", "Device able to provide an impulse", NULL);
	_deviceInterface = getSettings().add<EntityReference<hwl::ImpulseDevice> >("ImpulseDevice", "Device able to provide an impulse", EntityReference<hwl::ImpulseDevice>("ImpulseDevice"));
}


/* Methods from the serializable interface */

void ExerciseImpulse::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    ER_LOG_DEBUG("ExerciseImpulse::read");
    iarchive.readEnterScope("ExerciseImpulse");
    Instruction::read(iarchive, id);
	std::string deviceIdentifier = iarchive.read<std::string>("ImpulseDevice");
	_deviceInterface->getValue().set(deviceIdentifier);
	iarchive.readLeaveScope("ExerciseImpulse");
}

void ExerciseImpulse::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("ExerciseImpulse");
    Instruction::write(oarchive, id);
	oarchive.write(_deviceInterface->getValue().get()->getName(), "ImpulseDevice");
    oarchive.writeLeaveScope("ExerciseImpulse");
}
