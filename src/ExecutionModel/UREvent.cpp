/*
 * UREvent.cpp
 *
 *  Created on: May 1, 2019
 *      Author: thomas
 */

#include <ExecutionModel/UREvent.hpp>
#include <HardwareLayer/UR/URAction.hpp>
#include <Common/EntityRegister.hpp>
#include <HardwareLayer/ActionServer.hpp>


namespace exm {

UREvent::UREvent(): Instruction("UREvent") {
	// TODO Auto-generated constructor stub

}

UREvent::UREvent(std::string identifier, std::string eventName) : Instruction(identifier){
	initializeSettings();
	_eventName->getValue() = eventName;
}

UREvent::~UREvent() {
	// TODO Auto-generated destructor stub
}

void UREvent::initializeSettings()
{
	std::vector<std::string> empty;
	_params = getSettings().add<std::vector<std::string>>("Parameters", "Parameters for the action", empty);
	_eventName = getSettings().add<std::string>("EventName", "Name of the event", "");
}


bool UREvent::execute(ExecutionManager::Ptr execManager)
{
	execManager->getSystem()->update();
	hwl::URAction::Ptr newAction= new hwl::URAction(_eventName->getValue(), _params->getValue());
	er_common::EntityRegister::get<hwl::ActionServer>("URActionServer")->addAction(newAction);
	return true;
}

} /* namespace hwl */
