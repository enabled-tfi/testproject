#include "MoveQPlanned.hpp"

#include "MovePTP.hpp"

#include <rw/common/TimerUtil.hpp>
#include <rw/pathplanning/PlannerUtil.hpp>
#include <Common/EntityRegister.hpp>
#include <Common/Utils.hpp>
#include <Common/ManipulatorQ2QPlanner.hpp>
#include <Common/InputArchive.hpp>
#include <Common/OutputArchive.hpp>
#include <boost/foreach.hpp>

using namespace exm;
using namespace er_common;
using namespace rw::common;
using namespace rw::math;
using namespace rw::kinematics;
using namespace rw::trajectory;
using namespace rw::pathplanning;


MoveQPlanned::MoveQPlanned():
	Instruction("MoveQPlanned", "")
{
    initializeSettings();
    setIsInitialized(false);
}


MoveQPlanned::MoveQPlanned(const rw::math::Q& qtarget, const std::string& deviceIdentifier):
	Instruction("MoveQPlanned", "")
{
	initializeSettings();
	_qtarget->setValue(qtarget);
	if (EntityRegister::has(deviceIdentifier)) {
		_deviceInterface->getValue().set(EntityRegister::get<hwl::Manipulator>(deviceIdentifier));
	}
	else {
		_deviceInterface->getValue().set(deviceIdentifier);
	}
	setIsInitialized(true);
}





MoveQPlanned::MoveQPlanned(const rw::math::Q& qtarget, hwl::Manipulator::Ptr deviceInterface):
	Instruction("MoveQPlanned", "")
{
	initializeSettings();
	_qtarget->setValue(qtarget);
	_deviceInterface->getValue().set(deviceInterface);
	setIsInitialized(true);
}



bool MoveQPlanned::execute(ExecutionManager::Ptr execManager)
{
    ER_LOG_DEBUG("MoveQPlanned::Execute");

	//Call update to ensure that states from all hardware entities has been read. 
	execManager->getSystem()->update();

	State state = execManager->getSystem()->getState();
	System::Ptr system = execManager->getSystem();

	
	Q qcurrent = system->getManipulator()->getQ(state);
	Q qtarget = _qtarget->getValue();

	ManipulatorQ2QPlanner::Ptr manipulatorPlanner = system->getManipulatorQ2QPlanner(state);

	if (manipulatorPlanner->inCollision(qcurrent, state)) {
		RW_THROW("Unable to plan because current configuration, "<<qcurrent<<", is in collision");
	}

	if (manipulatorPlanner->inCollision(qtarget, state)) {
		RW_THROW("Unable to plan because target configuration, "<<qtarget<<", is in collision");
	}

	QPath qpath = manipulatorPlanner->query(qcurrent, qtarget, state);
	if (qpath.size() == 0) {
		RW_THROW("Unable to plan path");
	}
	else {
		ER_LOG_DEBUG("Found Solution Ready to move" );
		exm::MovePTP::Ptr movePTP = ownedPtr(new MovePTP(qpath, _deviceInterface->getValue().get()));
		movePTP->execute(execManager);
		return true;
	}

	return false;
}

void MoveQPlanned::initializeSettings()
{
	_qtarget = getSettings().add<Q>("QTArget", "Target configuration", Q::zero(6));
	_deviceInterface = getSettings().add<EntityReference<hwl::Manipulator> >("Manipulator", "Interface of the device to move", EntityReference<hwl::Manipulator>("Manipulator"));
	//_deviceInterface = getSettings().add<hwl::Manipulator::Ptr>("Manipulator", "Interface of the device to move", NULL);
}

/* Methods from the serializable interface */
void MoveQPlanned::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    iarchive.readEnterScope("MoveQPlanned");
    Instruction::read(iarchive, id);

    Q qtarget = iarchive.read<Q>("QTarget");
	_qtarget->setValue(qtarget);

    std::string deviceIdentifier = iarchive.read<std::string>("Manipulator");
    _deviceInterface->getValue().set(deviceIdentifier);
    iarchive.readLeaveScope("MoveQPlanned");
}

void MoveQPlanned::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("MoveQPlanned");
    Instruction::write(oarchive, id);
    oarchive.write(_qtarget->getValue(), "QTarget");
    oarchive.write(_deviceInterface->getValue().getName(), "Manipulator");
    oarchive.writeLeaveScope("MoveQPlanned");
}

