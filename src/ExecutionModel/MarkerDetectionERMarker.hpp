/* */
#ifndef EXM_MARKERDETECTION_ER_MARKER_HPP
#define EXM_MARKERDETECTION_ER_MARKER_HPP

#include "MarkerDetection.hpp"
#include "ExecutionManager.hpp"
#include <Common/EntityReference.hpp>
#include <HardwareLayer/Camera.hpp>

#include <ostream>
#include <string>

namespace exm {

/** 
 * @brief Performs a marker detection based on the OpenCV Chessboard marker 
 */
class MarkerDetectionERMarker: public MarkerDetection
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<MarkerDetectionERMarker> Ptr;

	/**
	 * @brief Default Constructor
	 */
	MarkerDetectionERMarker();

	MarkerDetectionERMarker(hwl::Camera::Ptr camera, int execCount = 0);

	bool execute(ExecutionManager::Ptr execManager);

    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

	const cv::Mat& getDetectionImage() const;

	double getMeanProjectionError();

	bool isDetected();

protected:
    virtual void initializeSettings();

private:

    double calcMeanErrorOfProjection(std::vector<cv::Point3f> boardPoints, cv::Mat rvecs, cv::Mat tvecs, std::pair<cv::Mat, cv::Mat> cameraMatrixAndDistCoeffs, std::vector<cv::Point2f> points);


	er_common::Setting < er_common::EntityReference<hwl::Camera> >::Ptr _camera;
	er_common::Setting<double>::Ptr _squareSize;
	er_common::Setting<double>::Ptr _aRsquareSize;
	er_common::Setting<int>::Ptr _rows;
	er_common::Setting<int>::Ptr _arUcoID;
	er_common::Setting<int>::Ptr _cols;
	er_common::Setting<std::string>::Ptr _knowledgeBaseIdentifier;
	cv::Mat _detectionImage;

	bool _detected;

	double _meanProjectionError;
	int _executionCount;
};

} //end namespace

#endif //#ifndef EXM_MARKERDETECTION_ER_MARKER_HPP
