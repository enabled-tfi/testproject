#include "Instruction.hpp"
#include "ExecutionManager.hpp"
#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>
#include <Common/EntityRepository.hpp>
using namespace exm;
using namespace er_common;


template<>
std::map<std::string, EntityFactoryBase::Ptr> EntityRepository<Instruction>::_entityFactories;

template<>
std::vector<rw::common::Ptr<rw::plugin::DynamicLibraryLoader<EntityFactoryBase> > > EntityRepository<Instruction>::_loaders;


Instruction::Instruction(const std::string& name, const std::string& description):
	Entity(name, description),
	_isInitialized(false)
	/*
	_name(name),
	_description(description)*/
{

}

//
//const std::string& Instruction::getName() const
//{
//	return _name;
//}
//
//const std::string& Instruction::getDescription() const
//{
//	return _description;
//}


bool Instruction::isInitialized() const
{
    return _isInitialized;
}

void Instruction::setIsInitialized(bool isInitialized)
{
    _isInitialized = isInitialized;
}

er_common::Settings& Instruction::getSettings()
{
    return _settings;
}

const er_common::Settings& Instruction::getSettings() const
{
    return _settings;
}

void Instruction::initialize(const er_common::Settings& settings)
{
    _settings = settings;
    setIsInitialized(true);
}


void Instruction::read(er_serialization::InputArchive& iarchive, const std::string& id)
{
	Entity::read(iarchive, id);
    iarchive.read(_isInitialized, "IsInitialized");
}

void Instruction::write(er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	Entity::write(oarchive, "id");
    oarchive.write(_isInitialized, "IsInitialized");
}
