/* */
#ifndef EXM_ARGUMENT_HPP
#define EXM_ARGUMENT_HPP

#include "../Common/Serializable.hpp"

#include <rw/common/Ptr.hpp>
#include <rw/common/PropertyMap.hpp>



namespace exm {
	
/**
 * An argument is a container class for providing run-time input to a instruction.
 *
 * An instruction does not receive and return arguments in its execute method, but are provided
 * with these upon construction. Arguments can be used both for specifying inputs but also
 * communicate outputs from an instruction.
 *
 * An instruction should not read the argument before it execute method is called.
 *
 */
class Argument: public er_common::Serializable
{
public:
	/** @brief Smart pointer to Argument object*/
	typedef rw::common::Ptr<Argument> Ptr;


	Argument();

	Argument(rw::common::PropertyMap::Ptr propertymap);

	/**
	 * @brief Templated method returning the value of the argument.
	 *
	 * The user is expected to know the object type.
	 * @return The value of the argument
	 */
	template <class T>
	T get()
	{
		return _propertymap->get<T>("Argument");
	}

	/**
	 * @brief Templated method setting the value of the argument
	 *
	 * The user is expected to know the object type.
	 * @param t [in] The value for the argument
	 */
	template <class T>
	void set(const T& t)
	{
		_propertymap->set<T>("Argument", t);
	}

	rw::common::PropertyMap::Ptr getPropertyMap() const;

	void setPropertyMap(rw::common::PropertyMap::Ptr propertymap);

    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

private:
	rw::common::PropertyMap::Ptr _propertymap;


};

} //end namespace

#endif //#ifndef EXM_ARGUMENT_HPP
