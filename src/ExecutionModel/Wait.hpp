/* */
#ifndef EXM_WAIT_HPP
#define EXM_WAIT_HPP

#include "SequentialInstruction.hpp"
#include <vector>
namespace exm {

/** 
 * @brief Waits a specified amount of time before returning from the execute method.
 */
class Wait: public Instruction
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<Wait> Ptr;

	/**
	 * @brief Default Constructor
	 */
	Wait();

	/**
	 * @brief Construct Wait instruction for waiting \btime.
	 * @param time [in] The amount of time to wait in ms
	 */
	Wait(double time);

	/**
     * @copydoc Instruction::execute
     */
    virtual bool execute(ExecutionManager::Ptr execManager);

    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


protected:
    virtual void initializeSettings();

private:
	er_common::Setting<double>::Ptr _time;

};

} //end namespace

#endif //#ifndef EXM_WAIT_HPP
