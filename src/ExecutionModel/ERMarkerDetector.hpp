#include <opencv/cv.h>


#include <stack>
#include <opencv2/core/utils/logger.hpp>
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include <Common/Logging.hpp>


using namespace cv;

template<typename _AccTp> static inline _AccTp normL2Sqr(const Point_<int>& pt);
template<typename _AccTp> static inline _AccTp normL2Sqr(const Point_<int64>& pt);
template<typename _AccTp> static inline _AccTp normL2Sqr(const Point_<float>& pt);
template<typename _AccTp> static inline _AccTp normL2Sqr(const Point_<double>& pt);

template<> inline int normL2Sqr<int>(const Point_<int>& pt) { return pt.dot(pt); }
template<> inline int64 normL2Sqr<int64>(const Point_<int64>& pt) { return pt.dot(pt); }
template<> inline float normL2Sqr<float>(const Point_<float>& pt) { return pt.dot(pt); }
template<> inline double normL2Sqr<double>(const Point_<int>& pt) { return pt.dot(pt); }

template<> inline double normL2Sqr<double>(const Point_<float>& pt) { return pt.ddot(pt); }
template<> inline double normL2Sqr<double>(const Point_<double>& pt) { return pt.ddot(pt); }




struct ChessBoardCorner
{
    cv::Point2f pt;  // Coordinates of the corner
    int row;         // Board row index
    int count;       // Number of neighbor corners
    struct ChessBoardCorner* neighbors[4]; // Neighbor corners
    bool firstInRow = false;

    ChessBoardCorner(const cv::Point2f& pt_ = cv::Point2f()) :
        pt(pt_), row(0), count(0)
    {
        neighbors[0] = neighbors[1] = neighbors[2] = neighbors[3] = NULL;
    }

    float sumDist(int& n_) const
    {
        float sum = 0;
        int n = 0;
        for (int i = 0; i < 4; ++i)
        {
            if (neighbors[i])
            {
            	sum += sqrt(normL2Sqr<float>(neighbors[i]->pt - pt));
                n++;

            }
        }
        n_ = n;
        return sum;
    }
};


struct ChessBoardQuad
{
    int count;      // Number of quad neighbors
    int group_idx;  // quad group ID
    int row, col;   // row and column of this quad
    bool ordered;   // true if corners/neighbors are ordered counter-clockwise
    float edge_len; // quad edge len, in pix^2
    // neighbors and corners are synced, i.e., neighbor 0 shares corner 0
    ChessBoardCorner *corners[4]; // Coordinates of quad corners
    struct ChessBoardQuad *neighbors[4]; // Pointers of quad neighbors

    ChessBoardQuad(int group_idx_ = -1) :
        count(0),
        group_idx(group_idx_),
        row(0), col(0),
        ordered(0),
        edge_len(0)
    {
        corners[0] = corners[1] = corners[2] = corners[3] = NULL;
        neighbors[0] = neighbors[1] = neighbors[2] = neighbors[3] = NULL;
    }
};

struct CvContourEx
{
    CV_CONTOUR_FIELDS()
    int counter;
};


class ERMarkerDetector
{
public:
    cv::Mat binarized_image;
    cv::Mat origimg;
    Size pattern_size;

    bool reverted = false;

    cv::AutoBuffer<ChessBoardQuad> all_quads;
    cv::AutoBuffer<ChessBoardCorner> all_corners;

    int all_quads_count;

    ERMarkerDetector(const Size& pattern_size_) :
        pattern_size(pattern_size_),
        all_quads_count(0)
    {
    }

    void reset()
    {
        all_quads.deallocate();
        all_corners.deallocate();
        all_quads_count = 0;
    }

    bool findErMarkerCorners(InputArray image_, Size pattern_size,
                               OutputArray corners_, int flags);

    void generateQuads(const cv::Mat& image_, int flags);

    bool processQuads(std::vector<cv::Point2f>& out_corners, int &prev_sqr_size);

    void findQuadNeighbors();

    void findConnectedQuads(std::vector<ChessBoardQuad*>& out_group, int group_idx);

    	/*
    	 * Prerequisites:
    	 *  ArUco marker is not in outer layer
    	 *  ArUco marker is exactly 3 chessboard quads large and wide
    	 *  Only one ArUco marker is present
    	 */

    int checkQuadGroup(std::vector<ChessBoardQuad*>& quad_group, std::vector<ChessBoardCorner*>& out_corners);

    int findFirstLine(std::vector<ChessBoardCorner*>& out_corners, bool inverted, ChessBoardCorner** cur, ChessBoardCorner** right, ChessBoardCorner** below, ChessBoardCorner** first, CvMat &debug_img);

    int cleanFoundConnectedQuads(std::vector<ChessBoardQuad*>& quad_group);

    int orderFoundConnectedQuads(std::vector<ChessBoardQuad*>& quads);

    void orderQuad(ChessBoardQuad& quad, ChessBoardCorner& corner, int common);

    void setOrigImage(cv::Mat img);

#ifdef ENABLE_TRIM_COL_ROW
    void trimCol(std::vector<ChessBoardQuad*>& quads, int col, int dir);
    void trimRow(std::vector<ChessBoardQuad*>& quads, int row, int dir);
#endif

    int addOuterQuad(ChessBoardQuad& quad, std::vector<ChessBoardQuad*>& quads);

    void removeQuadFromGroup(std::vector<ChessBoardQuad*>& quads, ChessBoardQuad& q0);

    bool checkBoardMonotony(const std::vector<cv::Point2f>& corners);
};
