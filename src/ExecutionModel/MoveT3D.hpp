/* */
#ifndef EXM_MOVET3D_HPP
#define EXM_MOVET3D_HPP

#include "Instruction.hpp"
#include "../HardwareLayer/Manipulator.hpp"
#include <Common/Target.hpp>
#include <Common/EntityReference.hpp>
#include <rw/math.hpp>
#include <vector>
namespace exm {

/**
 * @brief 
 */
class MoveT3D: public Instruction
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<MoveT3D> Ptr;

	MoveT3D();



	/**
	 */
	MoveT3D(const rw::math::Transform3D<>& refTtarget, rw::kinematics::Frame* reference, rw::kinematics::Frame* tcp, hwl::Manipulator::Ptr deviceInterface);

	MoveT3D(const rw::math::Transform3D<>& refTtarget, const std::string& reference, const std::string& tcp, const std::string& deviceIdentifier);

	bool execute(ExecutionManager::Ptr execManager);

    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


protected:
    void initializeSettings();

private:
	er_common::Setting<er_common::Target>::Ptr _target;
	er_common::Setting<bool>::Ptr _useCollisionChecking;
	//er_common::Setting<rw::math::Transform3D<> >::Ptr _refTtarget;
	//er_common::Setting<rw::kinematics::Frame*>::Ptr _reference;
	//std::string _referenceName;
	//er_common::Setting<hwl::Manipulator::Ptr>::Ptr _deviceInterface;
	er_common::Setting < er_common::EntityReference<hwl::Manipulator> >::Ptr _deviceInterface;

};

} //end namespace

#endif //#ifndef EXM_MOVET3D_HPP
