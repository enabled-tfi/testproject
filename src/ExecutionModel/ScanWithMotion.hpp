/* */
#ifndef EXM_SCANWITHMOTION_HPP
#define EXM_SCANWITHMOTION_HPP

#include "Instruction.hpp"
#include "ExecutionManager.hpp"
#include <HardwareLayer/Scanner2D.hpp>
#include <HardwareLayer/Manipulator.hpp>
#include <Common/FrameReference.hpp>
#include <Common/EntityReference.hpp>

#include <rw/models/Object.hpp>

#include <ostream>
#include <string>

namespace exm {

/** 
 * @brief Waits a specified amount of time before returning from the execute method.
 */
class ScanWithMotion: public Instruction
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<ScanWithMotion> Ptr;

	/**
	 * @brief Default Constructor
	 */
	ScanWithMotion();

	ScanWithMotion(hwl::Scanner2D::Ptr scanner, hwl::Manipulator::Ptr manipulator, rw::models::Object::Ptr part, double fov, double angleStep);

	ScanWithMotion(const std::string& scanner, const std::string& manipulator, rw::models::Object::Ptr part, double fov, double angleStep);

	bool execute(ExecutionManager::Ptr execManager);

    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

	

protected:
    virtual void initializeSettings();

private:
	er_common::Setting<double>::Ptr _fov;
	er_common::Setting<double>::Ptr _angleStep;
	er_common::Setting<hwl::Scanner2D::Ptr>::Ptr _scanner2d;
	//er_common::Setting<hwl::Manipulator::Ptr>::Ptr _manipulator;
	er_common::Setting < er_common::EntityReference<hwl::Manipulator> >::Ptr _manipulator;
	er_common::Setting<er_common::FrameReference>::Ptr _partFrame;
	rw::models::Object::Ptr _part;
	void scan(hwl::Scanner2D::Ptr scanner, hwl::Manipulator::Ptr manipulator, er_common::System::Ptr system, const rw::kinematics::State& state, std::vector<rw::math::Vector3D<> >& points);
	
	rw::math::Transform3D<> lookAtPart(const rw::math::Vector3D<>& basePsensor, er_common::System::Ptr system, const rw::kinematics::State& state) const;
	std::vector<rw::math::Transform3D<> > simpleScanStrategy(er_common::System::Ptr system, const rw::kinematics::State& state) const;
	std::vector<rw::math::Transform3D<> > threePointStrategy(er_common::System::Ptr system, const rw::kinematics::State& state) const;

};

} //end namespace

#endif //#ifndef EXM_SCANWITHMOTION_HPP
