#include "MoveLin.hpp"

#include "../Common/EntityRegister.hpp"

#include <Common/InputArchive.hpp>
#include <Common/OutputArchive.hpp>

#include <boost/foreach.hpp>

using namespace exm;
using namespace er_common;
using namespace rw::math;
using namespace rw::models;
using namespace rw::kinematics;

namespace {
	std::string getDes(const Transform3D<>& target) {
		std::stringstream sstr;
		sstr<<"Target: "<<target;
		return sstr.str();
	}

}

MoveLin::MoveLin():
	Instruction("MoveLin", "")
{
    initializeSettings();
    setIsInitialized(false);
}


MoveLin::MoveLin(const Transform3D<>& refTtarget, Frame* reference, Frame* tcp, hwl::Manipulator::Ptr deviceInterface):
	Instruction("MoveLin", getDes(refTtarget))
{
    initializeSettings();
	_target->getValue().setRefTtarget(refTtarget);
	_target->getValue().setReference(reference);
	_target->getValue().setToolFrame(tcp);
    //_refTtarget->setValue(refTtarget);
	//_reference->setValue(reference);
    _deviceInterface->getValue().set(deviceInterface);
    setIsInitialized(true);
}

MoveLin::MoveLin(const rw::math::Transform3D<>& refTtarget, const std::string& reference, const std::string& tcp, const std::string& deviceIdentifier):
	Instruction("MoveLin", getDes(refTtarget))
{
    initializeSettings();
	_target->getValue().setRefTtarget(refTtarget);
	_target->getValue().setReference(reference);
	_target->getValue().setToolFrame(tcp);
	//_refTtarget->setValue(refTtarget);
	if (EntityRegister::has(deviceIdentifier)) {
		_deviceInterface->getValue().set(EntityRegister::get<hwl::Manipulator>(deviceIdentifier));
	}
	else {
		_deviceInterface->getValue().set(deviceIdentifier);
	}
	//_referenceName = reference;
    setIsInitialized(true);
}

void MoveLin::initializeSettings()
{
	_target = getSettings().add<Target>("Target", "Target and reference frame", Target());
 //   _refTtarget = getSettings().add<Transform3D<> >("RefTtarget","Target relative to reference frame", Transform3D<>());
	//_reference = getSettings().add<Frame*>("Reference", "Reference frame", NULL);
	_deviceInterface = getSettings().add<EntityReference<hwl::Manipulator> >("Manipulator", "Interface of the device to move", EntityReference<hwl::Manipulator>("Manipulator"));
    //_deviceInterface = getSettings().add<hwl::Manipulator::Ptr>("Manipulator", "Interface to the serial device", NULL);
}

bool MoveLin::execute(ExecutionManager::Ptr execManager)
{

	execManager->getSystem()->update();
	State state = execManager->getSystem()->getState();
	System::Ptr system = execManager->getSystem();
	Transform3D<> manipulatorTref = Kinematics::frameTframe(system->getManipulator()->getBase(), _target->getValue().getReference(), state);
	Transform3D<> manipulatorTtarget = manipulatorTref*_target->getValue().refTtarget();

	Transform3D<> endTtcp = Kinematics::frameTframe(system->getManipulator()->getEnd(), _target->getValue().getToolFrame(), state);
	_deviceInterface->getValue().get()->setEndToTCP(endTtcp);
	_deviceInterface->getValue().get()->lin(manipulatorTtarget);
	return true;
}
		

/* Methods from the serializable interface */
void MoveLin::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    ER_LOG_DEBUG("MoveLin::read");
    iarchive.readEnterScope("MoveLin");
    Instruction::read(iarchive, id);

	_target->getValue().read(iarchive, "Target");
    //Transform3D<> t3d = iarchive.read<Transform3D<> >("RefTtarget");
    //_refTtarget->setValue(t3d);

    std::string deviceIdentifier = iarchive.read<std::string>("Manipulator");
	if (deviceIdentifier != "")
		_deviceInterface->getValue().set(deviceIdentifier);

	//_referenceName = iarchive.read<std::string>("Reference");	

    iarchive.readLeaveScope("MoveLin");
}

void MoveLin::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	ER_LOG_DEBUG("MoveLin::write" );
    oarchive.writeEnterScope("MoveLin");

	Instruction::write(oarchive, id);
	_target->getValue().write(oarchive, "Target");
	oarchive.write(_deviceInterface->getValue().getName(), "Manipulator");

    oarchive.writeLeaveScope("MoveLin");
}

