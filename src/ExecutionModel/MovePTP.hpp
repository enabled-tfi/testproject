/* */
#ifndef EXM_MOVEPTP_HPP
#define EXM_MOVEPTP_HPP

#include "Instruction.hpp"
#include "../HardwareLayer/Manipulator.hpp"
#include <Common/EntityReference.hpp>
#include <rw/math.hpp>
#include <rw/trajectory/Path.hpp>
#include <vector>
namespace exm {

/**
 * @brief Instruction for moving robot with a point-to-point motion. That is linear in configuration space.
 */
class MovePTP: public Instruction
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<MovePTP> Ptr;

	MovePTP();



	/**
	 * @brief Construct MovePTP action for moving along a path
	 * @param path [in] Path of joint configurations to move through.
	 * @param deviceInterface [in] The device to control
	 */
	MovePTP(const rw::trajectory::QPath& path, hwl::Manipulator::Ptr deviceInterface);

    /**
     * @brief Construct MovePTP action for moving along a path
     * @param path [in] Path of joint configurations to move through.
     * @param deviceIdentifier [in] Identifier for the device to control
     */
    MovePTP(const rw::trajectory::QPath& path, const std::string& deviceIdentifier);


	/**
	 * @brief Construct MovePTP action for moving to a specific configuration
	 * @param qtarget [in] Joint configuration to move through.
	 * @param deviceInterface [in] The device to control
	 */
	MovePTP(const rw::math::Q& qtarget, hwl::Manipulator::Ptr deviceInterface);

    /**
     * @brief Construct MovePTP action for moving to a specific configuration
     * @param qtarget [in] Joint configuration to move through.
     * @param deviceInterface [in] The device to control
     */
    MovePTP(const rw::math::Q& qtarget, const std::string& deviceIdentifier);


	/**
	 * @brief Returns the targets set for the action
	 * @return Targets.
	 */
	const rw::trajectory::QPath& getTargets() const;

	bool execute(ExecutionManager::Ptr execManager);

    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


protected:
    void initializeSettings();

private:
	er_common::Setting<rw::trajectory::QPath>::Ptr _path;
	er_common::Setting < er_common::EntityReference<hwl::Manipulator> >::Ptr _deviceInterface;
	//er_common::Setting<hwl::Manipulator::Ptr>::Ptr _deviceInterface;

//	std::vector<rw::math::Q> _targets;
//	hwl::SerialDevice::Ptr _deviceInterface;
};

} //end namespace

#endif //#ifndef EXM_MOVEPTP_HPP
