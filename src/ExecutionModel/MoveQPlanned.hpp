/* */
#ifndef EXM_MOVEQPLANNED_HPP
#define EXM_MOVEQPLANNED_HPP

#include "Instruction.hpp"
#include "../HardwareLayer/Manipulator.hpp"
#include <Common/EntityReference.hpp>

#include <rw/math.hpp>
#include <vector>
namespace exm {

/**
 * @brief 
 */
class MoveQPlanned: public Instruction
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<MoveQPlanned> Ptr;

	MoveQPlanned();



	/**
	 */
	MoveQPlanned(const rw::math::Q& qtarget, hwl::Manipulator::Ptr deviceInterface);

	MoveQPlanned(const rw::math::Q& qtarget, const std::string& deviceIdentifier);

	bool execute(ExecutionManager::Ptr execManager);

    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


protected:
    void initializeSettings();

private:
	er_common::Setting<rw::math::Q>::Ptr _qtarget;
	er_common::Setting < er_common::EntityReference<hwl::Manipulator> >::Ptr _deviceInterface;
	//er_common::Setting<hwl::Manipulator::Ptr>::Ptr _deviceInterface;
};

} //end namespace

#endif //#ifndef EXM_MOVEQPLANNED_HPP
