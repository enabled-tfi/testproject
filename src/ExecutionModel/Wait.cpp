#include "Wait.hpp"

#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>

#include <rw/common/TimerUtil.hpp>

#include <boost/foreach.hpp>



using namespace exm;


Wait::Wait():
        Instruction("Wait", "")
{
    initializeSettings();
    setIsInitialized(false);
}

Wait::Wait(double time):
	Instruction("Wait","")
{
    initializeSettings();
    _time->setValue(time);
    setIsInitialized(true);
}

void Wait::initializeSettings()
{
    _time = getSettings().add<double>("Time", "Number of ms to wait", 0);
}

bool Wait::execute(ExecutionManager::Ptr execManager)
{
    rw::common::TimerUtil::sleepUs((int)(_time->getValue()*1000));
    return true;
}
		
void Wait::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    ER_LOG_DEBUG("Wait::read");
    iarchive.readEnterScope("Wait");
    Instruction::read(iarchive, id);
    double time = iarchive.read<double>("Time");
    _time->setValue(time);
    iarchive.readLeaveScope("Wait");
}

void Wait::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("Wait");
    Instruction::write(oarchive, id);

    oarchive.write(_time->getValue(), "Time");
    oarchive.writeLeaveScope("Wait");
}
