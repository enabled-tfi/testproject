/* */
#ifndef EXM_DRIVEREL_HPP
#define EXM_DRIVEREL_HPP

#include "Instruction.hpp"
#include "ExecutionManager.hpp"
#include <Common/EntityReference.hpp>
#include "../HardwareLayer/MobileDevice.hpp"
#include <rw/math.hpp>

#include <vector>
namespace exm {
	
/**
 * @brief Instruction for moving robot with a linear path in tool space.
 */
class DriveRel: public Instruction
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<DriveRel> Ptr;

	DriveRel();

	/**
	 * @brief Constructs DriveRel action moving relative to the current pose
	 * @param distance [in] Distance to move.
	 * @param deviceInterface [in] Interface to the device to move
	 */
	DriveRel(double distance, double angle, hwl::MobileDevice::Ptr deviceInterface);

	DriveRel(double distance, double angle, const std::string& deviceIdentifier);

    bool execute(ExecutionManager::Ptr execManager);


   /* Methods from the serializable interface */
   virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

   virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


protected:
   void initializeSettings();

private:
	er_common::Setting<double>::Ptr _distance;
	er_common::Setting<double>::Ptr _angle;
	//er_common::Setting<hwl::MobileDevice::Ptr>::Ptr _deviceInterface;
	er_common::Setting < er_common::EntityReference<hwl::MobileDevice> >::Ptr _deviceInterface;
};

} //end namespace

#endif //#ifndef EXM_DRIVEREL_HPP
