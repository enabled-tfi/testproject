/* */
#ifndef EXM_ROBOTCAMERACALIBRATION_HPP
#define EXM_ROBOTCAMERACALIBRATION_HPP

#include "Instruction.hpp"

#include "MarkerDetectionArUco.hpp"
#include "MarkerDetectionChessboard.hpp"
#include <HardwareLayer/Manipulator.hpp>
#include <HardwareLayer/Camera.hpp>
#include <Common/EntityReference.hpp>
#include <rw/math.hpp>
#include <rw/trajectory/Path.hpp>
#include <vector>
namespace exm {

/**
 * @brief Instruction for moving robot with a point-to-point motion. That is linear in configuration space.
 */
class RobotCameraCalibration: public Instruction
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<RobotCameraCalibration> Ptr;

	RobotCameraCalibration();

	/**
	 * @brief Construct MovePTP action for moving along a path
	 * @param path [in] Path of joint configurations to move through.
	 * @param deviceInterface [in] The device to control
	 */
	RobotCameraCalibration(const rw::trajectory::QPath& path, const std::string& calibrationFileName, hwl::Manipulator::Ptr deviceInterface, hwl::Camera::Ptr cameraInterface);

    /**
     * @brief Construct MovePTP action for moving along a path
     * @param path [in] Path of joint configurations to move through.
     * @param deviceIdentifier [in] Identifier for the device to control
     */
	RobotCameraCalibration(const rw::trajectory::QPath& path, const std::string& calibrationFileName, const std::string& deviceIdentifier, const std::string& cameraIdentifier);

	/**
	 * @brief Returns the targets set for the action
	 * @return Targets.
	 */
	const rw::trajectory::QPath& getTargets() const;

	bool execute(ExecutionManager::Ptr execManager);

    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


protected:
    void initializeSettings();

private:
	er_common::Setting<rw::trajectory::QPath>::Ptr _path;
	er_common::Setting<std::string>::Ptr _calibrationFileName;
	er_common::Setting < er_common::EntityReference<hwl::Manipulator> >::Ptr _deviceInterface;
	er_common::Setting < er_common::EntityReference<hwl::Camera> >::Ptr _cameraInterface;
	er_common::Setting <int>::Ptr _markerDetectionAlgorithm;


	MarkerDetectionArUco _markerDetectionArUco;
	MarkerDetectionChessboard _markerDetectionChessboard;
	
	//er_common::Setting<hwl::Manipulator::Ptr>::Ptr _deviceInterface;

//	std::vector<rw::math::Q> _targets;
//	hwl::SerialDevice::Ptr _deviceInterface;
};

} //end namespace

#endif //#ifndef EXM_ROBOTCAMERACALIBRATION_HPP
