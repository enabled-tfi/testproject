/* */
#ifndef EXM_HMI_HPP
#define EXM_HMI_HPP


#include <rw/common/Ptr.hpp>

namespace exm {
	
/**
 * General interface for actions 
 */
class HMI
{
public:
    /** @brief Smart pointer to Action object*/
	typedef rw::common::Ptr<HMI> Ptr;

	
	static void setInstance(HMI::Ptr instance);
	static HMI::Ptr instance();

	virtual void infoBox(const std::string& title, const std::string& message, bool* blocking) = 0;

protected:
	HMI();
private:
	static HMI::Ptr _instance;

	

};

} //end namespace

#endif //#ifndef EXM_HMI_HPP
