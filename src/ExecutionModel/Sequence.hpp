/* */
#ifndef EXM_SEQUENCE_HPP
#define EXM_SEQUENCE_HPP

#include "Instruction.hpp"
#include <vector>

namespace exm {

/** 
 * @brief Sequence acts as a container which can have multiple instructions.
 *
 * When execute is called on the sequence the execute methods on the added
 * instructions are called in the same order as they occur in the internal list.
 * Execute on a subsequent instruction is not called until the previous instruction
 * has returned from it execute method.
 */
class Sequence: public Instruction
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<Sequence> Ptr;

	/**
	 * @brief Constructs an empty sequence
	 */
	Sequence();

	/**
	 * @brief Calls execute on the instructions.
	 *
	 * The order of the instructions is as they where added.
	 * Execute on an instruction is not called before the previous has terminated.
	 */
	virtual bool execute(ExecutionManager::Ptr execManager);

	/**
	 * @brief Add instruction to the end of the list
	 * @param instruction [in] Instruction to add
	 */
	void addInstruction(Instruction::Ptr instruction);


    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;



protected:
	void initializeSettings();

private:
	std::vector<Instruction::Ptr> _instructions;
};

} //end namespace

#endif //#ifndef EXM_SEQUENCE_HPP
