#include "InstructionFactory.hpp"

using namespace exm;

InstructionFactoryBase::InstructionFactoryBase(const std::string& identifier, const std::string& description):
    _identifier(identifier),
    _description(description)
{

}

const std::string& InstructionFactoryBase::getIdentifier() const
{
    return _identifier;
}


const std::string& InstructionFactoryBase::getDescription() const
{
    return _description;
}


