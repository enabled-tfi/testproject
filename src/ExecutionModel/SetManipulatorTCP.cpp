#include "SetManipulatorTCP.hpp"

#include "../Common/EntityRegister.hpp"

#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>

#include <boost/foreach.hpp>

using namespace exm;
using namespace er_common;
using namespace rw::math;
using namespace rw::kinematics;
using namespace rw::models;


namespace {
	std::string getDes(const Transform3D<>& target) 
	{
		std::stringstream sstr;
		sstr<<"EndTtcp: "<<target;
		return sstr.str();
	}

	std::string getDes(const std::string& frameId) 
	{
		std::stringstream sstr;
		sstr << "TCP Frame: " << frameId;
		return sstr.str();

	}

}

SetManipulatorTCP::SetManipulatorTCP():
	Instruction("SetManipulatorTCP", "")
{
    initializeSettings();
    setIsInitialized(false);
}


SetManipulatorTCP::SetManipulatorTCP(const Transform3D<>& endTtcp, hwl::Manipulator::Ptr deviceInterface):
	Instruction("SetManipulatorTCP", getDes(endTtcp))
{
    initializeSettings();
	_useWorkCellTCP->setValue(false);
    _endTtcp->setValue(endTtcp);
    _deviceInterface->getValue().set(deviceInterface);
    setIsInitialized(true);
}

SetManipulatorTCP::SetManipulatorTCP(const rw::math::Transform3D<>& endTtcp, const std::string& deviceIdentifier):
	Instruction("SetManipulatorTCP", getDes(endTtcp))
{
    initializeSettings();
	_useWorkCellTCP->setValue(false);
	_endTtcp->setValue(endTtcp);
	if (EntityRegister::has(deviceIdentifier)) {
		_deviceInterface->getValue().set(EntityRegister::get<hwl::Manipulator>(deviceIdentifier));
	}
	else {
		_deviceInterface->getValue().set(deviceIdentifier);
		//_deviceInterface->setValue(rw::common::ownedPtr(new hwl::ManipulatorDummy(deviceIdentifier)));
	}
    setIsInitialized(true);
}


SetManipulatorTCP::SetManipulatorTCP(const std::string& tcpFrameId, hwl::Manipulator::Ptr deviceInterface):
	Instruction("SetManipulatorTCP", getDes(tcpFrameId))
{
	initializeSettings();
	_useWorkCellTCP->setValue(true);
	_tcpFrameId->setValue(tcpFrameId);
	_deviceInterface->getValue().set(deviceInterface);
}



SetManipulatorTCP::SetManipulatorTCP(const std::string& tcpFrameId, const std::string& deviceIdentifier) :
	Instruction("SetManipulatorTCP", getDes(tcpFrameId))
{
	initializeSettings();
	_useWorkCellTCP->setValue(true);
	_tcpFrameId->setValue(tcpFrameId);
	if (EntityRegister::has(deviceIdentifier)) {
		_deviceInterface->getValue().set(EntityRegister::get<hwl::Manipulator>(deviceIdentifier));
	}
	else {
		_deviceInterface->getValue().set(deviceIdentifier);
	}
	setIsInitialized(true);
}


void SetManipulatorTCP::initializeSettings()
{
	_useWorkCellTCP = getSettings().add<bool>("CopyFromWorkCell", "Should the transform be copied from the work cell", false);
	_tcpFrameId = getSettings().add<std::string>("TCPFrameID", "Name of the TCP Frame in the WorkCell", "");
    _endTtcp = getSettings().add<Transform3D<> >("EndTtcp","Transform from robot end to tcp", Transform3D<>());
	_deviceInterface = getSettings().add<EntityReference<hwl::Manipulator> >("Manipulator", "Interface of the device to move", EntityReference<hwl::Manipulator>("Manipulator"));
}

bool SetManipulatorTCP::execute(ExecutionManager::Ptr execManager)
{
	if (_useWorkCellTCP->getValue())
	{
		System::Ptr system = execManager->getSystem();
		system->update();
		
		Frame* tcpFrame = execManager->getSystem()->getWorkCell()->findFrame(_tcpFrameId->getValue());
		Transform3D<> endTtcp = Kinematics::frameTframe(system->getManipulator()->getEnd(), tcpFrame, execManager->getSystem()->getState());
		_deviceInterface->getValue().get()->setEndToTCP(endTtcp);
	}
	else {
		Transform3D<> endTtcp = _endTtcp->getValue();
		_deviceInterface->getValue().get()->setEndToTCP(endTtcp);
	}
	return true;
}
		

/* Methods from the serializable interface */
void SetManipulatorTCP::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    ER_LOG_DEBUG("SetManipulatorTCP::read");
    iarchive.readEnterScope("SetManipulatorTCP");
    Instruction::read(iarchive, id);

	bool useWorkCellTCP = iarchive.read<bool>("UseWorkCellTCP");
	_useWorkCellTCP->setValue(useWorkCellTCP);
	if (useWorkCellTCP) 
	{
		std::string tcpFrameId = iarchive.read<std::string>("WorkCellTCPFrameID");
		_tcpFrameId->setValue(tcpFrameId);
	}
	else
	{
		Transform3D<> t3d = iarchive.read<Transform3D<> >("EndTtcp");
		_endTtcp->setValue(t3d);
	}
    std::string deviceIdentifier = iarchive.read<std::string>("Manipulator");
    ER_LOG_DEBUG("Device Identifier = "<<deviceIdentifier);
    _deviceInterface->getValue().set(deviceIdentifier);

    iarchive.readLeaveScope("SetManipulatorTCP");
}

void SetManipulatorTCP::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("SetManipulatorTCP");

	Instruction::write(oarchive, id);
	oarchive.write(_useWorkCellTCP->getValue(), "UseWorkCellTCP");
	if (_useWorkCellTCP->getValue()) 
	{
		oarchive.write(_tcpFrameId->getValue(), "WorkCellTCPFrameID");
	}
	else {
		oarchive.write(_endTtcp->getValue(), "EndTtcp");
	}
    oarchive.write(_deviceInterface->getValue().get()->getName(), "Manipulator");
    oarchive.writeLeaveScope("SetManipulatorTCP");
}

