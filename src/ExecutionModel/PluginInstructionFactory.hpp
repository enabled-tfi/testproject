/* */
#ifndef SWM_PLUGININSTRUCTIONFACTORY_HPP
#define SWM_PLUGININSTRUCTIONFACTORY_HPP

#include "Instruction.hpp"

#include <rw/common/Ptr.hpp>
#include <rw/common/PropertyMap.hpp>



namespace swm {
	


/**
 *
 */
class PluginInstructionFactory: public rw::plugin::PluginFactory<Instruction>
{
public:
	/** @brief Smart pointer to the InstructionFactory object*/
	typedef rw::common::Ptr<PluginInstructionFactory> Ptr;

};

} //end namespace

#endif //#ifndef SWM_PLUGININSTRUCTIONFACTORY_HPP
