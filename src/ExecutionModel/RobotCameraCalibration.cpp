#include "RobotCameraCalibration.hpp"

#include <rw/common/TimerUtil.hpp>

#include "../Common/EntityRegister.hpp"
#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>

//#include "MarkerDetectionArUco.hpp"

#include <WorkCellCalibration/src/CalibrationMeasurement.hpp>
#include <WorkCellCalibration/src/xml/XMLCalibrationMeasurementFile.hpp>
#include <WorkCellCalibration/src/xml/XmlCalibrationSaver.hpp>
#include <WorkCellCalibration/src/WorkCellExtrinsicCalibrator.hpp>
#include <WorkCellCalibration/src/WorkCellCalibration.hpp>
#include <WorkCellCalibration/src/CalibrationUtils.hpp>

#include <rwlibs/calibration/FixedFrameCalibration.hpp>
#include <rwlibs/calibration/WorkCellCalibration.hpp>
#include <rwlibs/calibration/xml/XmlCalibrationSaver.hpp>

#include <boost/foreach.hpp>

using namespace exm;
using namespace er_common;
using namespace rw::common;
using namespace rw::math;
using namespace rw::kinematics;
using namespace rw::models;
using namespace rw::trajectory;
using namespace sdurobotics::calibration;

RobotCameraCalibration::RobotCameraCalibration():
	Instruction("RobotCameraCalibration", "")
{
    initializeSettings();
    setIsInitialized(false);
}


RobotCameraCalibration::RobotCameraCalibration(const rw::trajectory::QPath& path, const std::string& calibrationFileName, const std::string& deviceIdentifier, const std::string& cameraIdentifier):
	Instruction("RobotCameraCalibration", "Robot camera calibration")
{
    initializeSettings();
    _path->setValue(path);
	_calibrationFileName->setValue(calibrationFileName);
	if (EntityRegister::has(deviceIdentifier)) {
		_deviceInterface->getValue().set(EntityRegister::get<hwl::Manipulator>(deviceIdentifier));
	}
	else {
		_deviceInterface->getValue().set(deviceIdentifier);
	}

	if (EntityRegister::has(cameraIdentifier)) {
		_cameraInterface->getValue().set(EntityRegister::get<hwl::Camera>(cameraIdentifier));
	}
	else {
		_cameraInterface->getValue().set(cameraIdentifier);
	}
	
	setIsInitialized(true);
}


RobotCameraCalibration::RobotCameraCalibration(const QPath& path, const std::string& calibrationFileName, hwl::Manipulator::Ptr deviceInterface, hwl::Camera::Ptr cameraInterface):
	Instruction("RobotCameraCalibration", "Robot camera calibration")
{
    initializeSettings();
    _path->setValue(path);
	_calibrationFileName->setValue(calibrationFileName);
    _deviceInterface->setValue(deviceInterface);
	_cameraInterface->setValue(cameraInterface);
    setIsInitialized(true);
}

bool RobotCameraCalibration::execute(ExecutionManager::Ptr execManager)
{
    ER_LOG_DEBUG("RobotCameraCalibration::Execute");
	WorkCell::Ptr workcell = System::getInstance()->getWorkCell();

	FixedFrame* cameraFrame = workcell->findFrame<FixedFrame>("Camera");
	if (cameraFrame == NULL)
		RW_THROW("No Camera frame found");
	FixedFrame* markerFrame = workcell->findFrame<FixedFrame>("Marker");
	if (markerFrame == NULL)
		RW_THROW("No Marker frame found");


	//MarkerDetectionArUco markerDetection(_cameraInterface->getValue().get());
	System::Ptr system = System::getInstance();
	std::vector<CalibrationMeasurement::Ptr> measurements;
	Instruction::Ptr markerDetection = NULL;
	std::string knowledgeBaseId;
	if (_markerDetectionAlgorithm->getValue() == 1) {
		markerDetection = &_markerDetectionArUco;
		knowledgeBaseId = _markerDetectionArUco.getSettings().get<std::string>("AruCo_KnowledgebaseId");
	}
	else {
		markerDetection = &_markerDetectionChessboard;
		knowledgeBaseId = _markerDetectionChessboard.getSettings().get<std::string>("Chessboard_KnowledgebaseId");
	}
	BOOST_FOREACH(const Q& q, _path->getValue()) {
        ER_LOG_DEBUG("\tMove To: "<<q);
		ER_LOG_DEBUG("Device Interface = " << _deviceInterface->getValue().get() );
		_deviceInterface->getValue().get()->ptp(q);
		ER_LOG_DEBUG("Start Sleep " );
		TimerUtil::sleepMs(1000);
		ER_LOG_DEBUG("End Sleep " );
		try {
			
			execManager->executeBlocking(markerDetection);

			if (execManager->getExecutionState() == ExecutionManager::Success&&  system->getKnowledgeBase()->has(knowledgeBaseId)) {
				Transform3D<> camTmarker1 = system->getKnowledgeBase()->get<Transform3D<> >(knowledgeBaseId);
				ER_LOG_DEBUG("Camera to marker: " << camTmarker1 );
				measurements.push_back(ownedPtr(new CalibrationMeasurement(q, inverse(camTmarker1), "UR10", "Marker", "Camera")));
			}
			else {
				ER_LOG_WARNING("No marker found" );

			}
		}
		catch (const std::exception& exp) {
			ER_LOG_WARNING(" Failure detecting marker with message: " << exp.what() );
		}		
	}

	XMLCalibrationMeasurementFile<>::save(measurements, std::string("Measurements")+_calibrationFileName->getValue());

	WorkCellExtrinsicCalibrator excalibrator(workcell, &rw::common::Log::debugLog());
	excalibrator.setMeasurements(measurements);
	excalibrator.setUsePosition(true);
	excalibrator.setUseRotation(true);
	
	std::vector<WorkCellCalibration::DeviceFramePair> deviceFramePairs;
	deviceFramePairs.push_back(std::make_pair(execManager->getSystem()->getManipulator(), cameraFrame));

	std::vector<Frame*> staticFrames;
	staticFrames.push_back(markerFrame);
	

	ER_LOG_DEBUG("Calibration Errors without calibration " );
	CalibrationUtils::printMeasurementSummary(measurements, workcell, system->getState(), std::cout, true);

	WorkCellCalibration::Ptr calibration = ownedPtr(new WorkCellCalibration(deviceFramePairs, staticFrames));
	ER_LOG_DEBUG("Ready to calibrate " ); 
	excalibrator.calibrate(calibration, system->getState());
	ER_LOG_DEBUG("Calibrated" );
	XmlCalibrationSaver::save(calibration, _calibrationFileName->getValue());

	calibration->apply();
	ER_LOG_DEBUG("Calibration Errors with calibration " );
	CalibrationUtils::printMeasurementSummary(measurements, workcell, system->getState(), std::cout, true);

	std::vector<std::pair<SerialDevice::Ptr, Frame*> > devframePair;
	std::vector<Frame*> frames;

	rwlibs::calibration::WorkCellCalibration::Ptr workcellCalibration = ownedPtr(new rwlibs::calibration::WorkCellCalibration());
	
	rwlibs::calibration::FixedFrameCalibration::Ptr markerCalibration = ownedPtr(new rwlibs::calibration::FixedFrameCalibration(markerFrame, markerFrame->getFixedTransform()));
	rwlibs::calibration::FixedFrameCalibration::Ptr cameraCalibration = ownedPtr(new rwlibs::calibration::FixedFrameCalibration(cameraFrame, cameraFrame->getFixedTransform()));

	workcellCalibration->addCalibration(markerCalibration);
	workcellCalibration->addCalibration(cameraCalibration);

	rwlibs::calibration::XmlCalibrationSaver::save(workcellCalibration, _calibrationFileName->getValue());

	return true;
}
		
const QPath& RobotCameraCalibration::getTargets() const {
	return _path->getValue();

}

void RobotCameraCalibration::initializeSettings()
{
    _path = getSettings().add<QPath>("Path", "Configurations of the path", QPath());
	_calibrationFileName = getSettings().add<std::string>("CalibrationFileName", "Calibration File Name", "RobotCameraCalibration.xml");
	//_deviceInterface = getSettings().add<hwl::Manipulator::Ptr>("Manipulator", "Interface of the device to move", NULL);
	_deviceInterface = getSettings().add<EntityReference<hwl::Manipulator> >("Manipulator", "Interface of the device to move", EntityReference<hwl::Manipulator>("Manipulator"));
	//_cameraInterface = getSettings().add<EntityReference<hwl::Camera::Ptr> >("Camera", "Interface of the camera", EntityReference<hwl::Camera::Ptr>("Camera"));
	_markerDetectionAlgorithm = getSettings().add<int>("MarkerDetectionAlgorithm", "1: AruCo, 2: Chessboard", 1);
	_markerDetectionAlgorithm->setRange(1, 2);
	getSettings().add(&(_markerDetectionArUco.getSettings()), false);
	getSettings().add(&(_markerDetectionChessboard.getSettings()), false);
}

/* Methods from the serializable interface */
void RobotCameraCalibration::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    //ER_LOG_DEBUG("MovePTP::read");
    iarchive.readEnterScope("RobotCameraCalibration");
    Instruction::read(iarchive, id);

	//QPath path = iarchive.read<QPath>("Path");
    //_path->setValue(path);

	//std::string calibrationFileName = iarchive.read<std::string>("CalibrationFileName");
	//_calibrationFileName->setValue(calibrationFileName);

    //std::string deviceIdentifier = iarchive.read<std::string>("Manipulator");
	//_deviceInterface->getValue().set(deviceIdentifier);

	//std::string cameraIdentifier = iarchive.read<std::string>("Camera");
	//_cameraInterface->getValue().set(cameraIdentifier);
	getSettings().read(iarchive, "Settings");
	//ER_LOG_DEBUG("Device Identifier = "<<deviceIdentifier);
    //_deviceInterface->setValue(EntityRegister::get<hwl::Manipulator::Ptr>(deviceIdentifier));
    iarchive.readLeaveScope("RobotCameraCalibration");
}

void RobotCameraCalibration::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("RobotCameraCalibration");
    Instruction::write(oarchive, id);
	//oarchive.write(_path->getValue(), "Path");
	//oarchive.write(_calibrationFileName->getValue(), "CalibrationFileName");
	//oarchive.write(_deviceInterface->getValue().getName(), "Manipulator");
	//oarchive.write(_cameraInterface->getValue().getName(), "Camera");

	getSettings().write(oarchive, "Settings");
	//WRITE THE MARKER DETECTION TO FILE S.T. SETTINGS ARE STORED

	oarchive.writeLeaveScope("RobotCameraCalibration");
}

