#include "ProgramState.hpp"

#include <rw/common/macros.hpp>

using namespace swm;

ProgramState::ProgramState() {

}

void ProgramState::set(int id, const boost::any& val) {
	_map[id] = val;
}

boost::any& ProgramState::get(int id) {
	if (has(id))
		return _map[id];
	RW_THROW("No element with id "<<id);
}

bool ProgramState::has(int id) {
	return _map.find(id) != _map.end();
}

void ProgramState::remove(int id) {
    if (_map.find(id) != _map.end()) {
        _map.erase(id);
    }
}
