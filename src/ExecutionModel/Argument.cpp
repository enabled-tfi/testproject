#include "Argument.hpp"

using namespace exm;


Argument::Argument(rw::common::PropertyMap::Ptr propertymap):
 	 _propertymap(propertymap)
{

}

Argument::Argument()
{
   _propertymap = rw::common::ownedPtr(new rw::common::PropertyMap());
}

rw::common::PropertyMap::Ptr Argument::getPropertyMap() const
{
    return _propertymap;
}

void Argument::setPropertyMap(rw::common::PropertyMap::Ptr propertymap)
{
    _propertymap = propertymap;
}

/* Methods from the serializable interface */
void Argument::read(er_serialization::InputArchive& iarchive, const std::string& id)
{

}

void Argument::write(er_serialization::OutputArchive& oarchive, const std::string& id) const
{

}
