/* */
#ifndef EXM_FOR_HPP
#define EXM_FOR_HPP

#include "Instruction.hpp"
#include "ExecutionManager.hpp"
#include <rw/common/Ptr.hpp>
#include <rw/common/PropertyMap.hpp>

namespace exm {
	


/**
 * Instruction for implementing a For-loop which counts based on integers.
 */
class For: public Instruction
{
public:
	/** @brief Smart pointer to the For loop object*/
	typedef rw::common::Ptr<For> Ptr;

	/**
	 * @brief Default Construction
	 */
	For();

	/**
	 * @brief Construct For loop object.
	 *
	 * The for loop works as for (int i = start; i<end; i+=step)
	 *
	 * @param start [in] The start value
	 * @param end [in] The value determining the end
	 * @param step [in] How large step to take
	 */
	For(int start, int end, int step, Instruction::Ptr body);


	/**
	 * @copydoc Instruction::execute
	 *
	 * Stores the current index value in the program state.
	 */
	bool execute(ExecutionManager::Ptr execManager);


    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


protected:
    void initializeSettings();


private:
	er_common::Setting<int>::Ptr _start;
	er_common::Setting<int>::Ptr _end;
	er_common::Setting<int>::Ptr _step;
	er_common::Setting<Instruction::Ptr>::Ptr _body;


};

} //end namespace

#endif //#ifndef EXM_SEQUENTIALINSTRUCTION_HPP
