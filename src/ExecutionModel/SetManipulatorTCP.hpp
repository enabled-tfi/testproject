/* */
#ifndef EXM_SETMANIPULATORTCP_HPP
#define EXM_SETMANIPULATORTCP_HPP

#include "Instruction.hpp"
#include <HardwareLayer/Manipulator.hpp>
#include <Common/EntityReference.hpp>
#include <rw/math.hpp>

#include <vector>
namespace exm {
	
/**
 * @brief Action for moving robot relative to the current pose with a linear path in tool space
 */
class SetManipulatorTCP: public Instruction
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<SetManipulatorTCP> Ptr;

	SetManipulatorTCP();


	SetManipulatorTCP(const rw::math::Transform3D<>& endTtcp, const std::string& deviceIdentifier);

	SetManipulatorTCP(const std::string& tcpFrameId, const std::string& deviceIdentifier);


	/**
	 * @brief Constructs MoveLin action moving to \btarget using \bdeviceInterface
	 * @param target [in] Target to which to move.
	 * @param deviceInterface [in] Interface to the device to move
	 */
	SetManipulatorTCP(const rw::math::Transform3D<>& endTtcp, hwl::Manipulator::Ptr deviceInterface);

	SetManipulatorTCP(const std::string& tcpFrameId, hwl::Manipulator::Ptr deviceInterface);

    bool execute(ExecutionManager::Ptr execManager);


    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


protected:
   void initializeSettings();

private:
	er_common::Setting<bool>::Ptr _useWorkCellTCP;
	er_common::Setting<std::string>::Ptr _tcpFrameId;
	er_common::Setting<rw::math::Transform3D<> >::Ptr _endTtcp;
	er_common::Setting < er_common::EntityReference<hwl::Manipulator> >::Ptr _deviceInterface;
};

} //end namespace

#endif //#ifndef EXM_SETMANIPULATORTCP_HPP
