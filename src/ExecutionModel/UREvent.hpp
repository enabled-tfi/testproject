/*
 * UREvent.hpp
 *
 *  Created on: May 1, 2019
 *      Author: thomas
 */

#ifndef SRC_EXECUTIONMODEL_UREVENT_HPP_
#define SRC_EXECUTIONMODEL_UREVENT_HPP_

#include <ExecutionModel/Instruction.hpp>

namespace exm {

class UREvent : public Instruction {
public:
	UREvent();
	UREvent(std::string identifier, std::string eventName);
	virtual ~UREvent();

    bool execute(ExecutionManager::Ptr execManager);


protected:
   void initializeSettings();

private:
	er_common::Setting<std::string>::Ptr _eventName;
	er_common::Setting<std::vector<std::string> >::Ptr _params;

};

} /* namespace hwl */

#endif /* SRC_EXECUTIONMODEL_UREVENT_HPP_ */
