#include "MessagePopup.hpp"
#include "HMI.hpp"

#include <Common/InputArchive.hpp>
#include <Common/OutputArchive.hpp>

#include <rw/common/TimerUtil.hpp>

using namespace exm;
using namespace er_common;
using namespace rw::common;

MessagePopup::MessagePopup():
        Instruction("MessagePopup", "")
{
    initializeSettings();
    setIsInitialized(false);
}

MessagePopup::MessagePopup(const std::string& title, const std::string& message):
	Instruction("MessagePopup", "")
{
	initializeSettings();
	_title->setValue(title);
	_message->setValue(message);

	setIsInitialized(true);
}

void MessagePopup::initializeSettings()
{
    _title = getSettings().add<std::string>("Title", "Title of MessageBox", "");
    _message = getSettings().add<std::string>("Message", "Message in MessageBos", "");
}

bool MessagePopup::execute(ExecutionManager::Ptr execManager)
{
	bool blocking = true;
	HMI::instance()->infoBox(_title->getValue(), _message->getValue(), &blocking); 
	while (blocking)
	{
		TimerUtil::sleepMs(10);
	}
	return true;
}
		
void MessagePopup::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    ER_LOG_DEBUG("MessagePopup::read");
    iarchive.readEnterScope("MessagePopup");
    Instruction::read(iarchive, id);

    _title->setValue(iarchive.read<std::string>("Title"));
    _message->setValue(iarchive.read<std::string>("Message"));

    iarchive.readLeaveScope("MessagePopup");
}

void MessagePopup::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("MessagePopup");
    Instruction::write(oarchive, id);

    oarchive.write(_title->getValue(), "Title");
    oarchive.write(_message->getValue(), "Message");
    oarchive.writeLeaveScope("MessagePopup");
}
