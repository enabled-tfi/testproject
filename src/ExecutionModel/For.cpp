#include "For.hpp"
#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>
#include <Common/EntityRepository.hpp>

using namespace exm;
using namespace er_common;
//using namespace rw::common;

For::For():
   Instruction("For", "")
{
    initializeSettings();
    setIsInitialized(false);
}

For::For(int start, int end, int step, Instruction::Ptr body):
   Instruction("For", "")
{
    initializeSettings();
    _start->setValue(start);
    _end->setValue(end);
    _step->setValue(step);
    _body->setValue(body);
    setIsInitialized(true);
}

void For::initializeSettings() {
    _start = getSettings().add<int>("Start", "Value of a in: for (int i = a; i<b; i+=step", 0);
    _end = getSettings().add<int>("End", "Value of b in: for (int i = a; i<b; i+=step", 0);
    _step = getSettings().add<int>("Step", "Value of step in: for (int i = a; i<b; i+=step", 1);
    _body = getSettings().add<Instruction::Ptr>("Body", "Body of the for loop", NULL);
}

bool For::execute(ExecutionManager::Ptr execManager)
{
    for (int i = _start->getValue(); i<_end->getValue(); i += _step->getValue()) {
        execManager->executeBlocking(_body->getValue());
    }
    return true;
}


/* Methods from the serializable interface */
void For::read(er_serialization::InputArchive& iarchive, const std::string& id)
{
    ER_LOG_DEBUG("For::read");
    iarchive.readEnterScope("For");
    Instruction::read(iarchive, id);

    int start = iarchive.read<int>("Start");
    _start->setValue(start);
    int end = iarchive.read<int>("End");
    _end->setValue(end);
    int step = iarchive.read<int>("Step");
    _step->setValue(step);

    ER_LOG_DEBUG("Start = "<<start<<" End = "<<end<<" step = "<<step);


	iarchive.readEnterScope("Instruction");

	std::vector<std::pair<std::string, er_serialization::InputArchive::Ptr> > subarchs = iarchive.getSubArchives();
	if (subarchs.size() > 0) {
		Instruction::Ptr instruction = EntityRepository<Instruction>::make(subarchs.front().first);
		_body->setValue(instruction);
	}
	
    //_body->setValue(iarchive.readInstruction(""));
    iarchive.readLeaveScope("Instruction");

    iarchive.readLeaveScope("For");
}

void For::write(er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("For");
    Instruction::write(oarchive, id);
    oarchive.write(_start->getValue(), "Start");
    oarchive.write(_end->getValue(), "End");
    oarchive.write(_step->getValue(), "Step");
    oarchive.writeEnterScope("Instruction");
    _body->getValue()->write(oarchive, "Instruction");
    oarchive.writeLeaveScope("Instruction");
    oarchive.writeLeaveScope("Sequence");
}

