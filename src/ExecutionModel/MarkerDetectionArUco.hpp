/* */
#ifndef EXM_MARKERDETECTIONARUCO_HPP
#define EXM_MARKERDETECTIONARUCO_HPP

#include "MarkerDetection.hpp"
#include "ExecutionManager.hpp"
#include <Common/EntityReference.hpp>
#include <HardwareLayer/Camera.hpp>

#include <ostream>
#include <string>

namespace exm {

/** 
 * @brief Performs a marker detection based on the OpenCV ArUco marker detection
 */
class MarkerDetectionArUco: public MarkerDetection
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<MarkerDetectionArUco> Ptr;

	/**
	 * @brief Default Constructor
	 */
	MarkerDetectionArUco();

	MarkerDetectionArUco(hwl::Camera::Ptr camera, int execCounter);

	bool execute(ExecutionManager::Ptr execManager);

    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

	const cv::Mat& getDetectionImage() const;

	bool isDetected();

	cv::Mat getRvecs();
	cv::Mat getTvecs();

protected:
    virtual void initializeSettings();

private:

	er_common::Setting < er_common::EntityReference<hwl::Camera> >::Ptr _camera;
	er_common::Setting<double>::Ptr _markerSize;
	er_common::Setting<int>::Ptr _arucoMarkerId;
	er_common::Setting<int>::Ptr _arucoDatabase;
	er_common::Setting<std::string>::Ptr _knowledgeBaseIdentifier;
	cv::Mat  _rvecs, _tvecs;
	int _arucoIdIndx;
	cv::Mat _detectionImage;
	int _executionCount;
	bool _detected;
	//er_common::Setting<hwl::Camera::Ptr>::Ptr _camera;
};

} //end namespace

#endif //#ifndef EXM_MARKERDETECTIONARUCO_HPP
