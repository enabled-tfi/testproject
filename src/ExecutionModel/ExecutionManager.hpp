/* */
#ifndef EXM_EXECUTIONMANAGER_HPP
#define EXM_EXECUTIONMANAGER_HPP


#include "../Common/System.hpp"
#include <rw/common/Ptr.hpp>

#include <thread>
#include <mutex>

namespace exm {
	
class Instruction;

/**
 *
 */
class ExecutionManager
{
public:
	typedef rw::common::Ptr<ExecutionManager> Ptr;

	ExecutionManager(er_common::System::Ptr system);

	enum ExecutionResult { ExecutionSuccess = 0, ExecutionStopped = 1, ExecutionError = 2};

	void executeNonBlocking(rw::common::Ptr<Instruction> instruction, const std::function<void(ExecutionResult)>& callback = std::function<void(ExecutionResult)>());

	void executeBlocking(rw::common::Ptr<Instruction> instruction);

	void stop();
	void pause();
	void resume();

	er_common::System::Ptr getSystem();


	enum ExecutionState { Idle = 0, Executing = 1, Success = 2, Pausing = 3, Paused = 4, Stopping = 5, Stopped = 6, Error = 7};


	ExecutionState getExecutionState() const;
	const std::string& getErrorMessage() const;
private:
	er_common::System::Ptr _system;
	rw::common::Ptr<Instruction> _instruction;
	rw::common::Ptr<std::thread> _thread;
	mutable std::mutex _mutex;
	void run();

	ExecutionState _executionState;
	std::string _errorMessage;
	bool _stopExecution;
	bool _pauseExecution;
	std::function<void(ExecutionResult)> _executionFinishedCallback;

	void doExecute(rw::common::Ptr<Instruction> instruction);
	void setExecutionState(ExecutionState state);

};

} //end namespace

#endif //#ifndef EXM_EXECUTIONMANAGER_HPP
