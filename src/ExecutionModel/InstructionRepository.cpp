#include "InstructionRepository.hpp"

#include "For.hpp"
#include "If.hpp"
#include "Print.hpp"
#include "Sequence.hpp"
#include "Wait.hpp"

#include "DriveRel.hpp"
#include "DriveTo.hpp"
#include "MoveLin.hpp"
#include "MoveLinRel.hpp"
#include "MovePTP.hpp"
#include "MoveQPlanned.hpp"
#include "MoveT3DPlanned.hpp"
#include "SetManipulatorTCP.hpp"
#include "MessagePopup.hpp"
#include "ExerciseImpulse.hpp"
#include "ScanWithMotion.hpp"
#include "AlignICP.hpp"
using namespace exm;

using namespace rw::plugin;
using namespace rw::common;


InstructionRepository::InstructionRepository()
{

    _instructionFactories[For().getName()] = ownedPtr(new InstructionFactory<For>());
    _instructionFactories[If().getName()] = ownedPtr(new InstructionFactory<If>());
    _instructionFactories[Print().getName()] = ownedPtr(new InstructionFactory<Print>());
    _instructionFactories[Sequence().getName()] = ownedPtr(new InstructionFactory<Sequence>());
    _instructionFactories[Wait().getName()] = ownedPtr(new InstructionFactory<Wait>());

    _instructionFactories[DriveRel().getName()] = ownedPtr(new InstructionFactory<DriveRel>());
    _instructionFactories[DriveTo().getName()] = ownedPtr(new InstructionFactory<DriveTo>());


    _instructionFactories[MoveLin().getName()] = ownedPtr(new InstructionFactory<MoveLin>());
	_instructionFactories[MoveLinRel().getName()] = ownedPtr(new InstructionFactory<MoveLinRel>());
    _instructionFactories[MovePTP().getName()] = ownedPtr(new InstructionFactory<MovePTP>());
	_instructionFactories[MoveQPlanned().getName()] = ownedPtr(new InstructionFactory<MoveQPlanned>());
	_instructionFactories[MoveT3DPlanned().getName()] = ownedPtr(new InstructionFactory<MoveT3DPlanned>());
	_instructionFactories[SetManipulatorTCP().getName()] = ownedPtr(new InstructionFactory<SetManipulatorTCP>());
	_instructionFactories[MessagePopup().getName()] = ownedPtr(new InstructionFactory<MessagePopup>());
	_instructionFactories[ExerciseImpulse().getName()] = ownedPtr(new InstructionFactory<ExerciseImpulse>());

	_instructionFactories[ScanWithMotion().getName()] = ownedPtr(new InstructionFactory<ScanWithMotion>());
	_instructionFactories[AlignICP().getName()] = ownedPtr(new InstructionFactory<AlignICP>());



}

void InstructionRepository::load(const std::string& filename)
{
    InstructionFactoryBase::Ptr constructor;
    try
    {
        rw::common::Ptr<DynamicLibraryLoader<InstructionFactoryBase> > loader = ownedPtr(new DynamicLibraryLoader<InstructionFactoryBase>(filename));
        constructor = ownedPtr(loader->get());
        _loaders.push_back(loader);
    } catch (const Exception& exp)
    {
        RW_THROW("Unable to load plugin: "<<filename<<". Failed with message: "<<exp.getMessage().getText());
    }

    if (constructor != NULL)
    {
        const std::string id = constructor->getIdentifier();
        if (_instructionFactories.find(id) == _instructionFactories.end()) {
            _instructionFactories[id] = constructor;
        } else {
            RW_THROW("A InstructionFactory with identifier "<<id<<" has already been loaded!");
        }
    } else {
        RW_THROW("Unable to create InstructionFactory from: "<<filename);
    }

}

Instruction::Ptr InstructionRepository::make(const std::string& id)
{
    if (_instructionFactories.find(id) == _instructionFactories.end())
    {
        RW_THROW("Could not find any Instruction Factory for id '"<<id<<"'");
    }
    return _instructionFactories[id]->make();
}

std::vector<std::string> InstructionRepository::getInstructionIds() const
{
    std::vector<std::string> result;
    for (std::map<std::string, InstructionFactoryBase::Ptr>::const_iterator it = _instructionFactories.begin(); it != _instructionFactories.end(); ++it)
    {
        result.push_back((*it).first);
    }

    return result;
}
