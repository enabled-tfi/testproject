/* */
#ifndef EXM_INSTRUCTIONFACTORY_HPP
#define EXM_INSTRUCTIONFACTORY_HPP

#include "Instruction.hpp"

#include <rw/plugin/FactoryMacro.hpp>
#include <rw/common/Ptr.hpp>
#include <rw/common/PropertyMap.hpp>

#define EXPORT_INSTRUCTION_FACTORY(name) \
    namespace { \
        class MyInstructionFactory: public InstructionFactory<name> { \
        }; \
        DLL_FACTORY_METHOD(MyInstructionFactory) \
    }




namespace exm {
	


/**
 *
 */

class InstructionFactoryBase
{
public:
	/** @brief Smart pointer to the InstructionFactory object*/
	typedef rw::common::Ptr<InstructionFactoryBase> Ptr;

	InstructionFactoryBase(const std::string& name, const std::string& description);

	virtual Instruction::Ptr make() = 0;

	const std::string& getIdentifier() const;

	const std::string& getDescription() const;

private:
	std::string _identifier;
	std::string _description;

};

template <class T>
class InstructionFactory: public InstructionFactoryBase
{
public:
    InstructionFactory():
        InstructionFactoryBase(T().getName(), T().getDescription())
    {}

    virtual Instruction::Ptr make() {
        return rw::common::ownedPtr(new T());
    }
};

} //end namespace

#endif //#ifndef EXM_INSTRUCTIONFACTORY_HPP
