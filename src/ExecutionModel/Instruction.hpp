/* */
#ifndef EXM_INSTRUCTION_HPP
#define EXM_INSTRUCTION_HPP

#include "ExecutionManager.hpp"
#include <Common/Serializable.hpp>
#include <Common/Settings.hpp>
#include <Common/Entity.hpp>

#include <rw/common/Ptr.hpp>
//#include <rw/common/PropertyMap.hpp>

namespace exm {
	


/**
 * General interface for Instructions
 */
class Instruction: public er_common::Entity
{
public:
	/** @brief Smart pointer to the Instruction object*/
	typedef rw::common::Ptr<Instruction> Ptr;


	Instruction(const std::string& identifier, const std::string& description = "");


	virtual er_common::Settings& getSettings();
	virtual const er_common::Settings& getSettings() const;

	virtual void initialize(const er_common::Settings& settings);

	bool isInitialized() const;

	void setIsInitialized(bool isInitialized);


	/**
	 * @brief Method for executing the instruction.
	 * @return The next instruction to call
	 */
	virtual bool execute(rw::common::Ptr<ExecutionManager> execManager) = 0;

	//

	//const std::string& getName() const;
	//const std::string& getDescription() const;

	/* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


protected:
	virtual void initializeSettings() = 0;
	//virtual void executeBody(ExecutionManager::Ptr execManager);


private:
	//std::string _name;
	//std::string _description;
	bool _isInitialized;
	int _id;
	er_common::Settings _settings;


};

} //end namespace

#endif //#ifndef EXM_INSTRUCTION_HPP
