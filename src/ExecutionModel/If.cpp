#include "If.hpp"

#include <Common/EntityRepository.hpp>

#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>

using namespace exm;
using namespace er_common;

If::If():
    Instruction("If","")
{
    initializeSettings();
    setIsInitialized(false);
}

If::If(Argument::Ptr argument, Instruction::Ptr ifBody, Instruction::Ptr elseBody):
    Instruction("If", "")
{
    initializeSettings();
    try {
        argument->get<bool>();
    }
    catch (const std::exception& )
    {
        RW_THROW("The argument provided for If does not evaluate to a boolean.");
    }

    _argument->setValue(argument);
    _ifBody->setValue(ifBody);
    _elseBody->setValue(elseBody);
    setIsInitialized(true);
}


void If::initializeSettings() {
    _argument = getSettings().add<Argument::Ptr>("Start", "Argument that evaluate to true of false", NULL);
    _ifBody = getSettings().add<Instruction::Ptr>("IfBody", "Body evaluated in case of the argument being true", NULL);
    _elseBody = getSettings().add<Instruction::Ptr>("ElseBody", "Body evaluated in case of the argument being false", NULL);
}



bool If::execute(ExecutionManager::Ptr execManager)
{
    if (_argument->getValue()->get<bool>())
    {
        execManager->executeBlocking(_ifBody->getValue());
    }
    else
    {
        if (_elseBody->getValue() != NULL)
            execManager->executeBlocking(_elseBody->getValue());
    }
    return true;
}


void If::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    ER_LOG_DEBUG("If::read");
    iarchive.readEnterScope("If");
    Instruction::read(iarchive, id);

    std::string argId = iarchive.read<std::string>("Argument");
    //ER_LOG_DEBUG("Argument Id "<<argId);
    //Argument::Ptr argument = iarchive.getArgument(argId);
    //_argument->setValue(argument);

    iarchive.readEnterScope("IfBody");

	std::vector<std::pair<std::string, er_serialization::InputArchive::Ptr> > subarchs = iarchive.getSubArchives();
	if (subarchs.size() > 0) {
		Instruction::Ptr instruction = EntityRepository<Instruction>::make(subarchs.front().first);
		_ifBody->setValue(instruction);
	}
    iarchive.readLeaveScope("IfBody");

    iarchive.readEnterScope("ElseBody");
	subarchs = iarchive.getSubArchives();
	if (subarchs.size() > 0) {
		Instruction::Ptr instruction = EntityRepository<Instruction>::make(subarchs.front().first);
		_elseBody->setValue(instruction);
	}
    iarchive.readLeaveScope("ElseBody");


    iarchive.readLeaveScope("If");
}

void If::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("If");
    Instruction::write(oarchive, id);

    //std::string argId = oarchive.addArgument(_argument->getValue());
    //oarchive.write(argId, "Argument");

    oarchive.writeEnterScope("IfBody");
    _ifBody->getValue()->write(oarchive, "IfBody");
    oarchive.writeLeaveScope("IfBody");
    oarchive.writeEnterScope("ElseBody");
    if (_elseBody->getValue() != NULL) {
        _elseBody->getValue()->write(oarchive, "ElseBody");
    }
    oarchive.writeLeaveScope("ElseBody");
    oarchive.writeLeaveScope("If");
}

