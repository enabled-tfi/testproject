#include "MovePTP.hpp"

#include <rw/common/TimerUtil.hpp>

#include "../Common/EntityRegister.hpp"
#include <Common/InputArchive.hpp>
#include <Common/OutputArchive.hpp>
#include <boost/foreach.hpp>

using namespace exm;
using namespace er_common;
using namespace rw::common;
using namespace rw::math;
using namespace rw::trajectory;


MovePTP::MovePTP():
	Instruction("MovePTP", "")
{
    initializeSettings();
    setIsInitialized(false);
}


MovePTP::MovePTP(const rw::trajectory::QPath& path, const std::string& deviceIdentifier):
	Instruction("MovePTP", "Move along path")
{
    initializeSettings();
    _path->setValue(path);
	if (EntityRegister::has(deviceIdentifier)) {
		_deviceInterface->getValue().set(EntityRegister::get<hwl::Manipulator>(deviceIdentifier));
	}
	else {
		_deviceInterface->getValue().set(deviceIdentifier);
	}
    setIsInitialized(true);
}


MovePTP::MovePTP(const rw::math::Q& qtarget, const std::string& deviceIdentifier):
	Instruction("MovePTP", "Move to target")
{
    initializeSettings();
    QPath path;
    path.push_back(qtarget);
    _path->setValue(path);
	if (EntityRegister::has(deviceIdentifier)) {
		_deviceInterface->getValue().set(EntityRegister::get<hwl::Manipulator>(deviceIdentifier));
	}
	else {
		_deviceInterface->getValue().set(deviceIdentifier);
	}
	setIsInitialized(true);
}

MovePTP::MovePTP(const QPath& path, hwl::Manipulator::Ptr deviceInterface):
	Instruction("MovePTP", "")
{
    initializeSettings();
    _path->setValue(path);
    _deviceInterface->setValue(deviceInterface);
    setIsInitialized(true);
}

MovePTP::MovePTP(const rw::math::Q& qtarget, hwl::Manipulator::Ptr deviceInterface):
	Instruction("MovePTP", "")
{
    initializeSettings();
    QPath path;
    path.push_back(qtarget);
    _path->setValue(path);
    _deviceInterface->setValue(deviceInterface);
    setIsInitialized(true);
}

bool MovePTP::execute(ExecutionManager::Ptr execManager)
{
    ER_LOG_DEBUG("MovePTP::Execute");
	BOOST_FOREACH(const Q& q, _path->getValue()) {
        ER_LOG_DEBUG("\tMove To: "<<q);
		ER_LOG_DEBUG("Device Interface = " << _deviceInterface->getValue().get() );
		_deviceInterface->getValue().get()->ptp(q);
		//TimerUtil::sleepMs(2000);
        //ER_LOG_DEBUG("Moved Completed");
	}
	return true;
}
		
const QPath& MovePTP::getTargets() const {
	return _path->getValue();

}

void MovePTP::initializeSettings()
{
    _path = getSettings().add<QPath>("Path", "Configurations of the path", QPath());
	
	//_deviceInterface = getSettings().add<hwl::Manipulator::Ptr>("Manipulator", "Interface of the device to move", NULL);
	_deviceInterface = getSettings().add<EntityReference<hwl::Manipulator> >("Manipulator", "Interface of the device to move", EntityReference<hwl::Manipulator>("Manipulator"));
}

/* Methods from the serializable interface */
void MovePTP::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    //ER_LOG_DEBUG("MovePTP::read");
    iarchive.readEnterScope("MovePTP");
    Instruction::read(iarchive, id);
	ER_LOG_DEBUG("Read Path " );
    QPath path = iarchive.read<QPath>("Path");
    _path->setValue(path);
	ER_LOG_DEBUG("Read Path " );

    std::string deviceIdentifier = iarchive.read<std::string>("Manipulator");
	_deviceInterface->getValue().set(deviceIdentifier);
	//ER_LOG_DEBUG("Device Identifier = "<<deviceIdentifier);
    //_deviceInterface->setValue(EntityRegister::get<hwl::Manipulator::Ptr>(deviceIdentifier));
    iarchive.readLeaveScope("MovePTP");
}

void MovePTP::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("MovePTP");
    Instruction::write(oarchive, id);
	oarchive.write(_path->getValue(), "Path");
	oarchive.write(_deviceInterface->getValue().getName(), "Manipulator");
	oarchive.writeLeaveScope("MovePTP");
}

