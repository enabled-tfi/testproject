#include "MoveT3DPlanned.hpp"

#include "MovePTP.hpp"

#include <rw/common/TimerUtil.hpp>
#include <rw/pathplanning/PlannerUtil.hpp>
#include <Common/EntityRegister.hpp>
#include <Common/Utils.hpp>
#include <Common/ManipulatorQ2QPlanner.hpp>
#include <Common/InputArchive.hpp>
#include <Common/OutputArchive.hpp>
#include <boost/foreach.hpp>

using namespace exm;
using namespace er_common;
using namespace rw::common;
using namespace rw::math;
using namespace rw::kinematics;
using namespace rw::trajectory;
using namespace rw::pathplanning;


MoveT3DPlanned::MoveT3DPlanned():
	Instruction("MoveT3DPlanned", "")
{
    initializeSettings();
    setIsInitialized(false);
}


MoveT3DPlanned::MoveT3DPlanned(const rw::math::Transform3D<>& refTtarget, const std::string& reference, const std::string& deviceIdentifier):
	Instruction("MoveT3DPlanned", "")
{
	initializeSettings();
	_target->getValue().setRefTtarget(refTtarget);
	_target->getValue().setReference(reference);
	if (EntityRegister::has(deviceIdentifier)) {
		_deviceInterface->getValue().set(EntityRegister::get<hwl::Manipulator>(deviceIdentifier));
	}
	else {
		_deviceInterface->getValue().set(deviceIdentifier);
	}
	setIsInitialized(true);
}





MoveT3DPlanned::MoveT3DPlanned(const rw::math::Transform3D<>& refTtarget, Frame* reference, hwl::Manipulator::Ptr deviceInterface):
	Instruction("MoveT3DPlanned", "")
{
	initializeSettings();
	_target->getValue().setRefTtarget(refTtarget);
	_target->getValue().setReference(reference);

	_deviceInterface->getValue().set(deviceInterface);
	setIsInitialized(true);
}



bool MoveT3DPlanned::execute(ExecutionManager::Ptr execManager)
{
    ER_LOG_DEBUG("MoveT3DPlanned::Execute");

	//Call update to ensure that states from all hardware entities has been read. 
	execManager->getSystem()->update();
	State state = execManager->getSystem()->getState();
	System::Ptr system = execManager->getSystem();
	Transform3D<> manipulatorTref = Kinematics::frameTframe(system->getManipulator()->getBase(), _target->getValue().getReference(), state);
	ER_LOG_DEBUG("Manipulator T Ref = " << manipulatorTref );
	//Transform3D<> worldTmanipulator = Kinematics::frameTframe(mm->getReferenceFrame().get(), mm->getManipulator()->getBase(), state);
	Transform3D<> refTtcp = _target->getValue().refTtarget();
	Transform3D<> manipulatorTtarget = manipulatorTref*refTtcp;

	//Find all IK solutions 
	//Transform3D<> endTtcp = Transform3D<>::identity();
	Transform3D<> endTtcp = _deviceInterface->getValue().get()->getEndToTCP();
	ER_LOG_DEBUG("TCP Transform = " << endTtcp );
	/*if (mm->getToolTCP() != NULL) {
		endTtcp = Kinematics::frameTframe(mm->getManipulator()->getEnd(), mm->getToolTCP().get(), state);
	}*/

	//TaskRedundacyIKSolver redundancyIKSolver(mm->getManipulatorIKSolver(), mm->getManipulator(), endTtcp, 12);
	ER_LOG_DEBUG("Manipulator T Target = " << manipulatorTtarget );
	ER_LOG_DEBUG("EndTtcp = " << endTtcp );
	ER_LOG_DEBUG(" Tries to solve IK for " << manipulatorTtarget*inverse(endTtcp) );
	std::vector<Q> qsol = system->getManipulatorIKSolver()->solve(manipulatorTtarget*inverse(endTtcp), state);
	ER_LOG_DEBUG("Qsol = " << qsol.size() );
	
	if (qsol.size() == 0) {
		RW_THROW("No inverse kinematics solution found for target: "<< refTtcp <<" with reference frame "<< _target->getValue().getReferenceFrameName());
	}

	//Select the most promising of the IK solutions
	Q qcurrent = system->getManipulator()->getQ(state);
	

	//TODO: Find another place to do this as it is time consuming:
	Q qweights = PlannerUtil::estimateMotionWeights(*system->getManipulator(), NULL, state, PlannerUtil::AVERAGE, 100);
	std::queue<Q> queue = Utils::sortQ(qsol, qcurrent, MetricFactory::makeWeightedEuclidean<Q>(qweights));


	ManipulatorQ2QPlanner::Ptr manipulatorPlanner = system->getManipulatorQ2QPlanner(state);
	
	while (queue.empty() == false) {
		Q q_manipulator = queue.front();
		queue.pop();

		if (manipulatorPlanner->inCollision(q_manipulator, state)) {
			ER_LOG_DEBUG("Potential Target Configuration "<<q_manipulator<<" in collision " );
			continue;
		}
		//qpath.clear();
		QPath qpath = manipulatorPlanner->query(qcurrent, q_manipulator, state);
		if (qpath.size() == 0) {
			continue;
		}
		else {
			ER_LOG_DEBUG("Found Solution Ready to move" );
			exm::MovePTP::Ptr movePTP = ownedPtr(new MovePTP(qpath, _deviceInterface->getValue().get()));
			movePTP->execute(execManager);
			return true;
		}
	}
	RW_THROW("Unable to find a path");
}

void MoveT3DPlanned::initializeSettings()
{
	_target = getSettings().add<Target>("Target", "Target and reference frame", Target());
    //_deviceInterface = getSettings().add<hwl::Manipulator::Ptr>("Manipulator", "Interface of the device to move", NULL);
	_deviceInterface = getSettings().add<EntityReference<hwl::Manipulator> >("Manipulator", "Interface of the device to move", EntityReference<hwl::Manipulator>("Manipulator"));
}

/* Methods from the serializable interface */
void MoveT3DPlanned::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    iarchive.readEnterScope("MoveT3DPlanned");
    Instruction::read(iarchive, id);

	_target->getValue().read(iarchive, "Target");

    std::string deviceIdentifier = iarchive.read<std::string>("Manipulator");
    _deviceInterface->getValue().set(deviceIdentifier);
    iarchive.readLeaveScope("MoveT3DPlanned");
}

void MoveT3DPlanned::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("MoveT3DPlanned");
    Instruction::write(oarchive, id);
	_target->getValue().write(oarchive, "Target");
    oarchive.write(_deviceInterface->getValue().getName(), "Manipulator");
	oarchive.writeLeaveScope("MoveT3DPlanned");
}

