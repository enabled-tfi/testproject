/* */
#ifndef EXM_MOVELIN_HPP
#define EXM_MOVELIN_HPP

#include "Instruction.hpp"
#include <HardwareLayer/Manipulator.hpp>
#include <Common/Target.hpp>
#include <Common/EntityReference.hpp>
#include <rw/math.hpp>

#include <vector>
namespace exm {
	
/**
 * @brief Instruction for moving robot with a linear path in tool space.
 */
class MoveLin: public Instruction
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<MoveLin> Ptr;

	MoveLin();


	MoveLin(const rw::math::Transform3D<>& refTtarget, const std::string& reference, const std::string& tcp, const std::string& deviceIdentifier);


	/**
	 * @brief Constructs MoveLin action moving to \btarget using \bdeviceInterface
	 * @param target [in] Target to which to move.
	 * @param deviceInterface [in] Interface to the device to move
	 */
	MoveLin(const rw::math::Transform3D<>& target, rw::kinematics::Frame* reference, rw::kinematics::Frame* tcp, hwl::Manipulator::Ptr deviceInterface);


    bool execute(ExecutionManager::Ptr execManager);


    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


protected:
   void initializeSettings();

private:	
	er_common::Setting<er_common::Target>::Ptr _target;
	//er_common::Setting<rw::math::Transform3D<> >::Ptr _refTtarget;
	//er_common::Setting<rw::kinematics::Frame*>::Ptr _reference;
	//std::string _referenceName;
	//er_common::Setting<hwl::Manipulator::Ptr>::Ptr _deviceInterface;
	er_common::Setting < er_common::EntityReference<hwl::Manipulator> >::Ptr _deviceInterface;
};

} //end namespace

#endif //#ifndef EXM_MOVELIN_HPP
