/* */
#ifndef EXM_INSTRUCTIONREPOSITORY_HPP
#define EXM_INSTRUCTIONREPOSITORY_HPP

#include "InstructionFactory.hpp"

#include <rw/common/Ptr.hpp>
#include <rw/common/PropertyMap.hpp>
#include <rw/plugin/DynamicLibraryLoader.hpp>
#include <map>


namespace exm {

/**
 * Repository collecting all buildin and addon instructions.
 */
class InstructionRepository
{
public:
	/** @brief Smart pointer to the Instruction object*/
	typedef rw::common::Ptr<InstructionRepository> Ptr;

	InstructionRepository();

	void load(const std::string& fileName);

	Instruction::Ptr make(const std::string& id);

	std::vector<std::string> getInstructionIds() const;


private:
	std::map<std::string, InstructionFactoryBase::Ptr> _instructionFactories;

	std::vector<rw::common::Ptr<rw::plugin::DynamicLibraryLoader<InstructionFactoryBase> > > _loaders;

};

} //end namespace

#endif //#ifndef EXM_INSTRUCTIONREPOSITORY_HPP
