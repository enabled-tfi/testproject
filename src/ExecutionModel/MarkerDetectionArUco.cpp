#include "MarkerDetectionArUco.hpp"

#include <rw/math/EAA.hpp>
#include <rw/math/Transform3D.hpp>
#include <Common/EntityRegister.hpp>

#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>

#include <opencv2/aruco.hpp>


using namespace exm;
using namespace hwl;
using namespace er_common;
using namespace rw::math;
using namespace rw::kinematics;
using namespace rw::common;
using namespace cv;

MarkerDetectionArUco::MarkerDetectionArUco() :
	MarkerDetection("MarkerDetectionArUco", "")
{
	initializeSettings();
	setIsInitialized(false);
	_executionCount = 0;
}

MarkerDetectionArUco::MarkerDetectionArUco(Camera::Ptr camera, int execCounter) :
    MarkerDetection("MarkerDetectionArUco", "")
{
	ER_LOG_DEBUG("Construction Aruco Marker Detection" );
	initializeSettings();
	_camera->getValue().set(camera);
	setIsInitialized(true);
	_executionCount = execCounter;
}


void MarkerDetectionArUco::initializeSettings()
{
	_camera = getSettings().add<EntityReference<hwl::Camera> >("Camera", "Interface of the camera", EntityReference<hwl::Camera>("Camera"));
	_markerSize = getSettings().add<double>("ArUco_MarkerSize", "Size of marking in meter", 0.1);
	_markerSize->setRange(0, 1.0);

	_arucoMarkerId = getSettings().add<int>("ArUco_MarkerId", "Id of Aruco marker", 0);
	_arucoMarkerId->setRange(0, 999999);

	_arucoDatabase = getSettings().add<int>("ArUco_Databse", "Id of Aruco marker", aruco::DICT_4X4_50);

	_knowledgeBaseIdentifier = getSettings().add<std::string>("ArUco_KnowledgebaseId", "Identifier for the results in the knowledge base", "ArUcoMarkerPose");

	//_camera = getSettings().add<Camera::Ptr>("Camera", "Camera for acquiring marker", NULL);

}

const cv::Mat& MarkerDetectionArUco::getDetectionImage() const
{
	return _detectionImage;
}


bool MarkerDetectionArUco::execute(ExecutionManager::Ptr execManager)
{
	_executionCount++;
	try {
		bool saveImages = er_common::System::getInstance()->getSaveImages();
		ER_LOG_DEBUG("MarkerDetectionArUco" );
		//Acquire Image
		Camera::Ptr camera = _camera->getValue().get();
		//ER_LOG_DEBUG("Camera = " << camera );
		
		for (int i = 0; i < 5; i++) {
			camera->acquire();
			TimerUtil::sleepMs(100); 
		}

		cv::Mat inputImage = camera->getLastImage();

		if(saveImages){
			std::stringstream imageOriginalfile;
			imageOriginalfile << er_common::System::getInstance()->getMarkerImageFolder() << "/ArucoImages/"<< _executionCount << "_ArucoInputImage_" << ".png";
			imwrite(imageOriginalfile.str(), inputImage);
		}



		//cv::cvtColor(inputImage, inputImage, cv::COLOR_BGR2GRAY);
//		imwrite("ImageForArucoMarkerDetection.png", inputImage);
		//cv::aruco::DetectorParameters parameters;
		cv::Ptr<aruco::DetectorParameters> params = cv::aruco::DetectorParameters::create();
		
		/*params->cornerRefinementMethod = cv::aruco::CORNER_REFINE_SUBPIX;
		params->cornerRefinementWinSize = 10;
		params->cornerRefinementMaxIterations = 1000;
		params->cornerRefinementMinAccuracy = 0.5;
		params->adaptiveThreshWinSizeMin = 3;
		params->adaptiveThreshWinSizeMax = 30;
		params->adaptiveThreshWinSizeStep = 2;
		*/

//		params->cornerRefinementMethod = cv::aruco::CORNER_REFINE_SUBPIX;
		params->cornerRefinementWinSize = 20;
		params->cornerRefinementMaxIterations = 200;
		params->cornerRefinementMinAccuracy = 0.5;
		params->adaptiveThreshConstant = 20;
		params->adaptiveThreshWinSizeMin = 3;
		params->adaptiveThreshWinSizeMax = 50;
		params->adaptiveThreshWinSizeStep = 2;
		params->maxErroneousBitsInBorderRate = 0.01;


		
		cv::Ptr<aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(_arucoDatabase->getValue());
		std::vector<int> ids;
		std::vector<std::vector<cv::Point2f> > corners;
		std::vector< std::vector<Point2f> > rejectedCandidates; 

		cv::aruco::detectMarkers(inputImage, dictionary, corners, ids, params, rejectedCandidates);
		ER_LOG_DEBUG("MarkerDetectionArUco: " );
		ER_LOG_DEBUG("\tImage Id: " << _executionCount );
		ER_LOG_DEBUG("\tRejected Candidates Count: " << rejectedCandidates.size() );

		cv::Mat outputImage = inputImage;
		cv::aruco::drawDetectedMarkers(outputImage, corners, ids);

		std::pair<Mat, Mat> cameraMatrixAndDistCoeffs = camera->getCameraCalibration();

		//ER_LOG_DEBUG("Camera Matrix = " << cameraMatrixAndDistCoeffs.first << " distortion = " << cameraMatrixAndDistCoeffs.second );
		double markerSize = _markerSize->getValue();

		cv::aruco::estimatePoseSingleMarkers(corners, (float)markerSize, cameraMatrixAndDistCoeffs.first, cameraMatrixAndDistCoeffs.second, _rvecs, _tvecs);
		//ER_LOG_DEBUG("rvecs " << rvecs );
//		ER_LOG_DEBUG("tvecs " << tvecs );

		_detected = false;

		if (ids.size() == 0) {
			if(saveImages){
				std::stringstream imageoutfile;
				imageoutfile << er_common::System::getInstance()->getMarkerImageFolder() << "/ArucoImages/" << _executionCount  << "_ArucoDetectionNoMarker"<< ".png";
				imwrite(imageoutfile.str(), inputImage);
			}
			_detectionImage = inputImage;
			ER_LOG_ERROR("MarkerDetectionArUco: No marker identified" );
			RW_THROW("MarkerDetectionArUco: No marker identified");
		} 


		//TODO: Remove limitation of only having a single marker in the view. Instead do a better job with searching for the right marker.
		_arucoIdIndx = -1;
		if (ids.size() > 0) {
			if(saveImages){
				std::stringstream imageoutfile;
				imageoutfile << er_common::System::getInstance()->getMarkerImageFolder() << "/ArucoImages/" << _executionCount << "_ArucoDetectionMultipleMarkers" << ".png";
				imwrite(imageoutfile.str(), inputImage);
			}
			_detectionImage = inputImage;

			for(int i = 0; i < ids.size(); i++){
				if(ids[i] == _arucoMarkerId->getValue()){
					_arucoIdIndx = i;
					ER_LOG_DEBUG("Found right id: " << ids[i] << " at position: " << i);
					break;
				}
			}

			if(_arucoIdIndx == -1){
				if(saveImages){
					std::stringstream imageoutfile;
					imageoutfile << er_common::System::getInstance()->getMarkerImageFolder() << "/ArucoImages/" << _executionCount << "_ArucoDetectionWrongMarkerId" << ".png";
					imwrite(imageoutfile.str(), inputImage);
				}
				_detectionImage = inputImage;
				ER_LOG_ERROR("MarkerDetectionArUco: Multiple markers identified, but expected "<< _arucoMarkerId->getValue() );
				RW_THROW("MarkerDetectionArUco: Multiple markers identified, but expected " <<  _arucoMarkerId->getValue());
			}
		}


		_detected = true;

		Transform3D<> cam2marker;
		cam2marker.R() = EAA<>(_rvecs.at<double>(_arucoIdIndx, 0), _rvecs.at<double>(_arucoIdIndx, 1), _rvecs.at<double>(_arucoIdIndx, 2)).toRotation3D();
		cam2marker.P() = Vector3D<>(_tvecs.at<double>(_arucoIdIndx, 0), _tvecs.at<double>(_arucoIdIndx, 1), _tvecs.at<double>(_arucoIdIndx, 2));
		cv::aruco::drawAxis(inputImage, cameraMatrixAndDistCoeffs.first, cameraMatrixAndDistCoeffs.second, _rvecs.row(_arucoIdIndx), _tvecs.row(_arucoIdIndx), 0.1f);

		if(saveImages){
			std::stringstream imageoutfile;
			imageoutfile << er_common::System::getInstance()->getMarkerImageFolder() << "/ArucoImages/" << _executionCount  << "_ArucoDetection"<< ".png";
			imwrite(imageoutfile.str(), inputImage);
		}
		_detectionImage = inputImage;

		std::string knowledgeBaseId = _knowledgeBaseIdentifier->getValue();
		execManager->getSystem()->getKnowledgeBase()->addForce(knowledgeBaseId, "Camera to Marker transform", cam2marker);
		ER_LOG_DEBUG("\tcam2marker: XYZ = " << cam2marker.P()<<" RPY = "<<RPY<>(cam2marker.R()) );

	}
	catch (const std::exception& exp) {
		ER_LOG_ERROR("MarkerDetectionArUco: Exception while trying to identify marker: " <<exp.what());
		throw exp;
	}
	catch (...) {
		ER_LOG_ERROR("MarkerDetectionArUco: Unknown Exception while trying to identify marker.");
		RW_THROW("Caught unknown exception while doing Aruco Marker Detection ");
	}

	return true;
}

bool MarkerDetectionArUco::isDetected(){
	return _detected;
}

cv::Mat MarkerDetectionArUco::getRvecs(){
	return _rvecs.row(_arucoIdIndx);
}

cv::Mat MarkerDetectionArUco::getTvecs(){
	return _tvecs.row(_arucoIdIndx);
}

/* Methods from the serializable interface */
void MarkerDetectionArUco::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    iarchive.readEnterScope("MarkerDetectionArUco");

    MarkerDetection::read(iarchive, id);
	getSettings().read(iarchive, "Settings");
	iarchive.readLeaveScope("MarkerDetectionArUco");
}

void MarkerDetectionArUco::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("MarkerDetectionArUco");
    MarkerDetection::write(oarchive, id);

	getSettings().write(oarchive, "Settings");
	

	oarchive.writeLeaveScope("MarkerDetectionArUco");
}
