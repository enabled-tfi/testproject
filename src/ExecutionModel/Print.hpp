/* */
#ifndef EXM_PRINT_HPP
#define EXM_PRINT_HPP

#include "Instruction.hpp"
#include "ExecutionManager.hpp"

#include <ostream>
#include <string>

namespace exm {

/** 
 * @brief Waits a specified amount of time before returning from the execute method.
 */
class Print: public Instruction
{
public:
	/** @brief Smart pointer to the Print object*/
	typedef rw::common::Ptr<Print> Ptr;

	/**
	 * @brief Default Constructor
	 */
	Print();

	/**
	 * @brief Construct Print instruction for outputting \b msg
	 * @param msg [in] The message to output
	 * @param ostr [in] The out stream to print to
	 */
	Print(const std::string& msg, std::ostream& out);

	bool execute(ExecutionManager::Ptr execManager);

    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;



protected:
    virtual void initializeSettings();

private:
	er_common::Setting<std::string>::Ptr _msg;
	std::ostream* _outstream;

};

} //end namespace

#endif //#ifndef EXM_WAIT_HPP
