#include "MoveT3D.hpp"

#include "MovePTP.hpp"

#include <rw/common/TimerUtil.hpp>
#include <rw/pathplanning/PlannerUtil.hpp>
#include <Common/EntityRegister.hpp>
#include <Common/Utils.hpp>
#include <Common/InputArchive.hpp>
#include <Common/OutputArchive.hpp>
#include <boost/foreach.hpp>

using namespace exm;
using namespace er_common;
using namespace rw::common;
using namespace rw::math;
using namespace rw::kinematics;
using namespace rw::trajectory;
using namespace rw::pathplanning;
using namespace rw::proximity;


MoveT3D::MoveT3D():
	Instruction("MoveT3D", "")
{
    initializeSettings();
    setIsInitialized(false);
}


MoveT3D::MoveT3D(const rw::math::Transform3D<>& refTtarget, const std::string& reference, const std::string& tcp, const std::string& deviceIdentifier):
	Instruction("MoveT3D", "")
{
	initializeSettings();
	_target->getValue().setRefTtarget(refTtarget);
	//_refTtarget->setValue(refTtarget);
	if (EntityRegister::has(deviceIdentifier)) {
		_deviceInterface->getValue().set(EntityRegister::get<hwl::Manipulator>(deviceIdentifier));
	}
	else {
		_deviceInterface->getValue().set(deviceIdentifier);
	}
	_target->getValue().setReference(reference);
	_target->getValue().setToolFrame(tcp);
	//_referenceName = reference;
	setIsInitialized(true);
}



MoveT3D::MoveT3D(const rw::math::Transform3D<>& refTtarget, Frame* reference, Frame* tcp, hwl::Manipulator::Ptr deviceInterface):
	Instruction("MoveT3D", "")
{
	initializeSettings();
	_target->getValue().setRefTtarget(refTtarget);
	_target->getValue().setReference(reference);
	_target->getValue().setToolFrame(tcp);
	//_refTtarget->setValue(refTtarget);
	_deviceInterface->getValue().set(deviceInterface);
	//_reference->setValue(reference);
	setIsInitialized(true);
}



bool MoveT3D::execute(ExecutionManager::Ptr execManager)
{
	RW_DEBUG("MoveT3D::Execute " << _target->getValue().refTtarget() << " " << _target->getValue().getReferenceFrameName());
	ER_LOG_DEBUG("MoveT3D::Execute");
    std::cout<<"MoveT3D::Execute"<<std::endl;
	//Call update to ensure that states from all hardware entities has been read. 
	execManager->getSystem()->update();
	State state = execManager->getSystem()->getState();
	System::Ptr system = execManager->getSystem();
	/*if (_reference->getValue() == NULL)
	{
		_reference->setValue(execManager->getSystem()->getWorkCell()->findFrame(_referenceName));
	}*/
	Transform3D<> manipulatorTref = Kinematics::frameTframe(system->getManipulator()->getBase(), _target->getValue().getReference(), state);
	ER_LOG_DEBUG("Manipulator T Ref = " << manipulatorTref.P()<<" "<<RPY<>(manipulatorTref.R()) );
	//Transform3D<> worldTmanipulator = Kinematics::frameTframe(mm->getReferenceFrame().get(), mm->getManipulator()->getBase(), state);
	Transform3D<> refTtcp = _target->getValue().refTtarget();
	Transform3D<> manipulatorTtarget = manipulatorTref*refTtcp;

	//Find all IK solutions 

	ER_LOG_DEBUG("Tool Frame "<<_target->getValue().getToolFrame());
	Transform3D<> endTtcp = Kinematics::frameTframe(system->getManipulator()->getEnd(), _target->getValue().getToolFrame(), state);

	ER_LOG_DEBUG("Manipulator T Target = " << manipulatorTtarget.P()<<" "<<RPY<>(manipulatorTtarget.R()) );
	ER_LOG_DEBUG("EndTtcp = " << endTtcp );
	ER_LOG_DEBUG(" Tries to solve IK for " << manipulatorTtarget*inverse(endTtcp) );
	std::vector<Q> qsol = system->getManipulatorIKSolver()->solve(manipulatorTtarget*inverse(endTtcp), state);
	if (qsol.size() == 0) {
		RW_THROW("No IK Solution found");
	}
	ER_LOG_DEBUG("Qsol = " << qsol.size() );
	//Select the most promising of the IK solutions
	Q qcurrent = system->getManipulator()->getQ(state);
	std::cout<<"QCurrent "<<qcurrent<<std::endl;
	//TODO: Find another place to do this as it is time consuming:
	Q qweights = PlannerUtil::estimateMotionWeights(*system->getManipulator(), NULL, state, PlannerUtil::AVERAGE, 100);
	std::cout<<"Motion Weights "<<qweights<<std::endl;
	std::queue<Q> queue = Utils::sortQ(qsol, qcurrent, MetricFactory::makeWeightedEuclidean<Q>(qweights));

	std::vector<Q> qtarget;
	if (_useCollisionChecking->getValue()) {
        CollisionDetector::Ptr collisionDetector = system->getCollisionDetector();
        while (queue.empty() == false) {
            const Q& q = queue.front();
            system->getManipulator()->setQ(q, state);
            std::cout << "IK Solution " << q << std::endl;
            if (collisionDetector->inCollision(state) == false) {
                std::cout<<"Collision Free"<<std::endl;
                qtarget.push_back(q);
            } else {
                std::cout<<"In Collision"<<std::endl;
            }
            queue.pop();
        }
        if (qtarget.size() == 0) {
            RW_THROW("Unable to find a collision free configuration");
        }
	} else {
	    qtarget.push_back(queue.front());
	}
	std::cout<<"Ready to move to configuration "<<qtarget.front()<<std::endl;
    exm::MovePTP::Ptr movePTP = ownedPtr(new MovePTP(qtarget.front(), _deviceInterface->getValue().get()));
    movePTP->execute(execManager);


	return true;

}

void MoveT3D::initializeSettings()
{
	_target = getSettings().add<Target>("Target", "Target with reference", Target());
	_useCollisionChecking = getSettings().add<bool>("UseCollisionChecking", "Use collision checking before moving robot", true);
	_deviceInterface = getSettings().add<EntityReference<hwl::Manipulator> >("Manipulator", "Interface of the device to move", EntityReference<hwl::Manipulator>("Manipulator"));
}

/* Methods from the serializable interface */
void MoveT3D::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    iarchive.readEnterScope("MoveT3D");
    Instruction::read(iarchive, id);
	_target->getValue().read(iarchive, "Target");
	_useCollisionChecking->setValue(iarchive.read<bool>("UseCollisionChecking"));
    std::string deviceIdentifier = iarchive.read<std::string>("Manipulator");
    _deviceInterface->getValue().set(deviceIdentifier);
    iarchive.readLeaveScope("MoveT3D");
}

void MoveT3D::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("MoveT3D");
    Instruction::write(oarchive, id);
	_target->getValue().write(oarchive, "Target");
	oarchive.write(_useCollisionChecking->getValue(), "UseCollisionChecking");
	oarchive.write(_deviceInterface->getValue().getName(), "Manipulator");

	oarchive.writeLeaveScope("MoveT3D");
}

