/* */
#ifndef EXM_DRIVETO_HPP
#define EXM_DRIVETO_HPP

#include "Instruction.hpp"
#include <Common/EntityReference.hpp>
#include "../HardwareLayer/MobileDevice.hpp"
#include <rw/math.hpp>

#include <vector>
namespace exm {
	
/**
 * @brief Instruction for moving robot with a linear path in tool space.
 */
class DriveTo: public Instruction
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<DriveTo> Ptr;

	DriveTo();

	/**
	 * @brief Constructs DriveTo action moving to \btarget using \bdeviceInterface
	 * @param target [in] Target to which to move.
	 * @param deviceInterface [in] Interface to the device to move
	 */
	DriveTo(const rw::math::Transform2D<>& target, bool isBlocking, hwl::MobileDevice::Ptr deviceInterface);

	DriveTo(const rw::math::Transform2D<>& target, bool isBlocking, const std::string& deviceIdentifier); 

    bool execute(ExecutionManager::Ptr execManager);


    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;



protected:
   void initializeSettings();

private:
	er_common::Setting<rw::math::Transform2D<> >::Ptr _target;
	//er_common::Setting<hwl::MobileDevice::Ptr>::Ptr _deviceInterface;
	er_common::Setting < er_common::EntityReference<hwl::MobileDevice> >::Ptr _deviceInterface;
	bool _isBlocking;
};

} //end namespace

#endif //#ifndef EXM_DRIVETO_HPP
