#include "MoveLinRel.hpp"

#include "../Common/EntityRegister.hpp"

#include <Common/InputArchive.hpp>
#include <Common/OutputArchive.hpp>

#include <boost/foreach.hpp>

using namespace exm;
using namespace er_common;
using namespace rw::math;
using namespace rw::kinematics;
using namespace rw::models;


namespace {
	std::string getDes(const Transform3D<>& target) {
		std::stringstream sstr;
		sstr<<"Target: "<<target;
		return sstr.str();
	}

}

MoveLinRel::MoveLinRel():
	Instruction("MoveLinRel", "")
{
    initializeSettings();
    setIsInitialized(false);
}


MoveLinRel::MoveLinRel(const Transform3D<>& target, hwl::Manipulator::Ptr deviceInterface):
	Instruction("MoveLinRel", getDes(target))
{
    initializeSettings();
    _target->setValue(target);
    _deviceInterface->setValue(deviceInterface);
    setIsInitialized(true);
}

MoveLinRel::MoveLinRel(const rw::math::Transform3D<>& target, const std::string& deviceIdentifier):
	Instruction("MoveLinRel", getDes(target))
{
    initializeSettings();
    _target->setValue(target);
	if (EntityRegister::has(deviceIdentifier)) {
		_deviceInterface->getValue().set(EntityRegister::get<hwl::Manipulator>(deviceIdentifier));
	}
	else {
		_deviceInterface->getValue().set(deviceIdentifier);
	}

    setIsInitialized(true);
}

void MoveLinRel::initializeSettings()
{
    _target = getSettings().add<Transform3D<> >("Target","Target as Transform3D", Transform3D<>());
 //   _deviceInterface = getSettings().add<hwl::Manipulator::Ptr>("Manipulator", "Interface to the serial device", NULL);
	_deviceInterface = getSettings().add<EntityReference<hwl::Manipulator> >("Manipulator", "Interface of the device to move", EntityReference<hwl::Manipulator>("Manipulator"));
	_deviceInterface->setInputType(InputType::Numeric);
}

bool MoveLinRel::execute(ExecutionManager::Ptr execManager)
{
	System::Ptr system = execManager->getSystem();
	SerialDevice::Ptr manipulator = system->getManipulator();

	system->update();
	State state = system->getState();
	Transform3D<> baseTcurrent = manipulator->baseTframe(system->getToolTCP().get(), state);
	Transform3D<> baseTtarget = baseTcurrent*_target->getValue();
	_deviceInterface->getValue().get()->lin(baseTtarget);
	return true;
}
		

/* Methods from the serializable interface */
void MoveLinRel::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    ER_LOG_DEBUG("MoveLinRel::read");
    iarchive.readEnterScope("MoveLinRel");
    Instruction::read(iarchive, id);

    Transform3D<> t3d = iarchive.read<Transform3D<> >("Target");
    _target->setValue(t3d);

    std::string deviceIdentifier = iarchive.read<std::string>("Manipulator");
    ER_LOG_DEBUG("Device Identifier = "<<deviceIdentifier);
    _deviceInterface->getValue().set(deviceIdentifier);

    iarchive.readLeaveScope("MoveLinRel");
}

void MoveLinRel::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("MoveLinRel");

	Instruction::write(oarchive, id);
    oarchive.write(_target->getValue(), "Target");
    oarchive.write(_deviceInterface->getValue().getName(), "Manipulator");
    oarchive.writeLeaveScope("MoveLinRel");
}

