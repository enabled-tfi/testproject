/* */
#ifndef EXM_MARKERDETECTION_HPP
#define EXM_MARKERDETECTION_HPP

#include "Instruction.hpp"
#include "ExecutionManager.hpp"
#include <Common/EntityReference.hpp>
#include <HardwareLayer/Camera.hpp>

#include <ostream>
#include <string>

namespace exm {

/** 
 * @brief Interface for MarkerDetection instructions
 */
class MarkerDetection: public Instruction
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<MarkerDetection> Ptr;

    MarkerDetection(const std::string& name, const std::string& description) :
        Instruction(name, description)
    {
        
    }

    
    virtual const cv::Mat& getDetectionImage() const = 0;

    virtual bool isDetected() = 0;

    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id)
    {
        Instruction::read(iarchive, "");
    }

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const 
    {
        Instruction::write(oarchive, "");
    }



private:

};

} //end namespace

#endif //#ifndef EXM_MARKERDETECTIONARUCO_HPP
