    var websocket;

    
    if (typeof MozWebSocket != "undefined") {
        websocket = new MozWebSocket('ws://127.0.0.1:7681',
                   "manipulator_control_protocol");
    } else {
        websocket = new WebSocket('ws://127.0.0.1:7681',
                   "manipulator_control_protocol");
    }

    try {
            websocket.onopen = function () {
            console.log("Connection established");
                $('h1').css('color', 'green');
            $('#stat').text('Connection established').css('color', 'green');
            };

            websocket.onerror = function (error) {
            console.log("Error received from WebSocket Server: " + error);
                $('h1').css('color', 'red');
            };

            websocket.onmessage = function got_packet(msg) {
            	var programsElement = document.getElementById("Programs");
            	var btn = document.createElement("button");
            	btn.type = "button";
            	btn.onclick = function() { runprogram(msg.data) };
            	
            	var t = document.createTextNode(msg.data); 
            	btn.appendChild(t);           	            	
            	document.body.appendChild(btn);
            	var br1 = document.createElement("br");
            	document.body.appendChild(br1);
            	var br2 = document.createElement("br");
            	document.body.appendChild(br2);
           	
            //document.getElementById("Programs").textContent = msg.data + "\n";
            console.log("onmessage called"+msg.data);
                //console.log(msg.data);
                //$('#msg').append($('<p>', { text: msg.data }));
            };
            websocket.onclose = function (){
            console.log("Connection closed");
            $('h1').css('color', 'red');
            $('#stat').text('Connection closed').css('color', 'red');
        };

            
    }catch(exception){
        alert('<p>Error' + exception); 
    }

    function reset() {
        console.log("reset sent");
        websocket.send("reset\n");
    }

    function sendmsg(msg){
        websocket.send(msg);
    }
    
    function getprograms() {
    	msg = "GetPrograms";    	
    	sendmsg(msg);
    }
    
    function runprogram(name) {
    	msg = "RunProgram "+name;
    	console.log(msg);
    	sendmsg(msg);
    }
    function hello() {
    	alert("Hello");
    }
