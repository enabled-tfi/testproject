    var websocket;

    
    if (typeof MozWebSocket != "undefined") {
        websocket = new MozWebSocket('ws://127.0.0.1:7681',
                   "manipulator_control_protocol");
    } else {
        websocket = new WebSocket('ws://127.0.0.1:7681',
                   "manipulator_control_protocol");
    }

    try {
            websocket.onopen = function () {
            console.log("Connection established");
                $('h1').css('color', 'green');
            $('#stat').text('Connection established').css('color', 'green');
            };

            websocket.onerror = function (error) {
            console.log("Error received from WebSocket Server: " + error);
                $('h1').css('color', 'red');
            };

            websocket.onmessage = function got_packet(msg) {
            document.getElementById("number").textContent = msg.data + "\n";
            //console.log("onmessage called");
                //console.log(msg.data);
                //$('#msg').append($('<p>', { text: msg.data }));
            };
            websocket.onclose = function (){
            console.log("Connection closed");
            $('h1').css('color', 'red');
            $('#stat').text('Connection closed').css('color', 'red');
        };

            
    }catch(exception){
        alert('<p>Error' + exception); 
    }

    function reset() {
        console.log("reset sent");
        websocket.send("reset\n");
    }

    function sendmsg(msg){
        websocket.send(msg);
    }
    
    function moveDq(joint, direction) {
    	msg = "MoveDeltaQ "+joint+" "+direction;
    	//alert(msg);
    	sendmsg(msg);
    }
