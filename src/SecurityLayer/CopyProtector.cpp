#include <boost/algorithm/string.hpp>
#include <src/SecurityLayer/CopyProtector.hpp>
#include <src/SecurityLayer/HWAddressProvider.hpp>

#include <fstream>
#include <string.h>
#include <sstream>

namespace er_security {

CopyProtector::CopyProtector():
	_unlocked(false)
{
}

CopyProtector::~CopyProtector() {
}

void CopyProtector::generateSecurityFile(std::string location, std::string adapter, std::string password){
	std::string macAddr = er_security::HWAddressProvider::getMacAddress(adapter);
//	ER_LOG_DEBUG(macAddr );
	er_security::CopyProtector copyProt;

	std::string dataString = adapter+"\n"+macAddr;

	std::string encryptedStr = copyProt.encrypt(dataString, password);

	std::string path = location;
	if(!boost::algorithm::ends_with(location, ".er")){
		path = location + "/license.er";
	}

	std::ofstream ofs (path, std::ofstream::out);

	ofs << encryptedStr;
	ofs.close();
}


std::string CopyProtector::encrypt(std::string data, std::string key){
    // Make sure the key is at least as long as the message
    std::string tmp(key);
    while (key.size() < data.size())
        key += tmp;

    // Encryption by simple XOR
    for (std::string::size_type i = 0; i < data.size(); ++i)
    	data[i] ^= key[i];
    return data;
}

std::string CopyProtector::decrypt(std::string data, std::string key){
		// To decrypt, simple XOR again
    return encrypt(data, key);
}

bool CopyProtector::unlock(std::string securityFile, std::string password){
	std::stringstream sstr;

	std::ifstream t(securityFile);
	sstr << t.rdbuf();

	std::string decryptedFile = decrypt(sstr.str(), password);
	std::string ethernetAdapter(decryptedFile.begin(), std::find(decryptedFile.begin(), decryptedFile.end(), '\n'));
	std::string macAddr = er_security::HWAddressProvider::getMacAddress(ethernetAdapter);

	if(macAddr == ""){
		_unlocked = false;
		return _unlocked;
	}

    std::string decryptedData(std::find(decryptedFile.begin(), decryptedFile.end(), '\n')+1, decryptedFile.end());

	if(decryptedData.length() != macAddr.length() || decryptedData.length() == 0){
		_unlocked = false;
		return _unlocked;
	}

	if(strcmp(decryptedData.c_str(), macAddr.c_str()) == 0)
		_unlocked = true;
	else
		_unlocked = false;

	return _unlocked;
}

bool CopyProtector::isLegal(){
	return _unlocked;
}

} /* namespace er_security */
