#
# Source files
#

set(TargetName er_security)


set(SrcFiles 
	HWAddressProvider.cpp
	CopyProtector.cpp
)

#
# Header files (optional)
#
set(HeaderFiles 
	HWAddressProvider.hpp
	CopyProtector.hpp
)

#
# Standard cpp files to compile into static library

add_library(${TargetName} ${SrcFiles} ${HeaderFiles})
target_link_libraries(${TargetName})


add_executable(GenerateSecurityFile GenerateSecurityFile.cpp)
target_link_libraries(GenerateSecurityFile ${TargetName} ${ROBWORK_LIBRARIES} er_common )
