#include <stdio.h>
#include <ifaddrs.h>
#include <netpacket/packet.h>
#include <src/SecurityLayer/HWAddressProvider.hpp>
#include <cstring>
#include <sstream>


namespace er_security {

HWAddressProvider::HWAddressProvider() {
	// TODO Auto-generated constructor stub

}

HWAddressProvider::~HWAddressProvider() {
	// TODO Auto-generated destructor stub
}

std::string HWAddressProvider::getMacAddress(std::string adapter){
    struct ifaddrs *ifaddr=NULL;
    struct ifaddrs *ifa = NULL;
    int i = 0;
    std::stringstream sstr;

    if (getifaddrs(&ifaddr) == -1)
    {
         perror("getifaddrs");
    }
    else
    {
         for ( ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
         {
             if ( (ifa->ifa_addr) && (ifa->ifa_addr->sa_family == AF_PACKET) )
             {
                  struct sockaddr_ll *s = (struct sockaddr_ll*)ifa->ifa_addr;
                  if (std::strcmp(ifa->ifa_name, adapter.c_str()) == 0) {
                      char strs[ 100 + 1 ];
                      for (i=0; i <s->sll_halen; i++)
                      {
                          sprintf(strs, "%02x%c", (s->sll_addr[i]), (i+1!=s->sll_halen)?':':'\0');
                          sstr << strs;
                      }
                  }
             }
         }
         freeifaddrs(ifaddr);
    }
    return sstr.str();
}


std::vector<std::string> HWAddressProvider::getNetworkInterfaces(){
    struct ifaddrs *ifaddr=NULL;
    struct ifaddrs *ifa = NULL;
    std::vector<std::string> netInterfaces;
    int i = 0;
    std::stringstream sstr;

    if (getifaddrs(&ifaddr) == -1)
    {
         perror("getifaddrs");
    }
    else
    {
         for ( ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
         {
             if ( (ifa->ifa_addr) && (ifa->ifa_addr->sa_family == AF_PACKET) )
             {
                  struct sockaddr_ll *s = (struct sockaddr_ll*)ifa->ifa_addr;
//				  ER_LOG_DEBUG(ifa->ifa_name );
				  netInterfaces.push_back(ifa->ifa_name);
             }
         }
         freeifaddrs(ifaddr);
    }

    return netInterfaces;
}


} /* namespace er_security */
