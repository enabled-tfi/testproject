#ifndef SRC_SECURITYLAYER_COPYPROTECTOR_HPP_
#define SRC_SECURITYLAYER_COPYPROTECTOR_HPP_

#include <iostream>
#include <rw/RobWork.hpp>

namespace er_security {

/**
 * Class for ensuring security of software.
 */
class CopyProtector {
public:
    /** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<CopyProtector> Ptr;

	/**
	 * @brief Constructs CopyProtection object
	 */
	CopyProtector();

	/**
	 * Destructor
	 */
	virtual ~CopyProtector();

	/**
	 * @brief Generates license file based on HW addresses
	 * @param password [in] password to use for encryption
	 * @param adapter [in] which adapter to generate security file for
	 * @param location [in] where to place the license file
	 */
	void generateSecurityFile(std::string location, std::string adapter, std::string password);

	/**
	 * @brief Method for unlocking the copy protection with a given license file
	 *
	 * @param securityFile [in] file to use for unlocking
  	 * @param adapter [in] which adapter to generate security file for
	 * @param password 	   [in] password used for unlocking the copy protection
	 * @return whether or not the copy protection was succesfully unlocked
	 */
	bool unlock(std::string securityFile, std::string password);

	/**
	 * @brief Gets whether copy protection has been unlocked
	 * @return Whether copy protection is unlocked
	 */
	bool isLegal();

protected:
	/**
	 * @brief Simple encryption of data with given password
	 *
	 * @param data [in] data to encrypt
	 * @param key [in] password to encrypt data
	 *
	 * @return Encrypted data as string
	 */
		// Simple XOR en- and decryption. TODO: Perhaps find an algorithm with a bit more security
	std::string encrypt(std::string data, std::string key);

	/**
	 * @brief Simple decryption of data with given password
	 *
	 * @param data [in] data to decrypt
	 * @param key [in] password to decrypt data
	 *
	 * @return Decrypted data as string
	 */
	std::string decrypt(std::string data, std::string key);

	bool _unlocked;
};

} /* namespace er_security */

#endif /* SRC_SECURITYLAYER_COPYPROTECTOR_HPP_ */
