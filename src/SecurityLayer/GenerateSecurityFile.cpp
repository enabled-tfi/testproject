#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <SecurityLayer/CopyProtector.hpp>
#include <SecurityLayer/HWAddressProvider.hpp>

#include <Common/Logging.hpp>

#include <fstream>

int main (int argc, const char * argv[])
{
	std::vector<std::string> interfaces = er_security::HWAddressProvider::getNetworkInterfaces();
	std::string ethInterface;
	for(int i = 0; i < interfaces.size(); i++){
		if (boost::starts_with(interfaces[i], "enp"))
			ethInterface = interfaces[i];
		else if (boost::starts_with(interfaces[i], "eno"))
			ethInterface = interfaces[i];
		else if (boost::starts_with(interfaces[i], "ens"))
			ethInterface = interfaces[i];
		else if (boost::starts_with(interfaces[i], "eth"))
			ethInterface = interfaces[i];

	}

	if(ethInterface == "")
		RW_THROW("Failed to find ethernet interface");

	er_security::CopyProtector copyProt;
	ER_LOG_DEBUG("generating security file with password: " << argv[1] );
	copyProt.generateSecurityFile(".", ethInterface, argv[1]);
}
