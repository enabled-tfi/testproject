#ifndef SRC_SECURITYLAYER_HWADDRESSPROVIDER_HPP_
#define SRC_SECURITYLAYER_HWADDRESSPROVIDER_HPP_

#include <iostream>
#include <vector>

namespace er_security {

class HWAddressProvider {
public:
	HWAddressProvider();
	virtual ~HWAddressProvider();

	/**
	 * @brief Method for getting Mac address of a given network adapter
	 * @param adapter [in] for which adapter to get Mac address
	 * @return Mac address of adapter
	 */
	static std::string getMacAddress(std::string adapter);

	/**
	 * @brief Get all available network interfaces
	 * @return	vector of available network interfaces
	 */
	static std::vector<std::string> getNetworkInterfaces();
};

} /* namespace er_security */

#endif /* SRC_SECURITYLAYER_HWADDRESSPROVIDER_HPP_ */
