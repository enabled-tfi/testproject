#include "PickOrPlace.hpp"

#include <Common/EntityRegister.hpp>
#include <ExecutionModel/MoveT3DPlanned.hpp>
#include <ExecutionModel/MoveLin.hpp>
#include <HardwareLayer/Manipulator.hpp>

#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>

using namespace er_pgi;
using namespace pgm;
using namespace exm;
using namespace hwl;
using namespace er_common;
using rw::common::Ptr;
using rw::common::ownedPtr;
PickOrPlace::PickOrPlace():
	Instruction("PickOrPlace", "Pick or place action")
{
	initializeSettings();
}

PickOrPlace::PickOrPlace(const std::string& name, const std::string& description) :
	Instruction(name, description)
{
	initializeSettings();
}

void PickOrPlace::initializeSettings()
{
	_approach = getSettings().add("Approach", "Position from which to approach target", Target());
	_target = getSettings().add("Target", "Target where the object is either picked or placed", Target());
	_depart = getSettings().add("Depart", "Departure position where the robot moved after the target", Target());
	setIsInitialized(true);
}

bool PickOrPlace::execute(exm::ExecutionManager::Ptr executionManager)
{
	Manipulator::Ptr manipulator = EntityRegister::get<Manipulator>("Manipulator");
	if (manipulator.isNull()) {
		RW_THROW("No Manipulator named 'Manipulator' found");
		return false;
	}


	const Target& approach = _approach->getValue();
	const Target& target = _target->getValue();
	const Target& depart = _depart->getValue();
	MoveT3DPlanned::Ptr moveApproach = ownedPtr(new MoveT3DPlanned(approach.refTtarget(), approach.getReference(), manipulator));
	MoveLin::Ptr moveTarget = ownedPtr(new MoveLin(target.refTtarget(), target.getReference(), target.getToolFrame(), manipulator));
	MoveLin::Ptr moveDepart = ownedPtr(new MoveLin(depart.refTtarget(), depart.getReference(), target.getToolFrame(),manipulator));

	executionManager->executeBlocking(moveApproach);
	executionManager->executeBlocking(moveTarget);
	executionManager->executeBlocking(moveDepart);
	return true;
}


void PickOrPlace::read(er_serialization::InputArchive& iarchive, const std::string& id)
{
	iarchive.readEnterScope("PickOrPlace");
	Instruction::read(iarchive, id);
	_approach->getValue().read(iarchive, "Approach");
	_target->getValue().read(iarchive, "Target");
	_depart->getValue().read(iarchive, "Depart");
	iarchive.readLeaveScope("PickOrPlace");
}
 
void PickOrPlace::write(er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	ER_LOG_DEBUG("PickOrPlace::write " );
	oarchive.writeEnterScope("PickOrPlace");
	Instruction::write(oarchive, id);
	_approach->getValue().write(oarchive, "Approach");
	_target->getValue().write(oarchive, "Target");
	_depart->getValue().write(oarchive, "Depart");
	oarchive.writeLeaveScope("PickOrPlace");
}
