#include "LocalizeArmWithEyeInHand.hpp"

#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>
#include <ExecutionModel/Sequence.hpp>
#include <ExecutionModel/MoveQPlanned.hpp>
#include <ExecutionModel/MarkerDetectionArUco.hpp>

#include <rw/models/WorkCell.hpp>

//using namespace pgm;
using namespace er_common;
using namespace exm;
using namespace er_pgi;
using rw::common::Ptr;
using rw::common::ownedPtr;
using namespace rw::math;
using namespace rw::kinematics;
using namespace rw::models;

LocalizeArmWithEyeInHand::LocalizeArmWithEyeInHand():
	Instruction("Localize Reference Frame", "Localize reference frame using tool mounted sensor")
{
	initializeSettings();
}

LocalizeArmWithEyeInHand::LocalizeArmWithEyeInHand(const std::string& name, const std::string& description, System::Ptr system) :
	Instruction(name, description),
	_system(system)
{
	initializeSettings();
}

void LocalizeArmWithEyeInHand::initializeSettings()
{
	Setting<Q>::Ptr qsetting = getSettings().add("QAcquire", "Configuration of the arm used for recording the position and orientation of the reference frame.", Q::zero(6));
	qsetting->setInputType(InputType::Teachin);
//
	if (System::isInitialized()) {
		ER_LOG_DEBUG("Set Range " );
		qsetting->setRange(System::getInstance()->getManipulator()->getBounds());
		ER_LOG_DEBUG("Range setted" );
	}
	//qsetting->setRange(_system->getMobileManipulator()->getManipulator()->getBounds());

	
	getSettings().add("SensorFrameName", "Name of the sensor frame.", std::string("ToolCamera"));
	getSettings().add("ManipulatorName", "Name of the robot arm to use.", std::string("Manipulator"));

	setIsInitialized(true);
}


bool LocalizeArmWithEyeInHand::execute(exm::ExecutionManager::Ptr executionManager)
{
	System::Ptr system = executionManager->getSystem();
	Sequence::Ptr sequence = ownedPtr(new Sequence());
	
	//Goto Arm Configuration
	Q q_acquire = getSettings().get<Q>("QAcquire");
	const std::string manipulatorName = getSettings().get<std::string>("ManipulatorName");
	MoveQPlanned::Ptr movePlanned = ownedPtr(new MoveQPlanned(q_acquire, manipulatorName));
	sequence->addInstruction(movePlanned);

	//Call FindMarkers instruction
	MarkerDetectionArUco::Ptr markerDetection = ownedPtr(new MarkerDetectionArUco());
	sequence->addInstruction(markerDetection);


	executionManager->executeBlocking(sequence);
	ER_LOG_DEBUG("ExecutionManager Message: " << executionManager->getErrorMessage() );
	//Compute ref2base transform
	if (system->getKnowledgeBase()->has("ArUcoMarkerPose") == false)
	{
		RW_THROW("No ArUcoMarkerPose present in knowledge base");
	}
	Transform3D<> camTmarker = system->getKnowledgeBase()->findProperty<Transform3D<> >("ArUcoMarkerPose")->getValue();
	ER_LOG_DEBUG("Camera to Marker " << camTmarker );
	//Compute baseTcam	
	State state = system->getState();
	Frame* camFrame = system->getWorkCell()->findFrame(getSettings().findSetting<std::string>("SensorFrameName")->getValue());
	ER_LOG_DEBUG("CamFrame = " << camFrame );
	Transform3D<> worldTcam = Kinematics::worldTframe(camFrame, state);
	Transform3D<> worldTmarker = worldTcam*camTmarker;	

	FixedFrame::Ptr markerFrame = system->getWorkCell()->findFrame<FixedFrame>("Marker");
	ER_LOG_DEBUG("Marker Frame = " << markerFrame );
	Frame* parent = markerFrame->getParent(state);
	Transform3D<> parentTworld = inverse(Kinematics::worldTframe(parent, state));
	ER_LOG_DEBUG("World T marker = " << worldTmarker );
	ER_LOG_DEBUG("parentTworld = " << parentTworld );
	ER_LOG_DEBUG(" Parent T marker = " << parentTworld*worldTmarker );
	markerFrame->setTransform(parentTworld*worldTmarker);
	
	system->setState(state); 
	return true;
}

void LocalizeArmWithEyeInHand::read(er_serialization::InputArchive& iarchive, const std::string& id)
{
	iarchive.readEnterScope("LocalizeArmWithEyeInHand");
	Instruction::read(iarchive, id);
	getSettings().read(iarchive, "Settings");
	iarchive.readLeaveScope("LocalizeArmWithEyeInHand");
}
 
void LocalizeArmWithEyeInHand::write(er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	oarchive.writeEnterScope("LocalizeArmWithEyeInHand");
	Instruction::write(oarchive, id);
	getSettings().write(oarchive, "Settings");

	oarchive.writeLeaveScope("LocalizeArmWithEyeInHand");
}


std::string LocalizeArmWithEyeInHand::getClassIdentifier()
{
	return "LocalizeArmWithEyeInHand";
}
