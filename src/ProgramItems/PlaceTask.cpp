#include "PickAndPlace.hpp"

#include "../Serialization/OutputArchive.hpp"
#include "../Serialization/InputArchive.hpp"

using namespace er_pgi;
using namespace pgm;
using namespace mms_common;
using rw::common::Ptr;
using rw::common::ownedPtr;
PickAndPlace::PickAndPlace():
	ProgramItem()
{
}

PickAndPlace::PickAndPlace(const std::string& name, const std::string& description) :
	ProgramItem(name, description)
{

}

void PickAndPlace::read(mms_serialization::InputArchive& iarchive, const std::string& id)
{
	iarchive.readEnterScope("PickAndPlace");
	//Entity::read(iarchive, id);
	iarchive.readLeaveScope("PickAndPlace");
}
 
void PickAndPlace::write(mms_serialization::OutputArchive& oarchive, const std::string& id) const
{
	oarchive.writeEnterScope("PickAndPlace");
	//Entity::write(oarchive, id);
	oarchive.writeLeaveScope("PickAndPlace");
}
