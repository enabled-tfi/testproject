/* */
#ifndef ER_PGI_PICKANDPLACE_HPP
#define ER_PGI_PICKANDPLACE_HPP

#include <Common/Settings.hpp>
#include <ProgramModel/ProgramItem.hpp>
#include <rw/common/Ptr.hpp>
#include <rw/common/PropertyMap.hpp>

namespace er_pgi {
	


/**
 * General interface for Instructions
 */
class PickAndPlace: public pgm::ProgramItem
{
public:
	/** @brief Smart pointer to the ProgramItem object*/
	typedef rw::common::Ptr<PickAndPlace> Ptr;


	PickAndPlace();

	PickAndPlace(const std::string& name, const std::string& description);



	/* Methods from the serializable interface */
    virtual void read(class mms_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class mms_serialization::OutputArchive& oarchive, const std::string& id) const;

};

} //end namespace

#endif //#ifndef ER_PGI_PICKANDPLACE_HPP
