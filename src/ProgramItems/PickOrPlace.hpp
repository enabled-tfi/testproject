/* */
#ifndef ER_PGI_PICKORPLACE_HPP
#define ER_PGI_PICKORPLACE_HPP

#include <Common/Settings.hpp>
#include <Common/Target.hpp>
#include <ProgramModel/ProgramItem.hpp>
#include <ExecutionModel/Instruction.hpp>
#include <rw/common/Ptr.hpp>
#include <rw/common/PropertyMap.hpp>

namespace er_pgi {
	


/**
 * General interface for Instructions
 */
class PickOrPlace : public exm::Instruction
{
public:
	/** @brief Smart pointer to the PickOrPlace object*/
	typedef rw::common::Ptr<PickOrPlace> Ptr;


	PickOrPlace();

	PickOrPlace(const std::string& name, const std::string& description);
	
	virtual bool execute(exm::ExecutionManager::Ptr executionManager);

	/* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

private:
	void initializeSettings();

	er_common::Setting<er_common::Target>::Ptr _approach;
	er_common::Setting<er_common::Target>::Ptr _target;
	er_common::Setting<er_common::Target>::Ptr _depart;

	//er_common::Target _approach;
	//er_common::Target _target;
	//er_common::Target _depart;
	exm::Instruction::Ptr _graspRoutine;



};

} //end namespace

#endif //#ifndef ER_PGI_PICKORPLACE_HPP
