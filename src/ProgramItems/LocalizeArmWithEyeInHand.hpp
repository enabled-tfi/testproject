/* */
#ifndef ER_PGI_LOCALIZEARMWITHWITHEYEINHAND_HPP
#define ER_PGI_LOCALIZEARMWITHWITHEYEINHAND_HPP

#include <Common/Entity.hpp>
#include <Common/Settings.hpp>
#include <ExecutionModel/Instruction.hpp>
#include <rw/common/Ptr.hpp>
#include <rw/common/PropertyMap.hpp>

namespace er_pgi {
	


/**
 * General interface for Instructions
 */
class LocalizeArmWithEyeInHand : public exm::Instruction
{
public:
	/** @brief Smart pointer to the ProgramItem object*/
	typedef rw::common::Ptr<LocalizeArmWithEyeInHand> Ptr;


	LocalizeArmWithEyeInHand();

	LocalizeArmWithEyeInHand(const std::string& name, const std::string& description, er_common::System::Ptr system);

	/* Methods from the serializable interface */
	virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

	virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

	bool execute(exm::ExecutionManager::Ptr executionManager);

	static std::string getClassIdentifier();

private:
	er_common::System::Ptr _system;
	void initializeSettings();

};

} //end namespace

#endif //#ifndef ER_PGI_LOCALIZEARMWIHEYEINHAND_HPP
