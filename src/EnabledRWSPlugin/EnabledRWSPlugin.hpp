#ifndef ENABLEDRWSPLUGIN_HPP
#define ENABLEDRWSPLUGIN_HPP

#include <rws/RobWorkStudioPlugin.hpp>

#ifndef Q_MOC_RUN
#include <rw/models/WorkCell.hpp>
#include <rw/models/SerialDevice.hpp>
#include <rw/kinematics/State.hpp>
#include <rw/rw.hpp>
#include <rwlibs/opengl/RenderLines.hpp>
#include <rwlibs/opengl/RenderPointCloud.hpp>


#include "../HardwareLayer/SMAC/SMAC.hpp"
#include "../HardwareLayer/UR/UR.hpp"
#include "../HardwareLayer/MobileDevice.hpp"
#include "../HardwareLayer/Scanner2D.hpp"
#include <HardwareLayer/CameraUSBOpenCV/CameraUSBOpenCV.hpp>
//#include "../Common/MobileManipulator.hpp"
#include "../Common/System.hpp"

#include <ProgramModel/Project.hpp>
#include <ApplicationLayer/ApplicationControl.hpp>
#endif 


#include <map>

#include "ui_EnabledRWSPlugin.h" 
#include "ProgramExecuteThread.hpp"
#include "ProgramWidget.hpp"

#include <QTimer>

class EnabledRWSPlugin: public rws::RobWorkStudioPlugin, private Ui::EnabledRWSPlugin {
	Q_OBJECT
	Q_INTERFACES(rws::RobWorkStudioPlugin)
	Q_PLUGIN_METADATA(IID "er.EnabledRWSPlugin.RobWorkStudioPlugin/0.1" FILE "enabledrws_plugin.json")
public:
	EnabledRWSPlugin();
	virtual ~EnabledRWSPlugin();

	virtual void open(rw::models::WorkCell* workcell);

	virtual void close();

	virtual void initialize();

private slots: 

	void stateChangedListener(const rw::kinematics::State& state);
	void timeout();

	void on_btnLoadSystem_clicked();
	void on_btnSaveSystem_clicked();
	void on_btnSelectSystemFile_clicked();

	void on_btnConnectMIR_clicked();
	void on_btnConnectUR_clicked();
	void on_btnConnectCamera_clicked();

	void on_chkAutoUpdateState_stateChanged(int state);


	void on_actionNewProject_triggered();
	void on_actionCloseProject_triggered();
	void on_actionLoadProject_triggered();
	void on_actionSaveProject_triggered(); 
	void on_actionRunProject_triggered();

	void on_actionAddEvent_triggered();
	void on_actionRemoveEvent_triggered();

	void on_actionNewProgram_triggered();
	void on_actionImportProgram_triggered();
	void eventTableUpdated();

private:
	rw::models::WorkCell::Ptr _workcell;
	mvc::ApplicationControl::Ptr _applicationControl;
	//er_common::System::Ptr _system;
	//er_common::MobileManipulator::Ptr _mobileManipulator;
	//pgm::Project::Ptr _project;

	QTimer _timer;
	hwl::Camera::Ptr _camera;

	struct EventWidgets {
		EventWidgets() {}
		EventWidgets(QComboBox* cmb, QSpinBox* spnReg, QSpinBox* spnVal) :
			cmbBox(cmb),
			spnRegister(spnReg),
			spnValue(spnVal)
		{}
			
		QComboBox* cmbBox;
		QSpinBox* spnRegister;
		QSpinBox* spnValue;
	};
	
	std::vector<EventWidgets> _eventWidgets;

	void addProgramToUI(pgm::Program::Ptr program);
	void addEventToUI(pgm::Event::Ptr event);
	std::vector<ProgramWidget*> _programWidgets;
};

#endif /*ENABLEDRWSPLUGIN_HPP*/
