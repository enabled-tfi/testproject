#include "SettingListWidget.hpp"


#include <iostream>
#include <Serialization/InputArchiveXML.hpp>
#include <Serialization/OutputArchiveXML.hpp>
#include <Common/Setting.hpp>
#include <QMessageBox>
#include <QPushButton>

using namespace pgm;
using namespace er_serialization;
using namespace er_common;
using namespace rw::common;

SettingListWidget::SettingListWidget(QWidget* parent):
	QWidget(parent)
{
    setupUi(this);

	_buttonGroup = new QButtonGroup(this);
	_buttonGroup->setExclusive(true);	
	QVBoxLayout* layout = new QVBoxLayout(wdtSettings);
} 


SettingListWidget::~SettingListWidget()
{ 

}

void SettingListWidget::setSettings(er_common::Settings::Ptr settings)
{
	//Start to removing old items
	for (SettingWidget* wdt: _settingWidgets)
	{
		this->layout()->removeWidget(wdt);
		_buttonGroup->removeButton(wdt);
		delete wdt;
	}
	_settingWidgets.clear();

	_settings = settings;
	ER_LOG_DEBUG("Set Settings " << _settings );
	for (SettingBase::Ptr setting : *_settings)
	{
		ER_LOG_DEBUG("Setting = " << setting );
		SettingWidget* wdt = new SettingWidget(setting, this);
		//this->layout()->addWidget(wdt);
		wdtSettings->layout()->addWidget(wdt);
		_settingWidgets.push_back(wdt);
		_buttonGroup->addButton(wdt);
	}
}   