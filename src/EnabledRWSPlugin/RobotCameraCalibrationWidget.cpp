#include "RobotCameraCalibrationWidget.hpp"

#include <QFileDialog>
#include <QMessageBox>
#include <QToolBar>

using namespace er_common;
using namespace mvc;


RobotCameraCalibrationWidget::RobotCameraCalibrationWidget(QWidget* parent):
	QWidget(parent)
{
    setupUi(this);
} 


RobotCameraCalibrationWidget::~RobotCameraCalibrationWidget()
{
}

void RobotCameraCalibrationWidget::initialize(pgm::Program::Ptr program, ApplicationControl::Ptr applicationControl)
{
	_applicationControl = applicationControl;
}


void RobotCameraCalibrationWidget::on_btnLoadConfigurationList_clicked()
{
	QString filename = QFileDialog::getOpenFileName(this, tr("Configuration File"), ".", "*.xml");
	if (filename.isEmpty()) {
		return;
	}
	
	//_applicationControl->loadCalibrationConfigurations(filename.toStdString());
}



void RobotCameraCalibrationWidget::on_btnRecordCalibrationData_clicked()
{
	//_applicationControl->recordCalibrationData();
}