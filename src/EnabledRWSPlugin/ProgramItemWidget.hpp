#ifndef PROGRAMITEMGWIDGET_HPP
#define PROGRAMITEMGWIDGET_HPP

#include "ui_ProgramItemWidget.h" 
#include <QPushButton>
#include <ExecutionModel/Instruction.hpp>

class ProgramItemWidget : public QPushButton, private Ui::ProgramItemWidget {
	Q_OBJECT
public:
	ProgramItemWidget(exm::Instruction::Ptr item, QWidget* parent);
	virtual ~ProgramItemWidget();

	exm::Instruction::Ptr getInstruction();

	private slots:


private:
	exm::Instruction::Ptr _instruction;
};

#endif /*PROGRAMITEMGWIDGET_HPP*/
