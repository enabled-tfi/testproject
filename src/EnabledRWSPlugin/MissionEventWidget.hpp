#ifndef MISSIONEVENTWIDGET_HPP
#define MISSIONEVENTWIDGET_HPP

#include "ui_MissionEventWidget.h" 
#include <Common/System.hpp>
#include <ProgramModel/Program.hpp>
#include <ApplicationLayer/ApplicationControl.hpp>

class MissionEventWidget: public QWidget, private Ui::MissionEventWidget {
	Q_OBJECT
public:
	MissionEventWidget(QWidget* parent);
	virtual ~MissionEventWidget();

	void initialize(pgm::Program::Ptr program, mvc::ApplicationControl::Ptr applicationControl);

private:
	mvc::ApplicationControl::Ptr _applicationControl;
};

#endif /*MISSINGEVENTWIDGET_HPP*/
