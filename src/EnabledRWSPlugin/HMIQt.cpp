#include "HMIQt.hpp"

#include <QMessageBox>
#include <iostream>

#include <Common/Logging.hpp>

HMIQt::HMIQt(QWidget* parent):
	_parent(parent)
{

}

void HMIQt::infoBox(const std::string& title, const std::string& message, bool* blocking)
{
	ER_LOG_DEBUG("Emit Message Box " << title.c_str() << " " << message.c_str() );
	emit showMessageBox(title.c_str(), message.c_str(), blocking);
	
}
