#ifndef DOUBLEEDITORUPDOWNBUTTONS_HPP
#define DOUBLEEDITORUPDOWNBUTTONS_HPP

#include "ui_DoubleEditorUpDownButtons.h" 

#include <QTimer>


class DoubleEditorUpDownButtons : public QWidget, private Ui::DoubleEditorUpDownButtons {
	Q_OBJECT
public:
	DoubleEditorUpDownButtons(QWidget* parent, int index = -1);
	
	virtual ~DoubleEditorUpDownButtons();

	void setCaption(const QString& caption);
	void setRange(double min, double max);
	void setValue(double val);
	void setPrefix(const QString& prefix);
	double getValue();

	void startSyncMode();
	void stopSyncMode();

private slots:
	void on_btnDecrease_pressed();
	void on_btnDecrease_released();
	void on_btnIncrease_pressed();
	void on_btnIncrease_released();
	void timeout();

signals:
	void valueChanged();
	void valueChanged(int index, double delta);
private:
	QTimer timer;
	int _holdCount;
	bool _syncmode;
	int _index;

};

#endif /*DOUBLEEDITORUPDOWNBUTTONS_HPP*/
