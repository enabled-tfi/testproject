#ifndef QNUMERICEDITOR_HPP
#define QNUMERICEDITOR_HPP

#include "ui_QNumericEditor.h" 
#include "DoubleEditorUpDownButtons.hpp"
#include "Editor.hpp"
#include <QPushButton>
#include <QDoubleSpinBox>
#include <Common/Setting.hpp>

class QNumericEditor : public Editor, private Ui::QNumericEditor {
	Q_OBJECT
public:
	QNumericEditor(QWidget* parent);
	QNumericEditor(er_common::Setting<rw::math::Q>::Ptr setting, QWidget* parent);
	virtual ~QNumericEditor();

	void writeChanges();
	void setSetting(er_common::Setting<rw::math::Q>::Ptr setting);

	void setQ(const rw::math::Q& q);
	rw::math::Q getQ() const;

	void startSyncMode();
	void stopSyncMode();

private slots:
	
signals:
	void valueChanged();
	void valueChanged(int index, double delta);
private:
	er_common::Setting<rw::math::Q>::Ptr _setting;
	std::vector<DoubleEditorUpDownButtons*> _spnBoxes;

	void initialize();
	void initializeSpinBoxes(const rw::math::Q& q, const std::pair<rw::math::Q, rw::math::Q>& qrange);
	QGridLayout* _pLayout;
};

#endif /*QNUMERICEDITOR_HPP*/

