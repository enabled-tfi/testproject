#include "QNumericEditor.hpp"
#include <iostream>
#include <Serialization/InputArchiveXML.hpp>
#include <Serialization/OutputArchiveXML.hpp>
#include <Common/DataType.hpp>

#include <QInputDialog>
#include <QScrollArea>
using namespace pgm;
using namespace er_serialization;
using namespace er_common;
using namespace rw::common;
using namespace rw::math;

QNumericEditor::QNumericEditor(QWidget* parent):
	Editor(parent)
{
	initialize();
}

QNumericEditor::QNumericEditor(Setting<Q>::Ptr setting, QWidget* parent) :
	Editor(parent),
	_setting(setting)
{
	initialize();
	setSetting(setting);
}

void QNumericEditor::initialize() {
	setupUi(this);
	this->setLayout(new QVBoxLayout());
	QScrollArea *widg = new QScrollArea();
	widg->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	this->layout()->addWidget(widg);
	widg->setWidgetResizable(true);
	QWidget* base = new QWidget(this);
	_pLayout = new QGridLayout(base);
	base->setLayout(_pLayout);
	widg->setWidget(base);
} 

void QNumericEditor::initializeSpinBoxes(const Q& q, const std::pair<Q,Q>& qrange) {
	for (DoubleEditorUpDownButtons* btn : _spnBoxes)
	{
		delete btn;
	}
	_spnBoxes.clear();

	for (size_t i = 0; i < q.size(); i++)
	{
		DoubleEditorUpDownButtons* spn = new DoubleEditorUpDownButtons(this, i);
		spn->setRange(-DBL_MAX, DBL_MAX);
		spn->setValue(q(i)*Rad2Deg);
		spn->setPrefix("Deg ");
		spn->setRange(qrange.first(i)*Rad2Deg, qrange.second(i)*Rad2Deg);
		spn->setCaption(tr("Q[%1]").arg(i + 1));
		QSizePolicy sizePolicy = spn->sizePolicy();
		sizePolicy.setVerticalPolicy(QSizePolicy::Fixed);
		//spn->setMinimumHeight(20);
		spn->setSizePolicy(sizePolicy);
		//
		_pLayout->addWidget(spn, i, 0);
		_pLayout->setRowStretch(i, 1);
		_spnBoxes.push_back(spn);
		connect(spn, SIGNAL(valueChanged()), this, SIGNAL(valueChanged()));
		connect(spn, SIGNAL(valueChanged(int, double)), this, SIGNAL(valueChanged(int, double)));
	}
	

}


QNumericEditor::~QNumericEditor()
{
	while (QWidget* w = findChild<QWidget*>())
		delete w;
	delete layout();
}

void QNumericEditor::setSetting(er_common::Setting<rw::math::Q>::Ptr setting)
{
	_setting = setting;
	if (setting->isRangeInitialized()) {
		ER_LOG_DEBUG("Setting Range initialized " );
		initializeSpinBoxes(_setting->getValue(), _setting->getRange());
	}
	else {
		ER_LOG_DEBUG("Setting Range initialized false " );
		const size_t n = _setting->getValue().size();
		Q qmax(n);
		for (size_t i = 0; i < n; i++) {
			qmax(i) = DBL_MAX;
		}
		initializeSpinBoxes(_setting->getValue(), std::make_pair(-qmax, qmax));
	}
	setQ(setting->getValue());
}

void QNumericEditor::setQ(const rw::math::Q& q) 
{
	if (_spnBoxes.size() != q.size()) {
		const size_t n = _setting->getValue().size();
		Q qmax(n);
		for (size_t i = 0; i < n; i++) {
			qmax(i) = DBL_MAX;
		}
		initializeSpinBoxes(q, std::make_pair(-qmax, qmax));
	}
	else {
		for (size_t i = 0; i < q.size(); i++)
		{
			_spnBoxes[i]->setValue(q(i)*Rad2Deg);
		}
	}
}
rw::math::Q QNumericEditor::getQ() const
{
	Q qres(_spnBoxes.size());
	for (size_t i = 0; i < _spnBoxes.size(); i++) {
		qres(i) = _spnBoxes[i]->getValue()*Deg2Rad;
	}

	return qres;
}


void QNumericEditor::startSyncMode()
{
	for (size_t i = 0; i < _spnBoxes.size(); i++) {
		_spnBoxes[i]->startSyncMode();
	}
}

void QNumericEditor::stopSyncMode()
{
	for (size_t i = 0; i < _spnBoxes.size(); i++) {
		_spnBoxes[i]->stopSyncMode();
	}
}



void QNumericEditor::writeChanges()
{
	if (_setting == NULL) {
		RW_THROW("QNumericEditor::writeChanges: The setting appears to be null");
	}
	_setting->setValue(getQ());
}