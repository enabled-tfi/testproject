#ifndef QTARGETEDITOR_HPP
#define QTARGETEDITOR_HPP

#include "ui_QTargetEditor.h" 
#include "Editor.hpp"
#include <QPushButton>
#include <QDoubleSpinBox>
#include <QTimer>
#include <Common/Setting.hpp>
#include <Common/Target.hpp>
#include <Common/System.hpp>
#include <ExecutionModel/ExecutionManager.hpp>

class QTargetEditor : public Editor, private Ui::QTargetEditor {
	Q_OBJECT
public:
	QTargetEditor(QWidget* parent);

	QTargetEditor(er_common::Setting<rw::math::Q>::Ptr setting, QWidget* parent);
	virtual ~QTargetEditor();

	void setSetting(er_common::Setting<rw::math::Q>::Ptr setting);

	void writeChanges();

private slots:
	void on_btnTeachMode_pressed();
	void on_btnTeachMode_released();
	void on_btnSetRobotPoseAsTarget_clicked();
	void on_btnGotoTargetPosition_clicked();
	void on_chkSyncWithRobot_stateChanged(int);
	void syncTimeOut();
	void valueChanged();
	void valueChanged(int index, double delta);
private:
	er_common::Setting<rw::math::Q>::Ptr _setting;
	er_common::System::Ptr _system;
	exm::ExecutionManager::Ptr _execManager;
	QTimer _syncTimer;
	void initialize();

};


#endif /*TARGETEDITOR_HPP*/

