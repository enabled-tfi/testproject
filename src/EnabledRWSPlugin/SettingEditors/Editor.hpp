#ifndef EDITOR_HPP
#define EDITOR_HPP

#include <QWidget>


class Editor : public QWidget {
	Q_OBJECT
public:
	Editor(QWidget* parent);
	virtual ~Editor();

	virtual void writeChanges() = 0;


};

#endif /*EDITOR_HPP*/

