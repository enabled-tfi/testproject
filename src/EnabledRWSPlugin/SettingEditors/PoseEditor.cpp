#include "PoseEditor.hpp"

#include <iostream>
#include <Serialization/InputArchiveXML.hpp>
#include <Serialization/OutputArchiveXML.hpp>
#include <Common/DataType.hpp>

#include <QInputDialog>
#include <QScrollArea>
using namespace pgm;
using namespace er_serialization;
using namespace er_common;
using namespace rw::common;
using namespace rw::math;


PoseEditor::PoseEditor(QWidget* parent) :
	QWidget(parent)
{
	setupUi(this);
	initialize();
}


PoseEditor::PoseEditor(Setting<Transform3D<> >::Ptr setting, QWidget* parent):
	QWidget(parent),
	_setting(setting)
{
    setupUi(this);	
	initialize();
} 


PoseEditor::~PoseEditor()
{
}

void PoseEditor::initialize() 
{
	editorPosX->setCaption("X");
	editorPosX->setRange(-DBL_MAX, DBL_MAX);
	editorPosY->setCaption("Y");
	editorPosY->setRange(-DBL_MAX, DBL_MAX);
	editorPosZ->setCaption("Z");
	editorPosZ->setRange(-DBL_MAX, DBL_MAX);
	editorRot1->setCaption("RX");
	editorRot1->setRange(-360, 360);
	editorRot2->setCaption("RY");
	editorRot2->setRange(-360, 360);
	editorRot3->setCaption("RZ");
	editorRot3->setRange(-360, 360);
}

void PoseEditor::setPose(const rw::math::Transform3D<>& pose)
{
	_pose = pose;
	editorPosX->setValue(pose.P()(0));
	editorPosY->setValue(pose.P()(1));
	editorPosZ->setValue(pose.P()(2));
	EAA<> eaa(pose.R());
	editorRot1->setValue(eaa(0)*Rad2Deg);
	editorRot2->setValue(eaa(1)*Rad2Deg);
	editorRot3->setValue(eaa(2)*Rad2Deg);
}

const rw::math::Transform3D<>& PoseEditor::getPose() const
{
	return _pose;
}

void PoseEditor::setSetting(er_common::Setting<rw::math::Transform3D<> >::Ptr setting)
{
	_setting = setting;
	setPose(_setting->getValue());
}

void PoseEditor::writeChanges()
{
	_pose.P()(0) = editorPosX->getValue();
	_pose.P()(1) = editorPosY->getValue();
	_pose.P()(2) = editorPosZ->getValue();

	EAA<> eaa(editorRot1->getValue()*Deg2Rad, editorRot2->getValue()*Deg2Rad, editorRot3->getValue()*Deg2Rad);
	_pose.R() = eaa.toRotation3D();

	if (_setting != NULL)
		_setting->setValue(_pose);
}