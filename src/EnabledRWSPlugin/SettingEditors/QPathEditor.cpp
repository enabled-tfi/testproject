#include "QPathEditor.hpp"

#include <iostream>
#include <Serialization/InputArchiveXML.hpp>
#include <Serialization/OutputArchiveXML.hpp>
#include <Common/DataType.hpp>
#include <Common/EntityRegister.hpp>
#include <HardwareLayer/Manipulator.hpp>
#include <ExecutionModel/MovePTP.hpp>
#include <ExecutionModel/ExecutionManager.hpp>

#include <QInputDialog>
#include <QScrollArea>
#include <QMessageBox>



using namespace pgm;
using namespace hwl;
using namespace er_serialization;
using namespace er_common;
using namespace exm;
using namespace rw::common;
using namespace rw::math;
using namespace rw::kinematics;
using namespace rw::trajectory;


QPathEditor::QPathEditor(Setting<QPath>::Ptr setting, QWidget* parent):
	Editor(parent),
	_setting(setting),
	_system(System::getInstance())
{
    setupUi(this);	
	_execManager = ownedPtr(new ExecutionManager(_system));
	
	for (Q& q : setting->getValue()) {
		listQs->addItem(tr("Q[%1]").arg(listQs->count()+1));
		_qvalues.push_back(ownedPtr(new Setting<Q>("Q","", q)));
	}
	if (listQs->count() > 0) {
		listQs->setCurrentRow(0);
	}

} 


QPathEditor::~QPathEditor()
{
}

void QPathEditor::writeChanges()
{
	targetEditor->writeChanges();
	int i = 0;
	_setting->getValue().clear();
	for (Setting<Q>::Ptr q : _qvalues) {
		_setting->getValue().push_back(q->getValue());
	}
}


void QPathEditor::on_btnAddConfiguration_clicked()
{
	State state = _system->getState();
	Q q = _system->getManipulator()->getQ(state);	
	_qvalues.push_back(ownedPtr(new Setting<Q>("Q", "", q)));
	_setting->getValue().push_back(q);
	listQs->addItem(tr("Q[%1]").arg(listQs->count()+1));
	listQs->setCurrentRow(listQs->count()-1);
}

void QPathEditor::on_btnRemoveConfiguration_clicked()
{
	int idx = listQs->currentRow();
	if (idx < 0)
		return;

	_qvalues.erase(_qvalues.begin() + idx);
	QListWidgetItem* item = listQs->takeItem(idx);
	delete item;

}

void QPathEditor::on_listQs_currentRowChanged(int currentRow)
{
	targetEditor->writeChanges();
	ER_LOG_DEBUG("Row Selected " << currentRow );
	if (currentRow <= (int)_qvalues.size()) {
		targetEditor->setSetting(_qvalues.at(currentRow));
	}
	else {
		QMessageBox::critical(this, "Exception", tr("No configuration matching row number %1").arg(currentRow));
	}
}