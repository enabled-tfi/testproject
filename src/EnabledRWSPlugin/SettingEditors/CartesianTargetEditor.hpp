#ifndef TARGETEDITOR_HPP
#define TARGETEDITOR_HPP

#include "ui_CartesianTargetEditor.h" 
#include <QPushButton>
#include <QDoubleSpinBox>
#include <Common/Setting.hpp>
#include <Common/Target.hpp>
#include <Common/System.hpp>
#include <ExecutionModel/ExecutionManager.hpp>

class CartesianTargetEditor : public QWidget, private Ui::CartesianTargetEditor {
	Q_OBJECT
public:
	CartesianTargetEditor(er_common::Setting<er_common::Target>::Ptr setting, QWidget* parent);
	virtual ~CartesianTargetEditor();

	void writeChanges();

private slots:
	void on_btnTeachMode_pressed();
	void on_btnTeachMode_released();
	void on_btnSetRobotPoseAsTarget_clicked();
	void on_btnGotoTargetPosition_clicked();

	void on_cmbReference_currentIndexChanged(int index);

private:
	er_common::Setting<er_common::Target>::Ptr _setting;
	er_common::System::Ptr _system;
	exm::ExecutionManager::Ptr _execManager;
	rw::kinematics::Frame* getSelectedReferenceFrame();
	rw::kinematics::Frame* _currentFrame;
};

#endif /*TARGETEDITOR_HPP*/

