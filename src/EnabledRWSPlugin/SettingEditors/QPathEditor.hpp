#ifndef QPATHEDITOR_HPP
#define QPATHEDITOR_HPP

#include "ui_QPathEditor.h" 
#include "Editor.hpp"
#include <QPushButton>
#include <QDoubleSpinBox>
#include <Common/Setting.hpp>
#include <Common/Target.hpp>
#include <Common/System.hpp>
#include <ExecutionModel/ExecutionManager.hpp>

class QPathEditor : public Editor, private Ui::QPathEditor {
	Q_OBJECT
public:
	QPathEditor(er_common::Setting<rw::trajectory::QPath>::Ptr setting, QWidget* parent);
	virtual ~QPathEditor();

	void writeChanges();

private slots:
	void on_btnAddConfiguration_clicked();
	void on_btnRemoveConfiguration_clicked();
	void on_listQs_currentRowChanged(int currentRow);
private:
	er_common::Setting<rw::trajectory::QPath>::Ptr _setting;
	std::vector<er_common::Setting<rw::math::Q>::Ptr> _qvalues;
	er_common::System::Ptr _system;
	exm::ExecutionManager::Ptr _execManager;
};

#endif /*QPATHEDITOR_HPP*/

