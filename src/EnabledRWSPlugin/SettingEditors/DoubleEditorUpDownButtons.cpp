#include "DoubleEditorUpDownButtons.hpp"

#include <iostream>




DoubleEditorUpDownButtons::DoubleEditorUpDownButtons(QWidget* parent, int index):
	QWidget(parent),
	_index(index)
{
    setupUi(this);
	 
	timer.setInterval(50);
	timer.setSingleShot(false);
	connect(&timer, SIGNAL(timeout()), this, SLOT(timeout()));

}

DoubleEditorUpDownButtons::~DoubleEditorUpDownButtons()
{

}

void DoubleEditorUpDownButtons::setCaption(const QString& caption) 
{
	lblCaption->setText(caption);
}

void DoubleEditorUpDownButtons::setRange(double min, double max)
{
	spnValue->setRange(min, max);
}

void DoubleEditorUpDownButtons::setValue(double val)
{
	spnValue->setValue(val);
}

void DoubleEditorUpDownButtons::setPrefix(const QString& prefix)
{
	spnValue->setPrefix(prefix);
}

double DoubleEditorUpDownButtons::getValue()
{
	return spnValue->value();
}

void DoubleEditorUpDownButtons::on_btnDecrease_pressed() 
{
	_holdCount = 0;
	timeout();
	timer.start();
}

void DoubleEditorUpDownButtons::on_btnDecrease_released()
{
	emit valueChanged(_index, 0);
	timer.stop();
}


void DoubleEditorUpDownButtons::on_btnIncrease_pressed()
{
	_holdCount = 0;
	timeout();
	timer.start();	
}

void DoubleEditorUpDownButtons::on_btnIncrease_released()
{
	emit valueChanged(_index, 0);
	timer.stop();
}

void DoubleEditorUpDownButtons::startSyncMode()
{
	_syncmode = true;
}


void DoubleEditorUpDownButtons::stopSyncMode()
{
	_syncmode = false;
}


void DoubleEditorUpDownButtons::timeout()
{
	_holdCount++;
	if (_holdCount > 10)
		_holdCount = 10;
	double delta = 0.001*(_holdCount*_holdCount);
	if (_syncmode == false) {
		if (btnDecrease->isDown()) {
			double val = spnValue->value();
			val -= delta;
			spnValue->setValue(val);
		}  
		if (btnIncrease->isDown()) {
			double val = spnValue->value();
			val += delta;
			spnValue->setValue(val);
		}
		emit valueChanged();
	}
	else {
		if (btnDecrease->isDown()) {
			emit valueChanged(_index, -delta);
		}
		else {
			emit valueChanged(_index, delta);
		}

	}
	

}