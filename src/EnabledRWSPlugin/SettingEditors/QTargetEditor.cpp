#include "QTargetEditor.hpp"

#include <iostream>
#include <Serialization/InputArchiveXML.hpp>
#include <Serialization/OutputArchiveXML.hpp>
#include <Common/DataType.hpp>
#include <Common/EntityRegister.hpp>
#include <HardwareLayer/Manipulator.hpp>
#include <ExecutionModel/MovePTP.hpp>
#include <ExecutionModel/ExecutionManager.hpp>

#include <QInputDialog>
#include <QScrollArea>
#include <QMessageBox>
using namespace pgm;
using namespace hwl;
using namespace er_serialization;
using namespace er_common;
using namespace exm;
using namespace rw::common;
using namespace rw::math;
using namespace rw::kinematics;


QTargetEditor::QTargetEditor(QWidget* parent):
	Editor(parent),
	_system(System::getInstance())
{
	initialize();
	//setupUi(this);
	//_execManager = ownedPtr(new ExecutionManager(_system));

}

QTargetEditor::QTargetEditor(Setting<Q>::Ptr setting, QWidget* parent):
	Editor(parent),
	_setting(setting),
	_system(System::getInstance())
{
    //setupUi(this);	
	initialize();
	numericEditor->setSetting(setting);

} 

void QTargetEditor::initialize()
{
	setupUi(this);
	_execManager = ownedPtr(new ExecutionManager(_system));
	connect(&_syncTimer, SIGNAL(timeout()), this, SLOT(syncTimeOut()));
	_syncTimer.setInterval(100);
	_syncTimer.setSingleShot(false);

	connect(numericEditor, SIGNAL(valueChanged()), this, SLOT(valueChanged()));
	connect(numericEditor, SIGNAL(valueChanged(int, double)), this, SLOT(valueChanged(int, double)));
}


QTargetEditor::~QTargetEditor()
{
}

void QTargetEditor::setSetting(Setting<rw::math::Q>::Ptr setting)
{
	_setting = setting;
	numericEditor->setSetting(setting);
}


void QTargetEditor::writeChanges()
{
	if (_setting != NULL)
	{
		Q q = numericEditor->getQ();
		_setting->setValue(q);
	}
}

void QTargetEditor::on_btnSetRobotPoseAsTarget_clicked()
{
	_system->update();
	State state = _system->getState();
	Q q = _system->getManipulator()->getQ(state);
	numericEditor->setQ(q);

}

void QTargetEditor::on_btnGotoTargetPosition_clicked()
{
	Q q = numericEditor->getQ();
	Manipulator::Ptr manipulator = EntityRegister::get<Manipulator>("Manipulator");
	if (manipulator.isNull()) {
		QMessageBox::critical(this, tr("QTargetEditor"), tr("No instance of Robot arm found"));
		return;
	}
	//manipulator->ptp(q);
	if (_execManager->getExecutionStatus() != ExecutionManager::Running) {
		MovePTP::Ptr move = ownedPtr(new MovePTP(q, manipulator));
		_execManager->execute(move, false);
	}
}


void QTargetEditor::syncTimeOut()
{
	on_btnSetRobotPoseAsTarget_clicked();
}

void QTargetEditor::valueChanged() {
	if (chkSyncWithRobot->isChecked()) {
		on_btnGotoTargetPosition_clicked();
	}
	ER_LOG_DEBUG("New Value = " << numericEditor->getQ() );
}

void QTargetEditor::valueChanged(int index, double delta)
{
	ER_LOG_DEBUG("valueChanged(int, double)" );
	if (chkSyncWithRobot->isChecked()) {
		_system->update();
		State state = _system->getState();
		Q q = _system->getManipulator()->getQ(state);
		ER_LOG_DEBUG("Q in " << q*Rad2Deg <<" "<<delta*Rad2Deg);
		q(index) += delta;
		ER_LOG_DEBUG("Q out " << q*Rad2Deg );

		Manipulator::Ptr manipulator = EntityRegister::get<Manipulator>("Manipulator");
		if (manipulator.isNull()) {
			QMessageBox::critical(this, tr("QTargetEditor"), tr("No instance of Robot arm found"));
			_syncTimer.stop();
			return;
		}
		//manipulator->ptp(q);
		if (_execManager->getExecutionStatus() != ExecutionManager::Running) {
			MovePTP::Ptr move = ownedPtr(new MovePTP(q, manipulator));
			_execManager->execute(move, false);
		}
		//TODO: Add check of bounds

	}
}

void QTargetEditor::on_btnTeachMode_pressed()
{
	try {
		Manipulator::Ptr manipulator = EntityRegister::get<Manipulator>("Manipulator");
		if (manipulator.isNull()) {
			QMessageBox::critical(this, tr("QTargetEditor"), tr("No instance of Robot arm found"));
			return;
		}
		manipulator->startTeachMode();
		if (chkSyncWithRobot->isChecked()) {
			_syncTimer.start();
		}
	}
	catch (const std::exception& exp) {
		QMessageBox::critical(this, tr("QTargetEditor"), tr("Exception: %1").arg(exp.what()));
	}
}

void QTargetEditor::on_btnTeachMode_released()
{
	try {
		Manipulator::Ptr manipulator = EntityRegister::get<Manipulator>("Manipulator");
		if (manipulator.isNull()) {
			QMessageBox::critical(this, tr("QTargetEditor"), tr("No instance of Robot arm found"));
			return;
		}
		manipulator->endTeachMode();
		if (chkSyncWithRobot->isChecked()) {
			_syncTimer.stop();
		}

	}
	catch (const std::exception& exp) {
		QMessageBox::critical(this, tr("QTargetEditor"), tr("Exception: %1").arg(exp.what()));
	}

}

void QTargetEditor::on_chkSyncWithRobot_stateChanged(int)
{
	if (chkSyncWithRobot->isChecked()) {
		Manipulator::Ptr manipulator = EntityRegister::get<Manipulator>("Manipulator");
		if (manipulator.isNull()) {
			QMessageBox::information(this, "QTargetEditor", "Unable to synchronize. No Manipulator found.");
			chkSyncWithRobot->setChecked(false);
		}
		else {
			numericEditor->startSyncMode();
			_syncTimer.start();
		}
	}
	else {
		numericEditor->stopSyncMode();
		_syncTimer.stop();
	}
}