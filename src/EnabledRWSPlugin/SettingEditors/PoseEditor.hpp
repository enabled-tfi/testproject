#ifndef POSEEDITOR_HPP
#define POSEEDITOR_HPP

#include "ui_PoseEditor.h" 
#include <QPushButton>
#include <QDoubleSpinBox>
#include <Common/Setting.hpp>
#include <rw/math/Transform3D.hpp>

class PoseEditor : public QWidget, private Ui::PoseEditor {
	Q_OBJECT
public:
	PoseEditor(QWidget* parent);
	PoseEditor(er_common::Setting<rw::math::Transform3D<> >::Ptr, QWidget* parent);
	virtual ~PoseEditor();

	void setSetting(er_common::Setting<rw::math::Transform3D<> >::Ptr setting);

	void setPose(const rw::math::Transform3D<>& pose);
	const rw::math::Transform3D<>& getPose() const;

	void writeChanges();

private slots:
	

private:
	void initialize();
	rw::math::Transform3D<> _pose;
	er_common::Setting<rw::math::Transform3D<> >::Ptr _setting;

};

#endif /*POSEEDITOR_HPP*/

