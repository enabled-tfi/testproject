#include "CartesianTargetEditor.hpp"

#include <iostream>
#include <Serialization/InputArchiveXML.hpp>
#include <Serialization/OutputArchiveXML.hpp>
#include <Common/DataType.hpp>
#include <Common/EntityRegister.hpp>
#include <HardwareLayer/Manipulator.hpp>
#include <ExecutionModel/MoveT3D.hpp>
#include <ExecutionModel/ExecutionManager.hpp>

#include <QInputDialog>
#include <QScrollArea>
#include <QMessageBox>
using namespace pgm;
using namespace hwl;
using namespace er_serialization;
using namespace er_common;
using namespace exm;
using namespace rw::common;
using namespace rw::math;
using namespace rw::kinematics;


CartesianTargetEditor::CartesianTargetEditor(Setting<Target>::Ptr setting, QWidget* parent):
	QWidget(parent),
	_setting(setting),
	_system(System::getInstance())
{
    setupUi(this);	
	//Initialize _currentFrame = NULL s.t. it can be used to check whether changes in the combobox is due to real changes or the initialization
	_currentFrame = NULL;
	
	if (setting->getValue().isInitialized() == false) {
		setting->getValue().setReference(_system->getManipulator()->getBase());
		setting->getValue().setRefTtarget(Kinematics::frameTframe(_system->getManipulator()->getBase(), _system->getToolTCP().get(), _system->getState()));
	}
	
	std::vector<Frame*> references = _system->getReferenceFrames();
	ER_LOG_DEBUG("Reference Frames " << references.size() );
	for (Frame* frame : references) {
		cmbReference->addItem(frame->getName().c_str());
	}





	int idx = cmbReference->findText(setting->getValue().getReferenceFrameName().c_str());
	ER_LOG_DEBUG("Index = " << idx << " " << setting->getValue().getReferenceFrameName() );
	if (idx > -1) {
		cmbReference->setCurrentIndex(idx);
	}
	poseEditor->setPose(setting->getValue().refTtarget());
	_currentFrame = setting->getValue().getReference();
	ER_LOG_DEBUG("Current Frame Initialized to " << _currentFrame );

	_execManager = ownedPtr(new ExecutionManager(_system));
} 


CartesianTargetEditor::~CartesianTargetEditor()
{
}


void CartesianTargetEditor::writeChanges()
{
	poseEditor->writeChanges();
	Transform3D<> refTtcp = poseEditor->getPose();
	_setting->getValue().setRefTtarget(refTtcp);
	_setting->getValue().setReference(getSelectedReferenceFrame());
}

Frame* CartesianTargetEditor::getSelectedReferenceFrame() {
	std::string refframename = cmbReference->currentText().toStdString();
	Frame* refframe = _system->getWorkCell()->findFrame(refframename);
	if (refframe == NULL) {
		QMessageBox::critical(this, tr("TargetEditor"), tr("Unabled to find specified reference frame named '%1").arg(refframename.c_str()));
		return NULL;
	}
	return refframe;
}

void CartesianTargetEditor::on_cmbReference_currentIndexChanged(int index)
{
	ER_LOG_DEBUG("Combobox updated " );
	if (_currentFrame == NULL)
		return;
	Transform3D<> oldTtarget = poseEditor->getPose();
	Frame* oldRef = _currentFrame;
	_currentFrame = getSelectedReferenceFrame();

	Transform3D<> newTold = Kinematics::frameTframe(_currentFrame, oldRef, _system->getState());
	Transform3D<> newTtarget = newTold*oldTtarget;
	ER_LOG_DEBUG("New Target = " << newTtarget );

	poseEditor->setPose(newTtarget);
}

void CartesianTargetEditor::on_btnSetRobotPoseAsTarget_clicked()
{
	_system->update();
	State state = _system->getState();
	FixedFrame::Ptr tcp = _system->getToolTCP();
	Frame* refframe = getSelectedReferenceFrame();
	if (refframe == NULL) {
		return;
	}
	Transform3D<> refTtcp = Kinematics::frameTframe(refframe, tcp.get(), state);
	ER_LOG_DEBUG("Sets Ref T tcp " << refTtcp );
	ER_LOG_DEBUG("Robot Config = " << _system->getManipulator()->getQ(state) );
	//_setting->getValue().setRefTtarget(refTtcp);
	poseEditor->setPose(refTtcp);
}

void CartesianTargetEditor::on_btnGotoTargetPosition_clicked()
{
	poseEditor->writeChanges();
	Transform3D<> refTtcp = poseEditor->getPose();
	ER_LOG_DEBUG("Ref To Target " << refTtcp );

	Manipulator::Ptr manipulator = EntityRegister::get<Manipulator>("Manipulator");
	if (manipulator.isNull()) {
		QMessageBox::critical(this, tr("TargetEditor"), tr("No instance of Robot arm found"));
		return;
	}
	_system->update();
	State state = _system->getState();
	Frame* base = _system->getManipulator()->getBase();
	Frame* refframe = _currentFrame;// getSelectedReferenceFrame();
	if (refframe == NULL) {
		return;
	}
	ER_LOG_DEBUG("Refframe = " << refframe->getName() );
	
	Transform3D<> baseTref = Kinematics::frameTframe(base, refframe, state);
	Transform3D<> baseTtcpTarget = baseTref*refTtcp;
	ER_LOG_DEBUG("baseTtarget" << baseTtcpTarget );
	  
	FixedFrame::Ptr tcp = _system->getToolTCP();
	manipulator->setEndToTCP(Kinematics::frameTframe(_system->getManipulator()->getEnd(), tcp.get(), state));
	
	Transform3D<> baseTtarget = Kinematics::frameTframe(base, tcp.get(), state);
	ER_LOG_DEBUG("baseTtarget2" << baseTtcpTarget );

	Transform3D<> tcpTend = Kinematics::frameTframe(tcp.get(), _system->getManipulator()->getEnd(), state);
	ER_LOG_DEBUG("Tries to run IK with target " << baseTtarget*tcpTend );
	std::vector<Q> qsol = _system->getManipulatorIKSolver()->solve(baseTtarget*tcpTend, state);
	ER_LOG_DEBUG("Number of IK solutions: " << qsol.size() );
	for (Q& q : qsol) {
		ER_LOG_DEBUG("\tQ=" << q );
	}
	//manipulator->ptp(qsol.front());


	MoveT3D::Ptr move = ownedPtr(new MoveT3D(refTtcp, refframe, manipulator));
	//MoveT3DPlanned::Ptr move = ownedPtr(new MoveT3DPlanned(refTtcp, refframe, manipulator));
	
	_execManager->execute(move, true);


}



void CartesianTargetEditor::on_btnTeachMode_pressed()
{
	try {
		Manipulator::Ptr manipulator = EntityRegister::get<Manipulator>("Manipulator");
		if (manipulator.isNull()) {
			QMessageBox::critical(this, tr("TargetEditor"), tr("No instance of Robot arm found"));
			return;
		}
		manipulator->startTeachMode();
	}
	catch (const std::exception& exp) {
		QMessageBox::critical(this, tr("TargetEditor"), tr("Exception: %1").arg(exp.what()));
	}
}

void CartesianTargetEditor::on_btnTeachMode_released()
{
	try {
		Manipulator::Ptr manipulator = EntityRegister::get<Manipulator>("Manipulator");
		if (manipulator.isNull()) {
			QMessageBox::critical(this, tr("TargetEditor"), tr("No instance of Robot arm found"));
			return;
		}
		manipulator->endTeachMode();
	}
	catch (const std::exception& exp) {
		QMessageBox::critical(this, tr("TargetEditor"), tr("Exception: %1").arg(exp.what()));
	}

}
