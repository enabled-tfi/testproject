#include "EnabledRWSPlugin.hpp"



#include <RobWorkStudio.hpp>
#include <QInputDialog>
#include <QFileDialog>
#include <QMessageBox>
#include <QToolBar>


#include <Serialization/InputArchiveXML.hpp>
#include <Serialization/OutputArchiveXML.hpp>


#include <rw/common/LogMultiWriter.hpp>
#include <rw/common/LogFileWriter.hpp>
#include <rw/common/LogStreamWriter.hpp>

USE_ROBWORK_NAMESPACE

using namespace er_common; 
using namespace er_serialization;
using namespace hwl;
using namespace pgm;
using namespace exm;
using namespace rws;
using namespace mvc;
using namespace rw::common;
using namespace rw::models;
using namespace rw::kinematics; 


EnabledRWSPlugin::EnabledRWSPlugin():
    RobWorkStudioPlugin("EnabledRWSPlugin", QIcon(":/workspace_icon.png"))
{
	ER_LOG_DEBUG("EnabledRWSPLugin" );
    setupUi(this);
	connect(&_timer, SIGNAL(timeout()), this, SLOT(timeout()));


	QToolBar* toolbarProject = new QToolBar(this);
	QVBoxLayout* layoutProject = new QVBoxLayout(wdtToolBarProject);
	wdtToolBarProject->layout()->addWidget(toolbarProject);
	toolbarProject->addAction(actionNewProject);
	toolbarProject->addAction(actionLoadProject);
	toolbarProject->addAction(actionSaveProject);
	toolbarProject->addAction(actionCloseProject);
	toolbarProject->addAction(actionRunProject);

	QToolBar* toolbarEvents = new QToolBar(this);
	QVBoxLayout* layouEventsSetup = new QVBoxLayout(wdtToolBarEventsSetup);
	wdtToolBarEventsSetup->layout()->addWidget(toolbarEvents);
	toolbarEvents->addAction(actionAddEvent);
	toolbarEvents->addAction(actionRemoveEvent);

	QToolBar* toolbarPrograms = new QToolBar(this);
	QVBoxLayout* layoutPrograms = new QVBoxLayout(wdtToolBarPrograms);
	wdtToolBarPrograms->layout()->addWidget(toolbarPrograms);
	toolbarPrograms->addAction(actionNewProgram);
	toolbarPrograms->addAction(actionImportProgram);
	ER_LOG_DEBUG("EnabledRWSPLugin finished" );



} 


EnabledRWSPlugin::~EnabledRWSPlugin()
{
	_timer.stop();
}



void EnabledRWSPlugin::initialize() {
    getRobWorkStudio()->stateChangedEvent().add(boost::bind(&EnabledRWSPlugin::stateChangedListener, this, _1), this);

	rw::common::Log::log().setLog(&getRobWorkStudio()->log());
	rw::common::Log::log().setLogIndexMask(rw::common::Log::AllMask);
	Ptr<LogMultiWriter> logmulti = ownedPtr(new LogMultiWriter());
	logmulti->addWriter(ownedPtr(new LogFileWriter("Log.txt")));
	logmulti->addWriter(rw::common::Log::log().getWriter(Log::Info));
	logmulti->addWriter(ownedPtr(new LogStreamWriter(&std::cout)));
	rw::common::Log::log().setWriterForMask(rw::common::Log::AllMask, logmulti);


	_applicationControl = ownedPtr(new ApplicationControl(getRobWorkStudio()));
	_applicationControl->newProject("Project");

    std::string systemfile = getRobWorkStudio()->getPropertyMap().get<std::string>("ER_SystemFile", "");
    edtSystemFile->setText(systemfile.c_str());

}

void EnabledRWSPlugin::open(rw::models::WorkCell* workcell)
{
	ER_LOG_DEBUG("WorkCell open" );
	_workcell = workcell;
	//If the workcell is empty we don't want to attempt initializing
	if (workcell->getDevices().size() == 0)
		return;
	System::getInstance()->setWorkCell(workcell);
	//ER_LOG_DEBUG("Create ApplicationControl " );
	//_applicationControl = ownedPtr(new ApplicationControl(workcell, getRobWorkStudio()));
	//_applicationControl->newProject("Project");

}
 
void EnabledRWSPlugin::close()
{
	
}


 
void EnabledRWSPlugin::stateChangedListener(const State& state) {
}
 
void EnabledRWSPlugin::on_btnSelectSystemFile_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Open System"), edtSystemFile->text(), "*.system.xml");
    if (filename.isEmpty())
        return;
    edtSystemFile->setText(filename);
    getRobWorkStudio()->getPropertyMap().addForce("ER_SystemFile", "System file", filename.toStdString());
    ER_LOG_DEBUG("ER System file = " << getRobWorkStudio()->getPropertyMap().get<std::string>("ER_SystemFile") );

}

void EnabledRWSPlugin::on_btnLoadSystem_clicked()
{
    QString filename = edtSystemFile->text();
    if (filename.isEmpty()) {
        QString filename = QFileDialog::getOpenFileName(this, tr("Open System"), ".", "*.system.xml");
        if (filename.isEmpty())
            return;
        getRobWorkStudio()->getPropertyMap().addForce("ER_SystemFile", "System file", filename.toStdString());

    }

	QMessageBox::StandardButton reply = QMessageBox::question(this, "Load System", "Connect to devices?");
	bool connect = reply == QMessageBox::Yes;
	_applicationControl->loadSystem(filename.toStdString(), connect);
}

void EnabledRWSPlugin::on_btnSaveSystem_clicked()
{
	QString filename = QFileDialog::getSaveFileName(this, tr("Save Project"), ".", "*.xml");
	if (filename.isEmpty())
		return;

	_applicationControl->saveSystem(filename.toStdString());

}



void EnabledRWSPlugin::on_btnConnectMIR_clicked()
{
	if (btnConnectMIR->text() == "Connect MIR") {
		try {
			std::string host = "";
			if (cmbMIRConnectType->currentText() == "Direct") {
				QStringList list;
				list << tr("127.0.0.1") << tr("mir.com") << tr("192.168.126.128");
				QString qhost = QInputDialog::getItem(this, "MIR Host", "Host:", list);
				if (qhost.isEmpty())
					return;
				host = qhost.toStdString();
			}

			_applicationControl->connectMobilePlatform(cmbMIRConnectType->currentText().toStdString(), host);
			btnConnectMIR->setText("Disconnect MIR");
			cmbMIRConnectType->setEnabled(false);
		}
		catch (const std::exception& exp) {
			QMessageBox::critical(this, "Connect MIR", tr("Exception while trying to connect: %1").arg(exp.what()));
		}
	}
	else {
		_applicationControl->disconnectMobilePlatform();
		btnConnectMIR->setText("Connect MIR");
		cmbMIRConnectType->setEnabled(true);
	}
	
	
}

void EnabledRWSPlugin::on_btnConnectUR_clicked()
{
	if (btnConnectUR->text() == "Connect UR") {
		try {
			std::string host = "";
			if (cmbURConnectType->currentText() == "Direct") {
				QStringList list;
				list << tr("169.254.190.100") << tr("192.168.126.130") << tr("192.168.1.5") << tr("192.168.60.4");
				QString qhost = QInputDialog::getItem(this, "UR Host", "Host:", list);
				if (qhost.isEmpty()) {
					return; 
				}
			host = qhost.toStdString();
			} 

			_applicationControl->connectManipulator(cmbURConnectType->currentText().toStdString(), host);
			btnConnectUR->setText("Disconnect UR");
			cmbURConnectType->setEnabled(false);
		}
		catch (const std::exception& exp) {
			QMessageBox::critical(this, "Connect UR", tr("Exception while trying to connect: %1").arg(exp.what()));
		} 
		catch (...) {
			QMessageBox::critical(this, "Connect UR", tr("Unknown Exception while trying to connect"));
		}
	}
	else {
		try {
			_applicationControl->disconnectManipulator();
			ER_LOG_DEBUG("Manipulator disconnected " );
		}
		catch (const std::exception& exp) {
			QMessageBox::critical(this, "Disconnect UR", tr("Exception while trying to disconnect: %1").arg(exp.what()));
		}
		btnConnectUR->setText("Connect UR");
		cmbURConnectType->setEnabled(true);
	}
}


#include <rwlibs/simulation/GLFrameGrabber.hpp>
#include <rwlibs/simulation/SimulatedCamera.hpp>
#include <rw/graphics/SceneViewer.hpp>

using namespace rw::graphics;
using namespace rwlibs::simulation;

void EnabledRWSPlugin::on_btnConnectCamera_clicked()
{
	if (btnConnectCamera->text() == "Connect Camera") {
		try {
			if (cmbCameraConnectType->currentText() == "USBOpenCV") {
				bool ok = true;
				int camid = QInputDialog::getInt(this, "Connect Camera", "Camera Id", 1, 0, 1024, 1, &ok);
				if (ok == false)
					return;

				QString calibfile = QFileDialog::getOpenFileName(this, "Open Camera Calibration File", "../../../data", "*.xml");
				if (calibfile.isEmpty())
					return;
				 
				QString robotcamcalibfile = QFileDialog::getOpenFileName(this, "Open Robot Camera Calibration File", "../../../data", "*.xml");
				if (robotcamcalibfile.isEmpty())
					return;


				_applicationControl->connectCamera(cmbCameraConnectType->currentText().toStdString(), camid, calibfile.toStdString());

				_applicationControl->loadCalibration(robotcamcalibfile.toStdString());
			}
			else if (cmbCameraConnectType->currentText() == "Simulated") {
				Frame* frame = _workcell->findFrame("CameraSimulated");
				//double fovy;
				//int width, height;
				//std::string camId("Camera");
				//std::string camParam = frame->getPropertyMap().get<std::string>(camId);
				//ER_LOG_DEBUG("Camera Params = " << camParam );
				//std::istringstream iss(camParam, std::istringstream::in);
				//iss >> fovy >> width >> height;
				////getRobWorkStudio()->getView()->makeCurrentContext(); TODO
				//GLFrameGrabber::Ptr framegrabber = ownedPtr(new GLFrameGrabber(width, height, fovy, 0.1, 10.0));
				//ER_LOG_DEBUG("a" );
				//SceneViewer::Ptr gldrawer = getRobWorkStudio()->getView()->getSceneViewer();
				//framegrabber->init(gldrawer);
				//
				//ER_LOG_DEBUG("b" );
				//SimulatedCamera *simcam = new SimulatedCamera("SimulatedCamera", fovy, frame, framegrabber);
				//ER_LOG_DEBUG("c" );
				//framegrabber->grab(frame, _applicationControl->getState());
				//sensor = simcam;
				/*simcam->initialize();
				simcam->start();

				simcam->acquire();
				simcam->getImage()->saveAsPPM("ImageDirect.ppm");
*/



				 
				const std::string framename = "CameraSimulated";
				_applicationControl->connectCamera("Simulated", 0, framename);

			}
			btnConnectCamera->setText("Disconnect Camera");
			cmbCameraConnectType->setEnabled(false);
		}
		catch (const std::exception& exp) {
			QMessageBox::critical(this, "Connect Camera", tr("Failed to connect to camera with message: %1").arg(exp.what()));
		}
	}
	else {
		try {
			_applicationControl->disconnectCamera();
		} 
		catch (const std::exception& exp) {
			QMessageBox::critical(this, "Disconnect Camera", tr("Failed to disconnect from camera with message: %1").arg(exp.what()));
		}
		btnConnectCamera->setText("Connect Camera");
		cmbCameraConnectType->setEnabled(true);
	}
}

void EnabledRWSPlugin::on_chkAutoUpdateState_stateChanged(int state)
{
	if (chkAutoUpdateState->isChecked()) {
		_timer.setInterval(spnUpdateRate->value());
		_timer.start();

	}
	else {
		_timer.stop();
	}
}

void EnabledRWSPlugin::timeout()
{
	ER_LOG_DEBUG("TimeOut " );
	//_system->update();
	try {
		_applicationControl->update();
	}
	catch (const std::exception& exp) {
		ER_LOG_DEBUG("Exception: " << exp.what() );
		QMessageBox::critical(this, "Exception", exp.what());
		chkAutoUpdateState->setChecked(false);
		_timer.stop();
		
			
	}
	ER_LOG_DEBUG("Application Control Update finished " );
	//
	//if (_camera != NULL) {
	//	_camera->acquire();
	//	_camera->getLastImage();
	//} 

	getRobWorkStudio()->setState(_applicationControl->getState());
	//if (_system->getMobileManipulator()->getCollisionDetector()->inCollision(_system->getState())) {
	//	ER_LOG_DEBUG("Current state in collision " );
	//}
	//else {
	//	ER_LOG_DEBUG("No in collision" );
	//}
}



void EnabledRWSPlugin::on_actionNewProject_triggered()
{
	//TODO Ask if the previous should be saved

	bool ok = true;
	QString name = QInputDialog::getText(this, tr("New Project"), tr("Project Name: "), QLineEdit::Normal, "Project", &ok);
	if (!ok)
		return;

	on_actionCloseProject_triggered();

	_applicationControl->newProject(name.toStdString());


	//on_actionCloseProject_triggered();

	//_project = ownedPtr(new Project(name.toStdString()));

}

void EnabledRWSPlugin::on_actionCloseProject_triggered()
{
	_applicationControl->closeProject();


	tableEvents->clear();
	tableEvents->setRowCount(0);

	for (EventWidgets& ewidgets : _eventWidgets)
	{
		delete ewidgets.cmbBox;
		delete ewidgets.spnRegister;
		delete ewidgets.spnValue;
	}


	for (ProgramWidget* wdt : _programWidgets) {
		delete wdt;
	}

}

void EnabledRWSPlugin::on_actionLoadProject_triggered()
{
	QString filename = QFileDialog::getOpenFileName(this, tr("Open Project"), ".", "*.xml");
	if (filename.isEmpty())
		return;

	int saveExisting = QMessageBox::warning(this, tr("Load Project"), tr("Save existing project?"), QMessageBox::Yes, QMessageBox::No, QMessageBox::Cancel);
	if (saveExisting == QMessageBox::Yes) {
		on_actionSaveProject_triggered();
	}
	else if (saveExisting == QMessageBox::Cancel) {
		return;
	}
	on_actionCloseProject_triggered();

	_applicationControl->openProject(filename.toStdString());
	

	for (pgm::Event::Ptr event : _applicationControl->getEvents()) {
		addEventToUI(event);
	}
	
	for (Program::Ptr program : _applicationControl->getPrograms()) {
		addProgramToUI(program);
	}

}

void EnabledRWSPlugin::on_actionSaveProject_triggered()
{
	QString filename = QFileDialog::getSaveFileName(this, tr("Save Project"), ".", "*.xml");
	if (filename.isEmpty())
		return;

	_applicationControl->saveProject(filename.toStdString());

}

void EnabledRWSPlugin::on_actionRunProject_triggered()
{
	try {
		ER_LOG_DEBUG("Run Program: " << _applicationControl );
		if (_applicationControl->isProjectRunning() == false) {
			_applicationControl->runProject();
			actionRunProject->setText("Stop Project");
		}
		else {
			_applicationControl->stopProject();
			actionRunProject->setText("Run Project");
		}
	}
	catch (const std::exception& exp) {
		QMessageBox::critical(this, tr("Run Project"), tr("Exception: %1").arg(exp.what()));
	} 
	catch (...) {
		QMessageBox::critical(this, tr("Run Project"), tr("Unknown Exception"));
	}
}


void EnabledRWSPlugin::addEventToUI(pgm::Event::Ptr event) 
{
	int n = tableEvents->rowCount();
	tableEvents->insertRow(n);

	QSpinBox* spnRegister = new QSpinBox();
	spnRegister->setProperty("Event", QVariant(n));
	spnRegister->setRange(1, 256);
	spnRegister->setValue(event->getRegister());
	tableEvents->setCellWidget(n, 0, spnRegister);
	connect(spnRegister, SIGNAL(valueChanged(int)), this, SLOT(eventTableUpdated()));

	QSpinBox* spnValue = new QSpinBox();
	spnValue->setProperty("Event", QVariant(n));
	spnValue->setRange(0, 65535);
	spnValue->setValue(event->getValue());
	tableEvents->setCellWidget(n, 1, spnValue);
	connect(spnValue, SIGNAL(valueChanged(int)), this, SLOT(eventTableUpdated()));

	QComboBox* cmbBox = new QComboBox();
	cmbBox->setProperty("Event", QVariant(n));

	for (Program::Ptr program: _applicationControl->getPrograms()) {
		cmbBox->addItem(program->getName().c_str());
	}

	cmbBox->addItem("No Program");
	int idx = cmbBox->findText(event->getHandlerName().c_str());
	if (idx == -1) {
		if (event->getHandlerName().size() == 0) 
		{
			cmbBox->setCurrentIndex(cmbBox->count() - 1);
		}
		else {
			QMessageBox::critical(this, tr("Error"), tr("No program matching event handler '%1' found").arg(event->getHandlerName().c_str()));
		}
	}
	else {
		cmbBox->setCurrentIndex(idx);
	}
	connect(cmbBox, SIGNAL(currentIndexChanged(int)), this, SLOT(eventTableUpdated()));
	tableEvents->setCellWidget(n, 2, cmbBox);

	_eventWidgets.push_back(EventWidgets(cmbBox, spnRegister, spnValue));

}

void EnabledRWSPlugin::on_actionAddEvent_triggered()
{
	pgm::Event::Ptr event = _applicationControl->addEvent(1, 1, "");

	addEventToUI(event);
}

void EnabledRWSPlugin::eventTableUpdated()
{
	int n = sender()->property("Event").toInt();
	ER_LOG_DEBUG(n << " changed " );
	if (n >= (int)_eventWidgets.size()) {
		QMessageBox::critical(this, "Event Changed ", tr("No entry for event with index %1").arg(n));
		return;
	} 
	pgm::Event::Ptr evt = _applicationControl->getEvents().at(n);
	evt->setRegister(_eventWidgets[n].spnRegister->value());
	evt->setValue(_eventWidgets[n].spnValue->value());
	evt->setHandlerName(_eventWidgets[n].cmbBox->currentText().toStdString());
}

void EnabledRWSPlugin::on_actionRemoveEvent_triggered()
{
	QMessageBox::information(this, "", "Remove Event");
}

void EnabledRWSPlugin::on_actionNewProgram_triggered() 
{
	bool finish = true;
	std::string programname = "";
	do {
		bool ok = true;
		QString name = QInputDialog::getText(this, tr("New Program"), tr("Program Name: "), QLineEdit::Normal, "Program", &ok);
		if (!ok)
			return;
		if (_applicationControl->getProject()->hasHandlerName(name.toStdString())) {
			QMessageBox::critical(this, tr("New Program"), tr("Name '%1' already exists").arg(name));
			finish = false;
		}
		else {
			finish = true;
			programname = name.toStdString();
		}
	} while (finish == false);

	Program::Ptr program = ownedPtr(new Program(programname));
	_applicationControl->getProject()->addProgram(program);
	addProgramToUI(program);
}



void EnabledRWSPlugin::on_actionImportProgram_triggered()
{
	QMessageBox::information(this, "", "Import Program");
}



void EnabledRWSPlugin::addProgramToUI(Program::Ptr program) 
{
	ProgramWidget* programWidget = new ProgramWidget(tabPrograms);
	//	MissionEventWidget* missionEventWidget = new MissionEventWidget(NULL);
	tabPrograms->addTab(programWidget, program->getName().c_str());
	programWidget->initialize(program, _applicationControl);

	_programWidgets.push_back(programWidget);

	std::string programname = program->getName();
	for (EventWidgets& ewidgets : _eventWidgets)
	{
		ewidgets.cmbBox->addItem(programname.c_str());
	}
}
