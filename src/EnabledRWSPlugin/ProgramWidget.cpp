#include "ProgramWidget.hpp"

#include <Common/DataType.hpp>
#include <Common/UserType.hpp>
#include <Common/InputType.hpp>
#include <Common/EntityRepository.hpp>
#include <Common/EntityFactory.hpp>
#include <iostream>
#include <Serialization/InputArchiveXML.hpp>
#include <Serialization/OutputArchiveXML.hpp>

#include <QMessageBox>
#include <QInputDialog>
#include <QToolBar>

using namespace pgm;
using namespace exm;
using namespace mvc;
using namespace er_serialization;
using namespace er_common;
using namespace rw::common;
using namespace rw::math;



ProgramWidget::ProgramWidget(QWidget* parent):
	QWidget(parent)
{
	ER_LOG_DEBUG("ProgramWidget Constructor " );
    setupUi(this);
	QToolBar* toolbar = new QToolBar(this);
	QVBoxLayout* layout = new QVBoxLayout(wdtToolBarPlaceHolder);
	wdtToolBarPlaceHolder->layout()->addWidget(toolbar);
	toolbar->addAction(actionAddProgramItem);
	toolbar->addAction(actionRemoveProgramItem);
	toolbar->addAction(actionRunProgram);

	_buttonGroup = new QButtonGroup(this);
	_buttonGroup->setExclusive(true);
	connect(_buttonGroup, SIGNAL(buttonClicked(int)), this, SLOT(itemSelected(int)));
	
	QVBoxLayout* wdtItemsLayout = new QVBoxLayout(wdtItems);

	ER_LOG_DEBUG("ProgramWidget Constructor Finished" );
} 


class UpdatesEnabledHelper
{
	QWidget* m_parentWidget;
public:
	UpdatesEnabledHelper(QWidget* parentWidget) : m_parentWidget(parentWidget) { parentWidget->setUpdatesEnabled(false); }
	~UpdatesEnabledHelper() { m_parentWidget->setUpdatesEnabled(true); }
};

ProgramWidget::~ProgramWidget()
{ 
	UpdatesEnabledHelper helper(this);
	qDeleteAll(this->findChildren<QWidget*>("", Qt::FindDirectChildrenOnly));
}


void ProgramWidget::on_actionAddProgramItem_triggered()
{
	std::vector<std::string> ids = EntityRepository<Instruction>::getIds();
	QStringList stringlist;
	for (std::string& id : ids) {
		stringlist.push_back(id.c_str());
	}
	bool ok = true;
	QString id = QInputDialog::getItem(this, tr("Add"), tr("Program Item"), stringlist, 0, false, &ok);
	if (ok) {
		try {
			Instruction::Ptr instruction = EntityRepository<Instruction>::make(id.toStdString());
			ER_LOG_DEBUG("Instruction  " << instruction << "  program " << _program );
			_program->addInstruction(instruction);
			ER_LOG_DEBUG("Instrucito Added" );
			initializeView();
			ER_LOG_DEBUG("View initalized " );
		}
		catch (const std::exception& exp) {
			QMessageBox::critical(this, tr("Exception"), tr("Failed to construct ProgramItem with Id '%1' with exception: %2").arg(id).arg(exp.what()));
		}
	}
}

void ProgramWidget::on_actionRemoveProgramItem_triggered()
{
	QMessageBox::information(this, "Remove", "Remove Program Item ");
}

void ProgramWidget::initialize(Program::Ptr program, ApplicationControl::Ptr applicationControl)
{

	//_program = ownedPtr(new Program("Empty Program"));
	_program = program;
	_applicationControl = applicationControl;
	initializeView();
}

void ProgramWidget::initializeView() {
	//Start by removing old items
	ER_LOG_DEBUG("A" );
	for (ProgramItemWidget* item : _programItemWidgets) {
		_buttonGroup->removeButton(item);
		wdtItems->layout()->removeWidget(item);
		delete item;
	}
	_programItemWidgets.clear();
	ER_LOG_DEBUG("A" << _program->getInstructions().size());
	for (Instruction::Ptr item : _program->getInstructions())
	{
		ER_LOG_DEBUG("c " <<item);
		ProgramItemWidget* widget = new ProgramItemWidget(item, this);
		_buttonGroup->addButton(widget, _programItemWidgets.size());
		ER_LOG_DEBUG("c " << wdtItems->layout());
		wdtItems->layout()->addWidget(widget);
		ER_LOG_DEBUG("c " << item );
		_programItemWidgets.push_back(widget);
	}
	ER_LOG_DEBUG("D" );
}

void ProgramWidget::itemSelected(int idx)
{
	if (idx < 0 && idx >= (int)_programItemWidgets.size()) 
	{
		QMessageBox::critical(this, tr("ProgramWidget"), tr("Invalid Index: %1").arg(idx));
		return;
	}
	wdtSettingsList->setSettings(&(_programItemWidgets[idx]->getInstruction()->getSettings()));
}

void ProgramWidget::loadProgram(const std::string& filename)
{
	ER_LOG_DEBUG("Load Program: " << filename );
	try { 
		Program::Ptr program = ownedPtr(new Program());
		er_serialization::InputArchiveXML inxml(filename);
		inxml.initialize();
		program->read(inxml, "");
		_program = program;
		initializeView();
	}
	catch (const std::exception& exp) {
		RW_THROW("Unabled to load program: " << exp.what());
	}
	catch (...) {
		RW_THROW("Caught unknown exception trying to load program");
	}
}

void ProgramWidget::saveProgram(const std::string& filename)
{
	if (_program == NULL)
	{
		RW_THROW("No program to save");
	}
	ER_LOG_DEBUG("Save Program: " << filename );
	try {
		er_serialization::OutputArchiveXML outxml(filename);
		_program->write(outxml, "");
		outxml.finalize();
	}
	catch (const std::exception& exp) {
		RW_THROW("Unabled to save program: " << exp.what());
	}
}


void ProgramWidget::on_actionRunProgram_triggered()
{
	
	ER_LOG_DEBUG("Ready to execute program " << _program );
	//_executionManager = ownedPtr(new ExecutionManager(System::getInstance()));
	try {
		_applicationControl->executeProgram(_program->getName());
		//_executionManager->execute(_program, false);
	}
	catch (const std::exception& exp) {
		QMessageBox::critical(this, "Program Execution", tr("Failed to execute program with exception: %1").arg(exp.what()));
	}
	catch (...) {
		QMessageBox::critical(this, "Unknown Exception", tr("Failed to execution program with unknowen exception"));
	}
	//_program->execute(_executionManager);

	ER_LOG_DEBUG("Program Executed " );

}