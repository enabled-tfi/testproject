/* */
#ifndef QTDIALOGS_HPP
#define QTDIALOGS_HPP


#include <rw/common/Ptr.hpp>
#include <ApplicationControl/UIDialogs.hpp>

#include <QWidget>
	
/**
 * General interface for actions 
 */
class QTDialogs: public mvc::UIDialogs
{
public:

	QTDialogs(QWidget* widget);

	virtual void infoBox(const std::string& title, const std::string& message);
	virtual void warningBox(const std::string& title, const std::string& message);
	virtual void criticalBox(const std::string& title, const std::string& message);

	virtual std::string getOpenFileName(const std::string& title, const std::string& directory, const std::string& filter);
	virtual std::string getSaveFileName(const std::string& title, const std::string& directory, const std::string& filter);


protected:
	QTDialogs();
private:
	QWidget* _parent;
	static UIDialogs::Ptr _instance;

	

};


#endif //#ifndef MVC_HMI_HPP
