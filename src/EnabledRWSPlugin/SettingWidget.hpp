#ifndef SETTINGWIDGET_HPP
#define SETTINGWIDGET_HPP

#include "ui_SettingWidget.h" 
#include <QPushButton>
#include <Common/Setting.hpp>
#include <Common/System.hpp>

class SettingWidget : public QPushButton, private Ui::SettingWidget {
	Q_OBJECT
public:
	SettingWidget(er_common::SettingBase::Ptr setting, QWidget* parent);
	virtual ~SettingWidget();

	er_common::SettingBase::Ptr getSetting();

private slots:
	void settingClicked();
	bool isSupported() const;

private:
	er_common::SettingBase::Ptr _setting;	
};

#endif /*SETTINGWIDGET_HPP*/

