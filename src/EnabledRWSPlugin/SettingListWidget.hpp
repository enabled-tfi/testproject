#ifndef SETTINGLISTWIDGET_HPP
#define SETTINGLISTWIDGET_HPP

#include "ui_SettingListWidget.h" 
#include "SettingWidget.hpp"
#include <Common/Settings.hpp>

class SettingListWidget : public QWidget, private Ui::SettingListWidget {
	Q_OBJECT
public:
	SettingListWidget(QWidget* parent);
	virtual ~SettingListWidget();

	void setSettings(er_common::Settings::Ptr settings);

private:
	er_common::Settings::Ptr _settings;
	QButtonGroup* _buttonGroup;
	std::vector<SettingWidget*> _settingWidgets;
};

#endif /*SETTINGLISTWIDGET_HPP*/
