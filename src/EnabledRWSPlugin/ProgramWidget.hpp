#ifndef PROGRAMWIDGET_HPP
#define PROGRAMWIDGET_HPP

#include "ui_ProgramWidget.h" 
#include "ProgramItemWidget.hpp"
#include "SettingListWidget.hpp"
#include <ProgramModel/Program.hpp>
#include <Common/System.hpp>
#include <ApplicationLayer/ApplicationControl.hpp>

class ProgramWidget: public QWidget, private Ui::ProgramWidget {
	Q_OBJECT
public:
	ProgramWidget(QWidget* parent);
	virtual ~ProgramWidget();

	void setSettingListWidget(SettingListWidget* settingListWidget);
	void initialize(pgm::Program::Ptr program, mvc::ApplicationControl::Ptr applicationControl);
public slots:
	void loadProgram(const std::string& filename);
	void saveProgram(const std::string& filename);

private slots: 
	void itemSelected(int idx);
	
	void on_actionAddProgramItem_triggered();
	void on_actionRemoveProgramItem_triggered();
	void on_actionRunProgram_triggered();

private:
	pgm::Program::Ptr _program;
	mvc::ApplicationControl::Ptr _applicationControl;
	//exm::ExecutionManager::Ptr _executionManager;
	std::vector<ProgramItemWidget*> _programItemWidgets;
	QButtonGroup* _buttonGroup;
	void initializeView();

};

#endif /*PROGRAMWIDGET_HPP*/
