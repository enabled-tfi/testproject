#ifndef MIRWIDGET_HPP
#define MIRWIDGET_HPP

#include "ui_MIRWidget.h"
#include <Common/System.hpp>

//#include <QWebEngineView>
//#include <QWebEngineWidgets>

class MIRWidget: public QWidget, private Ui::MIRWidget {
	Q_OBJECT
public:
	MIRWidget(QWidget* parent);
	virtual ~MIRWidget();

	void initialize();

private slots:
	void on_actionBrowseForward_triggered();
	void on_actionBrowseBack_triggered();

private:
	//QWebEngineView* _webview;
};

#endif /*MIRWIDGET_HPP*/
