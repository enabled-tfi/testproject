#include "SettingWidget.hpp"

#include "SettingEditors/QNumericEditor.hpp"
#include "SettingEditors/CartesianTargetEditor.hpp"
#include "SettingEditors/QTargetEditor.hpp"
#include "SettingEditors/QPathEditor.hpp"
#include <iostream>
#include <Serialization/InputArchiveXML.hpp>
#include <Serialization/OutputArchiveXML.hpp>
#include <Common/DataType.hpp>

#include <QInputDialog>
#include <QDialogButtonBox>
using namespace pgm;
using namespace er_serialization;
using namespace er_common;
using namespace rw::common;
using namespace rw::math;
using namespace rw::trajectory;

SettingWidget::SettingWidget(SettingBase::Ptr setting,  QWidget* parent):
	QPushButton(parent),
	_setting(setting)
{
    setupUi(this);
	setCheckable(true);
	if (setting != NULL) {
		lblTitle->setText(setting->getIdentifier().c_str());
		setToolTip(setting->getDescription().c_str());
		lblStatus->setMaximumSize(lblTitle->height(), lblTitle->height());
		lblStatus->setMinimumSize(lblTitle->height(), lblTitle->height());
	}
	connect(this, SIGNAL(clicked()), this, SLOT(settingClicked()));
	
	if (isSupported() == false) {
		setVisible(false);
	}

} 


SettingWidget::~SettingWidget()
{
}

SettingBase::Ptr SettingWidget::getSetting()
{
	return _setting;
}

bool SettingWidget::isSupported() const {
	switch (_setting->getDataType().getId()) {
	case DataType::Float:
		return true;
	case DataType::Double:
		return true;
	case DataType::Int:
		return true;
	case DataType::String:
		return true;
	case DataType::Q:
		return true;
	case DataType::Target:
		return true;
	case DataType::QPath:
		return true;
	default:
		return false;
	}

	return false;
}


void SettingWidget::settingClicked()
{
	switch (_setting->getDataType().getId()) {
	case DataType::Float:
	{
		Setting<float>::Ptr setting = _setting.cast<Setting<float> >();
		bool ok = true;
		double res = QInputDialog::getDouble(this, tr("Setting"), setting->getIdentifier().c_str(), setting->getValue(), setting->getRange().first, setting->getRange().second, 1, &ok);
		if (ok)
			setting->setValue((float)res);
		break;
	}
	case DataType::Double:
	{
		Setting<double>::Ptr setting = _setting.cast<Setting<double> >();
		bool ok = true;
		double res = QInputDialog::getDouble(this, tr("Setting"), setting->getIdentifier().c_str(), setting->getValue(), setting->getRange().first, setting->getRange().second, 4, &ok);
		if (ok)
			setting->setValue((float)res);
		break;
	}
	case DataType::Int:
	{
		Setting<int>::Ptr setting = _setting.cast<Setting<int> >();
		bool ok = true;
		int res = QInputDialog::getInt(this, tr("Setting"), setting->getIdentifier().c_str(), setting->getValue(), setting->getRange().first, setting->getRange().second, 1, &ok);
		if (ok)
			setting->setValue(res);
		break;
	}
	case DataType::String:
	{
		Setting<std::string>::Ptr setting = _setting.cast<Setting<std::string> >();
		bool ok = true;
		QString res = QInputDialog::getText(this, tr("Setting"), setting->getIdentifier().c_str(), QLineEdit::Normal, setting->getValue().c_str(), &ok);
		if (ok)
			setting->setValue(res.toStdString());
		break;
	}
	case DataType::Q:
	{
		ER_LOG_DEBUG("Data Type: Q" );
		Setting<Q>::Ptr setting = _setting.cast<Setting<Q> >();
		QDialog* dialog = new QDialog();
		dialog->setWindowTitle(tr("%1").arg(setting->getIdentifier().c_str()));
		dialog->setModal(true);
		dialog->setLayout(new QVBoxLayout());

		Editor* editor;

		if (setting->getInputType().getId() == InputType::Numeric)
		{
			editor = new QNumericEditor(setting, dialog);
		}
		else 
		{
			editor = new QTargetEditor(setting, dialog);
		}
		dialog->layout()->addWidget(boost::get<QWidget*>(editor));
		
		dialog->adjustSize();


		QDialogButtonBox* buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);		
		connect(buttonBox, SIGNAL(accepted()), dialog, SLOT(accept()));
		connect(buttonBox, SIGNAL(rejected()), dialog, SLOT(reject()));
		dialog->layout()->addWidget(buttonBox);
		if (dialog->exec() == QDialog::Accepted) {
			editor->writeChanges();
		}
			
		delete editor;
		delete buttonBox;
		delete dialog;
		break;
	}
	case DataType::Target:
	{
		ER_LOG_DEBUG("Data Type: Target " <<_setting->getIdentifier());
		Setting<Target>::Ptr setting = _setting.cast<Setting<Target> >();
		QDialog* dialog = new QDialog();
		dialog->setWindowTitle(tr("%1").arg(setting->getIdentifier().c_str()));
		dialog->setModal(true);
		ER_LOG_DEBUG("A" );
		CartesianTargetEditor* editor = new CartesianTargetEditor(setting, dialog);
		ER_LOG_DEBUG("B" );
		dialog->setLayout(new QVBoxLayout());
		dialog->layout()->addWidget(editor);
		QDialogButtonBox* buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
		connect(buttonBox, SIGNAL(accepted()), dialog, SLOT(accept()));
		connect(buttonBox, SIGNAL(rejected()), dialog, SLOT(reject()));
		dialog->layout()->addWidget(buttonBox);
		if (dialog->exec() == QDialog::Accepted) {
			editor->writeChanges();
			ER_LOG_DEBUG(setting->getValue().refTtarget() << setting->getValue().getReferenceFrameName());
		}

		delete editor;
		delete buttonBox;
		delete dialog;
		break;
	}
	case DataType::QPath:
	{
		ER_LOG_DEBUG("Data Type: QPath" );
		Setting<QPath>::Ptr setting = _setting.cast<Setting<QPath> >();
		QDialog* dialog = new QDialog();
		dialog->setWindowTitle(tr("%1").arg(setting->getIdentifier().c_str()));
		dialog->setModal(true);
		dialog->setLayout(new QVBoxLayout());

		Editor* editor;
		if (setting->getValue().size() == 0) {
			setting->getValue().push_back(Q::zero(6));
		}

		editor = new QPathEditor(setting, this);

		dialog->layout()->addWidget(boost::get<QWidget*>(editor));


		QDialogButtonBox* buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
		connect(buttonBox, SIGNAL(accepted()), dialog, SLOT(accept()));
		connect(buttonBox, SIGNAL(rejected()), dialog, SLOT(reject()));
		dialog->layout()->addWidget(buttonBox);
		if (dialog->exec() == QDialog::Accepted) {
			editor->writeChanges();
		}

		delete editor;
		delete buttonBox;
		delete dialog;
		break;

	}

	}

}