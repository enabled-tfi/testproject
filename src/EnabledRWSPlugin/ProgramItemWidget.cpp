#include "ProgramItemWidget.hpp"

#include <iostream>
#include <Serialization/InputArchiveXML.hpp>
#include <Serialization/OutputArchiveXML.hpp>

using namespace exm;
using namespace er_serialization;

using namespace rw::common;


ProgramItemWidget::ProgramItemWidget(Instruction::Ptr item, QWidget* parent):
	QPushButton(parent),
	_instruction(item)	
{
    setupUi(this);
	setCheckable(true);
	if (item != NULL) {
		lblTitle->setText(item->getName().c_str());
		setToolTip(item->getDescription().c_str());
		lblStatus->setMaximumSize(lblTitle->height(), lblTitle->height());
		lblStatus->setMinimumSize(lblTitle->height(), lblTitle->height());		
	}
} 


ProgramItemWidget::~ProgramItemWidget()
{
}

Instruction::Ptr ProgramItemWidget::getInstruction()
{
	return _instruction;
}