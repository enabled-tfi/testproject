#include "QTDialogs.hpp"
#include <QMessageBox>
#include <QFileDialog>
	

QTDialogs::QTDialogs(QWidget* parent) :
	_parent(parent)
{

}

void QTDialogs::infoBox(const std::string& title, const std::string& message)
{
	QMessageBox::information(_parent, title.c_str(), message.c_str());
}

void QTDialogs::warningBox(const std::string& title, const std::string& message)
{
	QMessageBox::warning(_parent, title.c_str(), message.c_str());
}

void QTDialogs::criticalBox(const std::string& title, const std::string& message)
{
	QMessageBox::critical(_parent, title.c_str(), message.c_str());
}

std::string QTDialogs::getOpenFileName(const std::string& title, const std::string& directory, const std::string& filter)
{
	QString res = QFileDialog::getOpenFileName(_parent, title.c_str(), directory.c_str(), filter.c_str());
	return res.toStdString();
}

std::string QTDialogs::getSaveFileName(const std::string& title, const std::string& directory, const std::string& filter)
{
	QString res = QFileDialog::getSaveFileName(_parent, title.c_str(), directory.c_str(), filter.c_str());
	return res.toStdString();
}
