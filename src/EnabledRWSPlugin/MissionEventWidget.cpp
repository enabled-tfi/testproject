#include "MissionEventWidget.hpp"

#include <QFileDialog>
#include <QMessageBox>
#include <QToolBar>

using namespace er_common;
using namespace mvc;


MissionEventWidget::MissionEventWidget(QWidget* parent):
	QWidget(parent)
{
    setupUi(this);
	//wdtProgram->setSettingListWidget(wdtSettingList);
} 


MissionEventWidget::~MissionEventWidget()
{
}

void MissionEventWidget::initialize(pgm::Program::Ptr program, ApplicationControl::Ptr applicationControl)
{
	_applicationControl = applicationControl;
	wdtProgram->initialize(program, applicationControl);
}
