#include "MIRWidget.hpp"

#include <QFileDialog>
#include <QMessageBox>
#include <QToolBar>

using namespace er_common;


MIRWidget::MIRWidget(QWidget* parent):
	QWidget(parent)
{
    setupUi(this);
	QToolBar* toolbar = new QToolBar(this);
	//QVBoxLayout* layout = new QVBoxLayout(toolbarPlaceHolder);
	toolbarPlaceHolder->layout()->addWidget(toolbar);
	toolbar->addAction(actionBrowseBack);
	toolbar->addAction(actionBrowseForward);

	//_webview = new QWebEngineView(wdtBrowser);

	QVBoxLayout* layout2 = new QVBoxLayout(wdtBrowser);
	//wdtBrowser->layout()->addWidget(_webview);
	//_webview->load(QUrl("http://192.168.126.128/index.php"));
}


MIRWidget::~MIRWidget()
{
}

void MIRWidget::initialize()
{

}

void MIRWidget::on_actionBrowseForward_triggered()
{

}

void MIRWidget::on_actionBrowseBack_triggered()
{

}
