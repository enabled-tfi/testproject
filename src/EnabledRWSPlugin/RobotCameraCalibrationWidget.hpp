#ifndef ROBOTCAMERACALIBRATIONWIDGET_HPP
#define ROBOTCAMERACALIBRATIONWIDGET_HPP

#include "ui_RobotCameraCalibrationWidget.h" 
#include <Common/System.hpp>
#include <ProgramModel/Program.hpp>
#include <ApplicationLayer/ApplicationControl.hpp>

class RobotCameraCalibrationWidget: public QWidget, private Ui::RobotCameraCalibrationWidget {
	Q_OBJECT
public:
	RobotCameraCalibrationWidget(QWidget* parent);
	virtual ~RobotCameraCalibrationWidget();

	void initialize(pgm::Program::Ptr program, mvc::ApplicationControl::Ptr applicationControl);

private slots:
	void on_btnLoadConfigurationList_clicked();
	void on_btnRecordCalibrationData_clicked();


private:
	mvc::ApplicationControl::Ptr _applicationControl;
};

#endif /*ROBOTCAMERACALIBRATIONWIDGET_HPP*/
