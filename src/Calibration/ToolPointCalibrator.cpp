#include "ToolPointCalibrator.hpp" 


#include <rw/math/LinearAlgebra.hpp>

using namespace er_calibration; 
using namespace rw::math;





std::pair<rw::math::Vector3D<>, double> ToolPointCalibration::calibrate(std::vector<Transform3D<> >& baseTends) 
{
	const size_t N = baseTends.size();
	if (N < 3) {
		RW_THROW("Calibration of tool offset requires at least three measurements");
	}


	Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> aMatrix(3*N, 3);
	Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> bVector(3*N,1);
	for (size_t i = 0; i<N; i++) {
		Transform3D<> ta = baseTends[i];
		Transform3D<> tb = baseTends[(i+1)%N];
		//ER_LOG_DEBUG("ta = "<<ta);

		Rotation3D<>::EigenMatrix3x3 diffR = ta.R().e() - tb.R().e();
		Vector3D<> diffP = tb.P() - ta.P();

		for (size_t j = 0; j<3; j++) {
			for (size_t k = 0; k<3; k++) {
				aMatrix(3*i+j,k) = diffR(j,k);
			}
			bVector(3*i+j,0) = diffP(j);
		}
	}

	Eigen::MatrixXd aInv = LinearAlgebra::pseudoInverse(aMatrix);
	Eigen::Vector3d p = aInv*bVector;

	Eigen::VectorXd residuals = aMatrix*p-bVector;
    RW_LOG_DEBUG("ToolPointCalibration: "<<residuals);
 //   RW_LOG_INCREASE_TAB_LEVEL();
    RW_LOG_DEBUG("Residuals: " << residuals);
	double residual = residuals.maxCoeff();
    Vector3D<> result(p);

    RW_LOG_DEBUG("Residual Max: " << residual);
    RW_LOG_DEBUG("TCP Point: " << result);
//    RW_LOG_DECREASE_TAB_LEVEL();


    return std::make_pair(result, residual);
}
