/* */
#ifndef ER_CALIBRATION_TOOLPOINTCALIBRATION_HPP
#define ER_CALIBRATION_TOOLPOINTCALIBRATION_HPP

#include <rw/math/Vector3D.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/common/PropertyMap.hpp>


namespace er_calibration {
	
/**
 * Provides a method for calibrating the tool position.
 *
 * As input the method requires a set of base to end transformations of the robot, where the TCP position are touching a fixed point in space.
 */
class ToolPointCalibration
{
public:
    /**     
     * @brief Computes the position of the TCP relative to the robot end.
     * @param baseTends [in] A list of no less than 3 (preferably more) baseTend transformations.
     * @return Pair containing the end to TCP translation and the max residual.
     */
	static std::pair<rw::math::Vector3D<>, double> calibrate(std::vector<rw::math::Transform3D<> >& baseTends);
private:
};

} //end namespace

#endif //#ifndef ER_CALIBRATION_TOOLPOINTCALIBRATION_HPP
