/* */
#ifndef ER_CALIBRATION_MOBILEMANIPULATORCALIBRATION_HPP
#define ER_CALIBRATION_MOBILEMANIPULATORCALIBRATION_HPP


#include <Common/Serializable.hpp>


#include <rw/math/Transform3D.hpp>
#include <rw/common/PropertyMap.hpp>
#include <rw/models.hpp>
#include <rw/kinematics.hpp>

namespace er_calibration {
	


/**
 * General container for storing calibrations of frames
 */
class MobileManipulatorCalibration: public er_common::Serializable
{
public:
    /** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<MobileManipulatorCalibration> Ptr;

    /**
     * @brief Constructs calibration with a given workcell in which frames are to be found
     */
    MobileManipulatorCalibration(rw::models::WorkCell::Ptr workcell);

    /**
     * @brief Destructor
     */
	~MobileManipulatorCalibration();

    /**
     * @brief Add a calibration frame. Initialize both original and calibration transform with the current transform
     * @param frame [in] Frame for which to hold the calibration
     */
    void addCalibrationFrame(rw::kinematics::FixedFrame::Ptr frame);


    /**
    * @brief Add a calibration frame with a given transform
    * @param frame [in] Frame for which to hold the calibration
    * @param transform [in] The calibration transform
    */
    void addCalibrationFrame(rw::kinematics::FixedFrame::Ptr frame, const rw::math::Transform3D<>& transform);

    /** 
     * @brief Sets the calibrated transform for a frame.
     *
     * Throws an exception if the frame cannot be found.
     * @param frame [in] Frame for which to set the transform
     * @param transform [in] The calibrated trnsform
     */
    void setCalibratedTransformForFrame(rw::kinematics::FixedFrame::Ptr frame, const rw::math::Transform3D<>& transform);

    /**
     * @brief Applies the calibrated transforms to all frames
     */
    void apply();

    /**
     * @brief Reverts the calibrated transform from all frames
     */
    void revert();


	/**
	* @copydoc Serializable::read
	*/
	virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

	/**
	* @copydoc Serializable::write
	*/
	virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;



private:
	rw::models::WorkCell::Ptr _workcell;
	class FrameCalibration: public er_common::Serializable {
	public:
		rw::kinematics::FixedFrame::Ptr _frame;
		std::string _frameName;
        rw::math::Transform3D<> _revertTransform;
        rw::math::Transform3D<> _calibratedTransform;

		/**
		* @copydoc Serializable::read
		*/
		virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

		/**
		* @copydoc Serializable::write
		*/
		virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


	
	};
    
    std::map<rw::kinematics::FixedFrame::Ptr, FrameCalibration> _calibrations;

};

} //end namespace

#endif //#ifndef ER_CALIBRATION_MOBILEMANIPULATORCALIBRATION_HPP
