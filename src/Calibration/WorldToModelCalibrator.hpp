/* */
#ifndef ER_CALIBRATION_WORLDTOMODELCALIBRATION_HPP
#define ER_CALIBRATION_WORLDTOMODELCALIBRATION_HPP


#include <rw/math/Transform3D.hpp>
#include <rw/common/PropertyMap.hpp>

namespace er_calibration {
	
/**
* Computes the transform minimizing: sum_i(T*p_i-p'_i), where p_i and p'_i is a pair of points corresponding
* to the same point in real world.
*/
class WorldToModelCalibration
{
public:

    typedef std::pair<rw::math::Vector3D<>, rw::math::Vector3D<> > PointPair;

	/**
	 * @brief Performs point pair registration
	 *
	 * By default it uses the SVD, but in case SVD produces an invalid rotation matrix, it calls the Quaternion based method
	 *
	 * @param pointPairs [in] Point pairs p_i and p'_i to register. 
	 */
	static rw::math::Transform3D<> pointPairRegistration(const std::vector<PointPair>& pointPairs);

	/**
	 * @brief Perform registration of point pairs using SVD based method.
	 * @param pointPairs [in] Point pairs p_i and p'_i to register. 
	 */
	static rw::math::Transform3D<> pointPairRegistrationSVD(const std::vector<PointPair>& pointPairs );

	/**
	* @brief Perform registration of point pairs using Quaternion based method.
	* @param pointPairs [in] Point pairs p_i and p'_i to register.
	*/
	static rw::math::Transform3D<> pointPairRegistrationQuaternion(const std::vector<PointPair>& pointPairs);



	
};

} //end namespace

#endif //#ifndef ER_CALIBRATION_WORLDTOMODELCALIBRATION_HPP
