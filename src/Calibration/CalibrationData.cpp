#include "CalibrationData.hpp" 

#include <Common/InputArchive.hpp>
#include <Common/OutputArchive.hpp>
#include <Common/Logging.hpp>

using namespace er_calibration; 
using namespace rw::kinematics;
using namespace rw::models;
using namespace rw::math;


MobileManipulatorCalibration::MobileManipulatorCalibration(WorkCell::Ptr workcell):
	_workcell(workcell)
{
    
}

MobileManipulatorCalibration::~MobileManipulatorCalibration()
{
}


void MobileManipulatorCalibration::addCalibrationFrame(rw::kinematics::FixedFrame::Ptr frame)
{
    FrameCalibration calibration;
    calibration._frame = frame;
    calibration._revertTransform = frame->getFixedTransform();
    calibration._calibratedTransform = frame->getFixedTransform();
    _calibrations[frame] = calibration;
}

void MobileManipulatorCalibration::addCalibrationFrame(rw::kinematics::FixedFrame::Ptr frame, const rw::math::Transform3D<>& transform)
{
    FrameCalibration calibration;
    calibration._frame = frame;
    calibration._revertTransform = frame->getFixedTransform();
    calibration._calibratedTransform = transform;
    _calibrations[frame] = calibration;
}

void MobileManipulatorCalibration::setCalibratedTransformForFrame(rw::kinematics::FixedFrame::Ptr frame, const rw::math::Transform3D<>& transform)
{
    if (_calibrations.find(frame) == _calibrations.end()) {
        RW_THROW("No calibration for frame "<<frame->getName()<<" has been setup");
    }
    _calibrations[frame]._calibratedTransform = transform;
}



void MobileManipulatorCalibration::apply()
{
    for (std::map<FixedFrame::Ptr, FrameCalibration>::iterator it = _calibrations.begin(); it != _calibrations.end(); ++it) {
		ER_LOG_DEBUG("Set Fixed transform " << (*it).first->getName() << " " << (*it).second._calibratedTransform );
        (*it).first->setTransform((*it).second._calibratedTransform);
    }
}


void MobileManipulatorCalibration::revert()
{
    for (std::map<FixedFrame::Ptr, FrameCalibration>::iterator it = _calibrations.begin(); it != _calibrations.end(); ++it) {
        (*it).first->setTransform((*it).second._revertTransform);
    }

}



/**
* @copydoc Serializable::read
*/
void MobileManipulatorCalibration::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
	ER_LOG_DEBUG("MobileManipulatorCalibration::read" );
	iarchive.readEnterScope("MobileManipulatorCalibration");
	
	std::vector<er_serialization::InputArchive::Ptr> subarchives = iarchive.getSubArchivesForId("FrameCalibration");
	for (er_serialization::InputArchive::Ptr arc : subarchives)
	{
		FrameCalibration calib;
		calib.read(*arc, "FrameCalibration");
		FixedFrame::Ptr frame = _workcell->findFrame<FixedFrame>(calib._frameName);
		calib._frame = frame;
		_calibrations[frame] = calib;
	}
	iarchive.readLeaveScope("MobileManipulatorCalibration");
}

/**
* @copydoc Serializable::write
*/
void MobileManipulatorCalibration::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	oarchive.writeEnterScope("MobileManipulatorCalibration");

	typedef std::map<rw::kinematics::FixedFrame::Ptr, FrameCalibration> CalibrationMap;
	for (CalibrationMap::const_iterator it = _calibrations.begin(); it != _calibrations.end(); ++it) {
		(*it).second.write(oarchive, "FrameCalibration");
	}
	oarchive.writeLeaveScope("MobileManipulatorCalibration");
}


/**
* @copydoc Serializable::read
*/
void MobileManipulatorCalibration::FrameCalibration::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
	iarchive.readEnterScope("FrameCalibration");
	_frameName = iarchive.read<std::string>("Frame");
	_calibratedTransform = iarchive.read<Transform3D<> >("CalibratedTransform");
	iarchive.readLeaveScope("FrameCalibration");
}

/**
* @copydoc Serializable::write
*/
void MobileManipulatorCalibration::FrameCalibration::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	oarchive.writeEnterScope("FrameCalibration");
	oarchive.write(_frame->getName(), "Frame");
	oarchive.write(_calibratedTransform, "CalibratedTransform");
	oarchive.writeLeaveScope("FrameCalibration");

}
