#ifndef MVC_APPLICATIONCONTROL_HPP
#define MVC_APPLICATIONCONTROL_HPP

#include <rw/common/Ptr.hpp>
#include <rw/models/WorkCell.hpp>

#include <ProgramModel/Project.hpp>
#include <ProgramModel/ProjectController.hpp>

//#include <ExecutionModel/ExecutionManager.hpp>
#include <rws/RobWorkStudio.hpp>
//#include "UIDialogs.hpp"

namespace mvc {

	class ApplicationControl
	{
	public:
		//! @brief Smart pointer type to this class
		typedef rw::common::Ptr<ApplicationControl> Ptr;

		/**
		 * @brief Constructs Setting.
		 * @param identifier [in] identifier
		 * @param description [in] description
		 * @param type [in] data type
		 * @param value [in] value
		 */
		ApplicationControl(rw::models::WorkCell::Ptr workcell, rws::RobWorkStudio* rwstudio);

		ApplicationControl(rws::RobWorkStudio* rwstudio);


		/**
		 * @brief Destroys Setting
		 * If the data type is a pointer, then what it points to is not deleted
		 */
		virtual ~ApplicationControl();

		void loadSystem(const std::string& filename, bool connect);
		void saveSystem(const std::string& filename);


		void newProject(const std::string& name);

		void openProject(const std::string& filename);

		void saveProject(const std::string& filename);

		void closeProject();

		void runProject(); 
		bool isProjectRunning();
		void stopProject();

		pgm::Project::Ptr getProject();

		std::vector<pgm::Event::Ptr> getEvents();

		std::vector<pgm::Program::Ptr> getPrograms();

		void executeProgram(const std::string& programname);


		pgm::Event::Ptr addEvent(unsigned int register, unsigned int value, const std::string& programId);

		void connectMobilePlatform(const std::string& connecttype, const std::string& host);
		void disconnectMobilePlatform();

		void connectManipulator(const std::string& connecttype, const std::string host);
		void disconnectManipulator();

		void connectCamera(const std::string& connecttype, int camid, const std::string& calibrationfile); 
		void disconnectCamera();
		
		void update();

		rw::kinematics::State getState();

		void loadCalibration(const std::string& filename);

	private:
		rws::RobWorkStudio* _rwstudio;
		pgm::ProjectController _projectController;
		pgm::Project::Ptr _project;
		er_common::System::Ptr _system;
//		er_common::MobileManipulator::Ptr _mobileManipulator;
		exm::ExecutionManager::Ptr _executionManager;

//		UIDialogs::Ptr _dialogs;
	};

} // end namespaces

#endif // end include guard
