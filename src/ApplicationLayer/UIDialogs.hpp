/* */
#ifndef MVC_UIDIALOGS_HPP
#define MVC_UIDIALOGS_HPP


#include <rw/common/Ptr.hpp>

namespace	mvc {
	
/**
 * Interface for user interface dialogs
 */
class UIDialogs
{
public:
    /** @brief Smart pointer to object*/
	typedef rw::common::Ptr<UIDialogs> Ptr;

	static void setInstance(UIDialogs::Ptr instance);
	static UIDialogs::Ptr instance();

	virtual void infoBox(const std::string& title, const std::string& message) = 0;
	virtual void warningBox(const std::string& title, const std::string& message) = 0;
	virtual void criticalBox(const std::string& title, const std::string& message) = 0;

	virtual std::string getOpenFileName(const std::string& title, const std::string& directory, const std::string& filter) = 0;
	virtual std::string getSaveFileName(const std::string& title, const std::string& directory, const std::string& filter) = 0;


protected:
	UIDialogs();
private:
	static UIDialogs::Ptr _instance;

	

};

} //end namespace

#endif //#ifndef MVC_HMI_HPP
