#include "UIDialogs.hpp"

using namespace mvc;

UIDialogs::Ptr UIDialogs::_instance;

UIDialogs::UIDialogs()
{
}


void UIDialogs::setInstance(UIDialogs::Ptr instance)
{
	_instance = instance;
}

UIDialogs::Ptr UIDialogs::instance()
{
	return _instance;
}

