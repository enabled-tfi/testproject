#ifndef ER_COMMON_ENTITYREPOSITORYINITIALIZER_HPP
#define ER_COMMON_ENTITYREPOSITORYINITIALIZER_HPP

#include <rw/common/Ptr.hpp>
#include <rw/models/WorkCell.hpp>

#include <ProgramModel/Project.hpp>
#include <ProgramModel/ProjectController.hpp>

#include <ExecutionModel/ExecutionManager.hpp>
#include <rws/RobWorkStudio.hpp>

namespace er_common {

    /**
     * @brief Class to be used for initializing the EntityRepository.
     */
	class EntityRepositoryInitializer
	{
	public:
        /** 
         * @brief Initialize the respository
         */
		static void initializeRepository();
	private:
		EntityRepositoryInitializer() {}
	};

} // end namespaces

#endif // end include guard
