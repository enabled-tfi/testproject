
#include "EntityRepositoryInitializer.hpp"

#include <Serialization/InputArchiveXML.hpp>
#include <Serialization/OutputArchiveXML.hpp>
#include <Common/EntityRepository.hpp>
#include <Common/EntityFactory.hpp>
#include <Common/EntityRegister.hpp>
#include <Common/HWEntity.hpp>
#include <Common/SWEntity.hpp>
#include <HardwareLayer/ManipulatorSimulated.hpp>
#include <HardwareLayer/MobileDeviceSimulated.hpp>
#include <HardwareLayer/ActuatorSimulated.hpp>
#include <HardwareLayer/Gripper1DSimulated.hpp>
#include <HardwareLayer/CameraSimulated/CameraSimulated.hpp>
#include <HardwareLayer/CameraSimulated/StaticImageCamera.hpp>
#include <HardwareLayer/Scanner2DSimulated/Scanner2DSimulated.hpp>
#include <HardwareLayer/DistanceSensor1DSimulated/DistanceSensor1DSimulated.hpp>
#include <HardwareLayer/SMAC/SMACSimulated.hpp>
#include <HardwareLayer/MIR/MIR.hpp>
#include <HardwareLayer/MIR/MIR2.hpp>
#include <HardwareLayer/UR/UR.hpp>
#include <HardwareLayer/CameraUSBOpenCV/CameraUSBOpenCV.hpp>
#include <HardwareLayer/CameraBaslerPylon/CameraBaslerPylon.hpp>
#include <HardwareLayer/CameraSee3CAM/CameraSee3CAM.hpp>
#include <HardwareLayer/LeuzeODSL9Analog/LeuzeODSL9Analog.hpp>
//#include <HardwareLayer/RealSenseD400/RealSenseD400.hpp>
#include <ProgramItems/LocalizeArmWithEyeInHand.hpp>
#include <ProgramItems/PickOrPlace.hpp>

#include <ExecutionModel/MovePTP.hpp>
#include <ExecutionModel/MoveLin.hpp>
#include <ExecutionModel/MoveT3D.hpp>
#include <ExecutionModel/Wait.hpp>

#include <ExecutionModel/MarkerDetectionArUco.hpp>
#include <ExecutionModel/MarkerDetectionChessboard.hpp>
#include <ExecutionModel/RobotCameraCalibration.hpp>

#include <EnvironmentLayer/MarkerDatabase.hpp>
#include <EnvironmentLayer/RobotDatabase.hpp>

using namespace er_common;
using namespace er_environment;

using namespace pgm;
using namespace exm;
using namespace hwl;
using namespace er_pgi;
using namespace rw::common;


template<class T>
std::map<std::string, EntityFactoryBase::Ptr> EntityRepository<T>::_entityFactories;

template<class T>
std::vector<rw::common::Ptr<rw::plugin::DynamicLibraryLoader<EntityFactoryBase> > > EntityRepository<T>::_loaders;


void EntityRepositoryInitializer::initializeRepository() {

	EntityRepository<Instruction>::addEntityFactory(LocalizeArmWithEyeInHand::getClassIdentifier(), ownedPtr(new EntityFactory<LocalizeArmWithEyeInHand>()));
	EntityRepository<Instruction>::addEntityFactory("PickOrPlace", ownedPtr(new EntityFactory<PickOrPlace>()));

	EntityRepository<Instruction>::addEntityFactory("MovePTP", ownedPtr(new EntityFactory<MovePTP>()));
	EntityRepository<Instruction>::addEntityFactory("MoveLin", ownedPtr(new EntityFactory<MoveLin>()));
	EntityRepository<Instruction>::addEntityFactory("MoveT3D", ownedPtr(new EntityFactory<MoveT3D>()));
	EntityRepository<Instruction>::addEntityFactory("Wait", ownedPtr(new EntityFactory<Wait>()));
	EntityRepository<Instruction>::addEntityFactory("MarkerDetectionArUco", ownedPtr(new EntityFactory<MarkerDetectionArUco>()));
	EntityRepository<Instruction>::addEntityFactory("MarkerDetectionChessboard", ownedPtr(new EntityFactory<MarkerDetectionChessboard>()));
	EntityRepository<Instruction>::addEntityFactory("RobotCameraCalibration", ownedPtr(new EntityFactory<RobotCameraCalibration>()));

	ER_LOG_DEBUG("Create UR Factory ");
	EntityRepository<HWEntity>::addEntityFactory("UR", ownedPtr(new EntityFactory<UR>()));

	ER_LOG_DEBUG("Create MIR Factory "); 
	EntityRepository<HWEntity>::addEntityFactory("MIR", ownedPtr(new EntityFactory<MIR>()));
	EntityRepository<HWEntity>::addEntityFactory("MIR2", ownedPtr(new EntityFactory<MIR2>()));

	ER_LOG_DEBUG("Create CameraUSBOpenCV Factory ");
	EntityRepository<HWEntity>::addEntityFactory("CameraUSBOpenCV", ownedPtr(new EntityFactory<CameraUSBOpenCV>()));

	ER_LOG_DEBUG("Create CameraBaslerPylon Factory ");
	EntityRepository<HWEntity>::addEntityFactory("CameraBaslerPylon", ownedPtr(new EntityFactory<CameraBaslerPylon>()));

#ifndef WIN32
	ER_LOG_DEBUG("Create CameraSee3CAM Factory ");
	EntityRepository<HWEntity>::addEntityFactory("CameraSee3CAM", ownedPtr(new EntityFactory<CameraSee3CAM>()));
#endif
	ER_LOG_DEBUG("Create StaticImageCamera Factory " );
	EntityRepository<HWEntity>::addEntityFactory("StaticImageCamera", ownedPtr(new EntityFactory<StaticImageCamera>()));

	ER_LOG_DEBUG("Create CameraSimulated Factory ");
	EntityRepository<HWEntity>::addEntityFactory("CameraSimulated", ownedPtr(new EntityFactory<CameraSimulated>()));

	ER_LOG_DEBUG("Create Scanner2DSimulated Factory ");
    EntityRepository<HWEntity>::addEntityFactory("Scanner2DSimulated", ownedPtr(new EntityFactory<Scanner2DSimulated>()));

    ER_LOG_DEBUG("Create DistanceSensor1DSimulated Factory ");
    EntityRepository<HWEntity>::addEntityFactory("DistanceSensor1DSimulated", ownedPtr(new EntityFactory<DistanceSensor1DSimulated>()));

    ER_LOG_DEBUG("Create ManipulatorSimulated Factory ");
	EntityRepository<HWEntity>::addEntityFactory("ManipulatorSimulated", ownedPtr(new EntityFactory<ManipulatorSimulated>()));

	ER_LOG_DEBUG("Create MobileDeviceSimulated Factory "); 
	EntityRepository<HWEntity>::addEntityFactory("MobileDeviceSimulated", ownedPtr(new EntityFactory<MobileDeviceSimulated>()));

    ER_LOG_DEBUG("Create ActuatorSimulated Factory ");
    EntityRepository<HWEntity>::addEntityFactory("ActuatorSimulated", ownedPtr(new EntityFactory<ActuatorSimulated>()));

    ER_LOG_DEBUG("Create Gripper1DSimulated Factory ");
    EntityRepository<HWEntity>::addEntityFactory("Gripper1DSimulated", ownedPtr(new EntityFactory<Gripper1DSimulated>()));
    
    ER_LOG_DEBUG("Create LeuzeODSL9Analog Factory ");
	EntityRepository<HWEntity>::addEntityFactory("LeuzeODSL9Analog", ownedPtr(new EntityFactory<LeuzeODSL9Analog>()));

//    ER_LOG_DEBUG("Create RealSenseD400 Factory ");
//    EntityRepository<HWEntity>::addEntityFactory("RealSenseD400", ownedPtr(new EntityFactory<RealSenseD400>()));


    ER_LOG_DEBUG("Create MarkerDatabase Factory ");
	EntityRepository<SWEntity>::addEntityFactory("MarkerDatabase", ownedPtr(new EntityFactory<MarkerDatabase>()));

	ER_LOG_DEBUG("Create RobotDatabase Factory ");
	EntityRepository<SWEntity>::addEntityFactory("RobotDatabase", ownedPtr(new EntityFactory<RobotDatabase>()));

}

