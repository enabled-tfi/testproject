
#include "ApplicationControl.hpp"

#include <rw/common/Ptr.hpp>
#include <Serialization/InputArchiveXML.hpp>
#include <Serialization/OutputArchiveXML.hpp>
#include <Common/EntityRepository.hpp>
#include <ApplicationLayer/EntityRepositoryInitializer.hpp>
#include <Common/EntityFactory.hpp>
#include <Common/EntityRegister.hpp>
#include <Common/HWEntity.hpp>
#include <Common/SWEntity.hpp>
#include <HardwareLayer/ManipulatorSimulated.hpp>
#include <HardwareLayer/MobileDeviceSimulated.hpp>
#include <HardwareLayer/CameraSimulated/CameraSimulated.hpp>
#include <HardwareLayer/SMAC/SMACSimulated.hpp>
#include <HardwareLayer/MIR/MIR.hpp>
#include <HardwareLayer/UR/UR.hpp>
#include <HardwareLayer/CameraUSBOpenCV/CameraUSBOpenCV.hpp>
#include <ProgramItems/LocalizeArmWithEyeInHand.hpp>
#include <ProgramItems/PickOrPlace.hpp>

#include <ExecutionModel/MovePTP.hpp>
#include <ExecutionModel/MoveLin.hpp>
#include <ExecutionModel/MoveT3D.hpp>

#include <ExecutionModel/MarkerDetectionArUco.hpp>
#include <ExecutionModel/RobotCameraCalibration.hpp>

using namespace mvc;
using namespace er_serialization;
using namespace er_common;
using namespace er_common;

using namespace pgm;
using namespace exm;
using namespace hwl;
using namespace er_pgi;
using namespace rw::common;
using namespace rw::kinematics;
using namespace rw::models;
using namespace rws;


namespace {


}


ApplicationControl::ApplicationControl(WorkCell::Ptr workcell, RobWorkStudio* rwstudio):
	_rwstudio(rwstudio)
{
	EntityRepositoryInitializer::initializeRepository();

	_system = System::getInstance();
	_system->initialize(workcell);
	_executionManager = ownedPtr(new ExecutionManager(_system));
}

ApplicationControl::ApplicationControl(RobWorkStudio* rwstudio):
	_rwstudio(rwstudio)
{
	EntityRepositoryInitializer::initializeRepository();
	_system = System::getInstance();
	_executionManager = ownedPtr(new ExecutionManager(_system));
}



ApplicationControl::~ApplicationControl()
{
	EntityRegister::clear();
}

void ApplicationControl::loadSystem(const std::string& filename, bool connect)
{
	InputArchiveXML iarchive(filename);
	_system = System::getInstance();

    std::string systemFolder = StringUtil::getDirectoryName(filename);
    _system->setSystemFolder(systemFolder);

    iarchive.initialize();
	ER_LOG_DEBUG("System::read " );
	_system->read(iarchive, "System");
	ER_LOG_DEBUG("System initialize " );
	_system->initialize();
	ER_LOG_DEBUG("RWStudio Set Work Cell" << _system->getWorkCell()<<" "<<_rwstudio);
	_rwstudio->setWorkCell(_system->getWorkCell());
	ER_LOG_DEBUG("Work cell set " );

	//Initialize entities found in the system
	if (connect) {
		std::vector<HWEntity::Ptr> entities = EntityRegister::getAll<HWEntity>();

		for (HWEntity::Ptr entity : entities) {
			try {
				if (entity.cast<hwl::SimulatedEntity>() != NULL) {
					entity.cast<SimulatedEntity>()->initializeSimulation(_rwstudio);
				}

				ER_LOG_DEBUG("Ready to connect to " << entity->getName() );
				entity->connect();
				entity->addUpdateRule(_system);

			}
			catch (const std::exception& exp) {
				ER_LOG_DEBUG("Exception while trying to connec to " << entity->getName() << ": " << exp.what() );
				throw exp;
			}
		}
	}
}

void ApplicationControl::saveSystem(const std::string& filename)
{
	OutputArchiveXML oarchive(filename);
	_system->write(oarchive, "System");
	oarchive.finalize();
}


void ApplicationControl::newProject(const std::string& name)
{
	_project = ownedPtr(new Project(name));
}

void ApplicationControl::openProject(const std::string& filename)
{
	_project = ownedPtr(new Project(""));
	InputArchiveXML iarchive(filename);
	iarchive.initialize();
	_project->read(iarchive, "Project");
}

void ApplicationControl::saveProject(const std::string& filename)
{
	OutputArchiveXML oarchive(filename);
	_project->write(oarchive, "Project");
	oarchive.finalize();

}

void ApplicationControl::closeProject()
{
	_project = NULL;
}


void ApplicationControl::runProject()
{
	if (_project == NULL)
		RW_THROW("No project to run");

	_projectController.startProject(_project, false);
	if (_projectController.getStatus() == ProjectController::Error) {
		RW_THROW("Error trying to execute project: " << _projectController.getLastError());
	}
}

bool ApplicationControl::isProjectRunning()
{
	return _projectController.getStatus() == ProjectController::Running;
}

void ApplicationControl::stopProject()
{
	_projectController.stopProject();
}


pgm::Project::Ptr ApplicationControl::getProject()
{
	return _project;
}

std::vector<pgm::Event::Ptr> ApplicationControl::getEvents()
{
	return _project->getEvents();
}

std::vector<pgm::Program::Ptr> ApplicationControl::getPrograms()
{
	return _project->getPrograms();
}

void ApplicationControl::executeProgram(const std::string& programname)
{
	Program::Ptr program = _project->getProgram(programname);
	_executionManager = ownedPtr(new ExecutionManager(_system));
	_executionManager->executeNonBlocking(program);
	ER_LOG_DEBUG("Execution Manager started " );
	/*while (_executionManager->getExecutionStatus() == ExecutionManager::Running) {
		ER_LOG_DEBUG("+";
		TimerUtil::sleepMs(100);
	}
	if (_executionManager->getExecutionStatus() == ExecutionManager::Success) {
		RW_THROW("Error while executing " << programname << ": " << _executionManager->getErrorMessage());
	}*/
}

pgm::Event::Ptr ApplicationControl::addEvent(unsigned int reg, unsigned int value, const std::string& programId)
{
	pgm::Event::Ptr event = ownedPtr(new pgm::Event(reg, value, programId));
	_project->addEvent(event);
	return event;
}


void ApplicationControl::connectMobilePlatform(const std::string& connecttype, const std::string& host) {

	if (EntityRegister::has("MobileDevice")) {
		RW_THROW("Mobile Device Already Connected");
	}
	if (connecttype == "Simulated") {
		hwl::MobileDevice::Ptr mobile = ownedPtr(new MobileDeviceSimulated(_system->getMobileBase(), _system, "MobileDevice"));
		EntityRegister::add("MobileDevice", mobile);

		ER_LOG_DEBUG("EntityRegister::Get<MobileDevice>() " << EntityRegister::get<hwl::MobileDevice::Ptr>("MobileDevice") );
		ER_LOG_DEBUG("EntityRegister::Get<HWEntity>() " << EntityRegister::get<HWEntity::Ptr>("MobileDevice") );

		_system->addUpdateRule(ownedPtr(new hwl::MobileDevice::MobileDeviceUpdateRule(_system->getMobileBase(), mobile)));
	}
	else if (connecttype == "Direct") {

		MIR::Ptr mir = ownedPtr(new MIR(host));
		mir->connect();

		EntityRegister::add("MobileDevice", mir);


		_system->addUpdateRule(ownedPtr(new hwl::MobileDevice::MobileDeviceUpdateRule(_system->getMobileBase(), mir)));
	}
}

void ApplicationControl::disconnectMobilePlatform() {
	if (EntityRegister::has("MobileDevice")) {
		hwl::MobileDevice::Ptr device = EntityRegister::get<hwl::MobileDevice>("MobileDevice");
		//device->disconnect();
		_system->removeUpdateRuleForEntity(device);
		EntityRegister::remove("MobileDevice");
	}
}

void ApplicationControl::connectManipulator(const std::string& connecttype, const std::string host)
{
	if (EntityRegister::has("Manipulator")) {
		RW_THROW("Manipulator Already Connected");
	}

	if (connecttype == "Simulated") {
		Manipulator::Ptr manipulator = ownedPtr(new ManipulatorSimulated(_system->getManipulator(), _system, "Manipulator"));
		EntityRegister::add("Manipulator", manipulator);
		_system->addUpdateRule(ownedPtr(new hwl::Manipulator::ManipulatorUpdateRule(_system->getManipulator(), manipulator)));
	}
	else if (connecttype == "Direct") {
		UR::Ptr ur = ownedPtr(new UR(host, UR::ControlAndListen));
		ur->connect();
		EntityRegister::add("Manipulator", ur.cast<Manipulator>());
		_system->addUpdateRule(ownedPtr(new hwl::Manipulator::ManipulatorUpdateRule(_system->getManipulator(), ur)));
	}
}



void ApplicationControl::disconnectManipulator()
{
	Manipulator::Ptr device = EntityRegister::get<Manipulator>("Manipulator");
	ER_LOG_DEBUG("Manipulator = " << device );

	_system->removeUpdateRuleForEntity(device);
	ER_LOG_DEBUG("Remove update rule " );
	EntityRegister::remove("Manipulator");
	ER_LOG_DEBUG("Remove entity " );

	return;
}

#include <rws/SceneOpenGLViewer.hpp>
#include <rw/graphics/WorkCellScene.hpp>
using namespace rw::graphics;

void ApplicationControl::connectCamera(const std::string& connecttype, int camid, const std::string& argument )
{
	if (EntityRegister::has("Camera")) {
		RW_THROW("Camera Already Connected");
	}
	if (connecttype == "USBOpenCV") {
		Camera::Ptr camera = ownedPtr(new CameraUSBOpenCV("Camera", camid, argument));
		camera->connect();

		EntityRegister::add("Camera", camera);
		_system->addUpdateRule(ownedPtr(new hwl::Camera::CameraUpdateRule(camera)));
	}

	if (connecttype == "Simulated") {
		ER_LOG_DEBUG("Connect Simulated Camera " << argument);
		Frame* cameraFrame = _system->getWorkCell()->findFrame(argument);
		ER_LOG_DEBUG("Camera Frame = " << cameraFrame );
		//sceneviewer->setWorkCellScene(_rwstudio->getWorkCellScene());
		//rw::graphics::SceneViewer::Ptr sceneviewer = _rwstudio->getView()->getSceneViewer();
		Camera::Ptr camera = ownedPtr(new CameraSimulated("Camera", cameraFrame, _rwstudio));
		ER_LOG_DEBUG("Camera Created " );
		camera->connect();
		ER_LOG_DEBUG("Camera connected " );
		EntityRegister::add("Camera", camera);
		_system->addUpdateRule(ownedPtr(new hwl::Camera::CameraUpdateRule(camera)));
	}

}

void ApplicationControl::disconnectCamera()
{
	Camera::Ptr cam = EntityRegister::get<Camera>("Camera");
	_system->removeUpdateRuleForEntity(cam);

	if (cam != NULL)
		EntityRegister::remove("Camera");

	cam->disconnect();
}


void ApplicationControl::update()
{
	_system->update();
}


State ApplicationControl::getState() {
	return _system->getState();
}

#include <WorkCellCalibration/src/xml/XmlCalibrationLoader.hpp>

using namespace sdurobotics::calibration;

void ApplicationControl::loadCalibration(const std::string& filename)
{
	WorkCellCalibration::Ptr calibration = XmlCalibrationLoader::load(_system->getWorkCell(), filename);
	calibration->apply();
	_rwstudio->updateAndRepaint();
}
