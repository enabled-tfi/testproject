/* */

#ifndef SRC_ER_ENVIRONMENT_MARKERTYPE_HPP_
#define SRC_ER_ENVIRONMENT_MARKERTYPE_HPP_

#include <rw/rw.hpp>
#include <Common/Serializable.hpp>

namespace er_environment {

/**
 * Object representing a single marker type
 */
class MarkerType : public er_common::Serializable {
public:

	/**
	 * @brief Virtual destructor
	 */
	virtual ~MarkerType(){};

	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<MarkerType> Ptr;

	/**
	 * @brief Returns the id of the MarkerType
	 * @return The id of the MarkerType
	 */
	std::string getID();

	/**
	 * @brief Get name of marker
	 * @return Name of marker
	 */
	virtual std::string getName() = 0;

	/**
	 * @brief Get the category of the marker
	 * @return Category of the marker
	 */
	std::string getCategory();

protected:
	std::string _ID;
	std::string _name;
	std::string _category;
};

} /* namespace er_environment */

#endif /* SRC_ER_ENVIRONMENT_MARKERTYPE_HPP_ */
