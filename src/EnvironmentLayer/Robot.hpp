/* */

#ifndef SRC_ER_ENVIRONMENT_ROBOT_HPP_
#define SRC_ER_ENVIRONMENT_ROBOT_HPP_

#include <rw/rw.hpp>
#include <Common/Serializable.hpp>

namespace er_environment {

/**
 * Object representing a single robot type
 */
class Robot {
public:

	Robot(std::string name, std::string workcellFilename);
	/**
	 * @brief Virtual destructor
	 */
	virtual ~Robot(){};

	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<Robot> Ptr;

	/**
	 * @brief Get name of robot
	 * @return Name of robot
	 */
	std::string getName();

	/**
	 * @brief Get filename of robot
	 * @return Filename of robot
	 */
	std::string getRWModelFilename();

	/**
	 * @brief Get robwork model of robot
	 * @return RW model of robot
	 */
	rw::models::SerialDevice::Ptr getRWModel();

protected:
	std::string _name;
	std::string _rwModelFilename;
};

} /* namespace er_environment */

#endif /* SRC_ER_ENVIRONMENT_ROBOT_HPP_ */
