/* */

#ifndef SRC_ER_ENVIRONMENT_STATICHARDWARE_ROBOTDATABASE_HPP_
#define SRC_ER_ENVIRONMENT_STATICHARDWARE_ROBOTDATABASE_HPP_

#include <Common/Serializable.hpp>
#include <Common/Entity.hpp>
#include <Common/SWEntity.hpp>
#include <EnvironmentLayer/Robot.hpp>

namespace er_environment {

/**
 * A database of available robot types
 */
class RobotDatabase : public er_common::SWEntity {
public:
    /** @brief Smart pointer to object*/
	typedef rw::common::Ptr<RobotDatabase> Ptr;

	/** @brief Default constructor **/
	RobotDatabase(std::string identifier);
	RobotDatabase();

	/** @brief Default destructor **/
	virtual ~RobotDatabase();

	/**
	 * @brief Remove all Robots from database
	 */
	void clear();

	/**
	 * @brief Remove single Robot with given identifier
	 * @param identifier [in] Identifier of Robot to remove
	 * @return	True if Robot was removed
	 */
	bool remove(const std::string& identifier);

	/**
	* @brief Returns true if the RobotDatabase has element matching \b identifer
	*
	* @param identifier [in] string identifier for the Robot.
	* @return True if the Robot exists. Otherwise false.
	*/
	bool has(const std::string& identifier);

	/**
	 * @brief Returns vector of all Robots
	 *
	 * @return Vector of all Robots in database
	 */
	std::vector<Robot::Ptr> getAll();

    /**
     * @brief Get names of all Robots in the database
     *
     * @return vector of all names in database
     */
	std::vector<std::string> getNames();

    /**
     * @brief Registers the Robot \b Robot with name \b identifier
     *
     * @param identifier [in] Identifer for looking up the Robot
     * @param robot [in] The Robot to register
     */
	void add(const std::string& identifier, Robot::Ptr robot);

    /**
     * @brief Returns the Robot matching \b identifer.
     *
     * If no entity with the given identifier and type can be found, it will throw an exception.
     *
     * @param identifier [in] string identifier for the Robot.
     * @return Reference to the Robot
     */
	Robot::Ptr get(const std::string& identifier);


	/**
	* @copydoc Serializable::read
	*/
	virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

	/**
	* @copydoc Serializable::write
	*/
	virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

	typedef std::map<std::string, Robot::Ptr> RobotMap;

	RobotMap _robots;

private:
	std::string _robotDatabasePath;
};

} /* namespace er_environment */

#endif /* SRC_ER_ENVIRONMENT_STATICHARDWARE_ROBOTDATABASE_HPP_ */
