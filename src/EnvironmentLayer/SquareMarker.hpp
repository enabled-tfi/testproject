/*
 * SquareMarker.hpp
 *
 *  Created on: May 4, 2018
 *      Author: thomas
 */

#ifndef SRC_ENVIRONMENTLAYER_SQUAREMARKER_HPP_
#define SRC_ENVIRONMENTLAYER_SQUAREMARKER_HPP_

#include <EnvironmentLayer/MarkerType.hpp>

namespace er_environment {

/**
 * Object representing a 2D square marker (ArUco, Chessboard...)
 */

class SquareMarker : public MarkerType {
public:
	/**
	 * @brief Struct for storing dimensions of a square marker.
	 */
	struct Dimensions{
		int cols, rows;
		double squareSize;
	};

	/**
	 * @brief Default constructor
	 */
	SquareMarker();

	/**
	 * @brief Default destructor
	 */
	~SquareMarker();

	/**
	 * @breief Returns the dimensions of the marker
	 * @return A struct representing the dimension of the marker
	 */
	Dimensions getDimensions();

	/**
	 * @copydoc MarkerType::getName
	 */
	std::string getName();

	int getID() {
		return _ID;
	}

	/**
	* @copydoc Serializable::read
	*/
	virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

	/**
	* @copydoc Serializable::write
	*/
	virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

private:
	Dimensions _dimension;
	int _ID;
};

} /* namespace er_environment */

#endif /* SRC_ENVIRONMENTLAYER_SQUAREMARKER_HPP_ */
