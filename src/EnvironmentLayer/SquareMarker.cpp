/*
 * SquareMarker.cpp
 *
 *  Created on: May 4, 2018
 *      Author: thomas
 */

#include <EnvironmentLayer/SquareMarker.hpp>
#include <Common/InputArchive.hpp>
#include <Common/System.hpp>

namespace er_environment {

SquareMarker::SquareMarker() {
	// TODO Auto-generated constructor stub

}

SquareMarker::~SquareMarker() {
	// TODO Auto-generated destructor stub
}

SquareMarker::Dimensions SquareMarker::getDimensions(){
	return _dimension;
}

std::string SquareMarker::getName(){
	return _name;
}

void SquareMarker::read(class er_serialization::InputArchive& iarchive, const std::string& id){
	ER_LOG_DEBUG("Reading squaremaker" );

	_name = iarchive.read<std::string>("Name");
	_category = iarchive.read<std::string>("Category");

	iarchive.readEnterScope("Dimension");

	_dimension.rows = iarchive.read<int>("Rows");
	_dimension.cols = iarchive.read<int>("Cols");
	_dimension.squareSize = iarchive.read<double>("SquareSize");

	iarchive.readLeaveScope("Dimension");

	_ID = iarchive.read<int>("ID", -1);

}

void SquareMarker::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const{

}


} /* namespace er_environment */

