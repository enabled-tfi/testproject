#include "Robot.hpp"
#include <boost/pointer_cast.hpp>
#include <Common/Logging.hpp>

namespace er_environment {

Robot::Robot(std::string name, std::string workcellFilename){
	_name = name;
	_rwModelFilename = workcellFilename;
	ER_LOG_DEBUG("Creating robot with name: " << _name << " and workcell: " << _rwModelFilename);
}


std::string Robot::getName(){
	return _name;
}


rw::models::SerialDevice::Ptr Robot::getRWModel(){

	return dynamic_cast<rw::models::SerialDevice*>(rw::loaders::XMLRWLoader::load(_rwModelFilename)->findDevice("_name").get());
}

std::string Robot::getRWModelFilename(){
	return _rwModelFilename;
}

}
