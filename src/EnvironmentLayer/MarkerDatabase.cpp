/*  */

#include <EnvironmentLayer/MarkerDatabase.hpp>
#include <EnvironmentLayer/SquareMarker.hpp>

#include <Common/InputArchive.hpp>
#include <Common/OutputArchive.hpp>
#include <Serialization/InputArchiveXML.hpp>
#include <Serialization/OutputArchiveXML.hpp>

namespace er_environment {

MarkerDatabase::MarkerDatabase(std::string identifier) :
	SWEntity(identifier)
{
}

MarkerDatabase::MarkerDatabase() :
	SWEntity("MarkerDatabase")
{
}

MarkerDatabase::~MarkerDatabase() {
}

void MarkerDatabase::clear(){
	_markers.clear();
}

bool MarkerDatabase::remove(const std::string& identifier){
	MarkerMap::iterator it = _markers.find(identifier);
	if (it != _markers.end()) {
		_markers.erase(it);
		return true;
	}
	return false;

}

bool MarkerDatabase::has(const std::string& identifier){
	return _markers.find(identifier) != _markers.end();

}

std::vector<std::string> MarkerDatabase::getNames() {
    std::vector<std::string> result;
    for (std::map<std::string, MarkerType::Ptr>::const_iterator it = _markers.begin(); it != _markers.end(); ++it) {
    	result.push_back((*it).first);
    }
    return result;
}

std::vector<MarkerType::Ptr> MarkerDatabase::getAll(){
	std::vector<MarkerType::Ptr> result;
	for (MarkerMap::iterator it = _markers.begin(); it != _markers.end(); ++it) {
		MarkerType::Ptr res = (*it).second;

		if (res != NULL) {
			result.push_back(res);
		}
	}
	return result;
}

std::vector<std::string> MarkerDatabase::getAllCategories(){
	return _markerCategories;
}


MarkerType::Ptr MarkerDatabase::get(const std::string& identifier){
	MarkerMap::iterator it = _markers.find(identifier);
	if (it != _markers.end()){
		MarkerType::Ptr res = (*it).second;
		if (res == NULL) {
			RW_THROW("Type of entity named '" << identifier << "' does not match.");
		}
		return res;
	}
	else {
		RW_THROW("No entity named '" << identifier << "' found.");
	}
}

void MarkerDatabase::add(const std::string& identifier, MarkerType::Ptr markertype){
	_markers[identifier] = markertype;
}

void MarkerDatabase::read(class er_serialization::InputArchive& iarchive, const std::string& id){

	_markerDatabasePath = iarchive.read<std::string>("MarkerDatabase");
	er_serialization::InputArchiveXML iarchiveDatabase(er_common::System::getInstance()->getSystemFolder() + _markerDatabasePath);
	iarchiveDatabase.initialize();


	iarchiveDatabase.readEnterScope("MarkerCategories");
	std::vector<std::pair<std::string, er_serialization::InputArchive::Ptr> > subarchs = iarchiveDatabase.getSubArchives();
	for (std::pair<std::string, er_serialization::InputArchive::Ptr> arch : subarchs) {
		if (arch.first == "Category") {
			std::string name = arch.second->read<std::string>("Name");
			_markerCategories.push_back(name);
			ER_LOG_DEBUG(name );
		}
	}
	iarchiveDatabase.readLeaveScope("MarkerCategories");

	iarchiveDatabase.readEnterScope("MarkerDatabase");

	subarchs = iarchiveDatabase.getSubArchives();

	for (std::pair<std::string, er_serialization::InputArchive::Ptr> arch : subarchs) {

		std::string filename = arch.second->read<std::string>("FileName");
	    er_serialization::InputArchiveXML iarchivenewMarker(er_common::System::getInstance()->getSystemFolder() + filename);
		iarchivenewMarker.initialize();
		iarchivenewMarker.readEnterScope("Marker");
		std::string markerName = iarchivenewMarker.read<std::string>("Name");

		if (arch.first == "Chessboard") {
			SquareMarker::Ptr newMarkerType = rw::common::ownedPtr<SquareMarker>(new SquareMarker());
			newMarkerType->read(iarchivenewMarker, "MarkerType");
			_markers.insert(std::pair<std::string, MarkerType::Ptr>(markerName, newMarkerType));
		}
		else if (arch.first == "ArUco") {
			SquareMarker::Ptr newMarkerType = rw::common::ownedPtr<SquareMarker>(new SquareMarker());
			newMarkerType->read(iarchivenewMarker, "MarkerType");
			_markers.insert(std::pair<std::string, MarkerType::Ptr>(markerName, newMarkerType));
		}
		else if (arch.first == "ChArUco") {
			SquareMarker::Ptr newMarkerType = rw::common::ownedPtr<SquareMarker>(new SquareMarker());
			newMarkerType->read(iarchivenewMarker, "MarkerType");
			_markers.insert(std::pair<std::string, MarkerType::Ptr>(markerName, newMarkerType));
		}
		else if (arch.first == "ERMarker") {
			SquareMarker::Ptr newMarkerType = rw::common::ownedPtr<SquareMarker>(new SquareMarker());
			newMarkerType->read(iarchivenewMarker, "MarkerType");
			_markers.insert(std::pair<std::string, MarkerType::Ptr>(markerName, newMarkerType));
		}
		else if (arch.first == "ERMarker0") {
			SquareMarker::Ptr newMarkerType = rw::common::ownedPtr<SquareMarker>(new SquareMarker());
			newMarkerType->read(iarchivenewMarker, "MarkerType");
			_markers.insert(std::pair<std::string, MarkerType::Ptr>(markerName, newMarkerType));
		}
		else if (arch.first == "ERMarker1") {
			SquareMarker::Ptr newMarkerType = rw::common::ownedPtr<SquareMarker>(new SquareMarker());
			newMarkerType->read(iarchivenewMarker, "MarkerType");
			_markers.insert(std::pair<std::string, MarkerType::Ptr>(markerName, newMarkerType));
		}
		else if (arch.first == "ERMarker2") {
			SquareMarker::Ptr newMarkerType = rw::common::ownedPtr<SquareMarker>(new SquareMarker());
			newMarkerType->read(iarchivenewMarker, "MarkerType");
			_markers.insert(std::pair<std::string, MarkerType::Ptr>(markerName, newMarkerType));
		}

		else
			RW_THROW("Found unknown markertype: " + arch.first);

		iarchivenewMarker.readLeaveScope("Marker");
	}
	iarchive.readLeaveScope("MarkerDatabase");

}

void MarkerDatabase::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const{
	oarchive.write(_markerDatabasePath, "MarkerDatabase");
}

} /* namespace hwl */
