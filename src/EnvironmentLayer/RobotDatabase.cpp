/*  */

#include <EnvironmentLayer/RobotDatabase.hpp>

#include <Common/InputArchive.hpp>
#include <Common/OutputArchive.hpp>
#include <Serialization/InputArchiveXML.hpp>
#include <Serialization/OutputArchiveXML.hpp>

namespace er_environment {

RobotDatabase::RobotDatabase(std::string identifier) :
	SWEntity(identifier)
{
}

RobotDatabase::RobotDatabase() :
	SWEntity("RobotDatabase")
{
}

RobotDatabase::~RobotDatabase() {
}

void RobotDatabase::clear(){
	_robots.clear();
}

bool RobotDatabase::remove(const std::string& identifier){
	RobotMap::iterator it = _robots.find(identifier);
	if (it != _robots.end()) {
		_robots.erase(it);
		return true;
	}
	return false;

}

bool RobotDatabase::has(const std::string& identifier){
	return _robots.find(identifier) != _robots.end();

}

std::vector<std::string> RobotDatabase::getNames() {
    std::vector<std::string> result;
    for (std::map<std::string, Robot::Ptr>::const_iterator it = _robots.begin(); it != _robots.end(); ++it) {
    	result.push_back((*it).first);
    }
    return result;
}

std::vector<Robot::Ptr> RobotDatabase::getAll(){
	std::vector<Robot::Ptr> result;
	for (RobotMap::iterator it = _robots.begin(); it != _robots.end(); ++it) {
		Robot::Ptr res = (*it).second;

		if (res != NULL) {
			result.push_back(res);
		}
	}
	return result;
}

Robot::Ptr RobotDatabase::get(const std::string& identifier){
	RobotMap::iterator it = _robots.find(identifier);
	if (it != _robots.end()){
		Robot::Ptr res = (*it).second;
		if (res == NULL) {
			RW_THROW("Type of entity named '" << identifier << "' does not match.");
		}
		return res;
	}
	else {
		RW_THROW("No entity named '" << identifier << "' found.");
	}
}

void RobotDatabase::add(const std::string& identifier, Robot::Ptr robot){
	_robots[identifier] = robot;
}

void RobotDatabase::read(class er_serialization::InputArchive& iarchive, const std::string& id){

	_robotDatabasePath = iarchive.read<std::string>("RobotDatabase");
	er_serialization::InputArchiveXML iarchiveDatabase(er_common::System::getInstance()->getSystemFolder() + _robotDatabasePath);
	iarchiveDatabase.initialize();

	iarchiveDatabase.readEnterScope("RobotDatabase");

	std::vector<std::pair<std::string, er_serialization::InputArchive::Ptr> > subarchs = iarchiveDatabase.getSubArchives();

	for (auto sa : subarchs) {
		ER_LOG_DEBUG("Got entity " + sa.first);

		Robot::Ptr robot = new Robot(sa.first, sa.second->read<std::string>("FileName"));
		add(sa.first, robot);
	}

//	iarchive.readLeaveScope("RobotDatabase");

}

void RobotDatabase::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const{
	oarchive.write(_robotDatabasePath, "RobotDatabase");
}

} /* namespace hwl */
