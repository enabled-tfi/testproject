/* */

#ifndef SRC_ER_ENVIRONMENT_STATICHARDWARE_MARKERDATABASE_HPP_
#define SRC_ER_ENVIRONMENT_STATICHARDWARE_MARKERDATABASE_HPP_

#include <EnvironmentLayer/MarkerType.hpp>
#include <Common/Serializable.hpp>
#include <Common/Entity.hpp>
#include <Common/SWEntity.hpp>

namespace er_environment {

/**
 * A database of available marker types
 */
class MarkerDatabase : public er_common::SWEntity {
public:
    /** @brief Smart pointer to EntityRegister object*/
	typedef rw::common::Ptr<MarkerDatabase> Ptr;

	/** @brief Default constructor **/
	MarkerDatabase(std::string identifier);
	MarkerDatabase();

	/** @brief Default destructor **/
	virtual ~MarkerDatabase();

	/**
	 * @brief Remove all MarkerTypes from database
	 */
	void clear();

	/**
	 * @brief Remove single MarkerType with given identifier
	 * @param identifier [in] Identifier of MarkerType to remove
	 * @return	True if MarkerType was removed
	 */
	bool remove(const std::string& identifier);

	/**
	* @brief Returns true if the MarkerDatabase has element matching \b identifer
	*
	* @param identifier [in] string identifier for the MarkerType.
	* @return True if the MarkerType exists. Otherwise false.
	*/
	bool has(const std::string& identifier);

	/**
	 * @brief Returns vector of all MarkerTypes
	 *
	 * @return Vector of all MarkerTypes in database
	 */
	std::vector<MarkerType::Ptr> getAll();

	/**
	 * @brief Returns a vector of all marker categories
	 *
	 * @return vector of all available marker categories
	 */
	std::vector<std::string> getAllCategories();

    /**
     * @brief Get names of all markers in the database
     *
     * @return vector of all names in database
     */
	std::vector<std::string> getNames();

    /**
     * @brief Returns the MarkerType matching \b identifer.
     *
     * If no entity with the given identifier and type can be found, it will throw an exception.
     *
     * @param identifier [in] string identifier for the MarkerType.
     * @return Reference to the MarkerType
     */
	MarkerType::Ptr get(const std::string& identifier);

    /**
     * @brief Registers the MarkerType \b MarkerType with name \b identifier
     *
     * @param identifier [in] Identifer for looking up the MarkerType
     * @param markertype [in] The MarkerType to register
     */
	void add(const std::string& identifier, MarkerType::Ptr markertype);

	/**
	* @copydoc Serializable::read
	*/
	virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

	/**
	* @copydoc Serializable::write
	*/
	virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

	typedef std::map<std::string, MarkerType::Ptr> MarkerMap;

	MarkerMap _markers;

private:
	std::string _markerDatabasePath;
	std::vector<std::string> _markerCategories;
};

} /* namespace er_environment */

#endif /* SRC_ER_ENVIRONMENT_STATICHARDWARE_MARKERDATABASE_HPP_ */
