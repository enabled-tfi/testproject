#ifndef ER_COMMON_SETTING_HPP
#define ER_COMMON_SETTING_HPP

#include "SettingBase.hpp"
#include <rw/common/Ptr.hpp>
#include <Common/Logging.hpp>

namespace er_common {

    /**
     * @brief Setting class using templates to support different data types.
     */
    template<class T>
    class Setting: public SettingBase
    {
    public:
        //! @brief Smart pointer type to this class
        typedef rw::common::Ptr<Setting> Ptr;

        /**
         * @brief Constructs Setting.
         *
         * Constructs a Setting with auto detected type.
         *
         * @param identifier [in] identifier
         * @param description [in] description
         * @param value [in] value
         */
		/*Setting(
            const std::string& identifier,
            const std::string& description,
            T value)
            :
			SettingBase(identifier, description, DataType::getType(value)),
            _value(value)
        {}
		*/
        /**
         * @brief Constructs Setting.
         * @param identifier [in] identifier
         * @param description [in] description
         * @param type [in] data type
         * @param value [in] value
         */
		Setting(
            const std::string& identifier,
            const std::string& description,
            const T& value,
			UserType userType= UserType::Default,
			InputType inputType = InputType::Numeric)
            :
			SettingBase(identifier, description, DataType::getType(value), userType, inputType),
            _value(value),
			_isRangeInitialized(false)
        {}

        
		/**
		* @brief Constructs Setting.
		* @param identifier [in] identifier
		* @param description [in] description
		* @param datatype [in] data type
		* @param usertype [in] mask for the user
		* @param inputtype [in] input type
		*/
		Setting(
			const std::string& identifier,
			const std::string& description,
			const DataType& datatype,
			const UserType& usertype,
			const InputType& inputtype)
			:
			SettingBase(identifier, description, datatype, usertype, inputtype),
			_isRangeInitialized(false)
		{}

		
		/**
         * @brief Destroys Setting
         * If the data type is a pointer, then what it points to is not deleted
         */
        virtual ~Setting() {};

        /**
         * @brief returns reference to the value
         * @return value
         */
        T& getValue() {
            return _value;
        }

        /**
         * @brief returns const reference to the value
         * @return value
         */
        const T& getValue() const {
            return _value;
        }

        /**
         * @brief Sets the value
         * @param value [in] the new value 
         */
        void setValue(const T& value) {
            _value = value;
        }

		std::pair<T, T> getRange() const {
			return _range;
		}

		void setRange(const std::pair<T, T>& range) {
			_range = range;
			_isRangeInitialized = true;
		}

		void setRange(const T&min, const T& max)
		{
			_range.first = min;
			_range.second = max;
			_isRangeInitialized = true;
		}

		bool isRangeInitialized() const {
			return _isRangeInitialized;
		}

        /**
           @copydoc PropertyBase::clone
        */
		rw::common::Ptr<SettingBase> clone() const
        {
            rw::common::Ptr<Setting<T> > setting = rw::common::ownedPtr(new Setting<T>(this->getIdentifier(),
                                   this->getDescription(),
                                   this->getDataType(),
								   this->getUserType(),
								   this->getInputType()));
			setting->setValue(_value);
			return setting;
        }



		//Methods implementing the Serializable interface
		/**
		* @copydoc Serializable::read
		*/
		virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id)
		{
			//iarchive.readEnterScope("Setting");
			//SettingBase::read(iarchive, id);
			ER_LOG_DEBUG("Setting " << getIdentifier() << " = ");
			_value = iarchive.read<T>("Value");
		
			_isRangeInitialized = iarchive.read<bool>("IsRangeInitialized");
			if (_isRangeInitialized) {
				_range.first = iarchive.read<T>("Minimum");
				_range.second = iarchive.read<T>("Maximum");
			}
			//iarchive.readLeaveScope("Setting");
		}
		

		/**
		* @copydoc Serializable::write
		*/
		virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
		{
		//	oarchive.writeEnterScope("Setting");
		//	SettingBase::write(oarchive, id);
			oarchive.write(_value, "Value");
			oarchive.write(_isRangeInitialized, "IsRangeInitialized");
			if (_isRangeInitialized) {
				oarchive.write(_range.first, "Minimum");
				oarchive.write(_range.second, "Maximum");
			}
		//	oarchive.writeLeaveScope("Setting");
		}

		

    private:
        T _value;
		bool _isRangeInitialized;
		std::pair<T, T> _range;
    };

    /**
     * @brief cast a property base to a specific property. Notice that the pointer
     * returned is owned by the PropertyBase::Ptr.
     * @param base [in] property base pointer
     * @return property of type \b T or null if property is of another type
     */
    template <class T>
    rw::common::Ptr<Setting<T> > toSetting(SettingBase::Ptr base)
    {
		typename Setting<T>::Ptr p = base.cast<Setting<T> >();// dynamic_cast<Property<T>*>(base.get());
        return p;
    }

    /*@}*/
} // end namespaces

#endif // end include guard
