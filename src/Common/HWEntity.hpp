/* */
#ifndef HWL_HWENTITY_HPP
#define HWL_HWENTITY_HPP

#include <Common/Entity.hpp>
#include <Common/System.hpp>



namespace hwl {

/** 
 * 
 */
class HWEntity : public er_common::Entity
{
public:
	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<HWEntity> Ptr;

	HWEntity(const std::string& identifier, bool isChild = false);

    /**
     * @brief Connect to the device
     */
	virtual void connect() = 0;

	virtual void disconnect() = 0;

	virtual void addUpdateRule(er_common::System::Ptr system) = 0;

    bool isChild() const;


    /**
    * @copydoc Serializable::read
    */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    /**
    * @copydoc Serializable::write
    */
	virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;
private:
    bool _isChild;
};

} //end namespace

#endif //#ifndef HWL_HWENTITY_HPP
