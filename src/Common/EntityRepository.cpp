#include "EntityRepository.hpp"

using namespace er_common;

using namespace rw::plugin;
using namespace rw::common;

 
template <class T>
std::map<std::string, EntityFactoryBase::Ptr> EntityRepository<T>::_entityFactories;

template <class T>
std::vector<rw::common::Ptr<rw::plugin::DynamicLibraryLoader<EntityFactoryBase> > > EntityRepository<T>::_loaders;

