/* */
#ifndef ER_COMMON_INSTRUCTIONREPOSITORY_HPP
#define ER_COMMON_INSTRUCTIONREPOSITORY_HPP

#include "EntityFactory.hpp"

#include <rw/common/Ptr.hpp>
#include <rw/common/PropertyMap.hpp>
#include <rw/plugin/DynamicLibraryLoader.hpp>
#include <map>


namespace er_common {

/**
 * Repository collecting all buildin and addon entities.
 */
template <class T>
class EntityRepository
{
public:
	/** @brief Smart pointer to the Instruction object*/
	typedef rw::common::Ptr<EntityRepository> Ptr;




	static void addEntityFactory(const std::string& id, EntityFactoryBase::Ptr factory)
	{
		if (_entityFactories.find(id) != _entityFactories.end()) {
			RW_THROW("Unable to register factory for entity with '" << id << "' because id already in repository.");
		}
		_entityFactories[id] = factory;
	}

	static void load(const std::string& fileName) {
		EntityFactoryBase::Ptr constructor;
		try
		{
			rw::common::Ptr<rw::plugin::DynamicLibraryLoader<EntityFactoryBase> > loader = rw::common::ownedPtr(new rw::plugin::DynamicLibraryLoader<EntityFactoryBase>(fileName));
			constructor = rw::common::ownedPtr(loader->get());
			_loaders.push_back(loader);
		}
		catch (const rw::common::Exception& exp)
		{
			RW_THROW("Unable to load plugin: " << fileName << ". Failed with message: " << exp.getMessage().getText());
		}

		if (constructor != NULL)
		{
			const std::string id = constructor->getIdentifier();
			if (_entityFactories.find(id) == _entityFactories.end()) {
				_entityFactories[id] = constructor;
			}
			else {
				RW_THROW("A EntityFactory with identifier " << id << " has already been loaded!");
			}
		}
		else {
			RW_THROW("Unable to create EntityFactory from: " << fileName);
		}
	}

	
	static rw::common::Ptr<T> make(const std::string& id)
	{
		if (_entityFactories.find(id) == _entityFactories.end())
		{
			RW_THROW("Could not find any Entity Factory for id '" << id << "'");
		} 
		Entity::Ptr entity = _entityFactories[id]->make();
		if (entity == NULL) {
		    RW_THROW("Failed to create entity for "<<id);
		}
		rw::common::Ptr<T> res = entity.cast<T>();
        if (res == NULL) {
            RW_THROW("Failed to cast entity to desired type "<<id);
        }

		return res;
	}
	
	static std::vector<std::string> getIds()
	{
		std::vector<std::string> result;
		for (std::map<std::string, EntityFactoryBase::Ptr>::const_iterator it = _entityFactories.begin(); it != _entityFactories.end(); ++it)
		{
			result.push_back((*it).first);
		}
		 
		return result;
	}



	//template <class T>
	//static std::vector<std::string> getIds() {
	//	std::vector<std::string> result;
	//	for (std::map<std::string, EntityFactoryBase::Ptr>::const_iterator it = _entityFactories.begin(); it != _entityFactories.end(); ++it)
	//	{
	//		if ((*it).second.cast < EntityFactory<T> >() != NULL) {
	//			result.push_back((*it).first);
	//		}
	//	}

	//	return result;
	//}

private:
	static std::map<std::string, EntityFactoryBase::Ptr> _entityFactories;

	static std::vector<rw::common::Ptr<rw::plugin::DynamicLibraryLoader<EntityFactoryBase> > > _loaders;

	EntityRepository() {};
};

} //end namespace

#endif //#ifndef EXM_INSTRUCTIONREPOSITORY_HPP
