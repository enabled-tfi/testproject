/* */
#ifndef ER_COMMON_TARGET_HPP
#define ER_COMMON_TARGET_HPP

#include "Serializable.hpp"

#include <rw/math/Transform3D.hpp>
#include <rw/common/PropertyMap.hpp>
#include <rw/models.hpp>
#include <rw/kinematics.hpp>
#include <rw/pathplanning/StateConstraint.hpp>

#include <boost/variant.hpp>

namespace er_common {
	
/**
 * Target represents a target for the robot comprised of a reference frame, a tool frame 
 * (frame on the robot to reach the target) and a transformation from the reference frame
 * to the target.
 *
 * Reference frame and tool frame can either be initialized with a pointer to the frame
 * or simply using the frame name. If the frame name is used the Target will search for 
 * the Frame in the work cell, when first queried for returning the frame.
 */
class Target: public Serializable
{
public:
	typedef rw::common::Ptr<Target> Ptr;

	Target();

	Target(const std::string& referenceFrameName, const std::string& toolFrameName, const rw::math::Transform3D<>& refTtarget = rw::math::Transform3D<>::identity());
	
	Target(rw::kinematics::Frame* referenceFrame, rw::kinematics::Frame* toolFrame, const rw::math::Transform3D<>& refTtarget = rw::math::Transform3D<>::identity());

	rw::kinematics::Frame* getReference() const;
	const std::string& getReferenceFrameName() const;

    rw::kinematics::Frame* getToolFrame() const;
    const std::string& getToolFrameName() const;

    
    const rw::math::Transform3D<>& refTtarget() const;

	void setReference(const std::string& referenceFrameName);
	void setReference(rw::kinematics::Frame* referenceFrame);

    void setToolFrame(const std::string& toolFrameName);
    void setToolFrame(rw::kinematics::Frame* toolFrame);

	void setRefTtarget(const rw::math::Transform3D<>& refTtarget);

	bool isInitialized() const;

	void read(er_serialization::InputArchive& iarchive, const std::string& id);
	void write(er_serialization::OutputArchive& oarchive, const std::string& id) const;



private:
	bool _isReferenceInitialized;
    bool _isToolFrameInitialized;
	bool _isTransformInitialized;
	boost::variant<std::string, rw::kinematics::Frame*> _reference;
    boost::variant<std::string, rw::kinematics::Frame*> _toolFrame;
    rw::math::Transform3D<> _refTtarget;

};

} //end namespace

#endif //#ifndef ER_COMMON_TARGET
