/* */
#ifndef ER_COMMON_ENTITYFACTORY_HPP
#define ER_COMMON_ENTITYFACTORY_HPP

#include "Entity.hpp"

#include <rw/plugin/FactoryMacro.hpp>
#include <rw/common/Ptr.hpp>
#include <rw/common/PropertyMap.hpp>

#ifndef EXPORT_INSTRUCTION_FACTORY
#define EXPORT_INSTRUCTION_FACTORY(name) \
    namespace { \
        class MyEntityFactory: public EntityFactory<name> { \
        }; \
        DLL_FACTORY_METHOD(MyEntityFactory) \
    }
#endif




namespace er_common {
	


/**
 *
 */
class EntityFactoryBase
{
public:
	/** @brief Smart pointer to the InstructionFactory object*/
	typedef rw::common::Ptr<EntityFactoryBase> Ptr;

	EntityFactoryBase(const std::string& name, const std::string& description);

	virtual Entity::Ptr make() = 0;

	const std::string& getIdentifier() const;

	const std::string& getDescription() const;

private:
	std::string _identifier;
	std::string _description;

};

template <class T>
class EntityFactory: public EntityFactoryBase
{
public:
	EntityFactory():
        EntityFactoryBase(T().getName(), T().getDescription())
    {}

    virtual Entity::Ptr make() {
        return rw::common::ownedPtr(new T());
    }
};

} //end namespace

#endif //#ifndef ER_COMMON_ENTITYFACTORY_HPP
