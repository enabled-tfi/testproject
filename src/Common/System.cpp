#include "System.hpp"

//#include "SystemSettings.hpp"
#include "EntityRegister.hpp"
#include "EntityRepository.hpp"
#include "CartesianQConstraint.hpp"

#include <Common/HWEntity.hpp>
#include <Common/SWEntity.hpp>

#include <rw/loaders/WorkCellLoader.hpp>
#include <rw/pathplanning/PlannerUtil.hpp>
#include <rw/proximity/CollisionDetector.hpp>
#include <rw/invkin/ClosedFormIKSolverUR.hpp>
#include <rwlibs/proximitystrategies/ProximityStrategyFactory.hpp>
#include <rwlibs/calibration/xml/XmlCalibrationLoader.hpp>
#include <Common/InputArchive.hpp>
#include <Common/OutputArchive.hpp>
#include <Serialization/InputArchiveXML.hpp>
#include <thread>

using namespace er_common;
using namespace rw::math;
using namespace rw::common;
using namespace rw::kinematics;
using namespace rw::models;
using namespace rw::trajectory;
using namespace rw::geometry;
using namespace rw::pathplanning;
using namespace rw::proximity;
using namespace rw::invkin;
using namespace rw::loaders;
using namespace rwlibs::proximitystrategies;
using namespace rwlibs::calibration;

System::Ptr System::_instance;
bool System::_singletonInitialized = false;

//template<>
//std::map<std::string, EntityFactoryBase::Ptr> EntityRepository<hwl::HWEntity>::_entityFactories;

template <class T>
std::map<std::string, EntityFactoryBase::Ptr> EntityRepository<T>::_entityFactories;

//template<>
//std::vector<rw::common::Ptr<rw::plugin::DynamicLibraryLoader<EntityFactoryBase> > > EntityRepository<hwl::HWEntity>::_loaders;


System::System()
{
	_wcChanged = false;
	_languageChanged = false;
}

System::Ptr System::getInstance()
{
//	if(!_copyProtection->isLegal())
//		RW_THROW("Copy protection not unlocked");

	if (_instance.isNull()) {
		_instance = ownedPtr(new System());
		_singletonInitialized = true;
	}
	return _instance;
}

bool System::isInitialized()
{
	return _singletonInitialized;
}

void System::initialize(rw::models::WorkCell::Ptr workcell)
{
	_workcell = workcell;
	initialize();
}

void System::initialize() { 

	ER_LOG_INFO("System::initialize");

	ER_LOG_DEBUG("Loading calibration files...");
	for (auto calibfile : _calibrationFiles)
	{
		ER_LOG_DEBUG("\tLoading calibration "<<calibfile.first);
        WorkCellCalibration::Ptr calibration = XmlCalibrationLoader::load(_workcell, getSystemFolder() + calibfile.second);
        calibration->apply();
	}
	ER_LOG_DEBUG("Calibrations loaded.");

	if(_workcell.isNull())
		RW_THROW("Workcell not properly initialized");

	_state = _workcell->getDefaultState();

	_manipulator = _workcell->findDevice<SerialDevice>(_manipulatorName);
	if (_manipulator == NULL)
	{
		RW_THROW("Unable to find Manipulator with name " << _manipulatorName << " in work cell");
	}
	_qHome = _manipulator->getQ(_state);
	_ikSolver = ownedPtr(new ClosedFormIKSolverUR(_manipulator, _state));
    
	_mobileBase = _workcell->findFrame<MovableFrame>(_mobileBaseName);
	if (_mobileBase == NULL)
	{
		ER_LOG_INFO("Could not find a Mobile Base with name "<<_mobileBaseName);
//		RW_THROW("Unable to find Mobile Base with name " << _mobileBaseName << " in work cell");
	}

	_toolTCPFrame = _workcell->findFrame<FixedFrame>(_toolTcpName);
	if (_toolTCPFrame == NULL)
	{
		RW_THROW("Unable to find Tool TCP Frame with name " << _toolTcpName << " in work cell");
	}

	_calibrationTCPFrame = _workcell->findFrame<FixedFrame>(_calibrationTCPName);
	if (_calibrationTCPFrame == NULL)
	{
		RW_THROW("Unable to find Calibration TCP Frame with name " << _calibrationTCPName << " in work cell");
	}

	/*if (_sensorFrameName != "") {
	    _sensorFrame = _workcell->findFrame<FixedFrame>(_sensorFrameName);
		if (_sensorFrame == NULL)
		{
			RW_THROW("Unable to find Tool Scanner Frame with name " << _sensorFrameName << " in work cell");
		}
	}*/

	_collisionDetector = ownedPtr(new CollisionDetector(_workcell, ProximityStrategyFactory::makeDefaultCollisionStrategy()));

	_mobileBaseCollisionDetector = _collisionDetector;

	_knowledgeBase = ownedPtr(new PropertyMap());

	_languageChanged = false;
	_language = "English";
}

void System::setState(const rw::kinematics::State& state) 
{
	boost::mutex::scoped_lock lock(_mutexState);
	_state = state;
	if (_isStateRecording) {
		_statePath.push_back(TimedState(0.1*_statePath.size(), state));
	}
}


rw::kinematics::State System::getState() const {
	boost::mutex::scoped_lock lock(_mutexState);
	return _state;
} 


void System::setWorkCell(rw::models::WorkCell::Ptr workcell)
{
	_workcell = workcell;
	_state = workcell->getDefaultState();
}

rw::models::WorkCell::Ptr System::getWorkCell()
{
	return _workcell;
}


void System::addWorkCellCalibrationFile(const std::string& id, const std::string& filename)
{
    _calibrationFiles[id] = filename;
}

void System::addUpdateRule(UpdateRule::Ptr rule)
{
	boost::mutex::scoped_lock lock(_mutexUpdateRule);
	_updateRules.push_back(rule);
}

void System::removeUpdateRuleForEntity(Entity::Ptr entity){
	boost::mutex::scoped_lock lock(_mutexUpdateRule);
	for (std::vector<UpdateRule::Ptr>::iterator it = _updateRules.begin(); it != _updateRules.end(); ++it) {		
		if ((*it)->getEntity() == entity) {
			it = _updateRules.erase(it);			
			if (it == _updateRules.end())
				break;
		}
	}
}

void System::update()
{
//	boost::mutex::scoped_lock lock(_updateMutex);
//	ER_LOG_DEBUG("Updating system");
	State state = _state;
	for (UpdateRule::Ptr rule : _updateRules) 
	{
		try {
			rule->update(state);
		}
		catch (const std::exception& exp) {
			ER_LOG_ERROR("System::update: Exception: " << exp.what());
		} 
		catch (...) {
			ER_LOG_ERROR("System::update: Unknown Exception");
		}
	}
//	boost::mutex::scoped_lock lock1(_mutexState);
	_state = state;
//	ER_LOG_DEBUG("Done updating system");
} 

//
//void System::setPointCloud(const std::vector < rw::math::Vector3D<> >& points)
//{
//	_pointCloud = points;
//}
//
//const std::vector<rw::math::Vector3D<> >& System::getPointCloud() const
//{
//	return _pointCloud;
//}

PropertyMap::Ptr System::getKnowledgeBase()
{
	return _knowledgeBase;
}

std::vector<Frame*> System::getReferenceFrames()
{
	std::vector<Frame*> result;
	for (Frame* frame : _workcell->getFrames()) {
		result.push_back(frame);
	}
	return result;
}

rw::models::SerialDevice::Ptr System::getManipulator() const
{
	return _manipulator;
}


rw::kinematics::MovableFrame::Ptr System::getMobileBase() const
{
	return _mobileBase;
}

void System::setToolTCP(FixedFrame::Ptr toolTCPFrame) {
	_toolTCPFrame = toolTCPFrame;
}

rw::kinematics::FixedFrame::Ptr System::getToolTCP() const
{
	return _toolTCPFrame;
}


void System::setCalibrationTCP(FixedFrame::Ptr calibrationTCPFrame) {
	_calibrationTCPFrame = calibrationTCPFrame;
}

rw::kinematics::FixedFrame::Ptr System::getCalibrationTCP() const
{
	return _calibrationTCPFrame;
}

void System::setReferenceFrame(rw::kinematics::FixedFrame::Ptr referenceFrame)
{
	_referenceFrame = referenceFrame;
}

FixedFrame::Ptr System::getReferenceFrame() const
{
	return _referenceFrame;
}

/*
void System::setSensorFrame(rw::kinematics::FixedFrame::Ptr sensorFrame)
{
	_sensorFrame = sensorFrame;
}

rw::kinematics::FixedFrame::Ptr System::setSensorFrame()
{
	return sensorFrame;
}
*/

void System::setManipulatorIKSolver(rw::invkin::InvKinSolver::Ptr ikSolver)
{
	_ikSolver = ikSolver;
}

rw::invkin::InvKinSolver::Ptr System::getManipulatorIKSolver() const
{
	if (_ikSolver == NULL) {
		RW_THROW("The IK Solver for the Manipulator has not be correctly initialized");
	}
	return _ikSolver;
}


rw::proximity::CollisionDetector::Ptr System::getMobileBaseCollisionDetector() const
{
	return _mobileBaseCollisionDetector;
}


void System::setCollisionDetector(rw::proximity::CollisionDetector::Ptr collisionDetector)
{
	_collisionDetector = collisionDetector;
}


rw::proximity::CollisionDetector::Ptr System::getCollisionDetector() const
{
	return _collisionDetector;
}


void System::setMobileBaseCollisionDetector(rw::proximity::CollisionDetector::Ptr collisionDetector)
{
	_mobileBaseCollisionDetector = collisionDetector;
}

void System::setQHome(const rw::math::Q& qhome)
{
	_qHome = qhome;
}

rw::math::Q System::getQHome() const
{
	return _qHome;
}


QConstraint::Ptr System::getManipulatorConstraint(const State& state) const
{
	std::vector<QConstraint::Ptr> constraints;

	Q activationVector = Q::zero(6);
	activationVector(0) = 1;
	QConstraint::Ptr qconstraint = ownedPtr(new CartesianQConstraint(_manipulator->getBase(), getToolTCP().get(), _manipulator, _state, activationVector, 1.6, CartesianQConstraint::InsideValid));
	constraints.push_back(qconstraint);

	QConstraint::Ptr colconstraint = QConstraint::make(getCollisionDetector(), _manipulator, state);
	constraints.push_back(colconstraint);

	return QConstraint::makeMerged(constraints);

}

ManipulatorQ2QPlanner::Ptr System::getManipulatorQ2QPlanner(const State& state) const
{
	ManipulatorQ2QPlanner::Ptr manipulatorPlanner = ownedPtr(new ManipulatorQ2QPlanner(getManipulator(), getManipulatorJointMotionMetric(), getManipulatorConstraint(state)));
	return manipulatorPlanner;
}

rw::math::QMetric::Ptr System::getManipulatorJointMotionMetric() const
{
	if (_manipulatorJointMotionMetric == NULL) {
		Q qweights = PlannerUtil::estimateMotionWeights(*_manipulator, NULL, _state, PlannerUtil::AVERAGE, 100);
		_manipulatorJointMotionMetric = MetricFactory::makeWeightedEuclidean<Q>(qweights);
	}
	return _manipulatorJointMotionMetric;
}


std::string System::getDataFolder(){
	return _dataFolder;
}

void System::setDataFolder(const std::string& foldername){
    _dataFolder = foldername;
}

void System::setProgramFolder(const std::string& foldername)
{
    _programFolder = foldername;
}

std::string System::getProgramFolder() const
{
    return _programFolder;
}


std::string System::getMarkerImageFolder() {
    return _markerImageFolder;
}

void System::setMarkerImageFolder(const std::string& foldername) {
    _markerImageFolder = foldername;
}

void System::setSystemFolder(const std::string& systemFolder)
{
    _systemFolder = systemFolder;
}

std::string System::getSystemFolder() const
{
    return _systemFolder;
}

void System::setCalibrationDataFilename(const std::string& filename){
	_calibrationDataFilename = filename;
}

std::string System::getCalibrationDataFilename() const{
	return _calibrationDataFilename;
}

void System::setSaveImages(const bool saveImages){
	_saveImages = saveImages;
}

bool System::getSaveImages() const {
	return _saveImages;
}

void System::setManipulator(std::string deviceName, std::string workcellFilename){
	boost::mutex::scoped_lock lock(_updateMutex);

	_workcellFileName = workcellFilename;
	_manipulatorName = deviceName;

	ER_LOG_DEBUG("Setting device: " << _manipulatorName << " with workcell: " << _workcellFileName);

	if (_workcellFileName.size() > 0) {
        if (StringUtil::isAbsoluteFileName(_workcellFileName)) {
            _workcell = WorkCellLoader::Factory::load(_workcellFileName);
        }
        else {
            _workcell = WorkCellLoader::Factory::load(getSystemFolder() + _workcellFileName);
        }
		if (_workcell == NULL) {
			RW_THROW("Unable to load workcell: " << _workcellFileName);
		}
	}

	initialize();

	_wcChangedMutex.lock();
	_wcChanged = true;
	_wcChangedMutex.unlock();
}

bool System::popWorkcellChanged(){
	_wcChangedMutex.lock();
	bool wcchanged = _wcChanged;
	_wcChanged = false;
	_wcChangedMutex.unlock();

	return wcchanged;
}

void System::changeLanguage(std::string lan){
	boost::mutex::scoped_lock(_mutexLanguage);
	_language = lan;
	_languageChanged = true;
}

bool System::popChangedLanguage() const{
	boost::mutex::scoped_lock(_mutexLanguage);
	if(!_languageChanged)
		return _languageChanged;
	else{
		bool langchanged = _languageChanged;
		_languageChanged = false;
		return langchanged;
	}
}

std::string System::getLanguage() const{
	boost::mutex::scoped_lock(_mutexLanguage);
	return _language;
}

/**
* @copydoc Serializable::read
*/
void System::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
	ER_LOG_DEBUG("Read in system ");
    EntityRegister::clear();

	iarchive.readEnterScope("System");

		// Only check if we are running linux
	#ifdef __linux__
		if(iarchive.has("LicenseFile")){
			ER_LOG_INFO("Look for license file");
			_copyProtection = new er_security::CopyProtector;
			_licenseFile = iarchive.read<std::string>("LicenseFile");
			ER_LOG_INFO("Trying to unlock system with file: " << getSystemFolder() + _licenseFile);
			_copyProtection->unlock(getSystemFolder() + _licenseFile, "EnabledRobotics");

			if(!_copyProtection->isLegal())
				RW_THROW("Copy Protection could not be unlocked");
		}
	#endif

	ER_LOG_DEBUG( "Look for work cell ");
	iarchive.readEnterScope("WorkCell");

	_workcellFileName = iarchive.read<std::string>("FileName");
	ER_LOG_DEBUG("Got Work cell filename " + _workcellFileName);
	if (_workcellFileName.size() > 0) {
        if (StringUtil::isAbsoluteFileName(_workcellFileName)) {
            _workcell = WorkCellLoader::Factory::load(_workcellFileName);
        }
        else {
            _workcell = WorkCellLoader::Factory::load(getSystemFolder() + _workcellFileName);
        }
		if (_workcell == NULL) {
			RW_THROW("Unable to load workcell: " << _workcellFileName);
		}
	}

	iarchive.readEnterScope("FrameNames");

	_manipulatorName = iarchive.read<std::string>("Manipulator");

	if(iarchive.has("MobileBase"))
		_mobileBaseName = iarchive.read<std::string>("MobileBase");

	_toolTcpName = iarchive.read<std::string>("ToolTCP");
	_calibrationTCPName = iarchive.read<std::string>("CalibrationTCP");
	//_toolSensorFrameName = iarchive.read<std::string>("Sensor");

	iarchive.readLeaveScope("FrameNames");

	iarchive.readLeaveScope("WorkCell");

	if(iarchive.has("HWEntities")){
		iarchive.readEnterScope("HWEntities");
		std::vector<std::pair<std::string, er_serialization::InputArchive::Ptr> > subarchs = iarchive.getSubArchives();
		ER_LOG_INFO("CREATE ENTITIES FROM REPOSITORY "<<subarchs.size());

		for (auto sa : subarchs) {
			ER_LOG_DEBUG("Got entity " + sa.first);
			hwl::HWEntity::Ptr entity = EntityRepository<hwl::HWEntity>::make(sa.first);
			if (entity == NULL) {
			    ER_LOG_ERROR("Unable to constrct Entity with id "<<sa.first);
			    abort();
			}
			entity->read(*(sa.second), sa.first);
			EntityRegister::add(entity->getName(), entity);
	        //std::cout<<"\tNumber of entities of type HWEntity"<<EntityRegister::findMatches<HWEntity>.size()<<std::endl;
	        //std::cout<<"\tNumber of entities of type Entity"<<EntityRegister::findMatches<Entity>().size()<<std::endl;
		}
		ER_LOG_INFO("FINISHED WITH HARDWARE ENTITIES FROM SYSTEM " );
		iarchive.readLeaveScope("HWEntities");
	}

    if(iarchive.has("SWEntities")){
        iarchive.readEnterScope("SWEntities");
        std::vector<std::pair<std::string, er_serialization::InputArchive::Ptr> > subarchs = iarchive.getSubArchives();
        ER_LOG_INFO("CREATE SOFTWARE ENTITIES FROM REPOSITORY "<<subarchs.size());

        for (auto sa : subarchs) {
            ER_LOG_DEBUG("Got entity " + sa.first);
            er_common::SWEntity::Ptr entity = EntityRepository<er_common::SWEntity>::make(sa.first);
            if (entity == NULL) {
                ER_LOG_ERROR("Unable to constrct Entity with id "<<sa.first);
                abort();
            }
            entity->read(*(sa.second), sa.first);
            EntityRegister::add(entity->getName(), entity);
        }
        ER_LOG_INFO("FINISHED WITH SOFTWARE ENTITIES FROM SYSTEM " );
        iarchive.readLeaveScope("SWEntities");
    }


	if(iarchive.has("CalibrationFiles")){
		iarchive.readEnterScope("CalibrationFiles");
		_calibrationFiles.clear();
		std::vector<std::pair<std::string, er_serialization::InputArchive::Ptr> > calibarchs = iarchive.getSubArchives();
		for (std::pair<std::string, er_serialization::InputArchive::Ptr> arch : calibarchs) {
			if (arch.first == "CalibrationFile") {
				std::string id = arch.second->read<std::string>("Id");
				std::string file = arch.second->read<std::string>("Filename");
				_calibrationFiles[id] = file;
			}
		}
		iarchive.readLeaveScope("CalibrationFiles");
	}

	if(iarchive.has("CalibrationData")){
		iarchive.readEnterScope("CalibrationData");
		_calibrationDataFilename = iarchive.read<std::string>("Filename");
		iarchive.readLeaveScope("CalibrationData");
	}
	else
		_calibrationDataFilename = "-1";


	if(iarchive.has("RobotDatabase")){
		er_common::SWEntity::Ptr entity = EntityRepository<er_common::SWEntity>::make("RobotDatabase");
		entity->read(iarchive, "RobotDatabase");
		EntityRegister::add(entity->getName(), entity);
	}

	if(iarchive.has("MarkerDatabase")){
		er_common::SWEntity::Ptr entity = EntityRepository<er_common::SWEntity>::make("MarkerDatabase");
		ER_LOG_DEBUG("Name of new entity = " << entity->getName());
		entity->read(iarchive, "MarkerDatabase");
		EntityRegister::add(entity->getName(), entity);
	}

	iarchive.readLeaveScope("System");

}

/**
* @copydoc Serializable::write
*/
void System::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{

	ER_LOG_DEBUG("This thread: " << std::this_thread::get_id());
	oarchive.writeEnterScope("System");

	if(_licenseFile != "")
		oarchive.write(_licenseFile, "LicenseFile");

	oarchive.writeEnterScope("WorkCell");

	ER_LOG_DEBUG("_workcellFileName:");
	ER_LOG_DEBUG(_workcellFileName);

	oarchive.write(_workcellFileName, "FileName");
	oarchive.writeEnterScope("FrameNames");

	oarchive.write(_manipulatorName, "Manipulator");

	oarchive.write(_mobileBaseName, "MobileBase");
	oarchive.write(_toolTcpName, "ToolTCP");
	oarchive.write(_calibrationTCPName, "CalibrationTCP");
	//oarchive.write(_toolSensorFrameName, "Sensor");

	oarchive.writeLeaveScope("FrameNames");
	oarchive.writeLeaveScope("WorkCell");

	oarchive.writeEnterScope("HWEntities");

	std::vector<hwl::HWEntity::Ptr> entities = EntityRegister::getAll<hwl::HWEntity>();
	ER_LOG_DEBUG("Number of entities " + entities.size() );
	for (hwl::HWEntity::Ptr entity : entities) {
		ER_LOG_DEBUG("Write Entity " + entity->getName());
		if (entity->isChild() == false)
            entity->write(oarchive, "");
	}

	oarchive.writeLeaveScope("HWEntities");

	oarchive.writeEnterScope("CalibrationFiles");
    for (auto element : _calibrationFiles) {
    	ER_LOG_DEBUG(element.first);
    	ER_LOG_DEBUG(element.second);
        oarchive.writeEnterScope("CalibrationFile");
        oarchive.write(element.first, "Id");
        oarchive.write(element.second, "Filename");
        oarchive.writeLeaveScope("CalibrationFile");
    }
	oarchive.writeLeaveScope("CalibrationFiles");

	if(_calibrationDataFilename != ""){
		oarchive.writeEnterScope("CalibrationData");
        oarchive.write(_calibrationDataFilename, "Filename");
		oarchive.writeLeaveScope("CalibrationData");
	}

	std::vector<er_common::SWEntity::Ptr> swentities = EntityRegister::getAll<er_common::SWEntity>();
	ER_LOG_DEBUG("Number of entities " + swentities.size() );
	for (er_common::SWEntity::Ptr entity : swentities) {
		entity->write(oarchive, "");
	}

	oarchive.writeLeaveScope("System");
}


