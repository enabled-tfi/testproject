#include "Settings.hpp"
#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>

using namespace er_common;
using namespace er_serialization;
using rw::common::ownedPtr;

Settings::Settings() {}

Settings::~Settings()
{
    _settings.clear();
}

Settings::Settings(const Settings& other)
{
    // Clone all property base objects.
	for (MapType::iterator it = other._settings.begin(); it != other._settings.end(); it++) {
		const SettingBase::Ptr base = *it;
        this->insert(base->clone());
    }
}

bool Settings::add(SettingBase::Ptr property) {
    return insert(property);
}


void Settings::add(Settings::Ptr settings, bool clone) {	
	for (MapType::iterator it = settings->getSettings().first; it != settings->getSettings().second; it++) {
		const SettingBase::Ptr base = *it;		
		if (clone) {
			this->insert(base->clone());
		}
		else {
			this->insert(base);
		}
	}
}

Settings& Settings::operator=(const Settings& other)
{
    // Assignment operator by the swap-idiom.
    if (this != &other) {
		Settings copy = other;
        swap(copy);
    }
    return *this;
}

void Settings::swap(Settings& other)
{
    _settings.swap(other._settings);
}

bool Settings::has(const std::string& identifier) const
{
    return findSettingBase(identifier) != NULL;
}

bool Settings::erase(const std::string& identifier)
{
    Setting<int> key(identifier, "", 0);

    typedef MapType::iterator I;
    const I p = _settings.find(&key);
    if (p != _settings.end()) {
		_settings.erase(p);
        return true;
    } else {
        return false;
    }
}

size_t Settings::size() const
{
    return _settings.size();
}

bool Settings::empty() const
{
    return _settings.empty();
}

bool Settings::insert(SettingBase::Ptr property)
{
    if(_settings.insert(property).second ){
        // add to changed listener
        //property->addChangedListener( boost::bind(&PropertyMap::propertyChangedListener,this,_1) );
        return true;
    }
    return false;
}

SettingBase::Ptr Settings::findSettingBase(const std::string& identifier)
{
    Setting<int> key(identifier, "", 0);
    typedef MapType::iterator I;
    const I p = _settings.find(&key);
    if (p != _settings.end())
        return *p;
    return NULL;
}

const SettingBase::Ptr Settings::findSettingBase(const std::string& identifier) const
{
    return const_cast<Settings*>(this)->findSettingBase(identifier);
}

std::pair<Settings::iterator, Settings::iterator> Settings::getSettings() const
{
    return std::make_pair(_settings.begin(), _settings.end());
}

void Settings::notifyListeners(SettingBase::Ptr base) {
    typedef std::vector<SettingChangedListener>::iterator I;
    for (I it = _listeners.begin(); it != _listeners.end(); ++it) {
        (*it)(this, base);
    }
}

void Settings::addChangedListener(SettingChangedListener callback) {
    _listeners.push_back(callback);
}

void Settings::propertyChangedListener(SettingBase::Ptr base){
    std::string id = base->getIdentifier();
    //ER_LOG_DEBUG("PropertyMap: Property Changed Listerner: " << id );
    // notify all listeners
    notifyListeners(base);
}


void Settings::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{

	//_settings.clear();
	iarchive.readEnterScope("Settings");
	_name = iarchive.read<std::string>("Name");
	std::vector<rw::common::Ptr<InputArchive> > iarchives = iarchive.getSubArchivesForId("Setting");
	ER_LOG_DEBUG("Number of archives " << iarchives.size() );
	for (rw::common::Ptr<InputArchive> ia : iarchives)
	{
		std::string identifier = ia->read<std::string>("Identifier");
		
		std::string description = ia->read<std::string>("Description");
		int datatype = ia->read<int>("DataType");
		int usertype = ia->read<int>("UserType");
		int inputtype = ia->read<int>("InputType");
		SettingBase::Ptr setting;
		ER_LOG_DEBUG("Found Setting " << identifier << " " << datatype );
		setting = findSettingBase(identifier);
		ER_LOG_DEBUG("Setting = " << setting );
		if (setting == NULL) {
			switch (datatype)
			{
			case DataType::Settings:
			{
				RW_THROW("Storing a Setting Object of	 type Setting is not supported yet");
				break;
			}
			case DataType::String:
				setting = ownedPtr(new Setting<std::string>(identifier, description, "", usertype, inputtype));
				break;
			case DataType::Float:
				setting = ownedPtr(new Setting<float>(identifier, description, 0.0f, usertype, inputtype));
				break;
			case DataType::Double:
				setting = ownedPtr(new Setting<double>(identifier, description, 0.0, usertype, inputtype));
				break;
			case DataType::Int:
				setting = ownedPtr(new Setting<int>(identifier, description, 0, usertype, inputtype));
				break;
			case DataType::Bool:
				setting = ownedPtr(new Setting<bool>(identifier, description, true, usertype, inputtype));
				break;
			case DataType::Vector3D:
				setting = ownedPtr(new Setting<rw::math::Vector3D<> >(identifier, description, datatype, usertype, inputtype));
				break;
			case DataType::Vector2D:
				setting = ownedPtr(new Setting<rw::math::Vector2D<> >(identifier, description, datatype, usertype, inputtype));
				break;
			case DataType::Q:
				setting = ownedPtr(new Setting<rw::math::Q>(identifier, description, datatype, usertype, inputtype));
				break;
			default:
				RW_THROW("Unknown data type");
				break;
			}
		}
		try {
			setting->read(*ia, "Value");			
		}
		catch (const std::exception& exp) {
			ER_LOG_DEBUG("Exception: " << exp.what() << " with " << identifier );
		}
		if (datatype == DataType::Settings)
		{
			RW_THROW("Current loader does not support loading nested settings");
			//setting = ownedPtr(new Setting<Settings>(identifier, description, datatype, usertype, inputtype));
			//setting->read(*ia, "Value");
		}
		//add(setting);
		

	}

	iarchive.readLeaveScope("Settings");
}

void Settings::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	oarchive.writeEnterScope("Settings");
	oarchive.write(_name, "Name");
	for (MapType::const_iterator it = _settings.begin(); it != _settings.end(); ++it) {
		oarchive.writeEnterScope("Setting");
		oarchive.write((*it)->getIdentifier(), "Identifier");
		oarchive.write((*it)->getDescription(), "Description");
		oarchive.write((*it)->getDataType().getId(), "DataType");
		oarchive.write((*it)->getUserType().getMask(), "UserType");
		oarchive.write((*it)->getInputType().getId(), "InputType");
		(*it)->write(oarchive, "Value");
		
		/*switch ((*it)->getType().getId()) {
		case DataType::String:
			oarchive.write((*it).cast<Setting<std::string> >(), "Value");
			break;
		case DataType::Float:
			oarchive.write((*it).cast<Setting<float> >(), "Value");
			break;
		case DataType::Double:
			oarchive.write((*it).cast<Setting<double> >(), "Value");
			break;
		case DataType::Int:
			oarchive.write((*it).cast<Setting<int> >(), "Value");
			break;
		case DataType::Bool:
			oarchive.write((*it).cast<Setting<bool> >(), "Value");
			break;
		case DataType::Vector3D:
			oarchive.write((*it).cast<Setting<rw::math::Vector3D<> > >(), "Value");
			break;
		case DataType::Vector2D:
			oarchive.write((*it).cast<Setting<rw::math::Vector2D<> > >(), "Value");
			break;
		case DataType::Q:
			oarchive.write((*it).cast<Setting<rw::math::Q> >(), "Value");
			break;
		default:
			RW_THROW("Data Type not supported yet");
			break;

		}*/

		/*
			Unknown = -1,            //!< Unknown type
			Settings = 0,            //!< Settings
			String,                  //!< std::string
			Float,                   //!< float
			Double,                  //!< double
			Int,                     //!< int
			Bool,                    //!< bool
			Vector3D,                //!< rw::math::Vector3D
			Vector2D,                //!< rw::math::Vector2D
			Q,                       //!< rw::math::Q
			Transform3D,             //!< rw::math::Transform3D
			Rotation3D,              //!< rw::math::Rotation3D
			RPY,                     //!< rw::math::RPY
			EAA,                     //!< rw::math::EAA
			Quaternion,              //!< rw::math::Quaternion
			Rotation2D,              //!< rw::math::Rotation2D
			VelocityScrew6D,         //!< rw::math::VelocityScrew6D
			QPath,                   //!< rw::trajectory::QPath
			QPathPtr,                //!< rw::trajectory::QPath::Ptr
			Transform3DPath,         //!< rw::trajectory::Transform3DPath
			Transform3DPathPtr,      //!< rw::trajectory::Transform3DPath::Ptr
			StringList,              //!< std::vector<std::string>
			IntList,                 //!< std::vector<int>
			DoubleList,              //!< std::vector<double>
			*/



		oarchive.writeLeaveScope("Setting");
	}
	oarchive.writeLeaveScope("Settings");
}
