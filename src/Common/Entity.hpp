/* */
#ifndef ER_COMMON_ENTITY_HPP
#define ER_COMMON_ENTITY_HPP


#include "Serializable.hpp"
#include "InputArchive.hpp"
#include "OutputArchive.hpp"

#include <rw/common/Ptr.hpp>
#include <rw/common/PropertyMap.hpp>



namespace er_common {
	
/**
 * Interface for Entities which has a name and description and implements
 * the serializable interface
 */
class Entity: public er_common::Serializable
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<Entity> Ptr;


	/**
     * @brief Constructor
     * @param name [in] Name of the entity. Defaults to ""
     * @param description [in] Description of entity. Defaults to "void"
     */
    Entity(const std::string& name = "", const std::string& description = "void") :
        _name(name),
        _description(description),
        _version(3, 0)
	{
	}

	
	//! destructor
    virtual ~Entity(){};

    /**
     * @brief Sets the version number for the entity.
     *
     * A version number is comprised of three integers constituting major, minor and revision.
     * @param major [in] The major revision number
     * @param minor [in] The minor revision number
     * @param revision [in] The revision number
     */
    void setVersion(int major, int minor, int revision);

    /**
     * @brief Returns the version of the entity
     *
     * A version is comprised of three integers constituting major, minor and revision.
     * @return List with the version in the order major, minor, revision.
     */
    const std::vector<int>& getVersion() const;


    /**
     * @brief Returns the name of the Entity
     * @return Name
     */
    const std::string& getName() const {
        return _name;
    }

	/**
	* @brief Returns the description of the Entity
	* @return Name
	*/
	const std::string& getDescription() const {
		return _description;
	}


	/* Methods from the serializable interface */
    /**
     * @copydoc Serializable::read
     */
	virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);
	
    /**
    * @copydoc Serializable::write
    */
    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;



private:
    std::string _name;
	std::string _description;
    std::vector<int> _version;
};

} //end namespace

#endif //#ifndef COMMON_ENTITY_HPP
