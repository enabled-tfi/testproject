#include "EntityFactory.hpp"

using namespace er_common;

EntityFactoryBase::EntityFactoryBase(const std::string& identifier, const std::string& description):
    _identifier(identifier),
    _description(description)
{

}

const std::string& EntityFactoryBase::getIdentifier() const
{
    return _identifier;
}


const std::string& EntityFactoryBase::getDescription() const
{
    return _description;
}


