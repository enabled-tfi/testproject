#include "FrameReference.hpp" 

#include <Common/System.hpp>

using namespace er_common; 
using namespace rw::math;
using namespace rw::kinematics;


FrameReference::FrameReference()
{

}

FrameReference::FrameReference(const std::string& referenceFrameName):
	_reference(referenceFrameName)
{

}

FrameReference::FrameReference(rw::kinematics::Frame* referenceFrame):
	_reference(referenceFrame)
{

}

rw::kinematics::Frame* FrameReference::get() const
{
	try {
		return boost::get<Frame*>(_reference);
	}
	catch (const boost::bad_get&) {
		if (System::isInitialized() == false) {
			RW_THROW("Reference Frame Not Initialized");
		}
		try {
			std::string refframe = boost::get<std::string>(_reference);
			return System::getInstance()->getWorkCell()->findFrame(refframe);
		}
		catch (boost::bad_get&) {
			RW_THROW("No reference frame or reference frame name initializezd for Target ");
		}
	}
	return NULL;
}

std::string FrameReference::getName() const
{
	if (get() != NULL)
		return get()->getName();
	else
		return boost::get<std::string>(_reference);
}


void FrameReference::set(const std::string& referenceFrameName)
{
	_reference = referenceFrameName;
}

void FrameReference::set(rw::kinematics::Frame* referenceFrame)
{
	_reference = referenceFrame;
}

std::vector<std::string> FrameReference::getNameOptions() const
{
	std::vector<std::string> result;
	std::vector<Frame*> frames = System::getInstance()->getWorkCell()->getFrames();
	for (Frame* frame : frames) {
		result.push_back(frame->getName());
	}
	return result;
//	return System::getInstance()->getWorkCell()->findFrame(refframe);
}


//void FrameReference::read(er_serialization::InputArchive& iarchive, const std::string& id)
//{
//	iarchive.readEnterScope(id);
//	std::string reference = iarchive.read<std::string>("Reference");
//	setReference(reference);
//	iarchive.readLeaveScope(id);
//
//}
//
//void FrameReference::write(er_serialization::OutputArchive& oarchive, const std::string& id) const
//{
//	oarchive.writeEnterScope(id);
//	oarchive.write(getReferenceFrameName(), "Reference");
//	oarchive.writeLeaveScope(id);	
//}
