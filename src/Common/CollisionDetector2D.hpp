/* */
#ifndef ER_COMMON_COLLISIONDECTECTOR2D_HPP
#define ER_COMMON_COLLISIONDECTECTOR2D_HPP

#include <Common/Target.hpp>
#include <ExecutionModel/Instruction.hpp>
#include <ExecutionModel/ExecutionManager.hpp>

#include <rw/math/Vector2D.hpp>
#include <rw/pathplanning/StateConstraint.hpp>

#include <string>



namespace er_common {

    /**
     * @brief CollisionDetector2D provides a mean for checking whether points of the robot is colliding with any obstacles defined in a image.
     *
     * The CollisionDetector uses the colors as defined by MIR. Colors ,black (obstacles), gray (undefined), red (forbidden) all results in collision.
     * The colors white (free), green (preferred), blue (nonpreferred) and yellow (critical) are all accepted.
     */
    class CollisionDetector2D: public rw::pathplanning::StateConstraint
	{
	public:
        /** @brief Pointer to object definition */
        typedef rw::common::Ptr<CollisionDetector2D> Ptr;

        /** 
         * @brief Construct CollisionDetector2D
         * @param image [in] Color image defining the environment
         * @param xoffset [in] Offset along x-axis between image plane and the origo of the robot
         * @param yoffset [in] Offset along y-axis between image plane and the origo of the robot
         * @param resolution [in] The distance (in meters) of a single pixel in the map
         * @param frame [in] The reference frame of the mobile robot
         * @param outline [in] Set of points defining the outline of the robots. The robot is assumed to occupy the convex hull of the outline points.
         */
        CollisionDetector2D(const rw::sensor::Image::Ptr image, double xoffset, double yoffset, double resolution, rw::kinematics::Frame* frame, const std::vector<rw::math::Vector2D<> >& outline);

        /**
         * @brief Destructor
         */
        virtual ~CollisionDetector2D();

        /**
         * @Enumeration defning the classification of the current robot pose
         */
        enum Classification { FREE = 0, WALL, UNKNOWN, FORBIDDEN, PREFERRED, NONPREFERRED, CRITICAL, OUTSIDE_MAP};

        /**
         * @brief Performs a classification of the robot location for the current state.
         * 
         * In case multiple points are in collision it will be the first point checked which defines the category.         
         * @param state [in] State to check
         * @return Classification telling why state is in collision. 
         */
        Classification classify(const rw::kinematics::State& state) const;

    protected:
        /**
         * @copydoc StateConstraint::doInCollision
         */
        virtual bool doInCollision(const rw::kinematics::State& state) const;

        /**
        * @copydoc StateConstraint::doInCollision
        */
        virtual void doSetLog(rw::common::Log::Ptr log);

    private:
        rw::sensor::Image::Ptr _image;
        double _xoffset;
        double _yoffset;
        double _resolution;
        rw::kinematics::Frame* _frame;
        std::vector<rw::math::Vector2D<> > _footprint;
        rw::common::Log::Ptr _log;
        Classification _classification;
    };

} //end namespace

#endif //#ifndef ER_COMMON_COLLISIONDECTECTOR2D_HPP
