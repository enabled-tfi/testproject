#ifndef ER_COMMON_SETTINGS_HPP	
#define ER_COMMON_SETTINGS_HPP

#include "SettingBase.hpp"
#include "Setting.hpp"
#include "Serializable.hpp"
#include <rw/common/macros.hpp>
#include <Common/Logging.hpp>

#include <set>

namespace er_common {

    /**
     * @brief Container for a collection of Setting objects
     */
    class Settings: public Serializable {
    public:

        //! @brief smart pointer type to this class
        typedef rw::common::Ptr<Settings> Ptr;

        /**
         * @brief Constructor
         */
        Settings();

        /**
         * @brief constructor
         * @param name [in] name of this propertymap
         */
		Settings(std::string name):_name(name){};

        /**
         * @brief Destructor
         */
        ~Settings();

        /**
           @brief Copy constructor.
        */
		Settings(const Settings& other);

        /**
           @brief Assignment operator.
        */
		Settings& operator=(const Settings& other);

        /**
           @brief swap operator.
        */
        void swap(Settings& other);

        /**
         * @brief get the name of this propertymap
         * @return name of this propertymap
         */
        const std::string& getName(){ return _name; };

        /**
         * @brief Set the value of a property
         *
         * If a property with the given identifier cannot be found, a new
         * property with no description is created and inserted.
         *
         * @param identifier [in] the property identifier
         * @param value [in] the new value
         */
        template<class T>
        rw::common::Ptr<Setting<T> > set(const std::string& identifier, const T& value)
        {
            rw::common::Ptr<Setting<T> > prop = findSetting<T>(identifier);
            if (prop) {
                prop->setValue(value);
                return prop;
            }
            return add(identifier, "", value);
        }

        /**
         * @brief Add a property to the map. If a property with the same identifier already
         * exists then nothing is added/changed and the existing property is returned.
         *
         * @param identifier [in] Property identifier.
         * @param description [in] Property description.
         * @param value [in] Property value.
         * @return The property if added or the existing property if the identifier is already in
         *  use.
         */
        template <typename T>
        rw::common::Ptr< Setting<T> > add(const std::string& identifier,
                         const std::string& description,
                         const T& value)
        {
            rw::common::Ptr<Setting<T> > s = findSetting<T>(identifier);
            if (!s) {
                rw::common::Ptr<Setting<T> > setting = rw::common::ownedPtr(new Setting<T>(identifier, description, value));

                const bool ok = insert(setting);
                if(ok)
                    return setting;
                else
                    return NULL;
            }
            return s;
        }

        /**
         * @brief Add a setting to the map and overwrite potentially existing with same name
         *
         * @param identifier [in] identifier.
         * @param description [in] description.
         * @param value [in] value.
         * @return The setting if added or null otherwise
         */
        template <typename T>
        rw::common::Ptr<Setting<T> > addForce(const std::string& identifier,
                         const std::string& description,
                         const T& value)
        {
            rw::common::Ptr< Setting<T> > setting = findSetting<T>(identifier);
            if (!setting) {
                rw::common::Ptr<Setting<T> > setting = rw::common::ownedPtr(new Setting<T>(identifier, description, value));
                const bool ok = insert(setting);
                if(ok)
                    return setting;
                else
                    return NULL;
            }
            setting->setValue(value);
	    setting->setDescription(description);
            return setting;
        }


        /**
         * @brief Adds a setting 
         *
         * @param setting [in] Setting to be added
         *
         * @return True if added, false otherwise
         */
        bool add(SettingBase::Ptr setting);

		/**
		 * @brief Adds a collection of settings
		 *
		 * @param settings [in] Settings object with settings to add
		 * @param clone [in] True if the setting is to be clone, false if taking a shallow copy
		 * @return True if added
		 */
		void add(Settings::Ptr settings, bool clone);

        /**
         * @brief Get pointer to the value of a setting or NULL if identified not found
         *
         * @param identifier [in] the identifier of the setting
         *
         * @return pointer to the value 
         */
        template<class T>
        T* getPtr(const std::string& identifier)
        {
            rw::common::Ptr< Setting<T> > prop = findSetting<T>(identifier);
            if (prop)
                return &prop->getValue();
            else
                return NULL;
        }

        /**
         * @brief Get const pointer to value of a setting or NULL if identified not found.
         *
         * @param identifier [in] the identifier of the setting
         *
         * @return pointer to the value 
         */
        template<class T>
        const T* getPtr(const std::string& identifier) const
        {
            // Forward to non-const method.
            Settings* s = const_cast<T*>(this);
            return s->getPtr<T>(identifier);
        }

        /**
         * @brief Get the value of a property. Throws exception if identified not found
         *
         * @param identifier [in] the identifier of the setting
         *
         * @return Reference to the value of the setting
         */
        template<class T>
        T& get(const std::string& identifier)
        {
	    T* p = getPtr<T>(identifier);
            if (!p) {
                RW_THROW("Setting "<< "'" << identifier << "'"<< " could not be found");
            }
            return *p;
        }

        /**
         * @brief Get the value of a property. Throws exception if identified not found
         *
         * @param identifier [in] the identifier of the setting
         *
         * @return Reference to the value of the setting
         */
        template<class T>
        const T& get(const std::string& identifier) const
        {
            // Forward to non-const method.
            return const_cast<Settings*>(this)->get<T>(identifier);
        }

        /**
         * @brief Get the value of a setting if it exists or return default value
         *
         * @param identifier [in] the identifier of the setting
         * @param defval [in] the value that will be returned if \b identifier is not found.
         * @return Reference the value if it exists, else \b defval is returned
         *
         */
        template<class T>
        T& get(const std::string& identifier, const T& defval)
        {
            T* p = getPtr<T>(identifier);
            if (!p) {
                set<T>(identifier,defval);
                return *getPtr<T>(identifier);
            }
            return *p;
        }

		/**
		* @brief Get the value of a setting if it exists or return default value
		*
		* @param identifier [in] the identifier of the setting
		* @param defval [in] the value that will be returned if \b identifier is not found.
		* @return Const reference the value if it exists, else \b defval is returned
		*
		*/
		template<class T>
        const T& get(const std::string& identifier, const T& defval) const
        {
            const T* p = getPtr<T>(identifier);
            if (!p) {
                return defval;
            }
            return *p;
        }


        /**
         * @brief True if a specific setting exists
         *
         * @param identifier [in] The identifier of the setting
         * @return true if the setting exists
         */
        bool has(const std::string& identifier) const;

        /**
         * @brief The number of properties
         */
        size_t size() const;

        /**
         * @brief True iff the property map contains no properties.
         */
        bool empty() const;

        /**
         * @brief Remove a setting
         *
         * @return true if the setting was successfully removed.
         */
        bool erase(const std::string& identifier);

        // The following methods are rarely used and are therefore given longer
        // names. They more strongly expose the internal use of Property<T>.

        /**
         * @brief Find the property for an identifier or returns NULL if it does not exists or does not match the type.
         *
         * @param identifier [in] identifier of setting
         *
         * @return Setting object with that identifier
         */
        template<class T>
        rw::common::Ptr<Setting<T> > findSetting(const std::string& identifier) const
        {
            return findSettingBase(identifier).cast<Setting<T> >();
        }

        /**
         * @brief Find the setting base for an identifier. Returns NULL if is does not exists
         * @param identifier [in] identifier for the setting
		 * @return Pointer to Setting base object
         */
        SettingBase::Ptr findSettingBase(const std::string& identifier);

        /**
         * @brief Find the setting base for an identifier.
         * @param identifier [in] identifier for setting.
		 * @return Const pointer to Setting base object
		 */
        const SettingBase::Ptr findSettingBase(const std::string& identifier) const;


        /**
         * @brief Method signature for a callback function
         */
        typedef boost::function<void(Settings*, SettingBase::Ptr)> SettingChangedListener;

        /**
         * @brief Add listener to be call, when the property changes
         * @param callback [in] Callback method
         */
        void addChangedListener(SettingChangedListener callback);

        /**
         * @brief Notifies listeners about a change in the Property
         */
        void notifyListeners(SettingBase::Ptr base=NULL);

        /**
         * @brief used for listening for property changes in the map
         * @param base
         */
        void propertyChangedListener(SettingBase::Ptr base);


		/**
		* @copydoc Serializable::read 
		*/
		virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

		/**
		 * @copydoc Serializable::write
		 */
		virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

    private:
        struct CmpPropertyBase
        {
            bool operator()(
                const SettingBase::Ptr a,
                const SettingBase::Ptr b) const
            {
                return a->getIdentifier() < b->getIdentifier();
            }
        };

        typedef std::set<SettingBase::Ptr, CmpPropertyBase> MapType;

    public:
        //! Iterator for const SettingBase::Ptr
        typedef MapType::const_iterator iterator;

        //! @brief Type for a range of properties.
        typedef std::pair<iterator,iterator> Range;

        /**
           @brief Range of all PropertyBase* objects stored.

           Note that this low-level interface does permits the PropertyBase
           values to be modified even though the method itself is declared
           const.
        */
        Range getSettings() const;

		iterator begin() {
			return _settings.begin();
		}

		iterator end() {
			return _settings.end();
		}

    private:
        bool insert(SettingBase::Ptr setting);

    private:
        MapType _settings;
        std::string _name;

        /**
         * @brief PropertyChanged Listeners
         */
        std::vector<SettingChangedListener> _listeners;
    };

    /** @} */
} // end namespaces

#endif // end include guard
