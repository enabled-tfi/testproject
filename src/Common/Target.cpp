#include "Target.hpp" 

#include <Common/InputArchive.hpp>
#include <Common/OutputArchive.hpp>

#include <Common/System.hpp>

using namespace er_common; 
using namespace rw::math;
using namespace rw::kinematics;


Target::Target():
	_reference("WORLD")
{
	_isReferenceInitialized = false;
	_isTransformInitialized = false;
}

Target::Target(const std::string& referenceFrameName, const std::string& toolFrameName, const rw::math::Transform3D<>& refTtarget):
	_reference(referenceFrameName),
    _toolFrame(toolFrameName),
	_refTtarget(refTtarget)
{
	_isReferenceInitialized = true;
	_isTransformInitialized = true;
}

Target::Target(Frame* referenceFrame, Frame* toolFrame, const rw::math::Transform3D<>& refTtarget):
	_reference(referenceFrame),
    _toolFrame(toolFrame),
	_refTtarget(refTtarget)
{
	_isReferenceInitialized = true;
	_isTransformInitialized = true;
}

rw::kinematics::Frame* Target::getReference() const
{
	try {
		return boost::get<Frame*>(_reference);
	}
	catch (const boost::bad_get&) {
		if (System::isInitialized() == false) {
			RW_THROW("Reference Frame Not Initialized");
		}
		try {
			std::string refframe = boost::get<std::string>(_reference);
			return System::getInstance()->getWorkCell()->findFrame(refframe);
		}
		catch (boost::bad_get&) {
			RW_THROW("No reference frame or reference frame name initializezd for Target ");
		}
	}
	return NULL;
}

const std::string& Target::getReferenceFrameName() const
{
	if (getReference() != NULL)
		return getReference()->getName();
	else
		return boost::get<std::string>(_reference);
}

rw::kinematics::Frame* Target::getToolFrame() const
{
    try {
        return boost::get<Frame*>(_toolFrame);
    }
    catch (const boost::bad_get&) {
        if (System::isInitialized() == false) {
            RW_THROW("Tool Frame Not Initialized");
        }
        try {
            std::string refframe = boost::get<std::string>(_toolFrame);
            return System::getInstance()->getWorkCell()->findFrame(refframe);
        }
        catch (boost::bad_get&) {
            RW_THROW("No tool frame or tool frame name initializezd for Target ");
        }
    }
    return NULL;
}


const std::string& Target::getToolFrameName() const
{
    if (getToolFrame() != NULL)
        return getToolFrame()->getName();
    else
        return boost::get<std::string>(_toolFrame);
}


const Transform3D<>& Target::refTtarget() const
{
	return _refTtarget;
}

void Target::setReference(const std::string& referenceFrameName)
{
	_reference = referenceFrameName;
	_isReferenceInitialized = true;
}

void Target::setReference(rw::kinematics::Frame* referenceFrame)
{
	_reference = referenceFrame;
	_isReferenceInitialized = true;
}

void Target::setToolFrame(const std::string& toolFrameName)
{
    _toolFrame = toolFrameName;
    _isToolFrameInitialized = true;
}

void Target::setToolFrame(Frame* toolFrame)
{
    _toolFrame = toolFrame;
    _isToolFrameInitialized = true;
}


void Target::setRefTtarget(const rw::math::Transform3D<>& refTtarget)
{
	_refTtarget = refTtarget;
	_isTransformInitialized = true;
}

bool Target::isInitialized() const 
{
	return _isReferenceInitialized && _isTransformInitialized;
}


void Target::read(er_serialization::InputArchive& iarchive, const std::string& id)
{
	iarchive.readEnterScope(id);
	_isTransformInitialized = iarchive.read<bool>("IsTransformInitialized");
	if (_isTransformInitialized) {
		_refTtarget.P() = iarchive.read<Vector3D<> >("Pos");
		_refTtarget.R() = iarchive.read<RPY<> >("RPY").toRotation3D();
	}

	_isReferenceInitialized = iarchive.read<bool>("IsReferenceInitialized");
	if (_isReferenceInitialized) {
		std::string reference = iarchive.read<std::string>("Reference");
		setReference(reference);
	}

    _isToolFrameInitialized = iarchive.read<bool>("IsToolFrameInitialized");
    if (_isReferenceInitialized) {
        std::string tf = iarchive.read<std::string>("ToolFrame");
        setToolFrame(tf);
    }

    
    iarchive.readLeaveScope(id);
}

void Target::write(er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	ER_LOG_DEBUG("Target::write " );
	oarchive.writeEnterScope(id);
	
	oarchive.write(_isTransformInitialized, "IsTransformInitialized");
	if (_isTransformInitialized) {
		oarchive.write(_refTtarget.P(), "Pos");
		oarchive.write(RPY<>(_refTtarget.R()), "RPY");
		//oarchive.write(_refTtarget, "RefTTarget");
	}

	oarchive.write(_isReferenceInitialized, "IsReferenceInitialized");
	if (_isReferenceInitialized) {
		oarchive.write(getReferenceFrameName(), "Reference");
	}

    oarchive.write(_isToolFrameInitialized, "IsToolFrameInitialized");
    if (_isToolFrameInitialized) {
        oarchive.write(getToolFrameName(), "ToolFrame");
    }

    
    
    oarchive.writeLeaveScope(id);
	ER_LOG_DEBUG("Target::write finished" );
}
