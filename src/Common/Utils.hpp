/* */
#ifndef ER_UTILS_HPP
#define ER_UTILS_HPP


#include <rw/math/Transform3D.hpp>
#include <rw/math/Pose2D.hpp>
#include <rw/math/Q.hpp>
#include <rw/math/Metric.hpp>
#include <rwlibs/opengl/Drawable.hpp>
#include <rw/sensor/Image.hpp>
#include <rw/kinematics/Frame.hpp>
#include <vector>
#include <queue>

namespace er_common {
	
/**
 * Utility functionality useful for working with the mobile manipulators
 */
class Utils
{
public:
	/**
	 * @brief Converts a Pose2D into a Transform3D. 
	 * 
	 * The x,y coordinates from the pose is copied in a x and y coordinates of the results. The rotation of the pose is set as the rotation around the z-axis.
	 *
	 * To take into account the missing degrees of freedom a \b prototype needs to be provided from which missing degrees of freedom can be resolved.
	 * @param pose [in] The Pose2D to convert
	 * @param prototype [in] The Transform3D prototype from which to resolve missings degrees of freedom.
	 * @return Resulting transform
	 */
	static rw::math::Transform3D<> pose2DtoTransform3D(const rw::math::Pose2D<>& pose, const rw::math::Transform3D<>& prototype);

	/**
	 * @brief Converts a Transform3D into a Transform2D
	 * 
	 * When converting only the x,y position and the rotation around the z axis is maintained.
	 * @param t3d [in] Transform to convert
	 * @return Resulting transform 
	 */ 
	static rw::math::Transform2D<> transform3DtoTransform2D(const rw::math::Transform3D<>& t3d);

    /**
     * @brief Sorts a list of configuration s.t. the nearest is first in the queue.
     * @param qs [in] List of configurations to sort
     * @param qnear [in] The configuration to which to compare distances.
     * @param metric [in] The metric to use for comparing. 
     */
	static std::queue<rw::math::Q> sortQ(std::vector<rw::math::Q>& qs, const rw::math::Q& qnear, rw::math::QMetric::Ptr metric);

    static std::string getTimestampAsString();

    static rwlibs::opengl::Drawable::Ptr getDrawableForMap(const rw::sensor::Image& image, double xoffset, double yoffset, double resolution);

    static bool isFrameParentOf(rw::kinematics::Frame* frame, rw::kinematics::Frame* child, const rw::kinematics::State& state);

private:
    const std::string _identifier;
};




} //end namespace

#endif //#ifndef ER_UTILS_HPP
