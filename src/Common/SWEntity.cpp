
#include <src/Common/SWEntity.hpp>
#include <Common/System.hpp>

namespace er_common {

SWEntity::SWEntity(const std::string& identifier):
			er_common::Entity(identifier){
	ER_LOG_DEBUG("SW entity constructor ");
}


SWEntity::SWEntity() {
}

SWEntity::~SWEntity() {
}

/* Methods from the serializable interface */
void SWEntity::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
	Entity::read(iarchive, id);
}

void SWEntity::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	Entity::write(oarchive, id);
}


} /* namespace er_common */
