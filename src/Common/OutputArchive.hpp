/* */
#ifndef MMS_SERIALIZATION_OUTPUTARCHIVE_HPP
#define MMS_SERIALIZATION_OUTPUTARCHIVE_HPP

#include "../Common/Serializable.hpp"
#include "../Common/Entity.hpp"
//#include "../ExecutionModel/Argument.hpp"
#include <rw/common/Ptr.hpp>
#include <rw/math.hpp>
#include <rw/trajectory/Path.hpp>
namespace er_serialization {
	


/**
 * General interface for Instructions
 */
class OutputArchive
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<OutputArchive> Ptr;

	/*
	std::string addArgument(exm::Argument::Ptr argument) {
	    return doAddArgument(argument);
	}
	*/

	/**
     * @brief create a serialized scope in which objects can be written
     * @param id [in] id of the scope
     */
    void writeEnterScope(const std::string& id){
        doWriteEnterScope(id);
    }

    /**
     * @brief leave the current scope
     * @param id [in] id of the scope
     */
    void writeLeaveScope(const std::string& id){
        doWriteLeaveScope(id);
    }


    /**
     * @brief generic write method. This method will write any objects that are either derived
     * from the common::Serializable class or where write methods have been explicitly defined for the archive.
     * @param object
     * @param id
     */
    template<class T>
    void write(const T& object, const std::string& id){
        // the data method must have an implementation of load/save and if not then we try the generic write
        // method which could provide a solution by the implementation itself
        doWrite(object, id);
    }


	template<class T>
	void write(const rw::common::Ptr<T>& object, const std::string& id) 
	{

	}

protected:
	//virtual void doWrite(const er_common::Entity::Ptr& entity, const std::string& id) = 0;
    virtual void doWrite(bool val, const std::string& id) = 0;
    virtual void doWrite(int val, const std::string& id) = 0;
    virtual void doWrite(float val, const std::string& id) = 0;
    virtual void doWrite(double val, const std::string& id) = 0;
    virtual void doWrite(const std::string& val, const std::string& id) = 0;
    virtual void doWrite(const std::vector<std::string>& val, const std::string& id) = 0;
    virtual void doWrite(const rw::math::Q& q, const std::string& id) = 0;
	virtual void doWrite(const rw::math::RPY<>& rpy, const std::string& id) = 0;
    virtual void doWrite(const rw::math::Transform3D<>& t3d, const std::string& id) = 0;
    virtual void doWrite(const rw::math::Vector3D<>& v3d, const std::string& id) = 0;
    virtual void doWrite(const rw::math::Vector2D<>& v2d, const std::string& id) = 0;
    virtual void doWrite(const rw::math::Transform2D<>& v2d, const std::string& id) = 0;
    virtual void doWrite(const rw::trajectory::QPath& path, const std::string& id) = 0;
	virtual void doWrite(const er_common::Serializable& s, const std::string& id) = 0;

    virtual void doWriteEnterScope(const std::string& id) = 0;
    virtual void doWriteLeaveScope(const std::string& id) = 0;

    //virtual std::string doAddArgument(exm::Argument::Ptr argument) = 0;
};

} //end namespace

#endif //#ifndef MMS_SERIALIZATION_OUTPUTARCHIVE_HPP
