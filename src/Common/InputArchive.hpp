/* */
#ifndef ER_SERIALIZATION_INPUTARCHIVE_HPP
#define ER_SERIALIZATION_INPUTARCHIVE_HPP

#include <Common/Serializable.hpp>
#include <Common/Entity.hpp>
#include <rw/common/Ptr.hpp>
#include <rw/math.hpp>
#include <rw/trajectory.hpp>
#include <vector>

namespace er_common {
	class Serializable;
}

namespace er_serialization {
	


/**
 * General interface for Instructions
 */
class InputArchive
{
public:
	/** @brief Smart pointer to the Instruction object*/
	typedef rw::common::Ptr<InputArchive> Ptr;




	/**
     * @brief create a serialized scope in which objects can be written
     * @param id [in] id of the scope
     */
    void readEnterScope(const std::string& id){
        doReadEnterScope(id);
    }

    /**
     * @brief leave the current scope
     * @param id [in] id of the scope
     */
    void readLeaveScope(const std::string& id){
        doReadLeaveScope(id);
    }

	virtual std::vector<InputArchive::Ptr> getSubArchivesForId(const std::string& id) = 0;

	virtual std::vector<std::pair<std::string, InputArchive::Ptr> > getSubArchives() = 0;

    /**
     * @brief generic write method. This method will write any objects that are either derived
     * from the common::Serializable class or where write methods have been explicitly defined for the archive.
     * @param object
     * @param id
     */
    template<class T>
    void read(T& object, const std::string& id){
        // the data method must have an implementation of load/save and if not then we try the generic write
        // method which could provide a solution by the implementation itself
        doRead(object, id);
    }

	
    template<class T>
    T read(const std::string& id) {
        T t;
        doRead(t, id);
        return t;
    }

    template<class T>
    T read(const std::string& id, T defaultValue) {
        T t;

        if(!has(id))
        	return defaultValue;

        doRead(t, id);
        return t;
    }

	virtual bool has(const std::string& id) = 0;


protected:
    /*virtual exm::Instruction::Ptr doReadInstruction(const std::string& id) = 0;
    virtual std::vector<exm::Instruction::Ptr> doReadInstructions() = 0;
	*/
	//virtual void doRead(er_common::Entity::Ptr entity, const std::string& id) {};
	virtual void doRead(er_common::Serializable::Ptr entity, const std::string& id) {};
    virtual void doRead(bool& val, const std::string& id) = 0;
    virtual void doRead(int& val, const std::string& id) = 0;
    virtual void doRead(float& val, const std::string& id) = 0;
    virtual void doRead(double& val, const std::string& id) = 0;
    virtual void doRead(std::string& val, const std::string& id) = 0;
    virtual void doRead(std::vector<std::string>& val, const std::string& id) = 0;
    virtual void doRead(rw::math::Q& q, const std::string& id) = 0;
	virtual void doRead(rw::math::RPY<>& rpy, const std::string& id) = 0;
    virtual void doRead(rw::math::Transform3D<>& t3d, const std::string& id) = 0;
    virtual void doRead(rw::math::Vector3D<>& v3d, const std::string& id) = 0;
    virtual void doRead(rw::math::Vector2D<>& v2d, const std::string& id) = 0;
    virtual void doRead(rw::math::Transform2D<>& v2d, const std::string& id) = 0;
    virtual void doRead(rw::trajectory::QPath& path, const std::string& id) = 0;
	virtual void doRead(er_common::Serializable& s, const std::string& id) = 0;
	
	virtual void doReadEnterScope(const std::string& id) = 0;
    virtual void doReadLeaveScope(const std::string& id) = 0;


   // virtual exm::Argument::Ptr doGetArgument(const std::string& argId) = 0;

};

} //end namespace

#endif //#ifndef ER_SERIALIZATION_INPUTARCHIVE_HPP
