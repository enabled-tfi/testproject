/* */
#ifndef ER_COMMON_REFERENCEBASE_HPP
#define ER_COMMON_REFERENCEBASE_HPP


#include "Serializable.hpp"
#include "InputArchive.hpp"
#include "OutputArchive.hpp"

namespace er_common {
	

class ReferenceBase : public Serializable
{
public:
	virtual void set(const std::string& name) = 0;
	virtual std::string getName() const = 0;
	virtual std::vector<std::string> getNameOptions() const = 0;


	void read(er_serialization::InputArchive& iarchive, const std::string& id);

	void write(er_serialization::OutputArchive& oarchive, const std::string& id) const;

};


} //end namespace

#endif //#ifndef ER_COMMON_REFERENCEBASE_HPP
