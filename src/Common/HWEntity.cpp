#include "HWEntity.hpp"


#include <Common/EntityRepository.hpp>

using namespace er_common;
using namespace hwl;




HWEntity::HWEntity(const std::string& identifier, bool isChild):
	Entity(identifier),
    _isChild(isChild)
{
	ER_LOG_DEBUG("HW entity constructor for "<<identifier);
}


bool HWEntity::isChild() const
{
    return _isChild;
}

/* Methods from the serializable interface */
void HWEntity::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
	Entity::read(iarchive, id);	
}

void HWEntity::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	Entity::write(oarchive, id);
}

