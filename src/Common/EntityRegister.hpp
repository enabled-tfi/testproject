/* */
#ifndef ER_COMMON_ENTITYREGISTER_HPP
#define ER_COMMON_ENTITYREGISTER_HPP

#include <Common/Entity.hpp>

#include <rw/common/Ptr.hpp>
#include <rw/common/PropertyMap.hpp>

#include <map>
#include <string>

namespace er_common {
	
/**
 * The EntityRegister provides a lookup functionality where one can find
 * entities that may be shared among multiple objects.
 *
 * The EntityRegister is created to work through static methods.
 */
class EntityRegister
{
public:
    /** @brief Smart pointer to EntityRegister object*/
	typedef rw::common::Ptr<EntityRegister> Ptr;

	

    /**
     * @brief Registers the entity \b entity with name \b identifier
     * @param identifier [in] Identifer for looking up the entity
     * @param entity [in] The entity to register
     */
	static void add(const std::string& identifier, Entity::Ptr entity)
	{
		_entities[identifier] = entity;
	}


    /**
     * @brief Returns the entity matching \b identifer.
     *
     * The method needs the return type as template argument
     * If no entity with the given identifier and type can be found, it will throw an exception.
     *
     * @param identifier [in] string identifier for the entity.
     * @return Reference to the entity
     */
	template <class T>
	static rw::common::Ptr<T> get(const std::string& identifier)
	{
		EntityMap::iterator it = _entities.find(identifier);
		if (it != _entities.end()) {
			rw::common::Ptr<T> res = (*it).second.cast<T>();
			if (res == NULL) {
				RW_THROW(std::string("Type of entity named '")<<identifier<<"'does not match");
			}
			return res;
		}
		else {
			RW_THROW(std::string("No entity named '")<<identifier<<"' found!");
		}
	}

    /**
    * @brief Returns the first found entity matching the specified type.
    *
    * If no entity with the given type can be found, it will throw an exception.
    *
    * @return Reference to the entity
    */
    template <class T>
    static rw::common::Ptr<T> get()
    {
        for (EntityMap::iterator it = _entities.begin(); it != _entities.end(); ++it) {
            rw::common::Ptr<T> res = (*it).second.cast<T>();
            if (res != NULL) {
                return res;
            }
        }
        RW_THROW("No entity with specified type found.");
    }

        

	template <class T>
	static std::vector<std::string> findMatches()
	{
		std::vector<std::string> result;
		
		for (EntityMap::iterator it = _entities.begin(); it != _entities.end(); ++it) {
			if ((*it).second.cast<T>() != NULL) {
				result.push_back((*it).first);
			}
		}
		return result;
	}

	template <class T>
	static std::vector<rw::common::Ptr<T> > getAll() {
		std::vector<rw::common::Ptr<T>> result;
		for (EntityMap::iterator it = _entities.begin(); it != _entities.end(); ++it) {
			rw::common::Ptr<T> entity = (*it).second.cast<T>();
			if (entity != NULL) {
				result.push_back(entity);
			}
		}
		return result;
	}

	/**
	* @brief Returns true if the EntityRegister has element matching \b identifer 
	*
	* @param identifier [in] string identifier for the entity.
	* @return True if the entity exists. Otherwise false.
	*/	
	static bool has(const std::string& identifier)
	{
		return _entities.find(identifier) != _entities.end();
	}


	/**
	 * @brief Removes entity from register
	 * @param identifier [in] id for the entity to remove
	 * @return True if entity was removed
	 */
	static bool remove(const std::string& identifier)
	{
		EntityMap::iterator it = _entities.find(identifier);
		if (it != _entities.end()) {
			_entities.erase(it);
			return true;
		}
		return false;
	}

	static void clear() {
		_entities.clear();
	}



private:
	//static rw::common::PropertyMap _entities;   

	typedef std::map<std::string, Entity::Ptr> EntityMap;
	static EntityMap _entities;
    static std::set<Entity::Ptr> _defaultEntities;

    /**
	 *  @brief Construct EntityRegister for sharing entities;
	 */
	EntityRegister()
	{
	}
};

} //end namespace

#endif //#ifndef ER_COMMON_ENTITYREGISTER_HPP  
