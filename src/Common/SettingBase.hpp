#ifndef ER_COMMON_SETTINGSBASE_HPP
#define ER_COMMON_SETTINGSBASE_HPP


#include "DataType.hpp"
#include "InputType.hpp"
#include "UserType.hpp"
#include <Common/InputArchive.hpp>
#include <Common/OutputArchive.hpp>
#include <rw/common/Ptr.hpp>
#include <rw/common/Event.hpp>

#include <boost/function.hpp>

#include <string>

namespace er_common {

    /**
     * @brief Base class for handling basic setting information
     */
	class SettingBase: public Serializable
    {
    public:
        //! @brief smart pointer type to this class
        typedef rw::common::Ptr<SettingBase> Ptr;

        /**
         * @brief Constructor
         *
         * @param identifier [in] identifier for the property
         * @param description [in] description of the property
         */
		SettingBase(const std::string& identifier, const std::string& description);

        /**
         * @brief Constructor
         *
         * @param identifier [in] identifier for the property
         * @param description [in] description of the property
         * @param type [in] type of the property
         */
		SettingBase(const std::string& identifier,
                     const std::string& description,
                     const DataType& dataType,					 
					 const UserType& = UserType::Default,
					 const InputType& = InputType::Numeric);

        /**
         * @brief Destroys PropertyBase
         */
        virtual ~SettingBase();

        /**
         * @brief Returns the Property identifier
         * @return string identifier
         */
        const std::string& getIdentifier() const;

        /**
         * @brief Returns description
         * @return string description
         */
        const std::string& getDescription() const;

        /**
         * @brief set description
         */
        void setDescription(const std::string& desc){ _description = desc;}
	
		const UserType getUserType() const { return _userType;  }
		void setUserType(UserType userType) { _userType = userType;  }

		const InputType getInputType() const { return _inputType;  }
		void setInputType(InputType inputType) { _inputType = inputType; }

        /**
           @brief Construct a clone of the property.
        */
        virtual SettingBase::Ptr clone() const = 0;

        /**
         * @brief Method signature for a callback function
         */
        typedef boost::function<void(SettingBase::Ptr)> PropertyListener;

        //! @brief Type for changed property events.
        typedef rw::common::Event<PropertyListener, SettingBase::Ptr> ChangedEvent;

        /**
         * @brief get changed event
         *
         * to add listener use:
         * changedEvent().add(...)
         *
         */
        ChangedEvent& changedEvent() { 
			return _changedEvent; 
		}

        /**
         * @brief Returns the PropertyType
         * @return the PropertyType
         */
        const DataType& getDataType() const;

	//	virtual void read(er_serialization::InputArchive& iarchive, const std::string&) = 0;
	//	virtual void write(er_serialization::OutputArchive& oarchive) = 0;

    private:
        /**
         * @brief Identifiers
         */
        std::string _identifier;

        /**
         * @brief Description
         */
        std::string _description;

        /**
         * @brief Type of property
         */
        DataType _dataType;

		UserType _userType;

		InputType _inputType;



        //! changed event handler
        ChangedEvent _changedEvent;
    private:
        SettingBase(const SettingBase&);
		SettingBase& operator=(const SettingBase&);
    };

} // end namespaces

#endif // end include guard
