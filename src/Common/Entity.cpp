#include "Entity.hpp"
#include <Common/InputArchive.hpp>
#include <Common/OutputArchive.hpp>
using namespace er_common;


void Entity::setVersion(int major, int minor, int revision)
{
    _version[0] = major;
    _version[1] = minor;
    _version[2] = revision;
}

const std::vector<int>& Entity::getVersion() const
{
    return _version;
}


/* Methods from the serializable interface */
void Entity::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
	_name = iarchive.read<std::string>("Name");
    if (iarchive.has("Description")) {
        _description = iarchive.read<std::string>("Description");
    }
    else {
        _description = "";
    }
    std::string version = iarchive.read<std::string>("Version");
    size_t first = version.find_first_of(".");
    size_t last = version.find_last_of(".");
    std::istringstream iss(version);
    char buf;
    iss >> _version[0];
    iss >> buf;
    iss >> _version[1];
    iss >> buf;
    iss >> _version[2];
}

void Entity::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	oarchive.write(_name, "Name");
	if (_description.size() > 0)
	    oarchive.write(_description, "Description");
    std::stringstream sstr;
    sstr << _version[0] << "." << _version[1] << "." << _version[2];
    oarchive.write(sstr.str(), "Version");
}

