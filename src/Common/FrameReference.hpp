/* */
#ifndef ER_COMMON_FRAMEREFERENCE_HPP
#define ER_COMMON_FRAMEREFERENCE_HPP

#include "ReferenceBase.hpp"

#include <rw/math/Transform3D.hpp>
#include <rw/common/PropertyMap.hpp>
#include <rw/models.hpp>
#include <rw/kinematics.hpp>
#include <rw/pathplanning/StateConstraint.hpp>

#include <boost/variant.hpp>

namespace er_common {
	



/**
 * 
 */
class FrameReference: public ReferenceBase
{
public:
	typedef rw::common::Ptr<FrameReference> Ptr;

	FrameReference();

	FrameReference(const std::string& referenceFrameName);
	
	FrameReference(rw::kinematics::Frame* referenceFrame);

	rw::kinematics::Frame* get() const;

	std::string getName() const;

	void set(const std::string& referenceFrameName);
	void set(rw::kinematics::Frame* referenceFrame);

	std::vector<std::string> getNameOptions() const;


	//void read(er_serialization::InputArchive& iarchive, const std::string& id);
	//void write(er_serialization::OutputArchive& oarchive, const std::string& id) const;

private:
	boost::variant<std::string, rw::kinematics::Frame*> _reference;

};

} //end namespace

#endif //#ifndef ER_COMMON_FRAMEREFERENCE_HPP
