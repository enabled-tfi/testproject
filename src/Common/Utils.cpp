#include "Utils.hpp"

#include <rw/math/RPY.hpp>
#include <boost/date_time.hpp>
#include <sstream>
#include <rwlibs/opengl/RenderImage.hpp>


using namespace er_common;

using namespace rw::math;
using namespace rw::common;
using namespace rw::kinematics;
using namespace rwlibs::opengl;


rw::math::Transform3D<> Utils::pose2DtoTransform3D(const rw::math::Pose2D<>& pose, const rw::math::Transform3D<>& prototype)
{
	Transform3D<> result(prototype);

	result.P()(0) = pose(0);
	result.P()(1) = pose(1);
	result.R() = RPY<>(pose(2), 0, 0).toRotation3D();

	return result;
}


rw::math::Transform2D<> Utils::transform3DtoTransform2D(const rw::math::Transform3D<>& t3d)
{
	RPY<> rpy(t3d.R());
	Transform2D<> t2d(Vector2D<>(t3d.P()(0), t3d.P()(1)), Rotation2D<>(rpy(0)));
	return t2d;
}


class QDistanceComparator
{
private:
	rw::math::Q _qref;
	rw::math::QMetric::Ptr _qmetric;
public:
	QDistanceComparator(const rw::math::Q& qref, rw::math::QMetric::Ptr qmetric) :
		_qref(qref),
		_qmetric(qmetric)
	{}

	bool operator() (const rw::math::Q& q1, const rw::math::Q& q2) {
		double d1 = _qmetric->distance(q1, _qref);
		double d2 = _qmetric->distance(q2, _qref);
		return d1 > d2;
	}
};


std::queue<rw::math::Q> Utils::sortQ(std::vector<rw::math::Q>& qs, const rw::math::Q& qnear, rw::math::QMetric::Ptr qmetric)
{
    std::priority_queue<Q, std::vector<Q>, QDistanceComparator> pqueue(QDistanceComparator(qnear, qmetric), qs);
	std::queue<Q> queue;
	while (pqueue.empty() == false) {
		queue.push(pqueue.top());
		pqueue.pop();
	}

	return queue;

}


std::string Utils::getTimestampAsString() {
    boost::posix_time::ptime now(boost::posix_time::second_clock::local_time());
    std::stringstream ss;
    ss << now.date() << "-" << now.time_of_day();
    std::string date = ss.str();

    //Windows does not accept : in filenames, hence replaces : with -
    std::replace(date.begin(), date.end(), ':', '-');
    return date;
}



rwlibs::opengl::Drawable::Ptr Utils::getDrawableForMap(const rw::sensor::Image& image, double xoffset, double yoffset, double resolution)
{
    
    xoffset = image.getWidth() / 2.0 * resolution + xoffset;
    yoffset = image.getHeight() / 2.0 * resolution + yoffset;

    RenderImage::Ptr imageRender = ownedPtr(new rwlibs::opengl::RenderImage(image, 1.0 / 20.0));
    Drawable::Ptr drawable = ownedPtr(new Drawable(imageRender, "Map"));   


    drawable->setTransform(Transform3D<>(Vector3D<>(xoffset, yoffset, 0), RPY<>(0, 0, 0))); 

    return drawable;
    
}

bool Utils::isFrameParentOf(Frame* frame, Frame* child, const rw::kinematics::State& state) {
    if (child->getParent(state) == NULL)
        return false;
    if (child->getParent(state) == frame) {
        return true;
    }
    return isFrameParentOf(frame, child->getParent(state), state);
}

