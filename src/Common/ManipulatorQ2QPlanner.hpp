/* */
#ifndef ER_COMMON_MANIPULATORQ2QPLANNER_HPP
#define ER_COMMON_MANIPULATORQ2QPLANNER_HPP


#include <rw/invkin/InvKinSolver.hpp>
#include <rw/math.hpp>
#include <rw/pathplanning.hpp>
#include <rw/models.hpp>
#include <rw/proximity.hpp>
#include <rw/geometry/QHullND.hpp>

#include <vector>
#include <queue>

namespace er_common {
	
    class ManipulatorQ2QPlanner {
    public:
    	typedef rw::common::Ptr<ManipulatorQ2QPlanner> Ptr;

		ManipulatorQ2QPlanner(rw::models::Device::Ptr manipulator, 
			rw::math::QMetric::Ptr qmetric, 
			rw::pathplanning::QConstraint::Ptr qconstraint);

        rw::trajectory::QPath query(const rw::math::Q& qstart, const rw::math::Q& qgoal, const rw::kinematics::State& state);

		bool inCollision(const rw::math::Q& q, const rw::kinematics::State& state);

	private:
        rw::models::Device::Ptr _manipulator;        
        rw::math::QMetric::Ptr _qmetric;
		rw::pathplanning::QConstraint::Ptr _qconstraint;
	};

} //end namespace

#endif //#ifndef ER_COMMON_MANIPULATORQ2QPLANNER_HPP
