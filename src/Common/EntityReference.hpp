/* */
#ifndef ER_COMMON_ENTITYREFERENCE_HPP
#define ER_COMMON_ENTITYREFERENCE_HPP


#include "Serializable.hpp"
#include "EntityRegister.hpp"
#include "ReferenceBase.hpp"
#include <rw/math/Transform3D.hpp>
#include <rw/common/PropertyMap.hpp>
#include <rw/models.hpp>
#include <rw/kinematics.hpp>
#include <rw/pathplanning/StateConstraint.hpp>

#include <boost/variant.hpp>

namespace er_common {
	

template <class T>
class EntityReference : public ReferenceBase
{
public:
	EntityReference()
	{

	}

	EntityReference(rw::common::Ptr<T> & t)
	{
		set(t);
	}

	EntityReference(const std::string& name)
	{
		set(name);
	}

	rw::common::Ptr<T> get() const {
		try {
			return boost::get<rw::common::Ptr<T> >(_data);
		}
		catch (const boost::bad_get&) {
			return initializeObject();
		}
	};

	

	std::string getName() const  {
		try {
			return boost::get<std::string>(_data);
		}
		catch (const boost::bad_get&) {
			return get()->getName();
		}
	}

	void set(rw::common::Ptr<T> t)
	{
		_data = t;
	}

	void set(const std::string& name)
	{
		_data = name;
	}

	
	std::vector<std::string> getNameOptions() const {
		return EntityRegister::findMatches<T>();
	}


private:
	mutable boost::variant<std::string, rw::common::Ptr<T> > _data;

	rw::common::Ptr<T> initializeObject() const {
		rw::common::Ptr<T> t = EntityRegister::get<T>(getName());
		_data = t;
		return t;
	};

};

} //end namespace

#endif //#ifndef ER_COMMON_ENTITYREFERENCE_HPP
