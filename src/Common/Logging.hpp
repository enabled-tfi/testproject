/*
 * Logging.h
 *
 *  Created on: May 31, 2018
 *      Author: thomas
 */

#ifndef SRC_COMMON_LOGGING_HPP_
#define SRC_COMMON_LOGGING_HPP_

#include <iostream>
#include <rw/rw.hpp>
#include <Common/Utils.hpp>

/**
 * @brief Writes \b ostreamExpression to log with LogIndex \b id.
 *
 * \b id be the log level of type er::common::Logging::LogIndex.
 *
 * \b ostreamExpression is an expression that is fed to an output stream.
 *
 * @param id [in] log level to write to.
 * @param ostreamExpression [in] Stream expression which should be written to the log
 */
#define ER_LOG(id, ostreamExpression) do { rw::common::Log::log().get(id) << "["<<  er_common::Utils::getTimestampAsString() <<  "]" << "[" << er_common::Logging::toStr(id) << "]" << ostreamExpression << std::endl; } while (0)

/**
 * @brief Writes \b ostreamExpression to error log.
 * @param ostreamExpression [in] Stream expression which should be written to the log
 */
#define ER_LOG_ERROR(ostreamExpression) ER_LOG(rw::common::Log::Error, ostreamExpression)

/**
 * @brief Writes \b ostreamExpression to warning log.
 * @param ostreamExpression [in] Stream expression which should be written to the log
 */
#define ER_LOG_WARNING(ostreamExpression) ER_LOG(rw::common::Log::Warning, ostreamExpression)

/**
 * @brief Writes \b ostreamExpression to debug log.
 * @param ostreamExpression [in] Stream expression which should be written to the log
 */
#define ER_LOG_DEBUG(ostreamExpression) ER_LOG(rw::common::Log::Debug, ostreamExpression)

/**
 * @brief Writes \b ostreamExpression to debug log.
 * @param ostreamExpression [in] Stream expression which should be written to the log
 */
#define ER_LOG_LINE ER_LOG(rw::common::Log::Debug, __FILE__ << ":" << __LINE__)

/**
 * @brief Writes \b ostreamExpression to info log.
 * @param ostreamExpression [in] Stream expression which should be written to the log
 */
#define ER_LOG_INFO(ostreamExpression) ER_LOG(rw::common::Log::Info, ostreamExpression)


namespace er_common {

class Logging {
public:

	/**
	 * @brief Indices for different logs. The loglevel will be Info as default. Everything below the
	 * loglevel is enabled.
	 *
	 */
	enum LogIndex {
		Fatal=0, Critical=1,
		Error=2, Warning=3,
		Info=4, Debug=5
	};


    static std::string toStr(rw::common::Log::LogIndex idx){
            std::string toMaskArr[] = {"ER_FATAL", "ER_CRITICAL",
                                      "ER_ERROR", "ER_WARNING",
										"ER_INFO", "ER_DEBUG"};
            return toMaskArr[idx];
        }

	Logging();
	virtual ~Logging();

	void setLevel(LogIndex index){_loglevel = index;}

    static std::string getTimestampAsString();
private:
	LogIndex _loglevel;
};

} /* namespace er_common */

#endif /* SRC_COMMON_LOGGING_HPP_ */
