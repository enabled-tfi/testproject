/* */
#ifndef ER_COMMON_CARTESIANQCONSTRAINT_HPP
#define ER_COMMON_CARTESIANQCONSTRAINT_HPP

#include <rw/math/Transform3D.hpp>
#include <rw/common/PropertyMap.hpp>
#include <rw/models.hpp>
#include <rw/kinematics.hpp>
#include <rw/pathplanning/QConstraint.hpp>

#include "CartesianConstraint.hpp"

namespace er_common {
	
/**
* A constraint in cartesian space implemeting the RobWork QConstraint interface
*/
class CartesianQConstraint: public rw::pathplanning::QConstraint
{
public:
	typedef rw::common::Ptr<CartesianQConstraint> Ptr;

	enum ConstraintType {InsideValid = 1, OutsideValid = 2};

	CartesianQConstraint(rw::kinematics::Frame* reference, 
		rw::kinematics::Frame* tcp, 
		rw::models::Device::Ptr device, 
		const rw::kinematics::State& state,
		const rw::math::Q& activationVector, 
		double distance, 
		ConstraintType constraintType);

	~CartesianQConstraint();


protected:
	/**
	@brief Subclass implementation of the inCollision() method.
	*/
	virtual bool doInCollision(const rw::math::Q& q) const;

	/**
	* @brief Set a log.
	* @param log [in] the log.
	*/
	virtual void doSetLog(rw::common::Log::Ptr log);

private:
	CartesianConstraint::Ptr _stateConstraint;
    rw::models::Device::Ptr _device;
	mutable rw::kinematics::State _state;
	rw::common::Log::Ptr _log;
};

} //end namespace

#endif //#ifndef ER_COMMON_CARTESIANQCONSTRAINT_HPP
