
#ifndef SRC_COMMON_SWENTITY_HPP_
#define SRC_COMMON_SWENTITY_HPP_

#include <Common/Entity.hpp>
#include <Common/System.hpp>

namespace er_common {

class SWEntity : public Entity {
public:

	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<SWEntity> Ptr;

	SWEntity();
	virtual ~SWEntity();

	SWEntity(const std::string& identifier);

    /**
    * @copydoc Serializable::write
    */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    /**
    * @copydoc Serializable::write
    */
	virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

};

} /* namespace er_common */

#endif /* SRC_COMMON_SWENTITY_HPP_ */
