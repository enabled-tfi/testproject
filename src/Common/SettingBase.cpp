
#include "SettingBase.hpp"
#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>
using namespace er_common;

SettingBase::SettingBase(const std::string& identifier,
                           const std::string& description):
    _identifier(identifier),
    _description(description)
{
}



SettingBase::SettingBase(const std::string& identifier,
                         const std::string& description,
                         const DataType& dataType,
						 const UserType& userType,
						 const InputType& inputType
						 ):
    _identifier(identifier),
    _description(description),
    _dataType(dataType),
	_userType(userType),
	_inputType(inputType)
{
}

SettingBase::~SettingBase() {}

const std::string& SettingBase::getIdentifier() const {
    return _identifier;
}

const std::string& SettingBase::getDescription() const {
    return _description;
}

const DataType& SettingBase::getDataType() const {
    return _dataType;
}


/**
* @copydoc Serializable::read
*/
/*void SettingBase::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
	_identifier = iarchive.read<std::string>("Identifier");
	_description = iarchive.read<std::string>("Description");
	_type = DataType(iarchive.read<int>("DataType"));
	_userType = UserType(iarchive.read<int>("UserType"));
	_inputType = InputType(iarchive.read<int>("InputType"));
}
*/
/**
* @copydoc Serializable::write
*/
/*void SettingBase::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	oarchive.write(_identifier, "Identifier");
	oarchive.write(_description, "Description");
	oarchive.write(_type.getId(), "DataType");
	oarchive.write(_userType.getMask(), "UserType");
	oarchive.write(_inputType.getId(), "InputType");
}
*/