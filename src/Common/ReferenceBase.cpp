#include "ReferenceBase.hpp" 

#include <Common/InputArchive.hpp>
#include <Common/OutputArchive.hpp>

#include <Common/System.hpp>

using namespace er_common; 

void ReferenceBase::read(er_serialization::InputArchive& iarchive, const std::string& id)
{
	iarchive.readEnterScope(id);
	std::string reference = iarchive.read<std::string>("Reference");
	set(reference);
	iarchive.readLeaveScope(id);
}

void ReferenceBase::write(er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	oarchive.writeEnterScope(id);
	oarchive.write(getName(), "Reference");
	oarchive.writeLeaveScope(id);
}
