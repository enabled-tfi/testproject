#include "CartesianQConstraint.hpp" 


using namespace er_common; 
using namespace rw::math;
using namespace rw::common;
using namespace rw::models;
using namespace rw::kinematics;


CartesianQConstraint::CartesianQConstraint(Frame* reference, 
	Frame* tcp, 
	Device::Ptr device, 
	const State& state,
	const Q& activationVector, 
	double distance, 
	ConstraintType constraintType):
	_device(device),
	_state(state)
{
	_stateConstraint = ownedPtr(new CartesianConstraint(reference, tcp, activationVector, distance, (CartesianConstraint::ConstraintType)constraintType));
}

CartesianQConstraint::~CartesianQConstraint()
{
}


bool CartesianQConstraint::doInCollision(const Q& q) const
{
//	ER_LOG_DEBUG("Dev = " << _device );
	_device->setQ(q, _state);
//	ER_LOG_DEBUG("q = " << q );
	return _stateConstraint->inCollision(_state);
}


void CartesianQConstraint::doSetLog(rw::common::Log::Ptr log)
{
	_log = log;
}