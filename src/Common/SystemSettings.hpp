///* */
//#ifndef ER_COMMON_SYSTEMSETTINGS_HPP
//#define ER_COMMON_SYSTEMSETTINGS_HPP
//
//#include <rw/math/Vector2D.hpp>
//#include <rw/common/PropertyMap.hpp>
//#include <rw/geometry/GeometryData.hpp>
//
//namespace er_common {
//
///**
// *
// */
//class SystemSettings
//{
//public:
//    typedef rw::common::Ptr<SystemSettings> Ptr;
//
//	SystemSettings();
//
//	double getMobileBaseSafetyMargin() const;
//
//	std::vector<rw::math::Vector2D<> > getMobileBaseFootPrint() const;
//
//	rw::geometry::GeometryData::Ptr getMobileBaseFootPrintGeometry() const;
//
////	std::string getManipulatorName() const;
////
////	std::string getMobileBaseName() const;
////
////	std::string getToolTCPName() const;
////
////	std::string getCalibrationTCPName() const;
////
////	std::string getToolSensorFrameName() const;
//
////	rw::math::Vector2D<> getManipulatorReachRange() const;
//
//
//	//void saveApplicationProperties(const std::string& applicationPropertiesFile) const;
//	//void loadApplicationProperties(const std::string& applicationPropertiesFile);
//
//	//const rw::common::PropertyMap& getApplicationProperties() const;
//	//rw::common::PropertyMap& getApplicationProperties();
//
////	std::string getCameraCalibrationFileName(const std::string& camId);
//
//};
//
//} //end namespace
//
//#endif //#ifndef ER_COMMON_SYSTEMSETTINGS_HPP
