#include "ManipulatorQ2QPlanner.hpp"

#include <boost/foreach.hpp>

#include <rw/common/macros.hpp>
#include <rw/kinematics/Kinematics.hpp>
#include <rw/pathplanning.hpp>
#include <rw/pathplanning/PathAnalyzer.hpp>
#include <rw/proximity/ProximityData.hpp>

#include <rwlibs/pathplanners/rrt/RRTPlanner.hpp>
#include <rwlibs/proximitystrategies/ProximityStrategyFactory.hpp>
#include <rwlibs/pathplanners/rrt/RRTPlanner.hpp>
#include <rwlibs/pathoptimization/pathlength/PathLengthOptimizer.hpp>

#include <float.h>

#include <Common/Logging.hpp>

using namespace rw::common;
using namespace rw::models;
using namespace rw::kinematics;
using namespace rw::geometry;
using namespace rw::math;
using namespace rw::invkin;
using namespace rw::proximity;
using namespace rw::pathplanning;
using namespace rw::trajectory;
using namespace rwlibs::pathplanners;
using namespace rwlibs::pathoptimization;
using namespace rwlibs::proximitystrategies;
using namespace rwlibs::pathplanners;

using namespace er_common;

ManipulatorQ2QPlanner::ManipulatorQ2QPlanner(rw::models::Device::Ptr manipulator, QMetric::Ptr qmetric, QConstraint::Ptr qconstraint):
	_manipulator(manipulator),
	_qmetric(qmetric),
	_qconstraint(qconstraint)
{
    
}

rw::trajectory::QPath ManipulatorQ2QPlanner::query(const rw::math::Q& qstart, const rw::math::Q& qgoal, const rw::kinematics::State& s)
{
    State state = s;

    QEdgeConstraint::Ptr qedgeconstraint = QEdgeConstraint::make(_qconstraint, _qmetric, 0.005);
    PlannerConstraint plannerConstraint = PlannerConstraint::make(_qconstraint, qedgeconstraint);        
    rw::pathplanning::QToQPlanner::Ptr q2qPlanner = RRTPlanner::makeQToQPlanner(plannerConstraint, _manipulator, RRTPlanner::RRTBalancedBidirectional);

    PathLengthOptimizer pathLengthOptimizer(plannerConstraint, _qmetric);

    QPath qpath;
    q2qPlanner->query(qstart, qgoal, qpath, 5 /* TODO: Make as setting */);
    if (qpath.size() == 0) {
        ER_LOG_DEBUG("NO SOLUTION FOR P2PPLANNER");
        return qpath;
    }
    else {
        qpath = pathLengthOptimizer.shortCut(qpath, 0, 2, 0.1);
        qpath = pathLengthOptimizer.pathPruning(qpath);
    }
    return qpath;
}


bool ManipulatorQ2QPlanner::inCollision(const rw::math::Q& q, const State& s)
{
	if (_qconstraint->inCollision(q))
		return true;
	else
		return false;
}
