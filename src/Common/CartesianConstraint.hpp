#ifndef ER_COMMON_CARTESIANCONSTRAINT_HPP
#define ER_COMMON_CARTESIANCONSTRAINT_HPP

#include <rw/math/Transform3D.hpp>
#include <rw/common/PropertyMap.hpp>
#include <rw/models.hpp>
#include <rw/kinematics.hpp>
#include <rw/pathplanning/StateConstraint.hpp>

namespace er_common {
	
/**
 * A constraint in cartesian space implemeting the RobWork StateConstraint interface
 */
class CartesianConstraint: public rw::pathplanning::StateConstraint
{
public:
	typedef rw::common::Ptr<CartesianConstraint> Ptr;

	enum ConstraintType {InsideValid = 1, OutsideValid = 2};

	CartesianConstraint(rw::kinematics::Frame* reference, 
		rw::kinematics::Frame* tcp, 
		const rw::math::Q& activationVector, 
		double distance, 
		ConstraintType constraintType);

	~CartesianConstraint();


protected:
	/**
	@brief Subclass implementation of the inCollision() method.
	*/
	virtual bool doInCollision(const rw::kinematics::State& state) const;

	/**
	* @brief Set a log.
	* @param log [in] the log.
	*/
	virtual void doSetLog(rw::common::Log::Ptr log);

private:
    rw::kinematics::Frame* _referenceFrame;
	rw::kinematics::Frame* _tcpFrame;
	rw::kinematics::FKRange _fkRange;
	rw::math::Q _activationVector;
	double _distance;
		
	ConstraintType _constraintType;
	rw::common::Log::Ptr _log;
};

} //end namespace

#endif //#ifndef ER_COMMON_CARTESIANCONSTRAINT_HPP
