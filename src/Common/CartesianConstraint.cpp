#include "CartesianConstraint.hpp" 


using namespace er_common; 
using namespace rw::math;
using namespace rw::models;
using namespace rw::kinematics;


CartesianConstraint::CartesianConstraint(Frame* reference, 
	Frame* tcp, 
	const Q& activationVector, 
	double distance, 
	ConstraintType constraintType):
	_referenceFrame(reference),
	_tcpFrame(tcp),
	_activationVector(activationVector),
	_distance(distance),
	_constraintType(constraintType)
{
	
}

CartesianConstraint::~CartesianConstraint()
{
}


bool CartesianConstraint::doInCollision(const State& state) const
{
	Transform3D<> refTtcp = Kinematics::frameTframe(_referenceFrame, _tcpFrame, state);
//	ER_LOG_DEBUG("CartesianConstraint " << refTtcp );
	Vector3D<> pos = refTtcp.P();
	RPY<> rpy(refTtcp.R());

	Q pose(6);
	pose(0) = pos(0) * _activationVector(0);
	pose(1) = pos(1) * _activationVector(1);
	pose(2) = pos(2) * _activationVector(2);
	pose(3) = rpy(0) * _activationVector(3);
	pose(4) = rpy(1) * _activationVector(4);
	pose(5) = rpy(2) * _activationVector(5);

	double dist = pose.norm2();
	if (_log != NULL) {
		_log->debug() << "CartesianConstraint:doInCollision" << std::endl << "\tPose=" << pose << " distance=" << dist << std::endl;
	}
	//ER_LOG_DEBUG( "CartesianConstraint:doInCollision" << std::endl << "\tPose=" << pose << " distance=" << dist );
	if (dist < _distance) {
		return _constraintType == OutsideValid;
	}
	else {
		return _constraintType == InsideValid;
	}
}


void CartesianConstraint::doSetLog(rw::common::Log::Ptr log)
{
	_log = log;
}
