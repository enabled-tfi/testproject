/* */
#ifndef ER_COMMON_SERIALIZABLE_HPP
#define ER_COMMON_SERIALIZABLE_HPP


#include <rw/common/Ptr.hpp>

//#include <rw/common/PropertyMap.hpp>

namespace er_serialization {

class InputArchive;
class OutputArchive;

}

namespace er_common {
	
/**
 * Interface for serializable objects
 *
 * Basic methods of the interface from rw::common::Serializable
 */
class Serializable
{
public:
	/** @brief Smart pointer to the Instruction object*/
	typedef rw::common::Ptr<Serializable> Ptr;


    //! destructor
    virtual ~Serializable(){};

    /**
     * Enable read-serialization of inherited class by implementing this method. Data is read
     * from iarchive and filled into this object.
     * @param iarchi�ve [in] the InputArchive from which to read data.
     * @param id [in] The id of the serialized object.
     *
     * @note the id can be empty in which case the overloaded method should provide
     * a default identifier. E.g. the Vector3D class defined "Vector3D" as its default
     * id.
     */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id) = 0;

    /**
     * Enable write-serialization of inherited class by implementing this method. Data is written
     * to oarchive from this object.
     * @param oarchive [out] the OutputArchive in which data should be written.
     * @param id [in] The id of the serialized sobject.
     *
     * @note the id can be empty in which case the overloaded method should provide
     * a default identifier. E.g. the Vector3D class defined "Vector3D" as its default
     * id.
     */
    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const = 0;


};

} //end namespace

#endif //#ifndef COMMON_SERIALIZABLE_HPP
