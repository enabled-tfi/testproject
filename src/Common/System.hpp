/* */
#ifndef ER_COMMON_SYSTEM_HPP
#define ER_COMMON_SYSTEM_HPP

#include <rw/common/Ptr.hpp>
#include <rw/common/PropertyMap.hpp>
#include <rw/trajectory/Path.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/common/PropertyMap.hpp>
#include <rw/models.hpp>
#include <rw/kinematics.hpp>
#include <rw/invkin/InvKinSolver.hpp>
#include <rw/proximity/CollisionDetector.hpp>
#include <rwlibs/calibration/WorkCellCalibration.hpp>

#include <Common/EnabledRoboticsConfig.hpp>

#include "ManipulatorQ2QPlanner.hpp"

#include "Entity.hpp"

#include <boost/thread.hpp>

#include <SecurityLayer/CopyProtector.hpp>
#include <Common/Logging.hpp>

namespace er_common {

/** 
 *
 */
class System: public Serializable
{
public:
    typedef rw::common::Ptr<System> Ptr;

	void initialize();
	void initialize(rw::models::WorkCell::Ptr workcell);

    void setState(const rw::kinematics::State& state);
    rw::kinematics::State getState() const;
	void addStateListener() {} //TODO: Implement

	void setWorkCell(rw::models::WorkCell::Ptr workcell);
	rw::models::WorkCell::Ptr getWorkCell();

    void addWorkCellCalibrationFile(const std::string& id, const std::string& filename);

	std::vector<rw::kinematics::Frame*> getReferenceFrames();

	//void setPointCloud(const std::vector<rw::math::Vector3D<> >& points);
	//const std::vector<rw::math::Vector3D<> >& getPointCloud() const;
	//
	rw::common::PropertyMap::Ptr getKnowledgeBase();

	class UpdateRule 
	{
	public:
		typedef rw::common::Ptr<UpdateRule> Ptr;
		virtual void update(rw::kinematics::State& state) = 0;
		virtual Entity::Ptr getEntity() = 0;
	};

	void addUpdateRule(UpdateRule::Ptr rule);
	void removeUpdateRuleForEntity(Entity::Ptr entity);

	void update();

	void setManipulator(std::string deviceName, std::string workcellFilename);
	rw::models::SerialDevice::Ptr getManipulator() const;
    
    rw::math::Vector2D<> getManipulatorReachRange() const { 
        //Rough way of computing it assuming that it has the longest reach when all joint configurations are zero.
        rw::kinematics::State state = _workcell->getDefaultState();
        rw::math::Q q = rw::math::Q::zero(getManipulator()->getDOF());
        getManipulator()->setQ(q, state);
        return rw::math::Vector2D<>(0.2, getManipulator()->baseTend(state).P().norm2());
    }

	rw::kinematics::MovableFrame::Ptr getMobileBase() const;

	void setToolTCP(rw::kinematics::FixedFrame::Ptr toolTCP);
	rw::kinematics::FixedFrame::Ptr getToolTCP() const;

	void setCalibrationTCP(rw::kinematics::FixedFrame::Ptr calibrationTCP);
	rw::kinematics::FixedFrame::Ptr getCalibrationTCP() const;

	void setReferenceFrame(rw::kinematics::FixedFrame::Ptr referenceFrame);
	rw::kinematics::FixedFrame::Ptr getReferenceFrame() const;

	/*void setSensorFrame(rw::kinematics::FixedFrame::Ptr toolScannerFrame);
	rw::kinematics::FixedFrame::Ptr getSensorFrame();*/

	void setManipulatorIKSolver(rw::invkin::InvKinSolver::Ptr ikSolver);
	rw::invkin::InvKinSolver::Ptr getManipulatorIKSolver() const;

	void setMobileBaseCollisionDetector(rw::proximity::CollisionDetector::Ptr collisionDetector);
	rw::proximity::CollisionDetector::Ptr getMobileBaseCollisionDetector() const;

	void setCollisionDetector(rw::proximity::CollisionDetector::Ptr collisionDetector);
	rw::proximity::CollisionDetector::Ptr getCollisionDetector() const;

	rw::pathplanning::QConstraint::Ptr getManipulatorConstraint(const rw::kinematics::State& state) const;

	ManipulatorQ2QPlanner::Ptr getManipulatorQ2QPlanner(const rw::kinematics::State& state) const;

	void setQHome(const rw::math::Q& qhome);
	rw::math::Q getQHome() const;

	rw::math::QMetric::Ptr getManipulatorJointMotionMetric() const;

	void setMarkerImageFolder(const std::string& foldername);
	std::string getMarkerImageFolder();

    void setDataFolder(const std::string& foldername);
    std::string getDataFolder();

    void setSystemFolder(const std::string& systemFolder);
    std::string getSystemFolder() const;

    void setProgramFolder(const std::string& foldername);
    std::string getProgramFolder() const;

    void setCalibrationDataFilename(const std::string& filename);
    std::string getCalibrationDataFilename() const;

    void setSaveImages(const bool saveImages);
    bool getSaveImages() const;

    bool popWorkcellChanged();

	static System::Ptr getInstance();
	static bool isInitialized();

	void changeLanguage(std::string lan);
	bool popChangedLanguage() const;
	std::string getLanguage() const;

	/**
	 * @copydoc Serializable::read
	 */
	virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

	/**
	* @copydoc Serializable::write
	*/
	virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

private:
	System();
	static System::Ptr _instance;
	static bool _singletonInitialized;

	#ifdef __linux__
		er_security::CopyProtector::Ptr _copyProtection;
	#endif

	mutable boost::mutex _mutexState;
	mutable boost::mutex _mutexUpdateRule;
	mutable boost::mutex _mutexLanguage;

    rw::kinematics::State _state;

    bool _isStateRecording;
    rw::trajectory::TimedStatePath _statePath;
    
    std::string _workcellFileName;
    rw::models::WorkCell::Ptr _workcell;
	std::map<std::string, std::string> _calibrationFiles;
	std::string _calibrationDataFilename;
    //rwlibs::calibration::WorkCellCalibration::Ptr _workcellCalibration;
	//MobileManipulator::Ptr _mobileManipulator;

	std::vector<UpdateRule::Ptr> _updateRules;

	std::vector<rw::math::Vector3D<> > _pointCloud;

	rw::common::PropertyMap::Ptr _knowledgeBase;

	rw::models::SerialDevice::Ptr _manipulator;
	rw::kinematics::MovableFrame::Ptr _mobileBase;
	rw::kinematics::FixedFrame::Ptr _calibrationTCPFrame;
	rw::kinematics::FixedFrame::Ptr _toolTCPFrame;
	rw::kinematics::FixedFrame::Ptr _referenceFrame;
	//rw::kinematics::FixedFrame::Ptr _sensorFrame;
	rw::invkin::InvKinSolver::Ptr _ikSolver;
	rw::proximity::CollisionDetector::Ptr _mobileBaseCollisionDetector;
	rw::proximity::CollisionDetector::Ptr _collisionDetector;

	rw::math::Q _qHome;

    std::string _dataFolder;
    std::string _systemFolder;
    std::string _programFolder;
	std::string _markerImageFolder;
	std::string _licenseFile;
	std::string _markerDatabasePath;
	std::string _manipulatorName;
	std::string _mobileBaseName;
	std::string _toolTcpName;
	std::string _calibrationTCPName;
	std::string _sensorFrameName;

	std::string _language;
	mutable bool _languageChanged;

	bool _saveImages;
	bool _wcChanged;
	boost::mutex _wcChangedMutex;
	boost::mutex _updateMutex;

	mutable rw::math::QMetric::Ptr _manipulatorJointMotionMetric;
};




} //end namespace

#endif //#ifndef ER_COMMON_SYSTEMBASE_HPP
