#include "CollisionDetector2D.hpp"
#include <Common/EntityRegister.hpp>
#include <Common/Utils.hpp>
#include <HardwareLayer/Manipulator.hpp>
#include <HardwareLayer/MobileDevice.hpp>

#include <rw/geometry/Polygon.hpp>
#include <rw/geometry/PolygonUtil.hpp>
#include <rw/trajectory/LinearInterpolator.hpp>


using namespace er_common;
using namespace exm;
using namespace hwl;

using namespace rw::math;
using namespace rw::models;
using namespace rw::kinematics;
using namespace rw::invkin;
using namespace rw::geometry;
using namespace rw::proximity;
using namespace rw::trajectory;
using namespace rw::sensor;

CollisionDetector2D::CollisionDetector2D(const rw::sensor::Image::Ptr image, double xoffset, double yoffset, double resolution, rw::kinematics::Frame* frame, const std::vector<rw::math::Vector2D<> >& outline):
    _image(image),
    _xoffset(xoffset),
    _yoffset(yoffset),
    _resolution(resolution),
    _frame(frame)
{    
    double xmin = DBL_MAX;
    double ymin = DBL_MAX;
    double xmax = -DBL_MAX;
    double ymax = -DBL_MAX;

    //Find the extremums
    rw::geometry::Polygon<Vector2D<> > polygon;
    for (const Vector2D<>& p: outline) {
        polygon.addVertex(p);
        xmax = std::max(xmax, p(0));
        xmin = std::min(xmin, p(0));
        ymax = std::max(ymax, p(1));
        ymin = std::min(ymin, p(1));
    }

    //Add the boundaries to the foot print
    for (size_t i = 0; i < outline.size(); i++) {
        const Vector2D<>& p1 = outline[i];
        const Vector2D<>& p2 = outline[(i + 1) % outline.size()];
        
        double length = (p2 - p1).norm2();
        LinearInterpolator<Vector2D<> > interpolator(p1, p2, length);
        for (double s = 0; s < length; s += resolution) {
            Vector2D<> pint = interpolator.x(s);
            _footprint.push_back(pint);
        }
    }

    for (double x = xmin; x < xmax; x += resolution) {
        for (double y = ymin; y < ymax; y += resolution) {
            Vector2D<> p(x, y);
            if (PolygonUtil::isInsideConvex(p, polygon, -resolution))
                _footprint.push_back(p);
        }
    }
    if (_footprint.size() == 0) {     
        RW_THROW("No points identifier for CollisionDetector2D ");
    }


    ER_LOG_DEBUG( "CollisionDetector2D generated footprint containing " << _footprint.size() << " points.");

    /*for (Vector2D<>& p : _footprint) {
        ER_LOG_DEBUG ("\t" << p );
    }
    */

}


CollisionDetector2D::~CollisionDetector2D()
{

}


CollisionDetector2D::Classification CollisionDetector2D::classify(const rw::kinematics::State& state) const
{
    Transform3D<> worldTframe = Kinematics::worldTframe(_frame, state);
    Rotation3D<> rot3d = worldTframe.R();
    Transform2D<> imageTframe(Vector2D<>(worldTframe.P()(0), worldTframe.P()(1)), Rotation2D<>(rot3d(0, 0), rot3d(0, 1), rot3d(1, 0), rot3d(1, 1)));
    imageTframe.P() -= Vector2D<>(_xoffset, _yoffset);
    for (const Vector2D<>& p : _footprint) {
        Vector2D<> pt = (imageTframe*p)/_resolution;
        int x = (int)std::round(pt(0));
        int y = (int)std::round(pt(1));

        y = _image->getHeight() -1 - y;

        if (x<0 || x>(int)_image->getWidth() - 1 || y<0 || y>(int)_image->getHeight() - 1) {
            return OUTSIDE_MAP;
        }
            
        
        int r = _image->getPixelValuei(x, y, 0);
        int g = _image->getPixelValuei(x, y, 1);
        int b = _image->getPixelValuei(x, y, 2);
        int max = std::max(r, std::max(g, b));        
        int avg = (r + g + b) / 3;
        //std::cout << "r = " << r << " g = " << g << " b = " << b << " max = " << max << " avg = " << avg << std::endl;
        if (avg > 250) { //We are in the write
            continue;
        }

        if (max < 100) { //We are in the black or grey area (typical value (0, 0, 0))
            return WALL;
        }
        if (r > 250 && g < 250 && b < 250) { //We are in the red area ((typical value (255, 200, 200))
            return FORBIDDEN;
        }
        if (g > 250 && r < 250 && b < 250) { //We are in the gree area ((typical value (200, 255, 200))
            return PREFERRED;
        }
        if (b > 250 && r < 250 && g < 250) { //We are in the blue area ((typical value (200, 200, 255))
            return NONPREFERRED;
        }
        if (r > 240 && g > 210 && b > 160) { //We are in the yellow area ((typical value (245, 216, 162))
            return CRITICAL;
        }
        else
            return UNKNOWN;  //Unknown areas are typically (200,200,200)
    }
    return FREE;
}

bool CollisionDetector2D::doInCollision(const rw::kinematics::State& state) const
{
    Classification classification = classify(state);
    if (_log != NULL) {
        _log->info() << "CollisionDetector2D::doInCollision. Map classified as " << classification;
    }
    if (classification == FREE || classification == PREFERRED || classification == NONPREFERRED || classification == CRITICAL)
        return false;
    else
        return true;

}

void CollisionDetector2D::doSetLog(rw::common::Log::Ptr log)
{
    _log = log;
}