#include "UserType.hpp"

using namespace er_common;


bool UserType::belongsTo(UserType type)
{
	return belongsTo(type.getMask());
}

bool UserType::belongsTo(int type)
{
	return (type & _mask) != 0;
}