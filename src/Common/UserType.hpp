
#ifndef ER_COMMON_USERTYPE_HPP
#define ER_COMMON_USERTYPE_HPP

/**
@file UserType.hpp
*/

#include <string>
#include <vector>


namespace er_common {


	/**
	* @brief Represents the user type
	*
	* UserType has a number of predefined types and allows for defining own types
	*/
	class UserType
	{
	public:
		//! @brief Predefined types
		typedef enum {
			Default = 0x0001,            //!< Default User
			Expert = 0x000F,         //!< Export User
			Admin = 0xFFFF			//!< Administrator
		} UserMask;

		/**
		* brief Constructs User of Default Type
		*/
		UserType() : _mask(Default) {}

		/**
		* @brief Construct UserType with the specified type
		*
		* @param id [in] either one of the predefined types or a user defined
		* type, generated by getNewID().
		*/
		UserType(int mask) : _mask(mask) {}

		/**
		* @brief Returns mask of the type
		* @return the mask
		*/
		int getMask() const { return _mask; }


		bool belongsTo(UserType type);
		bool belongsTo(int type);


	private:
		int _mask;
	};

	/** @} */

} // end namespace

#endif // end include guard
