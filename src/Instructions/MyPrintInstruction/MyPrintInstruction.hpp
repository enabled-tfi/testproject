/* */
#ifndef INSTRUCTIONS_MYPRINTINSTRUCTION_HPP
#define INSTRUCTIONS_MYPRINTINSTRUCTION_HPP

#include <SoftwareModel/Instruction.hpp>
#include <SoftwareModel/InstructionFactory.hpp>

#include <rw/common/Ptr.hpp>
#include <rw/common/PropertyMap.hpp>

namespace swm {
	


/**
 * General interface for Instructions
 */
class MyPrintInstruction: public swm::Instruction
{
public:
	/** @brief Smart pointer to the Instruction object*/
	typedef rw::common::Ptr<MyPrintInstruction> Ptr;


	MyPrintInstruction();


	/**
	 * @brief Method for executing the instruction.
	 * @return The next instruction to call
	 */
	virtual bool execute(swm::ExecutionManager::Ptr execManager);


private:

};

class MyPrintInstructionFactory: public swm::InstructionFactory
{
public:
    MyPrintInstructionFactory();

    virtual swm::Instruction::Ptr make();
};

} //end namespace

#endif //#ifndef INSTRUCTIONS_MYPRINTINSTRUCTION_HPP
