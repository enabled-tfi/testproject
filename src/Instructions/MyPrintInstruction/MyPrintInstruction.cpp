#include "MyPrintInstruction.hpp"

#include <iostream>

using namespace swm;
using namespace rw::common;


MyPrintInstruction::MyPrintInstruction():
        Instruction("MyPrintInstruction")
{

}


bool MyPrintInstruction::execute(ExecutionManager::Ptr execManager)
{
    ER_LOG_DEBUG("MY PRINT INSTRUCTION");
    return true;
}


MyPrintInstructionFactory::MyPrintInstructionFactory():
        InstructionFactory("MyPrintInstruction","Test Instruction Printing Out Something")
{
}

Instruction::Ptr MyPrintInstructionFactory::make()
{
    return ownedPtr(new MyPrintInstruction());
}


DLL_FACTORY_METHOD(MyPrintInstructionFactory)
