#include "InputArchiveXML.hpp"

#include "../ExecutionModel/InstructionRepository.hpp"
#include <boost/foreach.hpp>

#include <rw/loaders/dom/DOMBasisTypes.hpp>


using namespace er_serialization;
using namespace er_common;
using namespace exm;
using namespace rw::loaders;
using namespace rw::common;
using namespace rw::math;


InputArchiveXML::InputArchiveXML(const std::string& filename)
{
    DOMParser::Ptr parser = DOMParser::make();
    parser->load(filename);
    parser->setDebug(false);
    _root = parser->getRootElement();
    _element = _root;
}

InputArchiveXML::InputArchiveXML(DOMElem::Ptr root)
{
    _root = root;
    _element = _root;
}

void InputArchiveXML::initialize()
{
    doReadEnterScope("Archive");
    _root = _element;
}

bool InputArchiveXML::has(const std::string& id) 
{
	return _element->hasChild(id);
}

std::vector<er_serialization::InputArchive::Ptr> InputArchiveXML::getSubArchivesForId(const std::string& id)
{
	std::vector<InputArchive::Ptr> result;

	BOOST_FOREACH(DOMElem::Ptr child, _element->getChildren()) {
		if (child->getName() == id) {
			InputArchiveXML::Ptr subarchive = ownedPtr(new InputArchiveXML(child));
			result.push_back(subarchive);
		}
	}
	return result;
}

std::vector<std::pair<std::string, er_serialization::InputArchive::Ptr> > InputArchiveXML::getSubArchives()
{
	std::vector<std::pair<std::string, InputArchive::Ptr> > result;

	BOOST_FOREACH(DOMElem::Ptr child, _element->getChildren()) {
		//ER_LOG_DEBUG( "Child Name = " << child->getName());
		if (child->getName() != "<xmlcomment>") {
			InputArchiveXML::Ptr subarchive = ownedPtr(new InputArchiveXML(child));
			result.push_back(std::make_pair(child->getName(), subarchive));
		}
	}
	return result;
}

std::vector<Instruction::Ptr> InputArchiveXML::doReadInstructions()
{
    std::vector<Instruction::Ptr> result;

    //InstructionRepository repo;
    //BOOST_FOREACH(DOMElem::Ptr child, _element->getChildren()) {
    //    ER_LOG_DEBUG("Child.name "<<child->getName()<<" "<<child);
    //    Instruction::Ptr instruction = repo.make(child->getName());
    //    InputArchiveXML subarchive(child);
    //    instruction->read(subarchive, child->getName());
    //    result.push_back(instruction);
    //}
    return result;
}

exm::Instruction::Ptr InputArchiveXML::doReadInstruction(const std::string& id)
{
    //if (_element->hasChildren() == false) {
    //    return NULL;
    //}
    //InstructionRepository repo;
    //BOOST_FOREACH(DOMElem::Ptr child, _element->getChildren()) {
    //    ER_LOG_DEBUG("Child.name "<<child->getName());
    //    Instruction::Ptr instruction = repo.make(child->getName());
    //    if (instruction != NULL)
    //    {
    //        instruction->read(*this, child->getName());
    //        return instruction;
    //    }
    //}
    return NULL;
}



void InputArchiveXML::doRead(bool& val, const std::string& id)
{
	if (id == "") {
		val = DOMBasisTypes::readBool(_element);
	} else if (_element->hasChild(id)) {
        val = DOMBasisTypes::readBool(_element->getChild(id));
    } else {
        RW_THROW("Unable to find a child of "<<_element->getName()<<" with name "<<id);
    }

}

void InputArchiveXML::doRead(int& val, const std::string& id)
{
	if (id == "") {
		val = DOMBasisTypes::readInt(_element);
	} else if (_element->hasChild(id)) {
        val = DOMBasisTypes::readInt(_element->getChild(id));
    } else {
        RW_THROW("Unable to find a child of "<<_element->getName()<<" with name "<<id);
    }

}

void InputArchiveXML::doRead(float& val, const std::string& id)
{
	if (id == "") {
		val = DOMBasisTypes::readFloat(_element);
	} else if (_element->hasChild(id)) {
        val = DOMBasisTypes::readFloat(_element->getChild(id));
    } else {
        RW_THROW("Unable to find a child of "<<_element->getName()<<" with name "<<id);
    }
}

void InputArchiveXML::doRead(double& val, const std::string& id)
{
	ER_LOG_DEBUG("Read double " + id);
	if (id == "") {
		val = DOMBasisTypes::readDouble(_element);
	} else if (_element->hasChild(id)) { 
        val = DOMBasisTypes::readDouble(_element->getChild(id));
    } else {
        RW_THROW("Unable to find a child of "<<_element->getName()<<" with name "<<id);
    }
}

void InputArchiveXML::doRead(std::string& val, const std::string& id)
{	
	ER_LOG_DEBUG("Read string " + id);
	if (id == "") {
		val = DOMBasisTypes::readString(_element);
	} else if (_element->hasChild(id)) {
        val = DOMBasisTypes::readString(_element->getChild(id));
    } else {
        RW_THROW("Unable to find a child of "<<_element->getName()<<" with name "<<id);
    }
}

void InputArchiveXML::doRead(std::vector<std::string>& val, const std::string& id) {
    ER_LOG_DEBUG("Read list of string " + id);
    if (id == "") {
        val = DOMBasisTypes::readStringList(_element);
    }
    else if (_element->hasChild(id)) {
        val = DOMBasisTypes::readStringList(_element->getChild(id));
    }
    else {
        RW_THROW("Unable to find a child of " << _element->getName() << " with name " << id);
    }
}

void InputArchiveXML::doRead(rw::math::Q& q, const std::string& id)
{
	ER_LOG_DEBUG("Read q " + id);

	if (id == "") {
		q = DOMBasisTypes::readQ(_element);
	} else if (_element->hasChild(id)) {
        q = DOMBasisTypes::readQ(_element->getChild(id));
    } else {
        RW_THROW("Unable to find a child of "<<_element->getName()<<" with name "<<id);
    }
}

void InputArchiveXML::doRead(rw::math::RPY<>& rpy, const std::string& id)
{
	ER_LOG_DEBUG("Read rpy " + id);

	if (id == "") {
		rpy = DOMBasisTypes::readRPY(_element);
	} else if (_element->hasChild(id)) {
		rpy = DOMBasisTypes::readRPY(_element->getChild(id));
	}
	else {
		RW_THROW("Unable to find a child of " << _element->getName() << " with name " << id);
	}
}

void InputArchiveXML::doRead(rw::math::Transform3D<>& t3d, const std::string& id)
{
	ER_LOG_DEBUG("Read transform " + id);

	if (id == "") {
		t3d = DOMBasisTypes::readTransform3D(_element);
	} else if (_element->hasChild(id)) {
        t3d = DOMBasisTypes::readTransform3D(_element->getChild(id));
    } else {
        RW_THROW("Unable to find a child of "<<_element->getName()<<" with name "<<id);
    }
}

void InputArchiveXML::doRead(rw::math::Vector3D<>& v3d, const std::string& id)
{
	ER_LOG_DEBUG("Read vector3d " + id);

	if (id == "") {
		v3d = DOMBasisTypes::readVector3D(_element);
	} else if (_element->hasChild(id)) {
        v3d = DOMBasisTypes::readVector3D(_element->getChild(id));
    } else {
        RW_THROW("Unable to find a child of "<<_element->getName()<<" with name "<<id);
    }
}

void InputArchiveXML::doRead(rw::math::Vector2D<>& v2d, const std::string& id)
{
	ER_LOG_DEBUG("Read vector2d " + id);

	if (id == "") {
		v2d = DOMBasisTypes::readVector2D(_element);
	} else if (_element->hasChild(id)) {
        v2d = DOMBasisTypes::readVector2D(_element->getChild(id));
    } else {
        RW_THROW("Unable to find a child of "<<_element->getName()<<" with name "<<id);
    }
}

void InputArchiveXML::doRead(rw::math::Transform2D<>& t2d, const std::string& id)
{
	ER_LOG_DEBUG("Read transform2d " + id);

	if (id == "") {
		t2d = DOMBasisTypes::readTransform2D(_element);
	} else if (_element->hasChild(id)) {
        t2d = DOMBasisTypes::readTransform2D(_element->getChild(id));
    } else {
        RW_THROW("Unable to find a child of "<<_element->getName()<<" with name "<<id);
    }
}

void InputArchiveXML::doRead(rw::trajectory::QPath& path, const std::string& id)
{
	ER_LOG_DEBUG("Read qpath " + id);

	std::pair<DOMElem::Iterator, DOMElem::Iterator> iterators;
	if (id == "") {
		iterators = _element->getChildren();
	} else if (_element->hasChild(id)) {
		iterators = _element->getChild(id)->getChildren();
    } else {
        RW_THROW("Unable to find a child of "<<_element->getName()<<" with name "<<id);
    }

	BOOST_FOREACH(DOMElem::Ptr child, iterators) {
	    //ER_LOG_DEBUG("Child = " << child->getValue());
		Q q = DOMBasisTypes::readQ(child);
		path.push_back(q);
	}
	
}

void InputArchiveXML::doRead(er_common::Serializable& s, const std::string& id)
{
	s.read(*this, id);
}

void InputArchiveXML::doReadEnterScope(const std::string& id)
{
	ER_LOG_DEBUG("Enter scope " + id);

    if (_element->getName() == id) {
    	ER_LOG_DEBUG("Element is already in scope ");
		_scopeStack.push(_element);
        return;
    }

    if (_element->hasChild(id)) {
    	ER_LOG_DEBUG("Scope if child of element");
        _scopeStack.push(_element);
        _element = _element->getChild(id);
    } else {
        RW_THROW("Unable to find a child of "<<_element->getName()<<" with name "<<id);
    }
}

void InputArchiveXML::doReadLeaveScope(const std::string& id)
{

    if (_scopeStack.empty() == false) {
        _element = _scopeStack.top();
        _scopeStack.pop();
    } else {
        RW_THROW("Unable to leave scope. No outer-more scope exist.");
    }
}


/*Argument::Ptr InputArchiveXML::doGetArgument(const std::string& argId)
{
    std::map<std::string, exm::Argument::Ptr>::iterator it = _arguments.find(argId);
    if (it != _arguments.end())
        return (*it).second;

    if (_root->hasChild("Arguments")) {
        DOMElem::Ptr element = _root->getChild("Arguments");
        if (element->hasChild(argId)) {
            DOMElem::Ptr tmp = _element;
            _element = element;
            doReadEnterScope(argId);
            Argument::Ptr result = ownedPtr(new Argument());
            doRead(result, argId);
            doReadLeaveScope(argId);

            _element = tmp;
            return result;
        } else {
            return NULL;
        }
    } else {
        RW_THROW("No Arguments specified in file");
    }
}
*/
/*
void InputArchiveXML::doRead(exm::Argument::Ptr argument, const std::string& id)
{
    //TODO: Replace with the real property loader
    PropertyMap::Ptr propertymap = ownedPtr(new PropertyMap());
    doReadEnterScope("PropertyMap");
    BOOST_FOREACH(DOMElem::Ptr child, _element->getChildren()) {
        ER_LOG_DEBUG("Property Map Type = "<<child->getName());
        if (child->getName() == "Boolean")
        {
            bool b = true;
            doRead(b, "Boolean");
            propertymap->add("Argument", "", b);
        } else if (child->getName() == "String") {
            std::string str;
            doRead(str, "String");
            propertymap->add("Argument", "", str);
        }
    }
    argument->setPropertyMap(propertymap);
    doReadLeaveScope("PropertyMap");
}
*/

