#include "OutputArchiveXML.hpp"

#include <rw/loaders/dom/DOMBasisTypes.hpp>
#include <boost/foreach.hpp>

//using namespace exm;
using namespace er_serialization;
using namespace rw::loaders;
using namespace rw::common;
using namespace rw::math;



OutputArchiveXML::OutputArchiveXML(const std::string& filename)
{
    _parser = DOMParser::make();
    _parent = _parser->getRootElement();
    _filename = filename;

    doWriteEnterScope("Archive");
    
    
    std::setlocale(LC_NUMERIC, "en_US.UTF-8");

}

void OutputArchiveXML::finalize()
{
    doWriteEnterScope("Arguments");
/*    for (std::map<Argument::Ptr, std::string>::iterator it = _arguments.begin(); it != _arguments.end(); ++it) {
        doWriteEnterScope((*it).second);
        doWrite((*it).first, (*it).second);
        doWriteLeaveScope((*it).second);
    }
	*/
    doWriteLeaveScope("Arguments");

    doWriteLeaveScope("Archive");
    _parser->save(_filename);
}

/*void OutputArchiveXML::doWrite(exm::Argument::Ptr argument, const std::string& id)
{
    //TODO: Replace with the real property loader
    PropertyMap::Ptr propertymap = argument->getPropertyMap();
    //Store the propertymap when the saver is ready
    doWriteEnterScope("PropertyMap");

    PropertyMap::Range range = propertymap->getProperties();
    BOOST_FOREACH(PropertyBase::Ptr property, range) {
        switch (property->getType().getId()) {
        case PropertyType::Bool:
            doWrite(propertymap->get<bool>(property->getIdentifier()), "Boolean");
            break;
        case PropertyType::String:
            doWrite(propertymap->get<std::string>(property->getIdentifier()), "String");
            break;
        default:
            break;
        }

    }

    doWriteLeaveScope("PropertyMap");

}
*/
void OutputArchiveXML::doWrite(bool val, const std::string& id)
{
    DOMElem::Ptr element = DOMBasisTypes::createBoolean(val, _parent);
    element->setName(id);
}


void OutputArchiveXML::doWrite(int val, const std::string& id)
{
    DOMElem::Ptr element = DOMBasisTypes::createInteger(val, _parent);
    element->setName(id);
}


void OutputArchiveXML::doWrite(float val, const std::string& id)
{
    DOMElem::Ptr element = DOMBasisTypes::createFloat(val, _parent);
    element->setName(id);
}

void OutputArchiveXML::doWrite(double val, const std::string& id)
{
    DOMElem::Ptr element = DOMBasisTypes::createDouble(val, _parent);
    element->setName(id);
}


void OutputArchiveXML::doWrite(const std::string& val, const std::string& id)
{
    DOMElem::Ptr element = DOMBasisTypes::createString(val, _parent);
    element->setName(id);
}

void OutputArchiveXML::doWrite(const std::vector<std::string>& val, const std::string& id) {
    DOMElem::Ptr element = DOMBasisTypes::createStringList(val, _parent);
    element->setName(id);
}

void OutputArchiveXML::doWrite(const rw::math::Q& q, const std::string& id)
{
    DOMElem::Ptr element = DOMBasisTypes::createQ(q, _parent);
    element->setName(id);
}

void OutputArchiveXML::doWrite(const rw::math::RPY<>& rpy, const std::string& id)
{
	DOMElem::Ptr element = DOMBasisTypes::createRPY(rpy, _parent);
	element->setName(id);
}

void OutputArchiveXML::doWrite(const rw::math::Transform3D<>& t3d, const std::string& id)
{
    DOMElem::Ptr element = DOMBasisTypes::createTransform3D(t3d, _parent);
    element->setName(id);
}

void OutputArchiveXML::doWrite(const rw::math::Vector3D<>& v3d, const std::string& id)
{
    DOMElem::Ptr element = DOMBasisTypes::createVector3D(v3d, _parent);
    element->setName(id);
}

void OutputArchiveXML::doWrite(const rw::math::Vector2D<>& v2d, const std::string& id)
{
    DOMElem::Ptr element = DOMBasisTypes::createVector2D(v2d, _parent);
    element->setName(id);
}

void OutputArchiveXML::doWrite(const rw::math::Transform2D<>& t2d, const std::string& id)
{
    DOMElem::Ptr element = DOMBasisTypes::createTransform2D(t2d, _parent);
    element->setName(id);
}

void OutputArchiveXML::doWrite(const rw::trajectory::QPath& path, const std::string& id)
{
    //DOMElem::Ptr element = DOMBasisTypes::createElement("QPath", _parent);
    doWriteEnterScope(id);
    BOOST_FOREACH(const Q& q, path) {
        DOMBasisTypes::createQ(q, _parent);
    }
    doWriteLeaveScope(id);
}

void OutputArchiveXML::doWrite(const er_common::Serializable& s, const std::string& id)
{
	s.write(*this, id);
}

void OutputArchiveXML::doWriteEnterScope(const std::string& id)
{
    _scopeStack.push(_parent);
    _parent = DOMBasisTypes::createElement(id, "", _parent);
}

void OutputArchiveXML::doWriteLeaveScope(const std::string& id)
{
    if (_scopeStack.empty() == false) {
        _parent = _scopeStack.top();
        _scopeStack.pop();
    } else {
        RW_THROW("Unable to leave scope. No outer-more scope exist.");
    }
}

/*
std::string OutputArchiveXML::doAddArgument(Argument::Ptr argument)
{
    std::map<Argument::Ptr, std::string>::iterator it = _arguments.find(argument);
    if (it == _arguments.end()) {
        std::stringstream sstr;
        sstr<<"Argument_"<<_arguments.size();
        _arguments[argument] = sstr.str();;
    }
    return _arguments[argument];
}
*/
