/* */
#ifndef ER_SERIALIZATION_INPUTARCHIVEXML_HPP
#define ER_SERIALIZATION_INPUTARCHIVEXML_HPP


#include <Common/InputArchive.hpp>
#include <Common/Serializable.hpp>
#include <ExecutionModel/Instruction.hpp>
#include <ProgramModel/Program.hpp>
#include <rw/common/Ptr.hpp>
#include <rw/common/PropertyMap.hpp>
#include <rw/common/DOMParser.hpp>
#include <rw/math.hpp>
#include <rw/trajectory/Path.hpp>
#include <stack>
namespace er_serialization {
	


/**
 * General interface for Instructions
 */
class InputArchiveXML: public InputArchive
{
public:
	/** @brief Smart pointer to the Instruction object*/
	typedef rw::common::Ptr<InputArchiveXML> Ptr;

	InputArchiveXML(const std::string& filename);
	InputArchiveXML(rw::common::DOMElem::Ptr root);

	void initialize();

	virtual std::vector<std::pair<std::string, er_serialization::InputArchive::Ptr> > getSubArchives();
	virtual std::vector<InputArchive::Ptr> getSubArchivesForId(const std::string& id);

	virtual bool has(const std::string& id);

    virtual std::string getFilename(){
    	return _filename;
    }
protected:
    virtual exm::Instruction::Ptr doReadInstruction(const std::string& id);
    virtual std::vector<exm::Instruction::Ptr> doReadInstructions();

    virtual void doRead(bool& val, const std::string& id);
    virtual void doRead(int& val, const std::string& id);
    virtual void doRead(float& val, const std::string& id);
    virtual void doRead(double& val, const std::string& id);
    virtual void doRead(std::string& val, const std::string& id);
    virtual void doRead(std::vector<std::string>& val, const std::string& id);
    virtual void doRead(rw::math::Q& q, const std::string& id);
	virtual void doRead(rw::math::RPY<>& rpy, const std::string& id);
    virtual void doRead(rw::math::Transform3D<>& t3d, const std::string& id);
    virtual void doRead(rw::math::Vector3D<>& v3d, const std::string& id);
    virtual void doRead(rw::math::Vector2D<>& v2d, const std::string& id);
    virtual void doRead(rw::math::Transform2D<>& v2d, const std::string& id);
    virtual void doRead(rw::trajectory::QPath& path, const std::string& id);
	virtual void doRead(er_common::Serializable& s, const std::string& id);
    virtual void doReadEnterScope(const std::string& id);
    virtual void doReadLeaveScope(const std::string& id);


/*    virtual exm::Argument::Ptr doGetArgument(const std::string& argId);
    void doRead(exm::Argument::Ptr, const std::string& id);
	*/
private:
    //rw::common::DOMParser::Ptr _parser;
    rw::common::DOMElem::Ptr _root;
    rw::common::DOMElem::Ptr _element;
    std::string _filename;
    std::stack<rw::common::DOMElem::Ptr> _scopeStack;
    //std::map<std::string, exm::Argument::Ptr> _arguments;
};

} //end namespace

#endif //#ifndef ER_SERIALIZATION_INPUTARCHIVEXML_HPP
