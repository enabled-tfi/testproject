#
# The shared library to build:
#
set(TargetName er_serialization)


#
# Source files
#
set(SrcFiles 
	OutputArchiveXML.cpp	
	InputArchiveXML.cpp
)
 

#
# Header files (optional)
#
set(HeaderFiles 
	OutputArchiveXML.hpp
	InputArchiveXML.hpp	
)

#
# Standard cpp files to compile into static library

add_library(${TargetName} ${SrcFiles} ${HeaderFiles})
target_link_libraries(${TargetName} ${ROBWORK_LIBRARIES})
