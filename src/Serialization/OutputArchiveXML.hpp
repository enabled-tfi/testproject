/* */
#ifndef ER_SERIALIZATION_OUTPUTARCHIVEXML_HPP
#define ER_SERIALIZATION_OUTPUTARCHIVEXML_HPP

#include <Common/OutputArchive.hpp>
#include "../Common/Serializable.hpp"
#include <rw/common/Ptr.hpp>
#include <rw/common/PropertyMap.hpp>
#include <rw/common/DOMParser.hpp>
#include <rw/math.hpp>

#include <stack>

namespace er_serialization {
	


/**
 * General interface for Instructions
 */
class OutputArchiveXML: public OutputArchive
{
public:
	/** @brief Smart pointer to the Instruction object*/
	typedef rw::common::Ptr<OutputArchiveXML> Ptr;

	OutputArchiveXML(const std::string& filename);

	void finalize();
protected:

    virtual void doWrite(bool val, const std::string& id);
    virtual void doWrite(int val, const std::string& id);
    virtual void doWrite(float val, const std::string& id);
    virtual void doWrite(double val, const std::string& id);
    virtual void doWrite(const std::string& val, const std::string& id);
    virtual void doWrite(const std::vector<std::string>& val, const std::string& id);
    virtual void doWrite(const rw::math::Q& q, const std::string& id);
	virtual void doWrite(const rw::math::RPY<>& rpy, const std::string& id);
    virtual void doWrite(const rw::math::Transform3D<>& t3d, const std::string& id);
    virtual void doWrite(const rw::math::Vector3D<>& v3d, const std::string& id);
    virtual void doWrite(const rw::math::Vector2D<>& v2d, const std::string& id);
    virtual void doWrite(const rw::math::Transform2D<>& v2d, const std::string& id);
    virtual void doWrite(const rw::trajectory::QPath& path, const std::string& id);
	virtual void doWrite(const er_common::Serializable& s, const std::string& id);
    virtual void doWriteEnterScope(const std::string& id);
    virtual void doWriteLeaveScope(const std::string& id);


//    virtual std::string doAddArgument(exm::Argument::Ptr argument);
  //  virtual void doWrite(exm::Argument::Ptr argument, const std::string& id);
private:
    rw::common::DOMParser::Ptr _parser;
    rw::common::DOMElem::Ptr _parent;
    std::stack<rw::common::DOMElem::Ptr> _scopeStack;
    std::string _filename;
    //std::map<exm::Argument::Ptr, std::string> _arguments;
};

} //end namespace

#endif //#ifndef ER_SERIALIZATION_OUTPUTARCHIVEXML_HPP
