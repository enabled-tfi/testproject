#include "MobileDeviceSimulated.hpp"

#include <rw/common/TimerUtil.hpp>
#include <rw/math/EAA.hpp>
#include <rw/common/TimerUtil.hpp>
#include <rw/trajectory/LinearInterpolator.hpp>

using namespace hwl;
using namespace er_common;
using namespace rw::math;
using namespace rw::kinematics;
using namespace rw::common;
using namespace rw::trajectory;



MobileDeviceSimulated::MobileDeviceSimulated():
	MobileDevice("MobileDeviceSimulated")
{
	_velLinear = 0.3;
	_velRotational = 0.3;
	_dt = 0.05;
	_isMoving = false;
}

/**
 * @brief Construct MobileDeviceSimulated
 *
 * @param deviceBase [in] MovableFrame used to simulate the device
 * @param system [in] The general simulation system it is part of
 */
MobileDeviceSimulated::MobileDeviceSimulated(rw::kinematics::MovableFrame::Ptr deviceBase, const System::Ptr system, const std::string& id):
        MobileDevice(id),
        _deviceBase(deviceBase),
        _system(system)
{
    _velLinear = 0.3;
    _velRotational = 0.3;
    _dt = 0.05;
    _isMoving = false;
}


void MobileDeviceSimulated::initializeSimulation(rws::RobWorkStudio* rws)
{
    _system = System::getInstance();
    if (_frameName != "")
        _deviceBase = _system->getWorkCell()->findFrame<MovableFrame>(_frameName);
}

/**
 * @copydoc MobileDevice::move
 */
void MobileDeviceSimulated::move(double distance, double angle) {
    _system = System::getInstance();

    ER_LOG_DEBUG("MobileDeviceSimulated::move(" << distance << ", " << angle << ")" );
    _isMoving = true;
    Transform2D<> worldTcurrent = Pose2D<>::transform(getCurrentPose());
    std::cout<<"Current Pose "<<worldTcurrent<<std::endl;
    double time = std::max(fabs(distance)/_velLinear, fabs(angle)/_velRotational);

    int steps = (int)std::ceil(time/_dt);
    ER_LOG_DEBUG("Steps");
    double stepsizeDist = distance/steps;
    double stepsizeAngle = angle/steps;

    for (int i = 1; i<=steps; i++) {
        //ER_LOG_DEBUG("i = "<<i);
        double angle = i*stepsizeAngle;
        double dist = i*stepsizeDist;
        //ER_LOG_DEBUG(i<<": angle = "<<angle<<" dist = "<<dist);

        Transform2D<> currentTnew;
        if (fabs(angle) < 1e-4) {
            currentTnew.P()(0) = dist;
        } else {
            double r = dist/angle;
            currentTnew.P()(0) = r*sin(angle);
            currentTnew.P()(1) = r*(1-cos(angle));
        }
        currentTnew.R() = Rotation2D<>(angle);

        Transform2D<> worldTnew = worldTcurrent * currentTnew;
        doMoveTo(worldTnew);

        TimerUtil::sleepMs((int)(1000*_dt));
        
    }
    _isMoving = false;
}


/**
 * @copydoc MobileDevice::moveTo
 */

void MobileDeviceSimulated::moveTo(const rw::math::Pose2D<>& pose, double threshold)
{
    Transform2D<> worldTnew = Pose2D<>::transform(pose);
    _system = System::getInstance();

	ER_LOG_DEBUG("MobileDeviceSimulated::moveTo");
    _isMoving = true;
    Transform2D<> worldTcurrent = Pose2D<>::transform(getCurrentPose());
    Transform2D<> currentTnew = inverse(worldTcurrent)*worldTnew;
	
	//currentTnew.P()(0) += 0.05;// Math::ran(-0.05, 0.05);
	//currentTnew.P()(1) += 0.05;// Math::ran(-0.05, 0.05);
    
	double angle1 = std::atan2(currentTnew.P()(1), currentTnew.P()(0));
    move(0, angle1);
	ER_LOG_DEBUG("Current Pose = " << getCurrentPose().getPos() << " " << getCurrentPose().theta() );

    double distance = currentTnew.P().norm2();
    move(distance, 0);

    Transform2D<> worldTcurrent2 = Pose2D<>::transform(getCurrentPose());
	ER_LOG_DEBUG("World To Current 2 " << worldTcurrent2 );
    Transform2D<> currentTnew2 = inverse(worldTcurrent2)*worldTnew;
    if (currentTnew2.P().norm2() > 1e-4) {
        ER_LOG_DEBUG("DID NOT REACH THE RIGHT POSITION: Off by: "<<currentTnew2.P());
    }

    double angle2 = std::atan2(currentTnew2.R()(1,0), currentTnew2.R()(0,0));
    move(0, angle2);
    _isMoving = false;
}

void MobileDeviceSimulated::doMoveTo(const rw::math::Pose2D<>& target)
{
    _system = System::getInstance();

    _isMoving = true;
    //ER_LOG_DEBUG("doMoveTo"<<target);
    State state = _system->getState();
    Transform3D<> world2base = Kinematics::worldTframe(_deviceBase.get(), state);
    world2base.P()(0) = target.x();
    world2base.P()(1) = target.y();
    world2base.R() = RPY<>(target.theta(), 0, 0).toRotation3D();

    _deviceBase->setTransform(world2base, state);
    _system->setState(state);
    _isMoving = true;
}

/**
 * @copydoc MobileDevice::getCurrentTransform
 */
rw::math::Pose2D<> MobileDeviceSimulated::getCurrentPose() const {
    if (_deviceBase == NULL || _system == NULL)
        RW_THROW("MobileDeviceSimulated not yet initialized");

    State state = _system->getState();
    Transform3D<> world2base = Kinematics::worldTframe(_deviceBase.get(), state);
    Transform2D<> result;
    result.P()(0) = world2base.P()(0);
    result.P()(1) = world2base.P()(1);
    Rotation3D<> rot;
    result.R()(0,0) = world2base.R()(0,0);
    result.R()(0,1) = world2base.R()(0,1);
    result.R()(1,0) = world2base.R()(1,0);
    result.R()(1,1) = world2base.R()(1,1);

    return result;
}

std::vector<MobileDevice::DefinedPosition> MobileDeviceSimulated::getDefinedPositions() const
{
    std::vector<MobileDevice::DefinedPosition> result;
    result.push_back(MobileDevice::DefinedPosition("Home", "Home", Pose2D<>(0,0,0)));
    return result;
}

bool MobileDeviceSimulated::isMoving() const {
    return _isMoving;
}


/* Methods from the serializable interface */
void MobileDeviceSimulated::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
	iarchive.readEnterScope("MobileDeviceSimulated");
	HWEntity::read(iarchive, id);
	_frameName = iarchive.read<std::string>("Frame");
	iarchive.readLeaveScope("MobileDeviceSimulated");
}

void MobileDeviceSimulated::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	oarchive.writeEnterScope("MobileDeviceSimulated");
	HWEntity::write(oarchive, id);
	oarchive.write(_deviceBase->getName(), "Frame");
	oarchive.writeLeaveScope("MobileDeviceSimulated");
}
