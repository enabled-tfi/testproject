/* */
#ifndef HWL_SMAC_HPP
#define HWL_SMAC_HPP

#include "../ImpulseDevice.hpp"

#include <rwhw/serialport/SerialPort.hpp>

namespace hwl {

/** 
 * Control of SMAC using serial communication
 */
class SMAC: public ImpulseDevice
{
public:
	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<SMAC> Ptr;

	SMAC();

	SMAC(const std::string& port);
     
    ~SMAC();

    /**
     * @brief Connects to SMAC on \bport
     *
     * On connect the device also initializes, hence actuator moves to find it home and actuation positions.
     *
     * @param part [in] Port on which to connect
     */
	void connect();
    
	/**
	 * @brief Disconnect communication
	 */
    void disconnect();

    /**
     * @brief Exercise the impulse
     * @param force [in] No used
     */
	virtual void execute(double force);

private:
	std::string _port;

    void initialize();
	void finalize();
    void sendCmd(const std::string& cmd);
	int readMacroNumber();
    rwhw::SerialPort _serialPort;

    
};

} //end namespace

#endif //#ifndef HWL_SMAC_HPP
