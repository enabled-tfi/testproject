/* */
#ifndef HWL_SMACSIMULATED_HPP
#define HWL_SMACSIMULATED_HPP

#include "../ImpulseDevice.hpp"
#include "../../Common/System.hpp"

#include <rw/common/Ptr.hpp>
#include <rw/math.hpp>
#include <rw/kinematics.hpp>
#include <rw/models.hpp>

namespace hwl {

/** 
 * Simulated SMAC device able to supply an impulse
 */
class SMACSimulated: public ImpulseDevice
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<SMACSimulated> Ptr;

    SMACSimulated(rw::models::Device::Ptr device, er_common::System::Ptr system);
     
    ~SMACSimulated();

    void execute(double force);

    void gotoActivePosition();

    void gotoHome();

	/**
	* @brief Connect to the device
	*/
	virtual void connect() {};

	virtual void disconnect() {};

private:
    rw::models::Device::Ptr _device;
    er_common::System::Ptr _system;

    
};

} //end namespace

#endif //#ifndef HWL_SMACSIMULATED_HPP
