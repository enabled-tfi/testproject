#include "SMAC.hpp"

using namespace rw::common;
using namespace hwl;
using namespace rwhw;
#include <rw/common/macros.hpp>


SMAC::SMAC():
	ImpulseDevice("SMAC")

{

}

SMAC::SMAC(const std::string& port) :
	ImpulseDevice("SMAC"),
	_port(port)
{}


SMAC::~SMAC() {
    disconnect();
}

void SMAC::connect() 
{
    ER_LOG_DEBUG("Connect");
	if (_serialPort.open(_port, SerialPort::Baud115200) == false) {
	    ER_LOG_DEBUG("Unable to Connect");
        RW_THROW("Unable to open serial port on "<<_port);
	}
    ER_LOG_DEBUG("Initialize");
    initialize();
	//Initialize with the right macros

}

void SMAC::disconnect() {
	
	finalize();

	_serialPort.close();
}

namespace {
    void readAndPrint(SerialPort& port) {
        char buffer[256];
        int n = port.read(buffer, 256);
        buffer[n] = 0;
        ER_LOG_DEBUG("Output: "<<buffer);
    }
}

#include <rw/common/TimerUtil.hpp>

void SMAC::initialize() {    
    std::string cmd1 = "0 W 0x12C04 10\r";

    sendCmd(cmd1);    
	rw::common::TimerUtil::sleepMs(100);
}

void SMAC::finalize() {
	std::string cmd1 = "0 W 0x12C04 11\r";
	sendCmd(cmd1);
}


void SMAC::execute(double force) {
	ER_LOG_DEBUG("SMAC Actuate" );
	std::string cmd = "0 W 0x12C04 30\r";

	sendCmd(cmd);

	//Wait a bit 
	TimerUtil::sleepMs(100);
	
	int macro = 0;
	const int MACRO_GOAL = 59;
	int cnt = 0;
	do {
		macro = readMacroNumber();
		if (macro != MACRO_GOAL) {
			TimerUtil::sleepMs(100);
			cnt++;
		}
	} while (macro != MACRO_GOAL && cnt < 100);
	if (cnt == 100) {
		RW_THROW("The execution did not finished within expected the time frame");
	}


	ER_LOG_DEBUG("Actuation Finished" );
	
	
	return;
	//Read whichever might be in the buffer already
	char buffer[512];
	_serialPort.read(buffer, 512);
	


	char character[2];
	do {
		if (_serialPort.read(character, 1) == 0) {
			TimerUtil::sleepMs(100);
		}
		character[1] = 0;
	} while (character[0] != '\r');
	
    //TimerUtil::sleepMs(110);
    //char buffer[256];
    //int n = _serialPort.read(buffer, 256);
    //buffer[n] = 0;
    //ER_LOG_DEBUG("Execute Return Value: "<<buffer);

}

int SMAC::readMacroNumber() 
{
	{
		char buffer[512];
		int n = _serialPort.read(buffer, 512);
		ER_LOG_DEBUG("Read" << n );
		buffer[n] = 0;
		ER_LOG_DEBUG("Empty Buffer Reply = " << buffer );;
	}

	ER_LOG_DEBUG("Request MAcro Number" );
	std::string cmd = "0 R 0x12C0A\r";
	sendCmd(cmd);
	TimerUtil::sleepMs(1000);
	char buffer[512];
	int n = _serialPort.read(buffer, 512);
	ER_LOG_DEBUG("Read" << n );
	buffer[n] = 0;
	ER_LOG_DEBUG("Macro Number Reply = " << buffer );;
	std::string message(buffer);
	ER_LOG_DEBUG("MEsasge = " << message );
	int last = message.find_last_of(' ');
	ER_LOG_DEBUG("Last space " << last );
	std::string substr = message.substr(last);
	ER_LOG_DEBUG("Macro Number = " << substr );
	return atoi(substr.c_str());
	
//	              0x20 W 0x12C0A 30y = 0x12C0A


}

void SMAC::sendCmd(const std::string& cmd) {
	ER_LOG_DEBUG("SMAC Send = " << cmd );
	if (_serialPort.write(cmd.c_str(), cmd.length()) == false) {
		RW_THROW("Unable to end command " << cmd << " on serial port");
	}

	TimerUtil::sleepMs(200);
	char buffer[256];
	int bufidx = 0;
	char minibuf[2];
	do {
		int n = _serialPort.read(minibuf, 1);
		if (n > 0)
			buffer[bufidx++] = minibuf[0];
		else
			TimerUtil::sleepMs(100);
	} while (minibuf[0] != '\r');
	

	ER_LOG_DEBUG("Echo: :"<< buffer );

	//readAndPrint(_serialPort);
	//char buffer[256];
	//int n = _serialPort.read(buffer, 1);
	//buffer[n] = 0;
	//ER_LOG_DEBUG("Output: "<<buffer);

	ER_LOG_DEBUG("Command send" );
}