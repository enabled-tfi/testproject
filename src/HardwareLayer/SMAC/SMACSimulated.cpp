#include "SMACSimulated.hpp"

using namespace rw::common;
using namespace rw::math;
using namespace rw::models;
using namespace rw::kinematics;
using namespace hwl;
using namespace er_common;

#include <rw/common/macros.hpp>


SMACSimulated::SMACSimulated(Device::Ptr device, System::Ptr system):
	ImpulseDevice("SMAC"),
    _device(device),
    _system(system)
{

}

SMACSimulated::~SMACSimulated() {
    
}


void SMACSimulated::execute(double force) {
    ER_LOG_DEBUG("Simulated SMAC Execute");
    std::pair<Q,Q> qbounds = _device->getBounds();
    State state = _system->getState();
    _device->setQ(qbounds.second, state);
    _system->setState(state);
    _device->setQ(qbounds.first, state);
    _system->setState(state);
}


