#include "SMAC.hpp"

using namespace rw::common;
using namespace hwl;
using namespace rwhw;
#include <rw/common/macros.hpp>


SMAC::SMAC() 
{

}

SMAC::~SMAC() {
    disconnect();
}

void SMAC::connect(const std::string& port) 
{
    ER_LOG_DEBUG("Connect");
	if (_serialPort.open(port, SerialPort::Baud9600) == false) {
	    ER_LOG_DEBUG("Umnable to Connect");
        RW_THROW("Unable to open serial port on "<<port);
	}
    ER_LOG_DEBUG("Initialize");
    initialize();
	//Initialize with the right macros

}

void SMAC::disconnect() {
	_serialPort.close();
}

namespace {
    void readAndPrint(SerialPort& port) {
        char buffer[256];
        int n = port.read(buffer, 256);
        buffer[n] = 0;
        ER_LOG_DEBUG("Output: "<<buffer);
    }
}

#include <rw/common/TimerUtil.hpp>

void SMAC::initialize() {    
    std::string cmd1 = "RM,SG100,SI300,SD1600,IL5000\r";
    std::string cmd2 = "MD99,PM,SQ-10000,SC32767,MN,QM0,SQ-10000,GO,WA1000,DH\r";
    std::string cmd3 = "MD100,PM,SQ32676,SC32767,MN,QM0,SQ32767,GO,WA100,TP,MC101\r";
    std::string cmd4 = "MD101,PM,MN,SA50000,SV10000000,SQ20000,MA0,GO,WA500\r";
    
    //std::string cmd6 = "MC98\r";
    //std::string cmd4 = "MD101,MN,VM,SA100000,SV10000,SQ1000,DI0,GO,WA100,TP,MF\r";

    sendCmd(cmd1);
    rw::common::TimerUtil::sleepMs(500);
    sendCmd(cmd2);
    rw::common::TimerUtil::sleepMs(500);
    sendCmd(cmd3);
    rw::common::TimerUtil::sleepMs(500);
    sendCmd(cmd4);
    rw::common::TimerUtil::sleepMs(500);
    
    //rw::common::TimerUtil::sleepMs(500);
    //sendCmd(cmd6);
    //rw::common::TimerUtil::sleepMs(1000);

    char buffer[256];
    int n = _serialPort.read(buffer, 256);
    buffer[n] = 0;
    ER_LOG_DEBUG("Stuff not read: "<<buffer);
    execute(0);
    TimerUtil::sleepMs(200);
    
    n = _serialPort.read(buffer, 256);
    buffer[n] = 0;
    ER_LOG_DEBUG("Execute Return Value: "<<buffer<<" "<<n);
    int val = std::atoi(buffer);
    //_actuationPosition = std::atoi(buffer);
    TimerUtil::sleepMs(1000);
    std::stringstream sstr;
    //std::string cmd6 = "MD102,PM,MN,SA50000,SV10000000,SQ20000,MA15000,GO,WA500,MF\r";
    sstr<<"MD102,PM,MN,SA50000,SV10000000,SQ20000,MA"<<val<<",GO,WA500\r";
    //ER_LOG_DEBUG("Length = "<<sstr.str().length());
    sendCmd(sstr.str());
//    sendCmd(cmd6);
    //std::string cmd5 = "MD102,PM,SQ32676,SC32767,MN,QM0,SQ32767,GO,WA100,TP,MF\r";


}

void SMAC::execute(double force) {
	ER_LOG_DEBUG("SMAC Actuate");
	std::string cmd = "MC100\r";

    sendCmd(cmd);
    //TimerUtil::sleepMs(110);
    //char buffer[256];
    //int n = _serialPort.read(buffer, 256);
    //buffer[n] = 0;
    //ER_LOG_DEBUG("Execute Return Value: "<<buffer);

}


void SMAC::gotoActivePosition() {    
    sendCmd("MC102\r");
}

void SMAC::gotoHome() {
    sendCmd("MC99\r");
}

void SMAC::sendCmd(const std::string& cmd) {
    ER_LOG_DEBUG("Send = "<<cmd);
	if (_serialPort.write(cmd.c_str(), cmd.length()) == false) {
		RW_THROW("Unable to end command "<<cmd<<" on serial port");
	}


    char buffer[256];
    int n = _serialPort.read(buffer, cmd.length(), 100, 10);
    buffer[n] = 0;
    ER_LOG_DEBUG("Output: "<<buffer);

    //readAndPrint(_serialPort);
    //char buffer[256];
    //int n = _serialPort.read(buffer, 1);
    //buffer[n] = 0;
    //ER_LOG_DEBUG("Output: "<<buffer);

    ER_LOG_DEBUG("Command send");
}

