/* */
#ifndef HWL_IOINTERFACE_HPP
#define HWL_IOINTERFACE_HPP

#include <Common/HWEntity.hpp>
#include <rw/math/Vector3D.hpp>
#include <vector>


namespace hwl {

/** 
 * Interface for a 1D distance sensor
 */
class IOInterface : public HWEntity
{
public:
	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<IOInterface> Ptr;

	IOInterface(const std::string& identifier, bool isChild = false);

	IOInterface(const std::string& identifier, std::pair<int, int> digitalInRange, std::pair<int, int> digitalOutRange, std::pair<int, int> analogInRange, std::pair<int, int> analogOutRange, bool isChild = false);


	virtual std::pair<int, int> getDigitalInRange();
	virtual void setDigitalInRange(int startIdx, int endIdx);

	virtual std::pair<int, int> getDigitalOutRange();
	virtual void setDigitalOutRange(int startIdx, int endIdx);

	virtual std::pair<int, int> getAnalogInRange();
	virtual void setAnalogInRange(int startIdx, int endIdx);

	virtual std::pair<int, int> getAnalogOutRange();
	virtual void setAnalogOutRange(int startIdx, int endIdx);

	virtual bool getDigitalIn(int idx);
	virtual void setDigitalOut(int idx, bool val);
	virtual bool getDigitalOut(int idx);
	
	virtual double getAnalogIn(int idx);
	virtual void setAnalogOut(int idx, double value);
	virtual double getAnalogOut(int idx);
protected:
	virtual bool doGetDigitalIn(int idx) = 0;
	virtual void doSetDigitalOut(int idx, bool val) = 0;
	virtual bool doGetDigitalOut(int idx) = 0;

	virtual double doGetAnalogIn(int idx) = 0;
	virtual void doSetAnalogOut(int idx, double value) = 0;
	virtual double doGetAnalogOut(int idx) = 0;

private:
	std::pair<int, int> _digitalInRange;
	std::pair<int, int> _digitalOutRange;
	std::pair<int, int> _analogInRange;
	std::pair<int, int> _analogOutRange;

	void checkRange(int idx, const std::pair<int, int>& range, const std::string& type);
};

} //end namespace

#endif //#ifndef HWL_IOINTERFACE_HPP
