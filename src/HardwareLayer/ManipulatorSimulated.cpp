#include "ManipulatorSimulated.hpp"

#include <rw/common/TimerUtil.hpp>
#include <rw/math/EAA.hpp>
#include <rw/trajectory/LinearInterpolator.hpp>

using namespace hwl;
using namespace er_common;
using namespace rw::math;
using namespace rw::invkin;
using namespace rw::kinematics;
using namespace rw::common;
using namespace rw::trajectory;

ManipulatorSimulated::ManipulatorSimulated():
	Manipulator("ManipulatorSimulated")
{
    _velLinear = 0.1;
    _velRotational = 0.1;
    _dt = 0.05;
}

ManipulatorSimulated::ManipulatorSimulated(rw::models::Device::Ptr device, System::Ptr system, const std::string& id):
    Manipulator(id),
	_device(device),
	_system(system),
	_iksolver(ownedPtr(new JacobianIKSolver(_device, _system->getState()))),
    _timeMetric(MetricFactory::makeWeightedInfinity<Q>(_device->getVelocityLimits()))
{
    _velLinear = 0.1;
    _velRotational = 0.1;
    _dt = 0.05;
}

void ManipulatorSimulated::initializeSimulation(rws::RobWorkStudio* rws)
{
	_system = System::getInstance();
	_device = _system->getManipulator();
	_iksolver = ownedPtr(new JacobianIKSolver(_device, _system->getState()));
	_timeMetric = MetricFactory::makeWeightedInfinity<Q>(_device->getVelocityLimits());
}


void ManipulatorSimulated::ptp(const rw::math::Q& qtarget, double speed)
{
	ER_LOG_DEBUG("ManipulatorSimulated::ptp(" << qtarget << ")" );
	State state = _system->getState();
    Q qcurrent = _device->getQ(state);
    
    double time = _timeMetric->distance(qcurrent, qtarget);
	ER_LOG_DEBUG("Time to move " << time );
    LinearInterpolator<Q> interpolator(qcurrent, qtarget, time);

    for (double t = 0; t<time+_dt; t += _dt) {
        ER_LOG_DEBUG("\tMove To = "<<interpolator.x(t));
    	if (t > time)
    		t = time;
    	_device->setQ(interpolator.x(t), state);
	    _system->setState(state);
	    TimerUtil::sleepMs((int)(1000*_dt));
        //TimerUtil::sleepMs(100);
        //notifyListeners();
    }
	
}

void ManipulatorSimulated::ptp(const rw::math::Transform3D<>& baseTtcp, double speed) {
    ER_LOG_DEBUG("ManipulatorSimulated::lin(" << baseTtcp << ")" );

    State state = _system->getState();

    Transform3D<> baseTend = baseTtcp * inverse(_endTtcp);

    std::vector<Q> qsol = _system->getManipulatorIKSolver()->solve(baseTend, state);

    if (qsol.size() == 0) {
        RW_THROW("No IK Solution found");
    }
    ER_LOG_DEBUG("Qsol = " << qsol.size() );
    //Select the most promising of the IK solutions
    Q qcurrent = _system->getManipulator()->getQ(state);
    std::queue<Q> queue = Utils::sortQ(qsol, qcurrent, MetricFactory::makeEuclidean<Q>());
    Q qtarget = queue.front();
    ptp(qtarget, speed);
}


void ManipulatorSimulated::lin(const rw::math::Transform3D<>& baseTtarget, double speed)
{
	ER_LOG_DEBUG("ManipulatorSimulated::lin(" << baseTtarget << ")" );
	State state = _system->getState();
    Transform3D<> baseTtcp = _device->baseTend(state) * _endTtcp;

    double timePos = (baseTtcp.P() - baseTtarget.P()).norm2() / _velLinear;
    double timeRot = EAA<>(baseTtcp.R()*inverse(baseTtarget.R())).angle() / _velRotational;
    double time = std::max(timePos, timeRot);
    
    LinearInterpolator<Transform3D<> > interpolator(baseTtcp, baseTtarget, time);
	Transform3D<> tcpTend = inverse(_endTtcp);
    for (double t = 0; t<time; t += _dt) {
        Transform3D<> t3d = interpolator.x(t);
	    std::vector<Q> res = _iksolver->solve(t3d*tcpTend, state);
	    if (res.size() == 0) {
		    RW_THROW("Unable to find IK solution for target");
	    }
	    _device->setQ(res.front(), state);
	    _system->setState(state);
	   TimerUtil::sleepMs((int)(1000*_dt));
    }
}

Q ManipulatorSimulated::getQ() const {
    if (_device != NULL && _system != NULL)
        return _device->getQ(_system->getState());
    else
        RW_THROW("ManipulatorSimulated not yet initialized");
}

std::pair<rw::math::Q, rw::math::Q> ManipulatorSimulated::getBounds() const
{
    return _device->getBounds();
}


void ManipulatorSimulated::setEndToTCP(const rw::math::Transform3D<>& endTtcp)
{
	_endTtcp = endTtcp;
}

rw::math::Transform3D<> ManipulatorSimulated::getEndToTCP() const
{
	return _endTtcp;
}

rw::math::Transform3D<> ManipulatorSimulated::getBaseToTCP() const
{
	State state = _system->getState();
	Transform3D<> baseTtcp = _device->baseTend(state) * _endTtcp;
	return baseTtcp;
}

void ManipulatorSimulated::stop(){
}

void ManipulatorSimulated::startTeachMode()
{
	ER_LOG_INFO("ManipulatorSimulated::startTeachMode" );
}

void ManipulatorSimulated::endTeachMode()
{
	ER_LOG_INFO("ManipulatorSimulated::endTeachMode" );
}
 
/* Methods from the serializable interface */
void ManipulatorSimulated::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
	iarchive.readEnterScope("ManipulatorSimulated");
	HWEntity::read(iarchive, id);
	iarchive.readLeaveScope("ManipulatorSimulated");
}

void ManipulatorSimulated::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	oarchive.writeEnterScope("ManipulatorSimulated");
	HWEntity::write(oarchive, id);
	oarchive.writeLeaveScope("ManipulatorSimulated");
}
