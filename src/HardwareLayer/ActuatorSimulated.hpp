/* */
#ifndef HWL_ACTUATORSIMULATED_HPP
#define HWL_ACTUATORSIMULATED_HPP

#include "Actuator.hpp"
#include "SimulatedEntity.hpp"
#include <rw/models/Device.hpp>
#include <rw/common/Ptr.hpp>

namespace hwl {

/** 
 * Simulated actuator using a RobWork Device as the model.
 */
class ActuatorSimulated: public Actuator, public SimulatedEntity
{
public:
	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<ActuatorSimulated> Ptr;

    /**
     * @brief Construct empty ActuatorSimulated. Meant to be used when reading in fra Archives.
     */
    ActuatorSimulated();

    /** 
     * @brief Constructs ActuatorSimulated using \b device as the underlying model.
     * @param device [in] Device to use as model 
     * @param id [in] Identifier for the entity
     */
    ActuatorSimulated(rw::models::Device::Ptr device, const std::string& id);

    /**
     * @copydoc SimulatedEntity::intializeSimulation
     */
    void initializeSimulation(rws::RobWorkStudio* rws);

    /**
    * @copydoc HWEntity::connect
    */
    virtual void connect() {}

    /**
    * @copydoc HWEntity::disconnect
    */
    virtual void disconnect() {}

    /**
     * @brief home actuator
     */
    virtual void home();

    /**
     * @copydoc Actuator::setPosition
     */
	virtual void setPosition(double pos);

    /**
    * @copydoc Actuator::getPosition
    */
    virtual double getPosition() const;

    /**
     * @copydoc Actuator::getBounds
     */
    std::pair<double, double> getBounds() const;


    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

};

} //end namespace

#endif //#ifndef HWL_ACTUATORSIMULATED_HPP
