#ifndef SRC_HARDWARELAYER_REALSENSED400_REALSENSED400_HPP
#define SRC_HARDWARELAYER_REALSENSED400_REALSENSED400_HPP

#include <Common/HWEntity.hpp>
#include <HardwareLayer/Camera.hpp>
#include <HardwareLayer/CameraDepth.hpp>

#include <pcl/visualization/cloud_viewer.h>

namespace hwl {


class RealSenseD400Core;
class RealSenseD400Camera;
class RealSenseD400Depth;

/**
* Interface for Intel RealSenseD400 series
*/
class RealSenseD400 : public HWEntity {
public:

	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<RealSenseD400> Ptr;

    /**
     * @brief Default constructor
     */
    RealSenseD400();

    /**
     * @brief Constructor
     * @param identifier [in] Identifier for the entity
     */
    RealSenseD400(const std::string& identifier);

	/**
	 * @brief Destructor
	 */
	~RealSenseD400();

    /**
     * @copydoc HWEntity::connect
     */
	virtual void connect();

    /**
     * @copydoc HWEntity::disconnect
     */
    virtual void disconnect();

    /**
     * @brief Acquire input with sensor
     */
    void acquire();

    /**
     * @copydoc HWEntity::addUpdateRule
     */
    void addUpdateRule(er_common::System::Ptr system) {
        system->addUpdateRule(rw::common::ownedPtr(new RealSenseD400UpdateRule(this)));
    }

    void getColoredPointCloud(pcl::PointCloud<pcl::PointXYZRGB>& cloud);

    /**
     * @brief Standard update rule for Real sense showing image and point cloud
     */
    class RealSenseD400UpdateRule : public er_common::System::UpdateRule
    {
    public:
        RealSenseD400UpdateRule(RealSenseD400::Ptr rs) :
            er_common::System::UpdateRule(),
            _rs(rs),
            _visualizer("PointCloudViewer"),
            _pointCloud(new pcl::PointCloud<pcl::PointXYZRGB>())
        {
        }

        ~RealSenseD400UpdateRule()
        {

        }

        void update(rw::kinematics::State& state)
        {
            try {
                _rs->acquire();
                cv::Mat image = _rs->getCamera()->getLastImage();
                cv::imshow("RealSenseD400Image", image);
                _rs->getColoredPointCloud(*_pointCloud);
                _visualizer.removeAllPointClouds();
                _visualizer.addPointCloud<pcl::PointXYZRGB>(_pointCloud, "sample cloud");
                
            }
            catch (const std::exception& exp) {
                ER_LOG_ERROR("Camera caught exception: " + (std::string)exp.what());
                ER_LOG_DEBUG("Trying to reconnect to camera");
            }
        }

        er_common::Entity::Ptr getEntity() {
            return _rs;
        }


    private:
        RealSenseD400::Ptr _rs;
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr _pointCloud;
        pcl::visualization::PCLVisualizer _visualizer;
    };

    /**
     * @brief Returns instance of the Camera interface representing the color camera of the RealSense
     */
    hwl::Camera::Ptr getCamera();

    /**
     * @brief Returns instance of the CameraDepth interface representing the depth part of the RealSense
     */
    hwl::CameraDepth::Ptr getDepthCamera();


    /* Methods from the serializable interface */
    /**
     * @copydoc Serializable::read
     */
	virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    /**
     * @copydoc Serializable::write
     */
    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

private:
    void init();

    rw::common::Ptr<RealSenseD400Core> _core;
    rw::common::Ptr<RealSenseD400Camera> _camera;
    rw::common::Ptr<RealSenseD400Depth> _depth;

};


} /* namespace hwl */

#endif /* SRC_HARDWARELAYER_REALSENSED400_REALSENSED400_HPP */
