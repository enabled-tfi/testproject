#include <Common/EntityRegister.hpp>
#include <HardwareLayer/RealSenseD400/RealSenseD400.hpp>
#include <HardwareLayer/Camera.hpp>
#include <HardwareLayer/CameraDepth.hpp>
#include <librealsense2/rs.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

using namespace rs2;

using namespace hwl;
using namespace er_common;
using namespace rw::common;


class hwl::RealSenseD400Core {
public:
    typedef rw::common::Ptr<RealSenseD400Core> Ptr;
    RealSenseD400Core()
    {
        // Configure and start the pipeline
    }

    void connect(int width, int height)
    {
        ER_LOG_DEBUG("RealSenseD400Core::connect");
        config conf;
        conf.enable_stream(rs2_stream::RS2_STREAM_COLOR, width, height, rs2_format::RS2_FORMAT_BGR8);
        conf.enable_stream(rs2_stream::RS2_STREAM_DEPTH);
        _pipeline.start(conf);
        ER_LOG_DEBUG("RealSenseD400Core::connected");
    }

    void acquire() {
        _frames = _pipeline.wait_for_frames();
    }

    depth_frame getLastDepthFrame() {
        return _frames.get_depth_frame();
    }

    video_frame getLastColorFrame() {
        return _frames.get_color_frame();
    }

private:
    // Create a Pipeline, which serves as a top-level API for streaming and processing frames
    pipeline _pipeline;
    frameset _frames;
    
};


class hwl::RealSenseD400Camera : public Camera
{
private:
    RealSenseD400Core::Ptr _core;
    int _width;
    int _height;
public:
    RealSenseD400Camera(RealSenseD400Core::Ptr core):
        Camera(""),
        _core(core)
    {        
    }

   /* RealSenseD400Camera(const std::string& identifier) :
        Camera(identifier)
    {

    }*/

    void init() {
        EntityRegister::add(getName(), this);
    }

    /**
    * @copydoc HWEntity::connect
    */
    virtual void connect() {}

    /**
    * @copydoc HWEntity::disconnect
    */
    virtual void disconnect() {}

    /**
    * @copydoc Camera::acquire
    */
    virtual void acquire()
    {
        _core->acquire();
    }


    /**
    * @copydoc Camera::getLastImage
    */
    virtual cv::Mat getLastImage()
    {

        rs2::video_frame vf = _core->getLastColorFrame();
        const int w = vf.get_width();
        const int h = vf.get_height();

        if (vf.get_profile().format() == RS2_FORMAT_BGR8)
        {
            return cv::Mat(cv::Size(w, h), CV_8UC3, (void*)vf.get_data(), cv::Mat::AUTO_STEP);
        }
        else if (vf.get_profile().format() == RS2_FORMAT_RGB8)
        {
            auto r = cv::Mat(cv::Size(w, h), CV_8UC3, (void*)vf.get_data(), cv::Mat::AUTO_STEP);
            cv::cvtColor(r, r, CV_RGB2BGR);
            return r;
        }
        else if (vf.get_profile().format() == RS2_FORMAT_Z16)
        {
            return cv::Mat(cv::Size(w, h), CV_16UC1, (void*)vf.get_data(), cv::Mat::AUTO_STEP);
        }
        else if (vf.get_profile().format() == RS2_FORMAT_Y8)
        {
            return cv::Mat(cv::Size(w, h), CV_8UC1, (void*)vf.get_data(), cv::Mat::AUTO_STEP);
        }
        RW_THROW("RealSenseD400Camera::Frame format is not supported yet!");
    }

    int getWidth() const
    {
        return _width;
    }

    int getHeight() const {
        return _height;
    }

    /* Methods from the serializable interface */
    /**
    * @copydoc Serializable::read
    */
    void read(class er_serialization::InputArchive& iarchive, const std::string& id)
    {        
        iarchive.readEnterScope("RealSenseD400Camera");
        Camera::read(iarchive, id);
        _width = iarchive.read<int>("Width");
        _height = iarchive.read<int>("Height");
        init();
        iarchive.readLeaveScope("RealSenseD400Camera");
    }

    /**
    * @copydoc Serializable::write
    */
    void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
    {
        oarchive.writeEnterScope("RealSenseD400Camera");
        Camera::write(oarchive, id);
        oarchive.write(_width, "Width");
        oarchive.write(_height, "Height");
        oarchive.writeLeaveScope("RealSenseD400Camera");
    }

};


class hwl::RealSenseD400Depth: public CameraDepth
{
private:
    RealSenseD400Core::Ptr _core;
    int _width;
    int _height;
public:
    RealSenseD400Depth(RealSenseD400Core::Ptr core) :
        CameraDepth(""),
        _core(core)
    {
    }

    void init() {
        EntityRegister::add(getName(), this);
    }

    /**
    * @copydoc HWEntity::connect
    */
    virtual void connect() {}

    /**
    * @copydoc HWEntity::disconnect
    */
    virtual void disconnect() {}

    /**
    * @copydoc Camera::acquire
    */
    virtual void acquire()
    {
        _core->acquire();
    }

    


    std::tuple<uint8_t, uint8_t, uint8_t> get_texcolor(rs2::video_frame texture, rs2::texture_coordinate texcoords, const int ridx, const int gidx, const int bidx)
    {
        
        const int w = texture.get_width(), h = texture.get_height();
        int x = std::min(std::max(int(texcoords.u*w + .5f), 0), w - 1);
        int y = std::min(std::max(int(texcoords.v*h + .5f), 0), h - 1);
        int idx = x * texture.get_bytes_per_pixel() + y * texture.get_stride_in_bytes();
        const auto texture_data = reinterpret_cast<const uint8_t*>(texture.get_data());
        
        /*if (i % 10000 == 0) {
            std::cout << "tex coords " << texcoords.u << " " << texcoords.v << std::endl;
            std::cout << "Texture idx = " << i << std::endl;
        }*/
        return std::tuple<uint8_t, uint8_t, uint8_t>(texture_data[idx+ridx], texture_data[idx + gidx], texture_data[idx+bidx]);
    }

    void getColorOffsets(rs2_format format, int& r, int& g, int& b) {
        if (format == RS2_FORMAT_BGR8 || format == RS2_FORMAT_BGRA8) {
            r = 2;
            g = 1;
            b = 0;
        }
        else if (format == RS2_FORMAT_RGB8 ||format == RS2_FORMAT_RGBA8) {
            r = 2;
            g = 1;
            b = 0;
        }
        else {
            RW_THROW("Color encoding format " << format << " not supported yet");
        }

    }

    virtual void getLastPointCloud(pcl::PointCloud<pcl::PointXYZRGB>& cloud)
    {
        rs2::pointcloud pc;
        auto depth = _core->getLastDepthFrame();
        auto image = _core->getLastColorFrame();
        // Generate the pointcloud and texture mappings
        pc.map_to(image);
        auto points = pc.calculate(depth);
        
       
        auto sp = points.get_profile().as<rs2::video_stream_profile>();
        cloud.width = sp.width();
        cloud.height = sp.height();        
        cloud.is_dense = false;
        cloud.points.resize(points.size());

        auto tex_coords = points.get_texture_coordinates();
        auto vertices = points.get_vertices();
        int r, g, b;
        getColorOffsets(image.get_profile().format(), r, g, b);
        for (int i = 0; i < points.size(); ++i)
        {
            cloud.points[i].x = vertices[i].x;
            cloud.points[i].y = vertices[i].y;
            cloud.points[i].z = vertices[i].z;

            std::tuple<uint8_t, uint8_t, uint8_t> current_color;
            current_color = get_texcolor(image, tex_coords[i], r, g, b);

            cloud.points[i].r = std::get<0>(current_color);
            cloud.points[i].g = std::get<1>(current_color);
            cloud.points[i].b = std::get<2>(current_color);
        }

    }



    /**
    * @copydoc CameraDepth::getLastPointCloud()
    */
    virtual void getLastPointCloud(pcl::PointCloud<pcl::PointXYZ>& cloud)
    {
        rs2::pointcloud pc;
        auto depth = _core->getLastDepthFrame();
        // Generate the pointcloud and texture mappings
        auto points = pc.calculate(depth);
        auto sp = points.get_profile().as<rs2::video_stream_profile>();
        cloud.width = sp.width();
        cloud.height = sp.height();
        cloud.is_dense = false;
        cloud.points.resize(points.size());
        auto ptr = points.get_vertices();
        for (auto& p : cloud.points)
        {
            p.x = ptr->x;
            p.y = ptr->y;
            p.z = ptr->z;
            ptr++;
        }
    }

    int getWidth() const
    {
        return _width;
    }

    int getHeight() const {
        return _height;
    }

    /* Methods from the serializable interface */
    /**
    * @copydoc Serializable::read
    */
    void read(class er_serialization::InputArchive& iarchive, const std::string& id)
    {
        iarchive.readEnterScope("RealSenseD400Depth");
        CameraDepth::read(iarchive, id);
        init();
        iarchive.readLeaveScope("RealSenseD400Depth");
    }

    /**
    * @copydoc Serializable::write
    */
    void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
    {
        oarchive.writeEnterScope("RealSenseD400Depth");
        CameraDepth::write(oarchive, id);
        oarchive.writeLeaveScope("RealSenseD400Depth");
    }
};

RealSenseD400::RealSenseD400():
    HWEntity("")
{
    _core = ownedPtr(new RealSenseD400Core());
    _camera = ownedPtr(new RealSenseD400Camera(_core));
    _depth = ownedPtr(new RealSenseD400Depth(_core));
}

RealSenseD400::~RealSenseD400()
{

}

void RealSenseD400::init()
{
    EntityRegister::add(getName(), this);   
    
}

/**
* @copydoc HWEntity::connect
*/
void RealSenseD400::connect()
{
    _core->connect(_camera->getWidth(), _camera->getHeight());
}

/**
* @copydoc HWEntity::disconnect
*/
void RealSenseD400::disconnect()
{

}

void RealSenseD400::acquire()
{
    _core->acquire();
}


hwl::Camera::Ptr RealSenseD400::getCamera()
{
    return _camera;
}

hwl::CameraDepth::Ptr RealSenseD400::getDepthCamera()
{
    return _depth;
}

void RealSenseD400::getColoredPointCloud(pcl::PointCloud<pcl::PointXYZRGB>& cloud)
{
    _depth->getLastPointCloud(cloud);
}

/* Methods from the serializable interface */
/**
* @copydoc Serializable::read
*/
void RealSenseD400::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    iarchive.readEnterScope("RealSenseD400");
    HWEntity::read(iarchive, id);

    _camera->read(iarchive, "Camera");
    //_depth->read(iarchive, "CameraDepth");
    
    iarchive.readLeaveScope("RealSenseD400");
    init();
}

/**
* @copydoc Serializable::write
*/
void RealSenseD400::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("RealSenseD400");
    HWEntity::write(oarchive, id);
    _camera->write(oarchive, "Camera");
    //_depth->write(oarchive, "CameraDepth");
    oarchive.writeLeaveScope("RealSenseD400");
}
