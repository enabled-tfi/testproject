#include "LeuzeODSL9Analog.hpp"

#include <Common/EntityRegister.hpp>

#include <rw/common/TimerUtil.hpp>
#include <boost/bind.hpp>
#include <boost/thread/mutex.hpp>

using namespace hwl;
using namespace rw::math;
using namespace rw::common;
using namespace er_common;


LeuzeODSL9Analog::LeuzeODSL9Analog():
	DistanceSensor1D("LeuzeODSL9Analog")
{

}

LeuzeODSL9Analog::LeuzeODSL9Analog(const std::string& id) :
	DistanceSensor1D(id)
{

}

void LeuzeODSL9Analog::connect()
{
	ER_LOG_DEBUG("Available Entities: " );
	for (auto elem : EntityRegister::findMatches<HWEntity>()) {
		ER_LOG_DEBUG("\t" << elem);
	}

	if (EntityRegister::has(_ioInterfaceIdentifier)) {
		_ioInterface = EntityRegister::get<IOInterface>(_ioInterfaceIdentifier);
		ER_LOG_DEBUG("IOInterface = " << _ioInterface );
	}
	else {
		RW_THROW("Unable to find IOInterface named '" << _ioInterfaceIdentifier << "'");
	}
}

void LeuzeODSL9Analog::disconnect()
{
	_ioInterface = NULL;
}


void LeuzeODSL9Analog::sensorOn()
{

	bool digout = _ioInterface->getDigitalOut(_digitalOutIdx);
	if (digout == false) {
		_ioInterface->setDigitalOut(_digitalOutIdx, true);
	}
}

void LeuzeODSL9Analog::sensorOff()
{
	_ioInterface->setDigitalOut(_digitalOutIdx, false);
}

/**
* @brief returns the last measured distance from the sensopr
*/
double LeuzeODSL9Analog::getDistance()
{
	if (_ioInterface == NULL) {
		RW_THROW("No IO Interface available for receiving distance");
	}
	double analogIn = _ioInterface->getAnalogIn(_analogInIdx);
//    RW_LOG_DEBUG("Analog Signal["<<_analogInIdx<<"] : " << analogIn);
	const double a = (_sensingRange(1)-_sensingRange(0)) / (_outputRange(1) - _outputRange(0));
	const double b = _sensingRange(0) - a*_outputRange(0);
    double dist = a*analogIn + b;
    if (dist > _sensingRange(1))
	    return -2;
    if (dist < _sensingRange(0))
        return -1;
    return dist;
}

std::pair<double, double> LeuzeODSL9Analog::getRange() const
{
    return std::make_pair(_sensingRange(0), _sensingRange(1));
}

/* Methods from the serializable interface */
void LeuzeODSL9Analog::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
	iarchive.readEnterScope("LeuzeODSL9Analog");
    DistanceSensor1D::read(iarchive, id);
	_ioInterfaceIdentifier = iarchive.read<std::string>("IOInterfaceIdentifier");
	_analogInIdx = iarchive.read<int>("AnalogInIdx");
	_digitalOutIdx = iarchive.read<int>("DigitalOutIdx");
	_sensingRange = iarchive.read<Vector2D<> >("SensingRange");
	_outputRange = iarchive.read<Vector2D<> >("OutputRange");
	iarchive.readLeaveScope("LeuzeODSL9Analog");
}

void LeuzeODSL9Analog::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	ER_LOG_DEBUG("Load LeuzeODSL9Analog" );
	oarchive.writeEnterScope("LeuzeODSL9Analog");
	DistanceSensor1D::write(oarchive, id);

	oarchive.write(_ioInterfaceIdentifier, "IOInterfaceIdentifier");
	oarchive.write(_analogInIdx, "AnalogInIdx");
	oarchive.write(_digitalOutIdx, "DigitalOutIdx");
	oarchive.write(_sensingRange, "SensingRange");
	oarchive.write(_outputRange, "OutputRange");
	oarchive.writeLeaveScope("LeuzeODSL9Analog");
}
