/* */
#ifndef HWL_LEUZEODSL9ANALOG_HPP
#define HWL_LEUZEODSL9ANALOG_HPP


#include "../DistanceSensor1D.hpp"
#include <HardwareLayer/IOInterface.hpp>
#include <rw/common/Ptr.hpp>
#include <rw/math.hpp>



namespace hwl {
	
/**
 * General interface for actions 
 */
class LeuzeODSL9Analog: public DistanceSensor1D
{
public:
	typedef rw::common::Ptr<LeuzeODSL9Analog> Ptr;

	LeuzeODSL9Analog();
	LeuzeODSL9Analog(const std::string& id);


	virtual void connect();

	virtual void disconnect();


	void sensorOn();
	void sensorOff();

	/**
	* @brief returns the last measured distance from the sensopr
	*/
	double getDistance();

    virtual std::pair<double, double> getRange() const;

	/* Methods from the serializable interface */
	virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

	virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

private:
	std::string _ioInterfaceIdentifier;
	int _analogInIdx;
	int _digitalOutIdx;
	rw::math::Vector2D<> _sensingRange;
	rw::math::Vector2D<> _outputRange;
	IOInterface::Ptr _ioInterface;


	//void callback(LxS::eData dt, BYTE * ptr, int len);
};

} //end namespace

#endif //#ifndef HWL_LEUZELPS36_HPP
