/* */
#ifndef HWL_LEUZELPS36_HPP
#define HWL_LEUZELPS36_HPP


#include "../Scanner2D.hpp"

#include <rw/common/Ptr.hpp>
#include <rw/math.hpp>

#include <LxS_Lib/LxS_Device.h>


namespace hwl {
	
/**
 * General interface for actions 
 */
class LeuzeLPS36: public Scanner2D
{
public:
	typedef rw::common::Ptr<LeuzeLPS36> Ptr;

	LeuzeLPS36(const std::string& id);

	/**
	* @copydoc Scanner2D::connect
	*/
	virtual void connect(const std::string& host);

	virtual void disconnect();

	/**
	* @copydoc Scanner2D::getScan
	*/
	virtual std::vector<rw::math::Vector3D<> > getScan();

	virtual void scannerOn();
	virtual void scannerOff();

private:
	rw::common::Ptr<LxS::Device> _lpsDevice;

	//void callback(LxS::eData dt, BYTE * ptr, int len);
};

} //end namespace

#endif //#ifndef HWL_LEUZELPS36_HPP
