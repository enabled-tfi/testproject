#include "LeuzeLPS36.hpp"
#include <rw/common/TimerUtil.hpp>
#include <boost/bind.hpp>
#include <boost/thread/mutex.hpp>
using namespace hwl;
using namespace rw::math;
using namespace rw::common;


LeuzeLPS36::LeuzeLPS36(const std::string& id):
        Scanner2D(id)
{

}


namespace {
	UINT16 last_scan;
	INT16 * before_z = NULL;
	INT16 * before_x = NULL;
	HWND last_hwnd;	

	boost::mutex mutex;

}

void CALLBACK callback(LxS::eData dt, BYTE * ptr, int len)
{
	//ER_LOG_DEBUG("Got Call Back Data " << dt<<" "<<len);
	
	if (dt == LxS::LXS_DATA_Z)
	{
		boost::mutex::scoped_lock scoped_lock(mutex);
		delete[] before_z;
		before_z = (INT16*)ptr;
	};
	if (dt == LxS::LXS_DATA_X)
	{
	/*	UINT16 sn = lxs->GetScanNumber();
		if (sn != last_scan)
		{
			wchar_t buffer[256];
			last_scan = sn;
			swprintf(buffer, 256, L"Scan: %d", last_scan);
			SendMessage(GetDlgItem(last_hwnd, IDC_MY_EDT_013), WM_SETTEXT, NULL, (LPARAM)buffer);
		};
		
	*/	
		boost::mutex::scoped_lock scoped_lock(mutex);
		delete[] before_x;
		before_x = (INT16*)ptr;
		//InvalidateRect(last_hwnd, update_rect, false);
	};
	
	if (dt == LxS::LXS_DATA_ZX)
	{
		//UINT16 sn = lxs->GetScanNumber();
		//if (sn != last_scan)
		//{
			//wchar_t buffer[256];
		//	last_scan = sn;
			//swprintf(buffer, 256, L"Scan: %d", last_scan);
			//SendMessage(GetDlgItem(last_hwnd, IDC_MY_EDT_013), WM_SETTEXT, NULL, (LPARAM)buffer);
		//};
		boost::mutex::scoped_lock scoped_lock(mutex);
		delete[] before_z;
		// no delete, because before_x points on the same buffer.
		before_z = (INT16*)ptr;
		before_x = (INT16*)(ptr + 2*376);
		//InvalidateRect(last_hwnd, update_rect, false);
	};
	
}



void LeuzeLPS36::connect(const std::string& host)
{
	_lpsDevice = ownedPtr(new LxS::Device());
	if (_lpsDevice->Create(host.c_str()) == false)
	{
		RW_THROW("Unable tof create Leuze LPS36 Device");
	}
	if (_lpsDevice->Connect() == false) {
		RW_THROW("Unable to connect to Leuze LPS36 Device");
		
	}
	ER_LOG_DEBUG("LPS Connected" );
	_lpsDevice->SetDataHandler(callback);
//	_lpsDevice->SetSensorMode(LxS::LXS_MODE_MEASURE);
}


void LeuzeLPS36::disconnect() {
}


void LeuzeLPS36::scannerOn()
{
	ER_LOG_DEBUG("Set Scanner  On " );
	_lpsDevice->SetSensorMode(LxS::LXS_MODE_COMMAND);
	_lpsDevice->SetLaserGate(true);
	_lpsDevice->SetSensorMode(LxS::LXS_MODE_MEASURE);
}

void LeuzeLPS36::scannerOff()
{
	ER_LOG_DEBUG("Set Scanner Off" );
	_lpsDevice->SetSensorMode(LxS::LXS_MODE_COMMAND);
	_lpsDevice->SetLaserGate(false);
}


std::vector<rw::math::Vector3D<> > LeuzeLPS36::getScan()
{
	ER_LOG_DEBUG("Get Scan " );
	std::vector<Vector3D<> > result;

	if (before_x == NULL || before_x == NULL)
		return result;


	for (size_t i = 0; i < 376; i++) 
	{
		double x = before_x[i] / 1000.0 /10.0;
		double z = before_z[i] / 1000.0 /10.0;
		result.push_back(Vector3D<>(x, 0, z));
	}
	return result;
}
