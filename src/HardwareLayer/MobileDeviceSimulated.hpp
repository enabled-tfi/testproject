/* */
#ifndef HWL_MOBILEDEVICESIMULATED_HPP
#define HWL_MOBILEDEVICESIMULATED_HPP

#include "MobileDevice.hpp"
#include "SimulatedEntity.hpp"
#include "../Common/System.hpp"

#include <rw/common/Ptr.hpp>
#include <rw/math.hpp>
#include <rw/kinematics.hpp>
#include <rw/models.hpp>
#include <rw/invkin/JacobianIKSolver.hpp>

namespace hwl {
	
/**
 * Simulated Mobile Device
 */
class MobileDeviceSimulated: public MobileDevice, public SimulatedEntity
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<MobileDeviceSimulated> Ptr;

	MobileDeviceSimulated();

	/**
	 * @brief Construct MobileDeviceSimulated
	 *
	 * @param deviceBase [in] MovableFrame used to simulate the device
	 * @param system [in] The general simulation system it is part of
	 */
	MobileDeviceSimulated(rw::kinematics::MovableFrame::Ptr deviceBase, const er_common::System::Ptr system, const std::string& id);


	virtual void initializeSimulation(rws::RobWorkStudio* rws);
    /**
     * @copydoc MobileDevice::move
     */
    virtual void move(double distance, double angle);

    /**
     * @copydoc MobileDevice::moveTo
     */
    virtual void moveTo(const rw::math::Pose2D<>& target, double goalThreshold);

    /**
     * @copydoc MobileDevice::getCurrentTransform
     */
    virtual rw::math::Pose2D<> getCurrentPose() const;

    /**
     * @copydoc MobileDevice::getDefinedPositions
     */
    virtual std::vector<DefinedPosition> getDefinedPositions() const ;

	/**
	 * @copydoc MobileDevice::isMoving
	 */
    virtual bool isMoving() const;

	/**
	* @copydoc HWEntity::connect
	*/
	virtual void connect() {}

	/**
	* @copydoc HWEntity::disconnect
	*/
	virtual void disconnect() {};

	/* Methods from the serializable interface */
	virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

	virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


private:

    void doMoveTo(const rw::math::Pose2D<>& target);

	rw::kinematics::MovableFrame::Ptr _deviceBase;	
	er_common::System::Ptr _system;
	std::string _frameName;
	bool _isMoving;
    double _dt;
    double _velLinear;
    double _velRotational;


};

} //end namespace

#endif //#ifndef HWL_MOBILEDEVICESIMULATED_HPP
