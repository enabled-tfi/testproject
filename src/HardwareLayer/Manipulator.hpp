/* */
#ifndef HWL_MANIPULATOR_HPP
#define HWL_MANIPULATOR_HPP


#include <Common/HWEntity.hpp>
#include <Common/System.hpp>

#include <rw/common/Ptr.hpp>
#include <rw/math.hpp>
#include <rw/models/Device.hpp>
#include <rw/kinematics/State.hpp>

namespace hwl {
	
/**
 * General interface for a serial device
 */
class Manipulator: public HWEntity
{
public:

    Manipulator(const std::string& identifier);

	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<Manipulator> Ptr;

	/**
	 * @brief Perform a point-to-point motion (linear in configuration space)
	 * @param qtarget [in] Target configuration
	 * @param speed [in] Speed of the motion
	 */
	virtual void ptp(const rw::math::Q& qtarget, double speed = 0) = 0;


    /**
     * @brief Perform a point-to-point motion (linear in configuration space)
     * @param target [in] Target base to tool transform
     * @param speed [in] Speed of the motion
     */
    virtual void ptp(const rw::math::Transform3D<>& target, double speed = 0) = 0;

	/**
	 * @brief Perform a linear motion (linear in work space)
	 *
	 * The target may be expressed in which-ever frame the robot is set to use.
	 *
	 * @param target [in] Target configuration.
	 * @param speed [in] Speed of the motion
	 *
	 */
	virtual void lin(const rw::math::Transform3D<>& target, double speed = 0) = 0;


	/**
	 * @brief Stops the robot
	 *
	 */
	virtual void stop() = 0;

	/**
	 * @brief Returns the current configuration
	 * @return Current configuration
	 */
	virtual rw::math::Q getQ() const = 0;

	/**
	 @brief Returns lower and upper bound for the robot joint configurations.
	*/
	virtual std::pair<rw::math::Q, rw::math::Q> getBounds() const = 0;

	/**
	 * @brief Sets the TCP transform for the robot
	 */
	virtual void setEndToTCP(const rw::math::Transform3D<>& endTtcp) = 0;

	/**
	 * @brief Force reconnects the manipulator
	 */
	virtual void reconnect() = 0;

	virtual rw::math::Transform3D<> getEndToTCP() const = 0;

	virtual rw::math::Transform3D<> getBaseToTCP() const = 0;

	virtual void startTeachMode() = 0;
	virtual void endTeachMode() = 0;

	class ManipulatorUpdateRule: public er_common::System::UpdateRule
	{
	public:
		typedef rw::common::Ptr<ManipulatorUpdateRule> Ptr;

		ManipulatorUpdateRule(rw::models::Device::Ptr device, Manipulator::Ptr hwlEntity):
			er_common::System::UpdateRule(),
			_device(device),
			_hwlEntity(hwlEntity)
		{
		}

		void update(rw::kinematics::State& state)
		{			
			try {
				rw::math::Q q = _hwlEntity->getQ();
				_device->setQ(q, state);
			} catch (const std::exception& exp) {
				ER_LOG_ERROR("Exception when updating manipulator: ");
				ER_LOG_ERROR(exp.what());
				ER_LOG_WARNING("Trying to reconnect");
				_hwlEntity->disconnect();
				_hwlEntity->connect();
			}

		}

		void setDevice(rw::models::Device::Ptr device){
			_device = device;
		}

		er_common::Entity::Ptr getEntity() {
			return _hwlEntity;
		}

	private:
		rw::models::Device::Ptr _device;
		Manipulator::Ptr _hwlEntity;
	};

	ManipulatorUpdateRule::Ptr getManipulatorUpdateRule();

	/*
	* @copydoc HWEntity::addUpdateRule
	*/
	void addUpdateRule(er_common::System::Ptr system);

	Manipulator::ManipulatorUpdateRule::Ptr _updateRule;

};

} //end namespace

#endif //#ifndef HWL_MANIPULATOR_HPP
