/*
 * Action.hpp
 *
 *  Created on: May 2, 2019
 *      Author: thomas
 */

#ifndef SRC_HARDWARELAYER_ACTION_HPP_
#define SRC_HARDWARELAYER_ACTION_HPP_

#include <ExecutionModel/Instruction.hpp>

namespace hwl {

class Action{
public:
	typedef rw::common::Ptr<Action> Ptr;

	Action();
	virtual ~Action();
};

} /* namespace hwl */

#endif /* SRC_HARDWARELAYER_ACTION_HPP_ */
