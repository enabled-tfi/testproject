#include "UR.hpp"

#include <rw/common/TimerUtil.hpp>
#include <rwhw/universalrobots/UniversalRobotsData.hpp>
#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>
#include <Common/EntityRegister.hpp>

using namespace hwl;
using namespace er_common;
using namespace rw::math;
using namespace rw::common;
using namespace rwhw;

UR::UR():
        Manipulator("UR")
{
	ER_LOG_DEBUG("UR Default constructor");
	_urInterface = new rwhw::URCallBackInterface();
	_urLogging = new rwhw::UniversalRobotsRTLogging();
	init();
}

UR::UR(const std::string& ipaddress, URMode mode) :
	Manipulator("UR"),
	_ipaddress(ipaddress),
	_mode(mode)
{
	_urInterface = new rwhw::URCallBackInterface();
	_urLogging = new rwhw::UniversalRobotsRTLogging();
	init();

}


UR::~UR()
{
	ER_LOG_DEBUG( "UR destructor ");
	disconnect();
	ER_LOG_DEBUG("Disconnect finished ");

}


void UR::init()
{
	_ioInterface = ownedPtr(new URIOInterface(_urInterface->getPrimaryInterface()));
    
	std::stringstream sstr;
	sstr << getName() << ".IOInterface";
	EntityRegister::add(sstr.str(), _ioInterface);
}

URIOInterface::Ptr UR::getIOInterface() {
	return _ioInterface;
}

void UR::connect()
{
	try {
		_urLogging->connect(_ipaddress);
		_urLogging->start();

		_urInterface->connect(_ipaddress, 30002);

		_urInterface->getPrimaryInterface().start();
		if (_mode == ControlAndListen)
		{
			_urInterface->startCommunication(33334, rwhw::URCallBackInterface::CB3);
		}
	} catch (const std::exception& exp) {
		ER_LOG_ERROR("Could not connect to UR: ");
		ER_LOG_ERROR(exp.what());
	}
} 

void UR::reconnect(){

	if (_urInterface->isConnected()) {
		_urInterface->stopCommunication();
	}
	delete _urInterface;

	_urInterface = new rwhw::URCallBackInterface();

	_urInterface->connect(_ipaddress, 30002);

	if (_mode == ControlAndListen)
	{
		_urInterface->startCommunication(33334, rwhw::URCallBackInterface::CB3);
	}

	_urInterface->getPrimaryInterface().clearMessages();
}
 
void UR::disconnect() {
	_urLogging->stop();
	_urLogging->disconnect();

	if (_urInterface->isConnected()) {
		_urInterface->stopCommunication();
	}

}

void UR::ptp(const rw::math::Q& qtarget, double speed)
{
	ER_LOG_DEBUG("ur::ptp"<<qtarget<<" "<<_urInterface->isMoving());
	_urInterface->moveQ(qtarget, (float)speed);
	while ((getQ() - qtarget).normInf() > 0.1*Deg2Rad) {
		rw::common::TimerUtil::sleepMs(8);
	}
}

void UR::ptp(const rw::math::Transform3D<>& target, double speed)
{
    ER_LOG_DEBUG("UR::ptp: "<<target);
    _urInterface->moveT(target, (float)speed, 0);

    double error = 0;
    do {
        rwhw::URRTData data = _urLogging->getLastData();
        Transform3D<> diff = Transform3D<>(Vector3D<>(data.toolPose(0), data.toolPose(1), data.toolPose(2)), EAA<>(data.toolPose(3), data.toolPose(4), data.toolPose(5))) * inverse(target);
        double perror = diff.P().norm2();
        double angleError = EAA<>(diff.R()).angle();
        error = std::max(perror, angleError);
    } while (error > 0.005);

    while (_urInterface->isMoving()) {
        rw::common::TimerUtil::sleepMs(8);
    }

}


void UR::lin(const rw::math::Transform3D<>& target, double speed)
{
	ER_LOG_DEBUG("UR::lin: "<<target);
	_urInterface->moveT(target, (float)speed, 0);

	double error = 0;
	do {
		rwhw::URRTData data = _urLogging->getLastData();
		Transform3D<> diff = Transform3D<>(Vector3D<>(data.toolPose(0), data.toolPose(1), data.toolPose(2)), EAA<>(data.toolPose(3), data.toolPose(4), data.toolPose(5))) * inverse(target);
		double perror = diff.P().norm2();
		double angleError = EAA<>(diff.R()).angle();
		error = std::max(perror, angleError);
	} while (error > 0.005);

	while (_urInterface->isMoving()) {
		rw::common::TimerUtil::sleepMs(8);
	}

}

void UR::stop(){
	_urInterface->stopRobot();
}

rw::math::Q UR::getQ() const
{
    
  // Q q = _urInterface->getPrimaryInterface().getLastData().jointPosition;
 //  std::cout << "Q from primary " << q << std::endl;
	if(_urLogging){
        if (_urLogging->hasData()) {
			rwhw::URRTData data = _urLogging->getLastData();
			//ER_LOG_DEBUG("QTarget = " << data.qTarget );
			//ER_LOG_DEBUG("QActual = " << data.qActual );
			//ER_LOG_DEBUG("Pose Target = " << data.toolTargetPose );
			//ER_LOG_DEBUG(" Pose Actual = " << data.toolPose ); 
			return data.qActual;
		}
	}
	else
		RW_THROW("UR Logging interface is null");

	RW_THROW("No data from UR");
}

std::pair<rw::math::Q, rw::math::Q> UR::getBounds() const
{
    Q qmin(6);
    Q qmax(6);
    for (size_t i = 0; i<6; i++)
    {
        qmin(i) = -Pi;
        qmax(i) = Pi;
    }
    return std::make_pair(qmin, qmax);

}


void UR::setEndToTCP(const rw::math::Transform3D<>& endTtcp)
{
	_urInterface->setTCPTransform(endTtcp);
	_endTtcp = endTtcp;
}

/**
* @copydoc Manipulator::getTCPTransform 
*/ 
rw::math::Transform3D<> UR::getEndToTCP() const
{
    Vector3D<> pos = _urInterface->getPrimaryInterface().getLastData().toolOffsetPosition;
    Vector3D<> rot = _urInterface->getPrimaryInterface().getLastData().toolOffsetAxisAngle;
    return Transform3D<>(pos, EAA<>(rot(0), rot(1), rot(2)));
	//return _endTtcp;
}

rw::math::Transform3D<> UR::getBaseToTCP() const
{/*
    Vector3D<> axisangle = _urInterface->getPrimaryInterface().getLastData().toolAxisAngle;
    Vector3D<> pos = _urInterface->getPrimaryInterface().getLastData().toolPosition;
    return Transform3D<>(pos, EAA<>(axisangle(0), axisangle(1), axisangle(2)));
   */     
	if(_urLogging){
		if (_urLogging->hasData()) {
			rwhw::URRTData data = _urLogging->getLastData();
			//ER_LOG_DEBUG("QTarget = " << data.qTarget );
			ER_LOG_DEBUG("QActual = " << data.qActual );
			//ER_LOG_DEBUG("Pose Target = " << data.toolTargetPose );
			ER_LOG_DEBUG(" Pose Actual = " << data.toolPose );
			Transform3D<> baseTend;
			baseTend.P()(0) = data.toolPose(0);
			baseTend.P()(1) = data.toolPose(1);
			baseTend.P()(2) = data.toolPose(2);
			baseTend.R() = EAA<>(data.toolPose(3), data.toolPose(4), data.toolPose(5)).toRotation3D();
			return baseTend;
		}
	}
	else
		RW_THROW("UR Logging interface is null");

	RW_THROW("No data from UR");
}

void UR::startTeachMode()
{
	_urInterface->teachModeStart();
}

void UR::endTeachMode()
{
	_urInterface->teachModeEnd();
}

/* Methods from the serializable interface */
void UR::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
	iarchive.readEnterScope("UR");
	HWEntity::read(iarchive, id);
	_ipaddress = iarchive.read<std::string>("IP");
	_mode = (URMode)iarchive.read<int>("Mode");
	iarchive.readLeaveScope("UR");
}

void UR::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	oarchive.writeEnterScope("UR");
	HWEntity::write(oarchive, id);
	oarchive.write(_ipaddress, "IP");
	oarchive.write((int)_mode, "Mode");
	oarchive.writeLeaveScope("UR");
}
