#include "URIOInterface.hpp"

#include <rw/common/TimerUtil.hpp>
#include <rwhw/universalrobots/UniversalRobotsData.hpp>
#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>
 
using namespace hwl;
using namespace rw::math;
using namespace rwhw;

URIOInterface::URIOInterface(rwhw::URPrimaryInterface& ur) :
	IOInterface("URIOInterface", true),
	_urInterface(ur)
{
	setDigitalInRange(0, 8);
	setDigitalOutRange(0, 8);
	setAnalogInRange(0, 3);
	setAnalogOutRange(0, 3);
}


URIOInterface::~URIOInterface()
{

}

void URIOInterface::connect() 
{

}


void URIOInterface::disconnect() 
{
}

void URIOInterface::addUpdateRule(er_common::System::Ptr system) 
{
}

bool URIOInterface::doGetDigitalIn(int idx)
{
	if (_urInterface.hasData())
		return _urInterface.getLastData().digitalIn[idx];
	RW_THROW("No data available from UR");
}

void URIOInterface::doSetDigitalOut(int idx, bool val)
{
	RW_THROW("Setting digital output not supported yet");
}

bool URIOInterface::doGetDigitalOut(int idx) {
	if (_urInterface.hasData())
		return _urInterface.getLastData().digitalOut[idx];
	RW_THROW("No data available from UR");
}

double URIOInterface::doGetAnalogIn(int idx)
{
	if (_urInterface.hasData())
		return _urInterface.getLastData().analogIn[idx];
	RW_THROW("No data available from UR");
}

void URIOInterface::doSetAnalogOut(int idx, double value)
{
	RW_THROW("Setting analog output not supported yet");
}

double URIOInterface::doGetAnalogOut(int idx) {
	if (_urInterface.hasData())
		return _urInterface.getLastData().analogOut[idx];
	RW_THROW("No data available from UR");

}

