/*
 * URDashboard.hpp
 *
 *  Created on: Mar 28, 2019
 *      Author: thomas
 */

#ifndef SRC_HARDWARELAYER_UR_URDASHBOARD_HPP_
#define SRC_HARDWARELAYER_UR_URDASHBOARD_HPP_

#include <iostream>
#include <boost/asio.hpp>
#include <boost/assign.hpp>

namespace hwl {

enum robotMode {
	NO_CONTROLLER,
	DISCONNECTED,
	CONFIRM_SAFETY,
	BOOTING,
	POWER_OFF,
	POWER_ON,
	IDLE,
	BACKDRVE,
	RUNNING
};

enum eprogramState {
	STOPPED,
	PLAYING,
	PAUSED
};

enum safetyMode{
	NORMAL,
	REDUCED,
	PROTECTIVE_STOP,
	RECOVERY,
	SAFEGUARD_STOP,
	SYSTEM_EMERGENCY_STOP,
	ROBOT_EMERGENCY_STOP,
	VIOLATION,
	FAULT
};

class URDashboard {
public:
	URDashboard(std::string ip);
	virtual ~URDashboard();

	/*
	 * @Brief connect to the UR dashboard server
	 * @return bool indicating connected/not connected
	 */
	bool connect();

	/*
	 * @brief "Program running: True" OR "Program running: False"
	 */
	bool running();

	/*
	 * @brief Starts the current loaded program
	 * @return Whether actions was successful or not
	 */
	bool play();

	/*
	 * @brief Stops the current program
	 * @return Whether actions was successful or not
	 */
	bool stop();

	/*
	 * @brief Pauses the current program
	 * @return Whether actions was successful or not
	 */
	bool pause();

	/*
	 * @brief Loads a program from the URController
	 * @param prg [in] Name of the program to load
	 * @return Whether actions was successful or not
	 */
	bool load(std::string prg);

	/*
	 * @brief Closes connection
	 */
	bool quit();

	/*
	 * @brief Shuts down and turns off robot and controller
	 */
	bool shutdown();

	/*
	 * @brief Returns the current mode of the robot
	 */
	robotMode robotmode();

	/*
	 * @brief Name of loaded program
	 */
	std::string getLoadedProgram();

	/*
	 * @brief Show popup with text
	 */
	bool popup(std::string tex);

	/*
	 * @brief Close popup
	 */
	bool closePopup();

	/*
	 * @brief Add message to the date log
	 */
	bool addToLog(std::string message);

	/*
	 * @brief Returns whether current program is saved
	 */
	bool isProgramSaved();

	/*
	 *  @brief Current state of the robot
	 */
	eprogramState getProgramState();

	/*
	 * @brief Return polyscope version
	 */
	std::string PolyscopeVersion();

	/*
	 * @brief Set operational mode to either AUTOMATIC or MANUAL
	 */
	bool setOperationalMode (std::string mode);

	/*
	 * @brief If this function is called the operational mode can again be changed from PolyScope, and the user password is enabled.
	 */
	bool clearOperationalMode();


	/*
	 * @brief Powers on the robot arm
	 */
	bool powerOn();

	/*
	 * @brief Powers off the robot arm
	 */
	bool powerOff();

	/*
	 * @brief Release brakes
	 */
	bool brakeRelease();

	/*
	 * @brief Safety mode enquiry
	 */
	safetyMode safetymode();

	/*
	 * @brief Closes the current popup and unlocks protective stop
	 */
	bool unlockProtectiveStop();

	/*
	 * @brief Closes a safety popup
	 */
	bool closeSafetyPopup();

	/*
	 * @brief Used when robot gets a safety fault or violation to restart the safety. After safety has been rebooted the robot will be in Power Off.
	 */
	bool restartSafety();


private:

	/*
	 * @brief Send command to the UR Dashboard
	 * @param [in] cmd Command to send
	 */
	bool sendCommand(std::string cmd);


		// Maps for easier parsing of strings to enums
	std::map<std::string, robotMode> xmap = boost::assign::map_list_of("NO_CONTROLLER", NO_CONTROLLER)
																      ("DISCONNECTED", DISCONNECTED)
																	  ("CONFIRM_SAFETY",CONFIRM_SAFETY)
																      ("DISCONNECTED", DISCONNECTED)
																      ("BOOTING", BOOTING)
																      ("POWER_OFF", POWER_OFF)
																      ("POWER_ON", POWER_ON)
																      ("IDLE", IDLE)
																      ("BACKDRVE", BACKDRVE)
																      ("RUNNING", RUNNING);

	std::map<std::string, eprogramState> prgStateMap = boost::assign::map_list_of("STOPPE", STOPPED)
																      	     ("PLAYIN", PLAYING)
																			 ("PAUSED",PAUSED);

	std::map<std::string, safetyMode> safetyModeMap = boost::assign::map_list_of("NORMAL", NORMAL)
																      	     ("REDUCED", REDUCED)
																			 ("PROTECTIVE_STOP",PROTECTIVE_STOP)
																      	     ("RECOVERY", RECOVERY)
																      	     ("SAFEGUARD_STOP", SAFEGUARD_STOP)
																      	     ("SYSTEM_EMERGENCY_STOP", REDUCED)
																      	     ("ROBOT_EMERGENCY_STOP", ROBOT_EMERGENCY_STOP)
																      	     ("VIOLATION", VIOLATION)
																      	     ("FAULT", FAULT);

	/*
	 * @brief Receive data on the socket connection'
	 * @return Received data
	 */
	std::string receiveData();

	bool _connected;
	std::string _ip;

	boost::asio::ip::tcp::socket* _socket;
	boost::asio::io_service _ioService;

};

} /* namespace hwl */

#endif /* SRC_HARDWARELAYER_UR_URDASHBOARD_HPP_ */
