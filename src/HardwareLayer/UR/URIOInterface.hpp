/* */
#ifndef HWL_URIOINTERFACE_HPP
#define HWL_URIOINTERFACE_HPP

#include <HardwareLayer/Manipulator.hpp>
#include <HardwareLayer/IOInterface.hpp>

#include <rw/common/Ptr.hpp>
#include <rw/math.hpp>

#include <rwhw/universalrobots/UniversalRobotsRTLogging.hpp>
#include <rwhw/universalrobots/URCallBackInterface.hpp>


namespace hwl {
	
/**
 * General interface for actions 
 */
class URIOInterface: public IOInterface
{
public:
	typedef rw::common::Ptr<URIOInterface> Ptr;


	URIOInterface(rwhw::URPrimaryInterface& ur);

	~URIOInterface();

	void connect();
	void disconnect();
	void addUpdateRule(er_common::System::Ptr system);


    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id) {}

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const {}
protected:
	virtual bool doGetDigitalIn(int idx);
	virtual void doSetDigitalOut(int idx, bool val);
	virtual bool doGetDigitalOut(int idx);

	virtual double doGetAnalogIn(int idx);
	virtual void doSetAnalogOut(int idx, double value);
	virtual double doGetAnalogOut(int idx);

private: 
	std::string _urDeviceName;
	rwhw::URPrimaryInterface& _urInterface;

};

} //end namespace

#endif //#ifndef HWL_URIOINTERFACE_HPP
