/* */
#ifndef HWL_UR_HPP
#define HWL_UR_HPP

#include "../Manipulator.hpp"
#include "URIOInterface.hpp"

#include <rw/common/Ptr.hpp>
#include <rw/math.hpp>

#include <rwhw/universalrobots/UniversalRobotsRTLogging.hpp>
#include <rwhw/universalrobots/URCallBackInterface.hpp>


namespace hwl {
	
/**
 * General interface for actions 
 */
class UR: public Manipulator
{
public:
	typedef rw::common::Ptr<UR> Ptr;


	UR();
	enum URMode { ControlAndListen = 1, ListenOnly = 2};
	UR(const std::string& host, URMode mode);

	~UR();

	void connect();
	void disconnect();
	virtual void reconnect();

	virtual void ptp(const rw::math::Q& qtarget, double speed = 10);
	virtual void ptp(const rw::math::Transform3D<>& target, double speed = 10);
	virtual void lin(const rw::math::Transform3D<>& target, double speed = 10);

	rw::math::Q getQ() const;
	virtual std::pair<rw::math::Q, rw::math::Q> getBounds() const;

	/**
	* @copydoc Manipulator::setTCPTransform
	*/
	virtual void setEndToTCP(const rw::math::Transform3D<>& endTtcp);

	/**
	* @copydoc Manipulator::getTCPTransform
	*/
	virtual rw::math::Transform3D<> getEndToTCP() const;

	virtual rw::math::Transform3D<> getBaseToTCP() const;

	virtual void startTeachMode();
	virtual void endTeachMode();  

	virtual URIOInterface::Ptr getIOInterface();

	virtual void stop();

	 
	/* Methods from the serializable interface */
	virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

	virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

	 
	 
private: 
	void init();

	std::string _ipaddress; 
	URMode _mode;
	rwhw::URCallBackInterface* _urInterface;
	mutable rwhw::UniversalRobotsRTLogging* _urLogging;
	rw::math::Transform3D<> _endTtcp;
	URIOInterface::Ptr _ioInterface;

};

} //end namespace

#endif //#ifndef HWL_UR_HPP
