/*
 * UREvent.h
 *
 *  Created on: Apr 30, 2019
 *      Author: thomas
 */

#ifndef SRC_URACTIONSERVER_URACTION_HPP_
#define SRC_URACTIONSERVER_URACTION_HPP_

#include <iostream>
#include <vector>
#include <HardwareLayer/Action.hpp>

namespace hwl {

class URAction : public hwl::Action{
public:
	typedef rw::common::Ptr<URAction> Ptr;

	URAction(std::string name, std::vector<std::string> params);
	~URAction();

	std::string name;
	std::vector<std::string> params;
};

} /* namespace hwl */

#endif /* SRC_URACTIONSERVER_URACTION_HPP_ */
