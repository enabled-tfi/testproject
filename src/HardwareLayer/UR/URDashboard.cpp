/*
 * URDashboard.cpp
 *
 *  Created on: Mar 28, 2019
 *      Author: thomas
 */

#include <HardwareLayer/UR/URDashboard.hpp>
#include <rw/rw.hpp>

using boost::asio::ip::tcp;

namespace hwl {

URDashboard::URDashboard(std::string ip) {
	_ip = ip;
	_connected = false;
}

URDashboard::~URDashboard() {
}

bool URDashboard::connect(){
	try {
		boost::asio::ip::tcp::resolver resolver(_ioService);
		boost::asio::ip::tcp::resolver::query query(_ip.c_str(), "");
		boost::asio::ip::tcp::resolver::iterator iter = resolver.resolve(query);
		boost::asio::deadline_timer deadline_(_ioService);
		boost::posix_time::time_duration timeout = boost::posix_time::seconds(2);
		deadline_.expires_from_now(timeout);
		boost::asio::ip::tcp::endpoint ep = (*iter).endpoint();

		ep.port(29999);

		//Connecting to server
		_socket = new boost::asio::ip::tcp::socket(_ioService);
		_socket->connect(ep);

	} catch(boost::system::system_error& e) {
		return false;
	}

	std::string rec = receiveData();
	_connected = true;
	return true;
}

bool URDashboard::sendCommand(std::string cmd){
	if(!_connected)
		RW_THROW("Not connected to URDashboard");

	cmd = cmd + "\n";
    std::size_t bytesTransfered = 0;
    bytesTransfered = boost::asio::write(*_socket, boost::asio::buffer(cmd.c_str(), cmd.size()));

    if (cmd.size() == bytesTransfered) {
        return true;
    } else {
        return false;
    }
}

std::string URDashboard::receiveData(){
	std::size_t cnt = _socket->available();

    int readBytes;
    const int BUFFER_SIZE = 128;
    char charBuf[BUFFER_SIZE];
    do
    {
        readBytes = _socket->read_some(boost::asio::buffer(charBuf, BUFFER_SIZE));
    }
    while(readBytes >= BUFFER_SIZE);

    return charBuf;
}

bool URDashboard::running(){
	std::string str = "running";
	sendCommand(str);

	std::string rec = receiveData();

		// Find place after "Program Running: "
	std::string ans = rec.substr(rec.find(":") + 2, 5);

	if(ans.compare("true") == 1)
		return true;
	return false;
}


bool URDashboard::play(){
	std::string str = "play";
	sendCommand(str);

	std::string rec = receiveData();

	if(rec.substr(0,16).compare("Starting program") == 0)
		return true;
	return false;
}

bool URDashboard::stop(){
	std::string str = "stop";
	sendCommand(str);

	std::string rec = receiveData();
	if(rec.substr(0,7).compare("Stopped") == 0)
		return true;
	return false;
}

bool URDashboard::pause(){
	std::string str = "pause";
	sendCommand(str);

	std::string rec = receiveData();

	if(rec.substr(0,15).compare("Pausing program") == 0)
		return true;
	return false;
}

bool URDashboard::load(std::string prg){
	std::string str = "load " + prg;
	sendCommand(str);

	std::string rec = receiveData();

	if(rec.substr(0,15).compare("Loading program") == 0)
		return true;
	return false;
}

bool URDashboard::quit(){
	std::string str = "quit";
	sendCommand(str);

	std::string rec = receiveData();

	if(rec.substr(0,12).compare("Disconnected") == 0)
		return true;
	return false;
}

bool URDashboard::shutdown(){
	std::string str = "shutdown";
	sendCommand(str);

	std::string rec = receiveData();

	if(rec.substr(0,13).compare("Shutting down") == 0)
		return true;
	return false;
}

robotMode URDashboard::robotmode(){
	std::string str = "robotmode";
	sendCommand(str);

	std::string rec = receiveData();

	int start = rec.find(":") + 2;
	int end = rec.find("\n");
	std::string mode = rec.substr(start, (end-start));

	return xmap.at(mode);
}

std::string URDashboard::getLoadedProgram(){
	std::string str = "get loaded program";
	sendCommand(str);

	std::string rec = receiveData();

	int start = rec.find(":") + 2;
	int end = rec.find("\n");
	std::string program = rec.substr(start, (end-start));

	return program;
}

bool URDashboard::popup(std::string text){
	std::string str = "popup " + text;
	sendCommand(str);

	std::string rec = receiveData();

	if(rec.substr(0,13).compare("showing popup") == 0)
		return true;
	return false;
}

bool URDashboard::closePopup(){
	std::string str = "close popup";
	sendCommand(str);

	std::string rec = receiveData();

	if(rec.substr(0,13).compare("closing popup") == 0)
		return true;
	return false;
}

bool URDashboard::addToLog(std::string text){
	std::string str = "addToLog " + text;
	sendCommand(str);

	std::string rec = receiveData();

	if(rec.substr(0,17).compare("Added log message") == 0)
		return true;
	return false;
}

bool URDashboard::isProgramSaved(){
	std::string str = "isProgramSaved";
	sendCommand(str);

	std::string rec = receiveData();

	if(rec.substr(0,4).compare("true") == 0)
		return true;
	return false;
}

eprogramState URDashboard::getProgramState(){
	std::string str = "programState";
	sendCommand(str);

	std::string rec = receiveData();

	return prgStateMap.at(rec.substr(0,6));
}

std::string URDashboard::PolyscopeVersion(){
	std::string str = "PolyscopeVersion";
	sendCommand(str);

	std::string rec = receiveData();

	return rec;
}

bool URDashboard::setOperationalMode(std::string mode){
	std::string str = "set operational mode " + mode;
	sendCommand(str);

	std::string rec = receiveData();
	if(rec.substr(0,16).compare("Operational mode") == 0)
		return true;
	return false;
}

bool URDashboard::clearOperationalMode(){
	std::string str = "clear operational mode";
	sendCommand(str);

	receiveData();

	return true;
}

bool URDashboard::powerOn(){
	std::string str = "power on";
	sendCommand(str);

	receiveData();

	return true;
}

bool URDashboard::powerOff(){
	std::string str = "power off";
	sendCommand(str);

	receiveData();

	return true;
}

bool URDashboard::brakeRelease(){
	std::string str = "brake release";
	sendCommand(str);

	receiveData();

	return true;
}

safetyMode URDashboard::safetymode(){
	std::string str = "safetymode";
	sendCommand(str);

	std::string rec = receiveData();

	int start = rec.find(":") + 2;
	int end = rec.find("\n");
	std::string mode = rec.substr(start, (end-start));

	return safetyModeMap.at(mode);
}

bool URDashboard::unlockProtectiveStop(){
	std::string str = "unlock protective stop";
	sendCommand(str);

	receiveData();

	return true;
}

bool URDashboard::closeSafetyPopup(){
	std::string str = "close safety popup";
	sendCommand(str);

	receiveData();

	return true;
}

bool URDashboard::restartSafety(){
	std::string str = "restart safety";
	sendCommand(str);

	receiveData();

	return true;
}



} /* namespace hwl */
