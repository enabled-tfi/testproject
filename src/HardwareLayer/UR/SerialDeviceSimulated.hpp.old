/* */
#ifndef HWL_SERIALDEVICESIMULATED_HPP
#define HWL_SERIALDEVICESIMULATED_HPP

#include "../SerialDevice.hpp"
#include "../System.hpp"

#include <rw/common/Ptr.hpp>
#include <rw/math.hpp>
#include <rw/kinematics.hpp>
#include <rw/models.hpp>
#include <rw/invkin/JacobianIKSolver.hpp>

namespace hwl {
	
/**
 * Simulated Serial Device
 */
class SerialDeviceSimulated: public SerialDevice
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<SerialDeviceSimulated> Ptr;

	/**
	 * @brief Construct SerialDeviceSimulated
	 */
	SerialDeviceSimulated(rw::models::Device::Ptr device, const System::Ptr system, const std::string& identifier);

	/**
	 * @copydoc SerialDevice::ptp
	 */
	virtual void ptp(const rw::math::Q& qtarget);

	/**
	 * @copydoc SerialDevice::lin
	 */
	virtual void lin(const rw::math::Transform3D<>& target);

	/**
	 * @copydoc SerialDevice::getQ
	 */
	virtual rw::math::Q getQ() const;

	virtual std::pair<rw::math::Q, rw::math::Q> getBounds() const;

private:
	rw::models::Device::Ptr _device;
	System::Ptr _system;
	rw::invkin::JacobianIKSolver _iksolver;
    rw::math::QMetric::Ptr _timeMetric;
//	std::vector<boost::function<void(const rw::kinematics::State&)> > _callbacks;

    double _dt;
    double _velLinear;
    double _velRotational;



};

} //end namespace

#endif //#ifndef HWL_SERIALDEVICESIMULATED_HPP
