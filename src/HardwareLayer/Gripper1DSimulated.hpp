/* */
#ifndef HWL_GRIPPER1DSIMULATED_HPP
#define HWL_GRIPPER1DSIMULATED_HPP

#include "Gripper1D.hpp"
#include "SimulatedEntity.hpp"
#include <rw/models/Device.hpp>
#include <rw/common/Ptr.hpp>

namespace hwl {

/** 
 * Simulated Gripper1D using RobWork Device as the model
 */
class Gripper1DSimulated: public Gripper1D, public SimulatedEntity
{
public:
	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<Gripper1DSimulated> Ptr;

    /**
     * @brief Construct empty ActuatorSimulated. Meant to be used when reading in fra Archives.
     */
    Gripper1DSimulated();

    /**
     * @brief Constructs ActuatorSimulated using \b device as the underlying model.
     * @param device [in] Device to use as model
     * @param id [in] Identifier for the entity
     */
    Gripper1DSimulated(rw::models::Device::Ptr device, const std::string& id);

    /**
     * @copydoc SimulatedEntity::intializeSimulation
     */
    void initializeSimulation(rws::RobWorkStudio* rws);

    /**
     * @copydoc HWEntity::connect
     */
    virtual void connect() {}
    
    /**
     * @copydoc HWEntity::disconnect
     */
    virtual void disconnect() {}

    /**
     * @copydoc Gripper1D::home
     */
    virtual void home();

    /**
     * @copydoc Gripper1D::setGraspPower
     */
    virtual void setGraspPower(double pct);

    /**
     * @copydoc Gripper1D::open
     */
    virtual void open();

    /**
     * @copydoc Gripper1D::close
     */
    virtual void close();

    /**
     * @copydoc Gripper1D::setPosition
     */
    virtual void setPosition(double pos);
    
    /**
     * @copydoc Gripper1D::getPosition
     */
    virtual double getPosition() const;

    /**
     * @copydoc Gripper1D::getBounds
     */
    virtual std::pair<double, double> getBounds() const;
    

    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;



};

} //end namespace

#endif //#ifndef HWL_GRIPPER1DSIMULATED_HPP
