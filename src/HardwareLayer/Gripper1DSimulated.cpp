#include "Gripper1DSimulated.hpp"

using namespace hwl;
using namespace er_common;
using namespace rw::math;
using namespace rw::kinematics;

Gripper1DSimulated::Gripper1DSimulated():
    Gripper1D("Gripper1DSimulated")
{

}


Gripper1DSimulated::Gripper1DSimulated(rw::models::Device::Ptr device, const std::string& id):
    Gripper1D(id, device->getName())
{

}

void Gripper1DSimulated::initializeSimulation(rws::RobWorkStudio* rws)
{
    
}


void Gripper1DSimulated::home()
{
    State state = System::getInstance()->getState();
    Q q(1);
    std::pair<double, double> bounds = getBounds();
    if (bounds.first > 0) {
        q(0) = bounds.first;
    }
    else if (bounds.second < 0) {
        q(0) = bounds.second;
    }
    getDeviceModel()->setQ(q, state);
    System::getInstance()->setState(state);
}

void Gripper1DSimulated::setGraspPower(double pct)
{

}

void Gripper1DSimulated::open()
{
    setPosition(getBounds().second);
}

void Gripper1DSimulated::close()
{
    setPosition(getBounds().first);
}

void Gripper1DSimulated::setPosition(double pos)
{
    State state = System::getInstance()->getState();
    Q q(1);
    q(0) = pos;
    getDeviceModel()->setQ(q, state);
    System::getInstance()->setState(state);
}

double Gripper1DSimulated::getPosition() const
{
    State state = System::getInstance()->getState();
    Q q = getDeviceModel()->getQ(state);
    return q(0);
}

/**
* @brief Returns the lower and upper limit of the actuator.
*/
std::pair<double, double> Gripper1DSimulated::getBounds() const
{
    std::pair<Q, Q> bds = getDeviceModel()->getBounds();
    return std::make_pair(bds.first(0), bds.second(0));
}



/* Methods from the serializable interface */
void Gripper1DSimulated::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    iarchive.readEnterScope("Gripper1DSimulated");
    Gripper1D::read(iarchive, id);
    iarchive.readLeaveScope("Gripper1DSimulated");

}

void Gripper1DSimulated::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("Gripper1DSimulated");
    Gripper1D::write(oarchive, id);
    oarchive.writeLeaveScope("Gripper1DSimulated");
}