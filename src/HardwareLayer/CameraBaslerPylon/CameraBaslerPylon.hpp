/* */
#ifndef HWL_CAMERABASLERPYLON_HPP
#define HWL_CAMERABASLERPYLON_HPP

#include <Common/Entity.hpp>
#include <rw/math/Vector3D.hpp>
#include <HardwareLayer/Camera.hpp>
//#include <rw/sensor/Camera.hpp>
#include <vector>
#include <boost/thread/mutex.hpp>

#include <opencv2/opencv.hpp>



namespace Pylon
{
	class CInstantCamera;
}

namespace hwl {



/** 
 * Implementation of the Camera interface using the Basler Pylon SDK
 */
class CameraBaslerPylon : public Camera
{
public:
	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<CameraBaslerPylon> Ptr;

    /** 
     * @brief Default constructor
     */
	CameraBaslerPylon();

    /**
    * @brief Constructor
    * @param identifier [in] Identifier for the entity
    * @param calibrationfile [in] Filename for the calibration to use with the camera. Assumes an OpenCV compatible calibration file
    */
    CameraBaslerPylon(const std::string& identifier, const std::string& calibrationfile);

    /**
     * @brief Destructor
     */
	~CameraBaslerPylon();

    /**
     * @copydoc HWEntity::connect
     */
	virtual void connect();

    /**
     * @copydoc HWEntity::disconnet
     */
    virtual void disconnect();

    /**
     * @copydoc Camera::acquire
     */
	virtual void acquire();

    /**
     * @copydoc Camera::getLastImage
     */
    virtual cv::Mat getLastImage();

	/* Methods from the serializable interface */
    /**
     * @copydoc Serializable::read
     */
	virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    /**
     * @copydoc Serializable::write
     */
    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


private:
	rw::common::Ptr<Pylon::CInstantCamera> _camera;	
	cv::Mat _frame;
	int _exposureTime;
	boost::mutex _mutexAcquire;
	boost::mutex _mutexFrame;
};

} //end namespace

#endif //#ifndef HWL_CAMERABASLERPYLON_HPP
