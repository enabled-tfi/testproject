#include "CameraBaslerPylon.hpp"

#include <opencv2/highgui.hpp>

#include <rw/common/Ptr.hpp>

#include <pylon/PylonIncludes.h>
#include <pylon/usb/BaslerUsbInstantCamera.h>
#include <pylon/TypeMappings.h>
#ifdef PYLON_WIN_BUILD
#    include <pylon/PylonGUI.h>
#endif



using namespace rw::common;

using namespace er_common;
using namespace hwl;
using namespace Pylon;
using namespace GenApi;

CameraBaslerPylon::CameraBaslerPylon() :
	Camera("CameraBaslerPylon"),
	_exposureTime(-1)
{

}

CameraBaslerPylon::CameraBaslerPylon(const std::string& identifier, const std::string& calibrationfile):
	Camera(identifier, calibrationfile),
	_exposureTime(-1)
{

}

CameraBaslerPylon::~CameraBaslerPylon()
{
	disconnect();
}


void CameraBaslerPylon::connect()
{
	ER_LOG_DEBUG("CameraBaslerPylon::connect");
	try {
		PylonInitialize();
		rw::common::Ptr<CBaslerUsbInstantCamera> cam = ownedPtr(new CBaslerUsbInstantCamera(CTlFactory::GetInstance().CreateFirstDevice()));
		cam->RegisterConfiguration(new CAcquireSingleFrameConfiguration, RegistrationMode_ReplaceAll, Cleanup_Delete);
		cam->Open();
		if(!cam->IsOpen())
			RW_THROW("BaslerCamera not opened");


		if (_exposureTime < 0) {
			if (IsAvailable(cam->AutoFunctionROISelector))
			{				
				// Set the Auto Function ROI for luminance statistics.
				// We want to use ROI1 for gathering the statistics.
				if (IsWritable(cam->AutoFunctionROIUseBrightness))
				{
					cam->AutoFunctionROISelector.SetValue(Basler_UsbCameraParams::AutoFunctionROISelector_ROI1);
					cam->AutoFunctionROIUseBrightness.SetValue(true);   // ROI 1 is used for brightness control
					cam->AutoFunctionROISelector.SetValue(Basler_UsbCameraParams::AutoFunctionROISelector_ROI2);
					cam->AutoFunctionROIUseBrightness.SetValue(false);   // ROI 2 is not used for brightness control
				}
				// Set the ROI (in this example the complete sensor is used)
				cam->AutoFunctionROISelector.SetValue(Basler_UsbCameraParams::AutoFunctionROISelector_ROI1);  // configure ROI 1
				cam->AutoFunctionROIOffsetX.SetValue(0);
				cam->AutoFunctionROIOffsetY.SetValue(0);
				cam->AutoFunctionROIWidth.SetValue(cam->Width.GetMax());
				cam->AutoFunctionROIHeight.SetValue(cam->Height.GetMax());
			}
			// Set the target value for luminance control.
			// A value of 0.3 means that the target brightness is 30 % of the maximum brightness of the raw pixel value read out from the sensor.
			// A value of 0.4 means 40 % and so forth.
			cam->AutoTargetBrightness.SetValue(0.3);

			//ER_LOG_DEBUG("ExposureAuto 'GainAuto = Continuous'." );
			//ER_LOG_DEBUG("Initial exposure time = ";
			//ER_LOG_DEBUG(cam->ExposureTime.GetValue() << " us" );

			cam->ExposureAuto.SetValue(Basler_UsbCameraParams::ExposureAuto_Continuous);
			cam->BalanceWhiteAuto.SetValue(Basler_UsbCameraParams::BalanceWhiteAuto_Continuous);
		}
		_camera = cam;// ownedPtr(new CInstantCamera(CTlFactory::GetInstance().CreateFirstDevice()));
		_camera->MaxNumBuffer = 5;

	}
	catch (const GenericException& exp) {
		ER_LOG_ERROR("Caught generic exception " << exp.what() );
//		RW_THROW("CameraBaslerPylon Connect: " << exp.what());
	}



}

void CameraBaslerPylon::disconnect()
{
	if (_camera != NULL)
		_camera->Close();
	_camera = NULL;
}


int imageCnt = 0;
void CameraBaslerPylon::acquire()
{
//	ER_LOG_DEBUG("CameraBaslerPylon::acquire" );
	if (_camera == NULL)
		RW_THROW("Camera not connected");

	if(!_camera->IsOpen())
		RW_THROW("Camera not connected");

	try {
		_mutexAcquire.lock();
		CGrabResultPtr ptrGrabResult;
		_camera->GrabOne(5000, ptrGrabResult);
		//_camera->RetrieveResult(5000, ptrGrabResult, TimeoutHandling_ThrowException);
		_mutexAcquire.unlock();

		// Image grabbed successfully?
		if (ptrGrabResult->GrabSucceeded())
		{
			imageCnt++;
			// Access the image data.
			const uint8_t *pImageBuffer = (uint8_t *)ptrGrabResult->GetBuffer();

//#ifdef PYLON_WIN_BUILD
//			// Display the grabbed image.
//			Pylon::DisplayImage(1, ptrGrabResult);
//#endif
			CPylonImage pylonImage;
			CImageFormatConverter formatConverter;
			formatConverter.OutputPixelFormat = PixelType_BGR8packed;
			formatConverter.Convert(pylonImage, ptrGrabResult);
			_mutexFrame.lock();
			_frame = cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(), CV_8UC3, (uint8_t*)pylonImage.GetBuffer()).clone();
			_mutexFrame.unlock();
		}
		else
		{
			ER_LOG_ERROR("Error: " << ptrGrabResult->GetErrorCode() << " " << ptrGrabResult->GetErrorDescription() );
		}
	}
	catch (const TimeoutException& exp) {
		ER_LOG_ERROR("Caught timeout exception " << exp.what());
	}
	catch (const GenericException& exp) {
		RW_LOG_ERROR("Caught generic exception " << exp.what() << " after "<<imageCnt<<" images");
		RW_LOG_ERROR("Tries to reconnect ");
		connect();
		acquire();
	}
	catch (const std::exception& exp) {
		ER_LOG_ERROR("Exception: " << exp.what() );
		throw exp;
	}
}

cv::Mat CameraBaslerPylon::getLastImage()
{
	_mutexFrame.lock();

	if (_frame.empty())
		RW_THROW("No image to return. Did you remember to call acquire first?");

	cv::imwrite("PylonToOpenCVBeingReturned.png", _frame);

	cv::Mat clonedFrame = _frame.clone();

	_mutexFrame.unlock();

	return clonedFrame;
}


/* Methods from the serializable interface */
void CameraBaslerPylon::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
	iarchive.readEnterScope("CameraBaslerPylon");
	Camera::read(iarchive, id);
	if (iarchive.has("ExposureTime")) {
		_exposureTime = iarchive.read<int>("ExposureTime");
    }
    else {
        _exposureTime = 10;
        RW_WARN("No exposure time found for CameraBaslerPylon. Sets default to 10");
    }
	iarchive.readLeaveScope("CameraBaslerPylon");
}

void CameraBaslerPylon::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	oarchive.writeEnterScope("CameraBaslerPylon");
	Camera::write(oarchive, id);
	oarchive.write(_exposureTime, "ExposureTime");
	oarchive.writeLeaveScope("CameraBaslerPylon");
}
