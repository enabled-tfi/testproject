/* */
#ifndef HWL_SENSOR25D_HPP
#define HWL_SENSOR25D_HPP

#include <rw/math/Vector3D.hpp>

#include <vector>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>


namespace hwl {

/** 
 * Interface for a 2.5D sensor (such as Kinect)
 */
class Sensor25D
{
public:
	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<Sensor25D> Ptr;

    /**
     * @brief Connect to the device
     */
	virtual void connect() = 0;

	/**
	 * @brief Copies the current point cloud into the \bcloud argument
	 * @param cloud [out] PointCloud to copy the current values into.
	 */
	virtual void getPointCloud(pcl::PointCloud<pcl::PointXYZRGB>& cloud) = 0;
    

private:
    
};

} //end namespace

#endif //#ifndef HWL_SENSOR25D_HPP
