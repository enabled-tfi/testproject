#include "IOInterface.hpp"

using namespace er_common;
using namespace hwl;

IOInterface::IOInterface(const std::string& identifier, bool isChild):
	HWEntity(identifier, isChild)
{}

IOInterface::IOInterface(const std::string& identifier, std::pair<int, int> digitalInRange, std::pair<int, int> digitalOutRange, std::pair<int, int> analogInRange, std::pair<int, int> analogOutRange, bool isChild):
	HWEntity(identifier, isChild)
{}


std::pair<int, int> IOInterface::getDigitalInRange()
{
	return _digitalInRange;
}

void IOInterface::setDigitalInRange(int startIdx, int endIdx)
{
	_digitalInRange.first = startIdx;
	_digitalInRange.second = endIdx;
}

std::pair<int, int> IOInterface::getDigitalOutRange()
{
	return _digitalOutRange;
}

void IOInterface::setDigitalOutRange(int startIdx, int endIdx) 
{
	_digitalOutRange.first = startIdx;
	_digitalOutRange.second = endIdx;
}

std::pair<int, int> IOInterface::getAnalogInRange()
{
	return _analogInRange;
}

void IOInterface::setAnalogInRange(int startIdx, int endIdx)
{
	_analogInRange.first = startIdx;
	_analogInRange.second = endIdx;
}

std::pair<int, int> IOInterface::getAnalogOutRange()
{
	return _analogOutRange;
}

void IOInterface::setAnalogOutRange(int startIdx, int endIdx)
{
	_analogOutRange.first = startIdx;
	_analogOutRange.second = endIdx;
}

void IOInterface::checkRange(int idx, const std::pair<int, int>& range, const std::string& type) 
{
	if (idx < range.first || idx > range.second)
		RW_THROW("Index '" << idx << "' for " << type << " is not within range [" << range.first << ";" << range.second << "]");
}

bool IOInterface::getDigitalIn(int idx)
{
	checkRange(idx, _digitalInRange, "DigitalIn");
	return doGetDigitalIn(idx);
}

void IOInterface::setDigitalOut(int idx, bool val)
{
	checkRange(idx, _digitalOutRange, "DigitalOut");
	doSetDigitalOut(idx, val);
}

bool IOInterface::getDigitalOut(int idx)
{
	checkRange(idx, _digitalOutRange, "DigitalOut");
	return doGetDigitalOut(idx);
}


double IOInterface::getAnalogIn(int idx)
{
	checkRange(idx, _analogInRange, "AnalogIn");
	return doGetAnalogIn(idx);
}

void IOInterface::setAnalogOut(int idx, double val)
{
	checkRange(idx, _analogOutRange, "AnalogOut");
	doSetAnalogOut(idx, val);
}

double IOInterface::getAnalogOut(int idx)
{
	checkRange(idx, _analogOutRange, "AnalogOut");
	return doGetAnalogOut(idx);
}
