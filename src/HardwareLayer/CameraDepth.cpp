#include "CameraDepth.hpp"

#include <opencv2/highgui.hpp>

using namespace er_common;
using namespace hwl;
using namespace rw::common;

CameraDepth::CameraDepth(const std::string& identifier):
	HWEntity(identifier)
{

}


void CameraDepth::addUpdateRule(er_common::System::Ptr system)
{
    system->addUpdateRule(ownedPtr(new hwl::CameraDepth::CameraDepthUpdateRule(this)));
}

pcl::PointCloud<pcl::PointXYZ> CameraDepth::getLastPointCloud()
{
    pcl::PointCloud<pcl::PointXYZ> cloud;
    getLastPointCloud(cloud);
    return cloud;
}


void CameraDepth::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    HWEntity::read(iarchive, "");
}

void CameraDepth::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    HWEntity::write(oarchive, "");
}
