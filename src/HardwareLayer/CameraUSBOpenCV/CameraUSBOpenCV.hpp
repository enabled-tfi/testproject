/* */
#ifndef HWL_CAMERAUSBOPENCV_HPP
#define HWL_CAMERAUSBOPENCV_HPP

#include <Common/Entity.hpp>
#include <rw/math/Vector3D.hpp>
#include <HardwareLayer/Camera.hpp>
//#include <rw/sensor/Camera.hpp>
#include <vector>

#include <opencv2/opencv.hpp>


namespace hwl {

    /**
    * Implementation of the Camera interface using OpenCV's USB cam interface
    */
class CameraUSBOpenCV : public Camera
{
public:
	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<CameraUSBOpenCV> Ptr;

    /** 
     * @brief Default constructor
     */
	CameraUSBOpenCV();

    /**
     * @brief Constructor
     * @param identifier [in] Identifier for the entity
     * @param camid [in] Id for the camera
     * @param calibrationfile [in] Filename for the calibration to use with the camera. Assumes an OpenCV compatible calibration file
     */
	CameraUSBOpenCV(const std::string& identifier, int camid, const std::string& calibrationfile);

    /**
     * @brief Destructor
     */
	~CameraUSBOpenCV();

    /**
     * @copydoc HWEntity::connect
     */
    virtual void connect();

    /**
     * @copydoc HWEntity::disconnect
     */
    virtual void disconnect();

    /**
     * @copydoc Camera::acquire
     */
	virtual void acquire();

    /**
     * @copydoc Camera::getLastImage
     */
    virtual cv::Mat getLastImage();

	/* Methods from the serializable interface */
    /**
     * @copydoc Serializable::read
     */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    /**
     * @copydoc Serializable::write
     */
	virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


private:
	int _camid;
	cv::VideoCapture _videoCapture;
	cv::Mat _frame;
	boost::mutex _mutexFrame;

};

} //end namespace

#endif //#ifndef HWL_CAMERAUSBOPENCV_HPP
