#include "CameraUSBOpenCV.hpp"

#include <opencv2/highgui.hpp>

using namespace er_common;
using namespace hwl;

CameraUSBOpenCV::CameraUSBOpenCV() :
	Camera("CameraUSBOpenCV")
{

}

CameraUSBOpenCV::CameraUSBOpenCV(const std::string& identifier, int camid, const std::string& calibrationfile):
	Camera(identifier, calibrationfile), 
	_camid(camid)
{

}

CameraUSBOpenCV::~CameraUSBOpenCV()
{
	disconnect();
}

void CameraUSBOpenCV::connect()
{	
	_videoCapture.open(_camid);
	_videoCapture.set(cv::CAP_PROP_FRAME_WIDTH, 1920); // valueX = your wanted width 
	_videoCapture.set(cv::CAP_PROP_FRAME_HEIGHT, 1080); // valueY = your wanted heigth
}

void CameraUSBOpenCV::disconnect() 
{
	_videoCapture.release();
}

void CameraUSBOpenCV::acquire()
{
	_mutexFrame.lock();
	_videoCapture >> _frame;
	_mutexFrame.unlock();
}

cv::Mat CameraUSBOpenCV::getLastImage()
{
	_mutexFrame.lock();
	if (_frame.empty())
		RW_THROW("No image to return. Did you remember to call acquire first?");
	cv::Mat frameClone = _frame.clone();
	_mutexFrame.unlock();

	return frameClone;


	//int tries = 0;
	//do {
	//	cv::Mat frame;
	//	_videoCapture >> frame;
	//	if (frame.empty()) {
	//		tries++;
	//		continue; // Try agin
	//	}
	//	cv::imshow("Acquired Image", frame);
	//	return frame;
	//} while (tries < 10);
	//RW_THROW("Unabled to acquire image");
}


/* Methods from the serializable interface */
void CameraUSBOpenCV::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
	iarchive.readEnterScope("CameraUSBOpenCV");
	Camera::read(iarchive, id);
	_camid = iarchive.read<int>("CameraId");

	iarchive.readLeaveScope("CameraUSBOpenCV");
}

void CameraUSBOpenCV::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	oarchive.writeEnterScope("CameraUSBOpenCV");
	Camera::write(oarchive, id);
	oarchive.write(_camid, "CameraId");
	oarchive.writeLeaveScope("CameraUSBOpenCV");
}
