#include "Manipulator.hpp"


using namespace hwl;
using namespace rw::common;

Manipulator::Manipulator(const std::string& identifier):
    HWEntity(identifier)
{
	ER_LOG_DEBUG("Manipulator default constructor " );
}


void Manipulator::addUpdateRule(er_common::System::Ptr system)
{
	_updateRule = new Manipulator::ManipulatorUpdateRule(system->getManipulator(), this);
	system->addUpdateRule(_updateRule);
}

Manipulator::ManipulatorUpdateRule::Ptr Manipulator::getManipulatorUpdateRule(){
	return _updateRule;
}

