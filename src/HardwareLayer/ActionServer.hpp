/*
 * EventServer.hpp
 *
 *  Created on: May 2, 2019
 *      Author: thomas
 */

#ifndef SRC_HARDWARELAYER_ACTIONSERVER_HPP_
#define SRC_HARDWARELAYER_ACTIONSERVER_HPP_

#include <ExecutionModel/Instruction.hpp>
#include <Common/SWEntity.hpp>
#include <HardwareLayer/Action.hpp>

using namespace exm;
namespace hwl {

class ActionServer : public er_common::SWEntity {
public:
	ActionServer();
	virtual ~ActionServer();

	void addAction(Action::Ptr action);

protected:
	std::queue<Action::Ptr> _actionsToExecute;

};

} /* namespace hwl */

#endif /* SRC_HARDWARELAYER_ACTIONSERVER_HPP_ */
