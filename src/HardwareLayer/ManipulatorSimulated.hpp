/* */
#ifndef HWL_MANIPULATORSIMULATED_HPP
#define HWL_MANIPULATORSIMULATED_HPP

#include "Manipulator.hpp"
#include "SimulatedEntity.hpp"
#include "../Common/System.hpp"

#include <rw/common/Ptr.hpp>
#include <rw/math.hpp>
#include <rw/kinematics.hpp>
#include <rw/models.hpp>
#include <rw/invkin/JacobianIKSolver.hpp>

namespace hwl {
	
/**
 * Simulated Manipulator
 */
class ManipulatorSimulated: public Manipulator, public SimulatedEntity
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<ManipulatorSimulated> Ptr;

	ManipulatorSimulated();

	/**
	 * @brief Construct ManipulatorSimulated
	 */
	ManipulatorSimulated(rw::models::Device::Ptr device, er_common::System::Ptr system, const std::string& id);


	void initializeSimulation(rws::RobWorkStudio* rws);
	/**
	 * @copydoc Manipulator::ptp
	 */
	virtual void ptp(const rw::math::Q& qtarget, double speed = 0);

	/**
	 * @copydoc Manipulator::ptp
	 */
	virtual void ptp(const rw::math::Transform3D<>& target, double speed = 0);

	/**
	 * @copydoc Manipulator::lin
	 */
	virtual void lin(const rw::math::Transform3D<>& target, double speed = 0);

	/** 
	 * @copydoc Manipulator::getQ
	 */
	virtual rw::math::Q getQ() const;

	virtual void stop();

	/**
	* @copydoc Manipulator::getBounds
	*/
	virtual std::pair<rw::math::Q, rw::math::Q> getBounds() const;


	/**
	* @copydoc Manipulator::setEndToTCP
	*/
	virtual void setEndToTCP(const rw::math::Transform3D<>& endTtcp);

	/**
	 * @copydoc Manipulator::getEndToTCP
	 */
	virtual rw::math::Transform3D<> getEndToTCP() const;

	virtual rw::math::Transform3D<> getBaseToTCP() const;

	virtual void startTeachMode();
	virtual void endTeachMode();

	/**
	* @copydoc HWEntity::connect
	*/
	virtual void connect() {}

	/**
	* @copydoc HWEntity::disconnect
	*/
	virtual void disconnect() {};

	/**
	* @copydoc HWEntity::reconnect
	*/
	virtual void reconnect() {};


	/* Methods from the serializable interface */
	virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

	virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


private:
	rw::models::Device::Ptr _device;
	er_common::System::Ptr _system;
	rw::invkin::JacobianIKSolver::Ptr _iksolver;
    rw::math::QMetric::Ptr _timeMetric;
	rw::math::Transform3D<> _endTtcp;
//	std::vector<boost::function<void(const rw::kinematics::State&)> > _callbacks;

    double _dt;
    double _velLinear;
    double _velRotational;



};

} //end namespace

#endif //#ifndef HWL_MANIPULATORSIMULATED_HPP
