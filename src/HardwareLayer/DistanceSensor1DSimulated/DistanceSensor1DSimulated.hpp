﻿/* */
#ifndef HWL_DISTANCESENSOR1DDSIMULATED_HPP
#define HWL_DISTANCESENSOR1DDSIMULATED_HPP

#include <Common/Entity.hpp>
#include <Common/System.hpp>
#include <HardwareLayer/DistanceSensor1D.hpp>
#include <HardwareLayer/SimulatedEntity.hpp>
#include <rw/math/Vector3D.hpp>
//#include <rwlibs/simulation/SimulatedCamera.hpp>
#include <rwlibs/simulation/GLFrameGrabber25D.hpp>
#include <rw/graphics/SceneViewer.hpp>
#include <rws/RobWorkStudio.hpp>
#include <vector>
#include <thread>
#include <opencv2/opencv.hpp>
#include <boost/thread.hpp>
namespace hwl {

/** 
 * Simulated 1D distance sensor implementing the hwl::DistanceSensor1D interface
 */
class DistanceSensor1DSimulated : public DistanceSensor1D, public SimulatedEntity
{
public:
	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<DistanceSensor1DSimulated> Ptr;

    /** 
     * @brief Default constructor
     */
    DistanceSensor1DSimulated();

    /**
     * @brief Constructor
     * @param identifier [in] Identifier for the entity
     * @param scannerFrame [in] Pointer to the frame holding the scanner.
     * @param rwstudio [in] Instance of RobWorkStudio to use for the simulated scanner. Defaults to NULL, but the initializeSimulation should be called.
     */
    DistanceSensor1DSimulated(const std::string& identifier, rw::kinematics::Frame* scannerFrame, rws::RobWorkStudio* rwstudio);

    /**
     * @copydoc SimulatedEntity::initializeSimulation
     */
	void initializeSimulation(rws::RobWorkStudio* rwstudio);




    /**
     * @copydoc HWEntity::connect
     */
	virtual void connect();

    /**
     * @copydoc HWEntity::disconnect
     */
    virtual void disconnect();


    /**
     * @copydoc DistanceSensor1D::sensorOn
     */
    virtual void sensorOn() {};

    /**
     * @copydoc DistanceSensor1D::sensorOff
     */
    virtual void sensorOff() {};

    /**
    * @copydoc DistanceSensor1D::getDistance
    */
    double getDistance();

    /**
     * @copydoc DistanceSensor1D::getRange
     */
    virtual std::pair<double, double> getRange() const;

    /**
     * @copydoc Serializable::read
     */
	virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    /**
     * @copydoc Serializable::write
     */
    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

private:
	rws::RobWorkStudio* _rwstudio;
	rw::graphics::SceneViewer::Ptr _sceneviewer;
	rwlibs::simulation::GLFrameGrabber25D::Ptr _framegrabber;
	std::thread::id _threadId;
	rw::kinematics::Frame* _sensorSimulationFrame;
    double _lastMeasurement;
    rw::math::Vector2D<> _sensingRange;

	bool _isConnected;
	bool _acquireImage;
	bool _imageAcquired;

	rw::common::Ptr<boost::thread> _thread;
	boost::mutex _mutex;

};

} //end namespace

#endif //#ifndef HWL_DISTANCESENSOR1DDSIMULATED_HPP
