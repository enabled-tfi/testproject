#include "DistanceSensor1DSimulated.hpp"
//#include "SimpleGLViewer.hpp"
#include <rw/sensor/Image.hpp>

#include <rwlibs/simulation/SimulatedCamera.hpp>
#include <rws/SceneOpenGLViewer.hpp>
#include <opencv2/highgui.hpp>

//#include "GLXOffscreenViewer.cpp"
using namespace rw::math;
using namespace rw::common;
using namespace rw::kinematics; 
using namespace rw::graphics;
using namespace rws;
using namespace rw::sensor;
using namespace rwlibs::simulation;

using namespace er_common;
using namespace hwl;

#include <thread>



DistanceSensor1DSimulated::DistanceSensor1DSimulated():
    DistanceSensor1D("Scanner2DSimulated")
{

}

DistanceSensor1DSimulated::DistanceSensor1DSimulated(const std::string& identifier, Frame* sensorFrame, rws::RobWorkStudio* rwstudio):
    DistanceSensor1D(identifier),
	_rwstudio(rwstudio)
{
	
}

void DistanceSensor1DSimulated::initializeSimulation(rws::RobWorkStudio* rwstudio)
{
	_rwstudio = rwstudio;
}




void DistanceSensor1DSimulated::connect()
{

	if (_sensorFrame == NULL)
		RW_THROW("Sensor frame is NULL");
	
	_sceneviewer = _rwstudio->getView()->getSceneViewer();// sceneviewer;
	//ER_LOG_DEBUG("Width = " << _cnt << " fov = " << _fovy );
    _framegrabber = ownedPtr(new GLFrameGrabber25D(1, 1, 0.01));
    _framegrabber->init(_sceneviewer);


	_isConnected = true;
	_threadId = std::this_thread::get_id(); 
	//ER_LOG_DEBUG("THREAD ID = " << _threadId );

} 

void DistanceSensor1DSimulated::disconnect() {
	_isConnected = false;
}

std::pair<double, double> DistanceSensor1DSimulated::getRange() const {
    return std::make_pair(_sensingRange(0), _sensingRange(1));
}

double DistanceSensor1DSimulated::getDistance()
{
	//ER_LOG_DEBUG("ThreadId to match = " << _threadId << " Thread id is " << std::this_thread::get_id() );
	bool firstWait = true;
	if (_threadId == std::this_thread::get_id()) {
		boost::mutex::scoped_lock scoped_lock(_mutex);
        //ER_LOG_DEBUG("Scanner Frame " << _scannerFrame );
        _framegrabber->grab(_sensorSimulationFrame, System::getInstance()->getState());
        //ER_LOG_DEBUG("Grab done" );
        std::vector<Vector3D<float> > result = _framegrabber->getImage().getData();
        //ER_LOG_DEBUG("getImage getData" );
        _lastMeasurement = (double)result.front().norm2();
        if (_lastMeasurement < _sensingRange(0)) {
            _lastMeasurement = _sensingRange(0);
        } else if (_lastMeasurement >  _sensingRange(1)) {
            _lastMeasurement = _sensingRange(1);
        } 
		_imageAcquired = true;
	}
	else {
		_imageAcquired = false; 
		while (_imageAcquired == false) {
			if (firstWait) {
				//ER_LOG_DEBUG("Wait for someone to take a picture " );
				firstWait = false;
			}
			TimerUtil::sleepMs(100);
		}
	}
	return _lastMeasurement;
}



/**
* @copydoc Serializable::read
*/
void DistanceSensor1DSimulated::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    iarchive.readEnterScope("DistanceSensor1DSimulated");
    DistanceSensor1D::read(iarchive, id);
    std::string sensorSimulationFrameName = iarchive.read<std::string>("SensorSimulationFrame");
    if (sensorSimulationFrameName != "NULL") {
        _sensorSimulationFrame = System::getInstance()->getWorkCell()->findFrame(sensorSimulationFrameName);
        if (_sensorFrame == NULL) {
            RW_THROW("No frame named " << sensorSimulationFrameName << " found in work cell");
        }
    }
    
    _sensingRange = iarchive.read<Vector2D<> >("SensingRange");
    
    iarchive.readLeaveScope("DistanceSensor1DSimulated");

}

/**
* @copydoc Serializable::write
*/
void DistanceSensor1DSimulated::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("DistanceSensor1DSimulated");
    DistanceSensor1D::write(oarchive, id);
    if (_sensorFrame != NULL)
        oarchive.write(_sensorSimulationFrame->getName(), "SensorSimulationFrame");
    else
        oarchive.write(std::string("NULL"), "SensorSimulationFrame");
        
    oarchive.write(_sensingRange, "SensingRange");
    oarchive.writeLeaveScope("DistanceSensor1DSimulated");
}

 