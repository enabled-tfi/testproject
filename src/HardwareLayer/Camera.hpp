/* */
#ifndef HWL_CAMERA_HPP
#define HWL_CAMERA_HPP

#include <Common/HWEntity.hpp>
#include <Common/System.hpp>
#include <rw/math/Vector3D.hpp>
#include <rw/sensor/Camera.hpp>
#include <vector>

#include <opencv2/opencv.hpp>


namespace hwl {

/** 
 * Interface for a 2D laser sensor (such as a classic LIDAR scanner)
 */
class Camera : public HWEntity
{
public:
	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<Camera> Ptr;

    enum CameraPlacement{Manipulator, MobileBase};

	Camera(const std::string& identifier);

	Camera(const std::string& identifier, const std::string& calibrationfile);

	rw::kinematics::Frame* getSensorFrame();
	void setSensorFrame(rw::kinematics::Frame* sensorFrame);

    /**
     * @brief Connect to the device
     */
	//virtual void connect() = 0;

	//virtual void disconnect() = 0;

	virtual void acquire() = 0;

	virtual cv::Mat getLastImage() = 0;

	virtual std::pair<cv::Mat, cv::Mat> getCameraCalibration();

	void setCalibrationFileName(const std::string& calibrationFileName);
	std::string getCalibrationFileName() const;

	/*
	void setCameraPlacement(const CameraPlacement placement);
	CameraPlacement getCameraPlacement() const;
*/
	class CameraUpdateRule : public er_common::System::UpdateRule
	{
	public:
		CameraUpdateRule(Camera::Ptr camera) :
			er_common::System::UpdateRule(),
			_camera(camera)
		{
		}

		~CameraUpdateRule()
		{
			//ER_LOG_DEBUG("Destroy Camera Windows " );
			//cv::destroyWindow("Acquired Image");
			//ER_LOG_DEBUG("Camera Window destroyed " );
		}

		void update(rw::kinematics::State& state)
		{
			try {
				_camera->acquire();
//                cv::imshow("Acquired Image", _camera->getLastImage());
			} catch (const std::exception& exp) {
				ER_LOG_ERROR("Camera caught exception: " + (std::string)exp.what());
				ER_LOG_DEBUG("Trying to reconnect to camera");
				_camera->disconnect();
				_camera->connect();
			}
			//ER_LOG_DEBUG("Camera Update Rule::Update" );
			//ER_LOG_DEBUG("Camera Update Rule::Update Acquired" );
			//cv::imwrite("PylonToOpenCVToBeDisplayed.png", _camera->getLastImage());
			//cv::imshow("Acquired Image", _camera->getLastImage());
			//ER_LOG_DEBUG("Camera Update Rule::Show called" );
		}

		er_common::Entity::Ptr getEntity() {
			return _camera;
		}


	private:	
		Camera::Ptr _camera;
	};

	void addUpdateRule(er_common::System::Ptr system);

    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

private:
	//cv::VideoCapture _videoCapture;
	std::string _calibrationfile; 
	cv::Mat _cameraMatrix;
	cv::Mat _distCoeffs;
	//CameraPlacement _cameraPlacement;
	rw::kinematics::Frame* _sensorFrame;
};

} //end namespace

#endif //#ifndef HWL_SCANNER2D_HPP 
