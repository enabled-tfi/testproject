#include "DistanceSensor1D.hpp"

using namespace er_common;
using namespace hwl;

DistanceSensor1D::DistanceSensor1D(const std::string& identifier) :
	HWEntity(identifier),
    _sensorFrame(NULL)
{

}


rw::kinematics::Frame* DistanceSensor1D::getSensorFrame()
{
    return _sensorFrame;
}

void DistanceSensor1D::setCalibrationFileName(const std::string& calibrationFileName)
{
	_calibrationFile = calibrationFileName;
}

std::string DistanceSensor1D::getCalibrationFileName() const
{
	return _calibrationFile;
}


/* Methods from the serializable interface */
void DistanceSensor1D::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    HWEntity::read(iarchive, id);

    std::string calibrationFile = "";
    if(iarchive.has("CalibrationFile"))
    	calibrationFile = iarchive.read<std::string>("CalibrationFile");

    _calibrationFile = calibrationFile;

    std::string sensorFrameName = iarchive.read<std::string>("SensorFrame");    
    if (sensorFrameName != "NULL") {
        _sensorFrame = System::getInstance()->getWorkCell()->findFrame(sensorFrameName);
        if (_sensorFrame == NULL) {
            RW_THROW("No frame named " << sensorFrameName << " found in work cell");
        }
    }
}

void DistanceSensor1D::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    HWEntity::write(oarchive, id);

    oarchive.write(_calibrationFile, "CalibrationFile");

    if (_sensorFrame != NULL)
        oarchive.write(_sensorFrame->getName(), "SensorFrame");
    else
        oarchive.write(std::string("NULL"), "SensorFrame");

}
