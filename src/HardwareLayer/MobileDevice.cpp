#include "MobileDevice.hpp"

using namespace hwl;
using namespace rw::common;


MobileDevice::MobileDevice(const std::string& identifier):
    HWEntity(identifier)
{

}

void MobileDevice::addUpdateRule(er_common::System::Ptr system)
{
	system->addUpdateRule(ownedPtr(new MobileDevice::MobileDeviceUpdateRule(system->getMobileBase(), this)));
}