#include "Scanner2DSimulated.hpp"
//#include "SimpleGLViewer.hpp"
#include <rw/sensor/Image.hpp>

#include <rwlibs/simulation/SimulatedCamera.hpp>
#include <rws/SceneOpenGLViewer.hpp>
#include <opencv2/highgui.hpp>

//#include "GLXOffscreenViewer.cpp"
using namespace rw::math;
using namespace rw::common;
using namespace rw::kinematics; 
using namespace rw::graphics;
using namespace rws;
using namespace rw::sensor;
using namespace rwlibs::simulation;

using namespace er_common;
using namespace hwl;

#include <thread>

namespace
{
	const std::string scannerId("Scanner2D");
}

Scanner2DSimulated::Scanner2DSimulated():
	Scanner2D("Scanner2DSimulated")
{

}

Scanner2DSimulated::Scanner2DSimulated(const std::string& identifier, Frame* scannerFrame, rws::RobWorkStudio* rwstudio):
	Scanner2D(identifier),
    _scannerFrame(scannerFrame),
	_rwstudio(rwstudio)
{
	
}

void Scanner2DSimulated::initializeSimulation(rws::RobWorkStudio* rwstudio)
{
	_rwstudio = rwstudio;
}



void Scanner2DSimulated::connect()
{

	if (_scannerFrame == NULL)
		RW_THROW("Camera frame is NULL");

	if (_scannerFrame->getPropertyMap().has(scannerId) == false) {
		RW_THROW("Specified frame does not have scanner properties");
	}
	

	
	_sceneviewer = _rwstudio->getView()->getSceneViewer();// sceneviewer;
	std::string camParam = _scannerFrame->getPropertyMap().get<std::string>(scannerId);
	std::istringstream iss(camParam, std::istringstream::in);
	iss >> _fovy >> _cnt;
	//ER_LOG_DEBUG("Width = " << _cnt << " fov = " << _fovy );
    _framegrabber = ownedPtr(new GLFrameGrabber25D(1, _cnt, _fovy));
    _framegrabber->init(_sceneviewer);

	//_framegrabber = ownedPtr(new GLFrameGrabber25D(1, cnt, _fovy, 0.01, 10.0));
	//ER_LOG_DEBUG("_sceneviewer = " << _sceneviewer );
	//_framegrabber->init(_sceneviewer);
	
	_isConnected = true;
	_threadId = std::this_thread::get_id(); 
	//ER_LOG_DEBUG("THREAD ID = " << _threadId );

} 

void Scanner2DSimulated::disconnect() {
	_isConnected = false;
}

std::vector<rw::math::Vector3D<> > Scanner2DSimulated::getScan()
{
	//ER_LOG_DEBUG("ThreadId to match = " << _threadId << " Thread id is " << std::this_thread::get_id() );
	bool firstWait = true;
	if (_threadId == std::this_thread::get_id()) {
		boost::mutex::scoped_lock scoped_lock(_mutex);
        //ER_LOG_DEBUG("Scanner Frame " << _scannerFrame );
        _framegrabber->grab(_scannerFrame, System::getInstance()->getState());
        //ER_LOG_DEBUG("Grab done" );
        std::vector<Vector3D<float> > result = _framegrabber->getImage().getData();
        //ER_LOG_DEBUG("getImage getData" );
        _lastScan.clear();
        for (Vector3D<float> pf : result) {
            
            const double d = pf.norm2();
            if (d < _sensingRange(0)) {
                _lastScan.push_back(Vector3D<>(0, 0, 0));
            }
            else if (d > _sensingRange(1)) {
                _lastScan.push_back(cast<double>((normalize(pf)*d)));
            }
            else {
                _lastScan.push_back(cast<double>(pf));
            }
        }
		_imageAcquired = true;
	}
	else {
		_imageAcquired = false; 
		while (_imageAcquired == false) {
			if (firstWait) {
				//ER_LOG_DEBUG("Wait for someone to take a picture " );
				firstWait = false;
			}
			TimerUtil::sleepMs(100);
		}
	}
	return _lastScan;
}



/**
* @copydoc Serializable::read
*/
void Scanner2DSimulated::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    iarchive.readEnterScope("Scanner2DSimulated");
    HWEntity::read(iarchive, id);
    std::string scannerFrameName = iarchive.read<std::string>("Scanner2DFrame");
    if (scannerFrameName != "NULL") {
        _scannerFrame = System::getInstance()->getWorkCell()->findFrame(scannerFrameName);
        if (_scannerFrame == NULL) {
            RW_THROW("No frame named " << scannerFrameName << " found in work cell");
        }
    }
    _fovy = iarchive.read<double>("FieldOfView");
    _cnt = iarchive.read<int>("MeasurementCount");
    _sensingRange = iarchive.read<Vector2D<>>("SensingRange");

    iarchive.readLeaveScope("Scanner2DSimulated");

}

/**
* @copydoc Serializable::write
*/
void Scanner2DSimulated::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("Scanner2DSimulated");
    HWEntity::write(oarchive, id);
    if (_scannerFrame != NULL)
        oarchive.write(_scannerFrame->getName(), "Scanner2DFrame");
    else
        oarchive.write(std::string("NULL"), "Scanner2DFrame");

    oarchive.write(_fovy, "FieldOfView");
    oarchive.write(_cnt, "MeasurementCount");
    oarchive.write(_sensingRange, "SensingRange");
    oarchive.writeLeaveScope("Scanner2DSimulated");
}

 