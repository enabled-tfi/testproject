﻿/* */
#ifndef HWL_SCANNER2DSIMULATED_HPP
#define HWL_SCANNER2DSIMULATED_HPP

#include <Common/Entity.hpp>
#include <Common/System.hpp>
#include <HardwareLayer/Scanner2D.hpp>
#include <HardwareLayer/SimulatedEntity.hpp>
#include <rw/math/Vector3D.hpp>
//#include <rwlibs/simulation/SimulatedCamera.hpp>
#include <rwlibs/simulation/GLFrameGrabber25D.hpp>
#include <rw/graphics/SceneViewer.hpp>
#include <rws/RobWorkStudio.hpp>
#include <vector>
#include <thread>
#include <opencv2/opencv.hpp>
#include <boost/thread.hpp>
namespace hwl {

/** 
 * Simulated scanner 2D implementing the hwl::Scanner2D interface
 */
class Scanner2DSimulated : public Scanner2D, public SimulatedEntity
{
public:
	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<Scanner2DSimulated> Ptr;

    /** 
     * @brief Default constructor
     */
    Scanner2DSimulated();

    /**
     * @brief Constructor
     * @param identifier [in] Identifier for the entity
     * @param scannerFrame [in] Pointer to the frame holding the scanner.
     * @param rwstudio [in] Instance of RobWorkStudio to use for the simulated scanner. Defaults to NULL, but the initializeSimulation should be called.
     */
    Scanner2DSimulated(const std::string& identifier, rw::kinematics::Frame* scannerFrame, rws::RobWorkStudio* rwstudio);

    /**
     * @copydoc SimulatedEntity::initializeSimulation
     */
	void initializeSimulation(rws::RobWorkStudio* rwstudio);

    /**
     * @copydoc HWEntity::connect
     */
	virtual void connect();

    /**
     * @copydoc HWEntity::disconnect
     */
    virtual void disconnect();


    /**
     * @copydoc Scanner2D::scannerOn
     */
    virtual void scannerOn() {};

    /**
    * @copydoc Scanner2D::scannerOn
    */
    virtual void scannerOff() {};

    /**
    * @copydoc Scanner2D getScan
    */
    virtual std::vector<rw::math::Vector3D<> > getScan();


    /**
     * @copydoc Serializable::read
     */
	virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    /**
     * @copydoc Serializable::write
     */
    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

private:
	rws::RobWorkStudio* _rwstudio;
	rw::graphics::SceneViewer::Ptr _sceneviewer;
	rwlibs::simulation::GLFrameGrabber25D::Ptr _framegrabber;
	std::thread::id _threadId;
	rw::kinematics::Frame* _scannerFrame;
    std::vector<rw::math::Vector3D<double> > _lastScan;
	double _fovy;
	int _cnt;
    rw::math::Vector2D<> _sensingRange;

	bool _isConnected;
	bool _acquireImage;
	bool _imageAcquired;

	rw::common::Ptr<boost::thread> _thread;
	boost::mutex _mutex;

};

} //end namespace

#endif //#ifndef HWL_SCANNER2DSIMULATED_HPP
