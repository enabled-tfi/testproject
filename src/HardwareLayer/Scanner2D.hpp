/* */
#ifndef HWL_SCANNER2D_HPP
#define HWL_SCANNER2D_HPP

#include <Common/HWEntity.hpp>
#include <rw/math/Vector3D.hpp>
#include <vector>


namespace hwl {

/** 
 * Interface for a 2D laser sensor (such as a classic LIDAR scanner)
 */
class Scanner2D : public HWEntity
{
public:
	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<Scanner2D> Ptr;

	Scanner2D(const std::string& identifier);


	rw::kinematics::Frame* getSensorFrame();

    /**
     * @brief Connect to the device
     */
	virtual void connect() = 0;

	virtual void scannerOn() = 0;
	virtual void scannerOff() = 0;

	/**
	 * @brief Copies the current point cloud into the \bcloud argument
	 * @param cloud [out] PointCloud to copy the current values into.
	 */
	virtual std::vector<rw::math::Vector3D<> > getScan() = 0;
    
    class Scanner2DUpdateRule : public er_common::System::UpdateRule
    {
    public:
        Scanner2DUpdateRule(Scanner2D::Ptr scanner) :
            er_common::System::UpdateRule(),
            _scanner(scanner)
        {
        }

        ~Scanner2DUpdateRule()
        {
        }

        void update(rw::kinematics::State& state)
        {
            ER_LOG_DEBUG("Update scanner " << _scanner );
            std::vector<rw::math::Vector3D<> > result = _scanner->getScan();
            ER_LOG_DEBUG("SCAN: " );
            for (auto p : result) {
                ER_LOG_DEBUG("\t" << p );
            }
        }

        er_common::Entity::Ptr getEntity() {
            return _scanner;
        }


    private:
        Scanner2D::Ptr _scanner;
    };

    void addUpdateRule(er_common::System::Ptr system);



private:
    rw::kinematics::Frame* _sensorFrame;
};

} //end namespace

#endif //#ifndef HWL_SCANNER2D_HPP
