#include "Gripper1D.hpp"

using namespace hwl;
using namespace er_common;
using namespace rw::common;

Gripper1D::Gripper1D(const std::string& identifier, const std::string& deviceName):
	HWEntity(identifier),
    _deviceName(deviceName)
{

}


/**
* @copydoc HWEntity::addUpdateRule
*/
void Gripper1D::addUpdateRule(er_common::System::Ptr system) 
{
    if (getDeviceModel() != NULL) {
        system->addUpdateRule(ownedPtr(new Gripper1D::Gripper1DUpdateRule(this)));
    }
}

rw::models::Device::Ptr Gripper1D::getDeviceModel() const
{
    if (_device != NULL)
        return _device;
    _device = System::getInstance()->getWorkCell()->findDevice(_deviceName);
    return _device;
}


/* Methods from the serializable interface */
void Gripper1D::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    HWEntity::read(iarchive, id);
    if (iarchive.has("DeviceName")) {
        _deviceName = iarchive.read<std::string>("DeviceName");
    }

}

void Gripper1D::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    HWEntity::write(oarchive, id);
    if (_deviceName != "") {
        oarchive.write(_deviceName, "DeviceName");
    }
}