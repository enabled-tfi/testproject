/* */
#ifndef HWL_CAMERADEPTH_HPP
#define HWL_CAMERADEPTH_HPP

#include <Common/HWEntity.hpp>
#include <Common/System.hpp>
#include <rw/math/Vector3D.hpp>
#include <rw/sensor/Camera.hpp>
#include <vector>

#include <opencv2/opencv.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

namespace hwl {

/** 
 * Interface for a Depth Camera
 */
class CameraDepth : public HWEntity
{
public:
	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<CameraDepth> Ptr;

    CameraDepth(const std::string& identifier);

	virtual void acquire() = 0;

    virtual void getLastPointCloud(pcl::PointCloud<pcl::PointXYZ>& result) = 0;

    virtual pcl::PointCloud<pcl::PointXYZ> getLastPointCloud();

	class CameraDepthUpdateRule : public er_common::System::UpdateRule
	{
	public:
        CameraDepthUpdateRule(CameraDepth::Ptr camera) :
			er_common::System::UpdateRule(),
			_camera(camera)
		{
		}

		~CameraDepthUpdateRule()
		{
			//ER_LOG_DEBUG("Destroy Camera Windows " );
			//cv::destroyWindow("Acquired Image");
			//ER_LOG_DEBUG("Camera Window destroyed " );
		}

		void update(rw::kinematics::State& state)
		{
			try {
				_camera->acquire();
			} catch (const std::exception& exp) {
				ER_LOG_ERROR("Camera caught exception: " + (std::string)exp.what());
				ER_LOG_DEBUG("Trying to reconnect to camera");
				_camera->disconnect();
				_camera->connect();
			}
			//ER_LOG_DEBUG("Camera Update Rule::Update" );
			//ER_LOG_DEBUG("Camera Update Rule::Update Acquired" );
			//cv::imwrite("PylonToOpenCVToBeDisplayed.png", _camera->getLastImage());
			//cv::imshow("Acquired Image", _camera->getLastImage());
			//ER_LOG_DEBUG("Camera Update Rule::Show called" );
		}

		er_common::Entity::Ptr getEntity() {
			return _camera;
		}


	private:	
        CameraDepth::Ptr _camera;
	};

	void addUpdateRule(er_common::System::Ptr system);

    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

private:
};

} //end namespace

#endif //#ifndef HWL_CAMERADEPTH_HPP 
