#include "ActuatorSimulated.hpp"

using namespace hwl;
using namespace er_common;
using namespace rw::math;
using namespace rw::kinematics;

ActuatorSimulated::ActuatorSimulated():
	Actuator("ActuatorSimulated")
{

}


ActuatorSimulated::ActuatorSimulated(rw::models::Device::Ptr device, const std::string& id):
    Actuator(id, device->getName())
{

}

void ActuatorSimulated::initializeSimulation(rws::RobWorkStudio* rws)
{
}

void ActuatorSimulated::home()
{
    State state = System::getInstance()->getState();
    Q q(1);
    std::pair<double, double> bounds = getBounds();
    if (bounds.first > 0) {
        q(0) = bounds.first;
    }
    else if (bounds.second < 0) {
        q(0) = bounds.second;
    }
    getDeviceModel()->setQ(q, state);
    System::getInstance()->setState(state);
}


void ActuatorSimulated::setPosition(double pos)
{
    if (pos < getBounds().first || pos > getBounds().second) {
        RW_THROW("ActuatorSimulated::move: Target value of " << pos << " outside bounds [" << getBounds().first << ";" << getBounds().second << "]");
    }

    State state = System::getInstance()->getState();
    Q q(1);
    q(0) = pos;
    getDeviceModel()->setQ(q, state);
    System::getInstance()->setState(state);
}


double ActuatorSimulated::getPosition() const
{
    State state = System::getInstance()->getState();
    return getDeviceModel()->getQ(state)(0);
}


std::pair<double, double> ActuatorSimulated::getBounds() const
{
    std::pair<Q, Q> bds = getDeviceModel()->getBounds();
    return std::make_pair(bds.first(0), bds.second(0));
}


/* Methods from the serializable interface */
void ActuatorSimulated::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    iarchive.readEnterScope("ActuatorSimulated");
    Actuator::read(iarchive, id);
    iarchive.readLeaveScope("ActuatorSimulated");

}

void ActuatorSimulated::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("ActuatorSimulated");
    Actuator::write(oarchive, id);
    oarchive.writeLeaveScope("ActuatorSimulated");
}