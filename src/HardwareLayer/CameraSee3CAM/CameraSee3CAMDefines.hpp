#ifndef SRC_HARDWARELAYER_CAMERASEE3CAM_CAMERASEE3CAMDEFINES_HPP_
#define SRC_HARDWARELAYER_CAMERASEE3CAM_CAMERASEE3CAMDEFINES_HPP_

#define ENABLEMASTERMODE	0x50
#define ENABLETRIGGERMODE	0x51

#define CAMERA_CONTROL_CU20         0x86
#define GET_SENSOR_MODE_CU20        0x01
#define SET_SENSOR_MODE_CU20        0x02
#define GET_CAMERA_MODE_CU20        0x03
#define SET_CAMERA_MODE_CU20        0x04
#define SET_SPECIAL_MODE_CU20       0x08
#define GET_SPECIAL_MODE_CU20       0x07
#define GET_ORIENTATION_CU20        0x09
#define SET_ORIENTATION_CU20        0x0A
#define SET_STROBEMODE_CU20         0x0C
#define GET_STROBEMODE_CU20         0x0B
#define SET_AE_ROI_MODE_CU20        0x06
#define GET_AE_ROI_MODE_CU20        0x05
#define SET_COLORKILLVAL_CU20       0x0E
#define GET_COLORKILLVAL_CU20       0x0D
#define SET_BURSTLENGTH_CU20        0x10
#define GET_BURSTLENGTH_CU20        0x0F
#define SET_ANTIFLICKER_CU20        0x12
#define GET_ANTIFLICKER_CU20        0x11
#define SET_DENOISE_MODE_CU20       0x16
#define GET_DENOISE_MODE_CU20       0x15
#define SET_LSCMODE_CU20            0x14
#define GET_LSCMODE_CU20            0x13
#define SET_TO_DEFAULT_CU20         0xFF

#define SET_EXPOSURE_COMPENSATION_CU20  0x18
#define GET_EXPOSURE_COMPENSATION_CU20  0x17

#define SET_FAIL		0x00
#define SET_SUCCESS		0x01

#define GET_FAIL		0x00
#define GET_SUCCESS		0x01

#define BUFFER_LENGTH 64

const std::string econCameraVid  = "2560";


namespace hwl{
	
	enum sensorModes {
	    SENSOR_STANDARD = 0x01,
	    SENSOR_HDR_DLO  = 0x02
	};
	
	enum cameraModes {
	    CAMERA_MASTER = 0x01,
	    CAMERA_SLAVE  = 0x02
	};
	
	enum specialModes {
	    SPECIAL_NORMAL = 0x01,
	    SPECIAL_GREYSCALE  = 0x02
	};
	
	enum flipMirrorControls{
	    SetBothFlipDisable = 0x01,
	    SetHorzFlip = 0x03,
	    SetVertiFlip = 0x02,
	    SetBothFlipEnable = 0x04
	};
	
	enum strobeValues{
	    STROBE_OFF = 0x04,
	    STROBE_FLASH_VIDEO = 0x01,
	    STROBE_TORCH = 0x03
	};
	
	enum camROIAutoExpMode {
	    AutoExpCentered = 0x01,
	    AutoExpManual = 0x02,
	    AutoExpDisabled = 0x03
	};
	
	enum camAntiFlickerMode {
	    AntiFlickerAuto = 0x01,
	    AntiFlicker50Hz = 0x02,
	    AntiFlicker60Hz = 0x03
	};
	
	enum denoiseModes {
	    DenoiseEnable = 0x01,
	    DenoiseDisable = 0x02
	};
	
	enum lscModes {
	    LSC_AUTO = 0x01,
	    LSC_DAYLIGHT = 0x02,
	    LSC_CWFLIGHT = 0x03,
	    LSC_ALIGHT = 0x04
	};
}


#endif /* SRC_HARDWARELAYER_CAMERASEE3CAM_CAMERASEE3CAMDEFINES_HPP_ */
 
