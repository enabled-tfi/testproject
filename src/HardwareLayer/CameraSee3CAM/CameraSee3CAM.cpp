#include <fcntl.h>
#include <unistd.h>
#include "libudev.h"

#include <src/HardwareLayer/CameraSee3CAM/CameraSee3CAM.hpp>

namespace hwl {

CameraSee3CAM::CameraSee3CAM() :
	Camera("CameraSee3CAM", "")
{
	_hid_fd = -1;
	_camid = -1;
}

CameraSee3CAM::CameraSee3CAM(const std::string& identifier):
	Camera(identifier){
	_hid_fd = -1;
	_camid = -1;

}

CameraSee3CAM::~CameraSee3CAM() {

}

void CameraSee3CAM::connect(){
		// Find device
	std::string dev = findEconDevice("hidraw");

		// Open device and get hid filedescriptor
	_hid_fd = open(dev.c_str(), O_RDWR );

	ER_LOG_DEBUG("Setting CameraSee3CAM to default values: " << setToDefaultValues());

		// OpenCV usb interface for grabbing images
	_videoCapture.open(_camid);
	_videoCapture.set(cv::CAP_PROP_FRAME_WIDTH, 1920); // valueX = your wanted width
	_videoCapture.set(cv::CAP_PROP_FRAME_HEIGHT, 1080); // valueY = your wanted heigth
}

void CameraSee3CAM::disconnect(){
	_videoCapture.release();
}

void CameraSee3CAM::acquire(){
	_mutexFrame.lock();
	_videoCapture >> _image;
	_mutexFrame.unlock();}

cv::Mat CameraSee3CAM::getLastImage() {
	_mutexFrame.lock();
	if (_image.empty())
		RW_THROW("No image to return. Did you remember to call acquire first?");
	cv::Mat frameClone = _image.clone();
	_mutexFrame.unlock();

	return frameClone;
}

bool CameraSee3CAM::sendHidCmd(unsigned char *outBuf, unsigned char *inBuf, int len)
{
	if(_hid_fd < 0)
		RW_THROW("CameraSee3CAM not initialized properly, did you call connect()?");

    // Write data into camera
    int ret = pwrite(_hid_fd, outBuf, len, 0);

    if (ret < 0) {
    	ER_LOG_ERROR("Could not connect to See3CAM with hid: " << _hid_fd);
        perror("write");
        return false;
    }
    struct timeval tv;
    fd_set rfds;

    FD_ZERO(&rfds);
    FD_SET(_hid_fd, &rfds);

    /* Wait up to 5 seconds. */
    tv.tv_sec = 0;
    tv.tv_usec = 100;

    // Monitor read file descriptor for 5 secs
    if(0 > select(1, &rfds, NULL, NULL, &tv)){
    	ER_LOG_ERROR("See3CAM did not answer");
    	perror("select");
        return false;
    }

    // Read data from camera
    int retval = pread(_hid_fd, inBuf, len, 0);

    if (retval < 0) {
    	ER_LOG_ERROR("See3CAM action did not succeed");
        perror("read");
        return false;
    }
    else{
        return true;
    }

}

std::string CameraSee3CAM::findEconDevice(char * parameter)
{
    struct udev *udev;
    struct udev_enumerate *enumerate;
    struct udev_list_entry *devices, *dev_list_entry;
    struct udev_device *dev,*pdev;

    /* Create the udev object */
    udev = udev_new();
    if (!udev) {
    	return "-1";
    }

    /* Create a list of the devices in the 'hidraw' subsystem. */
    enumerate = udev_enumerate_new(udev);
    udev_enumerate_add_match_subsystem(enumerate, parameter);
    udev_enumerate_scan_devices(enumerate);
    devices = udev_enumerate_get_list_entry(enumerate);

    udev_list_entry_foreach(dev_list_entry, devices) {
        const char *path;

        path = udev_list_entry_get_name(dev_list_entry);
        dev = udev_device_new_from_syspath(udev, path);

        /* usb_device_get_devnode() returns the path to the device node
           itself in /dev. */
        pdev = udev_device_get_parent_with_subsystem_devtype(
                    dev,
                    "usb",
                    "usb_device");
        if (!pdev) {
        	continue;
        }

        if ((udev_device_get_sysattr_value(pdev,"idVendor") == econCameraVid))
        {
            std::string hid_device = udev_device_get_devnode(dev);
//            std::string productName = udev_device_get_sysattr_value(pdev,"product");
//            std::string serialNumber = udev_device_get_sysattr_value(pdev,"serial");
//            std::string vidValue = udev_device_get_sysattr_value(pdev,"idVendor");
//            std::string pidValue = udev_device_get_sysattr_value(pdev,"idProduct");
            if(parameter!="video4linux"){
                return hid_device;
            }
        }
//
    }
//    /* Free the enumerator object */
    udev_enumerate_unref(enumerate);
    udev_unref(udev);
    RW_THROW("Could not find any USB devices matching See3CAM");
    return "-1";
}


bool CameraSee3CAM::setSensorMode(sensorModes  sensorMode)
{
    // hid validation
    if(_hid_fd < 0)
    {
        return false;
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = SET_SENSOR_MODE_CU20; /* set sensor command  */
    _g_out_packet_buf[3] = sensorMode; /* pass camera mode value */

    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6] == SET_FAIL) {
            return false;
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == SET_SENSOR_MODE_CU20 &&
            _g_in_packet_buf[6] == SET_SUCCESS) {
            return true;
        }
    }
    return false;
}

sensorModes CameraSee3CAM::getSensorMode()
{
    // hid validation
    if(_hid_fd < 0)
    {
    	RW_THROW("Could not receive sensorMode");
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = GET_SENSOR_MODE_CU20; /* get sensor command  */

    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6] == GET_FAIL) {
        	RW_THROW("Could not receive sensorMode");
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == GET_SENSOR_MODE_CU20 &&
            _g_in_packet_buf[6] == GET_SUCCESS) {
        	return static_cast<sensorModes>(_g_in_packet_buf[2]);
        }
    }
	RW_THROW("Could not receive sensorMode");
}

bool CameraSee3CAM::setCameraMode(cameraModes  cameraMode)
{
    // hid validation
    if(_hid_fd < 0)
    {
        return false;
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = SET_CAMERA_MODE_CU20; /* set camera mode command  */
    _g_out_packet_buf[3] = cameraMode; /* pass camera mode value */

    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6] == SET_FAIL) {
            return false;
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == SET_CAMERA_MODE_CU20 &&
            _g_in_packet_buf[6] == SET_SUCCESS) {
            return true;
        }
    }
    return false;
}

cameraModes CameraSee3CAM::getCameraMode()
{
    // hid validation
    if(_hid_fd < 0)
    {
    	RW_THROW("Could not get camera mode");
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = GET_CAMERA_MODE_CU20; /* get camera command  */

    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6] == GET_FAIL) {
        	RW_THROW("Could not get camera mode");
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == GET_CAMERA_MODE_CU20 &&
            _g_in_packet_buf[6] == GET_SUCCESS) {
        	return static_cast<cameraModes>(_g_in_packet_buf[2]);
        }
    }
	RW_THROW("Could not get camera mode");
}

bool CameraSee3CAM::setSpecialMode(specialModes  specialMode)
{
    // hid validation
    if(_hid_fd < 0)
    {
        return false;
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = SET_SPECIAL_MODE_CU20; /* set special mode command  */
    _g_out_packet_buf[3] = specialMode; /* pass special mode value */

    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6]==SET_FAIL) {
            return false;
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == SET_SPECIAL_MODE_CU20 &&
            _g_in_packet_buf[6] == SET_SUCCESS) {
            return true;
        }
    }
    return false;
}

specialModes CameraSee3CAM::getSpecialMode()
{
    // hid validation
    if(_hid_fd < 0)
    {
    	RW_THROW("Could not get special mode");
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = GET_SPECIAL_MODE_CU20; /* get special mode command  */

    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6]==GET_FAIL) {
        	RW_THROW("Could not get special mode");
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1]==GET_SPECIAL_MODE_CU20 &&
            _g_in_packet_buf[6]==GET_SUCCESS) {\
        	return static_cast<specialModes>(_g_in_packet_buf[2]);
        }
    }
	RW_THROW("Could not get special mode");
}


/**
 * @brief See3CAM_CU20::setOrientation
 * @param horzModeSel - select/deselect horizontal flip mode
 * @param vertiModeSel - select/deselect vertical flip mode
 * @return true/false
 */
bool CameraSee3CAM::setOrientation(bool horzModeSel, bool vertiModeSel)
{
    // hid validation
    if(_hid_fd < 0)
    {
        return false;
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = SET_ORIENTATION_CU20; /* set orientation command  */

    if(horzModeSel && vertiModeSel){
        _g_out_packet_buf[3] = SetBothFlipEnable; /* both flip enable */
    }else if(horzModeSel && !vertiModeSel){
        _g_out_packet_buf[3] = SetHorzFlip; /* horizontal flip only mode */
    }else if(!horzModeSel && vertiModeSel){
        _g_out_packet_buf[3] = SetVertiFlip; /* vertical flip only mode */
    }else
        _g_out_packet_buf[3] = SetBothFlipDisable; /* both flip disable */

    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6]==SET_FAIL) {
            return false;
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == SET_ORIENTATION_CU20 &&
            _g_in_packet_buf[6] == SET_SUCCESS) {\
            return true;
        }
    }
    return false;
}

bool CameraSee3CAM::setStrobeMode(strobeValues strobeMode){
    // hid validation
    if(_hid_fd < 0)
    {
        return false;
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = SET_STROBEMODE_CU20; /* set strobe mode command  */
    _g_out_packet_buf[3] = strobeMode; /* pass strobe mode value */

    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6]==SET_FAIL) {
            return false;
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == SET_STROBEMODE_CU20 &&
            _g_in_packet_buf[6] == SET_SUCCESS) {
            return true;
        }
    }
    return false;
}

strobeValues CameraSee3CAM::getStrobeMode(){

    // hid validation
    if(_hid_fd < 0)
    {
    	RW_THROW("Could not get strobe mode");
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = GET_STROBEMODE_CU20; /* get strobe mode command  */

    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6]==GET_FAIL) {
        	RW_THROW("Could not get strobe mode");
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == GET_STROBEMODE_CU20 &&
            _g_in_packet_buf[6] == GET_SUCCESS) {\
        	return static_cast<strobeValues>(_g_in_packet_buf[2]);
        }
    }
	RW_THROW("Could not get strobe mode");
}

bool CameraSee3CAM::setROIAutoExposure(uint see3camAeROIMode, uint vidResolnWidth, uint vidResolnHeight, uint xCord, uint yCord, uint winSize){

    if(_hid_fd < 0)
    {
        return false;
    }

    double outputLow = 0;
    double outputHigh = 255;
    double inputXLow = 0;
    double inputXHigh = vidResolnWidth-1;
    double inputXCord = xCord;
    int outputXCord = ((inputXCord - inputXLow) / (inputXHigh - inputXLow)) * (outputHigh - outputLow) + outputLow;

    double inputYLow = 0;
    double inputYHigh = vidResolnHeight-1;
    double inputYCord = yCord;
    int outputYCord = ((inputYCord - inputYLow) / (inputYHigh - inputYLow)) * (outputHigh - outputLow) + outputLow;

    //Initialize buffers
    initializeBuffers();

    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = SET_AE_ROI_MODE_CU20; /* set auto exposure ROI mode command */
    _g_out_packet_buf[3] = see3camAeROIMode; /* auto exposure ROI mode value */

    if(see3camAeROIMode == AutoExpManual){
        _g_out_packet_buf[4] = outputXCord; // x cord
        _g_out_packet_buf[5] = outputYCord; // y cord
        _g_out_packet_buf[6] = winSize; // window size
    }

    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6]==SET_FAIL) {
            return false;
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == SET_AE_ROI_MODE_CU20 &&
            _g_in_packet_buf[6] == SET_SUCCESS) {
            return true;
        }
    }

    return false;
}
//bool CameraSee3CAM::getAutoExpROIModeAndWindowSize();
//

bool CameraSee3CAM::setColourKill(uint colourKillValue){
    // hid validation
    if(_hid_fd < 0)
    {
        return false;
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = SET_COLORKILLVAL_CU20; /* set color kill status command  */
    _g_out_packet_buf[3] = colourKillValue; /* pass colour kill status value */

    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6]==SET_FAIL) {
            return false;
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == SET_COLORKILLVAL_CU20 &&
            _g_in_packet_buf[6] == SET_SUCCESS) {
            return true;
        }
    }
    return false;
}

uint CameraSee3CAM::getColourKill(){

    // hid validation
    if(_hid_fd < 0)
    {
    	RW_THROW("Could not get colour kill value");
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = GET_COLORKILLVAL_CU20; /* get colour kill command  */

    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6]==GET_FAIL) {
        	RW_THROW("Could not get colour kill value");
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == GET_COLORKILLVAL_CU20 &&
            _g_in_packet_buf[6] == GET_SUCCESS) {
        	return static_cast<uint>(_g_in_packet_buf[2]);
        }
    }
	RW_THROW("Could not get colour kill value");
}

bool CameraSee3CAM::setBurstLength(uint burstLength){
    // hid validation
    if(_hid_fd < 0)
    {
        return false;
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = SET_BURSTLENGTH_CU20; /* set burst length command  */
    _g_out_packet_buf[3] = burstLength; /* set burst length value */

    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6]==SET_FAIL) {
            return false;
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == SET_BURSTLENGTH_CU20 &&
            _g_in_packet_buf[6] == SET_SUCCESS) {
            return true;
        }
    }
    return false;
}

uint CameraSee3CAM::getBurstLength(){
    // hid validation
    if(_hid_fd < 0)
    {
    	RW_THROW("Could not get burst length value");
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = GET_BURSTLENGTH_CU20; /* get burst length command  */

    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6]==GET_FAIL) {
        	RW_THROW("Could not get burst length value");
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == GET_BURSTLENGTH_CU20 &&
            _g_in_packet_buf[6] == GET_SUCCESS) {
        	return static_cast<uint>(_g_in_packet_buf[2]);
        }
    }
	RW_THROW("Could not get burst length value");
}

bool CameraSee3CAM::setAntiFlickerMode(camAntiFlickerMode antiFlickerMode){
    // hid validation
    if(_hid_fd < 0)
    {
        return false;
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = SET_ANTIFLICKER_CU20; /* set anti flicker command  */
    _g_out_packet_buf[3] = antiFlickerMode; /* anti flicker mode to set */
    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6]==SET_FAIL) {
            return false;
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == SET_ANTIFLICKER_CU20 &&
            _g_in_packet_buf[6] == SET_SUCCESS) {
            return true;
        }
    }
    return false;
}

camAntiFlickerMode CameraSee3CAM::getAntiFlickerMode(){
    // hid validation
    if(_hid_fd < 0)
    {
    	RW_THROW("Could not get anti flicker mode");
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = GET_ANTIFLICKER_CU20; /* get anti flicker command  */

    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6] == GET_FAIL) {
        	RW_THROW("Could not get anti flicker mode");
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == GET_ANTIFLICKER_CU20 &&
            _g_in_packet_buf[6] == GET_SUCCESS) {
        	return static_cast<camAntiFlickerMode>(_g_in_packet_buf[2]);
        }
    }
	RW_THROW("Could not get anti flicker mode");
}

bool CameraSee3CAM::setDenoiseCtrlMode(denoiseModes  deNoiseMode)
{
    // hid validation
    if(_hid_fd < 0)
    {
        return false;
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = SET_DENOISE_MODE_CU20; /* set special mode command  */
    _g_out_packet_buf[3] = deNoiseMode; /* pass denoise mode value */

    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6]==SET_FAIL) {
            return false;
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == SET_DENOISE_MODE_CU20 &&
            _g_in_packet_buf[6] == SET_SUCCESS) {
            return true;
        }
    }
    return false;
}

denoiseModes CameraSee3CAM::getDenoiseCtrlMode()
{
    // hid validation
    if(_hid_fd < 0)
    {
    	RW_THROW("Could not get denoise control mode");
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = GET_DENOISE_MODE_CU20; /* get special mode command  */

    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6]==GET_FAIL) {
        	RW_THROW("Could not get denoise control mode");
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == GET_DENOISE_MODE_CU20 &&
            _g_in_packet_buf[6] == GET_SUCCESS) {
        	return static_cast<denoiseModes>(_g_in_packet_buf[2]);
        }
    }
	RW_THROW("Could not get denoise control mode");
}

bool CameraSee3CAM::setLSCMode(uint lscMode){
    // hid validation
    if(_hid_fd < 0)
    {
        return false;
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = SET_LSCMODE_CU20; /* set Lsc mode command  */
    _g_out_packet_buf[3] = lscMode; /* LSC Mode value to set */

    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6]==SET_FAIL) {
            return false;
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == SET_LSCMODE_CU20 &&
            _g_in_packet_buf[6] == SET_SUCCESS) {
            return true;
        }
    }
    return false;
}

uint CameraSee3CAM::getLSCMode()
{
    // hid validation
    if(_hid_fd < 0)
    {
    	RW_THROW("Could not get LCS mode");
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = GET_LSCMODE_CU20; /* get LSC mode command  */

    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6]==GET_FAIL) {
        	RW_THROW("Could not get LCS mode");
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == GET_LSCMODE_CU20 &&
            _g_in_packet_buf[6] == GET_SUCCESS) {
        	return static_cast<uint>(_g_in_packet_buf[2]);
        }
    }
	RW_THROW("Could not get LCS mode");
}


bool CameraSee3CAM::setExposureCompensation(unsigned int exposureCompValue){

    // hid validation
    if(_hid_fd < 0)
    {
        return false;
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = SET_EXPOSURE_COMPENSATION_CU20; /* set exposure compensation command  */
    _g_out_packet_buf[3] = exposureCompValue;

    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6]==SET_FAIL) {
            return false;
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == SET_EXPOSURE_COMPENSATION_CU20 &&
            _g_in_packet_buf[6] == SET_SUCCESS) {
            return true;
        }
    }
    return false;
}

unsigned int CameraSee3CAM::getExposureCompensation(){

    // hid validation
    if(_hid_fd < 0)
    {
    	RW_THROW("Could not get exposure compensation");
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = GET_EXPOSURE_COMPENSATION_CU20; /* get exposure compensation command */

    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6]==GET_FAIL) {
        	RW_THROW("Could not get exposure compensation");
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == GET_EXPOSURE_COMPENSATION_CU20 &&
            _g_in_packet_buf[6] == GET_SUCCESS) {
        	return static_cast<int>(_g_in_packet_buf[2]);
        }
    }
	RW_THROW("Could not get exposure compensation");
}

bool CameraSee3CAM::setToDefaultValues(){
    // hid validation
    if(_hid_fd < 0)
    {
        return false;
    }

    //Initialize buffers
    initializeBuffers();

    // fill buffer values
    _g_out_packet_buf[1] = CAMERA_CONTROL_CU20; /* camera id */
    _g_out_packet_buf[2] = SET_TO_DEFAULT_CU20; /* set default command  */

    // send request and get reply from camera
    if(sendHidCmd(_g_out_packet_buf, _g_in_packet_buf, BUFFER_LENGTH)){
        if (_g_in_packet_buf[6]==SET_FAIL) {
            return false;
        } else if(_g_in_packet_buf[0] == CAMERA_CONTROL_CU20 &&
            _g_in_packet_buf[1] == SET_TO_DEFAULT_CU20 &&
            _g_in_packet_buf[6] == SET_SUCCESS) {
            return true;
        }
    }
    return false;
}




bool CameraSee3CAM::enableMasterMode()
{
    int ret = 0;

    if(_hid_fd < 0)
    {
        return false;
    }
    //Initialize the buffer
    memset(_g_out_packet_buf, 0x00, sizeof(_g_out_packet_buf));

    //Set the Report Number
    _g_out_packet_buf[1] = ENABLEMASTERMODE; /* Report Number */
    ret = ::write(_hid_fd, _g_out_packet_buf, BUFFER_LENGTH);
    if (ret < 0) {
    	ER_LOG_ERROR("Could not set CameraSee3CAM in master mode");
        perror("write");
        return false;
    }
    return true;
}

bool CameraSee3CAM::enableTriggerMode()
{
    int ret =0;

    if(_hid_fd < 0)
    {
        return false;
    }
    //Initialize the buffer
    initializeBuffers();

    //Set the Report Number
    _g_out_packet_buf[1] = ENABLETRIGGERMODE; /* Report Number */

    ret = ::write(_hid_fd, _g_out_packet_buf, BUFFER_LENGTH);
    if (ret < 0) {
    	ER_LOG_ERROR("Could not set CameraSee3CAM in trigger mode");
        perror("write");
        return false;
    }
    return true;
}

void CameraSee3CAM::initializeBuffers(){

    //Initialize input and output buffers
    memset(_g_out_packet_buf, 0x00, sizeof(_g_out_packet_buf));
    memset(_g_in_packet_buf, 0x00, sizeof(_g_in_packet_buf));
}



void CameraSee3CAM::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
	iarchive.readEnterScope("CameraSee3CAM");
	Camera::read(iarchive, id);

	_camid = iarchive.read<int>("CameraId");

    iarchive.readLeaveScope("CameraSee3CAM");
}

void CameraSee3CAM::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	oarchive.writeEnterScope("CameraSee3CAM");
	Camera::write(oarchive, id);

	oarchive.write(_camid, "CameraId");

	oarchive.writeLeaveScope("CameraSee3CAM");
}



} /* namespace hwl */
