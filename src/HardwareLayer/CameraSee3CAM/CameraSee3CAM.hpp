#ifndef SRC_HARDWARELAYER_CAMERASEE3CAM_CAMERASEE3CAM_HPP_
#define SRC_HARDWARELAYER_CAMERASEE3CAM_CAMERASEE3CAM_HPP_

#include <HardwareLayer/Camera.hpp>
#include "CameraSee3CAMDefines.hpp"

namespace hwl {

/**
 * Camera interface for the See3CAM_CU20 (Might work for other See3CAMs, but ptopably need some modification to do so)
 */


class CameraSee3CAM : public Camera {
public:

	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<CameraSee3CAM> Ptr;

    /**
     * @brief Default constructor
     */
	CameraSee3CAM();

    /**
     * @brief Constructor
     * @param identifier [in] Identifier for the entity
     */
	CameraSee3CAM(const std::string& identifier);

	/**
	 * @brief Destructor
	 */
	~CameraSee3CAM();

    /**
     * @copydoc HWEntity::connect
     */
	virtual void connect();

    /**
     * @copydoc HWEntity::disconnect
     */
    virtual void disconnect();

    /**
     * @copydoc Camera::acquire
     */
	virtual void acquire();

    /**
     * @copydoc Camera::getLastImage
     */
    virtual cv::Mat getLastImage();

	/**
	 * @brief Setting sensor mode
	 * @param sensorMode [in] standard/dsr hlo mode
	 * @return true/false
	 */
    bool setSensorMode(sensorModes sensorMode);

    /**
     * @brief Getting sensor mode
     * @return Sensormode of camera
     */
    sensorModes getSensorMode();

    /**
     * @brief Setting camera mode
     * @param cameraMode [in] master/slave
     * @return true/false
     */
    bool setCameraMode(cameraModes  cameraMode);

    /**
     * @brief Getting camera mode
     * @return Cameramode of camera
     */
    cameraModes getCameraMode();

    /**
     * @brief Setting special mode in the camera
     * @param specialMode [in] Normal/Greyscale
     * @return true/false
     */
    bool setSpecialMode(specialModes  specialMode);

    /**
     * @brief Getting special mode in the camera
     * @return Special mode of camera
     */
    specialModes getSpecialMode();

    /**
     * @brief Set orientation flip modes
     * @param horzModeSel [in] select/deselect horizontal flip mode
     * @param vertiModeSel [in] select/deselect vertical flip mode
     * @return true/false
     */
    bool setOrientation(bool horzModeSel, bool vertiModeSel);

    /**
     * @brief Setting strobe mode in camera
     * @param strobeMode [in] off/flash/torch
     * @return true/false
     */
    bool setStrobeMode(strobeValues strobeMode);

    /**
     * @brief Get Strobe mode
     * @return Strobe mode of camera
     */
    strobeValues getStrobeMode();

    /**
     * @brief Set ROI auto exposure to camera
     * @param camROIAutoExposureMode [in] ROI mode
     * @param vidResolnWidth [in] preview resolution width
     * @param vidResolnHeight [in] preview resolution height
     * @param xCord [in] mouse xCordinate from preview
     * @param yCord [in] mouse yCordinate from preview
     * @param winSize [in] ROI window size
     * @return true/false
     */
    bool setROIAutoExposure(uint see3camAeROIMode, uint vidResolnWidth, uint vidResolnHeight, uint xCord, uint yCord, uint winSize);
//    bool getAutoExpROIModeAndWindowSize();

    /**
     * @brief Set Colour Kill value
     * @param colourKillValue [in] value to set
     * @return true/false
     */
    bool setColourKill(uint colourKillValue);

    /**
     * @brief Get color kill status value
     * @return true/false
     */
    uint getColourKill();

    /**
     * @brief Setting burst length value
     * @param burstLength [in] number of shots to take
     * @return true/false
     */
    bool setBurstLength(uint burstLength);

    /**
     * @brief Get current value of burst length
     * @return true/false
     */
    uint getBurstLength();


    /**
     * @briefSetting anti flicker mode
     * @param antiFlickerMode [in] auto/50hz/60hz
     * @return true/false
     */
    bool setAntiFlickerMode(camAntiFlickerMode antiFlickerMode);

    /**
     * @brief Get current anti flicker mode from camera
     * @return - anti flicker mode of camera
     */
    camAntiFlickerMode getAntiFlickerMode();

    /**
     * @brief Set denoise mode into the camera
     * @param deNoiseMode [in] denoise mode value to set
     * @return true/false
     */
    bool setDenoiseCtrlMode(denoiseModes  deNoiseMode);

    /**
     * @brief Get denoise control mode
     * @return true/false
     */
    denoiseModes getDenoiseCtrlMode();

    /**
     * @brief Set LSC mode
     * @param LSC mode to set
     * @return true/false
     */
    bool setLSCMode(uint lscMode);

    /**
     * @brief Get current LSC mode from camera
     * @return true/false
     */
    uint getLSCMode();


    /**
     * @brief Set exposure compensation
     * param - exposureCompensation [in] value
     * @return true/false
     */
    bool setExposureCompensation(unsigned int exposureCompValue);

    /**
     * @brief Getting exposure compensation
     * @return Exposure compensation of camera
     */
    unsigned int getExposureCompensation();

    /**
     * @brief Set all control values to default
     * @return  true/false
     */
    bool setToDefaultValues();

    /**
     * @brief Enable Camera in Master Mode
     *  - This function enables the camera in master mode
     * @return
     * true - On Success, false - On Failure
     */
    bool enableMasterMode();

    /**
     * @brief Enable Camera in trigger mode
     *  - This function enables the camera in trigger mode
     * @return
     * true - On Success, false - On Failure
     */
    bool enableTriggerMode();

	/* Methods from the serializable interface */
    /**
     * @copydoc Serializable::read
     */
	virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    /**
     * @copydoc Serializable::write
     */
    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

private:

    bool sendHidCmd(unsigned char *outBuf, unsigned char *inBuf, int len);
    std::string findEconDevice(char * parameter);
    void initializeBuffers();

	int _camid;
	cv::VideoCapture _videoCapture;
	boost::mutex _mutexFrame;

    cv::Mat _image;
	cv::Mat _cameraMatrix;
	cv::Mat _distCoeffs;

    unsigned char _g_out_packet_buf[BUFFER_LENGTH];
    unsigned char _g_in_packet_buf[BUFFER_LENGTH];


	unsigned int _hid_fd;

};

} /* namespace hwl */

#endif /* SRC_HARDWARELAYER_CAMERASEE3CAM_CAMERASEE3CAM_HPP_ */
