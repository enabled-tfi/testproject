/* */
#ifndef HWL_MOBILEDEVICE_HPP
#define HWL_MOBILEDEVICE_HPP

#include <Common/HWEntity.hpp>
#include <Common/System.hpp>
#include <Common/Utils.hpp>
#include <rw/common/Ptr.hpp>
#include <rw/math.hpp>
#include <rw/kinematics/MovableFrame.hpp>

namespace hwl {
	
/**
 * General interface for a mobile device
 */
class MobileDevice: public HWEntity
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<MobileDevice> Ptr;

	/**
	 * @brief Constructs a MobileDevice with a given identifier
	 */
	MobileDevice(const std::string& identifier);


	/**
	 * @brief Move the robot to a given absolute position and orientation
	 *
	 * The robot may apply a path planner underneath to find the right path.
	 *
	 * @param target [in] Target position and orientation
	 */

	virtual void moveTo(const rw::math::Pose2D<>& target, double goalThreshold) = 0;

	/**
	 * @brief Returns the current state
	 * @return Current state
	 */
	virtual rw::math::Pose2D<> getCurrentPose() const = 0;

	/**
	 * Struct for a defined pose in the mobile device.
	 */
	struct DefinedPosition {
	    std::string uid;
	    std::string displayName;
	    rw::math::Pose2D<> pose;
	    DefinedPosition(const std::string& uid, const std::string& displayName, const rw::math::Pose2D<>& pose):
	        uid(uid), displayName(displayName), pose(pose)
	    {

	    }
	};

	/**
	 * @brief Returns the uids of all prefined positions in the mobile device
	 * @return List of uids of de
	 */
	virtual std::vector<DefinedPosition> getDefinedPositions() const = 0;

	/**
	 * @brief Returns true if the mobile device is moving
	 * @return True if moving. False otherwise
 	 */
	virtual bool isMoving() const = 0;




	class MobileDeviceUpdateRule: public er_common::System::UpdateRule
	{
	public:
		MobileDeviceUpdateRule(rw::kinematics::MovableFrame::Ptr frame, MobileDevice::Ptr hwlEntity):
			er_common::System::UpdateRule(),
			_frame(frame),
			_hwlEntity(hwlEntity)
		{
		}

		void update(rw::kinematics::State& state)
		{
			rw::math::Pose2D<> pose = _hwlEntity->getCurrentPose();
			//ER_LOG_DEBUG("MobileDeviceUpdateRule: " << pose.getPos() << " " << pose.theta() );
			rw::math::Transform3D<> t3d = er_common::Utils::pose2DtoTransform3D(pose, _frame->getTransform(state));
			//ER_LOG_DEBUG("Mobile Device Update Rule " << t3d );
			_frame->setTransform(t3d, state);
		}

		er_common::Entity::Ptr getEntity() {
			return _hwlEntity;
		}


	private:
		rw::kinematics::MovableFrame::Ptr _frame;
		MobileDevice::Ptr _hwlEntity;
	};

	void addUpdateRule(er_common::System::Ptr system);
};

} //end namespace

#endif //#ifndef HWL_MOBILEDEVICE_HPP
