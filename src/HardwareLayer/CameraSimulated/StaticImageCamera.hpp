
#ifndef SRC_HARDWARELAYER_CAMERASIMULATED_STATICIMAGECAMERA_HPP_
#define SRC_HARDWARELAYER_CAMERASIMULATED_STATICIMAGECAMERA_HPP_

#include <Common/Entity.hpp>
#include <Common/System.hpp>
#include <HardwareLayer/Camera.hpp>

#include <opencv2/opencv.hpp>

namespace hwl {

/**
 * Simulated camera implementing the hwl::Camera interface and always return a loaded image
 */

class StaticImageCamera : public Camera{
public:

	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<StaticImageCamera> Ptr;

    /**
     * @brief Default constructor
     */
	StaticImageCamera();

    /**
     * @brief Constructor
     * @param identifier [in] Identifier for the entity
     * @param imageLocation [in] Filename of image
     */
	StaticImageCamera(const std::string& identifier, const std::string& imageLocation);

	/**
	 * @brief Destructor
	 */
	~StaticImageCamera();

    /**
     * @copydoc HWEntity::connect
     */
	virtual void connect();

    /**
     * @copydoc HWEntity::disconnect
     */
    virtual void disconnect();

    /**
     * @copydoc Camera::acquire
     */
	virtual void acquire();

    /**
     * @copydoc Camera::getLastImage
     */
    virtual cv::Mat getLastImage();

    void setImage(cv::Mat img);

	virtual std::pair<cv::Mat, cv::Mat> getCameraCalibration();

	/* Methods from the serializable interface */
    /**
     * @copydoc Serializable::read
     */
	virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    /**
     * @copydoc Serializable::write
     */
    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

private:
    cv::Mat _image;
	cv::Mat _cameraMatrix;
	cv::Mat _distCoeffs;

	std::string _imageLocation;

};

} /* namespace hwl */

#endif /* SRC_HARDWARELAYER_CAMERASIMULATED_STATICIMAGECAMERA_HPP_ */
