
#include <src/HardwareLayer/CameraSimulated/StaticImageCamera.hpp>

namespace hwl {

StaticImageCamera::StaticImageCamera() :
		Camera("StaticImageCamera", "")
{

}

StaticImageCamera::StaticImageCamera(const std::string& identifier, const std::string& imageLocation):
	Camera(identifier){
}

StaticImageCamera::~StaticImageCamera() {
	// TODO Auto-generated destructor stub
}

void StaticImageCamera::connect(){
	_image = cv::imread(er_common::System::getInstance()->getSystemFolder() + _imageLocation);
}

void StaticImageCamera::disconnect(){
	return;
}

void StaticImageCamera::acquire(){
	return;
}

cv::Mat StaticImageCamera::getLastImage() {
	return _image.clone();
}


std::pair<cv::Mat, cv::Mat> StaticImageCamera::getCameraCalibration(){
	if (_cameraMatrix.empty()) {
		_cameraMatrix = cv::Mat::eye(3, 3, CV_64F);
		_distCoeffs = cv::Mat::zeros(5, 1, CV_32F);

	}
	return std::make_pair(_cameraMatrix, _distCoeffs);
}

void StaticImageCamera::setImage(cv::Mat img){
	_image = img.clone();
}

void StaticImageCamera::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
	iarchive.readEnterScope("StaticImageCamera");
	Camera::read(iarchive, id);
	_imageLocation = iarchive.read<std::string>("ImageLocation");

    iarchive.readLeaveScope("StaticImageCamera");
}

void StaticImageCamera::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	oarchive.writeEnterScope("StaticImageCamera");
	Camera::write(oarchive, id);

	oarchive.write(_imageLocation, "ImageLocation");

	oarchive.writeLeaveScope("StaticImageCamera");
}


} /* namespace hwl */
