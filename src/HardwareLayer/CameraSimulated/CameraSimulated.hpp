/* */
#ifndef HWL_CAMERASIMULATED_HPP
#define HWL_CAMERASIMULATED_HPP

#include <Common/Entity.hpp>
#include <Common/System.hpp>
#include <HardwareLayer/Camera.hpp>
#include <HardwareLayer/SimulatedEntity.hpp>
#include <rw/math/Vector3D.hpp>
//#include <rwlibs/simulation/SimulatedCamera.hpp>
#include <rwlibs/simulation/GLFrameGrabber.hpp>
#include <rw/graphics/SceneViewer.hpp>
#include <rws/RobWorkStudio.hpp>
#include <vector>
#include <thread>
#include <opencv2/opencv.hpp>
#include <boost/thread.hpp>
namespace hwl {

/** 
 * Simulated camera implementing the hwl::Camera interface
 */
class CameraSimulated : public Camera, public SimulatedEntity
{
public:
	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<CameraSimulated> Ptr;

    /** 
     * @brief Default constructor
     */
	CameraSimulated();

    /**
     * @brief Constructor
     * @param identifier [in] Identifier for the entity
     * @param cameraFrame [in] Pointer to the frame holding the camera.
     * @param rwstudio [in] Instance of RobWorkStudio to use for the simulated camera. Defaults to NULL, but the initializeSimulation should be called.
     */
	CameraSimulated(const std::string& identifier, rw::kinematics::Frame* cameraFrame, rws::RobWorkStudio* rwstudio);

	/**
	 * @brief Set a new camera frame for the simulated camera to grab images from
	 * @param cameraFrame [in] camera frame
	 */
	void setCameraFrame(rw::kinematics::Frame* cameraFrame = NULL);

    /**
     * @copydoc SimulatedEntity::initializeSimulation
     */
	void initializeSimulation(rws::RobWorkStudio* rwstudio);

    /**
     * @copydoc HWEntity::connect
     */
	virtual void connect();

    /**
     * @copydoc HWEntity::disconnect
     */
    virtual void disconnect();

    /**
     * @copydoc Camera::acquire
     */
	virtual void acquire();

    /**
     * @copydoc Camera::getLastImage
     */
    virtual cv::Mat getLastImage();

	virtual std::pair<cv::Mat, cv::Mat> getCameraCalibration();

	/* Methods from the serializable interface */
    /**
     * @copydoc Serializable::read
     */
	virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    /**
     * @copydoc Serializable::write
     */
    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

private:
	rws::RobWorkStudio* _rwstudio;
	rw::graphics::SceneViewer::Ptr _sceneviewer;
	rwlibs::simulation::GLFrameGrabber::Ptr _framegrabber;
	std::thread::id _threadId;
	rw::kinematics::Frame* _cameraFrame;
	cv::Mat _cameraMatrix;
	cv::Mat _distCoeffs;

	std::string _cameraFrameName;

	double _fovy;
	int _width, _height;

	bool _isConnected;
	bool _acquireImage;
	bool _imageAcquired;

	rw::common::Ptr<boost::thread> _thread;
	boost::mutex _mutex;

};

} //end namespace

#endif //#ifndef HWL_CAMERASIMULATED_HPP
