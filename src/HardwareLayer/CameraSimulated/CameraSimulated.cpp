#include "CameraSimulated.hpp"
//#include "SimpleGLViewer.hpp"
#include <rw/sensor/Image.hpp>

#include <rwlibs/simulation/SimulatedCamera.hpp>
#include <rws/SceneOpenGLViewer.hpp>
#include <opencv2/highgui.hpp>

//#include "GLXOffscreenViewer.cpp"
using namespace rw::math;
using namespace rw::common;
using namespace rw::kinematics; 
using namespace rw::graphics;
using namespace rws;
using namespace rw::sensor;
using namespace rwlibs::simulation;

using namespace er_common;
using namespace hwl;

#include <thread>

namespace
{
	const std::string camId("Camera");
}

CameraSimulated::CameraSimulated():
	Camera("CameraSimulated", "")
{

}

CameraSimulated::CameraSimulated(const std::string& identifier, Frame* cameraFrame, rws::RobWorkStudio* rwstudio):
	Camera(identifier, ""),
	_cameraFrame(cameraFrame),
	_rwstudio(rwstudio)
{
	
}

void CameraSimulated::initializeSimulation(rws::RobWorkStudio* rwstudio)
{
	_rwstudio = rwstudio;
}


std::pair<cv::Mat, cv::Mat> CameraSimulated::getCameraCalibration()
{
	if (_cameraMatrix.empty()) {
		double focal = (_height / 2.0) / std::tan((_fovy/2.0)*Deg2Rad);

		_cameraMatrix = cv::Mat::zeros(3, 3, CV_64F);
		_cameraMatrix.at<double>(0, 0) = focal;
		_cameraMatrix.at<double>(0, 2) = _width / 2;
		_cameraMatrix.at<double>(1, 1) = focal;
		_cameraMatrix.at<double>(1, 2) = _height / 2; 
		_cameraMatrix.at<double>(2, 2) = 1;
		_distCoeffs = cv::Mat::zeros(5, 1, CV_32F);
	}
	ER_LOG_DEBUG("Camera Matrix = "<<_cameraMatrix );
	return std::make_pair(_cameraMatrix, _distCoeffs);
	//RW_THROW("Not supported for simulated camera yet");
}


void CameraSimulated::connect()
{

	if (_cameraFrame == NULL)
		RW_THROW("Camera frame is NULL");

	if (_cameraFrame->getPropertyMap().has(camId) == false) {
		RW_THROW("Specified frame does not have camera properties");
	}
	

	
	_sceneviewer = _rwstudio->getView()->getSceneViewer();// sceneviewer;
	std::string camParam = _cameraFrame->getPropertyMap().get<std::string>(camId);
	std::istringstream iss(camParam, std::istringstream::in);
	iss >> _fovy >> _width >> _height;
	ER_LOG_DEBUG("Width = " << _width << " height = " << _height << " fov = " << _fovy );
	_framegrabber = ownedPtr(new GLFrameGrabber(_width, _height, _fovy, 0.1, 10.0));
	ER_LOG_DEBUG("_sceneviewer = " << _sceneviewer );
	_framegrabber->init(_sceneviewer);

	_isConnected = true;
	_threadId = std::this_thread::get_id(); 
	ER_LOG_DEBUG("THREAD ID = " << _threadId );

} 

void CameraSimulated::disconnect() {
	_isConnected = false;
}

void CameraSimulated::acquire() 
{
	//ER_LOG_DEBUG("ThreadId to match = " << _threadId << " Thread id is " << std::this_thread::get_id() );
	bool firstWait = true;
	if (_threadId == std::this_thread::get_id()) {
		_mutex.lock();
		_framegrabber->grab(_cameraFrame, System::getInstance()->getState());
		_mutex.unlock();
		_imageAcquired = true;
	}
	else {
		_imageAcquired = false;
		while (_imageAcquired == false) {
			if (firstWait) {
//				ER_LOG_DEBUG("Wait for someone to take a picture " );
				firstWait = false;
			}
			TimerUtil::sleepMs(100);
		}
	}
	return; 
	_acquireImage = true;
	do {
		TimerUtil::sleepMs(1);
	} while (_acquireImage);
}

cv::Mat CameraSimulated::getLastImage()
{
	boost::mutex::scoped_lock scoped_lock(_mutex);
	const Image& image = _framegrabber->getImage();
	//ER_LOG_DEBUG(" image grapped" );
	cv::Mat mat(image.getHeight(), image.getWidth(), CV_8UC3);
	memcpy(mat.data, image.getImageData(), (image.getNrOfChannels()*image.getBitsPerPixel() / 8) * image.getWidth() * image.getHeight());
	cv::cvtColor(mat, mat, cv::COLOR_RGB2BGR);
	cv::flip(mat, mat, 1);
	return mat;
}

void CameraSimulated::setCameraFrame(rw::kinematics::Frame* cameraFrame){
	if(cameraFrame == NULL)
		_cameraFrame = System::getInstance()->getWorkCell()->findFrame(_cameraFrameName);
	else
		_cameraFrame = cameraFrame;
}


/* Methods from the serializable interface */
void CameraSimulated::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
	iarchive.readEnterScope("CameraSimulated");
	Camera::read(iarchive, id);
	_cameraFrameName = iarchive.read<std::string>("CameraFrame");
	if (_cameraFrameName != "NULL") {
		_cameraFrame = System::getInstance()->getWorkCell()->findFrame(_cameraFrameName);
		if (_cameraFrame == NULL) {
			RW_THROW("No frame named " << _cameraFrameName << " found in work cell");
		}
	}

    iarchive.readLeaveScope("CameraSimulated");
}

void CameraSimulated::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	oarchive.writeEnterScope("CameraSimulated");
	Camera::write(oarchive, id);
	if (_cameraFrame != NULL)
		oarchive.write(_cameraFrame->getName(), "CameraFrame");
	else
		oarchive.write(std::string("NULL"), "CameraFrame");

	oarchive.writeLeaveScope("CameraSimulated");
}
