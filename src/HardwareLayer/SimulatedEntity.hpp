/* */
#ifndef HWL_SIMULATEDENTITY_HPP
#define HWL_SIMULATEDENTITY_HPP

#include <Common/Entity.hpp>
#include <Common/System.hpp>
#include <rws/RobWorkStudio.hpp>



namespace hwl {

class SimulatedEntity
{
public:

	virtual void initializeSimulation(rws::RobWorkStudio* rws) = 0;

};

} //end namespace

#endif //#ifndef HWL_SIMULATEDENTITY_HPP
