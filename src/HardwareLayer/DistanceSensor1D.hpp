/* */
#ifndef HWL_DISTANCESENSOR1D_HPP
#define HWL_DISTANCESENSOR1D_HPP

#include <Common/HWEntity.hpp>
#include <rw/math/Vector3D.hpp>
#include <vector>


namespace hwl {

/** 
 * Interface for a 1D distance sensor
 */
class DistanceSensor1D : public HWEntity
{
public:
	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<DistanceSensor1D> Ptr;

	DistanceSensor1D(const std::string& identifier);


    rw::kinematics::Frame* getSensorFrame();
    
    /**
     * @brief Connect to the device
     */
	virtual void sensorOn() = 0;
	virtual void sensorOff() = 0;

	/**
	 * @brief returns the last measured distance from the sensopr
     * 
     * If the distance is outside the range of the sensor (below or above) getDistance will return a negative value
	 */
	virtual double getDistance() = 0;

    /**
     * @brief Returns pair containing the minimum and maximum distance of the sensor
     */
    virtual std::pair<double, double> getRange() const = 0;

    /**
     * @brief Set the calibration filename
     * @param calibrationFileName
     */
    void setCalibrationFileName(const std::string& calibrationFileName);

    /**
     * @brief Get the calibration filename
     * @return calibrationFileName
     */
    std::string getCalibrationFileName() const;



	class DistanceSensor1DUpdateRule : public er_common::System::UpdateRule
	{
	public:
		DistanceSensor1DUpdateRule(DistanceSensor1D::Ptr sensor) :
			er_common::System::UpdateRule(),
			_sensor(sensor)
		{
		}

		void update(rw::kinematics::State& state)
		{
			//ER_LOG_DEBUG("DistanceSensor1DUpdateRule::update " );
			//_sensor->getDistance();
			//ER_LOG_DEBUG("DistanceSensor1D Distance = " << dist );
			//RW_LOG_INFO(_sensor->getName() << " = " << dist);
		}

		er_common::Entity::Ptr getEntity() {
			return _sensor;
		}

	private:
		rw::models::Device::Ptr _device;
		DistanceSensor1D::Ptr _sensor;
	};

	void addUpdateRule(er_common::System::Ptr system)
	{
		system->addUpdateRule(rw::common::ownedPtr(new DistanceSensor1DUpdateRule(this)));
	}

    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

protected:
    rw::kinematics::Frame* _sensorFrame;
    std::string _calibrationFile;
};

} //end namespace

#endif //#ifndef HWL_DISTANCESENSOR1D_HPP
