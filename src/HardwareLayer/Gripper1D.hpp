/* */
#ifndef HWL_GRIPPER1D_HPP
#define HWL_GRIPPER1D_HPP

#include <Common/HWEntity.hpp>
#include <rw/common/Ptr.hpp>

namespace hwl {

/** 
 * Interface for a gripper with 1 degrees of freedom
 */
class Gripper1D: public HWEntity
{
public:
	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<Gripper1D> Ptr;

    /**
    * @brief Construct gripper with 1 degrees of freedom with a given identifier and optionally name of the device models to use
    * @param identifier [in] Identifier use to identify the entity
    * @param deviceName [in] Name of the RobWork Device used for virtual representation of the gripper
    */
    Gripper1D(const std::string& identifier, const std::string& deviceName = "");

    /**
     * @brief Move gripper to home configuration
     */
    virtual void home() = 0;

    /**
     * @brief Set the power with which to grasp.
     * The power is set in percentage of full power [0;100]
     * @param pct [in] Percenter of power
     */
    virtual void setGraspPower(double pct) = 0;

    /**
     * @brief Open gripper
     */
    virtual void open() = 0;

    /**
     * @brief Close gripper
     */
    virtual void close() = 0;

    /**
     * @brief Move gripper to the specified position    
     * @param pos [in] Target position of the gripper
     */
    virtual void setPosition(double pos) = 0;

    /**
     * @brief Returns the position of the gripper
     * @return Current position of the gripper
     */
    virtual double getPosition() const = 0;

    /**
     * @brief Returns the positional bounds for the gripper
     * @return pair containing lower and upper bound of the gripper positions.
     */
    virtual std::pair<double, double> getBounds() const = 0;


    /**
    * @copydoc HWEntity::addUpdateRule
    */
    virtual void addUpdateRule(er_common::System::Ptr system);


    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

protected:
    virtual rw::models::Device::Ptr getDeviceModel() const;
private:
    std::string _deviceName;
    mutable rw::models::Device::Ptr _device;



    /**
    * Update rule for Gripper1D
    */
    class Gripper1DUpdateRule : public er_common::System::UpdateRule
    {
    public:
        Gripper1DUpdateRule(Gripper1D::Ptr gripper) :
            er_common::System::UpdateRule(),
            _gripper(gripper)
        {
        }

        ~Gripper1DUpdateRule()
        {
        }

        void update(rw::kinematics::State& state)
        {
            //std::cout << "Gripper Update "<< _gripper->getDeviceModel() << std::endl;
            try {
                double p = _gripper->getPosition();
                rw::math::Q q(1);
                q(0) = p;
                _gripper->getDeviceModel()->setQ(q, state);
                // _rwDevice->setQ(q, state);
            }
            catch (const std::exception&) {
            }
        }

        er_common::Entity::Ptr getEntity() {
            return _gripper;
        }


    private:
        Gripper1D::Ptr _gripper;
        //rw::models::Device::Ptr _rwDevice;
    };


};

} //end namespace

#endif //#ifndef HWL_GRIPPER1D_HPP
