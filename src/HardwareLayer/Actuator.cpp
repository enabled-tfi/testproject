#include "Actuator.hpp"

using namespace hwl;
using namespace er_common;
using namespace rw::common;

Actuator::Actuator(const std::string& identifier, const std::string& deviceName):
	HWEntity(identifier),
    _deviceName(deviceName)
{

}


/**
* @copydoc HWEntity::addUpdateRule
*/
void Actuator::addUpdateRule(er_common::System::Ptr system)
{
    if (getDeviceModel() != NULL) {
        system->addUpdateRule(ownedPtr(new Actuator::ActuatorUpdateRule(this)));
    }
}


rw::models::Device::Ptr Actuator::getDeviceModel() const
{
    if (_device != NULL)
        return _device;
    _device = System::getInstance()->getWorkCell()->findDevice(_deviceName);
    return _device;
}

/* Methods from the serializable interface */
void Actuator::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    HWEntity::read(iarchive, id);
    if (iarchive.has("DeviceName")) {
        _deviceName = iarchive.read<std::string>("DeviceName");
    }
}

void Actuator::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    HWEntity::write(oarchive, id);
    if (_deviceName != "") {
        oarchive.write(_deviceName, "DeviceName");
    }
}