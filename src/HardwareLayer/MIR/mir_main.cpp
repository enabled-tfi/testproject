

#include <iostream>
#include <rw/common/Log.hpp>
#include "MIR2.hpp"
#include <Serialization/OutputArchiveXML.hpp>
#include <Serialization/InputArchiveXML.hpp>

using namespace hwl;
using namespace er_serialization;
using namespace rw::math;

int main(int argc, char** argv)
{
    try {
        rw::common::Log::log().setLogIndexMask(rw::common::Log::AllMask);
        rw::common::Log::log().setWriterForMask(rw::common::Log::AllMask, ownedPtr(new rw::common::LogStreamWriter(&std::cout)));

        std::cout << "HELLO MIR" << std::endl;                                                
        /*MIR2 mir("http://192.168.126.128/api/v2.0.0", "Basic RGlzdHJpYnV0b3I6NjJmMmYwZjFlZmYxMGQzMTUyYzk1ZjZmMDU5NjU3NmU0ODJiYjhlNDQ4MDY0MzNmNGNmOTI5NzkyODM0YjAxNA==");
        OutputArchiveXML outarchive("MyMIR2.xml");        
        mir.write(outarchive, "MIR2");
        outarchive.finalize();*/
        MIR2 mir;
        InputArchiveXML inarchive("MyMIR2.xml");
        inarchive.initialize();
        mir.read(inarchive, "MIR2");

        std::cout << "Constructed" << std::endl;
        mir.connect();
        
       /* int cnt = 0;
        while (cnt < 1000) {
            mir.vel(1, 1);
            std::cout << "cnt= "<<cnt++ << std::endl;
        }
        return 0;*/
        //
        std::vector<MobileDevice::DefinedPosition> pos = mir.getDefinedPositions();
        for (MobileDevice::DefinedPosition p : pos) {
            std::cout << "Position " << p.displayName << " " << p.uid << std::endl;
        }
        /*
        mir.moveTo(pos.front().uid);
        mir.start();

        std::cout << "Configure a target position coordinates " << std::endl;
        Pose2D<> target(Vector2D<>(-3, 2), 90 * Deg2Rad);
        mir.moveTo(target, 0.2);    
        std::cout << "Initialized" << std::endl;
        mir.start();
        std::cout << "Start finished " << std::endl;

        while (mir.isMoving()) {
            std::cout << "Moving Moving Moving " << std::endl;
        }
        mir.waitForLastMissionsToFinish();
        std::cout << "All missions finished " << std::endl;
*/
        //int cnt = 0;
        //while (cnt < 1000) {
        //    mir.vel(1, 1);
        //    std::cout << cnt++ << std::endl;
        //}
        //mir.waitForMissionToFinish(id);
        std::cout << "Pose: ";
        Pose2D<>::print(mir.getCurrentPose());
        /*
        char ch;
        do {
            std::cin >> ch;
            std::cout << "Got = " << ch << std::endl;
            const double dr = 0.2;
            const double dp = 0.2;
            if (ch == 'a') {
                mir.vel(0, dr);
            }
            else if (ch == 'd') {
                mir.vel(0, -dr);
            }
            else if (ch == 'w') {
                mir.vel(dp, 0);
            }
            else if (ch == 's') {
                mir.vel(-dp, 0);
            }
            else if (ch == 't') {
                ER_LOG_DEBUG("x:");
                double x, y, angle;
                std::cin >> x;
                ER_LOG_DEBUG("y:");
                std::cin >> y;
                ER_LOG_DEBUG("angle:");
                std::cin >> angle;
                Transform2D<> target(Vector2D<>(x, y), Rotation2D<>(angle));
                mir.moveTo(target, 0.1);
            }
            else if (ch == 'p') {
                ER_LOG_DEBUG("Position:");
                std::string identifier;
                std::cin >> identifier;
                mir.moveTo(identifier);
            }
            else if (ch == 'u') {
                Pose2D<> pose = mir.getCurrentPose();
                ER_LOG_DEBUG("Pose = " << pose.x() << " " << pose.y() << " " << pose.theta());
            }
        } while (ch != 'x');
        */
        ER_LOG_DEBUG("Command called");
        mir.disconnect();
    }
    catch (const std::exception& exp) {
        std::cout << "Exception: " << exp.what() << std::endl;
    }
    return 0;
}
