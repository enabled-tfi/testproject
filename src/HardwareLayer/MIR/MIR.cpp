#include "MIR.hpp"
#include <rw/common/TimerUtil.hpp>

#include <Common/InputArchive.hpp>
#include <Common/OutputArchive.hpp>


using namespace hwl;
using namespace rw::math;
using namespace rw::common;

using namespace web::http;
using namespace web::http::client;


MIR::MIR():
        MobileDevice("MIR")
{

}

MIR::MIR(const std::string& host) :
	MobileDevice("MIR"),
	_host(host)
{

}


namespace {
    bool connected = false;
}



void MIR::connect()
{
    utility::string_t whost(_host.begin(), _host.end());
    utility::string_t httpAddress = U("http://")+whost;
    httpAddress.append(U(":8000"));
    _restURI = web::http::uri(httpAddress);
    ER_LOG_DEBUG("a");

    utility::string_t wsAddress = U("ws://")+whost;
    wsAddress.append(U(":9090"));
    web::uri uri = web::uri(wsAddress);
    ER_LOG_DEBUG("c");
    _wsClient.connect(uri).then([](){ ER_LOG_DEBUG("Finished Connecting"); connected = true; });
    ER_LOG_DEBUG("b");
    while (!connected) {
		ER_LOG_DEBUG("." );
        TimerUtil::sleepUs(100*1000);
    }
    ER_LOG_DEBUG("e");
	
    std::stringstream sstr;
    sstr<<"{\"op\":\"advertise\",\"id\":\"advertise:/cmd_vel:1\",\"type\":\"geometry_msgs/Twist\",\"topic\":\"/cmd_vel\",\"latch\":false}";
    ER_LOG_DEBUG("c");
    web::websockets::client::websocket_outgoing_message msg;
    msg.set_utf8_message(sstr.str());
    ER_LOG_DEBUG("d");
    _wsClient.send(msg);
    ER_LOG_DEBUG("e");
	
     return;

}


void MIR::disconnect() {
    _wsClient.close();
}

/**
 * @copydoc MobileDevice::vel
 */
void MIR::vel(double lin, double rot)
{
    std::stringstream sstr;
    //websocket_write_back(_wstools.wsi, cmd);

    web::json::value payload = web::json::value::object();
    payload[U("op")] = web::json::value::string(U("publish"));
    payload[U("id")] = web::json::value::string(U("publish:/cmd_vel:2"));
    payload[U("topic")] = web::json::value::string(U("/cmd_vel"));
    web::json::value msg = web::json::value::object();
    web::json::value linear = web::json::value::object();
    linear[U("x")] = web::json::value::number(lin);
    linear[U("y")] = web::json::value::number(0);
    linear[U("z")] = web::json::value::number(0);
    msg[U("linear")] = linear;
    web::json::value angular = web::json::value::object();
    angular[U("x")] = web::json::value::number(0);
    angular[U("y")] = web::json::value::number(0);
    angular[U("z")] = web::json::value::number(rot);
    msg[U("angular")] = angular;
    payload[U("msg")] = msg;
    payload[U("latch")] = web::json::value::boolean(false);


    web::websockets::client::websocket_outgoing_message wsmsg;
    utility::string_t msg_ustring = payload.serialize();
    const std::string msg_string(msg_ustring.begin(), msg_ustring.end());
    wsmsg.set_utf8_message(msg_string);
  //  ER_LOG_DEBUG("payload = "<<payload.serialize());
    _wsClient.send(wsmsg);
}



int MIR::extractMissionId(const http_response &response)
{
 //   std::cout <<"Extract response: "<< response.to_string() );

    web::json::value json =  response.extract_json().get();
    //std::cout <<"JSON: "<< json );
    web::json::value idvalue = json[U("id")];
    //ER_LOG_DEBUG("idvalue = "<<idvalue);
    utility::string_t idval = idvalue.as_string();
    //ER_LOG_DEBUG("idval = "<<idval);
    return std::stoi(idval.c_str());
}


void MIR::start()
{
    http_client client(uri_builder(_restURI).append_path(U("/json/robot")).to_uri());

    web::json::value payload = web::json::value::object();
    payload[U("command")] = web::json::value::string(U("continue"));
    client.request(methods::POST, U(""), payload).get();
}


void MIR::pause()
{
    http_client client(uri_builder(_restURI).append_path(U("/json/robot")).to_uri());

    web::json::value payload = web::json::value::object();
    payload[U("command")] = web::json::value::string(U("pause"));
    client.request(methods::POST, U(""), payload).get();
    //checkResponse();
}


/**
 * @copydoc MobileDevice::moveTo
 */

void MIR::moveTo(const rw::math::Pose2D<>& target, double goalThreshold)
{
    http_client client(uri_builder(_restURI).append_path(U("/json/mission")).to_uri());

    web::json::value payload = web::json::value::object();
    payload[U("type")] = web::json::value::string(U("taxa"));
    payload[U("x")] = web::json::value::number(target.x());
    payload[U("y")] = web::json::value::number(target.y());
    double orientation = target.theta();
    payload[U("orientation")] = web::json::value::number(orientation*Rad2Deg);
    payload[U("retries")] = web::json::value::number(10);
    payload[U("goal_threshold")] = web::json::value::number(goalThreshold);

    int missionId = extractMissionId(client.request(methods::POST, U(""), payload).get());

    while (isMissionDone(missionId) == false) {
        ER_LOG_DEBUG("Waiting for Mission "<<missionId<<"to finish ");
        TimerUtil::sleepUs(1000*1000);
    }


    if (getState() == "Pause") {
        start();
    }
    //return missionId;
}

void MIR::moveTo(const std::string& identifier)
{
    http_client client(uri_builder(_restURI).append_path(U("/json/mission")).to_uri());

    web::json::value payload = web::json::value::object();
    payload[U("type")] = web::json::value::string(U("taxa"));
    payload[U("name")] = web::json::value::string(utility::string_t(identifier.begin(), identifier.end()));

    int missionId = extractMissionId(client.request(methods::POST, U(""), payload).get());

    while (isMissionDone(missionId) == false) {
        ER_LOG_DEBUG("Waiting for Mission "<<missionId<<"to finish ");
        TimerUtil::sleepUs(1000*1000);
    }


    if (getState() == "Pause") {
        start();
    }

}

/**
 * @copydoc MobileDevice::getCurrentTransform
 */
rw::math::Pose2D<> MIR::getCurrentPose() const
{
    http_client client(uri_builder(_restURI).append_path(U("/json/robot/position")).to_uri());


    http_response response = client.request(methods::GET, U("")).get();
    //web::json::value jsonstate = response.extract_json().get();

    web::json::value jsonposition = response.extract_json().get()[U("position")];
    //ER_LOG_DEBUG("State = "<<jsonposition);
    //ER_LOG_DEBUG("x = "<<jsonposition["x"];
    double x = std::stod(jsonposition[U("x")].as_string());
    double y = std::stod(jsonposition[U("y")].as_string());
    double theta = std::stod(jsonposition[U("theta")].as_string()) * Deg2Rad;

    /*if (jsonposition["x"].is_double()) {
        ER_LOG_DEBUG("Is double");
        x = jsonposition["x"].as_double();
    }
    if (jsonposition["x"].is_number()) {
        ER_LOG_DEBUG("Is number");
        x = jsonposition["x"].as_number().to_double();
    }

    ER_LOG_DEBUG("x = "<<x);
    double y = jsonposition["y"].as_double();
    double theta = jsonposition["theta"].as_double();
*/
    //ER_LOG_DEBUG("Position: "<<x<<" "<<y<<" "<<theta);

    return Pose2D<>(x,y,theta);

}

std::vector<MobileDevice::DefinedPosition> MIR::getDefinedPositions() const
{
    return std::vector<MobileDevice::DefinedPosition>();
}


std::string MIR::getState() const {
    http_client client(uri_builder(_restURI).append_path(U("/json/robot/state")).to_uri());

    http_response response = client.request(methods::GET, U("")).get();
    //web::json::value jsonstate = response.extract_json().get();
    //ER_LOG_DEBUG("State = "<<jsonstate);
    web::json::value jsonstate = response.extract_json().get();//jsonstate["position"];
    //ER_LOG_DEBUG("jsonstate = "<<jsonstate);
    utility::string_t state = jsonstate[U("state")].as_string();
    //ER_LOG_DEBUG("state = "<<state);
    return std::string(state.begin(), state.end());
}

bool MIR::isMissionDone(int id) {
    utility::stringstream_t sstr;
    sstr<<U("/json/mission/id/")<<id;
    http_client client(uri_builder(_restURI).append_path(sstr.str()).to_uri());
    http_response response = client.request(methods::GET, U("")).get();
    web::json::value jsonstate = response.extract_json().get();
    utility::string_t state = jsonstate[U("value")].as_string();

    //ER_LOG_DEBUG("state = "<<state);
    return state == U("Done");
}

 
bool MIR::isMoving() const {
    ER_LOG_DEBUG("State = "<<getState());
    if (getState() == "InTransit") {
        //ER_LOG_DEBUG("IsMoving");
        return true;
    } else {
        //ER_LOG_DEBUG("Is NOT Moving");
        return false;
    }

}

void MIR::setRegister(unsigned int index, unsigned int value)
{
	utility::stringstream_t sstr;
	sstr << U("/json/register/") << index;
	http_client client(uri_builder(_restURI).append_path(sstr.str()).to_uri());

	utility::stringstream_t sstrval;
	sstrval << value;

	web::json::value payload = web::json::value::object();
	payload[U("value")] = web::json::value::string(sstrval.str());
	
//	std::wcout << "Payload = " << payload );
	http_response response = client.request(methods::POST, U(""), payload).get();

	web::json::value jsonstate = response.extract_json().get();
	utility::string_t state = jsonstate[U("success")].as_string();
	if (state != U("true")) {
		RW_THROW("Unable to set register");
	}

	//ER_LOG_DEBUG("state = "<<state);
	//return state == U("true");




}

int MIR::getRegister(unsigned int index)
{
	utility::stringstream_t sstr;
	sstr << U("/json/register/") << index;
	http_client client(uri_builder(_restURI).append_path(sstr.str()).to_uri());

	http_response response = client.request(methods::GET, U("")).get();
	web::json::value jsonstate = response.extract_json().get();//jsonstate["position"];
															   //std::wcout<<"jsonstate = "<<jsonstate);
	return jsonstate[U("value")].as_integer();
	

	//ER_LOG_DEBUG("state = "<<state);
	//return std::string(state.begin(), state.end());

}



/* Methods from the serializable interface */
void MIR::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
	iarchive.readEnterScope("MIR");
	HWEntity::read(iarchive, id);
	_host = iarchive.read<std::string>("IP");

	iarchive.readLeaveScope("MIR");
}

void MIR::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	oarchive.writeEnterScope("MIR");
	HWEntity::write(oarchive, id);
	oarchive.write(_host, "IP");
	oarchive.writeLeaveScope("MIR");
}
