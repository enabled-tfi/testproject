/* */
#ifndef HWL_MIR2_HPP
#define HWL_MIR2_HPP


#include "../MobileDevice.hpp"

#include <rw/common/Ptr.hpp>
#include <rw/math.hpp>

#include <cpprest/http_client.h>
#include <cpprest/ws_client.h>

namespace hwl {
	
/**
 * Interface for MIR robots with software version 2 and higher
 */
class MIR2: public MobileDevice
{
public:
	typedef rw::common::Ptr<MIR2> Ptr;

    /** 
     * @brief Default constructor. To be used when reading in from file
     */
	MIR2();

    /**
     * @brief Constructrs MIR2 with giving host and authorization code
     * @param host [in] URL for the REST interface on the MIR. Example http://192.168.126.128/api/v2.0.0
     * @param authorizationCode [in] Code to authorize the connection. Example Basic RGlzdHJpYnV0b3I6NjJmMmYwZjFlZmYxMGQzMTUyYzk1ZjZmMDU5NjU3NmU0ODJiYjhlNDQ4MDY0MzNmNGNmOTI5NzkyODM0YjAxNA==
     */
	MIR2(const std::string& url, const std::string& authorizationCode);

    /**
     * @copydoc MobileDevice::connect
     */
	virtual void connect();

    /**
    * @copydoc MobileDevice::disconnect
    */
    virtual void disconnect();

    /**
     * @copydoc MobileDevice::vel
     */
	virtual void vel(double lin, double rot);

    /**
     * @brief Start execution on the MIR
     */
	void start();

    /**
    * @brief Pause execution on the MIR
    */
    void pause();

    /**
     * @copydoc MobileDevice::moveTo
     */
    virtual void moveTo(const rw::math::Pose2D<>& target, double goalThreshold);

    /**
     * @copydoc MobileDevice::moveTo
     */
    virtual void moveTo(const std::string& identifier);

    /**
     * @brief Wait for mission with specific ID to finish
     * @param id [in] Mission id to wait for
     */
    virtual void waitForMissionToFinish(int id);

    /**
     * @brief Wait for the last mission currently in the queue to finish
     */
    virtual void waitForLastMissionsToFinish();

    /**
     * @brief Struct representing positions defined in the MIR
     */
    /*struct Position {
        std::string name;
        std::string id;
        Position(const std::string& name, const std::string& id):
            name(name),
            id(id)
        {}
    };*/

    /**
     * @brief Returns list with all available positions
     */
//    std::vector<Position> getPositions() const;

    /**
     * @copydoc MobileDevice::getCurrentPose
     */
    virtual rw::math::Pose2D<> getCurrentPose() const;


    /**
     * @copydoc MobileDevice::getDefinedPosition
     */
    std::vector<MobileDevice::DefinedPosition> getDefinedPositions() const;


    /**
     * @copydoc MobileDevice::isMoving
     */
    virtual bool isMoving() const;

    /**
     * @brief Returns true if the specified mission has finished
     * @param id [in] Id of the mission
     */
    virtual bool isMissionDone(int id) const;

    /**
     * @brief Sets register on the MIR
     * 
     * CURRENTLY NOT IMPLEMENTED
     */
	void setRegister(unsigned int index, unsigned int value);

    /**
    * @brief Gets register on the MIR
    *
    * CURRENTLY NOT IMPLEMENTED
    */
	int getRegister(unsigned int index);

    /**     
     * @brief Returns if of the last mission send to the MIR
     */
    int getLastMissionId() const;

	/* Methods from the serializable interface */
    /**
     * @copydoc Serialization::read
     */
	virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    /**
     * @copydoc Serialization::write
     */
    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


private:    
	std::string _host;
    utility::string_t _authorizationCode;
    web::http::uri _restURI;

    int _lastMissionId;
    web::websockets::client::websocket_client _wsClient;

    void sendToMissionQueue(const utility::string_t& uid);
    int extractMissionId(const web::http::http_response &response);
    web::http::http_request generateRequest() const;
    void checkResponse(const web::http::http_response &response, const std::string& caller, int acceptValue) const;
    //std::string getState() const;
    
    std::string getSessionId();
    std::string getUserId();
    void initializeDefaultMissions();


};

} //end namespace

#endif //#ifndef HWL_MIR2_HPP
