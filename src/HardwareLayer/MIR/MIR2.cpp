#include "MIR2.hpp"
#include <rw/common/TimerUtil.hpp>

#include <Common/InputArchive.hpp>
#include <Common/OutputArchive.hpp>


using namespace hwl;
using namespace rw::math;
using namespace rw::common;

using namespace web::http;
using namespace web::http::client;


MIR2::MIR2():
        MobileDevice("MIR2")
{
   // _authorizationCode = U("Basic RGlzdHJpYnV0b3I6NjJmMmYwZjFlZmYxMGQzMTUyYzk1ZjZmMDU5NjU3NmU0ODJiYjhlNDQ4MDY0MzNmNGNmOTI5NzkyODM0YjAxNA==");    
}

MIR2::MIR2(const std::string& host, const std::string& authorizationCode):
	MobileDevice("MIR2"),
	_host(host),
    _authorizationCode(authorizationCode.begin(), authorizationCode.end())
{    
    //std::string ac = "Basic RGlzdHJpYnV0b3I6NjJmMmYwZjFlZmYxMGQzMTUyYzk1ZjZmMDU5NjU3NmU0ODJiYjhlNDQ4MDY0MzNmNGNmOTI5NzkyODM0YjAxNA==";
    //_authorizationCode = utility::string_t(authorizationCode.begin(), authorizationCode.end());
    //_authorizationCode = U("Basic RGlzdHJpYnV0b3I6NjJmMmYwZjFlZmYxMGQzMTUyYzk1ZjZmMDU5NjU3NmU0ODJiYjhlNDQ4MDY0MzNmNGNmOTI5NzkyODM0YjAxNA==");
    //_authorizationCode = utility::string_t();
        //U("Basic RGlzdHJpYnV0b3I6NjJmMmYwZjFlZmYxMGQzMTUyYzk1ZjZmMDU5NjU3NmU0ODJiYjhlNDQ4MDY0MzNmNGNmOTI5NzkyODM0YjAxNA==");
}



void MIR2::connect()
{
    std::cout<<"MIR2::connect "<< _host<<std::endl;
    ER_LOG_DEBUG("MIR2::connect "<< _host);
    utility::string_t ip(_host.begin(), _host.end());
    utility::string_t httpAddress = U("http://") + ip +U("/api/v2.0.0");
    
    _restURI = web::http::uri(httpAddress);
    ER_LOG_DEBUG("a");


    utility::string_t wsAddress = U("ws://")+ip+U(":9090");
    web::uri uri = web::uri(wsAddress);
    ER_LOG_DEBUG("c");
    //_wsClient.connect(uri).then([]() { ER_LOG_DEBUG("Finished Connecting"); connected = true; });
    _wsClient.connect(uri).wait();
    ER_LOG_DEBUG("finshed ");
    /*while (!connected) {
        ER_LOG_DEBUG(".");
        TimerUtil::sleepUs(100 * 1000);
    }
    ER_LOG_DEBUG("e");
*/
  /*  std::stringstream sstr;
    sstr << "{\"op\":\"advertise\",\"id\":\"advertise:/cmd_vel:1\",\"type\":\"geometry_msgs/Twist\",\"topic\":\"/cmd_vel\",\"latch\":false}";
    ER_LOG_DEBUG("c");
    web::websockets::client::websocket_outgoing_message msg;
    msg.set_utf8_message(sstr.str());
    ER_LOG_DEBUG("d");
    _wsClient.send(msg);
    ER_LOG_DEBUG("e");
    */
    initializeDefaultMissions();
    return;


}

std::string MIR2::getSessionId() {
    try {
        http_client client(uri_builder(_restURI).append_path(U("/status")).to_uri());
        http_request request = generateRequest();
        request.set_method(methods::GET);
        utility::string_t strRequest = request.to_string();
        http_response response = client.request(request).get();

        if (response.status_code() != 200) {
            std::stringstream sstr;
            utility::string_t rstr = response.to_string();
            sstr << "Failed to query robot with message " << std::string(rstr.begin(), rstr.end()) << std::endl;
            RW_THROW(sstr.str());
        }
        web::json::value jsonSessionId = response.extract_json().get()[U("session_id")];
        std::string sessionId = jsonSessionId.as_string();
        std::cout<<"Session Id "<<sessionId<<std::endl;
        return sessionId;
    }
    catch (std::exception& exp) {
        RW_THROW("Exception trying to retrieve Session Id: "<<exp.what()<< "  "<<_host);
    }
}

std::string MIR2::getUserId() {
    try {
        http_client client(uri_builder(_restURI).append_path(U("/users/me")).to_uri());
        http_request request = generateRequest();
        request.set_method(methods::GET);
        utility::string_t strRequest = request.to_string();
        http_response response = client.request(request).get();

        if (response.status_code() != 200) {
            std::stringstream sstr;
            utility::string_t rstr = response.to_string();
            sstr << "Failed to query robot with message " << std::string(rstr.begin(), rstr.end()) << std::endl;
            RW_THROW(sstr.str());
        }
        web::json::value jsonUserId = response.extract_json().get()[U("guid")];
        std::string userId = jsonUserId.as_string();
        std::cout<<"User Id "<<userId<<std::endl;

        return userId;
    }
    catch (std::exception& exp) {
        RW_THROW("Exception trying to retrieve User Id: "<<exp.what()<< "  "<<_host);
    }
}


namespace {

    const std::string moveToCoordinatesMissionName = "ER-MoveToCoordinatesMission000000002";
    const std::string moveToCoordinatesActionName = "ER-MoveToCoordinatesAction0000000002";
}

void MIR2::initializeDefaultMissions() {
    //Create mission for going to coordinates
    /*{
      "guid": "MyMission5cff-11e9-9bb2-94c691a73b86",
      "name": "MyMission",
      "description": "This is my mission",
      "hidden": true,
      "group_id": "mirconst-guid-0000-0011-missiongroup",
      "session_id": "3f8e666d-2ead-11e9-97f4-94c691a73b86",
      "created_by_id": "mirconst-guid-0000-0004-users0000000"
    }*/
   //curl -X POST "http://mir.com/api/v2.0.0/missions" -H "accept: application/json" -H "Authorization: Basic ZGlzdHJpYnV0b3I6NjJmMmYwZjFlZmYxMGQzMTUyYzk1ZjZmMDU5NjU3NmU0ODJiYjhlNDQ4MDY0MzNmNGNmOTI5NzkyODM0YjAxNA==" -H "Accept-Language: en_US" -H "Content-Type: application/json" -d "{ \"guid\": \"MyMission5cff-11e9-9bb2-94c691a73b86\", \"name\": \"MyMission\", \"description\": \"This is my mission\", \"hidden\": true, \"group_id\": \"mirconst-guid-0000-0011-missiongroup\", \"session_id\": \"3f8e666d-2ead-11e9-97f4-94c691a73b86\", \"created_by_id\": \"mirconst-guid-0000-0004-users0000000\"}"


    //TODO Request name of all missions to check if it already exists
    try
    {
        http_client client(uri_builder(_restURI).append_path(U("/missions")).to_uri());
        http_request request = generateRequest();

        web::json::value payload = web::json::value::object();
        payload[U("guid")] = web::json::value::string(U(moveToCoordinatesMissionName));
        payload[U("name")] = web::json::value::string(U("ER MoveToCoordinates Mission"));
        payload[U("description")] = web::json::value::string(U("ER MoveToCoordinates Mission"));
        payload[U("hidden")] = web::json::value::boolean(false);
        payload[U("group_id")] = web::json::value::string(U("mirconst-guid-0000-0011-missiongroup"));
        payload[U("session_id")] = web::json::value::string(U(getSessionId()));
        payload[U("created_by_id")] = web::json::value::string(U(getUserId()));

        request.set_body(payload);
        request.set_method(methods::POST);

        http_response response = client.request(request).get();
        checkResponse(response, "Create Move To Coordinates Mission", 409 /*Ok if entry already exists */);
    } catch (const std::exception& exp) {
        std::cout<<"Failed to create mission"<<exp.what()<<std::endl;
    }

    //Create mission for going to defined position
     /* {
      "guid": "MyAction0-cff-11e9-9bb2-94c691a73b86",
      "action_type": "move_to_position",
      "mission_id": "MyMission5cff-11e9-9bb2-94c691a73b86",
      "priority": 0,
      "parameters": [
        {"id": "distance_threshold", "value": 0.1},
        {"id": "orientation", "value": 0},
        {"id": "x", "value": 0.1},
        {"id": "y", "value": 0.1},
        {"id": "retries", "value": 12}
      ]
        }
     */
    //curl -X POST "http://mir.com/api/v2.0.0/missions/MyMission5cff-11e9-9bb2-94c691a73b86/actions" -H "accept: application/json" -H "Authorization: Basic ZGlzdHJpYnV0b3I6NjJmMmYwZjFlZmYxMGQzMTUyYzk1ZjZmMDU5NjU3NmU0ODJiYjhlNDQ4MDY0MzNmNGNmOTI5NzkyODM0YjAxNA==" -H "Accept-Language: en_US" -H "Content-Type: application/json" -d "{ \"guid\": \"MyAction0-cff-11e9-9bb2-94c691a73b86\", \"action_type\": \"move_to_position\", \"mission_id\": \"MyMission5cff-11e9-9bb2-94c691a73b86\", \"priority\": 0, \"parameters\": [ {\"id\": \"distance_threshold\", \"value\": 0.1}, {\"id\": \"orientation\", \"value\": 0}, {\"id\": \"x\", \"value\": 0.1}, {\"id\": \"y\", \"value\": 0.1}, {\"id\": \"retries\", \"value\": 12} ]}"
    try
    {
        http_client client(uri_builder(_restURI).append_path(U("/missions/"+moveToCoordinatesMissionName+"/actions")).to_uri());
        http_request request = generateRequest();

        web::json::value payload = web::json::value::object();
        payload[U("guid")] = web::json::value::string(U(moveToCoordinatesActionName));
        payload[U("action_type")] = web::json::value::string(U("move_to_position"));
        payload[U("mission_id")] = web::json::value::string(U(moveToCoordinatesMissionName));
        payload[U("priority")] = web::json::value::number(0);

        web::json::value parameters = web::json::value::array();
        parameters[0] = web::json::value::object();
        parameters[0][U("id")] = web::json::value::string(U("x"));
        parameters[0][U("value")] = web::json::value::number(0);
        parameters[1] = web::json::value::object();
        parameters[1][U("id")] = web::json::value::string(U("y"));
        parameters[1][U("value")] = web::json::value::number(0);
        parameters[2] = web::json::value::object();
        parameters[2][U("id")] = web::json::value::string(U("orientation"));
        parameters[2][U("value")] = web::json::value::number(0);
        parameters[3] = web::json::value::object();
        parameters[3][U("id")] = web::json::value::string(U("distance_threshold"));
        parameters[3][U("value")] = web::json::value::number(0.1);
        parameters[4] = web::json::value::object();
        parameters[4][U("id")] = web::json::value::string(U("retries"));
        parameters[4][U("value")] = web::json::value::number(10);

        payload[U("parameters")] = parameters;
        request.set_body(payload);
        request.set_method(methods::POST);

        http_response response = client.request(request).get();
        checkResponse(response, "Create Action for Move To Coordinates Mission" , 409 /*Ok if entry already exists */);
    } catch (const std::exception& exp) {
        std::cout<<"Failed to create action"<<exp.what()<<std::endl;
    }

}


void MIR2::disconnect() {
    //Clean up by removing missions added

    {
        http_client client(uri_builder(_restURI).append_path(U("/missions/"+moveToCoordinatesMissionName+"/actions/"+moveToCoordinatesActionName)).to_uri());
        http_request request = generateRequest();
        request.set_method("DELETE");

        http_response response = client.request(request).get();
        checkResponse(response, "Remove Action for Move To Coordinates Mission" , 204 /*Return code for item removed*/);
    }
    {
        http_client client(uri_builder(_restURI).append_path(U("/missions/"+moveToCoordinatesMissionName)).to_uri());
        http_request request = generateRequest();
        request.set_method("DELETE");

        http_response response = client.request(request).get();
        checkResponse(response, "Remove Action for Move To Coordinates Mission" , 204 /*Return code for item removed*/);
    }
    _wsClient.close();
}

/**
 * @copydoc MobileDevice::vel
 */
void MIR2::vel(double lin, double rot)
{
    std::stringstream sstr;
    //websocket_write_back(_wstools.wsi, cmd);

    web::json::value payload = web::json::value::object();
    payload[U("op")] = web::json::value::string(U("publish"));
    payload[U("id")] = web::json::value::string(U("publish:/cmd_vel:2"));
    payload[U("topic")] = web::json::value::string(U("/cmd_vel"));
    web::json::value msg = web::json::value::object();
    web::json::value linear = web::json::value::object();
    linear[U("x")] = web::json::value::number(lin);
    linear[U("y")] = web::json::value::number(0);
    linear[U("z")] = web::json::value::number(0);
    msg[U("linear")] = linear;
    web::json::value angular = web::json::value::object();
    angular[U("x")] = web::json::value::number(0);
    angular[U("y")] = web::json::value::number(0);
    angular[U("z")] = web::json::value::number(rot);
    msg[U("angular")] = angular;
    payload[U("msg")] = msg;
    payload[U("latch")] = web::json::value::boolean(false);


    web::websockets::client::websocket_outgoing_message wsmsg;
    utility::string_t msg_ustring = payload.serialize();
    const std::string msg_string(msg_ustring.begin(), msg_ustring.end());
    wsmsg.set_utf8_message(msg_string);
    _wsClient.send(wsmsg);
}



int MIR2::extractMissionId(const http_response &response)
{
    web::json::value json =  response.extract_json().get();
    web::json::value idvalue = json[U("id")];
    int intval = idvalue.as_number().to_int32();
    return intval;
}


void MIR2::start()
{
    std::cout << "Start " << std::endl;
    http_client client(uri_builder(_restURI).append_path(U("/status")).to_uri());
    web::json::value payload = web::json::value::object();
    payload[U("state_id")] = web::json::value::number(3);
    http_request request;
    request.headers().add(U("Authorization"), _authorizationCode);
    request.headers().add(U("Accept-Language"), U("en_US"));    
    request.set_body(payload);
    request.set_method(methods::PUT);

    http_response response = client.request(request).get();   
    checkResponse(response, "Start mission", 200 /*PUT ok*/);
}


void MIR2::pause()
{
    RW_THROW("Not implemented");
    /*
    http_client client(uri_builder(_restURI).append_path(U("/json/robot")).to_uri());

    web::json::value payload = web::json::value::object();
    payload[U("command")] = web::json::value::string(U("pause"));
    http_response response = client.request(methods::POST, U(""), payload).get();
    
    checkResponse(response,"Pause mission", );*/
}

void MIR2::checkResponse(const web::http::http_response &response, const std::string& caller, int acceptValue) const {
    const short code = response.status_code();
    if (code != acceptValue) {
        std::stringstream sstr;
        utility::string_t rstr = response.to_string();
        sstr <<caller<<" failed with message " << std::string(rstr.begin(), rstr.end()) << std::endl;
        RW_THROW(sstr.str());
    }
}


void MIR2::moveTo(const rw::math::Pose2D<>& target, double goalThreshold)
{
    //Configure the GoToPositionPrototype action
    //curl - X PUT "http://192.168.126.128/api/v2.0.0/missions/mirconst-guid-0000-0003-actionlist00/actions/mirconst-guid-0000-0003-actlistcont0" 
    //- H "accept: application/json" - H "Authorization: Basic RGlzdHJpYnV0b3I6NjJmMmYwZjFlZmYxMGQzMTUyYzk1ZjZmMDU5NjU3NmU0ODJiYjhlNDQ4MDY0MzNmNGNmOTI5NzkyODM0YjAxNA=="
    //- H "Accept-Language: en_US" - H "Content-Type: application/json" - d "{ \"scope_reference\": null, \"priority\": 0, \"parameters\": [ {\"id\": \"x\", \"value\": 11} ]}"

    http_client client(uri_builder(_restURI).append_path(U("/missions/"+moveToCoordinatesMissionName+"/actions/"+moveToCoordinatesActionName)).to_uri());
    http_request request = generateRequest();
    /*request.headers().add(U("Authorization"), _authorizationCode);
    request.headers().add(U("Accept-Language"), U("en_US")); */

    web::json::value payload = web::json::value::object();
    payload[U("priority")] = web::json::value::number(0);
    web::json::value parameters = web::json::value::array();
    parameters[0] = web::json::value::object();
    parameters[0][U("id")] = web::json::value::string(U("x"));
    parameters[0][U("value")] = web::json::value::number(target.x());
    parameters[1] = web::json::value::object();
    parameters[1][U("id")] = web::json::value::string(U("y"));
    parameters[1][U("value")] = web::json::value::number(target.y());
    parameters[2] = web::json::value::object();
    parameters[2][U("id")] = web::json::value::string(U("orientation"));
    parameters[2][U("value")] = web::json::value::number(Rad2Deg*target.theta());
    payload[U("parameters")] = parameters;
    request.set_body(payload);
    request.set_method(methods::PUT);

    http_response response = client.request(request).get();
    checkResponse(response, "Set position", 200 /* PUT Accept */);

    sendToMissionQueue(U(moveToCoordinatesMissionName));
}

void MIR2::sendToMissionQueue(const utility::string_t& uid)
{
    http_client client(uri_builder(_restURI).append_path(U("/mission_queue")).to_uri());
    http_request request = generateRequest();
    web::json::value payload = web::json::value::object();
    payload[U("mission_id")] = web::json::value::string(uid);
    request.set_body(payload);
    request.set_method(methods::POST);
    http_response response = client.request(request).get();
    checkResponse(response, "Add to mission queue", 201 /* POST Accept */);
    _lastMissionId = extractMissionId(response);
    std::cout << "Mission ID " << _lastMissionId << std::endl;
}

web::http::http_request MIR2::generateRequest() const
{
    http_request request;
    request.headers().add(U("Authorization"), _authorizationCode);
    request.headers().add(U("Accept-Language"), U("en_US"));
    return request;
}

//std::vector<MIR2::Position> MIR2::getPositions() const
std::vector<MobileDevice::DefinedPosition> MIR2::getDefinedPositions() const
{
    http_client client(uri_builder(_restURI).append_path(U("/positions")).to_uri());
    http_request request = generateRequest();
    request.set_method(methods::GET);
    http_response response = client.request(request).get();
    checkResponse(response, "Request positions", 200 );

    web::json::array positions = response.extract_json().get().as_array(); 
    std::vector<DefinedPosition> result;
    for (web::json::value p : positions) {
        utility::string_t name = p[U("name")].as_string();
        utility::string_t id = p[U("guid")].as_string();
        //TODO: Add sending a request to the MIR for the actual position
        result.push_back(DefinedPosition(std::string(name.begin(), name.end()), std::string(id.begin(), id.end()), Pose2D<>()));
    }
    return result;
}



void MIR2::moveTo(const std::string& identifier)
{
    //Configure the GoToPositionPrototype action
    //curl -X PUT "http://192.168.126.128/api/v2.0.0/missions/mirconst-guid-0000-0001-actionlist00/actions/mirconst-guid-0000-0001-actlistcont0" 
    //-H "accept: application/json" -H "Authorization: Basic RGlzdHJpYnV0b3I6NjJmMmYwZjFlZmYxMGQzMTUyYzk1ZjZmMDU5NjU3NmU0ODJiYjhlNDQ4MDY0MzNmNGNmOTI5NzkyODM0YjAxNA=="
    //-H "Accept-Language: en_US" -H "Content-Type: application/json" -d "{ \"priority\": 1, \"parameters\": [ {\"id\": \"position\", \"value\":\"mirconst-guid-0000-0001-positions000\"} ]}"

    http_client client(uri_builder(_restURI).append_path(U("/missions/mirconst-guid-0000-0001-actionlist00/actions/mirconst-guid-0000-0001-actlistcont0")).to_uri());
    http_request request = generateRequest();
    web::json::value payload = web::json::value::object();
    payload[U("priority")] = web::json::value::number(0);
    web::json::value parameters = web::json::value::array();
    parameters[0] = web::json::value::object();
    parameters[0][U("id")] = web::json::value::string(U("position"));
    parameters[0][U("value")] = web::json::value::string(utility::string_t(identifier.begin(), identifier.end())); 
    payload[U("parameters")] = parameters;
    request.set_body(payload);
    request.set_method(methods::PUT);
    http_response response = client.request(request).get();
    checkResponse(response, "Set position", 200);
    sendToMissionQueue(U("mirconst-guid-0000-0001-actionlist00"));
}

/**
 * @copydoc MobileDevice::getCurrentTransform
 */
rw::math::Pose2D<> MIR2::getCurrentPose() const
{
    try {
        http_client client(uri_builder(_restURI).append_path(U("/status")).to_uri());
        http_request request = generateRequest();
        request.set_method(methods::GET);
        utility::string_t strRequest = request.to_string();
        http_response response = client.request(request).get();
        checkResponse(response, "Get Position", 200);
        /*if (response.status_code() != 200) {
            std::stringstream sstr;
            utility::string_t rstr = response.to_string();
            sstr << "Failed to query robot with message " << std::string(rstr.begin(), rstr.end()) << std::endl;
            RW_THROW(sstr.str());
        }*/
        web::json::value jsonposition = response.extract_json().get()[U("position")];
        double x = jsonposition[U("x")].as_number().to_double();
        double y = jsonposition[U("y")].as_double();
        double theta = jsonposition[U("orientation")].as_double() * Deg2Rad;
        return Pose2D<>(x, y, theta);
    }
    catch (std::exception& exp) {
        std::cout << "MIR2::getCurrentPose()  Exception: " << exp.what() << "  "<<_host<< std::endl;
        RW_THROW("Exception trying to retrieve robot pose: "<<exp.what()<< "  "<<_host);
    }
}


int MIR2::getLastMissionId() const {
    return _lastMissionId;
}

void MIR2::waitForMissionToFinish(int id)
{
    utility::stringstream_t sstr;
    sstr << "/mission_queue/" << id;
    http_client client(uri_builder(_restURI).append_path(sstr.str()).to_uri());
    http_request request = generateRequest();
    std::string state = "";
    do {
        http_response response = client.request(request).get();
        checkResponse(response, "Wait for mission to finish", 200);
        web::json::value jsonstate = response.extract_json().get();
        utility::string_t s = jsonstate[U("state")].as_string();
        state = std::string(s.begin(), s.end());
    }
    while (state.compare("Done") != 0 && state.compare("Abort") != 0);    
}

void MIR2::waitForLastMissionsToFinish()
{
    waitForMissionToFinish(getLastMissionId());
}
 

bool MIR2::isMoving() const {
    return isMissionDone(getLastMissionId()) == false;
}

bool MIR2::isMissionDone(int id) const {
    utility::stringstream_t sstr;
    sstr << "/mission_queue/" << id;
    http_client client(uri_builder(_restURI).append_path(sstr.str()).to_uri());
    http_request request = generateRequest();
    http_response response = client.request(request).get();
    checkResponse(response, "isMissionDone", 200);
    web::json::value jsonstate = response.extract_json().get();
    utility::string_t s = jsonstate[U("state")].as_string();
    std::string state = std::string(s.begin(), s.end());
//    std::cout << "State of mission " << state << std::endl;    
    return state.compare("Done") == 0 || state.compare("Abort") == 0; //They are the same
}

void MIR2::setRegister(unsigned int index, unsigned int value)
{
    RW_THROW("Not implemented for MIR2");
}

int MIR2::getRegister(unsigned int index)
{
    RW_THROW("Not implemented for MIR2");
}



/* Methods from the serializable interface */
void MIR2::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
	iarchive.readEnterScope("MIR2");
	HWEntity::read(iarchive, id);
	_host = iarchive.read<std::string>("IP");
    std::string authoCode = iarchive.read<std::string>("AuthorizationCode");
    _authorizationCode = utility::string_t(authoCode.begin(), authoCode.end());
	iarchive.readLeaveScope("MIR2");
}

void MIR2::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
	oarchive.writeEnterScope("MIR2");
	HWEntity::write(oarchive, id);
	oarchive.write(_host, "IP");
    std::string authoCode(_authorizationCode.begin(), _authorizationCode.end());
    oarchive.write(authoCode, "AuthorizationCode");
	oarchive.writeLeaveScope("MIR2");
}
