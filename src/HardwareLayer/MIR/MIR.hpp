/* */
#ifndef HWL_MIR_HPP
#define HWL_MIR_HPP


#include "../MobileDevice.hpp"

#include <rw/common/Ptr.hpp>
#include <rw/math.hpp>

#include <cpprest/http_client.h>
#include <cpprest/ws_client.h>

namespace hwl {
	
/**
 * General interface for actions 
 */
class MIR: public MobileDevice
{
public:
	typedef rw::common::Ptr<MIR> Ptr;

	MIR();

	MIR(const std::string& host);

	virtual void connect();

	virtual void disconnect();


	virtual void vel(double lin, double rot);

	void start();
	void pause();

    /**
     * @copydoc MobileDevice::moveTo
     */
    virtual void moveTo(const rw::math::Pose2D<>& target, double goalThreshold);

    virtual void moveTo(const std::string& identifier);

    /**
     * @copydoc MobileDevice::getCurrentTransform
     */
    virtual rw::math::Pose2D<> getCurrentPose() const;

    /**
     * @copydoc MobileDevice::getDefinedPositions
     */
    std::vector<MobileDevice::DefinedPosition> getDefinedPositions() const;

    virtual bool isMoving() const;

	void setRegister(unsigned int index, unsigned int value);

	int getRegister(unsigned int index);



	/* Methods from the serializable interface */
	virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

	virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


private:
	std::string _host;
    web::http::uri _restURI;
    web::websockets::client::websocket_client _wsClient;

    int extractMissionId(const web::http::http_response &response);
    std::string getState() const;
    bool isMissionDone(int id);


};

} //end namespace

#endif //#ifndef HWL_MIR_HPP
