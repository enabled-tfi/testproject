#
# The shared library to build:
#
set(TargetName MIR)


#
# Source files
#
set(SrcFiles
    MIR.cpp
    MIR2.cpp
)


#
# Header files (optional)
#
set(HeaderFiles
    MIR.hpp
    MIR2.hpp
)

#
# Standard cpp files to compile into static library



add_library(${TargetName} ${SrcFiles} ${HeaderFiles})
target_link_libraries(${TargetName}  ${ROBWORKHARDWARE_LIBRARIES} ${ROBWORK_LIBRARIES} ${CPPREST_LIBRARIES})

#add_executable(mir_test mir_main.cpp)
#target_link_libraries(mir_test er_applicationlayer ${TargetName}  er_serialization er_hardwarelayer er_common er_security ${ROBWORKHARDWARE_LIBRARIES} ${ROBWORK_LIBRARIES} ${CPPREST_LIBRARIES} ${OpenCV_LIBS} )

#ssl crypto
