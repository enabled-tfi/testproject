/* */
#ifndef HWL_ACTUATOR_HPP
#define HWL_ACTUATOR_HPP

#include <Common/HWEntity.hpp>
#include <rw/common/Ptr.hpp>

namespace hwl {

/** 
 * Interface for a simple 1d actuator
 */
class Actuator: public HWEntity
{
public:
	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<Actuator> Ptr;

    /**
     * @brief Construct actuator with a given identifier and optionally name of the device models to use
     * @param identifier [in] Identifier use to identify the entity
     * @param deviceName [in] Name of the RobWork Device used for virtual representation of the actuator.
     */
    Actuator(const std::string& identifier, const std::string& deviceName = "");


    /**
     * @brief Perform homing of actuator
     */
    virtual void home() = 0;

    /**
     * @brief Set the position of the actuator
     * @param pos [in] The position meaured in either meter or radians.
     */
	virtual void setPosition(double pos) = 0;

    /**
     * @brief Returns the current position of the actuator
     * @return Current position in meter or radians
     */
    virtual double getPosition() const = 0;

    /**
     * @brief Returns the lower and upper limit of the actuator.
     * @return Pair of doubles containing lower and upper bound.
     */
    virtual std::pair<double, double> getBounds() const = 0;


    /**
    * @copydoc HWEntity::addUpdateRule
    */
    virtual void addUpdateRule(er_common::System::Ptr system);

    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

protected:
    virtual rw::models::Device::Ptr getDeviceModel() const;  


private:    
    std::string _deviceName;
    mutable rw::models::Device::Ptr _device;


    /**
    * Update rule for Gripper1D
    */
    class ActuatorUpdateRule : public er_common::System::UpdateRule
    {
    public:
        ActuatorUpdateRule(Actuator::Ptr actuator) :
            er_common::System::UpdateRule(),
            _actuator(actuator)
        {
        }

        ~ActuatorUpdateRule()
        {
        }

        void update(rw::kinematics::State& state)
        {
            try {
                double p = _actuator->getPosition();
                rw::math::Q q(1);
                q(0) = p;
                _actuator->getDeviceModel()->setQ(q, state);
            }
            catch (const std::exception&) {
            }
        }

        er_common::Entity::Ptr getEntity() {
            return _actuator;
        }

    private:
        Actuator::Ptr _actuator;
        rw::models::Device::Ptr _rwDevice;
    };


};

} //end namespace

#endif //#ifndef HWL_IMPULSEDEVICE_HPP
