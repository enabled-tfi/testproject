#include "Camera.hpp"

#include <opencv2/highgui.hpp>

using namespace er_common;
using namespace hwl;
using namespace rw::common;

Camera::Camera(const std::string& identifier):
	HWEntity(identifier)
{

}

Camera::Camera(const std::string& identifier, const std::string& calibrationfile) :
	HWEntity(identifier),
	_calibrationfile(calibrationfile),
	_sensorFrame(NULL)
{

}

rw::kinematics::Frame* Camera::getSensorFrame() {
    return _sensorFrame;
}

void Camera::setSensorFrame(rw::kinematics::Frame* sensorFrame) {
    _sensorFrame = sensorFrame;
}

void Camera::setCalibrationFileName(const std::string& calibrationFileName)
{
	_calibrationfile = calibrationFileName;
	_cameraMatrix = cv::Mat();
}

std::string Camera::getCalibrationFileName() const
{
	return _calibrationfile;
}

std::pair<cv::Mat, cv::Mat> Camera::getCameraCalibration()
{
	if (_cameraMatrix.empty()) {
		if (_calibrationfile.size() > 0) {
			cv::FileStorage fs2(er_common::System::getInstance()->getSystemFolder() + _calibrationfile, cv::FileStorage::READ);
			fs2["camera_matrix"] >> _cameraMatrix;
			fs2["distortion_coefficients"] >> _distCoeffs;
		}
		else {
			RW_THROW("No calibration file specified for camera "<<this->getName());
		}
	}
	return std::make_pair(_cameraMatrix, _distCoeffs);
}

/*
void Camera::setCameraPlacement(const Camera::CameraPlacement placement){
	_cameraPlacement = placement;
}
Camera::CameraPlacement Camera::getCameraPlacement() const {
	return _cameraPlacement;
}
*/

void Camera::addUpdateRule(er_common::System::Ptr system)
{
	system->addUpdateRule(ownedPtr(new hwl::Camera::CameraUpdateRule(this)));
}

void Camera::read(class er_serialization::InputArchive& iarchive, const std::string& id)
{
    HWEntity::read(iarchive, "");
    std::string calibrationFile = "";
    if(iarchive.has("CalibrationFile"))
    	calibrationFile = iarchive.read<std::string>("CalibrationFile");
    setCalibrationFileName(calibrationFile);


    std::string sensorFrameName = iarchive.read<std::string>("SensorFrame");
    if (sensorFrameName != "NULL") {
        _sensorFrame = System::getInstance()->getWorkCell()->findFrame(sensorFrameName);
        if (_sensorFrame == NULL) {
            RW_THROW("No frame named " << sensorFrameName << " found in work cell");
        }
    }

    /*std::string cameraplacement = iarchive.read<std::string>("CameraPlacement");
    if (cameraplacement == "Manipulator")
        setCameraPlacement(Camera::CameraPlacement::Manipulator);
    else if (cameraplacement == "MobileBase")
        setCameraPlacement(Camera::CameraPlacement::MobileBase);
    else
        RW_THROW("Unknown camera placement");*/
}

void Camera::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    HWEntity::write(oarchive, "");
    oarchive.write(getCalibrationFileName(), "CalibrationFile");
    if (_sensorFrame != NULL) {
        oarchive.write(_sensorFrame->getName(), "SensorFrame");
    } else {
        oarchive.write(_sensorFrame->getName(), "NULL");
    }
    /*std::string placement = "Undefined";
    switch (getCameraPlacement()) {
    case CameraPlacement::Manipulator:
        placement = "Manipulator";
        break;
    case CameraPlacement::MobileBase:
        placement = "MobileBase";
        break;
    }
    oarchive.write(placement, "CameraPlacement");*/
}
