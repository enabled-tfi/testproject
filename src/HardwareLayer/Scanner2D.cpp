#include "Scanner2D.hpp"

using namespace er_common;
using namespace hwl;

Scanner2D::Scanner2D(const std::string& identifier) :
	HWEntity(identifier)
{

}

rw::kinematics::Frame* Scanner2D::getSensorFrame() {
    //TODO Implement storing sensor frame for Scanner2D
    RW_THROW("Scanner2D::getSensorFrame() Not implemented");
}

void Scanner2D::addUpdateRule(er_common::System::Ptr system)
{
    system->addUpdateRule(rw::common::ownedPtr(new hwl::Scanner2D::Scanner2DUpdateRule(this)));
}
