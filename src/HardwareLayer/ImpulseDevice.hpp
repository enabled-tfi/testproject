/* */
#ifndef HWL_IMPULSEDEVICE_HPP
#define HWL_IMPULSEDEVICE_HPP

#include <Common/HWEntity.hpp>
#include <rw/common/Ptr.hpp>

namespace hwl {

/** 
 * Interface for a device able to supply an impulse
 */
class ImpulseDevice: public HWEntity
{
public:
	/** @brief Smart pointer to the object*/
    typedef rw::common::Ptr<ImpulseDevice> Ptr;

	ImpulseDevice(const std::string& identifier);

    /**
     * @brief Execute the impulse command on the device
     */
	virtual void execute(double force) = 0;

};

} //end namespace

#endif //#ifndef HWL_IMPULSEDEVICE_HPP
