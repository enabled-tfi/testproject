#include <iostream>

#include "../Common/EntityRegister.hpp"
#include "../Common/System.hpp"
#include "../ExecutionModel/Sequence.hpp"
#include "../ExecutionModel/Print.hpp"

#include "../ExecutionModel/For.hpp"
#include "../ExecutionModel/If.hpp"
#include "../ExecutionModel/Wait.hpp"
#include "../ExecutionModel/Argument.hpp"
#include "../ExecutionModel/DriveRel.hpp"
#include "../ExecutionModel/DriveTo.hpp"
#include "../ExecutionModel/MoveLin.hpp"
#include "../ExecutionModel/MovePTP.hpp"

#include <ProgramModel/Program.hpp>

#include "../Serialization/OutputArchiveXML.hpp"
#include "../Serialization/InputArchiveXML.hpp"

#include "../Common/System.hpp"
#include "../HardwareLayer/MobileDevice.hpp"
#include "../HardwareLayer/MobileDeviceSimulated.hpp"
#include "../HardwareLayer/Manipulator.hpp"
#include "../HardwareLayer/ManipulatorSimulated.hpp"


#include <rw/loaders/WorkCellLoader.hpp>

using namespace exm;
using namespace pgm;
using namespace hwl;
using namespace mms_common;
using namespace mms_serialization;
using namespace rw::math;
using namespace rw::common;
using namespace rw::loaders;
using namespace rw::models;
using namespace rw::kinematics;

void basicFunctionAndSerializationTest();
void entityFunctionAndSerializationTest(WorkCell::Ptr workcell);

int main(int argc, char** argv) {
    ER_LOG_DEBUG("SoftwareModelTest");

    //basicFunctionAndSerializationTest();


    ER_LOG_DEBUG("Trying to load work cell file: "<<argv[1]);
    WorkCell::Ptr workcell = WorkCellLoader::Factory::load(argv[1]);

    entityFunctionAndSerializationTest(workcell);



}

void entityFunctionAndSerializationTest(WorkCell::Ptr workcell)
{
	MovableFrame* mobileDeviceFrame = workcell->findFrame<MovableFrame>("MobileDevice");
	if (mobileDeviceFrame == NULL) {
		ER_LOG_DEBUG("No MovableFrame found for the Mobile Device" );
		return;
	}


	SerialDevice::Ptr ur10 = workcell->findDevice<SerialDevice>("UR10");
	if (ur10 == NULL) {
		ER_LOG_DEBUG("No Device for UR10" );
		return;
	}


	MobileManipulator::Ptr mobileManipulator = rw::common::ownedPtr(new MobileManipulator(workcell, ownedPtr(new Setup()), workcell->getDefaultState()));
	System::Ptr system = System::getInstance();
	system->initialize(workcell, mobileManipulator);
	hwl::MobileDevice::Ptr mobileDevice = rw::common::ownedPtr(new MobileDeviceSimulated(mobileDeviceFrame, system, "MIR"));
	EntityRegister::add(mobileDevice->getName(), mobileDevice);
	 
	ExecutionManager::Ptr executionManager = rw::common::ownedPtr(new ExecutionManager(system));

	hwl::Manipulator::Ptr manipulator = rw::common::ownedPtr(new ManipulatorSimulated(ur10, system, "Manipulator"));
	EntityRegister::add(manipulator->getName(), manipulator);

	{
		Sequence sequence;
		DriveRel driveRel(0.1, 0, "MIR");
		sequence.addInstruction(&driveRel);
		DriveTo driveTo(Transform2D<>(Vector2D<>(0.1, 0.1), Rotation2D<>::identity()), true, "MIR");
		sequence.addInstruction(&driveTo);

		ER_LOG_DEBUG("A" );
		Q q(6);
		q(0) = 0;
		q(1) = 0.5;
		q(2) = 0.5;
		q(3) = 0.2;
		q(4) = 0.3;
		q(5) = 0.4;
		MovePTP movePTP(q, "UR10");
		sequence.addInstruction(&movePTP);
		ER_LOG_DEBUG("A+" );
		State state = workcell->getDefaultState();
		ur10->setQ(q, state);
		ER_LOG_DEBUG("A++" );
		Transform3D<> target = ur10->baseTend(state);
		target.P() -= Vector3D<>(0, 0.1, 0);
		MoveLin moveLin(target, ur10->getBase()->getName(), "UR10");

		sequence.addInstruction(&moveLin);
		ER_LOG_DEBUG("B" );
		Program program("MyDriveProgram");
		program.execute(executionManager);
		ER_LOG_DEBUG("C" );

		OutputArchiveXML oarchive("ArchiveDriveTest.xml");
		program.write(oarchive, "MyDriveProgram");
		oarchive.finalize();

		InputArchiveXML iarchive("ArchiveDriveTest.xml");
		iarchive.initialize();
		ER_LOG_DEBUG("Load Initialized" );
		Program program2;
		program2.read(iarchive, "Program");

		ER_LOG_DEBUG("Execution of program after load:" );
		program2.execute(executionManager);
	}
	//basicFunctionAndSerializationTest()
	{
		Sequence sequence;

		Print p1("Hello1\n", std::cout);
		Print p2("Hello2\n", std::cout);

		Sequence sequence2;
		sequence2.addInstruction(&p1);
		Wait wait(1000);
		sequence2.addInstruction(&wait);
		For forInstruction(0, 10, 1, &sequence2);

		Argument::Ptr ifArgument = rw::common::ownedPtr(new Argument());
		ifArgument->set<bool>(true);
		If ifInstruction(ifArgument, &forInstruction, &p2);
		sequence.addInstruction(&ifInstruction);



		Program program("MyTestProgram");
		program.execute(executionManager);


		OutputArchiveXML oarchive("Archive.xml");
		program.write(oarchive, "MyProgram");
		oarchive.finalize();

		ER_LOG_DEBUG("REady to load" );
		InputArchiveXML iarchive("Archive.xml");
		iarchive.initialize();
		ER_LOG_DEBUG("Load Initialized" );
		Program program2;
		program2.read(iarchive, "Program");

		ER_LOG_DEBUG("Execution of program after load:" );
		program2.execute(executionManager);



		OutputArchiveXML oarchive2("Archive2.xml");
		program2.write(oarchive2, "MyProgram");
		oarchive2.finalize();


	}
}