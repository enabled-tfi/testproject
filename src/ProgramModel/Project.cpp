#include "Project.hpp"

#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>

#include <iostream>
#include <string>

using rw::common::ownedPtr;
using namespace pgm;
using namespace er_serialization;
using namespace er_common;

Project::Project():
	_name("Untitled")
{

}

Project::Project(const std::string& name):
	_name(name)
{

}


void Project::addEvent(Event::Ptr event)
{
	_events.push_back(event);
}

std::vector<Event::Ptr>& Project::getEvents()
{
	return _events;
}

const std::vector<Event::Ptr>& Project::getEvents() const
{
	return _events;
}



bool Project::hasHandlerName(const std::string& name)
{
	return _programs.find(name) != _programs.end();
}



void Project::addProgram(Program::Ptr program)
{
	std::string programname = program->getName();
	if (hasHandlerName(programname)) {
		RW_THROW("Project::addProgram: Attempting to add program named '" << programname << " which is already exists in the project.");
	}
	_programs[programname] = program;
}

std::vector<Program::Ptr> Project::getPrograms() const 
{
	std::vector<Program::Ptr> result;
	for (auto arg : _programs) {
		result.push_back(arg.second);
	}
	return result;
}

Program::Ptr Project::getProgram(const std::string& name) const
{
	std::map<std::string, Program::Ptr>::const_iterator it = _programs.find(name);
	if (it != _programs.end())
		return (*it).second;
	else
		return NULL;
}

/* Methods from the serializable interface */
void Project::read(InputArchive& iarchive, const std::string& id)
{
    ER_LOG_DEBUG("Project::read");
    iarchive.readEnterScope("Project");
    _name = iarchive.read<std::string>("Name");
    ER_LOG_DEBUG("Name = "<<_name);

	iarchive.readEnterScope("Programs");
	std::vector<std::pair<std::string, InputArchive::Ptr> > pgmarchives = iarchive.getSubArchives();
	ER_LOG_DEBUG("Number of sub archives " << pgmarchives.size() );
	for (auto sarch : pgmarchives) {
		Program::Ptr program = ownedPtr(new Program(""));
		program->read(*(sarch.second),"");
		ER_LOG_DEBUG("Sub Archive Name = " << sarch.first << program->getName());
		_programs[program->getName()] = program;
	}
	iarchive.readLeaveScope("Programs");

	iarchive.readEnterScope("Events");
	std::vector<std::pair<std::string, InputArchive::Ptr> > eventarchives = iarchive.getSubArchives();
	for (auto sarch : eventarchives) {
		Event::Ptr event = ownedPtr(new Event());
		event->read(*(sarch.second), "");
		_events.push_back(event);
	}
	iarchive.readLeaveScope("Events");


	iarchive.readLeaveScope("Project");
}

void Project::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("Project");
    oarchive.write(_name, "Name");

	oarchive.writeEnterScope("Programs");
	for (std::map<std::string, Program::Ptr>::const_iterator it = _programs.begin(); it != _programs.end(); it++) {
		(*it).second->write(oarchive, (*it).first);
	}	

	oarchive.writeLeaveScope("Programs");

	oarchive.writeEnterScope("Events");
	for (Event::Ptr event : _events) {
		event->write(oarchive, "Event");
	}
	oarchive.writeLeaveScope("Events");

    oarchive.writeLeaveScope("Project");
}
