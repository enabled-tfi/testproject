#include "Program.hpp"

#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>
#include <Common/EntityRepository.hpp>

#include <iostream>
#include <string>

using rw::common::ownedPtr;
using namespace pgm;
using namespace exm;
using namespace er_serialization;
using namespace er_common;

Program::Program():
	Instruction("Program"),
    _isInitialized(false)
{

}

Program::Program(const std::string& name):
	Instruction(name),
	_name(name),
    _isInitialized(true)
{

}


bool Program::isInitialized() const
{
    return _isInitialized;
}


void Program::addInstruction(Instruction::Ptr item)
{
	_instructions.push_back(item);
}

const std::vector<Instruction::Ptr>& Program::getInstructions() const
{
	return _instructions;
}

void Program::setName(const std::string& name)
{
	_name = name;
}

const std::string& Program::getName() const
{
	return _name;
}

void Program::setDescription(const std::string& description)
{
	_description = description;
}

const std::string& Program::getDescription() const
{
	return _description;
}


bool Program::execute(exm::ExecutionManager::Ptr executionManager)
{
	for (Instruction::Ptr item : _instructions)
	{
		item->execute(executionManager);
	}
	return true;
}


/* Methods from the serializable interface */
void Program::read(InputArchive& iarchive, const std::string& id)
{
    ER_LOG_DEBUG("Program::read");
    iarchive.readEnterScope("Program");
    _name = iarchive.read<std::string>("Name");
	_description = iarchive.read<std::string>("Description");
    ER_LOG_DEBUG("Name = "<<_name);
    iarchive.readEnterScope("Instructions");
	std::vector<std::pair<std::string, InputArchive::Ptr> > archives = iarchive.getSubArchives();
	ER_LOG_DEBUG("Number of Program Items " << archives.size() );
	for (std::pair<std::string, InputArchive::Ptr>& ia : archives) {
		ER_LOG_DEBUG("Archive with id " <<ia.first );
		Instruction::Ptr instruction = EntityRepository<Instruction>::make(ia.first);
		//ProgramItem::Ptr programitem = entity.cast<ProgramItem>();
		//ProgramItem::Ptr item = ownedPtr(new ProgramItemDummy());
		instruction->read(*ia.second, ia.first);
		addInstruction(instruction);
	}
	iarchive.readLeaveScope("Instructions");
	iarchive.readLeaveScope("Program");
}

void Program::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("Program");
    oarchive.write(_name, "Name");
	oarchive.write(_description, "Description");
	oarchive.writeEnterScope("Instructions");
	for (Instruction::Ptr item : _instructions) {
		item->write(oarchive, item->getName());
		//oarchive.write(item, "ProgramItem");
	}
    
    oarchive.writeLeaveScope("Instructions");
    oarchive.writeLeaveScope("Program");
}
