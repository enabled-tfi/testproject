/* */
#ifndef PGM_PROGRAMITEM_HPP
#define PGM_PROGRAMITEM_HPP

#include <Common/Entity.hpp>
#include <Common/Settings.hpp>
#include <ExecutionModel/ExecutionManager.hpp>
#include <rw/common/Ptr.hpp>
#include <rw/common/PropertyMap.hpp>

namespace pgm {
	
//
//
///**
// * General interface for Instructions
// */
//class ProgramItem: public er_common::Entity
//{
//public:
//	/** @brief Smart pointer to the ProgramItem object*/
//	typedef rw::common::Ptr<ProgramItem> Ptr;
//
//
//	ProgramItem();
//
//	ProgramItem(const std::string& name, const std::string& description);
//
//	void initialize(er_common::System::Ptr system);
//
//	virtual void execute(exm::ExecutionManager::Ptr executionManager) = 0;
//
//
//	
//
//	/* Methods from the serializable interface */
//    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);
//
//    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;
//
//	er_common::Settings::Ptr settings() {
//		return _settings;
//	}
//
//protected:
//	er_common::Settings::Ptr _settings;
//
//	er_common::System::Ptr _system;
//
//
//
//private:
//	void initializeSettings();
//
//};
//
//class ProgramItemDummy : public ProgramItem
//{
//public:
//	ProgramItemDummy() : ProgramItem()
//	{
//		//_settings = rw::common::ownedPtr(new er_common::Settings());
//	}
//
//	ProgramItemDummy(const std::string& name, const std::string& description) :
//		ProgramItem(name, description)
//	{
//		//_settings = rw::common::ownedPtr(new er_common::Settings());
//	}
//
//
//
//	void execute(exm::ExecutionManager::Ptr executionManager) {
//
//	}
//
//};

} //end namespace

#endif //#ifndef PRGM_PROGRAMITEM_HPP
