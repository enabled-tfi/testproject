/* */
#ifndef PGM_EVENT_HPP
#define PGM_EVENT_HPP

#include "Program.hpp"

#include <Common/Serializable.hpp>

#include <rw/common/Ptr.hpp>

#include <string>

namespace pgm {
	

/**
 * General interface for program
 */
class Event: public er_common::Serializable
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<Event> Ptr;


	Event();
	
	/**
	 * @brief Construct a program
	 * @param name [in] Name of the program
	 * @param mainMethod [in] The main instruction of the program
	 */
	Event(unsigned int reg, unsigned int value, const std::string& handlerName); 

	unsigned int getRegister() const;
	void setRegister(unsigned int reg);

	unsigned int getValue() const;
	void setValue(unsigned int val);

	const std::string& getHandlerName() const;
	void setHandlerName(const std::string& handlerName);



    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

private:
	unsigned int _reg;
	unsigned int _value;
	std::string _handlerName;
};

} //end namespace

#endif //#ifndef PGM_EVENT_HPP
