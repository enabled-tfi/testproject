/* */
#ifndef PGM_PROJECT_HPP
#define PGM_PROJECT_HPP

#include "Program.hpp"
#include "Event.hpp"
#include <Common/Serializable.hpp>

#include <rw/common/Ptr.hpp>

#include <string>

namespace pgm {
	

/**
 * General interface for program
 */
class Project: public er_common::Serializable
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<Project> Ptr;


	Project();
	
	/**
	 * @brief Construct a program
	 * @param name [in] Name of the program
	 * @param mainMethod [in] The main instruction of the program
	 */
	Project(const std::string& name);

	void addEvent(Event::Ptr event);

	std::vector<Event::Ptr>& getEvents();

	const std::vector<Event::Ptr>& getEvents() const;

	std::vector<Program::Ptr> getPrograms() const;

	Program::Ptr getProgram(const std::string& name) const;

	void addProgram(Program::Ptr program);

	bool hasHandlerName(const std::string& name);

    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;


private:
	bool _isInitialized;

	std::string _name;

	std::map<std::string, Program::Ptr> _programs;
	std::vector<Event::Ptr> _events;
};

} //end namespace

#endif //#ifndef PGM_PROJECT_HPP
