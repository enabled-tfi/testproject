#include "ProjectController.hpp"

#include <Common/EntityRegister.hpp>
#include <ExecutionModel/ExecutionManager.hpp>
#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>

#include <HardwareLayer/MIR/MIR.hpp>


#include <iostream>
#include <string>

using rw::common::ownedPtr;
using namespace pgm;
using namespace exm;
using namespace hwl;
using namespace er_common;


ProjectController::ProjectController():
	_isProjectRunning(false),
	_stopController(false)
{

}

void ProjectController::startProject(Project::Ptr project, bool blocking)
{
	_stopController = false;
	_project = project;
	_status = Running;
	_errorMsg = "";
	_thread = ownedPtr(new boost::thread(boost::bind(&ProjectController::run, this)));
	if (blocking) {		
		_thread->join();
	}
}



void ProjectController::stopProject() {
	boost::mutex::scoped_lock scoped_lock(_mutex);
	_stopController = true;
}

void ProjectController::run()
{	
	try {
		ER_LOG_DEBUG("ProjectController::run " << _project );
		_isProjectRunning = true;
		std::vector<Event::Ptr> events = _project->getEvents();
		ER_LOG_DEBUG("Events " << events.size() );
		if (EntityRegister::has("MobileDevice") == false) {
			ER_LOG_DEBUG("Ready to throw exceptin " );
			RW_THROW("Unable to find MobileDevice");
		}

		ER_LOG_DEBUG("Has not crashed" );
		MIR::Ptr mir = EntityRegister::get<MIR>("MobileDevice");
		ER_LOG_DEBUG("MIR = " << mir );
		if (mir == NULL) {
			RW_THROW("Unable to find MobileDevice");
		}


		ExecutionManager execManager(System::getInstance());

		bool stop = _stopController;
		while (_stopController == false && _status == Running) {

			for (Event::Ptr event : events)
			{
				unsigned int reg = event->getRegister();
				unsigned int regval = mir->getRegister(reg);

				if (regval == event->getValue()) {
					//Start the associated program
					Program::Ptr program = _project->getProgram(event->getHandlerName());
					execManager.executeBlocking(program);
					if (execManager.getExecutionState() == ExecutionManager::Success) {
						mir->setRegister(reg, 0);
					}
					else {
						RW_THROW("Error while execution program: " << execManager.getErrorMessage());
					}
				}

			}
			boost::mutex::scoped_lock scoped_lock(_mutex);
			stop = _stopController;
		}
		_status = Ready;
	}
	catch (const std::exception& exp) {
		std::stringstream sstr;
		sstr << "Exception while executing project: " << exp.what();
		_errorMsg = sstr.str();
		_status = Error;
	}
}

ProjectController::Status ProjectController::getStatus() const
{
	return _status;
}

void ProjectController::resetErrors() {
	_status = Ready;
}

std::string ProjectController::getLastError() const
{
	return _errorMsg;
}
