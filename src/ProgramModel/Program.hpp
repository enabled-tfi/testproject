/* */
#ifndef PGM_PROGRAM_HPP
#define PGM_PROGRAM_HPP


#include <ExecutionModel/Instruction.hpp>

#include <Common/Serializable.hpp>

#include <rw/common/Ptr.hpp>

#include <string>

namespace pgm {
	

/**
 * General interface for program
 */
class Program: public exm::Instruction
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<Program> Ptr;


	Program();
	
	/**
	 * @brief Construct a program
	 * @param name [in] Name of the program
	 * @param mainMethod [in] The main instruction of the program
	 */
	Program(const std::string& name); 

	bool isInitialized() const;

	void addInstruction(exm::Instruction::Ptr instruction);
	const std::vector<exm::Instruction::Ptr>& getInstructions() const;

	void setName(const std::string& name);
	const std::string& getName() const;

	void setDescription(const std::string& description);
	const std::string& getDescription() const;



	/**
	 * @brief Calls execute on the main instruction.
	 */
	virtual bool execute(exm::ExecutionManager::Ptr executionManager);

    /* Methods from the serializable interface */
    virtual void read(class er_serialization::InputArchive& iarchive, const std::string& id);

    virtual void write(class er_serialization::OutputArchive& oarchive, const std::string& id) const;

protected:
	virtual void initializeSettings() {};

private:
	bool _isInitialized;

	std::string _name;
	std::string _description;


	std::vector<exm::Instruction::Ptr> _instructions;
};

} //end namespace

#endif //#ifndef PGM_PROGRAM_HPP
