/* */
#ifndef PGM_PROGRAMMANAGEMENT_HPP
#define PGM_PROGRAMMANAGEMENT_HPP

//#include "ProgramItem.hpp"

#include <ExecutionModel/ExecutionManager.hpp>
#include <ProgramModel/Program.hpp>
#include <rw/common/Ptr.hpp>

#include <string>

namespace pgm {
	

/**
 * Manages programs and provides an interface for starting and stopping programs
 */
class ProgramManager
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<ProgramManager> Ptr;

    /**
     * @brief Constructs default ProgramManager
     */
    ProgramManager();


    /** 
     * @brief Construct ProgramManager looking into the specified folder
     * @param programFolder [in] path to the folder
     */
    ProgramManager(const std::string& programFolder);

    /**
     * @brief Sets the folder inside which the ProgramManager are looking for and saving programs
     * @param programFolder [in] path to the folder
     */
    void setProgramFolder(const std::string& programFolder);
	
    /** 
     * @brief Return the names of all program available 
     * @return List of program names
     */
    std::vector<std::string> getProgramNames();
	
    /**
     * @brief Save program
     * @param name [in] Name of the program to save
     * @param programCode [in] The XML program code from serializing a Program
     * @param frontEndCode [in] The data needed to represent the program in the front-end
     * @param overwrite [in] Whether to overwrite existing file or throw exception
     */
    void saveProgram(const std::string& name, const std::string& programCode, const std::string& frontEndCode, bool overwrite);

    /**
     * @brief Loads in a program
     * @param name [in] Name of the program to load
     * @return std::pair containing string representing the program and the front end representation.
     */
    std::string loadProgramFrontEndCode(const std::string& name);

    enum State { Ready = 0, ProgramLoaded = 1, Executing = 2, Error = 3};
    State getState();

    Program::Ptr loadProgram(const std::string& name);
    void startProgram();
    void stopProgram();


private:
    std::string _programFolder;
	
    bool fileExists(const std::string& filename);

    exm::ExecutionManager _execManager;
    Program::Ptr _currentProgram;
    State _state;

};

} //end namespace

#endif //#ifndef PGM_PROGRAMMANAGEMENT_HPP
