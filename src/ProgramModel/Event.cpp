#include "Event.hpp"

#include <Common/OutputArchive.hpp>
#include <Common/InputArchive.hpp>

#include <iostream>
#include <string>

using rw::common::ownedPtr;
using namespace pgm;
using namespace er_serialization;
using namespace er_common;

Event::Event():
	_reg(0),
	_value(0)
{

}

Event::Event(unsigned int reg, unsigned int value, const std::string& handlerName) :
	_reg(reg),
	_value(value),
	_handlerName(handlerName)
{

}


unsigned int Event::getRegister() const
{
	return _reg;
}

void Event::setRegister(unsigned int reg) 
{
	_reg = reg;
}

unsigned int Event::getValue() const
{
	return _value;
}

void Event::setValue(unsigned int val)
{
	_value = val;
}

const std::string& Event::getHandlerName() const
{
	return _handlerName;
}

void Event::setHandlerName(const std::string& handlerName)
{
	_handlerName = handlerName;
}


/* Methods from the serializable interface */
void Event::read(InputArchive& iarchive, const std::string& id)
{
    ER_LOG_DEBUG("Event::read");
    iarchive.readEnterScope("Event");
	_reg = iarchive.read<int>("Register");
	_value = iarchive.read<int>("ActivationValue");
    _handlerName = iarchive.read<std::string>("Handler");
	iarchive.readLeaveScope("Event");
}

void Event::write(class er_serialization::OutputArchive& oarchive, const std::string& id) const
{
    oarchive.writeEnterScope("Event");
    oarchive.write((int)_reg, "Register");
	oarchive.write((int)_value, "ActivationValue");
	oarchive.write(_handlerName, "Handler");
    oarchive.writeLeaveScope("Event");
}
