/* */
#ifndef PGM_PROJECTCONTROLLER_HPP
#define PGM_PROJECTCONTROLLER_HPP

#include "Project.hpp"
#include <Common/Serializable.hpp>

#include <rw/common/Ptr.hpp>

#include <string>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

namespace pgm {
	

/**
 * General interface for program
 */
class ProjectController
{
public:
	/** @brief Smart pointer to the object*/
	typedef rw::common::Ptr<ProjectController> Ptr;


	ProjectController();

	void startProject(Project::Ptr project, bool blocking);

	void stopProject();

	enum Status { Ready = 0, Running = 1, Error = 2};
	Status getStatus() const;

	std::string getLastError() const;
	void resetErrors();
private:
	Project::Ptr _project;
	rw::common::Ptr<boost::thread> _thread;
	boost::mutex _mutex;
	void run();
	bool _isProjectRunning;
	bool _stopController;
	
	Status _status;
	std::string _errorMsg;
};

} //end namespace

#endif //#ifndef PGM_PROJECTCONTROLLER_HPP
