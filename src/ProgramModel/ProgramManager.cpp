#include "ProgramManager.hpp"

#include <Serialization/InputArchiveXML.hpp>
#include <rw/common/IOUtil.hpp>


#include <iostream>
#include <string>
#include <fstream>

using rw::common::ownedPtr;
using namespace pgm;
using namespace exm;
using namespace er_serialization;
using namespace er_common;
using namespace rw::common;

ProgramManager::ProgramManager():
    _programFolder(""),
    _execManager(System::getInstance()),
    _state(Ready)
{

}

ProgramManager::ProgramManager(const std::string& programFolder):
    _programFolder(programFolder),
    _execManager(System::getInstance()),
    _state(Ready)
{

}

ProgramManager::State ProgramManager::getState() {
    return _state;
}


void ProgramManager::setProgramFolder(const std::string& programFolder)
{
    _programFolder = programFolder;
}

std::vector<std::string> ProgramManager::getProgramNames()
{
    std::vector<std::string> files = IOUtil::getFilesInFolder(_programFolder, false, false, "*.er_program.xml");
    std::vector<std::string> result;
    for (std::string name : files) {
        int n = name.find_first_of('/');
        name = name.substr(n, std::string::npos);
        n = name.find_first_of('.');
        name = name.substr(0, n);
        result.push_back(name);
    }
    return result;
}

void ProgramManager::saveProgram(const std::string& name, const std::string& programCode, const std::string& frontendCode, bool overwrite)
{
	const std::string erProgramFile = _programFolder + "/" + name + ".er_program.xml";
	const std::string frontendFile = _programFolder + "/" + name + ".frontend";
    if (!overwrite) {
        if (fileExists(erProgramFile)) {
            RW_THROW("Program named '" << name << "' already exists");
        }
        if (fileExists(frontendFile)) {
            RW_THROW("Frontend Program named '" << name << "' already exists");
        }
    }

    std::ofstream erProgram(erProgramFile.c_str());
    if (erProgram.is_open() == false) {
    	RW_THROW("Unable to open file for saving");
    }
    std::ofstream erFrontendCode(frontendFile.c_str());
    if (erFrontendCode.is_open() == false) {
    	RW_THROW("Unable to open file for saving");
    }

    erProgram<<programCode;
    erProgram.close();

    erFrontendCode<<frontendCode;
    erFrontendCode.close();

}

std::string ProgramManager::loadProgramFrontEndCode(const std::string& name)
{
	//const std::string erProgramFile = _programFolder + "/" + name + ".er_program.xml";
	const std::string frontendFile = _programFolder + "/" + name + ".frontend";
	//if (fileExists(erProgramFile) == false) {
	//	RW_THROW("Program named '" << name << "' does not exist");
	//}
	if (fileExists(frontendFile) == false) {
		RW_THROW("Frontend Program named '" << name << "' does not exist");
	}

    //std::ifstream erProgram(erProgramFile.c_str());
    //if (erProgram.is_open() == false) {
//    	RW_THROW("Unable to open file");
  //  }
    std::ifstream erFrontendCode(frontendFile.c_str());
    if (erFrontendCode.is_open() == false) {
    	RW_THROW("Unable to open file");
    }

    //std::stringstream erProgramBuffer;
    //erProgramBuffer << erProgram.rdbuf();

    std::stringstream frontendBuffer;
    frontendBuffer << erFrontendCode.rdbuf();

    return frontendBuffer.str();
    //return std::make_pair(erProgramBuffer.str(), frontendBuffer.str());
}


Program::Ptr ProgramManager::loadProgram(const std::string& name)
{
    const std::string erProgramFile = _programFolder + "/" + name + ".er_program.xml";
    if (fileExists(erProgramFile) == false) {
        RW_THROW("Program named '" << name << "' does not exist");
    }
    InputArchiveXML iarchive(erProgramFile);
    iarchive.initialize();
    _currentProgram = ownedPtr(new Program());
    _currentProgram->read(iarchive, "Program");

    return _currentProgram;
}

void ProgramManager::startProgram()
{
    if (_currentProgram == NULL) {
        RW_THROW("No program loaded");
    }

    _state = Executing;
    _execManager.executeNonBlocking(_currentProgram, [this](ExecutionManager::ExecutionResult result)
            {
                switch (result) {
                case ExecutionManager::ExecutionSuccess:
                    _state = Ready;
                    break;
                case ExecutionManager::ExecutionStopped:
                    _state = Ready;
                    break;
                case ExecutionManager::ExecutionError:
                    _state = Error;
                    break;
                }

            });

}

void ProgramManager::stopProgram()
{
    _execManager.stop();
}


bool ProgramManager::fileExists(const std::string& filename)
{
    if (FILE *file = fopen(filename.c_str(), "r")) 
    {
        fclose(file);
        return true;
    }
    else {
        return false;
    }
}
