#!/bin/bash
# Installation script for Enabled Robotics framework and dependencies.
# Tested on clean Ubuntu 16.04
# The following dependencies are build:
# - URCaps SDK
# - RobWork, RobWorkStudio, RobWorkHardware
# - GoogleTest
# - OpenCV
# - CppRest
# - Basler Pylon
# - dlib
# - PCL
# - ROS (Kinetic) Full Desktop
# - xmlrpc++0.7
# - WorkcellCalibration
# - EnabledRobotics: Base, er_light

# The following properties are set up:
# - Apache2 server hosting er.com
# - DNS cache forwarding traffic to er.com
# - xorg.conf for dummy screen
# - udev rules for camera entities

# Make sure passwords are not saved and stop at error
unset HISTFILE
set -e

# Read git username and password (for checking out ER git repositories)
echo -n Git Username:
read gitUser

echo -n Git Password:
read -s gitPass

echo \n

# Update apt-get
sudo apt-get update

# Install general build tools
sudo apt-get install subversion git mercurial gcc g++ cmake -y

# Install SSH to access the computer remotely
sudo apt install openssh-server -y

# URCaps dependencies
sudo apt-get install maven dialog sshpass -y

# Fetch URCaps SDK
mkdir ~/local
cd ~/local
mkdir URCaps_SDK
cd URCaps_SDK
wget -c https://s3-eu-west-1.amazonaws.com/urplus-developer-site/sdk/sdk-1.6.1.zip
unzip sdk-1.6.1.zip
rm sdk-1.6.1.zip

# Delete option for installing cross-compiler toolchain (We dont need it)
sed -i '75,81d;' install.sh

# Install URCaps SDK
./install.sh -y

# Install RobWork dependencies
sudo apt-get install libboost-dev libboost-date-time-dev libboost-filesystem-dev libboost-program-options-dev libboost-regex-dev libboost-serialization-dev libboost-system-dev libboost-test-dev libboost-thread-dev qtbase5-dev libdc1394-22-dev -y

# Optional dependencies for RobWork - Not available on Ubuntu 18.04
sudo apt-get install libxerces-c3.2 libxerces-c-dev -y

# GoogleTest - both used for RobWork and for Enabled Robotics Software
sudo apt-get install libgtest-dev -y
cd ~/local
git clone https://github.com/google/googletest.git
cd googletest
mkdir build
cd build
cmake ..
make
sudo make install

# Fetch RobWork
cd
git clone https://gitlab.com/sdurobotics/RobWork.git

# Fetch EnabledRobotics code
mkdir ~/code
cd ~/code

git clone https://$gitUser:$gitPass@gitlab.com/enabled-robotics/base.git
cd base
git remote set-url origin https://gitlab.com/enabled-robotics/base.git

# Copy UR driver updates to RWHW installation
cp ext/universalrobots/URPrimaryInterface.cpp ~/RobWork/RobWorkHardware/src/rwhw/universalrobots/URPrimaryInterface.cpp
cp ext/universalrobots/UniversalRobotsData.hpp ~/RobWork/RobWorkHardware/src/rwhw/universalrobots/UniversalRobotsData.hpp

# Build RobWork, RobWorkStudio and RobWorkHardware
cd ~/RobWork
mkdir Build
mkdir Build/RW
mkdir Build/RWStudio
mkdir Build/RWHardware

cd ~/RobWork/Build/RW
cmake -DCMAKE_BUILD_TYPE=Release ../../RobWork
make -j4

cd ~/RobWork/Build/RWStudio
cmake -DCMAKE_BUILD_TYPE=Release ../../RobWorkStudio
make -j4

cd ~/RobWork/Build/RWHardware
cmake -DCMAKE_BUILD_TYPE=Release ../../RobWorkHardware
make -j4

export RW_ROOT=~/RobWork/RobWork/
export RWS_ROOT=~/RobWork/RobWorkStudio/
export RWHW_ROOT=~/RobWork/RobWorkHardware/

echo '#ROBWORK#' >> ~/.bashrc
echo 'export RW_ROOT=~/RobWork/RobWork/' >> ~/.bashrc
export RW_ROOT=~/RobWork/RobWork/
echo 'export RWS_ROOT=~/RobWork/RobWorkStudio/' >> ~/.bashrc
export RWS_ROOT=~/RobWork/RobWorkStudio/
echo 'export RWHW_ROOT=~/RobWork/RobWorkHardware/' >> ~/.bashrc
export RWHW_ROOT=~/RobWork/RobWorkHardware/
source ~/.bashrc

#Remove RobworkHardwares FindOpencv as it does not work
rm ~/RobWork/RobWorkHardware/cmake/Modules/FindOpenCV.cmake

# Install Point Cloud Library
sudo apt-get install libpcl-dev -y

# Install OpenCV dependencies
sudo apt-get install build-essential libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev -y

# Download and install OpenCV version 3.3.0 and OpenCV-contrib
opencv_version=3.3.0
cd ~/local
mkdir OpenCV
cd OpenCV

wget -c -O OpenCV.zip http://sourceforge.net/projects/opencvlibrary/files/opencv-unix/$opencv_version/opencv-$opencv_version.zip/download/
unzip OpenCV.zip
rm OpenCV.zip

wget -c -O OpenCV-contrib.zip https://github.com/opencv/opencv_contrib/archive/$opencv_version.zip
unzip OpenCV-contrib.zip
rm OpenCV-contrib.zip

cd opencv-$opencv_version
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib-$opencv_version/modules ..
make -j 4
sudo make install

# CppRest interface
sudo apt-get install libcpprest-dev -y

## Fetch and install Basler Pylon drivers
cd ~/local
wget -c -O pylon_5.0.12.11829-deb0_amd64.deb https://www.baslerweb.com/fp-1523350893/media/downloads/software/pylon_software/pylon_5.0.12.11829-deb0_amd64.deb
sudo dpkg -i  pylon_5.0.12.11829-deb0_amd64.deb
rm pylon_5.0.12.11829-deb0_amd64.deb

# Fetch and install dlib library
cd ~/local
mkdir dlib
cd dlib
wget -c -O dlib-19.12.tar.bz2 http://dlib.net/files/dlib-19.12.tar.bz2
tar jxf dlib-19.12.tar.bz2
rm dlib-19.12.tar.bz2
cd dlib-19.12
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j4
sudo make install

# Install ros-melodic-full-desktop (Right now industrial-messages is not released for melodic - should be build in workspace)
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116
sudo apt-get update
sudo apt-get install ros-melodic-desktop-full -y
sudo apt-get install ros-melodic-rosbridge-suite ros-melodic-tf2-web-republisher -y


# Setup ROS
sudo rosdep init
rosdep update

echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
source /opt/ros/melodic/setup.bash

source ~/.bashrc

# Setup ROS Workspace
mkdir -p ~/code/catkin_ws/src
cd ~/code/catkin_ws/
catkin_make
echo "source ~/code/catkin_ws/devel/setup.bash" >> ~/.bashrc
source ~/code/catkin_ws/devel/setup.bash

## Fetch industrial_core and compile (necessary as industrial_msgs not rleased for melodic yet)
sudo apt-get install ros-melodic-moveit -y
cd ~/code/catkin_ws/src
git clone https://github.com/ros-industrial/industrial_core.git
cd ~/code/catkin_ws
catkin_make

# Install the RealSenseSDK
echo 'deb http://realsense-hw-public.s3.amazonaws.com/Debian/apt-repo bionic main' | sudo tee /etc/apt/sources.list.d/realsense-public.list
sudo apt-key adv --keyserver keys.gnupg.net --recv-key 6F3EFCDE
sudo apt-get update
sudo apt-get install librealsense2-dkms librealsense2-utils librealsense2-dev librealsense2-dbg -y

# Dependencies for ER_Base
sudo apt-get install libudev-dev openjdk-8-jdk -y

# Workcell Calibration from Base repository
cd ~/code/base/ext/WorkCellCalibration
mkdir build
cd build
mkdir Release
cd Release
cmake -DCMAKE_BUILD_TYPE=Release ../..
make -j4

# Make xmlrpc interface from Base repository
cd ~/code/base/ext/xmlrpc++0.7
make -j4

# Make build directory for base repository
cd ~/code/base
mkdir build
cd build
mkdir Release
cd Release

# Build base repository
cmake -DCMAKE_BUILD_TYPE=Release -DER_BUILD_TESTS=0 ../..
make -j4

# Set ER_Base and ER_One locations
echo "export ER_BASE=~/code/base" >> ~/.bashrc
export ER_BASE=~/code/base
echo "export ER_ONE=~/code/er-one/server" >> ~/.bashrc
export ER_ONE=~/code/er-one/server

# Fetch er_light and compile it
cd ~/code
git clone https://$gitUser:$gitPass@gitlab.com/enabled-robotics/er-one.git
cd er-one/server
git remote set-url origin https://gitlab.com/enabled-robotics/er-one.git
mkdir build
cd build
mkdir Release
cd Release
cmake -DCMAKE_BUILD_TYPE=Release ../..
make -j4

cd ~/code/er-one/urcaps
./buildAll.sh

# Fetch data repository
cd ~/code
git clone https://$gitUser:$gitPass@gitlab.com/enabled-robotics/data.git
cd data
git remote set-url origin https://gitlab.com/enabled-robotics/data.git

# Add udev rules to allow all access to econ cams
cd ~/code/base/Installation
sudo cp 68-econ-cameras.rules /etc/udev/rules.d/68-econ-cameras.rules

# Add a dummy video driver, to make remote desktop run (Seems to ruin ubuntu 18.04 installation)
#sudo apt-get install xserver-xorg-video-dummy-hwe-18.04
sudo cp xorg.conf /etc/X11/xorg.conf

# Set power button to shut down computer when pressed
sudo cp ~/code/base/Installation/power.in /etc/acpi/events/power

# Clone and build ur_modern_driver
cd ~/code/catkin_ws/src
git clone -b kinetic-devel https://github.com/ros-industrial/universal_robot.git
#git clone -b kinetic-devel https://github.com/ros-industrial/ur_modern_driver.git
# clone dniewinskies modern branch, as it incluydes eSeries robots
git clone https://github.com/dniewinski/ur_modern_driver.git
cd ~/code/catkin_ws
catkin_make -DCMAKE_BUILD_TYPE=Release

# Clone er_ros repository and make links in catkin workspace
cd ~/code
git clone https://$gitUser:$gitPass@gitlab.com/enabled-robotics/er_ros.git

ln -s ~/code/er_ros/src/er_base_ros ~/code/catkin_ws/src
ln -s ~/code/er_ros/src/er_camera_ros ~/code/catkin_ws/src
ln -s ~/code/er_ros/src/er_robot_ros ~/code/catkin_ws/src
ln -s ~/code/er-one/ros ~/code/catkin_ws/src/er-one
ln -s ~/code/er-ability/cmake ~/code/catkin_ws
ln -s ~/code/er_ros/src/er_backend_control ~/code/catkin_ws/src
ln -s ~/code/er_ros/src/er_hwl_ros ~/code/catkin_ws/src
ln -s ~/code/er_ros/src/er_web_visualizer ~/code/catkin_ws/src
ln -s ~/code/er-ability/ros/src/ability_backend ~/code/catkin_ws/src
ln -s ~/code/er-ability/ros/src/program_backend ~/code/catkin_ws/src
ln -s ~/code/er-ability/ros/src/backend_modules ~/code/catkin_ws/src
ln -s ~/code/er-ability/ros/src/system_backend ~/code/catkin_ws/src

cd ~/code/catkin_ws
catkin_make -DCMAKE_BUILD_TYPE=Release

# Setup apache webservier to host cad files for ros and the web visualizer
sudo apt-get install apache2 -y

sudo cp ~/code/base/Installation/er.com.conf /etc/apache2/sites-available/er.com.conf
sudo cp ~/code/base/Installation/apache2.conf /etc/apache2/apache2.conf
sudo cp ~/code/base/Installation/ports.conf /etc/apache2/ports.conf

sudo a2enmod headers
sudo a2ensite er.com.conf
sudo a2dissite 000-default.conf
service apache2 reload
sudo systemctl restart apache2
sudo service apache2 restart

# Make service launching ros nodes (Seems more stable to do with a shell script)
#sudo apt-get install ros-kinetic-robot-upstart -y
#cd ~/code/catkin_ws/src
#rosrun robot_upstart install er_web_visualizer/launch/launch_web_app.launch --job ER_WebVisualizer --logdir /home/thomas/log/ --rosdistro kinetic --symlink

#sudo systemctl daemon-reload
#sudo systemctl start ER_WebVisualizer

sudo reboot


