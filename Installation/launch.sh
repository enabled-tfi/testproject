#!/bin/bash

source /opt/ros/melodic/setup.bash
source /home/enabled/code/catkin_ws/devel/setup.bash

sleep 5

now=$(date +"%d-%m-%y - %T")
echo "Current time : $now" >> lastStart.txt

roslaunch er_ros_application er_full_system.launch

