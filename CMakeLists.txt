#####################################################
# Template for building RobWork dependent projects
# - the src should contain code for putting into libraries
# - the plugins contain code for multiple RobWorkStudio plugins
# - the test for testing
# - the example dir for illustrativ examples of your project
#
# Use config.cmake to change default settings and edit this
# file for adding additional dependencies to external projects
#####################################################

#
# Test CMake version
#
cmake_minimum_required(VERSION 2.8.3)

# The name of the project.
PROJECT(EnabledRoboticsFrameWork)

# Used to resolve absolute path names
SET(ROOT ${CMAKE_CURRENT_SOURCE_DIR})

set(CMAKE_VERBOSE_MAKEFILE True)

# If not the -DCMAKE_BUILD_TYPE option is given when running CMake, use Release as default.
IF(NOT CMAKE_BUILD_TYPE )
    SET(CMAKE_BUILD_TYPE Release)
ENDIF()
MESSAGE("-- Build type: " ${CMAKE_BUILD_TYPE})

IF (WIN32)
  set(OpenCV_DIR "C:/local/OpenCV-3.2/build")
  find_package( OpenCV 3.2 REQUIRED)
ELSE()
  find_package( OpenCV 3.3 REQUIRED)
ENDIF()

message("OpenCV include dir:" ${OpenCV_INCLUDE_DIRS})
include_directories( ${OpenCV_INCLUDE_DIRS} )

# Now set the RW/RWS root (edit this if necessary)
IF(WIN32)
  # On Windows platforms, the RW/RWS installers create the necessary environment variables
  IF(DEFINED ENV{RW_ROOT} AND DEFINED ENV{RWS_ROOT})
    SET(RW_ROOT "$ENV{RW_ROOT}")
    SET(RWSTUDIO_ROOT "$ENV{RWS_ROOT}")
  ELSE()
    SET(RW_ROOT "${ROOT}/../../../RobWork")
    SET(RWSTUDIO_ROOT "${ROOT}/../../../RobWorkStudio")
  ENDIF()

  IF (DEFINED ENV{RWHW_ROOT})
	SET(RWHW_ROOT "$ENV{RWHW_ROOT}")
  ELSE()
	SET(RWHW_ROOT "${RW_ROOT}/../RobWorkHardware")
  ENDIF()
ELSEIF(UNIX)
  # On Ubuntu platforms, the Debian packages install into /usr/local or /usr
  IF(EXISTS "/usr/local/RobWork" AND EXISTS "/usr/local/RobWorkStudio" AND EXISTS "/usr/local/RobWorkHardware")
    SET(RW_ROOT "/usr/local/RobWork")
    SET(RWSTUDIO_ROOT "/usr/local/RobWorkStudio")
	SET(RWHW_ROOT "/usr/local/RobWorkHardware")
  ELSEIF(EXISTS "/usr/RobWork" AND EXISTS "/usr/RobWorkStudio" AND EXISTS "/usr/RobWorkHardware")
    SET(RW_ROOT "/usr/RobWork")
    SET(RWSTUDIO_ROOT "/usr/RobWorkStudio")
	SET(RWHW_ROOT "/usr/RobWorkHardware")
  ELSE()
    SET(RW_ROOT "$ENV{RW_ROOT}")
    SET(RWSTUDIO_ROOT "$ENV{RWS_ROOT}")
    SET(RWHW_ROOT "$ENV{RWHW_ROOT}")
  ENDIF()
ELSE()
  SET(RW_ROOT "~/RobWork/RobWork")
  SET(RWSTUDIO_ROOT "~/RobWork/RobWorkStudio")
  SET(RWHW_ROOT "~/RobWork/RobWorkHardware")
ENDIF()

#Include default settings for constructing a robwork dependent project
SET(RobWork_DIR ${RW_ROOT}/cmake)
FIND_PACKAGE(RobWork REQUIRED)
INCLUDE_DIRECTORIES( ${ROBWORK_INCLUDE_DIRS} )
LINK_DIRECTORIES( ${ROBWORK_LIBRARY_DIRS} )
MESSAGE("-- RobWork found: " ${RW_ROOT})

#Include default settings for constructing a robworkstudio dependent project
SET(RobWorkStudio_DIR ${RWSTUDIO_ROOT}/cmake)
FIND_PACKAGE(RobWorkStudio REQUIRED)
INCLUDE_DIRECTORIES( ${ROBWORKSTUDIO_INCLUDE_DIRS} )
LINK_DIRECTORIES( ${ROBWORKSTUDIO_LIBRARY_DIRS} )
MESSAGE("-- RobWorkStudio found: " ${RWSTUDIO_ROOT})
#find_package(Qt5WebEngineWidgets)

#Include default settings for constructing a robworkhardware dependent project
SET(RobWorkHardware_DIR ${RWHW_ROOT}/cmake)
FIND_PACKAGE(RobWorkHardware COMPONENTS serialport universalrobots )
INCLUDE_DIRECTORIES( ${ROBWORKHARDWARE_INCLUDE_DIRS} )
INCLUDE_DIRECTORIES( ${ROBWORKHARDWARE_INCLUDE_DIRS}../Release )
LINK_DIRECTORIES( ${ROBWORKHARDWARE_LIBRARY_DIRS} )
LINK_DIRECTORIES( ${ROBWORKHARDWARE_LIBRARY_DIRS}../Release )
MESSAGE("-- RobWorkHardware found: " ${RWHW_ROOT})
MESSAGE("-- RobWorkHardware Include: " ${ROBWORKHARDWARE_INCLUDE_DIRS})

# Set the output dir for generated libraries and binaries
IF(MSVC)
	SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${ROOT}/bin" CACHE PATH "Runtime directory" FORCE)
	SET(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${ROOT}/libs" CACHE PATH "Library directory" FORCE)
	SET(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${ROOT}/libs" CACHE PATH "Archive directory" FORCE)
ELSE()
	SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${ROOT}/bin/${CMAKE_BUILD_TYPE}" CACHE PATH "Runtime directory" FORCE)
	SET(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${ROOT}/libs/${CMAKE_BUILD_TYPE}" CACHE PATH "Library directory" FORCE)
	SET(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${ROOT}/libs/${CMAKE_BUILD_TYPE}" CACHE PATH "Archive directory" FORCE)
ENDIF()

include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
if(COMPILER_SUPPORTS_CXX11)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
else()
        message(STATUS "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
endif()

find_package(PCL 1.7 REQUIRED ) 
find_package(VTK REQUIRED )

include_directories(${PCL_INCLUDE_DIRS} ${ER_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS} ${ER_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

if (UNIX)
	SET(CPPREST_LIBRARIES cpprest ssl crypto)
else()
	include_directories("C:/local/cpprestsdk-master/Release/include")
	link_directories("C:/local/cpprestsdk-master/Binaries/x64/Release")
	link_directories("C:/local/cpprestsdk-master/Binaries/Win32/Release")
	IF(CMAKE_BUILD_TYPE MATCHES Debug)
		SET(CPPREST_LIBRARIES cpprest140d_2_8)
	else()
		SET(CPPREST_LIBRARIES cpprest140_2_8)
	endif()
endif()


include_directories("../LeuzeLPS36/")
link_directories("../LeuzeLPS36/${CMAKE_BUILD_TYPE}")

#IF(WIN32)
	#SET(WORKCELLCALIBRATION "c:/local/WorkCellCalibration")
#ELSE()
	SET(WORKCELLCALIBRATION "ext/WorkCellCalibration")
#ENDIF()
include_directories("${WORKCELLCALIBRATION}/../")
link_directories("${WORKCELLCALIBRATION}/libs/${CMAKE_BUILD_TYPE}")

include_directories(${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/src)


add_subdirectory(src)

option(ER_BUILD_TESTS "Whether to build the gTests" OFF)

#CMAKE_DEPENDENT_OPTION(ER_IS_TESTS_ENABLED "Set when you want to build the tests" ON "${ER_BUILD_TESTS}" OFF)
IF(ER_BUILD_TESTS )
	enable_testing()
	add_subdirectory(gtest)
ELSE()
	MESSAGE("Tests not enabled")
ENDIF()

